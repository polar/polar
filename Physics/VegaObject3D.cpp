#include "VegaObject3D.h"
#include <generateMeshGraph.h>
#include <cstdio>
#include <tetMesh.h>
#include <cubicMesh.h>
#include <QDebug>

using namespace PoLAR;

struct VegaObject3D::MeshUpdater: public osg::Drawable::UpdateCallback
{
    MeshUpdater(VegaObject3D *obj)
        : _obj(obj)
    {}

    virtual ~MeshUpdater(){}

    virtual void update(osg::NodeVisitor*, osg::Drawable* draw)
    {
        if(_obj->_requestMeshUpdate)
        {
            osg::Geometry* geom(draw->asGeometry());
            osg::Vec3Array* verts(dynamic_cast<osg::Vec3Array*>(geom->getVertexArray()));
            int numTetraVertices = 3 * _obj->_voluMesh->getNumVertices();
            double *u = (double*) calloc (numTetraVertices, sizeof(double));
            memcpy(u, _obj->_integrator->Getq(), sizeof(double) * numTetraVertices);

            float *array = (float*)verts->getDataPointer();
            for (unsigned int ptSimu=0; ptSimu<verts->size(); ptSimu++)
            {
	      //                double posx = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][0]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+0]);
	      //                double posy = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][1]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+1]);
	      //                double posz = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][2]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+2]);

                double posx = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+0]);
                double posy = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[1]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+1]);
                double posz = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[2]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+2]);
		  /*
                 double posx = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][0]+u[3*_obj->_indexInSoftBody[ptSimu]+0]);
                double posy = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][1]+u[3*_obj->_indexInSoftBody[ptSimu]+1]);
                double posz = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][2]+u[3*_obj->_indexInSoftBody[ptSimu]+2]);
                */
                array[0] = posx;
                array[1] = posy;
                array[2] = posz;
                array += 3;
            }
            verts->dirty();
            //draw->dirtyBound();
            _obj->dirty();
            free(u);

            _obj->_requestMeshUpdate = false;
        }
    }

    VegaObject3D* _obj;
};


VegaObject3D::VegaObject3D(Node *node, const char* vegaFile,  bool material, bool editable, bool pickable):
    PhysicsObject3D(node, material, editable, pickable),
    _integrator(NULL), _voluMesh(NULL), _meshGraph(NULL), _indexInSoftBody(NULL), _indexInOsg(NULL),
    _verticesArray(NULL), _externalForces(NULL), _defaultExternalForces(NULL), _requestMeshUpdate(false)
{
    //Load a 3D mesh
    bool b = loadVolumetricMesh(vegaFile);

    if(b)
    {
        // Create mesh graph for fast Neighbors finding
        _meshGraph = GenerateMeshGraph::Generate(_voluMesh);

        computeOsgToVegaIndices();

        copyOriginalVertices();

        osg::Drawable *draw = getDrawable(0);
        draw->setUpdateCallback(new MeshUpdater(this));
        allocFExt();
    }
}


VegaObject3D::VegaObject3D(const std::string& filename, const char* vegaFile,  bool material, bool editable, bool pickable):
    PhysicsObject3D(filename, material, editable, pickable),
    _integrator(NULL), _voluMesh(NULL), _meshGraph(NULL), _indexInSoftBody(NULL), _indexInOsg(NULL),
    _verticesArray(NULL), _externalForces(NULL), _defaultExternalForces(NULL), _requestMeshUpdate(false)
{
    //Load a 3D mesh
    bool b = loadVolumetricMesh(vegaFile);

    if(b)
    {
        // Create mesh graph for fast Neighbors finding
        _meshGraph = GenerateMeshGraph::Generate(_voluMesh);

        computeOsgToVegaIndices();

        copyOriginalVertices();

        osg::Drawable *draw = getDrawable(0);
        draw->setUpdateCallback(new MeshUpdater(this));
        allocFExt();
    }
}


VegaObject3D::~VegaObject3D()
{
    if(_indexInSoftBody) delete[] _indexInSoftBody;
    if(_indexInOsg) delete[] _indexInOsg;
    if(_meshGraph) delete _meshGraph;
    //if(_integrator) delete _integrator;

    std::list<VegaInteraction*>::iterator it;
    for(it=_forcesList.begin(); it!=_forcesList.end(); ++it)
    {
        VegaInteraction *force = *it;
        delete force;
    }
    _forcesList.clear();

    if(_externalForces) free(_externalForces);
    if(_defaultExternalForces) free(_defaultExternalForces);
}




void VegaObject3D::setIntegrator(IntegratorBase *integrator)
{
    if(_integrator) delete _integrator;
    _integrator = integrator;
}


void VegaObject3D::addForce(VegaInteraction *force)
{
    if(force)
        _forcesList.push_back(force);
}


void VegaObject3D::removeForce(VegaInteraction *force)
{
    if(force)
    {
        _forcesList.remove(force);
    }

}

void VegaObject3D::initForces()
{
    std::list<VegaInteraction*>::iterator it;
    for(it=_forcesList.begin(); it!=_forcesList.end(); ++it)
    {
        (*it)->initializeForce(this);
    }
}


double *VegaObject3D::applyForces()
{

    memcpy(_externalForces, _defaultExternalForces, sizeof(double) * _voluMesh->getNumVertices()*3);
    std::list<VegaInteraction*>::iterator it;
    for(it=_forcesList.begin(); it!=_forcesList.end(); ++it)
    {
        double* force = (*it)->applyForce(this);
        if(force)
        {
            for(int i=0; i<_voluMesh->getNumVertices()*3; i++)
            {
                _externalForces[i] += force[i];
            }
        }
    }
    return _externalForces;
}


void VegaObject3D::applyForceToFace()
{
    int numVertices;
    double *vertices;
    int numElements;
    int numElementVertices;
    int *elements;
    _voluMesh->exportMeshGeometry(&numVertices, &vertices, &numElements, &numElementVertices, &elements);

    //std::cout << numVertices << " " << numElements << " " << numElementVertices << std::endl;
    //std::cout << elements[numElements * numElementVertices-1] << std::endl;
    //std::cout << vertices[numVertices * 3 - 1] << std::endl;

    //int index = 4;
    //std::cout << elements[index] << " " << elements[index+1] << " " << elements[index+2] << " " << elements[index+3] << " " << std::endl;

    //int indexel = 3878;

    //Vec3d vec(vertices[3*elements[indexel]], vertices[3*elements[indexel]+1], vertices[3*elements[indexel]+2]);
    //Vec3d vec(vertices[3*indexel], vertices[3*indexel+1], vertices[3*indexel+2]);
    // Vec3d *vec = _voluMesh->getVertex(315);
    // std::cout << "vertex: " << vec[0] << " " << vec[1] << " " << vec[2] << std::endl;
    // for(int i=0; i<numElements; i++)
    // {
    //     if(_voluMesh->containsVertex(i, *vec))
    //         std::cout << "i:  " << i <<std::endl;
    // }
    //std::cout << _voluMesh->containsVertex(0, vec) << std::endl;
}


/*
void VegaObject3D::updateOsgMesh()
{
    if(_verticesArray)
    {
        double *u = (double*) calloc (_numTetraVertices, sizeof(double));
        memcpy(u, _integrator->Getq(), sizeof(double) * _numTetraVertices);

        unsigned int size = _verticesArray->size();
        float *array = (float*)_verticesArray->getDataPointer();

        for (unsigned int ptSimu=0; ptSimu<size; ptSimu++)
        {
            double posx = (_tetraMesh->getVertex(_indexInTetra[ptSimu])[0][0]+1.5*u[3*_indexInTetra[ptSimu]+0]);
            double posy = (_tetraMesh->getVertex(_indexInTetra[ptSimu])[0][1]+1.5*u[3*_indexInTetra[ptSimu]+1]);
            double posz = (_tetraMesh->getVertex(_indexInTetra[ptSimu])[0][2]+1.5*u[3*_indexInTetra[ptSimu]+2]);

            array[0] = posx;
            array[1] = posy;
            array[2] = posz;
            array += 3;

        }

        _verticesArray->dirty();
        this->dirty();
        free(u);
    }
}
*/

void VegaObject3D::resetSimulation()
{
    //resetAllManipulations();
    osg::Drawable *draw = getDrawable(0);
    draw->setUpdateCallback(NULL);
    float *verts = (float*)_verticesArray->getDataPointer();
    memcpy(verts, _originalVertices, 3*_verticesArray->size()*sizeof(float));

    _verticesArray->dirty();
    this->dirty();

    std::list<VegaInteraction*>::iterator it;
    for(it=_forcesList.begin(); it!=_forcesList.end(); ++it)
    {
        (*it)->resetForce(this);
    }
    _integrator->SetExternalForcesToZero();
    _integrator->ResetToRest();
    draw->setUpdateCallback(new MeshUpdater(this));
}


void VegaObject3D::computeOsgToVegaIndices()
{
    osg::Vec3Array *objvertices = getVertexList(0);
    if(objvertices)
    {
        float *array = (float*)objvertices->getDataPointer();
        if(array)
        {
            unsigned int size = objvertices->size();
            _indexInSoftBody = new unsigned int[size];
            for(unsigned int i=0; i<size; i++)
            {
                Vec3d ptFound = Vec3d(array[0], array[1], array[2]);
                array++;
                array++;
                array++;
                _indexInSoftBody[i] = _voluMesh->getClosestVertex(ptFound);
            }

            _indexInOsg = new unsigned int[size];
            for(unsigned int i=0; i<size; i++)
            {
                _indexInOsg[_indexInSoftBody[i]] = i;
            }
        }
        else
            qCritical() << "Unable to get vertex array";

    }
    else
        qCritical() << "Unable to get vertex array";

    _verticesArray = objvertices;
}


bool VegaObject3D::loadVolumetricMesh(const char *vegaFile)
{
    VolumetricMesh * volumetricMesh = VolumetricMeshLoader::load(vegaFile);
    if (volumetricMesh == NULL)
    {
        qCritical() << "Error: failed to load mesh.";
        return false;
    }
    else
        qDebug() << "Load .veg file: Success. Number of vertices: " << volumetricMesh->getNumVertices()
                 << ". Number of elements: " << volumetricMesh->getNumElements();

    //Check and assign a 3D deformable model
    if (volumetricMesh->getElementType() == VolumetricMesh::TET)
        _voluMesh = (TetMesh*)volumetricMesh; // such down-casting is safe in Vega
    else
    {
        if(volumetricMesh->getElementType() == VolumetricMesh::CUBIC)
            _voluMesh = (CubicMesh*)volumetricMesh;
        else
        {
            qCritical() << "Error: invalid mesh type.";
            return false;
        }
    }
    return true;
}

void VegaObject3D::copyOriginalVertices()
{
    osg::Vec3Array *objvertices = getVertexList(0);
    if(!_originalVertices)
    {
        float *array = (float*)objvertices->getDataPointer();
        _originalVertices = new float[3*objvertices->size()];
        memcpy(_originalVertices, array, 3*objvertices->size()*sizeof(float));
    }
}


void VegaObject3D::allocFExt()
{
    int r = _voluMesh->getNumVertices()*3;
    _externalForces = (double*) calloc (r, sizeof(double));
    _defaultExternalForces = (double*) calloc (r, sizeof(double));
}
