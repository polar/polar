#include "BulletUtil.h"
#include <iostream>


using namespace PoLAR;
using namespace Bullet;

osg::ref_ptr<DynamicsWorld> DynamicsWorld::_ptr = NULL;
btSoftRigidDynamicsWorld* DynamicsWorld::_dynworld = NULL;
btConstraintSolver* DynamicsWorld::_solver = NULL;
btCollisionDispatcher* DynamicsWorld::_dispatcher = NULL;
btSoftBodyRigidBodyCollisionConfiguration*DynamicsWorld:: _collision = NULL;
btBroadphaseInterface* DynamicsWorld::_broadphase = NULL;
btSoftBodyWorldInfo* DynamicsWorld::_worldinfo = NULL;


DynamicsWorld::DynamicsWorld()
{
    initBulletWorld();
}


DynamicsWorld::~DynamicsWorld()
{
    delete _dynworld;
    delete _broadphase;
    delete _solver;
    delete _dispatcher;
    delete _collision;
    delete _worldinfo;
}

void DynamicsWorld::allocate()
{
    if(!_ptr.valid())
    {
        _ptr = new DynamicsWorld;
    }
}

DynamicsWorld* DynamicsWorld::getInstance()
{
    allocate();
    return _ptr.get();
}


btSoftBodyWorldInfo *DynamicsWorld::getWorldInfo()
{
    allocate();
    return _worldinfo;
}

btSoftRigidDynamicsWorld* DynamicsWorld::getDynamicsWorld()
{
    allocate();
    return _dynworld;
}

void DynamicsWorld::initBulletWorld()
{
    _worldinfo = new btSoftBodyWorldInfo;
    _collision = new btSoftBodyRigidBodyCollisionConfiguration();
    _dispatcher = new btCollisionDispatcher(_collision);
    _worldinfo->m_dispatcher = _dispatcher;
    _solver = new btSequentialImpulseConstraintSolver;

    btVector3 worldAabbMin(-10000, -10000, -10000);
    btVector3 worldAabbMax(10000, 10000, 10000);
    _broadphase = new btAxisSweep3(worldAabbMin, worldAabbMax, 1000);
    _worldinfo->m_broadphase = _broadphase;

    _dynworld = new btSoftRigidDynamicsWorld(_dispatcher, _broadphase, _solver, _collision);

    btVector3 gravity(0, 0, -10);
    _dynworld->setGravity(gravity);
    _worldinfo->m_gravity = gravity;

    _worldinfo->air_density = btScalar(1.2);
    _worldinfo->water_density = 0;
    _worldinfo->water_offset = 0;
    _worldinfo->water_normal = btVector3(0, 0, 0);
    _worldinfo->m_sparsesdf.Initialize();
}


osg::Matrixd Bullet::asOsgMatrix(const btTransform& t)
{
    btScalar val[16];
    t.getOpenGLMatrix(val);
    osg::Matrixd m(val);
    return m;
}


btTransform Bullet::asBulletMatrix(const osg::Matrixd& m)
{
    const osg::Matrixd::value_type* osgVal = m.ptr();
    btScalar bulletVal[16];
    for (int i=0; i<16; i++)
        bulletVal[i] = osgVal[i];
    btTransform t;
    t.setFromOpenGLMatrix(bulletVal);
    return t;
}
