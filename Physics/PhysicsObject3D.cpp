#include "PhysicsObject3D.h"

using namespace PoLAR;
using namespace osg;


PhysicsObject3D::PhysicsObject3D(Node *node, bool material, bool editable, bool pickable):
    Object3D(node, material, editable, pickable), _originalVertices(NULL)
{
    setEditOn();
}


PhysicsObject3D::PhysicsObject3D(const std::string& filename, bool material, bool editable, bool pickable):
    Object3D(filename, material, editable, pickable), _originalVertices(NULL)
{
    setEditOn();
}

PhysicsObject3D::~PhysicsObject3D()
{
    if(_originalVertices) delete []_originalVertices;
}
