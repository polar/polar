#include <osg/TriangleFunctor>
#include <osg/Matrixd>
#include <osg/io_utils>
#include <QDebug>
#include "BulletObject3D.h"

using namespace PoLAR;
using namespace osg;

BulletObject3D::BulletObject3D(Node *node, float mass, bool material, bool editable, bool pickable):
    PhysicsObject3D(node, material, editable, pickable), _bulletBody(NULL), _collisionShape(NULL), _motionState(NULL),
    _hasDefaultCollisionShape(false), _mass(mass),
    _angularVelocity(0.,0.,0.), _linearVelocity(0.,0.,0.)
{
}

BulletObject3D::BulletObject3D(const std::string& filename, float mass, bool material, bool editable, bool pickable):
    PhysicsObject3D(filename, material, editable, pickable), _bulletBody(NULL), _collisionShape(NULL), _motionState(NULL),
    _hasDefaultCollisionShape(false), _mass(mass),
    _angularVelocity(0.,0.,0.), _linearVelocity(0.,0.,0.)
{
}


BulletObject3D::~BulletObject3D()
{
    if(_collisionShape && _hasDefaultCollisionShape)
        delete _collisionShape;
    if(_bulletBody)
        delete _bulletBody;
    if(_motionState)
        delete _motionState;
}


btRigidBody *BulletObject3D::getBtRigidBody()
{

    btRigidBody *rigidBody = dynamic_cast<btRigidBody*>(_bulletBody);
    if(rigidBody) return rigidBody;

    qCritical() << getNameAsQString() << ": no valid rigid body";
    return NULL;
}


void BulletObject3D::createRigidBody(btCollisionShape *collisionShape)
{
    // this line to ensure all the transformations made before this point will be saved and left untouched after a simulation reset
    setFrameMatrix(osg::Matrixd::identity());
    _collisionShape = collisionShape;

    btVector3 localInertia(0., 0., 0.);
    if(_mass != 0.f)
    {
        _collisionShape->calculateLocalInertia(_mass, localInertia);
    }
    _motionState = new MotionState(this, _originalCenter);

    btRigidBody::btRigidBodyConstructionInfo rbInfo(_mass, _motionState, _collisionShape, localInertia);

    if(_bulletBody) delete _bulletBody;
    btRigidBody *rigidBody = new btRigidBody(rbInfo);
    if(!rigidBody)
    {
        qCritical() << getNameAsQString() << ": rigid body creation failed.";
    }
    else
    {
        btTransform wt;
        _motionState->getWorldTransform(wt);
        rigidBody->setWorldTransform(wt);
        rigidBody->setAngularVelocity(_angularVelocity);
        rigidBody->setLinearVelocity(_linearVelocity);
    }
    _bulletBody = rigidBody;
}


void BulletObject3D::createRigidBody()
{
    if(!_collisionShape) // we create a default shape
    {
        //_collisionShape = createShape();
        _collisionShape = createConvexHullShape();
    }
    _hasDefaultCollisionShape = true;
    createRigidBody(_collisionShape);
}



void BulletObject3D::setCollisionShape(btCollisionShape *shape)
{
    if(shape)
    {
        if(_collisionShape) delete _collisionShape;
        _collisionShape = shape;
    }
    else
        qCritical() << getNameAsQString() << ": Collision shape not valid";
}


/*
btTriangleMesh *BulletObject3D::createTriangleMesh()
{
    Vec3d scl = getTransformationMatrix().getScale();
    Matrixd mtemp(Matrixd::translate(-_origCenter) * Matrixd::scale(scl));

    TriangleMeshVisitor visitor;
    _originalNode->accept(visitor);
    Vec3Array *vertices = visitor.getVertices();
    btTriangleMesh *mesh = new btTriangleMesh;

    Vec3d p1, p2, p3;
    for(unsigned int i=0; i<vertices->size()-2; i+=3)
    {
        p1 = vertices->at(i) * mtemp;
        p2 = vertices->at(i+1) * mtemp;
        p3 = vertices->at(i+2) * mtemp;
        mesh->addTriangle(asBtVector3(p1), asBtVector3(p2), asBtVector3(p3));
    }
    return mesh;
}


btConvexTriangleMeshShape *BulletObject3D::createShape()
{
    btTriangleMesh *mesh = createTriangleMesh();
    btConvexTriangleMeshShape *shape = new btConvexTriangleMeshShape(mesh);


    return shape;
}
*/

btConvexHullShape *BulletObject3D::createConvexHullShape(bool withTransform)
{
    Vec3d scl = getTransformationMatrix().getScale();
    Matrixd mtemp(Matrixd::translate(-_originalCenter) * Matrixd::scale(scl));

    btConvexHullShape* chs = new btConvexHullShape();

    Vec3Array *vertices = getVertexList(0);
    unsigned int size = vertices->size();
    float *array = (float*)vertices->getDataPointer();
    for(unsigned int i=0; i<size; i++)
    {
        Vec3d ptFound(array[0], array[1], array[2]);
        array++;
        array++;
        array++;
        btVector3 btpt;
        if(withTransform)
            btpt = asBtVector3(ptFound * mtemp);
        else
            btpt = asBtVector3(ptFound);
        chs->addPoint(btpt);
    }
    return chs;
}


void BulletObject3D::setMass(float newMass)
{
    _mass = newMass;
    btRigidBody *rigidBody = dynamic_cast<btRigidBody*>(_bulletBody);
    if(rigidBody)
    {
        btVector3 localInertia(0., 0., 0.);
        if(_mass != 0.f && _collisionShape)
        {
            _collisionShape->calculateLocalInertia(_mass, localInertia);
        }
        rigidBody->setMassProps(_mass, localInertia);
    }
}


void BulletObject3D::resetSimulation()
{
    resetAllManipulations();
    btRigidBody *rigidBody = dynamic_cast<btRigidBody*>(_bulletBody);
    if(rigidBody) // btRigidBody
    {
        if(_motionState)
        {
            btTransform wt;
            _motionState->initTransform();
            _motionState->getWorldTransform(wt);
            rigidBody->setWorldTransform(wt);
        }
        rigidBody->setAngularVelocity(_angularVelocity);
        rigidBody->setLinearVelocity(_linearVelocity);
    }
}


void BulletObject3D::setEditOff()
{
    qCritical() << getNameAsQString() << ": Bullet objects cannot be uneditable";
}

