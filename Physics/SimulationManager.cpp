#include "SimulationManager.h"
#include "EventUtils.h"
#include <iostream>
#include <QDebug>

using namespace PoLAR;

const double SimulationManager::defaultTimeStep = 33.0; // 30 FPS
const std::string SimulationManager::threadPaused = "Physics thread paused";
const std::string SimulationManager::threadStopped = "Physics thread stopped";
const std::string SimulationManager::threadWorking = "Physics thread frame rate ";

SimulationManager::SimulationManager(SimulationWorker *worker, double timestep, bool launch):
    _qthread(NULL), _simWorker(worker), _timestep(timestep),  _hudText(new osgText::Text),
    _isFramerateDisplayed(false)
{
    _hudText->setCharacterSize(12);
    if(!_simWorker.get())
    {
        qCritical() << "No simulation worker set";
    }
    else
    {
        _simWorker->setTimeStep(_timestep);
        _simWorker->setLaunchOnStart(launch);
        _qthread = new QThread;
        _qthread->setObjectName("PoLARPhysics");
        init();
    }
}

SimulationManager::~SimulationManager()
{
    displayFramerate(false, NULL);
    stop();
    if(_qthread)
    {
        _qthread->wait();
        delete _qthread;
    }
    if(_hudNode.get() && _sceneGraph.get() && _sceneGraph->getSceneGraph()->containsNode(_hudNode.get()))
    {
        _sceneGraph->getSceneGraph()->removeChild(_hudNode.get());
    }
}


void SimulationManager::init()
{
    if(_simWorker.get())
    {
        _simWorker->moveToThread(_qthread);
        QObject::connect(_simWorker.get(), SIGNAL(updateFramerate(double)), this, SLOT(updateFramerate(double)), Qt::UniqueConnection);
        QObject::connect(_qthread, SIGNAL(started()), _simWorker.get(), SLOT(init()));
        QObject::connect(_simWorker.get(), SIGNAL(finished()), _qthread, SLOT(quit()), Qt::DirectConnection);
    }
}

void SimulationManager::launch()
{
    if(_qthread) _qthread->start(QThread::HighestPriority);
}


void SimulationManager::pause()
{
    if(_simWorker.get())
    {
        _simWorker->pause();
        if(isFramerateDisplayed())
        {
            _hudText->setText(threadPaused);
        }
    }
}

void SimulationManager::play()
{
    if(_simWorker.get()) _simWorker->play();
}

void SimulationManager::stop()
{
    if(_simWorker.get())
    {
        _simWorker->stop();
        if(isFramerateDisplayed())
        {
            _hudText->setText(threadStopped);
        }
    }
}


bool SimulationManager::isFramerateDisplayed() const
{
    return _isFramerateDisplayed;
}


void SimulationManager::displayFramerate(bool b, Viewer *viewer)
{
    _isFramerateDisplayed = b;
    if(_simWorker.get()) _simWorker->displayFramerate(b);

    if(b)
    { // create the HUD if not already existing
        if(!_hudNode.get() && viewer)
        {
            _hudNode = createHUD(viewer->width(), viewer->height(), _hudText.get());
            _hudText->setColor(osg::Vec4(1.f,1.f,1.f,1.f));
            _hudText->setPosition(osg::Vec3(5.f, viewer->height()-20.f, 0.f));
        }
        if(viewer && !(viewer->getSceneGraph()->getSceneGraph()->containsNode(_hudNode.get())))
        {
            viewer->getSceneGraph()->getSceneGraph()->addChild(_hudNode.get());
            _sceneGraph = viewer->getSceneGraph();
        }
        if(_simWorker.get() && !_simWorker->isWorking())
        {
            _hudText->setText(threadPaused);
        }
    }
    else
        _hudText->setText("");
}


void SimulationManager::updateFramerate(double fps)
{
    if(isRunning() && _isFramerateDisplayed)
    {
        std::ostringstream os;
        os << threadWorking;
        os << fps;
        _hudText->setText(os.str());
    }
}

/*
        void SimulationManager::reset()
        {
            if(_qthread && _qthread->isRunning())
            {
                stop();
                _qthread->wait();
                delete _qthread;
                _qthread = NULL;
            }
        //    _qthread = new QThread;
        //    init();
        //    _simWorker->setTimeStep(_timestep);
        //    launch();
        }
        */
