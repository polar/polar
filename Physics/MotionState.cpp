#include "MotionState.h"
#include "Util.h"
#include "BulletUtil.h"
#include <osg/MatrixTransform>
#include <osg/Notify>
#include <osg/io_utils>
#include <iostream>


using namespace PoLAR;
using namespace Util;
using namespace Bullet;

MotionState::MotionState(PhysicsObject3D *object, const osg::Vec3d& centerOfMass):
    _object(object), _centerOfMass(centerOfMass)
{
    // Bullet does not handle scaled rotation matrix. We need to normalize it and re-add the scale later.
    _parentTransform = osg::Matrixd::orthoNormal(object->getTransformationMatrix());
    _scale = object->getTransformationMatrix().getScale();
    _transform.setIdentity();
    initTransform();
}


void MotionState::setWorldTransform(const btTransform& worldTrans)
{
    // get the transform matrix from Bullet
    _transform = worldTrans;

    // then update the OSG object's rotation and translation
    const osg::Matrix dt = asOsgMatrix(_transform);
    const osg::Matrix t = _bulletToOsgMatrix * dt;

    //_object->setTransformationMatrix(t);
    osg::Quat rot = dt.getRotate();
    osg::Vec3d trans = t.getTrans();

    if(rot != _previousRotation)
    {
        _object->rotate(_previousRotation.inverse() * rot);
        _previousRotation = rot;
    }
    if(trans != _previousTranslation)
    {
        _object->translate(trans - _previousTranslation);
        _previousTranslation = trans;
    }
}

void MotionState::getWorldTransform(btTransform& worldTrans) const
{
    worldTrans = _transform;
}


void MotionState::initTransform()
{
    // multiply OSG center with the scale to get the translation of it into Bullet world (and vice versa)
    const osg::Vec3d coms = multVec3(_centerOfMass, _scale);
    _osgToBulletMatrix = osg::Matrixd::translate(coms); // Bullet does not handle scaled matrix
    _bulletToOsgMatrix = osg::Matrixd::scale(_scale) * osg::Matrixd::translate(-coms); // But we need to scale back from Bullet to OSG

    // get the parent transform into Bullet world
    osg::Matrixd worldTransform = _osgToBulletMatrix * _parentTransform;

    _previousRotation = worldTransform.getRotate();
    _previousTranslation = _parentTransform.getTrans();

    setWorldTransform(asBulletMatrix(worldTransform));
}

