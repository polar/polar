#include "VegaWorker.h"
#include <QDebug>

using namespace PoLAR;
using namespace std;


VegaWorker::VegaWorker(IntegratorBase *integrator):
    _integrator(integrator),
    _f_extBase(NULL),
    _f_ext(NULL),
    _r(0)
{
    allocFExt();
}

VegaWorker::~VegaWorker()
{
    if(_f_ext) free(_f_ext);
    if(_f_extBase) free(_f_extBase);
    if (_integrator) delete _integrator;
}


void VegaWorker::setExternalForcesToBaseForce()
{
    if(_f_ext && _f_extBase)
        // reset external forces (usually to zero)
        memcpy(_f_ext, _f_extBase, sizeof(double) * _r);
}

void VegaWorker::setIntegrator(IntegratorBase *integrator)
{
    if(_integrator) delete _integrator;
    _integrator = integrator;
    allocFExt();
}


int VegaWorker::doTimestep()
{
    if(_integrator)
        return _integrator->DoTimestep();
    else return -1;
}


void VegaWorker::allocFExt()
{
    if(_integrator)
    {
        _r = _integrator->Getr();
        _f_extBase  = (double*) calloc (_r, sizeof(double));
        _f_ext = (double*) calloc (_r, sizeof(double));
    }
}


void VegaWorker::addExternalForce(double *force)
{
    if(_f_ext && force)
    {
        for(int i=0; i<_r; i++)
        {
            _f_ext[i] += force[i];
        }
    }

}

void VegaWorker::setExternalForce(double *force)
{
    if(_f_ext && force)
    {
        memcpy(_f_ext, force, sizeof(double) * _r);
    }
}


void VegaWorker::setExternalForcesToIntegrator()
{
    if(_integrator) _integrator->SetExternalForces(_f_ext);
}

void VegaWorker::setExternalForcesToIntegrator(double* force)
{
    if(_integrator) _integrator->SetExternalForces(force);
}

void VegaWorker::resetExternalForces()
{
    free(_f_ext);
    free(_f_extBase);
    _f_ext = NULL;
    _f_extBase = NULL;
    allocFExt();
    setExternalForcesToIntegrator();
}

void VegaWorker::setBaseExternalForce(double *force)
{
    if(force && _f_extBase)
        memcpy(_f_extBase, force, sizeof(double) * _r);
}
