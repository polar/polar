#include "BulletWorker.h"


using namespace PoLAR;


BulletWorker::~BulletWorker()
{
}


void BulletWorker::run()
{
    const double currSimTime = getTimeStep();
    _bulletWorld->stepSimulation(currSimTime);
}
