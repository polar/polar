#include "BulletSoftObject3D.h"
#include "Util.h"
#include "BulletUtil.h"
#include <osg/Matrixd>
#include <osg/io_utils>
#include <BulletSoftBody/btSoftBodyHelpers.h>
#include <osg/LightModel>
#include <map>
#include <QDebug>

using namespace PoLAR;
using namespace osg;


struct BulletSoftObject3D::MeshUpdater : public osg::Drawable::UpdateCallback
{
    MeshUpdater(const btSoftBody* softBody, unsigned int *indexInBullet)
        : _softBody(softBody),
          _indexInBullet(indexInBullet)
    {}

    virtual ~MeshUpdater(){}

    virtual void update(osg::NodeVisitor*, osg::Drawable* draw)
    {
        osg::Geometry* geom(draw->asGeometry());
        osg::Vec3Array* verts( dynamic_cast<osg::Vec3Array*>(geom->getVertexArray()));

        // Update the vertex array from the softbody node array.
        const btSoftBody::tNodeArray& nodes = _softBody->m_nodes;
        osg::Vec3Array::iterator it(verts->begin());
        for(unsigned int i=0; i<verts->size(); i++)
        {
            unsigned int ind = _indexInBullet[i];
            *it++ = osg::Vec3(nodes[ind].m_x.x(), nodes[ind].m_x.y(), nodes[ind].m_x.z());
        }
        verts->dirty();
        draw->dirtyBound();

        // Generate new normals.
        osgUtil::SmoothingVisitor smooth;
        smooth.smooth(*geom);
        geom->getNormalArray()->dirty();
    }

    const btSoftBody* _softBody;
    unsigned int *_indexInBullet;
};


BulletSoftObject3D::BulletSoftObject3D(Node *node, float mass, bool material, bool editable, bool pickable):
    BulletObject3D(node, mass, material, editable, pickable),
    _indexInBullet(NULL), _indexInOsg(NULL)
{
    _transform.setIdentity();
    _scale = btVector3(1.0,1.0,1.0);
}

BulletSoftObject3D::BulletSoftObject3D(const std::string& filename, float mass, bool material, bool editable, bool pickable):
    BulletObject3D(filename, mass, material, editable, pickable),
    _indexInBullet(NULL), _indexInOsg(NULL)
{
    _transform.setIdentity();
    _scale = btVector3(1.0,1.0,1.0);
}


BulletSoftObject3D::BulletSoftObject3D(Node *node, btSoftBody *softBody, bool material, bool editable, bool pickable):
    BulletObject3D(node, softBody->getTotalMass(), material, editable, pickable),
    _indexInBullet(NULL), _indexInOsg(NULL)
{
    _transform.setIdentity();
    _scale = btVector3(1.0,1.0,1.0);
    setBtSoftBody(softBody);
}

BulletSoftObject3D::BulletSoftObject3D(btSoftBody *softBody, bool material, bool editable, bool pickable):
    BulletObject3D(nodeFromBtSoftBody(softBody), softBody->getTotalMass(), material, editable, pickable),
    _indexInBullet(NULL), _indexInOsg(NULL)
{
    _transform.setIdentity();
    _scale = btVector3(1.0,1.0,1.0);
    setBtSoftBody(softBody);
}


BulletSoftObject3D::~BulletSoftObject3D()
{
    if(_indexInBullet) delete [] _indexInBullet;
    if(_indexInOsg) delete [] _indexInOsg;
}


btSoftBody *BulletSoftObject3D::getBtSoftBody()
{

    btSoftBody *softBody = dynamic_cast<btSoftBody*>(_bulletBody);
    if(softBody) return softBody;

    qCritical() << getNameAsQString() << ": no valid soft body";
    return NULL;
}


void BulletSoftObject3D::createSoftBody()
{
    if(_bulletBody) delete _bulletBody;

    _collisionShape = createConvexHullShape(false);
    _hasDefaultCollisionShape = true;
    btConvexHullShape *shape = dynamic_cast<btConvexHullShape *>(_collisionShape);
    int numVertices = shape->getNumVertices();
    btVector3 *vertices = shape->getUnscaledPoints();
    btSoftBodyWorldInfo& softBodyWorldInfo = *(Bullet::DynamicsWorld::getWorldInfo());
    btSoftBody *softBody = btSoftBodyHelpers::CreateFromConvexHull(softBodyWorldInfo, vertices, numVertices);
    _bulletBody = softBody;
    osgToBulletIndices();
    osg::Vec3Array *objvertices = getVertexList(0);
    if(!_originalVertices)
    {
        float *array = (float*)objvertices->getDataPointer();
        _originalVertices = new float[3*objvertices->size()];
        memcpy(_originalVertices, array, 3*objvertices->size()*sizeof(float));
    }

    softBody->getCollisionShape()->setMargin(0.1);
    softBody->setTotalMass(_mass, true);
    softBody->scale(_scale);
    softBody->transform(_transform);
    softBody->setVelocity(_linearVelocity);

    osg::Drawable *draw = getDrawable(0);
    draw->setUpdateCallback(new MeshUpdater(softBody, _indexInBullet));

}


void BulletSoftObject3D::setBtSoftBody(btSoftBody *softbody)
{
    if(softbody)
    {
        _bulletBody = softbody;
        Vec3Array *osgVerts = getVertexList(0);
        int osgSize = osgVerts->size();
        int bulletSize = softbody->m_nodes.size();
        if(osgSize != bulletSize)
        {
            qCritical() << getNameAsQString() << ": softbody mesh does not match OSG mesh !";
            return;
        }
        else
        {
            _collisionShape = softbody->getCollisionShape();
            osgToBulletIndices();
            if(!_originalVertices)
            {
                float *array = (float*)osgVerts->getDataPointer();
                _originalVertices = new float[3*osgVerts->size()];
                memcpy(_originalVertices, array, 3*osgVerts->size()*sizeof(float));
            }

            //softbody->setTotalMass(_mass, true);
            softbody->scale(_scale);
            softbody->transform(_transform);
            softbody->setVelocity(_linearVelocity);
            osg::Drawable *draw = getDrawable(0);
            draw->setUpdateCallback(new MeshUpdater(softbody, _indexInBullet));
        }
    }
}


int BulletSoftObject3D::getClosestVertex(btVector3 pos, btSoftBody::tNodeArray& nodes) const
{
    double closestDist = DBL_MAX;
    int closestVertex = -1;

    for(int i=0; i<nodes.size(); i++)
    {
        btVector3 vertexPosition(nodes.at(i).m_x.getX(), nodes.at(i).m_x.getY(), nodes.at(i).m_x.getZ());
        double dist = (double)(vertexPosition.distance(pos));
        if (dist < closestDist)
        {
            closestDist = dist;
            closestVertex = i;
        }
    }
    return closestVertex;
}



void BulletSoftObject3D::osgToBulletIndices()
{
    Vec3Array *objvertices = getVertexList(0);
    btSoftBody::tNodeArray& nodes = dynamic_cast<btSoftBody*>(_bulletBody)->m_nodes;
    if(objvertices)
    {
        float *array = (float*)objvertices->getDataPointer();
        unsigned int size = objvertices->size();
        if(_indexInBullet) delete [] _indexInBullet;
        if(_indexInOsg) delete [] _indexInOsg;
        _indexInBullet = new unsigned int[size];
        for(unsigned int i=0; i<size; i++)
        {
            Vec3d ptOsg(array[0], array[1], array[2]);
            array++;
            array++;
            array++;
            btVector3 ptBullet(asBtVector3(ptOsg));
            _indexInBullet[i] = getClosestVertex(ptBullet, nodes);
        }

        _indexInOsg = new unsigned int[size];
        for(unsigned int i=0; i<size; i++)
        {
            _indexInOsg[_indexInBullet[i]] = i;
        }

    }
    else
        qCritical() << getNameAsQString() << ": Unable to get vertex array";

}


void BulletSoftObject3D::resetSimulation()
{
    resetAllManipulations();
    btSoftBody *softBody = dynamic_cast<btSoftBody*>(_bulletBody);
    if(softBody) // btSoftBody
    {
        osg::Drawable *draw = getDrawable(0);
        draw->setUpdateCallback(NULL);
        Vec3Array *verts = getVertexList(0);
        float *array = _originalVertices;
        btSoftBody::tNodeArray& nodes = softBody->m_nodes;
        for(unsigned int i=0; i<verts->size(); i++)
        {
            unsigned int ind = _indexInBullet[i];
            Vec3d ptOsg(array[0], array[1], array[2]);
            array++;
            array++;
            array++;
            btVector3 ptBullet(asBtVector3(ptOsg));
            nodes[ind].m_x = ptBullet;
        }
        softBody->scale(_scale);
        softBody->transform(_transform);
        softBody->setVelocity(_linearVelocity);
        softBody->resetLinkRestLengths();
        draw->setUpdateCallback(new MeshUpdater(softBody, _indexInBullet));
    }

}


void BulletSoftObject3D::setStaticVertices(std::list<int> &vertList)
{
    std::list<int>::iterator it;
    btSoftBody *softBody = dynamic_cast<btSoftBody*>(_bulletBody);
    if(softBody)
        for(it = vertList.begin(); it!=vertList.end(); ++it)
        {
            softBody->setMass((*it), 0.0f);
        }
}


void BulletSoftObject3D::scale(double s)
{
    scale(s, s, s);
}


void BulletSoftObject3D::scale(double sx, double sy, double sz)
{
    _scale = btVector3(sx, sy, sz);
    btSoftBody *softBody = dynamic_cast<btSoftBody*>(_bulletBody);
    if(softBody)
    {
        softBody->scale(_scale);
    }
}

void BulletSoftObject3D::translate(double tx, double ty, double tz)
{
    _transform.setOrigin(btVector3(tx, ty, tz));
    applyTransform();
}

void BulletSoftObject3D::rotate(double angle, double x, double y, double z)
{
    Quat q(angle, Vec3d(x, y, z));
    rotate(q);
}

void BulletSoftObject3D::rotate(osg::Quat q)
{
    btQuaternion bq(q.x(), q.y(), q.z(), q.w());
    _transform.setRotation(bq);
    applyTransform();
}


void BulletSoftObject3D::applyTransform()
{
    btSoftBody *softBody = dynamic_cast<btSoftBody*>(_bulletBody);
    if(softBody)
    {
        softBody->transform(_transform);
    }
}


osg::Node *BulletSoftObject3D::nodeFromBtSoftBody(btSoftBody *softBody)
{
    Geode *geode = new Geode();
    Geometry* geom = new Geometry();
    geode->addDrawable(geom);

    // set a default color
    geom->setTexCoordArray(0, new osg::Vec2Array);
    Vec4Array *colorArray = new Vec4Array;
    colorArray->push_back(Vec4( 1., 1., 1., 1.));
    geom->setColorArray(colorArray);
    geom->setColorBinding(Geometry::BIND_OVERALL);

    Vec3Array *vertices = new Vec3Array;
    Vec3Array *normals = new Vec3Array;
    std::map<int, Vec3> verticesMap; // map to associate the vertices and their index
    unsigned int index = 0;
    const btSoftBody::tNodeArray& nodes = softBody->m_nodes;

    // copy the vertices
    for(int i=0; i<nodes.size(); i++)
    {
        Vec3 point = Vec3(nodes[i].m_x.x(), nodes[i].m_x.y(), nodes[i].m_x.z());
        vertices->push_back(point);
        verticesMap[index] = point;
        index++;
    }
    geom->setVertexArray(vertices);

    // create the faces
    const btSoftBody::tFaceArray& faces = softBody->m_faces;
    for(int i=0; i<faces.size(); i++)
    {
        DrawElementsUInt* newFace = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
        unsigned int vertexIndices[3];
        for(unsigned int j=0; j<3; j++)
        {
            btSoftBody::Node *node = faces.at(i).m_n[j];
            Vec3Array::iterator it;
            unsigned int position = 0;
            for(it=vertices->begin(); it!=vertices->end(); ++it)
            {
                if((*it).x() == node->m_x.x() && (*it).y() == node->m_x.y() && (*it).z() == node->m_x.z())
                    break;
                else position++;
            }

            vertexIndices[j] = position;
            newFace->push_back(position);
        }
        geom->addPrimitiveSet(newFace);

        // Compute the normals
        Vec3 associatedNormal = Util::calculateNormal(verticesMap[vertexIndices[0]], verticesMap[vertexIndices[1]], verticesMap[vertexIndices[2]]);
        normals->push_back(associatedNormal);
    }

    geom->setNormalArray(normals, Array::BIND_PER_PRIMITIVE_SET);

    // Set up for dynamic data buffer objects
    geom->setDataVariance(Object::DYNAMIC);
    geom->setUseDisplayList(false);
    geom->setUseVertexBufferObjects(true);
    geom->getOrCreateVertexBufferObject()->setUsage(GL_DYNAMIC_DRAW);

    return geode;
}

BulletSoftObject3D *BulletSoftObject3D::fromBtSoftBody(btSoftBody *softBody, bool generateTexCoord)
{
    osg::Node* node = nodeFromBtSoftBody(softBody);
    BulletSoftObject3D* object = new BulletSoftObject3D(node, softBody);

    // texture coordinates
    if(generateTexCoord)
    {
        object->generateTextureCoordinates();
    }
    return object;
}


