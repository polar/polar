#include "SofaObject3D.h"


using namespace PoLAR;

struct SofaObject3D::MeshUpdater: public osg::Drawable::UpdateCallback
{
    MeshUpdater(SofaObject3D *obj)
        : _obj(obj)
    {}

    virtual ~MeshUpdater(){}

    virtual void update(osg::NodeVisitor*, osg::Drawable* draw)
    {
      /*  if(_obj->_requestMeshUpdate)
        {
            osg::Geometry* geom(draw->asGeometry());
            osg::Vec3Array* verts( dynamic_cast<osg::Vec3Array*>(geom->getVertexArray()));
            int numTetraVertices = 3 * _obj->_voluMesh->getNumVertices();
            double *u = (double*) calloc (numTetraVertices, sizeof(double));
            memcpy(u, _obj->_integrator->Getq(), sizeof(double) * numTetraVertices);

            float *array = (float*)verts->getDataPointer();
            for (unsigned int ptSimu=0; ptSimu<verts->size(); ptSimu++)
            {
                double posx = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][0]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+0]);
                double posy = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][1]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+1]);
                double posz = (_obj->_voluMesh->getVertex(_obj->_indexInSoftBody[ptSimu])[0][2]+1.5*u[3*_obj->_indexInSoftBody[ptSimu]+2]);

                array[0] = posx;
                array[1] = posy;
                array[2] = posz;
                array += 3;
            }
            verts->dirty();
            //draw->dirtyBound();
            _obj->dirty();
            free(u);

            _obj->_requestMeshUpdate = false;
        }*/
    }

    SofaObject3D* _obj;
};


SofaObject3D::SofaObject3D(Node *node,  bool material, bool editable, bool pickable):
    PhysicsObject3D(node, material, editable, pickable),
    _indexInSoftBody(NULL), _indexInOsg(NULL),
    _verticesArray(NULL), _requestMeshUpdate(false)
{
    setFrameMatrix(osg::Matrixd::identity());

   

    osgToSofaIndices();

    osg::Vec3Array *objvertices = findVertexList();
    if(!_originalVertices)
    {
        float *array = (float*)objvertices->getDataPointer();
        _originalVertices = new float[3*objvertices->size()];
        memcpy(_originalVertices, array, 3*objvertices->size()*sizeof(float));
    }

    osg::Drawable *draw = findDrawable(_originalNode.get());
    draw->setUpdateCallback(new MeshUpdater(this));


}


SofaObject3D::~SofaObject3D()
{
    if(_indexInSoftBody) delete[] _indexInSoftBody;
    if(_indexInOsg) delete[] _indexInOsg;
}




void SofaObject3D::resetSimulation()
{
    //resetAllManipulations();
    osg::Drawable *draw = findDrawable(_originalNode.get());
    draw->setUpdateCallback(NULL);
    float *verts = (float*)_verticesArray->getDataPointer();
    memcpy(verts, _originalVertices, 3*_verticesArray->size()*sizeof(float));

    _verticesArray->dirty();
    this->dirty();


    draw->setUpdateCallback(new MeshUpdater(this));
}


void SofaObject3D::osgToSofaIndices()
{
    osg::Vec3Array *objvertices = findVertexList();
    if(objvertices)
    {
        float *array = (float*)objvertices->getDataPointer();
        unsigned int size = objvertices->size();
        _indexInSoftBody = new unsigned int[size];
        for(unsigned int i=0; i<size; i++)
        {
            //Vec3d ptFound = Vec3d(array[0], array[1], array[2]);
            array++;
            array++;
            array++;
            //_indexInSoftBody[i] = _voluMesh->getClosestVertex(ptFound);
        }

        _indexInOsg = new unsigned int[size];
        for(unsigned int i=0; i<size; i++)
        {
            _indexInOsg[_indexInSoftBody[i]] = i;
        }

    }
    else
        std::cerr << "Unable to get vertex array" << std::endl;

    _verticesArray = objvertices;
}

