#include "SimulationWorker.h"
#include <iostream>
#include <sstream>

using namespace PoLAR;


SimulationWorker::SimulationWorker():
    _calculateFramerate(false),
    _currentFramerate(0),
    _timerId(0)
{
    _mutex.lock();
    _timestep = 33.0;
    _isWorking = false;
    _launchOnStart = true;
    _mutex.unlock();
}


SimulationWorker::~SimulationWorker()
{
}

void SimulationWorker::play()
{
    _mutex.lock();
    _isWorking = true;
    _mutex.unlock();
}

void SimulationWorker::pause()
{
    _mutex.lock();
    _isWorking = false;
    _mutex.unlock();
}


void SimulationWorker::stop()
{
    _isWorking = false;
    emit stopTimer();
}

void SimulationWorker::init()
{
    QObject::connect(this, SIGNAL(stopTimer()), this, SLOT(timerStopSlot()));
    _mutex.lock();
    this->initialize(); // reimplemented in each subclass
    if(_launchOnStart)
    {
        _isWorking = true;
    }
    _mutex.unlock();
    _timerId = this->startTimer(_timestep);
}


void SimulationWorker::timerEvent(QTimerEvent *)
{
    if(_isWorking)
    {
        if(_calculateFramerate)
        {
            calculateFramerate();
        }
        this->run(); // reimplemented in each subclass
    }
}


void SimulationWorker::setTimeStep(double timestep)
{
    if(timestep == _timestep) return;
    _mutex.lock();
    _timestep = timestep;
    _mutex.unlock();

    if(_timerId!=0)
    {
        this->killTimer(_timerId);
        _timerId = this->startTimer(_timestep);
    }
}

void SimulationWorker::timerStopSlot()
{
    _mutex.lock();
    _isWorking = false;
    if(_timerId!=0)
    {
        this->killTimer(_timerId);
        _timerId = 0;
    }
    _mutex.unlock();
    emit finished();
}


void SimulationWorker::calculateFramerate()
{
    _currentFramerate = 1.0/_lastFrameStartTime.time_s();
    emit updateFramerate(_currentFramerate);
    _lastFrameStartTime.setStartTick();
}
