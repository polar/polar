/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "GeometryPickHandler.h"
#include "Util.h"
#include "EventUtils.h"
#include <QDebug>

using namespace std;
using namespace osg;
using namespace osgManipulator;
using namespace PoLAR;



GeometryPickHandler::GeometryPickHandler(bool displayInfo):
    EventHandlerInterface(displayInfo),
    _activeDragger(NULL),
    _manipulationGroup(NULL),
    _interactMode(VIEW),
    _currentObservedObject((ref_ptr<Object3D>)NULL),
    _origMode(PolygonMode::FILL),
    _editVertex(false),
    _shiftPressed(false),
    _saveFile(NULL)
{
    _geometryEditionModeKey = '2';
    _cameraManipulationModeKey = '0';
    _saveManipulatedVertexInfoKey = 'w';
    _manipulateVertexModifier = osgGA::GUIEventAdapter::KEY_Control_L;
    _usingTranslate1DVertexDraggerMouseButton = osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON;
    _usingTranslate2DVertexDraggerMouseButton = osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON;
    _vertexDeletionMouseButton = osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON;
}


GeometryPickHandler::~GeometryPickHandler()
{
    if (_saveFile) delete []_saveFile;
    cleanUp();
}


bool GeometryPickHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    bool foundVertex;

    BaseViewer *baseViewer = dynamic_cast<BaseViewer*>(&aa);
    if (!baseViewer) return false;

    Viewer2D3D *viewer2D3D = dynamic_cast<Viewer2D3D*>(baseViewer->getGraphicsViewer());
    if (!viewer2D3D) return false;

    addHUD(viewer2D3D->width(), viewer2D3D->height());

    osgViewer::View* viewer = dynamic_cast<osgViewer::View*>(&aa);
    if (!viewer) return false;

    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) && (ea.getKey() == _geometryEditionModeKey)) // Geometry edition mode
    {
        osgViewer::View::EventHandlers eventHandlers = baseViewer->getEventHandlers();
        osgViewer::View::EventHandlers::iterator itrL;
        for (itrL=eventHandlers.begin(); itrL != eventHandlers.end(); ++itrL)
        {
            Object3DPickHandler* sgPick = dynamic_cast<Object3DPickHandler*>((*itrL).get());
            if (sgPick) {sgPick->setInteractMode(Object3DPickHandler::VIEW); sgPick->cleanUp(viewer2D3D);}
        }
        _interactMode = PICK;
    }
    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) && (ea.getKey() == _cameraManipulationModeKey)) // camera manipulation mode
    {
        _interactMode = VIEW;
        cleanUp();
    }

    if (_interactMode == VIEW) return false;

    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) && (ea.getKey() == _saveManipulatedVertexInfoKey)) // save manipulated vertex
    {
        if (_activeDragger && (!_activeDragger->getObjectEdited()->getName().empty()) && _vertexManipulated.valid())
        {
            if (_saveFile)
            {
                std::ofstream out(_saveFile, std::ios::out | std::ios::app);
                if (out.good()) out.precision(7), out << fixed <<_vertexManipulated<<std::endl;
                else qCritical() << "Saving failure : "<<_saveFile<<" opening failure";
                out.close();
            }
            else qCritical() << "Saving failure : no save file set!";
        }
    }

    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) && (ea.getKey() == _manipulateVertexModifier))
    {
        if (_activeDragger)
        {
            if ((!_activeDragger->getObjectEdited()->getName().empty()) && _vertexManipulated.valid())
            {
                std::ostringstream os;
                os<<"Point ("<< _vertexManipulated<<") in the native geometry of "<<_activeDragger->getObjectEdited()->getName()<<" is currently edited"<<std::endl;
                setLabel(os.str());
            }
            _editVertex = true;
        }
    }
    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYUP) && (ea.getKey() == _manipulateVertexModifier))
    {
        _editVertex = false;
    }

    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) && (ea.getKey() == osgGA::GUIEventAdapter::KEY_Shift_L)) _shiftPressed = true;
    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYUP) && (ea.getKey() == osgGA::GUIEventAdapter::KEY_Shift_L)) _shiftPressed = false;

    switch (ea.getEventType())
    {
    case osgGA::GUIEventAdapter::PUSH:
    {
        osgUtil::LineSegmentIntersector::Intersections intersections;

        _pointer.reset();

        if (computeIntersections(viewer, ea.getX(),ea.getY(),intersections,0x00000001))
        {
            _pointer.setCamera(viewer->getCamera());
            _pointer.setMousePosition(ea.getX(), ea.getY());
            foundVertex = false;
            Vec3f intersectPoint;
            std::pair<unsigned int, Vec3f> hitVertex;
            ref_ptr<Object3D> hitObject3D;

            for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();  hitr != intersections.end(); ++hitr)
            {
                NodePath nodePath = (NodePath)hitr->nodePath;

                if (!_editVertex)
                {
                    for(NodePath::iterator itrNodePath = nodePath.begin(); itrNodePath != nodePath.end(); ++itrNodePath)
                    {
                        if (!foundVertex)
                        {
                            hitObject3D = dynamic_cast<Object3D*>(*itrNodePath);
                            if (hitObject3D != NULL /*&& !hitObject3D->isAPhantom()*/)
                            {
                                std::ostringstream os;
                                if (hitObject3D->isPickable())
                                {
                                    viewer2D3D->setCurrentPickedObject(hitObject3D.get());
                                    if (hitObject3D->isEditable())
                                    {

                                        intersectPoint = hitr->getWorldIntersectPoint();
                                        if (hitObject3D->findNearestVertex((Matrix::inverse(hitObject3D->getTransformationMatrix())).preMult(intersectPoint), hitVertex))
                                        {
                                            foundVertex = true;
                                            //second condition is to avoid to edit an object already in edition mode, it is necessary to have a good working of the program
                                            if ((ea.getButtonMask() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON) && ((_manipulationGroup.get() == NULL) || (hitVertex.second != _vertexManipulated)))
                                            {
                                                manipulateVertex(hitObject3D.get(), hitVertex.second, baseViewer->getCameraManipulator()->getInverseMatrix());
                                                _activeDragger = dynamic_cast<VertexDragger*>(_manipulationGroup->getChild(0));
                                            }
                                        }
                                        else os<<"The geometry of "<<hitObject3D->getName()<<" is not available"<<std::endl;
                                    }
                                    else os<<hitObject3D->getName()<<" is not editable"<<std::endl;
                                }
                                else os<<hitObject3D->getName()<<" is not pickable"<<std::endl;
                                setLabel(os.str());
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                /* if (!(hitr->nodePath).empty()) */
                _pointer.addIntersection(hitr->nodePath, hitr->getLocalIntersectPoint());
            }

            bool handled = false;
            if (_activeDragger && _editVertex)
            {
                handled = _activeDragger->handle(_pointer, ea, aa, _shiftPressed);
                if (handled) _activeDragger->getObjectEdited()->refresh();
                if (handled && _activeDragger->stopEdition()) stopManipulation();
                else
                {
                    std::ostringstream os;
                    os<<"New coordinates : ("<< _activeDragger->getVertexEdited()<<") "<<std::endl;
                    setLabel(os.str());
                }
                break;
            }
        }
        break;
    }

    case osgGA::GUIEventAdapter::DRAG:
    case osgGA::GUIEventAdapter::RELEASE:
    {
        if (_activeDragger && _editVertex)
        {
            _pointer._hitIter = _pointer._hitList.begin();
            _pointer.setCamera(viewer->getCamera());
            _pointer.setMousePosition(ea.getX(), ea.getY());

            if (_activeDragger->handle(_pointer, ea, aa)) _activeDragger->getObjectEdited()->refresh();
            std::ostringstream os;
            os<<"New coordinates : ("<< _activeDragger->getVertexEdited()<<") "<<std::endl;
            setLabel(os.str());
        }
        break;
    }
    default:
    {
        if (!_editVertex)
        {
            osgUtil::LineSegmentIntersector::Intersections intersections;
            std::string gdlist="";

            if (computeIntersections(viewer, ea.getX(),ea.getY(),intersections,0x00000001))
            {
                foundVertex = false;
                for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin(); hitr != intersections.end(); ++hitr)
                {
                    std::ostringstream os;
                    ref_ptr<Object3D> hitObject3D;
                    NodePath nodePath= (NodePath) hitr->nodePath;
                    for(NodePath::iterator itrNodePath = nodePath.begin(); itrNodePath != nodePath.end(); ++itrNodePath)
                    {
                        if (!foundVertex)
                        {
                            hitObject3D = dynamic_cast<Object3D*>(*itrNodePath);
                            if (hitObject3D != NULL/* && !hitObject3D->isAPhantom()*/)
                            {
                                os<<"Object \""<<hitObject3D->getName()<<"\"";
                                if (hitObject3D->isPickable()) os<<", pickable";
                                else os<<", non pickable";
                                if (hitObject3D->isEditable()) os<<", editable";
                                else os<<", non editable";
                                os<<std::endl<<"World coords intersect point ("<< hitr->getWorldIntersectPoint()<<")"<<std::endl;

                                if (hitObject3D->isPickable() && hitObject3D->isEditable())
                                {

                                    std::pair<unsigned int, Vec3f> hitVertex;
                                    bool b = hitObject3D->findNearestVertex((Matrix::inverse(hitObject3D->getTransformationMatrix())).preMult(hitr->getWorldIntersectPoint()), hitVertex);
                                    if (b)
                                    {
                                        os<<"Nearest point in the native geometry ("<< hitVertex.second<<")"<<std::endl;
                                        foundVertex = true;
                                        if (_currentObservedObject!=hitObject3D)
                                        {
                                            if (_currentObservedObject.valid())
                                            {
                                                if (_origMode) _currentObservedObject->getOrCreateStateSet()->setAttributeAndModes(new PolygonMode(PolygonMode::FRONT_AND_BACK, _origMode),  StateAttribute::OVERRIDE);
                                                else _currentObservedObject->getOrCreateStateSet()->removeAttribute(StateAttribute::POLYGONMODE);
                                            }

                                            _currentObservedObject = hitObject3D;
                                            StateAttribute *attribute = _currentObservedObject->getOrCreateStateSet()->getAttribute(StateAttribute::POLYGONMODE);
                                            if (attribute) _origMode = (dynamic_cast< PolygonMode* > (_currentObservedObject->getOrCreateStateSet()->getAttribute(StateAttribute::POLYGONMODE)))->getMode(PolygonMode::FRONT_AND_BACK);

                                            _currentObservedObject->getOrCreateStateSet()->setAttributeAndModes(new PolygonMode(PolygonMode::FRONT_AND_BACK, PolygonMode::LINE),  StateAttribute::OVERRIDE);
                                        }
                                    }
                                    else
                                        os<<std::endl<<"The geometry of this object is not available"<<std::endl;
                                }
                            }
                        }
                    }
                    if (gdlist == "")
                        gdlist += os.str();
                }
            }
            if (gdlist == "")
            {
                if (_currentObservedObject.valid())
                {
                    if (_origMode)
                        _currentObservedObject->getOrCreateStateSet()->setAttributeAndModes(new PolygonMode(PolygonMode::FRONT_AND_BACK, _origMode),  StateAttribute::OVERRIDE);
                    else
                        _currentObservedObject->getOrCreateStateSet()->removeAttribute(StateAttribute::POLYGONMODE);
                }
                _currentObservedObject = NULL;
            }
            setLabel(gdlist);
        }
        break;
    }
    }

    if (ea.getEventType() == osgGA::GUIEventAdapter::RELEASE) _pointer.reset();

    return true;
}

void GeometryPickHandler::cleanUp()
{
    stopManipulation();
    EventHandlerInterface::cleanUp();
    if (_currentObservedObject.valid())
    {
        if (_origMode) _currentObservedObject->getOrCreateStateSet()->setAttributeAndModes(new PolygonMode(PolygonMode::FRONT_AND_BACK, _origMode),  StateAttribute::OVERRIDE);
        else _currentObservedObject->getOrCreateStateSet()->removeAttribute(StateAttribute::POLYGONMODE);
    }
    _editVertex = false;
}

ref_ptr<Group> GeometryPickHandler::addDraggerToVertex(Object3D* object3D, Vec3f vertex, const Matrixd viewMatrix)
{
    VertexDragger *dragger = createDragger(object3D, vertex, viewMatrix);

    // set some mouse buttons for interaction
    dragger->setUsingTranslate1DVertexDraggerMouseButton(_usingTranslate1DVertexDraggerMouseButton);
    dragger->setUsingTranslate2DVertexDraggerMouseButton(_usingTranslate2DVertexDraggerMouseButton);
    dragger->setVertexDeletionMouseButton(_vertexDeletionMouseButton);

    ref_ptr<Group> root = new Group;
    root->addChild(dragger);
    //_sceneGraph->getSceneGraph()->addChild(dragger);
    if(_sceneGraph.get()) _sceneGraph->getSceneGraph()->addChild(root.get());

    float scale = object3D->getOriginalNode()->getBound().radius() * 0.25;
    dragger->setMatrix(Matrix::inverse(Matrix::rotate(object3D->getTransformationMatrix().getRotate())) *
                       Matrix::scale(scale, scale, scale) *
                       Matrix::translate(vertex));
    dragger->postMult(object3D->getTransformationMatrix());
    dragger->preMult(Matrix::inverse(Matrix::rotate(viewMatrix.getRotate())));

    return root;
}


void GeometryPickHandler::manipulateVertex(Object3D* object3D, Vec3f vertex, const Matrixd viewMatrix)
{
    emit geometryEditionSignal();
    stopManipulation();
    _manipulationGroup = addDraggerToVertex(object3D, vertex, viewMatrix);
    _vertexManipulated = vertex;
}


void GeometryPickHandler::stopManipulation()
{
    if (_manipulationGroup.get() != NULL)
    {
        // if ((_activeDragger) && (_activeDragger->getObjectEdited()->hasInternalGtsSurface())) _activeDragger->getObjectEdited()->updateGeometry(); // for normal update
        _manipulationGroup->removeChild(_activeDragger);
        if (_sceneGraph.get()) _sceneGraph->getSceneGraph()->removeChild(_manipulationGroup);
        _activeDragger = NULL;
        _manipulationGroup = NULL;
        _vertexManipulated = Vec3f();

    }
}



