/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "RotateSpherePickHandler.h"
using namespace PoLAR;

osgManipulator::Dragger* RotateSpherePickHandler::createDragger() 
{
    osgManipulator::RotateSphereDragger* d = new osgManipulator::RotateSphereDragger();
    d->setupDefaultGeometry();
    d->setColor(osg::Vec4f(0.0,1.0,0.0,0.2));
    d->setPickColor(osg::Vec4f(1.0,1.0,0.0,0.2));
    d->getOrCreateStateSet()->setMode(GL_BLEND,osg::StateAttribute::ON);
    d->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
    return d;
}
