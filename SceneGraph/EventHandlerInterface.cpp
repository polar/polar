/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "EventHandlerInterface.h"
#include "EventUtils.h"

using namespace PoLAR;

EventHandlerInterface::EventHandlerInterface(bool displayInfo):
    _hudText(new osgText::Text),
    _display(displayInfo)
{

}


EventHandlerInterface::~EventHandlerInterface()
{
    cleanUp();
}


void EventHandlerInterface::cleanUp()
{
    if (_display && _sceneGraph.get()) _sceneGraph->getSceneGraph()->removeChild(_hudNode.get());
}


void EventHandlerInterface::addHUD(float w, float h)
{
    if (_display && _sceneGraph.get() && !(_sceneGraph->getSceneGraph()->containsNode(_hudNode.get())))
    {
        _hudNode = createHUD(w, h, _hudText.get());
        //_hudNode = createHUD(500,500);
        _sceneGraph->getSceneGraph()->addChild(_hudNode.get());
    }
}
