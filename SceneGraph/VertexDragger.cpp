/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifdef WIN32
#define _USE_MATH_DEFINES
#endif
#include <cmath>
#include "VertexDragger.h"
#include <osg/PolygonMode>

using namespace PoLAR;
using namespace osg;
using namespace osgManipulator;

bool Translate1DVertexDragger::handle(const PointerInfo &pointer, const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
{
    // Check if the dragger node is in the nodepath.
    //     if (_checkForNodeInNodePath)
    //     {
    //         if (!pointer.contains(this)) return false;
    //     }

    // This "trick" is to make the interaction with dragger more convenient

    Vec3d nearPoint, farPoint;
    pointer.getNearFarPoints(nearPoint, farPoint);

    nearPoint = nearPoint * Matrixd::rotate(-M_PI/2, 1.0, 0.0, 0.0);
    farPoint = farPoint * Matrixd::rotate(-M_PI/2, 1.0, 0.0, 0.0);

    PointerInfo pi(pointer);
    pi.setNearFarPoints(nearPoint, farPoint);

    switch (ea.getEventType())
    {

    // Pick start.
    case (osgGA::GUIEventAdapter::PUSH):
    {
        _vertex = (*_vertexArray)[_vertexIndex];

        // Get the LocalToWorld matrix for this node and set it for the projector.
        NodePath nodePathToRoot;
        computeNodePathToRoot(*this, nodePathToRoot);
        _localToWorld = computeLocalToWorld(nodePathToRoot);
        _worldToLocal = Matrix::inverse(_localToWorld);
        _projector->setLocalToWorld(_localToWorld);

        if (_projector->project(pi, _startProjectedPoint))
        {
            // Generate the motion command.
            ref_ptr<TranslateInLineCommand> cmd = new TranslateInLineCommand(_projector->getLineStart(),
                                                                             _projector->getLineEnd());
            cmd->setStage(MotionCommand::START);
            cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(),_projector->getWorldToLocal());

            // Dispatch command.
            setMaterialColor(_pickColor,*this);

            aa.requestRedraw();
        }
        return true;
    }

        // Pick move.
    case (osgGA::GUIEventAdapter::DRAG):
    {
        Vec3d projectedPoint;
        if (_projector->project(pi, projectedPoint))
        {
            // Generate the motion command.
            ref_ptr<TranslateInLineCommand> cmd = new TranslateInLineCommand(_projector->getLineStart(),
                                                                             _projector->getLineEnd());
            cmd->setStage(MotionCommand::MOVE);
            cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(),_projector->getWorldToLocal());

            translate(cmd.get(), projectedPoint);

            Vec3d trans = (_localToWorld * cmd->getWorldToLocal()  * cmd->getMotionMatrix() * cmd->getLocalToWorld() * _worldToLocal *  _startMotionMatrix).getTrans();

            Vec3d newVertex = _vertex;
            newVertex.x()+=trans.x();
            newVertex.y()+=trans.y();
            newVertex.z()+=trans.z();
            (*_vertexArray)[_vertexIndex] = newVertex;

            aa.requestRedraw();
        }
        return true;
    }

        // Pick finish.
    case (osgGA::GUIEventAdapter::RELEASE):
    {
        Vec3d projectedPoint;
        if (_projector->project(pi, projectedPoint))
        {
            ref_ptr<TranslateInLineCommand> cmd = new TranslateInLineCommand(_projector->getLineStart(),
                                                                             _projector->getLineEnd());

            cmd->setStage(MotionCommand::FINISH);
            cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(),_projector->getWorldToLocal());

            // Reset color.
            setMaterialColor(_color,*this);

            aa.requestRedraw();
        }

        return true;
    }
    default:
        return false;
    }
    return false;
}


bool Translate2DVertexDragger::handle(const PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    // Check if the dragger node is in the nodepath.
    //     if (!pointer.contains(this)) return false;

    switch (ea.getEventType())
    {
    // Pick start.
    case (osgGA::GUIEventAdapter::PUSH):
    {
        _vertex = (*_vertexArray)[_vertexIndex];

        // Get the LocalToWorld matrix for this node and set it for the projector.
        NodePath nodePathToRoot;
        computeNodePathToRoot(*this, nodePathToRoot);
        _localToWorld = computeLocalToWorld(nodePathToRoot);
        _worldToLocal = Matrix::inverse(_localToWorld);
        _projector->setLocalToWorld(_localToWorld);

        if (_projector->project(pointer, _startProjectedPoint))
        {
            // Generate the motion command.
            ref_ptr<TranslateInPlaneCommand> cmd = new TranslateInPlaneCommand(_projector->getPlane());

            cmd->setStage(MotionCommand::START);
            cmd->setReferencePoint(_startProjectedPoint);
            cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(),_projector->getWorldToLocal());

            // Set color to pick color.
            setMaterialColor(_pickColor,*this);
            getOrCreateStateSet()->setAttributeAndModes(_polygonOffset.get(), StateAttribute::ON);

            aa.requestRedraw();
        }
        return true;
    }

        // Pick move.
    case (osgGA::GUIEventAdapter::DRAG):
    {
        Vec3d projectedPoint;
        if (_projector->project(pointer, projectedPoint))
        {
            // Generate the motion command.
            ref_ptr<TranslateInPlaneCommand> cmd = new TranslateInPlaneCommand(_projector->getPlane());

            cmd->setStage(MotionCommand::MOVE);
            cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(),_projector->getWorldToLocal());
            cmd->setTranslation(projectedPoint - _startProjectedPoint);
            cmd->setReferencePoint(_startProjectedPoint);

            Vec3d trans = (_localToWorld * cmd->getWorldToLocal()  * cmd->getMotionMatrix() * cmd->getLocalToWorld() * _worldToLocal *  _startMotionMatrix).getTrans();

            Vec3d newVertex = _vertex;
            newVertex.x()+=trans.x();
            newVertex.y()+=trans.y();
            newVertex.z()+=trans.z();
            (*_vertexArray)[_vertexIndex] = newVertex;

            aa.requestRedraw();
        }
        return true;
    }

        // Pick finish.
    case (osgGA::GUIEventAdapter::RELEASE):
    {
        ref_ptr<TranslateInPlaneCommand> cmd = new TranslateInPlaneCommand(_projector->getPlane());

        cmd->setStage(MotionCommand::FINISH);
        cmd->setReferencePoint(_startProjectedPoint);
        cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(),_projector->getWorldToLocal());

        // Reset color.
        setMaterialColor(_color,*this);
        getOrCreateStateSet()->removeAttribute(_polygonOffset.get());

        aa.requestRedraw();

        return true;
    }
    default:
        return false;
    }
    return false;
}


VertexDragger::VertexDragger(Object3D *object3D, Vec3f& vertex):
    _usingTranslate1DVertexDragger(false), _usingTranslate2DVertexDragger(false), _object3D(object3D), _vertex(vertex),
    _displayListState(false), _stopEdition(false)
{
    _usingTranslate1DVertexDraggerMouseButton = osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON;
    _usingTranslate2DVertexDraggerMouseButton = osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON;
    _vertexDeletionMouseButton = osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON;
}


void VertexDragger::setupDraggers(const Matrixd &viewMatrix)
{
    _object3D->setDataVariance(Object::DYNAMIC);
    osg::Vec3Array* vertexArray = NULL;
    unsigned int nbDraw = _object3D->getNumDrawables();

    std::vector<Vec3>::iterator itr;
    unsigned int idx;
    unsigned int drawIndex = 0;
    bool vertexFound = false;
    while(drawIndex != nbDraw && !vertexFound)
    {
        vertexArray = _object3D->getVertexList(drawIndex);
        for (itr=vertexArray->begin(), idx=0; itr != vertexArray->end(); ++itr, idx++)
        {
            if (*itr == _vertex)
            {
                _vertexIndex = std::make_pair(drawIndex, idx);
                vertexFound = true;
                break;
            }
        }
        drawIndex++;
    }

    float scale = _object3D->getOriginalNode()->getBound().radius() * 0.25;

    _translate2DVertexDragger = new Translate2DVertexDragger(vertexArray, _vertexIndex.second, Matrix::inverse(Matrix::rotate(viewMatrix.getRotate()))
                                                             *  Matrix::inverse(Matrix::rotate(_object3D->getTransformationMatrix().getRotate()))
                                                             * Matrixd::scale(scale, scale, scale) , Plane(0.0,0.0,1.0,0.0));
    addChild(_translate2DVertexDragger.get());
    addDragger(_translate2DVertexDragger.get());

    _translate1DVertexDragger = create1DVertexDragger(scale, viewMatrix); // virtual method. Differences occur between Default and Epipolar pick handlers
    _translate1DVertexDragger->setCheckForNodeInNodePath(false);
    addChild(_translate1DVertexDragger.get());
    addDragger(_translate1DVertexDragger.get());

    _displayListState = _object3D->getDrawable(_vertexIndex.first)->getUseDisplayList();
    if(_displayListState) _object3D->getDrawable(_vertexIndex.first)->setUseDisplayList(false);
}


VertexDragger::~VertexDragger()
{
    if(_object3D)
    {
        Drawable *draw = _object3D->getDrawable(_vertexIndex.first);
        if(draw)
        {
            draw->dirtyDisplayList();
            draw->setUseDisplayList(_displayListState);
            draw->dirtyBound();
        }
        _object3D->setDataVariance(Object::STATIC);
    }
}


Vec3f VertexDragger::getVertexEdited()
{
    if (!_stopEdition)
    {
        Vec3Array *vertexArray = _object3D->getVertexList(_vertexIndex.first);
        return vertexArray->at(_vertexIndex.second);
    }
    else return Vec3f();
}


void VertexDragger::setupDefaultGeometry()
{
    // Create translate 2D geometry.
    {
        Geode* geode = new Geode;
        Geometry* geometry = new Geometry();

        Vec3Array* vertices = new Vec3Array(4);
        (*vertices)[0] = Vec3(-0.25,0.25,0.0);
        (*vertices)[1] = Vec3(-0.25,-0.25,0.0);
        (*vertices)[2] = Vec3(0.25,-0.25,0.0);
        (*vertices)[3] = Vec3(0.25,0.25,0.0);

        geometry->setVertexArray(vertices);
        geometry->addPrimitiveSet(new DrawArrays(PrimitiveSet::QUADS,0,vertices->size()));

        Vec3Array* normals = new Vec3Array;
        normals->push_back(Vec3(0.0,0.0,1.0));
        geometry->setNormalArray(normals);
        geometry->setNormalBinding(Geometry::BIND_OVERALL);

        geode->addDrawable(geometry);

        PolygonMode* polymode = new PolygonMode;
        polymode->setMode(PolygonMode::FRONT_AND_BACK,PolygonMode::LINE);
        geode->getOrCreateStateSet()->setAttributeAndModes(polymode, StateAttribute::OVERRIDE|StateAttribute::ON);

        geode->getOrCreateStateSet()->setMode(GL_LIGHTING,StateAttribute::OFF);

        _translate2DVertexDragger->addChild(geode);
        _translate1DVertexDragger->addChild(geode);
    }
}


bool DefaultTranslate1DVertexDragger::handle(const PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    return Translate1DVertexDragger::handle(pointer, ea, aa);
}


void DefaultTranslate1DVertexDragger::translate(TranslateInLineCommand* cmd, Vec3d &projectedPoint)
{
    cmd->setTranslation(projectedPoint - _startProjectedPoint);
}


DefaultVertexDragger::DefaultVertexDragger(Object3D* object3D, Vec3f& vertex, const Matrixd& viewMatrix):
    VertexDragger(object3D, vertex)
{
    setupDraggers(viewMatrix);
}


Translate1DVertexDragger* DefaultVertexDragger::create1DVertexDragger(float scale, const Matrixd& viewMatrix)
{
    osg::Vec3Array* vertexArray = _object3D->getVertexList(_vertexIndex.first);
    return new DefaultTranslate1DVertexDragger(vertexArray, _vertexIndex.second, Matrix::inverse(Matrix::rotate(viewMatrix.getRotate()))
                                               * Matrix::inverse(Matrix::rotate(_object3D->getTransformationMatrix().getRotate()))
                                               * Matrixd::scale(scale, scale, scale), Vec3(0.0f,0.0f,0.0f),Vec3(0.0f,0.0f,1.0f));
}


bool DefaultVertexDragger::handle(const PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, bool shift)
{
    // Check if the dragger node is in the nodepath, implemented my own method since osg's one only checks the first list (and this results in a non pickable dragger when it is behind the surface...)

    bool dragger = false;
    {
        typedef std::pair<osg::NodePath, osg::Vec3d> NodePathIntersectionPair;
        typedef std::list< NodePathIntersectionPair> IntersectionList;

        IntersectionList::const_iterator Iter;
        for (Iter=pointer._hitList.begin(); Iter!=pointer._hitList.end(); Iter++)
        {
            dragger = dragger || (std::find((*Iter).first.begin(), (*Iter).first.end(), this) != (*Iter).first.end());
        }
    }
    if (!dragger) return false;


    bool handled = false;
    _stopEdition = false;

    if ((ea.getButtonMask() == _usingTranslate1DVertexDraggerMouseButton) &&
            (ea.getEventType() == osgGA::GUIEventAdapter::PUSH) && !shift) // 1D drag
        _usingTranslate1DVertexDragger = true;
    else if ((ea.getButtonMask() == _usingTranslate2DVertexDraggerMouseButton) &&
             (ea.getEventType() == osgGA::GUIEventAdapter::PUSH)) //2D drag
        _usingTranslate2DVertexDragger = true;

    if (_usingTranslate1DVertexDragger)
    {
        if (_translate1DVertexDragger->handle(pointer, ea, aa))
            handled = true;
    }
    else if (_usingTranslate2DVertexDragger)
    {
        if (_translate2DVertexDragger->handle(pointer, ea, aa))
            handled = true;
    }

    if (ea.getEventType() == osgGA::GUIEventAdapter::RELEASE)
    {
        _usingTranslate1DVertexDragger = false;
        _usingTranslate2DVertexDragger = false;
    }

    /*
    if ((ea.getButtonMask() == _vertexDeletionMouseButton) && (ea.getEventType() == osgGA::GUIEventAdapter::PUSH) && s)
    {
        // *************** vertex deletion with and without retriangulation  ********************** /
        // TODO
    }

    if ((ea.getButtonMask() == osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON) && (ea.getEventType() == osgGA::GUIEventAdapter::PUSH) && shift && s)
    {
        // *************** boundary edge addition  ********************** /
        // TODO
    }
    */

    return handled;
}
