/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "VertexSelectionHandler.h"
#include "Util.h"
#include "EventUtils.h"
#include <osgUtil/LineSegmentIntersector>
#include <QDebug>

using namespace std;
using namespace osg;
using namespace osgManipulator;
using namespace PoLAR;



VertexSelectionHandler::VertexSelectionHandler(bool displayInfo):
    EventHandlerInterface(displayInfo), _isClicked(false)
{
    _mouseButton = osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON;
}



bool VertexSelectionHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    BaseViewer *baseViewer = dynamic_cast<BaseViewer*>(&aa);
    if (!baseViewer) return false;

    Viewer2D3D *viewer2D3D = dynamic_cast<Viewer2D3D*>(baseViewer->getGraphicsViewer());
    if (!viewer2D3D) return false;

    addHUD(viewer2D3D->width(), viewer2D3D->height());

    osgViewer::View* viewer = dynamic_cast<osgViewer::View*>(&aa);
    if (!viewer) return false;

    switch (ea.getEventType())
    {
    case osgGA::GUIEventAdapter::PUSH:
    {
        if(ea.getButton() == _mouseButton)
        {
            _isClicked = true;
            _pointer.reset();
            _pointer.setCamera(viewer->getCamera());
            _pointer.setMousePosition(ea.getX(), ea.getY());
            std::string gdlist="";
            std::ostringstream os;
            bool found = findNearestVertex(ea.getX(), ea.getY(), viewer);
            if(found)
            {
                if(_currentObservedObject)
                {
                    os << "Object \"" << _currentObservedObject->getName() << "\"";
                    os << std::endl << "Screen coordinates " << ea.getX() << " " << ea.getY();
                    os << std::endl << "Nearest point in the native geometry "<< _vertexManipulated.second << std::endl;
                    if (gdlist.empty())
                        gdlist += os.str();
                }
            }
            if (gdlist.empty())
            {
                _currentObservedObject = NULL;
            }
            setLabel(gdlist);
            _dragStartX = ea.getX();
            _dragStartY = ea.getY();
            if(found)
            {
                emit push(_dragStartX, _dragStartY, _vertexManipulated.second);
                //_currentObservedObject->dragVertex(_mouseX, _mouseY, _vertexManipulated.second);
                return true;
            }
        }
        break;
    }


    case osgGA::GUIEventAdapter::DRAG:
    {
        if(_isClicked)
        {
            _mouseX = ea.getX();
            _mouseY = ea.getY();
            float deltaX = _mouseX - _dragStartX;
            float deltaY = _mouseY - _dragStartY;

            osg::Vec3d screenCoord(deltaX, deltaY, 0.0);

            // compute screen to world coordinates
            osgViewer::View* viewer = dynamic_cast<osgViewer::View*>((&aa));
            if (!viewer) return false;
            const osg::Camera* camera = viewer->getCamera();
            if(camera && camera->getViewport())
            {
                osg::Matrixd matrix;
                matrix.postMult(camera->getViewMatrix());
                matrix.postMult(camera->getProjectionMatrix());
                matrix.postMult(camera->getViewport()->computeWindowMatrix());
                osg::Matrixd inverse;
                inverse.invert(matrix);
                osg::Vec3d worldCoord = screenCoord * inverse;
                std::cout << "world coord : " << worldCoord << std::endl;
                emit force(worldCoord[0], worldCoord[1], worldCoord[2]);
            }

            //emit drag(deltaX, deltaY);
            return false;
        }
        break;
    }
    case osgGA::GUIEventAdapter::RELEASE:
    {
        if(_isClicked)
        {
            emit release();
            _pointer.reset();
            _isClicked = false;
        }
        break;
    }
    default:
    {

        break;
    }
    }

    return false;
}



bool VertexSelectionHandler::findNearestVertex(float x, float y, osgViewer::View* viewer)
{
    bool foundVertex = false;
    osgUtil::LineSegmentIntersector::Intersections intersections;
    if(computeIntersections(viewer, x, y ,intersections, 0x00000001))
    {
        for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin(); hitr != intersections.end(); ++hitr)
        {
            ref_ptr<Object3D> hitObject3D;
            NodePath nodePath = (NodePath) hitr->nodePath;
            for(NodePath::iterator itrNodePath = nodePath.begin(); itrNodePath != nodePath.end(); ++itrNodePath)
            {
                if (!foundVertex)
                {
                    hitObject3D = dynamic_cast<Object3D*>(*itrNodePath);
                    if (hitObject3D != NULL && hitObject3D->isPickable())
                    {
                        std::pair<unsigned int, Vec3f> hitVertex;
                        bool b = hitObject3D->findNearestVertex((Matrix::inverse(hitObject3D->getTransformationMatrix())).preMult(hitr->getWorldIntersectPoint()), hitVertex);
                        if (b)
                        {
                            foundVertex = true;
                            if (_currentObservedObject != hitObject3D)
                                _currentObservedObject = hitObject3D;
                            _vertexManipulated = hitVertex;
                            _pointer.addIntersection(hitr->nodePath, hitr->getLocalIntersectPoint());
                        }
                    }
                }

            }
        }
    }
    return foundVertex;
}

