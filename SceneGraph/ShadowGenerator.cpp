/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "ShadowGenerator.h"
#include "Light.h"
#include "ShaderManager.h"
#include <osg/Camera>
#include <osg/CullFace>
#include <osg/Texture2D>
#include <osg/PolygonOffset>
#include <osg/RenderInfo>
#include <iostream>

using namespace PoLAR;
using namespace osg;



ShadowGenerator::ShadowGenerator():
    _shadowerLight(NULL), _unit(1), _ambientBias(0.5)
{
}

void ShadowGenerator::setShadowerLight(PoLAR::Light *light)
{
    _shadowerLight = light;
}


void ShadowGenerator::createShadowedScene()
{
    _stateSet = this->getOrCreateStateSet();
    unsigned int m_shadowWidth = 1024;
    unsigned int m_shadowHeight = 1024;

#if !USE_DEPTH
    GLint internalFormat = GL_RGBA8;
    GLenum sourceFormat = GL_RGBA;
    GLbitfield clearMask = GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT;
    Camera::BufferComponent component = Camera::COLOR_BUFFER;
#else
    GLint internalFormat = GL_DEPTH_COMPONENT;
    GLenum sourceFormat = GL_NONE;
    GLbitfield clearMask = GL_DEPTH_BUFFER_BIT;
    osg::Camera::BufferComponent component = Camera::DEPTH_BUFFER;
#endif

    // create shadow texture
    _texture = new Texture2D;
    _texture->setTextureSize(m_shadowWidth, m_shadowHeight);
    _texture->setInternalFormat(internalFormat);
    _texture->setSourceFormat(sourceFormat);
    _texture->setShadowComparison(true);
    _texture->setShadowTextureMode(Texture2D::LUMINANCE);
    _texture->setFilter(Texture2D::MIN_FILTER, Texture2D::LINEAR);
    _texture->setFilter(Texture2D::MAG_FILTER, Texture2D::LINEAR);
    _texture->setBorderColor(Vec4(1, 1, 1, 0));
    _texture->setWrap(Texture2D::WRAP_S, Texture2D::CLAMP_TO_BORDER);
    _texture->setWrap(Texture2D::WRAP_T, Texture2D::CLAMP_TO_BORDER);
    _texture->setResizeNonPowerOfTwoHint(false);

    // create shadow camera
    _shadowsCamera = new Camera;
    _shadowsCamera->setClearMask(clearMask);
    _shadowsCamera->setClearColor(Vec4(1.0f,1.0f,1.0f,1.0f));
    _shadowsCamera->setComputeNearFarMode(Camera::DO_NOT_COMPUTE_NEAR_FAR);
    _shadowsCamera->setViewport(0, 0, m_shadowWidth, m_shadowHeight);
    _shadowsCamera->setRenderOrder(Camera::PRE_RENDER, 0);
    _shadowsCamera->setRenderTargetImplementation(Camera::FRAME_BUFFER_OBJECT);
    _shadowsCamera->attach(component, _texture.get());

    StateSet* camStateSet = _shadowsCamera->getOrCreateStateSet();
    camStateSet->setMode(GL_LIGHTING, StateAttribute::OFF | StateAttribute::OVERRIDE);
#if !USE_DEPTH
    _program = new osg::Program;
    _program->setName("shadow shader");
    ShaderManager::instanciateShadowShaders(_program.get());
    camStateSet->setAttributeAndModes(_program.get(), StateAttribute::ON|StateAttribute::OVERRIDE);
#endif
    float factor = 4;//1.5f
    float units = 8;//4.0f

    ref_ptr<PolygonOffset> polygon_offset = new PolygonOffset;
    polygon_offset->setFactor(factor);
    polygon_offset->setUnits(units);
    camStateSet->setAttribute(polygon_offset.get(), StateAttribute::ON | StateAttribute::OVERRIDE);
    camStateSet->setMode(GL_POLYGON_OFFSET_FILL, StateAttribute::ON | StateAttribute::OVERRIDE);

    ref_ptr<CullFace> cull_face = new CullFace;
    cull_face->setMode(CullFace::FRONT);
    camStateSet->setAttribute(cull_face.get(), StateAttribute::ON | StateAttribute::OVERRIDE);
    camStateSet->setMode(GL_CULL_FACE, StateAttribute::ON | StateAttribute::OVERRIDE);

    this->getParent(0)->addChild(_shadowsCamera.get());
    _shadowsCamera->addChild(this);

    _stateSet->setTextureAttributeAndModes(_unit, _texture.get(), StateAttribute::ON | StateAttribute::OVERRIDE);

    setUniforms();
    computeShadowViewAndProjection();
    // setDrawCallbacks();
}

bool ShadowGenerator::computeShadowViewAndProjection()
{
    if(!_shadowerLight) return false;
    if (!_shadowerLight->isOn())
    {
        this->getOrCreateStateSet()->removeTextureAttribute(_unit, StateAttribute::TEXTURE);
        this->getOrCreateStateSet()->removeTextureAttribute(_unit, StateAttribute::TEXTURE);
        //this->getOrCreateStateSet()->getUniform("shadower")->set(false);
        //this->getOrCreateStateSet()->getUniform("shadower")->set(false);
        return false; // corresponds to the case where the actual shadower(light) was turned off
    }

    if (_shadowerLight->isOn())
    {
        this->getOrCreateStateSet()->setTextureAttributeAndModes(_unit,_texture.get(),StateAttribute::ON | StateAttribute::OVERRIDE);
        this->getOrCreateStateSet()->setTextureAttributeAndModes(_unit,_texture.get(),StateAttribute::ON | StateAttribute::OVERRIDE);
        //this->getOrCreateStateSet()->getUniform("shadower")->set(true);
        //this->getOrCreateStateSet()->getUniform("shadower")->set(true);
    }


    BoundingSphere bs;
    for(unsigned int i=0; i<_shadowsCamera->getNumChildren(); ++i)
    {
        bs.expandBy(_shadowsCamera->getChild(i)->getBound());
    }

    if (!bs.valid())
    {
        // notify(WARN) << "bb invalid"<<_shadowsCamera.get()<<std::endl;
        return false;
    }

    Vec4 positionGet = _shadowerLight->getPosition();
    Vec3 position;
    position[0] = positionGet[0]/positionGet[3], position[1] = positionGet[1]/positionGet[3], position[2] = positionGet[2]/positionGet[3];
    float centerDistance = (position-bs.center()).length();

    float znear = centerDistance-bs.radius();
    float zfar  = centerDistance+bs.radius();
    float zNearRatio = 0.001f;
    if (znear<zfar*zNearRatio) znear = zfar*zNearRatio;

#if 0
    // hack to illustrate the precision problems of excessive gap between near far range.
    znear = 0.00001*zfar;
#endif
    float top   = (bs.radius()/centerDistance)*znear;
    float right = top;

    // if(_shadowerLight->isReferenceFrameVideo())
    _shadowsCamera->setReferenceFrame(Camera::ABSOLUTE_RF);
    // else
    //     _shadowsCamera->setReferenceFrame(Camera::RELATIVE_RF);
    _shadowsCamera->setProjectionMatrixAsFrustum(-right,right,-top,top,znear,zfar);
    _shadowsCamera->setViewMatrixAsLookAt(position, bs.center(), Vec3(0.0f,1.0f,0.0f));

    // compute the matrix which takes a vertex from local coords into tex coords
    Matrix MVPT = _shadowsCamera->getViewMatrix() *
            _shadowsCamera->getProjectionMatrix();

    Matrix bias(0.5f, 0.0f, 0.0f, 0.0f,
                0.0f, 0.5f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.5f, 0.0f,
                0.5f, 0.5f, 0.5f, 1.0f);


    Matrix lightMVP = MVPT*bias;

    if(_stateSet->getUniform("lightMVP"))
    {
        _stateSet->getUniform("lightMVP")->set(lightMVP);
    }
    else
    {
        Uniform* uniform = new Uniform(Uniform::FLOAT_MAT4, "lightMVP");
        uniform->set(lightMVP);
        _stateSet->addUniform(uniform);
    }

    return true;
}



void ShadowGenerator::setShadowsOff()
{
    this->getParent(0)->removeChild(_shadowsCamera.get());
    if(_stateSet->getUniform("withShadows"))
    {
        _stateSet->getUniform("withShadows")->set(false);
    }
    else
    {
        _stateSet->addUniform(new Uniform("withShadows", false));
    }
}


void ShadowGenerator::setTextureUnit(unsigned int i)
{
    _unit = i;
    if(_stateSet.valid() && _texture.valid())
    {
        _stateSet->setTextureAttributeAndModes(_unit, _texture.get(), StateAttribute::ON | StateAttribute::OVERRIDE);
        setUniforms();
        computeShadowViewAndProjection();
    }
}


void ShadowGenerator::setUniforms()
{
    if(_stateSet.valid())
    {
        if(_stateSet->getUniform("shadowTexture"))
        {
            _stateSet->getUniform("shadowTexture")->set(int(_unit));
        }
        else
        {
            _stateSet->addUniform(new Uniform("shadowTexture", int(_unit)));
        }

        if(_stateSet->getUniform("withShadows"))
        {
            _stateSet->getUniform("withShadows")->set(true);
        }
        else
        {
            _stateSet->addUniform(new Uniform("withShadows", true));
        }

        if(_stateSet->getUniform("ambientBias"))
        {
            _stateSet->getUniform("ambientBias")->set(_ambientBias);
        }
        else
        {
            _stateSet->addUniform(new Uniform("ambientBias", _ambientBias));
        }
    }
}


void ShadowGenerator::setDrawCallbacks()
{

    struct PreDrawCallback: public Camera::DrawCallback
    {
        PreDrawCallback(StateSet* stateSet, Program* program):
            _stateSet(stateSet), _program(program)
        {}

        virtual void operator () (RenderInfo& /*renderInfo*/) const
        {
            _stateSet->setAttributeAndModes(_program, StateAttribute::ON|StateAttribute::OVERRIDE);
        }

        StateSet* _stateSet;
        Program* _program;
    };

    struct PostDrawCallback: public Camera::DrawCallback
    {
        PostDrawCallback(StateSet* stateSet, Program* program):
            _stateSet(stateSet), _program(program)
        {}

        virtual void operator () (RenderInfo& /*renderInfo*/) const
        {
            //Camera* cam = renderInfo.getCurrentCamera();
            _stateSet->setAttributeAndModes(_program, StateAttribute::OFF);
        }

        StateSet* _stateSet;
        Program* _program;
    };
    _shadowsCamera->setPreDrawCallback(new PreDrawCallback(_stateSet.get(), _program.get()));
    _shadowsCamera->setPostDrawCallback(new PostDrawCallback(_stateSet.get(), _program.get()));
}
