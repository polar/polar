/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DraggerManipulationHandler.h"
#include "EventUtils.h"
#include <osgViewer/View>
#include <iostream>

typedef osgUtil::LineSegmentIntersector::Intersections::iterator intersectIter;

using namespace PoLAR;
using namespace osg;

DraggerManipulationHandler::DraggerManipulationHandler():
    osgGA::GUIEventHandler(),
    _activeDragger(NULL),
    _camera(NULL)
{
}

bool DraggerManipulationHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
    if (view)
    {
        if(!_camera) _camera = view->getCamera();
        switch (ea.getEventType())
        {

        case osgGA::GUIEventAdapter::PUSH:
        {
            osgUtil::LineSegmentIntersector::Intersections intersections;
            _pointerInfo.reset();
            _activeDragger = NULL;

#if 1 // original computeIntersections from OSG
            if(view->computeIntersections(ea.getX(), ea.getY(), intersections/*, 0x00000001*/))
#else // rewritten computeIntersections
            if (computeIntersections(view, ea.getX(),ea.getY(),intersections,0x00000001))
#endif
            {
                _pointerInfo.setCamera(_camera);
                _pointerInfo.setMousePosition(ea.getX(), ea.getY());

                for (intersectIter iter = intersections.begin();
                     iter != intersections.end();
                     ++iter)
                {
                    _pointerInfo.addIntersection(iter->nodePath, iter->getLocalIntersectPoint());
                }

                for (NodePath::iterator iter = _pointerInfo._hitList.front().first.begin(); iter != _pointerInfo._hitList.front().first.end(); ++iter)
                {

                    if (osgManipulator::Dragger* dragger = dynamic_cast<osgManipulator::Dragger*>(*iter))
                    {
                        dragger->handle(_pointerInfo, ea, aa);
                        _activeDragger = dragger;
                        return true;
                    }
                }
            }
            break;
        }
        case osgGA::GUIEventAdapter::RELEASE:
        case osgGA::GUIEventAdapter::DRAG:
            if (_activeDragger)
            {
                _pointerInfo._hitIter = _pointerInfo._hitList.begin();
                _pointerInfo.setCamera(_camera);
                _pointerInfo.setMousePosition(ea.getX(), ea.getY());
                _activeDragger->handle(_pointerInfo, ea, aa);
                return true;
                break;
            }
            return false;
            break;
        default:
            return false;
            break;
        }
    }
    if (ea.getEventType() == osgGA::GUIEventAdapter::RELEASE)
    {
        _activeDragger = NULL;
        _pointerInfo.reset();
        return false;
    }
    return false;
}

