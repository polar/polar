/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "SceneGraphManipulator.h"
#include <osg/BoundsChecking>
#include <osg/io_utils>

using namespace osg;
using namespace osgGA;
using namespace PoLAR;


class CollectParentPaths : public NodeVisitor
{
public:
    CollectParentPaths() :
        NodeVisitor(NodeVisitor::TRAVERSE_PARENTS) {}

    using NodeVisitor::apply;
    virtual void apply(Node& node)
    {
        if (node.getNumParents()==0)
            _nodePaths.push_back(getNodePath());
        traverse(node);
    }

    NodePath _nodePath;
    typedef std::vector< NodePath > NodePathList;
    NodePathList _nodePaths;
};


SceneGraphManipulator::SceneGraphManipulator():
    _thrown(false),
    _trackerMode(0), // VISION type
    _panRating(1.0),
    _distanceX(0.0f),
    _distanceY(0.0f),
    _distanceZ(1.0f),
    _init(false),
    _currentModifier(0)
{
    initDefaultShortcuts();
    _positionInit = Vec3d(0.0,0.0,-1.0);
    _rotationInit = Matrixd::identity();
}


SceneGraphManipulator::~SceneGraphManipulator()
{
}


NodePath SceneGraphManipulator::getNodePath() const
{
    NodePath nodePath;
    ObserveredNodePath::const_iterator itr = _trackNodePath.begin();
    for(; itr != _trackNodePath.end(); ++itr)
        nodePath.push_back(const_cast<Node*>(itr->get()));
    return nodePath;
}


bool SceneGraphManipulator::validateNodePath() const
{
    ObserveredNodePath::const_iterator itr = _trackNodePath.begin();
    for(; itr != _trackNodePath.begin(); ++itr)
        if (*itr==0)
        {
            notify(NOTICE)<<"Warning: tracked node path has been invalidated by changes in the scene graph."<<std::endl;
            const_cast<ObserveredNodePath&>(_trackNodePath).clear();
            return false;
        }
    return true;
}


void SceneGraphManipulator::setTrackNode(Node* node)
{
    if (!node)
    {
        notify(NOTICE)<<"PoLAR Trackball::setTrackNode(Node*):  Unable to set tracked node due to null Node*"<<std::endl;
        return;
    }
    CollectParentPaths cpp;
    node->accept(cpp);

    if (!cpp._nodePaths.empty())
    {
        notify(INFO)<<"PoLAR Trackball::setTrackNode(Node*"<<node<<" "<<node->getName()<<"): Path set"<<std::endl;
        _trackNodePath.clear();
        setTrackNodePath( cpp._nodePaths[0] );
    }
    else
        notify(NOTICE)<<"PoLAR Trackball::setTrackNode(Node*): Unable to set tracked node due to empty parental path."<<std::endl;

#ifdef PoLAR_DEBUG
    notify(NOTICE)<<"setTrackNode("<<node->getName()<<")"<<std::endl;
    unsigned int i;
    for(i=0; i<_trackNodePath.size(); ++i)
        notify(NOTICE)<<"  "<<_trackNodePath[i]->className()<<" '"<<_trackNodePath[i]->getName()<<"'"<<std::endl;
#endif

}


void SceneGraphManipulator::home(double)
{
    if (getAutoComputeHomePosition()) computeHomePosition();
    computePosition(_homeEye, _homeCenter, _homeUp);
    _thrown = false;
}


void SceneGraphManipulator::home(const GUIEventAdapter& ea ,GUIActionAdapter& us)
{
    home(ea.getTime());
    us.requestRedraw();
}


void SceneGraphManipulator::init(const GUIEventAdapter& ,GUIActionAdapter& )
{
    flushMouseEventStack();
}


void SceneGraphManipulator::getUsage(ApplicationUsage& usage) const
{
    usage.addKeyboardMouseBinding("Trackball: Space","Reset the viewing position to home");
}

bool SceneGraphManipulator::handle(const GUIEventAdapter& ea,GUIActionAdapter& us)
{
    if (ea.getHandled()) return false;

    BaseViewer *v = dynamic_cast<BaseViewer*>(&us);
    if (!v)
        return false;
    else if (!_init) {_sceneGraph = dynamic_cast<SceneGraph*>(getUserData()); _init=true;}

    Viewer2D3D *v2d3d = dynamic_cast<Viewer2D3D*>(v->getGraphicsViewer());
    if (!v2d3d)
        return false;

    if(_sceneGraph.valid() && _sceneGraph->getScene())
        setInteractionMode(v2d3d->getProjectionType(), (_sceneGraph->getScene()->getBound()).center(), v2d3d->getZoom());

    switch(ea.getEventType())
    {
    case(GUIEventAdapter::PUSH):
        flushMouseEventStack();
        addMouseEvent(ea);
        if (calcMovement()) us.requestRedraw();
        us.requestContinuousUpdate(false);
        _thrown = false;
        return true;

    case(GUIEventAdapter::RELEASE):
        if (ea.getButtonMask()==0)
            if (isMouseMoving())
            {
                if (calcMovement())
                {
                    us.requestRedraw();
                    us.requestContinuousUpdate(false);
                    _thrown = false;
                }
            }
            else
            {
                flushMouseEventStack();
                addMouseEvent(ea);
                if (calcMovement()) us.requestRedraw();
                us.requestContinuousUpdate(false);
                _thrown = false;
            }
        else
        {
            flushMouseEventStack();
            addMouseEvent(ea);
            if (calcMovement()) us.requestRedraw();
            us.requestContinuousUpdate(false);
            _thrown = false;
        }
        return true;

    case(GUIEventAdapter::DRAG):
        addMouseEvent(ea);
        if (calcMovement()) us.requestRedraw();
        us.requestContinuousUpdate(false);
        _thrown = false;
        return true;

    case(GUIEventAdapter::MOVE):
        return false;

    case(GUIEventAdapter::KEYDOWN):
        if (ea.getKey()== _resetCameraKey) // reset camera
        {
            flushMouseEventStack();
            _thrown = false;
            home(ea,us);
            us.requestRedraw();
            us.requestContinuousUpdate(false);
            return true;
        }
        else if(ea.getKey() >= 0xFFE1 && ea.getKey() <= 0xFFEE)
        { // modifier keys
            _currentModifier = ea.getKey();
            return true;
        }
        return false;
    case(GUIEventAdapter::KEYUP):
    {
        if(ea.getKey() == _currentModifier)
        {
            _currentModifier = 0;
            return true;
        }
        return false;
    }
    case(GUIEventAdapter::FRAME):
        if (_thrown && calcMovement()) us.requestRedraw();
        return false;
    default:
        return false;
    }
}


bool SceneGraphManipulator::isMouseMoving()
{
    if (_ga_t0.get()==NULL || _ga_t1.get()==NULL) return false;

    static const float velocity = 0.1f;

    float dx = _ga_t0->getXnormalized()-_ga_t1->getXnormalized();
    float dy = _ga_t0->getYnormalized()-_ga_t1->getYnormalized();
    float len = sqrtf(dx*dx+dy*dy);
    float dt = _ga_t0->getTime()-_ga_t1->getTime();

    return (len>dt*velocity);
}


void SceneGraphManipulator::flushMouseEventStack()
{
    _ga_t1 = NULL;
    _ga_t0 = NULL;
}


void SceneGraphManipulator::addMouseEvent(const GUIEventAdapter& ea)
{
    _ga_t1 = _ga_t0;
    _ga_t0 = &ea;
}


void SceneGraphManipulator::setByMatrix(const Matrixd& matrix)
{
    osg::Vec3d eye, center, up;
    matrix.getLookAt(eye, center, up);
    setHomePosition(eye, center, up);
    home(0.0);
}


void SceneGraphManipulator::computeNodeWorldToLocal(Matrixd& worldToLocal) const
{
    if (validateNodePath())
        worldToLocal = computeWorldToLocal(getNodePath());
}


void SceneGraphManipulator::computeNodeLocalToWorld(Matrixd& localToWorld) const
{
    if (validateNodePath())
    {
        localToWorld = computeLocalToWorld(getNodePath());
    }
}


void SceneGraphManipulator::computeNodeCenterAndRotation(Vec3d& nodeCenter, Quat& nodeRotation) const
{
    Matrixd localToWorld;
    computeNodeLocalToWorld(localToWorld);

    if (validateNodePath())
        nodeCenter = Vec3d(_trackNodePath.back()->getBound().center())*localToWorld;
    else
        nodeCenter = Vec3d(0.0f,0.0f,0.0f)*localToWorld;

    // scale the matrix to get rid of any scales before we extract the rotation.
    double sx = 1.0/sqrt(localToWorld(0,0)*localToWorld(0,0) + localToWorld(1,0)*localToWorld(1,0) + localToWorld(2,0)*localToWorld(2,0));
    double sy = 1.0/sqrt(localToWorld(0,1)*localToWorld(0,1) + localToWorld(1,1)*localToWorld(1,1) + localToWorld(2,1)*localToWorld(2,1));
    double sz = 1.0/sqrt(localToWorld(0,2)*localToWorld(0,2) + localToWorld(1,2)*localToWorld(1,2) + localToWorld(2,2)*localToWorld(2,2));
    localToWorld = localToWorld*Matrixd::scale(sx,sy,sz);

    nodeRotation = localToWorld.getRotate();
}


void SceneGraphManipulator::computePosition(const osg::Vec3d& eye,const osg::Vec3d& center,const osg::Vec3d& up)
{

    osg::Matrixd m;
    m.makeLookAt(eye, center, up);
    _rotationInit.makeRotate(m.getRotate());
    _positionInit = m.getTrans();

    _distanceX = 0.0;
    _distanceY = 0.0;
    _distanceZ = 0.0;
    _rotation = Matrixd::identity();
}


Matrixd SceneGraphManipulator::getInverseMatrix() const
{
    return Matrixd::inverse(getMatrix());
}


Matrixd SceneGraphManipulator::getMatrix() const
{
    Vec3d nodeCenter;
    Quat nodeRotation;
    computeNodeCenterAndRotation(nodeCenter, nodeRotation);

    return Matrixd::translate(-_distanceX,-_distanceY,-_distanceZ) * Matrixd::translate(-_positionInit) * Matrixd::translate(-nodeCenter *_rotationInit) * Matrixd::inverse(_rotation) * Matrixd::inverse(_rotationInit) * Matrixd::translate(nodeCenter);
}


void SceneGraphManipulator::setInteractionMode(int mode, Vec3f center, float imageZoom)
{ 
    if (_trackerMode == -1) _trackerMode = 0; // if ProjectionType is UNKNOWN treat it as VISION
    else _trackerMode = mode;
    Matrixd viewMat = getMatrix();
    float C = 1.5;
    _panRating = C * (viewMat.preMult(center)).length() / imageZoom; //(viewMat.preMult(center)).length() is the distance between the eye and the center of the viewed scene
}


bool SceneGraphManipulator::calcMovement()
{
    // return if less than two events have been added.
    if (_ga_t0.get()==NULL || _ga_t1.get()==NULL) return false;

    float dx = _ga_t0->getXnormalized()-_ga_t1->getXnormalized();
    float dy = _ga_t0->getYnormalized()-_ga_t1->getYnormalized();


    // return if there is no movement.
    if (dx==0.0f && dy==0.0f) return false;

    int buttonMask = _ga_t1->getButtonMask();
    int modifier = _currentModifier;
    bool rotateCamModifier;
    bool rotateCamModifier2;
    bool panXYModifier;
    bool panZModifier;
    if(_useModifiers)
    {
        rotateCamModifier = (modifier==_rotateCameraModifier);
        rotateCamModifier2 = (modifier==_rotateCamera2Modifier);
        panXYModifier = (modifier==_panXYModifier);
        panZModifier = (modifier==_panZModifier);
    }
    else
    {
        rotateCamModifier = true;
        rotateCamModifier2 = true;
        panXYModifier = true;
        panZModifier = true;
    }

    if (buttonMask==_rotateCameraMouseButton  && rotateCamModifier)
    {
        // rotate camera.
        Matrixd new_rotate;

        float mag = sqrt((dy)*(dy)+(dx)*(dx));

        if((mag != 0.0)&&(_trackerMode == 0)) new_rotate.makeRotate( 2.0 * asin(mag),(float)-dy, (float)dx, 0.0);  //VISION
        if((mag != 0.0)&&(_trackerMode == 1)) new_rotate.makeRotate( 2.0 * asin(mag),(float)dy, (float)dx, 0.0);   //ANGIO
        if((mag != 0.0)&&(_trackerMode == 2)) new_rotate.makeRotate( 2.0 * asin(mag),(float)dy, (float)-dx, 0.0);  //ANGIOHINV

        _rotation = _rotation * new_rotate;
        return true;
    }
    else if (buttonMask==_rotateCamera2MouseButton  && rotateCamModifier2)
    {
        // rotate camera.
        Matrixd new_rotate;

        float mag = sqrt((dy)*(dy)+(dx)*(dx));

        if(mag != 0.0) new_rotate.makeRotate(2.0*asin(mag), 0.0, 0.0, (float)dy);

        _rotation = _rotation * new_rotate;
        return true;
    }

    else if (buttonMask==_panXYMouseButton && panXYModifier)
    {
        // pan model. (X Y)
        float scale=_panRating;
        Vec3 dv;
        float panAttenuation0 = 0.2, panAttenuation1 = 0.06;

        if (_trackerMode == 0)
        {
            scale *= panAttenuation0;
            dv.set(dx*scale,dy*scale,0.0f);
        }

        if (_trackerMode == 1)
        {
            scale *= panAttenuation1;
            dv.set(-dx*scale,dy*scale,0.0f);
        }

        if (_trackerMode == 2)
        {
            scale *= panAttenuation1;
            dv.set(dx*scale,dy*scale,0.0f);
        }
        _distanceX += (dv)[0];
        _distanceY += (dv)[1];

        return true;

    }
    else if (buttonMask==_panZMouseButton && panZModifier)
    {
        // pan model (Z)
        float scale=_panRating;
        float dv=dy;
        float zoomAttenuation0 = 0.3, zoomAttenuation1 = 0.2;

        if (_trackerMode == 0)
        {
            scale *= zoomAttenuation0;
            dv = dy*scale;
        }

        if ((_trackerMode == 1)||(_trackerMode == 2))
        {
            scale *= zoomAttenuation1;
            dv = dy*scale;
        }

        _distanceZ -= dv;

        return true;
    }
    return false;
}


void SceneGraphManipulator::initDefaultShortcuts()
{
    _currentModifier = 0;
    _useModifiers = true;
    _panXYMouseButton = GUIEventAdapter::MIDDLE_MOUSE_BUTTON;
    _panZMouseButton = GUIEventAdapter::RIGHT_MOUSE_BUTTON;
    _rotateCameraMouseButton = GUIEventAdapter::LEFT_MOUSE_BUTTON;
    _rotateCamera2MouseButton = GUIEventAdapter::LEFT_MOUSE_BUTTON;
    _resetCameraKey = GUIEventAdapter::KEY_Space;
    _panXYModifier = GUIEventAdapter::NONE;
    _panZModifier = GUIEventAdapter::NONE;
    _rotateCameraModifier = GUIEventAdapter::NONE;
    _rotateCamera2Modifier = GUIEventAdapter::KEY_Control_L;
}
