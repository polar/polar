/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "EventUtils.h"
#include <osg/Camera>

using namespace PoLAR;
using namespace osg;

bool PoLAR::computeIntersections(osgViewer::View *viewer, float x, float y, osgUtil::LineSegmentIntersector::Intersections &intersections, osg::Node::NodeMask traversalMask)
{
    float local_x = 0.0, local_y = 0.0;

    const osgGA::GUIEventAdapter* eventState = viewer->getEventQueue()->getCurrentEventState();
    bool view_invert_y = eventState->getMouseYOrientation()==osgGA::GUIEventAdapter::Y_INCREASING_DOWNWARDS;
    double epsilon = 1.0;

    Camera *camera = viewer->getCamera();
    const Viewport* viewport = camera->getViewport();

    double new_x = static_cast<double>(camera->getGraphicsContext()->getTraits()->width) * (x - eventState->getXmin())/(eventState->getXmax()-eventState->getXmin());

    double new_y = view_invert_y ? static_cast<double>(camera->getGraphicsContext()->getTraits()->height) * (1.0 - (y-eventState->getYmin())/(eventState->getYmax()-eventState->getYmin())) :
                                   static_cast<double>(camera->getGraphicsContext()->getTraits()->height) * (y - eventState->getYmin())/(eventState->getYmax()-eventState->getXmin());


    if (viewport &&
            new_x >= (viewport->x()-epsilon) && new_y >= (viewport->y()-epsilon) &&
            new_x < (viewport->x()+viewport->width()-1.0+epsilon) && new_y <= (viewport->y()+viewport->height()-1.0+epsilon) )
    {
        local_x = new_x;
        local_y = new_y;
    }

    // The direction is given by the following vectors for the LineSegmentIntersector
    const Vec3d start(local_x, local_y, 0.0);
    const Vec3d end(local_x, local_y, 1.0);

    osgUtil::LineSegmentIntersector* picker = new osgUtil::LineSegmentIntersector(osgUtil::Intersector::WINDOW,start/*local_x*/, end/*local_y*/);

    osgUtil::IntersectionVisitor iv(picker);
    iv.setTraversalMask(traversalMask);
    const_cast<Camera*>(camera)->accept(iv);

    if (picker->containsIntersections())
    {
        intersections = picker->getIntersections();
        return true;
    }
    else
    {
        intersections.clear();
        return false;
    }

    return false;
}


Node* PoLAR::createHUD(int width, int height, osgText::Text* updateText, int precision)
{
    int dec=precision;

    Camera* hudCamera = new Camera;
    hudCamera->setReferenceFrame(Transform::ABSOLUTE_RF);
    hudCamera->setProjectionMatrixAsOrtho2D(0,width,0,height);
    hudCamera->setViewMatrix(Matrix::identity());
    hudCamera->setRenderOrder(Camera::NESTED_RENDER);
    hudCamera->setClearMask(GL_DEPTH_BUFFER_BIT);
    hudCamera->setName("HudCamera");

    Vec3 position(dec,height-dec,0.0f);

    if(updateText)
    {
        Geode* geode = new Geode();
        StateSet* stateset = geode->getOrCreateStateSet();
        stateset->setMode(GL_LIGHTING,StateAttribute::OFF);
        stateset->setMode(GL_DEPTH_TEST,StateAttribute::OFF);
        geode->setName("HudCamera Text Label");
        geode->addDrawable(updateText);
        hudCamera->addChild(geode);

        updateText->setAlignment(osgText::Text::LEFT_TOP);
        updateText->setCharacterSizeMode(osgText::Text::OBJECT_COORDS);
        updateText->setColor(Vec4(1.0f,1.0f,0.0f,1.0f));
        updateText->setText("");
        updateText->setPosition(position);
        updateText->setDataVariance(Object::DYNAMIC);
    }

    return hudCamera;
}
