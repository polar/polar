/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "EpipolarPickHandler.h"

using namespace std;
using namespace osg;
using namespace osgManipulator;
using namespace PoLAR;


bool EpipolarTranslate1DVertexDragger::handle(const PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    return Translate1DVertexDragger::handle(pointer, ea, aa);
}

void EpipolarTranslate1DVertexDragger::translate(TranslateInLineCommand* cmd, Vec3d &projectedPoint)
{
    // To modulate the generated translation, original computation results in a too big translation
    // 		    cmd->setTranslation((projectedPoint - _startProjectedPoint)
    cmd->setTranslation((projectedPoint - _startProjectedPoint)/((_projector->getLineStart() - _projector->getLineEnd()).length()/250));
}



EpipolarVertexDragger::EpipolarVertexDragger(Object3D* object3D, Vec3f& vertex, const Matrixd& viewMatrix, Vec3d& focalPoint):
    VertexDragger(object3D, vertex), _currentFocalPoint(focalPoint)
{
    setupDraggers(viewMatrix);
}


Translate1DVertexDragger* EpipolarVertexDragger::create1DVertexDragger(float scale, const Matrixd& viewMatrix)
{
    osg::Vec3Array* vertexArray = _object3D->getVertexList(_vertexIndex.first);
    return new EpipolarTranslate1DVertexDragger(vertexArray, _vertexIndex.second, Matrix::inverse(Matrix::rotate(viewMatrix.getRotate()))
                                                * Matrix::inverse(Matrix::rotate(_object3D->getTransformationMatrix().getRotate()))
                                                * Matrixd::scale(scale, scale, scale), viewMatrix.preMult(_currentFocalPoint),viewMatrix.preMult(_vertex)); // creates an epipolar displacement
}


bool EpipolarVertexDragger::handle(const PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, bool shift)
{
    // Check if the dragger node is in the nodepath, implemented my own method since osg's one only checks the first list (and this results in a non pickable dragger when it is behind the surface...)

    bool dragger = false;
    {
        typedef std::pair<osg::NodePath, osg::Vec3d> NodePathIntersectionPair;
        typedef std::list<NodePathIntersectionPair> IntersectionList;

        IntersectionList::const_iterator Iter;
        for (Iter=pointer._hitList.begin(); Iter!=pointer._hitList.end(); Iter++)
        {
            dragger = dragger || (std::find((*Iter).first.begin(), (*Iter).first.end(), this) != (*Iter).first.end());
        }
    }
    if (!dragger) return false;

    bool handled = false;
    _stopEdition = false;

    if ((ea.getButtonMask() == _usingTranslate1DVertexDraggerMouseButton) &&
            (ea.getEventType() == osgGA::GUIEventAdapter::PUSH) && !shift)
        _usingTranslate1DVertexDragger = true;
    else if ((ea.getButtonMask() == _usingTranslate2DVertexDraggerMouseButton) &&
             (ea.getEventType() == osgGA::GUIEventAdapter::PUSH))
        _usingTranslate2DVertexDragger = true;

    if (_usingTranslate1DVertexDragger)
    {
        BaseViewer* baseViewer = dynamic_cast<BaseViewer*>(&aa);
        if(baseViewer)
        {
            // to have a correct translation along epipolar line (even if the vertex was previously edited in the (x,y) plane, indeed in that case the current lineprojector is no more correct)
            (dynamic_cast<EpipolarTranslate1DVertexDragger*>(_translate1DVertexDragger.get()))->updateLineProjector(baseViewer->getCameraManipulator()->getInverseMatrix().preMult(_currentFocalPoint),
                                                                                                                    baseViewer->getCameraManipulator()->getInverseMatrix().preMult(getVertexEdited()));
        }

        if (_translate1DVertexDragger->handle(pointer, ea, aa))
            handled = true;
    }
    else if (_usingTranslate2DVertexDragger)
    {
        if (_translate2DVertexDragger->handle(pointer, ea, aa))
            handled = true;
    }

    if (ea.getEventType() == osgGA::GUIEventAdapter::RELEASE)
    {
        _usingTranslate1DVertexDragger = false;
        _usingTranslate2DVertexDragger = false;
    }

    /*
    if ((ea.getButtonMask() == _vertexDeletionMouseButton) && (ea.getEventType() == osgGA::GUIEventAdapter::PUSH) && s)
    {
        // *************** vertex deletion with and without retriangulation  ********************** /
        // TODO
    }

    if ((ea.getButtonMask() == osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON) && (ea.getEventType() == osgGA::GUIEventAdapter::PUSH) && shift && s)
    {
        // *************** boundary edge addition  ********************** /
        // TODO
    }
    */

    return handled;
}



bool EpipolarPickHandler::handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
{
    if(!_initialized)
    {
        osgViewer::View* viewer = dynamic_cast<osgViewer::View*>((&aa));
        if(viewer)
        {
            osgGA::CameraManipulator* manip = viewer->getCameraManipulator();
            if(manip)
            {
                _focalPointsArray->at(0) = manip->getMatrix().getTrans();
                setFocalPoint(0);
            }
            _initialized = true;
        }
    }
    return GeometryPickHandler::handle(ea, aa);
}



VertexDragger* EpipolarPickHandler::createDragger(Object3D* object3D, Vec3f& vertex, const Matrixd& viewMatrix)
{
    VertexDragger* d = new EpipolarVertexDragger(object3D, vertex, viewMatrix, _currentFocalPoint);
    d->setupDefaultGeometry();
    return d;
}

int EpipolarPickHandler::addFocalPoint(Vec3d center)
{
    _focalPointsArray->push_back(center);
    return (int)(_focalPointsArray->size()-1);
}

bool EpipolarPickHandler::getFocalPoint(int index, Vec3d &focalPoint)
{
    if (index < (int)_focalPointsArray->size())
    {
        focalPoint = (*_focalPointsArray)[index];
        return true;
    }
    else return false;
}

void EpipolarPickHandler::setFocalPoint(int index) 
{
    getFocalPoint(index, _currentFocalPoint);
}
