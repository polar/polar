/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <algorithm>
#include <osgShadow/SoftShadowMap>
#include <osg/io_utils>
#include <QDebug>
#include "SceneGraph.h"
#include "ShaderManager.h"
#include "Object3D.h"
#include "ShadowGenerator.h"
#include "config.h"


#define MAX_LIGHTS 8 // maximum number of lights managed by OpenGL

using namespace PoLAR;
using namespace osgShadow;
using osg::ref_ptr;
using osg::Group;
using osg::Node;
using osg::BlendColor;
using osg::BlendFunc;
using osg::StateAttribute;
using osg::StateSet;
using osg::LightSource;
using std::list;



SceneGraph::SceneGraph(bool withShadows):
    _numLights(0), _withShadows(withShadows), _shadowerLight(NULL)
{
    _root = new Group;
    _scene = new Group;
    _root->addChild(_scene.get());
    _root->setName("root");
    _scene->setName("scene");

#ifdef USE_SHADOWS
#ifdef USE_OSG_SHADOWS
    _shadowedScene  = new ShadowedScene;
    _shadowedScene->setName("shadowedScene");
    _shadowedScene->setReceivesShadowTraversalMask(SHADOW_RECEIVER_MASK);
    _shadowedScene->setCastsShadowTraversalMask(SHADOW_CASTER_MASK);
    _scene->addChild(_shadowedScene.get());

    if(withShadows)
    {
        createShadowTechnique();
        _shadowedScene->setShadowTechnique(_shadowTechnique.get());
    }
    else
    {
        _shadowedScene->setShadowTechnique(0);
    }
#else
    ShadowGenerator* shadowGen = new ShadowGenerator();
    _shadowedScene = shadowGen;
    _scene->addChild(_shadowedScene.get());
    shadowGen->setName("shadowedScene");
    shadowGen->setTextureUnit(1);
    shadowGen->setAmbientBias(0.5);
    if(withShadows)
    {
        shadowGen->createShadowedScene();
    }
#endif
#endif

    _shadowedObjectsGroup = new Group;
    _shadowedObjectsGroup->setName("shadowedObjectsNode");
#ifdef USE_PHONG_SHADING
    // add defaut Phong shading to the shadowed objects
    ShaderManager::addDefaultShaders(_shadowedObjectsGroup.get());
#endif

    _lightGroup = new Group;
    _lightGroup->setName("lightGroup");

#ifdef USE_SHADOWS
    _shadowedScene->addChild(_shadowedObjectsGroup.get());
    _shadowedScene->addChild(createPhantomGroup());
    _shadowedScene->addChild(_lightGroup.get());
#else
    _scene->addChild(_shadowedObjectsGroup.get());
    _scene->addChild(createPhantomGroup());
    _scene->addChild(_lightGroup.get());
#endif

    _graphStateSet = new StateSet;
    ref_ptr<BlendColor> bc = new BlendColor(osg::Vec4f(1.0,1.0,1.0,1.0));
    _graphStateSet->setAttribute(bc.get());
    ref_ptr<BlendFunc> bf = new BlendFunc(GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
    _graphStateSet->setAttribute(bf.get());
    _scene->setStateSet(_graphStateSet.get());
}

SceneGraph::~SceneGraph()
{
    _lights.clear();
    _root->removeChildren(0,2);
}


void SceneGraph::setShadowsOn()
{
    _withShadows = true;
#ifdef USE_OSG_SHADOWS
    if(!_shadowTechnique.valid())
        createShadowTechnique();
    _shadowedScene->setShadowTechnique(_shadowTechnique.get());
#else
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen)
        shadowGen->createShadowedScene();
#endif
}

void SceneGraph::setShadowsOff()
{
    _withShadows = false;
#ifdef USE_OSG_SHADOWS
    _shadowedScene->setShadowTechnique(0);
#else
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen)
        shadowGen->setShadowsOff();
#endif
}


#ifdef USE_OSG_SHADOWS
void SceneGraph::createShadowTechnique()
{
#if 1
    _shadowTechnique = new SoftShadowMap;
    SoftShadowMap* softShadow = dynamic_cast<SoftShadowMap*>(_shadowTechnique.get());
    if(softShadow)
    {
        softShadow->setAmbientBias(osg::Vec2(0.5,0.5));
        softShadow->setTextureUnit(1);
    }
#else
    _shadowTechnique = new ShadowMap;
    ShadowMap* shadowMap = dynamic_cast<ShadowMap*>(_shadowTechnique.get());
    if(shadowMap)
    {
        shadowMap->setAmbientBias(osg::Vec2(0.5,0.5));
        shadowMap->setTextureUnit(1);
    }
#endif
#if 0
    softShadow->clearShaderList();
    osg::ref_ptr<osg::Shader> vertShader = new osg::Shader(osg::Shader::VERTEX);
    osg::ref_ptr<osg::Shader> fragShader = new osg::Shader(osg::Shader::FRAGMENT);
    const std::string vertStr(getIncludePath() + "/Shaders/vertShader.glsl");
    const std::string fragStr(getIncludePath() + "/Shaders/fragShader.glsl");
    ShaderManager::loadShaderSource(vertShader.get(), vertStr);
    ShaderManager::loadShaderSource(fragShader.get(), fragStr);
    softShadow->addShader(fragShader.get());
    softShadow->addShader(vertShader.get());
#endif
}

void SceneGraph::setShadowTechnique(ShadowTechnique *technique)
{
    _shadowTechnique = technique;
#ifdef USE_OSG_SHADOWS
    if(_withShadows) _shadowedScene->setShadowTechnique(_shadowTechnique.get());
#endif
}
#endif

Group* SceneGraph::createPhantomGroup()
{
    _phantomObjectsGroup = new Group;
    _phantomObjectsGroup->setName("phantomObjectsNode");
    _phantomGroupStateSet = new StateSet;
#if 1
    _phantomGroupStateSet->setMode(GL_BLEND, StateAttribute::ON|StateAttribute::OVERRIDE);
    ref_ptr<BlendFunc> bf = new BlendFunc(GL_DST_COLOR, GL_ZERO, GL_ZERO, GL_ONE);
    _phantomGroupStateSet->setAttribute(bf.get(),StateAttribute::ON|StateAttribute::OVERRIDE);
    _phantomGroupStateSet->setRenderBinDetails(-1000, "SORT_FRONT_TO_BACK");
    _phantomGroupStateSet->setMode(GL_CULL_FACE, StateAttribute::ON|StateAttribute::OVERRIDE);
    _phantomGroupStateSet->setMode(GL_LIGHTING, StateAttribute::OFF|StateAttribute::OVERRIDE);
#endif
    _phantomObjectsGroup->setStateSet(_phantomGroupStateSet.get());
    _phantomObjectsGroup->setNodeMask(SHADOW_RECEIVER_MASK);
#ifdef USE_PHONG_SHADING
    ShaderManager::addPhantomShaders(_phantomObjectsGroup.get());
#endif
    return _phantomObjectsGroup.get();
}



std::string SceneGraph::getLightUniformName(unsigned int id)
{
    std::stringstream uniNameStream;
    uniNameStream << "LightSource[" << id << "]";
    std::string uniName = uniNameStream.str();
    return uniName;
}

void SceneGraph::addLightUniforms(unsigned int id, StateSet* stateSet)
{
    Light* light = getLight(id);
    if(stateSet && light)
    {
        std::string uniName = getLightUniformName(id);
        osg::Uniform* ambient = new osg::Uniform(osg::Uniform::FLOAT_VEC4, uniName+".ambient");
        osg::Uniform* diffuse = new osg::Uniform(osg::Uniform::FLOAT_VEC4, uniName+".diffuse");
        osg::Uniform* specular = new osg::Uniform(osg::Uniform::FLOAT_VEC4, uniName+".specular");
        osg::Uniform* position = new osg::Uniform(osg::Uniform::FLOAT_VEC4, uniName+".position");
        osg::Uniform* halfVector = new osg::Uniform(osg::Uniform::FLOAT_VEC4, uniName+".halfVector");
        osg::Uniform* spotDirection = new osg::Uniform(osg::Uniform::FLOAT_VEC3, uniName+".spotDirection");
        osg::Uniform* spotExponent = new osg::Uniform(osg::Uniform::FLOAT, uniName+".spotExponent");
        osg::Uniform* spotCutoff = new osg::Uniform(osg::Uniform::FLOAT, uniName+".spotCutoff");
        osg::Uniform* spotCosCutoff = new osg::Uniform(osg::Uniform::FLOAT, uniName+".spotCosCutoff");
        osg::Uniform* constantAttenuation = new osg::Uniform(osg::Uniform::FLOAT, uniName+".constantAttenuation");
        osg::Uniform* linearAttenuation = new osg::Uniform(osg::Uniform::FLOAT, uniName+".linearAttenuation");
        osg::Uniform* quadraticAttenuation = new osg::Uniform(osg::Uniform::FLOAT, uniName+".quadraticAttenuation");
        osg::Uniform* isInEyeSpace = new osg::Uniform(osg::Uniform::INT, uniName+".isInEyeSpace");
        osg::Uniform* isShadowCaster = new osg::Uniform(osg::Uniform::INT, uniName+".isShadowCaster");

        ambient->set(light->getAmbient());
        diffuse->set(light->getDiffuse());
        specular->set(light->getSpecular());
        position->set(light->getPosition());
        halfVector->set(light->getHalfVector());
        spotDirection->set(light->getDirection());
        spotExponent->set(light->getSpotExponent());
        spotCutoff->set(light->getSpotCutoff());
        spotCosCutoff->set(light->getSpotCosCutoff());
        constantAttenuation->set(light->getConstantAttenuation());
        linearAttenuation->set(light->getLinearAttenuation());
        quadraticAttenuation->set(light->getQuadraticAttenuation());

        isInEyeSpace->set(light->isReferenceFrameVideo() ? 1 : 0);
        isShadowCaster->set(light->isShadowCaster() ? 1: 0);

        stateSet->addUniform(ambient);
        stateSet->addUniform(diffuse);
        stateSet->addUniform(specular);
        stateSet->addUniform(position);
        stateSet->addUniform(halfVector);
        stateSet->addUniform(spotDirection);
        stateSet->addUniform(spotExponent);
        stateSet->addUniform(spotCutoff);
        stateSet->addUniform(spotCosCutoff);
        stateSet->addUniform(constantAttenuation);
        stateSet->addUniform(linearAttenuation);
        stateSet->addUniform(quadraticAttenuation);
        stateSet->addUniform(isInEyeSpace);
        stateSet->addUniform(isShadowCaster);
    }
}

void SceneGraph::removeLightUniforms(unsigned int id)
{
    osg::StateSet* stateSet = _shadowedObjectsGroup->getOrCreateStateSet();
    if(stateSet->getMode(GL_LIGHTING) != StateAttribute::OFF)
        removeLightUniforms(id, stateSet);
}


void SceneGraph::removeLightUniforms(unsigned int id, StateSet* stateSet)
{
    if(stateSet)
    {
        setOffLightUniforms(id, stateSet);
        std::string uniName = getLightUniformName(id);
        stateSet->removeUniform(uniName+".ambient");
        stateSet->removeUniform(uniName+".diffuse");
        stateSet->removeUniform(uniName+".specular");
        stateSet->removeUniform(uniName+".position");
        stateSet->removeUniform(uniName+".halfVector");
        stateSet->removeUniform(uniName+".spotDirection");
        stateSet->removeUniform(uniName+".spotExponent");
        stateSet->removeUniform(uniName+".spotCutoff");
        stateSet->removeUniform(uniName+".spotCosCutoff");
        stateSet->removeUniform(uniName+".constantAttenuation");
        stateSet->removeUniform(uniName+".linearAttenuation");
        stateSet->removeUniform(uniName+".quadraticAttenuation");
        stateSet->removeUniform(uniName+".isInEyeSpace");
        stateSet->removeUniform(uniName+".isShadowCaster");
    }
}

void SceneGraph::updateLightUniforms(unsigned int id, StateSet* stateSet)
{
    Light* light = getLight(id);
    if(stateSet && light)
    {
        if(stateSet->getMode(GL_LIGHTING) != StateAttribute::OFF)
        {
            std::string uniName = getLightUniformName(id);
            if(!stateSet->getUniform(uniName+".ambient"))
                addLightUniforms(id, stateSet);
            stateSet->getUniform(uniName+".ambient")->set(light->getAmbient());
            stateSet->getUniform(uniName+".diffuse")->set(light->getDiffuse());
            stateSet->getUniform(uniName+".specular")->set(light->getSpecular());
            stateSet->getUniform(uniName+".position")->set(light->getPosition());
            stateSet->getUniform(uniName+".halfVector")->set(light->getHalfVector());
            stateSet->getUniform(uniName+".spotDirection")->set(light->getDirection());
            stateSet->getUniform(uniName+".spotExponent")->set(light->getSpotExponent());
            stateSet->getUniform(uniName+".spotCutoff")->set(light->getSpotCutoff());
            stateSet->getUniform(uniName+".spotCosCutoff")->set(light->getSpotCosCutoff());
            stateSet->getUniform(uniName+".constantAttenuation")->set(light->getConstantAttenuation());
            stateSet->getUniform(uniName+".linearAttenuation")->set(light->getLinearAttenuation());
            stateSet->getUniform(uniName+".quadraticAttenuation")->set(light->getQuadraticAttenuation());
            stateSet->getUniform(uniName+".isInEyeSpace")->set(light->isReferenceFrameVideo() ? 1 : 0);
            stateSet->getUniform(uniName+".isShadowCaster")->set(light->isShadowCaster() ? 1 : 0);
        }
    }
}


void SceneGraph::setOffLightUniforms(unsigned int id)
{
    osg::StateSet* stateSet = _shadowedObjectsGroup->getOrCreateStateSet();
    setOffLightUniforms(id, stateSet);
}

void SceneGraph::setOffLightUniforms(unsigned int id, StateSet* stateSet)
{
    Light* light = getLight(id);
    if(stateSet && light)
    {
        std::string uniName = getLightUniformName(id);
        stateSet->getUniform(uniName+".ambient")->set(osg::Vec4(0.0f, 0.0f, 0.0f, 0.0f));
        stateSet->getUniform(uniName+".diffuse")->set(osg::Vec4(0.0f, 0.0f, 0.0f, 0.0f));
        stateSet->getUniform(uniName+".specular")->set(osg::Vec4(0.0f, 0.0f, 0.0f, 0.0f));
    }
}


void SceneGraph::updateLightUniforms(unsigned int id)
{
    osg::StateSet* stateSet = _shadowedObjectsGroup->getOrCreateStateSet();
    updateLightUniforms(id, stateSet);
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->computeShadowViewAndProjection();
#endif
}


void SceneGraph::updateLightUniforms(osg::StateSet* stateSet)
{
    if(stateSet)
    {
        osg::Uniform* uniform = stateSet->getUniform("nbLights");
        if(!uniform) stateSet->addUniform(new osg::Uniform("nbLights", (int)_lights.size()));
        else uniform->set((int)_lights.size());
        std::list< osg::ref_ptr<Light> >::iterator it = _lights.begin();
        for(unsigned int i=0; i<_lights.size(); i++)
        {
            Light* light = it->get();
            updateLightUniforms(light->getLightNum(), stateSet);
            ++it;
        }
    }
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->computeShadowViewAndProjection();
#endif
}

void SceneGraph::addLightUniforms(osg::StateSet* stateSet)
{
    if(stateSet)
    {
        osg::Uniform* uniform = stateSet->getUniform("nbLights");
        if(!uniform) stateSet->addUniform(new osg::Uniform("nbLights", (int)_lights.size()));
        else uniform->set((int)_lights.size());
        std::list< osg::ref_ptr<Light> >::iterator it = _lights.begin();
        for(unsigned int i=0; i<_lights.size(); i++)
        {
            Light* light = it->get();
            addLightUniforms(light->getLightNum(), stateSet);
            ++it;
        }
    }
}


void SceneGraph::addLightUniforms(unsigned int id)
{

    osg::StateSet* stateSet = _shadowedObjectsGroup->getOrCreateStateSet();
    if(stateSet->getMode(GL_LIGHTING) != StateAttribute::OFF)
        addLightUniforms(id, stateSet);
}


void SceneGraph::updateNumLightsUniform()
{
    osg::StateSet* stateSet = _shadowedObjectsGroup->getOrCreateStateSet();
    osg::Uniform* uniform = stateSet->getUniform("nbLights");
    if(!uniform) stateSet->addUniform(new osg::Uniform("nbLights", (int)_lights.size()));
    else uniform->set((int)_lights.size());
}


bool SceneGraph::addObject(Node* object)
{
    bool res = false;
    if(object)
    {
        object->getOrCreateStateSet()->setMode(GL_NORMALIZE, StateAttribute::ON);
        res = _shadowedObjectsGroup->addChild(object);
        Object3D* obj3D = dynamic_cast<Object3D*>(object->asGroup());
        if(obj3D && res)
        {
            obj3D->addSceneGraph(this);
            if(obj3D->isAPhantom())
            {
                res = switchToPhantom(obj3D);
            }
        }
    }
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->computeShadowViewAndProjection();
#endif
    return res;
}


bool SceneGraph::removeObject(Node* object)
{
    bool res = false;
    if(object)
    {
        Object3D* obj3D = dynamic_cast<Object3D*>(object->asGroup());
        if(obj3D)
        {
            obj3D->removeSceneGraph(this);
            if(obj3D->isAPhantom())
                res = _phantomObjectsGroup->removeChild(obj3D);
            else
                res = _shadowedObjectsGroup->removeChild(obj3D);
        }
        else
            res = _shadowedObjectsGroup->removeChild(object);
    }
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->computeShadowViewAndProjection();
#endif
    return res;
}



bool SceneGraph::replaceObject(Node *origObject, Node *newObject)
{
    bool res = false;
    if(origObject && newObject)
    {
        Object3D* orig3D = dynamic_cast<Object3D*>(origObject->asGroup());
        Object3D* new3D = dynamic_cast<Object3D*>(newObject->asGroup());
        if(orig3D && new3D)
        {
            orig3D->removeSceneGraph(this);
            new3D->addSceneGraph(this);
            if(orig3D->isAPhantom() && new3D->isAPhantom())
                res = _phantomObjectsGroup->replaceChild(orig3D, new3D);
            else if(!orig3D->isAPhantom() && !new3D->isAPhantom())
                res = _shadowedObjectsGroup->replaceChild(orig3D, new3D);
        }
        else if(orig3D)
        {
            orig3D->removeSceneGraph(this);
            res = _shadowedObjectsGroup->replaceChild(orig3D, newObject);
        }
        else if(new3D)
        {
            new3D->addSceneGraph(this);
            res = _shadowedObjectsGroup->replaceChild(origObject, new3D);
        }
        else
            res = _shadowedObjectsGroup->replaceChild(origObject, newObject);
    }
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->computeShadowViewAndProjection();
#endif
    return res;
}


bool SceneGraph::switchToPhantom(Object3D *object)
{
    if(!object) return false;
    bool res1 = _phantomObjectsGroup->addChild(object);
    bool res2 = _shadowedObjectsGroup->removeChild(object);
#ifdef USE_PHONG_SHADING
    // check if the object already has a shader program
    StateSet* stateSet = object->getOrCreateStateSet();
    osg::Program* program = dynamic_cast<osg::Program*>(stateSet->getAttribute(StateAttribute::PROGRAM));
    if(program)
    {
        stateSet->setAttributeAndModes(program, StateAttribute::OFF|StateAttribute::OVERRIDE);
    }
#endif
    return (res1 && res2);
}


bool SceneGraph::switchFromPhantom(Object3D *object)
{
    if(!object) return false;
    bool res1 = _shadowedObjectsGroup->addChild(object);
    bool res2 = _phantomObjectsGroup->removeChild(object);
#ifdef USE_PHONG_SHADING
    // check if the object already has a shader program
    StateSet* stateSet = object->getOrCreateStateSet();
    osg::Program* program = dynamic_cast<osg::Program*>(stateSet->getAttribute(StateAttribute::PROGRAM));
    if(program)
    {
        stateSet->setAttributeAndModes(program, StateAttribute::ON|StateAttribute::OVERRIDE);
    }
#endif
    return (res1 && res2);
}

Node* SceneGraph::getObject(int index) const
{
    if (_shadowedObjectsGroup->getNumChildren() < (unsigned) index+1) return NULL;
    else return _shadowedObjectsGroup->getChild(index);
}

unsigned int SceneGraph::getNumObjects() const
{
    return _shadowedObjectsGroup->getNumChildren();
}

int SceneGraph::getObjectIndex(Node* object) const
{
    if(!object) return -1;
    unsigned int index = _shadowedObjectsGroup->getChildIndex(object);
    if (index==getNumObjects()) return -1;
    else return (int)index;
}

Node* SceneGraph::getPhantomObject(int index) const
{
    if (_phantomObjectsGroup->getNumChildren() < (unsigned) index+1) return NULL;
    else return _phantomObjectsGroup->getChild(index);
}

unsigned int SceneGraph::getNumPhantomObjects() const
{
    return _phantomObjectsGroup->getNumChildren();
}

int SceneGraph::getPhantomObjectIndex(Node* object) const
{
    if(!object) return -1;
    unsigned int index = _phantomObjectsGroup->getChildIndex(object);
    if (index==getNumPhantomObjects()) return -1;
    else return (int)index;
}


void SceneGraph::setShadowLight(Light *lightSource)
{
#ifdef USE_OSG_SHADOWS
    ShadowMap* map = dynamic_cast<ShadowMap*>(_shadowTechnique.get());
    if(map && lightSource)
    {
        if(!_shadowerLight)
        {
            if(lightSource) map->setLight(lightSource->getLightSource());
            _shadowerLight = lightSource;
        }
        else
            qWarning() << "Warning: Shadowed scene only accepts one shadower light";
    }
#else
    _shadowerLight = lightSource;
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->setShadowerLight(lightSource);
#endif
#endif
}

int SceneGraph::addLight(Light *lightSource)
{
    if(!lightSource) return -1;
    if (_numLights == MAX_LIGHTS)
    {
        qWarning() << "Maximal number of light sources reached";
        return -1;
    }
    ref_ptr<Light> refLight = lightSource;
    list<ref_ptr<Light> >::iterator findIt = std::find(_lights.begin(), _lights.end(), refLight);
    if(findIt == _lights.end())
    {
        if(lightSource->getLightNum()<0 || lightSource->getLightNum()>7) // default num or unmanaged num
        {
            int lightNum;
            list<ref_ptr<Light> >::iterator posIt = findFirstFreeLightNum(lightNum);
            lightSource->setLightNum(lightNum);
            _lights.insert(posIt, lightSource);
        }
        else // lightNum set by the user -> insert at the right place (if possible)
        {
            bool exists;
            list<ref_ptr<Light> >::iterator posIt = findPositionInList(lightSource->getLightNum(), exists);
            if(exists)
            {
                qWarning() << "Light number " << lightSource->getLightNum() << " already in usage";
                int lightNum;
                posIt = findFirstFreeLightNum(lightNum);
                lightSource->setLightNum(lightNum);
            }
            _lights.insert(posIt, lightSource);
        }

        _lightGroup->addChild(lightSource->getLightSource());
        lightSource->addSceneGraph(this);

#ifdef USE_PHONG_SHADING
        addLightUniforms(lightSource->getLightNum());
        updateNumLightsUniform();
#endif
        if(lightSource->isViewable())
        {
            lightSource->addViewableObject(this);
        }
        if(lightSource->isShadowCaster())
        {
            setShadowLight(lightSource);
        }
        _numLights++;

        if(lightSource->isOn())
        {
            _graphStateSet->setAssociatedModes(lightSource->getLightSource()->getLight(), StateAttribute::ON);
        }
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
        ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
        if(shadowGen && _withShadows)
            shadowGen->computeShadowViewAndProjection();
#endif
        return lightSource->getLightNum();
    }
    qWarning() << "Light already existing";
    return (*findIt)->getLightNum();
}



Light* SceneGraph::getLight(int lightNum)
{
    if(lightNum>8 || lightNum<0) return NULL;
    list< ref_ptr<Light> >::iterator it;
    for(it=_lights.begin(); it!=_lights.end(); ++it)
    {
        if((*it)->getLightNum() == lightNum)
        {
            return (*it).get();
        }
    }
    qWarning() << "Light source " << lightNum << " is inexistant";
    return NULL;
}


int SceneGraph::getLightNumber(Light *lightSource)
{
    int i = -1;
    if(lightSource)
    {
        ref_ptr<Light> refLight = lightSource;
        list< ref_ptr<Light> >::iterator it = find(_lights.begin(), _lights.end(), refLight);
        if(it!=_lights.end())
            i = (*it)->getLightNum();
    }
    return i;
}


bool SceneGraph::removeLight(int lightNum)
{
    if(lightNum<0 && lightNum>8)
    {
        qWarning() << "Light number " << lightNum << " is invalid";
        return false;
    }
    else
    {
        list< ref_ptr<Light> >::iterator it = findLight(lightNum);
        if(it!=_lights.end() && it->valid())
        {
            _graphStateSet->setAssociatedModes((*it)->getLightSource()->getLight(), StateAttribute::OFF);
            (*it)->removeSceneGraph(this);
            bool ret1 = _lightGroup->removeChild((*it)->getLightSource());
#ifdef PoLAR_DEBUG
            if(ret1) qDebug() << "Light source number " << lightNum << " was suppressed";
#endif
            bool ret2 = true;
            if((*it)->isViewable())
            {
                //ret2 = _shadowedObjectsGroup->removeChild((*it)->getViewableObject());
                ret2 = (*it)->removeViewableObject(this);
#ifdef PoLAR_DEBUG
                if(ret2) qDebug() << "Its 3D representation was also suppressed";
#endif
            }
            bool wasShadowCaster = (_shadowerLight == it->get());
            _lights.erase(it);
            _numLights--;

            if(wasShadowCaster) // need to replace the shadower light
            {
                _shadowerLight = NULL;
                list< ref_ptr<Light> >::iterator shadowIt = findFirstShadowerLight();
                if(shadowIt != _lights.end())
                    setShadowLight(shadowIt->get());
                else
                    setShadowLight(_lights.begin()->get());
            }

#ifdef USE_PHONG_SHADING
            removeLightUniforms(lightNum);
            updateNumLightsUniform();
#endif
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
            ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
            if(shadowGen && _withShadows)
            {
                shadowGen->setShadowerLight(_shadowerLight);
                shadowGen->computeShadowViewAndProjection();
            }
#endif
            return ret1 && ret2;
        }
        return false;
    }
}


bool SceneGraph::setIsNotAShadowReceiver(Object3D* obj)
{
    if(obj)
    {
        obj->setShadowReceiverOff();
        return true;
    }
    return false;
}

bool SceneGraph::setIsAShadowReceiver(Object3D* obj)
{
    if(obj)
    {
        obj->setShadowReceiverOn();
        return true;
    }
    return false;
}

bool SceneGraph::setIsNotAShadowCaster(Object3D* obj)
{
    if(obj)
    {
        obj->setShadowCasterOff();
        return true;
    }
    return false;
}

bool SceneGraph::setIsAShadowCaster(Object3D* obj)
{
    if(obj)
    {
        obj->setShadowCasterOn();
        return true;
    }
    return false;
}

bool SceneGraph::setScene(Node *scene)
{
    bool res = false;
    if(scene && scene->asGroup() && !scene->getName().compare("scene"))
    {
        unsigned int index = _root->getChildIndex(_scene.get());
        _scene = scene->asGroup();
        _graphStateSet = scene->asGroup()->getStateSet();
        bool res1 = false, res2 = false, res3 = false, res4 = false;

#ifdef USE_SHADOWS
#ifdef USE_OSG_SHADOWS
        osgShadow::ShadowedScene *shadow = dynamic_cast<osgShadow::ShadowedScene*>(_scene->getChild(0));
#else
        ShadowGenerator* shadow = dynamic_cast<ShadowGenerator*>(_scene->getChild(0));
#endif
        if(shadow)
        {
            _shadowedScene = shadow;
#ifdef USE_OSG_SHADOWS
            _shadowTechnique = shadow->getShadowTechnique();
#endif
            if(_shadowedScene.valid())
                res1 = true;
        }
#else
        res1 = true;
#endif
        if(res1)
        {
#ifdef USE_SHADOWS
            for(unsigned int i=0; i<_shadowedScene->getNumChildren(); i++)
            {
                Node* child = _shadowedScene->getChild(i);
#else
            for(unsigned int i=0; i<_scene->getNumChildren(); i++)
            {
                Node* child = _scene->getChild(i);
#endif
                std::string name = child->getName();
                if(!name.compare("shadowedObjectsNode"))
                {
                    _shadowedObjectsGroup = child->asGroup();
                    if(_shadowedObjectsGroup.valid()) res2 = true;
                }
                else if(!name.compare("lightGroup"))
                {
                    _lightGroup = child->asGroup();
                    if(_lightGroup.valid())
                    {
                        res3 = true;
                        _lights.clear();
                        _numLights = 0;
                        for(unsigned int j=0; j<child->asGroup()->getNumChildren(); j++)
                        {
                            LightSource* ls = dynamic_cast<LightSource*>(child->asGroup()->getChild(j));
                            if(ls)
                            {
                                ref_ptr<Light> light = new Light(ls);
                                _lights.push_back(light);
                                _numLights++;
                            }
                        }
                    }
                }
                else if(!name.compare("phantomObjectsNode"))
                {
                    _phantomObjectsGroup = child->asGroup();
                    _phantomGroupStateSet = child->asGroup()->getStateSet();
                    if(_phantomObjectsGroup.valid() && _phantomGroupStateSet.valid()) res4 = true;
                }
            }
        }
        res = res1 && res2 && res3 && res4;

        if(res)
        {
            res = res && _root->removeChild(index, 1);
            res = res && _root->addChild(_scene.get());
        }
    }
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->computeShadowViewAndProjection();
#endif
    return res;
}


bool SceneGraph::setShadowedObjectsNode(osg::Node *node)
{
    bool res = false;
    if(node && node->asGroup() && !node->getName().compare("shadowedObjectsNode"))
    {
#ifdef USE_SHADOWS
        Group* parentGroup = _shadowedScene.get();
#else
        Group* parentGroup = _scene.get();
#endif
        unsigned int index = parentGroup->getChildIndex(_shadowedObjectsGroup.get());
        _shadowedObjectsGroup = node->asGroup();
        res = parentGroup->removeChild(index, 1);
        res = res && parentGroup->addChild(_shadowedObjectsGroup.get());
    }
    return res;
}


bool SceneGraph::setPhantomObjectsNode(osg::Node *node)
{
    bool res = false;
    if(node && node->asGroup() && !node->getName().compare("phantomObjectsNode"))
    {
#ifdef USE_SHADOWS
        Group* parentGroup = _shadowedScene.get();
#else
        Group* parentGroup = _scene.get();
#endif
        unsigned int index = parentGroup->getChildIndex(_phantomObjectsGroup.get());
        _phantomObjectsGroup = node->asGroup();
        res = parentGroup->removeChild(index, 1);
        res = res && parentGroup->addChild(_phantomObjectsGroup.get());
    }
    return res;
}

bool SceneGraph::setLightGroup(osg::Node *node)
{
    bool res = false;
    if(node && node->asGroup() && !node->getName().compare("lightGroup"))
    {
#ifdef USE_SHADOWS
        Group* parentGroup = _shadowedScene.get();
#else
        Group* parentGroup = _scene.get();
#endif
        unsigned int index = parentGroup->getChildIndex(_lightGroup.get());
        _phantomObjectsGroup = node->asGroup();
        res = parentGroup->removeChild(index, 1);
        res = res && parentGroup->addChild(_lightGroup.get());
        _lights.clear();
        _numLights = 0;
        for(unsigned int j=0; j<_lightGroup->getNumChildren(); j++)
        {
            LightSource* ls = dynamic_cast<LightSource*>(_lightGroup->getChild(j));
            if(ls)
            {
                ref_ptr<Light> light = new Light(ls);
                _lights.push_back(light);
                _numLights++;
            }
        }
    }
#if defined(USE_SHADOWS) && !defined(USE_OSG_SHADOWS)
    ShadowGenerator* shadowGen = dynamic_cast<ShadowGenerator*>(_shadowedScene.get());
    if(shadowGen && _withShadows)
        shadowGen->computeShadowViewAndProjection();
#endif
    return res;
}


list< ref_ptr<Light> >::iterator SceneGraph::findFirstFreeLightNum(int& num)
{
    int lightNum = 0;
    list< ref_ptr<Light> >::iterator it = _lights.begin();
    while(lightNum < 8 && it!=_lights.end())
    {
        if((*it)->getLightNum() > lightNum) break;
        ++it;
        lightNum++;
    }
    num = lightNum;
    return it;
}

list< ref_ptr<Light> >::iterator SceneGraph::findPositionInList(int lightNum, bool& exists)
{
    list< ref_ptr<Light> >::iterator it = _lights.begin();
    while(it!=_lights.end())
    {
        if((*it)->getLightNum() < lightNum)
            ++it;
        else break;
    }
    if(it!=_lights.end() && (*it)->getLightNum() == lightNum) exists = true;
    else exists = false;
    return it;
}

list< ref_ptr<Light> >::iterator SceneGraph::findLight(int lightNum)
{
    list< ref_ptr<Light> >::iterator it = _lights.begin();
    while((*it)->getLightNum() != lightNum && it != _lights.end())
        ++it;
    return it;
}

list< ref_ptr<Light> >::iterator SceneGraph::findFirstShadowerLight()
{
    list< ref_ptr<Light> >::iterator it = _lights.begin();
    while(it!=_lights.end() && !(*it)->isShadowCaster())
    {
        ++it;
    }
    return it;
}

