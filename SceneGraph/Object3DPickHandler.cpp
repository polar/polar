/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QDebug>
#include "Object3DPickHandler.h"
#include "EventUtils.h"
#include "config.h"

using namespace std;
using namespace osg;
using namespace osgManipulator;
using namespace PoLAR;


Object3DPickHandler::Object3DPickHandler(bool displayInfo):
    EventHandlerInterface(displayInfo),
    _activeDragger(NULL),
    _cmdMgr(new osgManipulator::CommandManager),
    _interactMode(VIEW)
{
    //_manipulationGroup = NULL;
    _objectEditionModeKey = '1';
    _cameraManipulationModeKey = '0';
}


bool Object3DPickHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    bool foundObject3D;

    BaseViewer* baseViewer = dynamic_cast<BaseViewer*>(&aa);
    if (!baseViewer) return false;

    Viewer2D3D* viewer2D3D = dynamic_cast<Viewer2D3D*>(baseViewer->getGraphicsViewer());
    if (!viewer2D3D) return false;

    addHUD(viewer2D3D->width(), viewer2D3D->height());

    osgViewer::View* viewer = dynamic_cast<osgViewer::View*>((&aa));
    if (!viewer) return false;

    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) && (ea.getKey() == _objectEditionModeKey)) // Object edition mode
    {
        osgViewer::View::EventHandlers eventHandlers = baseViewer->getEventHandlers();
        osgViewer::View::EventHandlers::iterator itrL;
        for (itrL=eventHandlers.begin(); itrL != eventHandlers.end(); ++itrL)
        {
            GeometryPickHandler* geomPick = dynamic_cast<GeometryPickHandler*>((*itrL).get());
            if (geomPick) {geomPick->setInteractMode(GeometryPickHandler::VIEW); geomPick->cleanUp();}
        }
        _interactMode = PICK;
    }
    if ((ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) && (ea.getKey() == _cameraManipulationModeKey)) // camera manipulation mode
    {
        _interactMode = VIEW;
        cleanUp(viewer2D3D);
    }

    if (_interactMode == VIEW) return false;


    switch (ea.getEventType())
    {
    case osgGA::GUIEventAdapter::PUSH:
    {
        osgUtil::LineSegmentIntersector::Intersections intersections;

        _pointer.reset();

        if (computeIntersections(viewer, ea.getX(),ea.getY(),intersections,~PICKHANDLER_MASK))
        {
            _pointer.setCamera(viewer->getCamera());
            _pointer.setMousePosition(ea.getX(), ea.getY());
            foundObject3D = false;

            for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();  hitr != intersections.end(); ++hitr)
            {
                NodePath nodePath = (NodePath) hitr->nodePath;
                ref_ptr<Object3D> hitObject3D;
                for(NodePath::iterator itrNodePath = nodePath.begin(); itrNodePath != nodePath.end(); ++itrNodePath)
                {
                    if (!foundObject3D && (((*itrNodePath)->getName())=="Dragger Handle"))
                    {
                        foundObject3D = true;
                    }
                    if (!foundObject3D)
                    {
                        hitObject3D = dynamic_cast<Object3D*>(*itrNodePath);
                        if (hitObject3D != NULL)
                        {
                            foundObject3D = true;
                            if (hitObject3D->isPickable())
                            {
                                viewer2D3D->setCurrentPickedObject(hitObject3D.get());
                                if (hitObject3D->isEditable())
                                {
                                    //second condition is to avoid to edit an object already in edition mode, it is necessary to have a good working of the program
                                    /* if ((_manipulationGroup.get() == NULL) || (hitObject3D.get() != dynamic_cast<Object3D*>(_manipulationGroup->getChild(1))))
                                    {
                                        manipulateObject(hitObject3D.get());
                                        _activeDragger = dynamic_cast<osgManipulator::Dragger*>(_manipulationGroup->getChild(0));
                                    }*/
                                    if(_manipulationGroup.isNull() || (hitObject3D.get() != dynamic_cast<Object3D*>(_manipulationGroup.object)))
                                    {
                                        manipulateObject(hitObject3D.get());
                                        _activeDragger = dynamic_cast<osgManipulator::Dragger*>(_manipulationGroup.activeDragger);
                                    }
                                }
                            }
                            else qWarning() << hitObject3D->getNameAsQString() << "is not pickable.";
                        }

                    }
                    else break;
                }

                /*if (!(hitr->nodePath).empty())*/
                {
                    _pointer.addIntersection(hitr->nodePath, hitr->getLocalIntersectPoint());
                }
            }

            if (_activeDragger)
            {
                _activeDragger->handle(_pointer, ea, aa);
                break;
            }
        }
        qWarning("\n");
        break;
    }

    case osgGA::GUIEventAdapter::DRAG:
    case osgGA::GUIEventAdapter::RELEASE:
    {
        if (_activeDragger)
        {
            _pointer._hitIter = _pointer._hitList.begin();
            _pointer.setCamera(viewer->getCamera());
            _pointer.setMousePosition(ea.getX(), ea.getY());

            _activeDragger->handle(_pointer, ea, aa);
        }
        break;
    }
    default:

    {
        osgUtil::LineSegmentIntersector::Intersections intersections;
        std::string gdlist="";

        if (computeIntersections(viewer, ea.getX(),ea.getY(),intersections,~PICKHANDLER_MASK))
        {
            foundObject3D = false;
            for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin(); hitr != intersections.end(); ++hitr)
            {
                std::ostringstream os;
                ref_ptr<Object3D> hitObject3D;
                NodePath nodePath = (NodePath) hitr->nodePath;
                for(NodePath::iterator itrNodePath = nodePath.begin(); itrNodePath != nodePath.end(); ++itrNodePath)
                {
                    if (!foundObject3D)
                    {
                        hitObject3D = dynamic_cast<Object3D*>(*itrNodePath);
                        if (hitObject3D != NULL)
                        {
                            foundObject3D = true;
                            os<<"Object \""<<hitObject3D->getName()<<"\"";
                            if (hitObject3D->isPickable()) os<<", pickable";
                            else os<<", non pickable";
                            if (hitObject3D->isEditable()) os<<", editable";
                            else os<<", non editable";
                            os<<std::endl<<"World coords intersect point ("<< hitr->getWorldIntersectPoint()<<")"<<std::endl;
                        }
                    }
                }
                if (gdlist == "") gdlist += os.str();
            }
        }
        setLabel(gdlist);
        break;
    }
    }

    if (ea.getEventType() == osgGA::GUIEventAdapter::RELEASE) _pointer.reset();

    return true;
}

void Object3DPickHandler::cleanUp(Viewer2D3D* viewer)
{
    stopManipulation();
    cleanUp();
    viewer->setCurrentPickedObject((Object3D*)NULL);
    viewer->update();
}


ref_ptr<Group> Object3DPickHandler::addDraggerToObject(Object3D* object3D, ManipulationGroup &manipulationGroup)
{
    object3D->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
    ref_ptr<Selection> selection;
    //if (object3D->isAPhantom()) selection = dynamic_cast<Selection*>(dynamic_cast<Group*>(object3D->getChild(0))->getChild(0));
    //else
    selection = dynamic_cast<Selection*>(object3D->getChild(0));

    Dragger* dragger = createDragger();
    ref_ptr<Group> root = new Group;
    root->addChild(dragger);
    //root->addChild(object3D);
    //_sceneGraph->getSceneGraph()->addChild(dragger);
    if(_sceneGraph.get()) _sceneGraph->getSceneGraph()->addChild(root.get());

    //Group* parent = object3D->getParent(0);
    //parent->addChild(dragger);

    float scale = object3D->getOriginalNode()->getBound().radius() * 1.6;
    dragger->setMatrix(object3D->getFrameMatrix() *
                       Matrix::inverse(Matrix::translate(object3D->getFrameMatrix().getTrans())) *
                       Matrix::inverse(Matrix::rotate(object3D->getTransformationMatrix().getRotate())) *
                       Matrix::scale(scale, scale, scale) *
                       Matrix::translate(object3D->getOriginalNode()->getBound().center()));

    dragger->postMult(selection->getMatrix());

    _cmdMgr->connect(*dragger, *selection);

    manipulationGroup.object = object3D;
    manipulationGroup.activeDragger = dragger;

    //return dragger->getParent(0);
    return root;
}


void Object3DPickHandler::manipulateObject(Object3D* object3D)
{
    // if (_manipulationGroup.get() != NULL) stopManipulation();
    // _manipulationGroup = addDraggerToObject(object3D);
    if(!_manipulationGroup.isNull()) stopManipulation();
    addDraggerToObject(object3D, _manipulationGroup);
}


void Object3DPickHandler::stopManipulation()
{
    /* if (_manipulationGroup != NULL)
    {
        _manipulationGroup->removeChild(0,2);
        if (_sceneGraph.get()) _sceneGraph->getSceneGraph()->removeChild(_activeDragger);
        _manipulationGroup = NULL;
    }*/
    if(!_manipulationGroup.isNull())
    {
        if (_sceneGraph.get()) _sceneGraph->getSceneGraph()->removeChild(_activeDragger->getParent(0));
        _manipulationGroup.setNull();
    }
}
