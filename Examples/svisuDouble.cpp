/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>

#include <QApplication>
#include <QMainWindow>
#include <QLayout>

#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>

#include <PoLAR/Image.h>
#include <PoLAR/Util.h>
#include "MyViewer.h"

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    QMainWindow mw;

    QFrame f(&mw);
    mw.setCentralWidget(&f);

    QHBoxLayout lo(&f);

    int width;
    int height;

    if (argc > 1)
    {
        osg::ref_ptr<PoLAR::Image_uc> myImage;
        if(PoLAR::Util::fileExists(argv[1]))
            myImage = new PoLAR::Image_uc(argv[1], true);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
        width = myImage->getWidth();
        height = myImage->getHeight();

        // first viewer
        MyViewer *viewer = new MyViewer(&f);
        viewer->resize(width,height);
        viewer->setBgImage(myImage);
        viewer->bgImageOn();
        viewer->startEditImageSlot();

        // second viewer
        MyViewer *viewer2 = new MyViewer(&f);
        viewer2->resize(viewer->width(), viewer->height());
        viewer2->setBgImage<unsigned char>(NULL, width, height, 3, 1);
        viewer2->startEditImageSlot();

        // load the scene
        osg::ref_ptr<osg::Node> loadedModel;
        if (argc > 3 && PoLAR::Util::fileExists(argv[3]))
        {
            // object loaded once but shared between 2 PoLAR::Object3D
            loadedModel = osgDB::readNodeFile(argv[3]);
            osgUtil::SmoothingVisitor smooth;
            osgUtil::Optimizer optimizer;
            optimizer.optimize(loadedModel.get());
            loadedModel->accept(smooth);

            viewer->addObject3D(new PoLAR::Object3D(loadedModel.get(), false));
            viewer2->addObject3D(new PoLAR::Object3D(loadedModel.get(), false));
        }

        // create a ground
        {
            viewer->addLightSource(5,2,5, true);
            osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
            osg::ref_ptr<osg::ShapeDrawable> shape;
            shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 5.0f, 5.0f, 0.00001f));
            floorGeode->addDrawable(shape.get());
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get());
            object3D->setName("Floor");
            object3D->setEditOn();
            object3D->setPhantomOn();
            viewer->addObject3D(object3D.get());
            viewer->setShadowsOn();
        }

        // Read type of projection given in parameter
        PoLAR::Viewer2D3D::ProjectionType pt=PoLAR::Viewer2D3D::VISION;
        if (argc > 4)
        {
            switch(atoi(argv[4]))
            {
            case 1: pt=PoLAR::Viewer2D3D::ANGIO; break;
            case 2: pt=PoLAR::Viewer2D3D::ANGIOHINV; break;
            default: pt=PoLAR::Viewer2D3D::VISION;
            }
        }

        osg::Matrixd P;
        if (argc > 2)
            P = PoLAR::Util::readProjectionMatrix(argv[2]); // load the projection
        else
            P = PoLAR::Viewer2D3D::createVisionProjection(width, height, loadedModel->getBound().center(),loadedModel->getBound().radius()); // Or if no projection is given in parameter, create a custom one function of the size of the background image and the model size

        // Set a projection to first viewer
        viewer->setProjection(P, pt);

        // Set a projection to second viewer (this method sets a projection function of background image and model size)
        viewer2->setProjection();

        lo.addWidget(viewer);
        lo.addWidget(viewer2);
        mw.resize(width*2, height);

        //center the window
        mw.setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, mw.size(), qApp->desktop()->availableGeometry()));

        mw.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image> <projection> <3D model> [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]" <<std::endl;
        exit(0);
    }
    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
