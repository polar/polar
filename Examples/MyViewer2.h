/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWER2_H
#define _MYVIEWER2_H

#include <osg/MatrixTransform>
#include <osg/Geode>
#include <osg/ShapeDrawable>
#include <osgSim/SphereSegment>
#include <osg/io_utils>

#include "MyViewer.h"
#include <PoLAR/ShaderManager.h>
#include <PoLAR/BaseImage.h>

class MyViewer2: public MyViewer
{
    Q_OBJECT
public:
    MyViewer2(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        MyViewer(parent, name, f), _ima(NULL), _geode(new osg::Geode), _trans(new osg::MatrixTransform)
    {
        getOrthoCamera()->setClearColor(osg::Vec4f(0.2,0.2,0.8,1));
        addObject3D(new PoLAR::Object3D(_trans.get()));
        _trans->addChild(_geode.get());
    }

    void setImage(PoLAR::BaseImage *ima, float resX, float resY)
    {
        if (ima)
        {
            _ima = ima;

            int w=_ima->getWidth(), h=_ima->getHeight();
            float wT = 1.0, hT = 1.0;
            if (_geode->getNumDrawables() == 0)
                _geode->addDrawable(osg::createTexturedQuadGeometry( osg::Vec3(0,0,0),
                                                                     osg::Vec3(w*resX,0,0),
                                                                     osg::Vec3(0,h*resY,0),
                                                                     wT,hT
                                                                     )
                                    );
            else
                _geode->setDrawable(0, osg::createTexturedQuadGeometry( osg::Vec3(0,0,0),
                                                                       osg::Vec3(w*resX,0,0),
                                                                       osg::Vec3(0,h*resY,0),
                                                                       wT,hT
                                                                       )
                                    );
            osg::ref_ptr<osg::Texture2D> t = _ima->makeTexture();
            t->setResizeNonPowerOfTwoHint(false);

            osg::ref_ptr<osg::StateSet> stateSet = _geode->getOrCreateStateSet();
            stateSet->setTextureAttributeAndModes(0, t.get(), osg::StateAttribute::ON);
            stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

            // Adding a shader system for window level
            PoLAR::ShaderManager::addWindowLevel(_geode.get(), _ima.get());

            // each time the image is updated, it requests a redraw of the OSG scene
            QObject::connect(_ima.get(), SIGNAL(updateSignal()), this, SLOT(requestRedraw()));
            // connect the method used to update the transformation matrix to the signal indicating the image index was changed
            QObject::connect(_ima.get(), SIGNAL(indexSignal(int)), this, SLOT(changeIndexSlot(int)));
            osg::Matrixd P = PoLAR::Viewer2D3D::createVisionProjection(w,h,osg::Vec3f(w*resX/2,h*resY/2,_sl[_ima->getNbImages()/2]), w*resX/2);
            setProjection(P);
            changeIndexSlot(_ima->imageIndex());
        }
        else
        {
            if (_ima->valid())
            {
                QObject::disconnect(getGLViewer(), SIGNAL(mousePressSignal(QMouseEvent*)), _ima.get(), SLOT(mousePressSlot(QMouseEvent*)));
                QObject::disconnect(getGLViewer(), SIGNAL(mouseReleaseSignal(QMouseEvent*)), _ima.get(), SLOT(mouseReleaseSlot(QMouseEvent*)));
                QObject::disconnect(_ima.get(), SIGNAL(indexSignal(int)), this, SLOT(changeIndexSlot(int)));
                _ima = NULL;
            }
        }
    }

public slots:

    virtual void startEditImageSlot()
    {
        QObject::connect(getGLViewer(), SIGNAL(mousePressSignal(QMouseEvent*)), _ima.get(), SLOT(mousePressSlot(QMouseEvent*)));
        QObject::connect(getGLViewer(), SIGNAL(mouseReleaseSignal(QMouseEvent*)), _ima.get(), SLOT(mouseReleaseSlot(QMouseEvent*)));
        PoLAR::Viewer::startEditImageSlot();
    }
    virtual void startManipulateSceneSlot()
    {
        QObject::disconnect(getGLViewer(), SIGNAL(mousePressSignal(QMouseEvent*)), _ima.get(), SLOT(mousePressSlot(QMouseEvent*)));
        QObject::disconnect(getGLViewer(), SIGNAL(mouseReleaseSignal(QMouseEvent*)), _ima.get(), SLOT(mouseReleaseSlot(QMouseEvent*)));
        PoLAR::Viewer::startManipulateSceneSlot();
    }
    
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch (event->key())
        {
        case Qt::Key_I:
            this->startEditImageSlot();
            break;
        case Qt::Key_Down:
            if (_ima.valid() && _ima->valid()) _ima->previousImageSlot();
            break;
        case Qt::Key_Up:
            if (_ima.valid() && _ima->valid()) _ima->nextImageSlot();
            break;
        default: MyViewer::keyPressEvent(event);
        }
    }

    virtual void changeIndexSlot(int n)
    {
        if (getCameraManipulator())
        {
            osg::Matrix m;
            m.makeTranslate(osg::Vec3f(0,0,_sl[n]));
            _trans->setMatrix(m);
            update();
        }
    }

private:

    osg::ref_ptr<PoLAR::BaseImage> _ima;
    osg::ref_ptr<osg::Geode> _geode;
    osg::ref_ptr<osg::MatrixTransform> _trans;
    const static float _sl[32];
};

#endif
