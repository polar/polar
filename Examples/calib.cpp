/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>

#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>

#include "CalibViewer.h"
#include <PoLAR/Image.h>
#include <iostream>



int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    CalibViewer viewer;
    viewer.center();
    viewer.show();

    int width=0, height=0;
    osg::ref_ptr<PoLAR::Image<unsigned char> > img;
    if(argc > 1)
    {
        if(PoLAR::Util::fileExists(argv[1]))
            img = new PoLAR::Image<unsigned char>(argv[1], true);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
    }
    else
    {
        QString fname = QFileDialog::getOpenFileName(&viewer,
                                                     "Choose an image",
                                                     QString::null);
        if(!fname.isEmpty())
            img = new PoLAR::Image<unsigned char>(fname.toStdString(), true);
        else
            std::cerr << "Invalid file\n";
    }
    if(img.valid() && img->valid())
    {
        width = img->getWidth();
        height = img->getHeight();
        viewer.setBgImage(img.get());
        viewer.resize(width, height);
        viewer.bgImageOn();
    }
    viewer.setSaveFName("points.txt");

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}

