Portable Library for Augmented Reality - collection of samples

These assets are intended to be used as example assets combined with the example programs provided in the parent directory.
All of them are published under the GNU General Public License.

Assets:
* example1.ppm  - a photography of the interior of a building
* example1.proj - the projection matrix associated with the previous photography
* armchair.obj  - a simple armchair 3D model
* armchair.mtl  - the material associated to the previous 3D model
* armchair.jpg  - the texture associated to the previous material
* carpet.jpg    - a simple carpet texture
* icons folder  - a folder containing some simple icons to use in GUI examples