/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>

#include <PoLAR/Image.h>
#include "MyViewer2Dbis.h"
#include "ImageSequence.h"
#include <iostream>


int main(int argc,char ** argv)
{
    if (argc == 1 || (argc > 2 && argc < 7))
    {
        std::cerr << "Syntax: " << argv[0] << " <data> [<width> <height> <nima> <header offset> <pixel type> [<proj> <off> <projType> <savename>]]\n";
        exit(0);
    }

    QApplication app(argc, argv);
    MyViewer2D viewer;

    int width=0, height=0, nima=1;

    if(argc > 2)
    {
        // load data sequence
        width=atoi(argv[2]);
        height=atoi(argv[3]);
        nima=atoi(argv[4]);

        int offset=atoi(argv[5]);
        float* sequence = readData(argv[1],width,height,nima,offset,argv[6]);
        if (!sequence) std::cerr << "Could not read image data\n";
        viewer.setBgImage<float>(sequence, width, height, 1, nima, true);
        unsigned int zoom = 2;
        viewer.resize(width*zoom, height*zoom);
        viewer.setZoom(zoom);
    }
    else
    {
        //load classic image
        osg::ref_ptr<PoLAR::Image<unsigned char> > img;
        if(PoLAR::Util::fileExists(argv[1]))
            img = new PoLAR::Image<unsigned char>(argv[1], true);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
        width = img->getWidth();
        height = img->getHeight();
        viewer.setBgImage(img.get());
        viewer.resize(width, height);
    }
    // scene creation
    viewer.bgImageOn();
    viewer.startEditImageSlot();
    viewer.setZoomMode(PoLAR::PanZoom::SCROLL, 10.0f);


    PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION;
    if (argc > 9)
    {
        switch(atoi(argv[9]))
        {
        case 1: pt=PoLAR::Viewer2D3D::ANGIO; break;
        case 2: pt=PoLAR::Viewer2D3D::ANGIOHINV; break;
        default: pt=PoLAR::Viewer2D3D::VISION;
        }
    }

    // Load the projection given in parameter
    if (argc > 7)
    {
        osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[7]);
        viewer.setProjection(P, pt);
    }

    if (argc > 8)
    {
        if(!PoLAR::Util::fileExists(argv[8]))
        {
            std::cerr << "Unable to load model" << std::endl;
            exit(0);
        }

        osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(argv[8], true);
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;
        optimizer.optimize(object3D.get());
        object3D->accept(smooth);
        object3D->setShininess(0.7);
        object3D->setAmbientColor(1,1,1);
        object3D->setDiffuseColor(0.7, 0.7, 0.7);
        object3D->setSpecularColor(1,1,1);
        viewer.addObject3D(object3D);

        viewer.setTrackNode(object3D.get());
    }

    if (argc > 10) viewer.setSaveFName(argv[10]);
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}

