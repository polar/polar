/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <PoLAR/Spline2D.h>
#include <cstdlib>
#include <iostream>

using namespace PoLAR;
int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        std::cerr << "This procedure resamples a group of points using cardinal splines" << std::endl;
        std::cerr << "Syntax: resampleSpline <filein> <nbpts> <fileout>" << std::endl;
        exit(0);
    }


    std::list<Marker2D*> list;

#if defined(WIN32) || defined(_WIN32)
	errno_t err;
	FILE *f;
	err = fopen_s(&f, argv[1], "r");
	if(err)
#else
    FILE *f = fopen(argv[1], "r");
    if(!f)
#endif
    {
        std::cerr << "file unreadable" << std::endl;
        exit(-1);
    }
    Marker2D *marker;
    float x, y, label;
    int n;
#if defined(WIN32) || defined(_WIN32)
	while (!feof(f) && (n = fscanf_s(f, "%f %f %f", &label, &x, &y)) == 3)
#else
    while(!feof(f) && (n=fscanf(f, "%f %f %f", &label, &x, &y)) == 3)
#endif
    {
        marker = new Marker2D(x, y, label);
        list.push_front(marker);
    }
    fclose(f);

    int N=atoi(argv[2]);
    if (N <= 0)
    {
        std::cerr << "The number of points must be strictly positive" << std::endl;
        exit(-1);
    }

    Spline2D b(list);

    std::list<Marker2D*> out = b.resample(N);

#if defined(WIN32) || defined(_WIN32)
	FILE *fout;
	err = fopen_s(&fout, argv[3], "w+");
	if (err)
#else
	FILE *fout = fopen(argv[3], "w+");
    if(!fout)
#endif
		exit(-1);

    std::list<Marker2D*>::iterator it = out.begin();
    while(it != out.end())
    {
        (*it)->fprintf(fout);
        ++it;
    }
    fclose(fout);

    return 0;
}
