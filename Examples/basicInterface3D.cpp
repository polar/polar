/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "basicInterface3D.h"
#include "props.h"
#include <QColorDialog>

using PoLAR::Marker2D;

Interface::Interface()
{
    QString dir = dataPath() + "icons/";

    viewer = new PoLAR::Viewer(this, "viewer");
    viewer->setShadowsOn();
    viewer->addEventHandler(new osgViewer::StatsHandler);// add stats handler
    setCentralWidget(viewer);

    toolbar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea, toolbar);
    toolbar->setOrientation(Qt::Vertical);

    statuslabel = new QLabel(toolbar);
    statuslabel->setText(*(new QString("Tools")));


    toolbar->addAction(QIcon(dir+"exit.png"), QString("Quit"), this, SLOT(exit()));

    QAction *editImgAct = new QAction(QIcon(dir+"img.png"), tr("edit image"), this);
    editImgAct->setStatusTip(tr("Edit Image"));
    QObject::connect(editImgAct, SIGNAL(triggered()), viewer, SLOT(startEditImageSlot()));
    QObject::connect(editImgAct, SIGNAL(triggered()), this, SLOT(deactivateLabelBox()));
    toolbar->addAction(editImgAct);

    toolbar->addAction(QIcon(dir+"edit.png"), QString("Edit Markers"), this, SLOT(newSpline()));

    toolbar->addAction(QIcon(dir+"save.png"), QString("Save Markers"), this, SLOT(saveMarkers()));

    labelBox = new QSpinBox(this);
    labelBox->setRange(-1,19);
    labelBox->setSingleStep(1);
    toolbar->addWidget(labelBox);


    deactivateLabelBox();
    QObject::connect(labelBox, SIGNAL(valueChanged(int)), viewer, SLOT(changeMarkerLabelSlot(int)));
    QObject::connect(viewer, SIGNAL(selectMarkerSignal(Marker2D*)), this, SLOT(activateLabelBox(Marker2D*)));
    QObject::connect(viewer, SIGNAL(unselectMarkerSignal()), this, SLOT(deactivateLabelBox()));

    scrollbarTB = new QToolBar(this);

    scrollbarTB->setOrientation(Qt::Vertical);

    scrollbar = new QScrollBar(this);
    scrollbar->setMinimum(0);
    scrollbar->setMaximum(0);
    scrollbar->setPageStep(10);
    scrollbar->setValue(0);
    scrollbar->setFixedHeight(height());
    scrollbar->setOrientation(Qt::Vertical);
    scrollbarTB->addWidget(scrollbar);
    scrollbarTB->adjustSize();


    QObject::connect(scrollbar, SIGNAL(valueChanged(int)), viewer, SLOT(gotoImageSlot(int)));
    QObject::connect(scrollbar, SIGNAL(valueChanged(int)), this, SLOT(changeTitle(int)));

    addToolBar(Qt::RightToolBarArea, scrollbarTB);

    move(0,0);

    QObject::connect(this, SIGNAL(startEditImageSignal()), viewer, SLOT(startEditImageSlot()));
    QObject::connect(this, SIGNAL(startEditMarkersSignal()), viewer, SLOT(startEditMarkersSlot()));

    toolbar->addAction(QIcon(dir+"scene.png"), QString("Manipulate Scene"), viewer, SLOT(startManipulateSceneSlot()));


    // create a PoLAR::Light corresponding to a world light positionned in (5.0,2.0,5.0)
    PoLAR::Light *light2 = getViewer()->addLightSource(5.0, 2.0, 5.0, false/*, true*/);

    // connect it to the custom PoLAR::LightDialog to be able to manage its parameters
    PoLAR::LightDialog *lightDialog = new PoLAR::LightDialog(viewer, light2, this);
    QPushButton *moreButton2 = new QPushButton(QString("Worldlight parameters"), this);
    toolbar->addWidget(moreButton2);
    QObject::connect(moreButton2, SIGNAL(pressed()), lightDialog, SLOT(show()));

    // A blend dialog to be able to manage the blending of a PoLAR::Object3D (see in basicInterface.h, method addNode to see how to connect a PoLAR::Object3D to it)
    _blendDialog = new PoLAR::BlendDialog(viewer, 0, this);
    QPushButton *moreButton3 = new QPushButton(QString("Blending parameters"), this);
    toolbar->addWidget(moreButton3);
    QObject::connect(moreButton3, SIGNAL(pressed()), _blendDialog, SLOT(show()));

    displayCheckBox = new QCheckBox(QString("display on/off the object3D"), this);
    toolbar->addWidget(displayCheckBox);
    displayCheckBox->setChecked(true);

    toolbar->adjustSize();
    toolbar->setAutoFillBackground(true);

    // add focus to this interface in order to get the keyboard events
    this->setFocusPolicy(Qt::StrongFocus);
    // remove the focus from the Viewer, otherwise it will bypass the interface's events
    viewer->setFocusPolicy(Qt::NoFocus);

    mb= new QMessageBox(
                QMessageBox::Information,
                QString("Help: shortkeys"),
                QString("Shortkeys:\n")+
                QString("H: this help\n")+
                QString("Q: quit the application\n")+
                QString("I: edit image mode (pan/zoom/window level)\n")+
                QString("M: add/edit markers mode (spline)\n")+
                QString("W: manipulate scene mode\n")+
                QString("D: toggle background image display\n")+
                QString("G: grab a window snapshot\n")+
                QString("X: translate the loaded model along X axis (+)\n")+
                QString("Y: translate the loaded model along Y axis (+)\n")+
                QString("T: translate the loaded model along Y axis (-)\n")+
                QString("Z: translate the loaded model along Z axis (+)\n")+
                QString("A: translate the loaded model along Z axis (-)\n")+
                QString("Up: rotate the loaded model around Y axis (+)\n")+
                QString("Down: rotate the loaded model around Y axis (-)\n")+
                QString("Right: rotate the loaded model around X axis (+)\n")+
                QString("Left: rotate the loaded model around X axis (-)\n")+
                QString("Page Down: scale the loaded model along Y axis (-)\n")+
                QString("Page Up: scale the loaded model along Y axis (+)\n")+
                QString("R: reset the manipulations done in the current frame\n")+
                QString("C: assign a custom frame matrix (see source code) to the manipulations\n")+
                QString("O: hide all the objects\n")+
                QString("P: set all the objects to be displayed\n")+
                QString("else default openscenegraph keys\n"),
                QMessageBox::NoButton,
                this);
    mb->setWindowModality(Qt::NonModal);
}

Interface::~Interface()
{
}

void Interface::activateLabelBox(Marker2D *m)
{
    labelBox->setEnabled(true);
    if (m) labelBox->setValue((int)m->label());
}

void Interface::deactivateLabelBox()
{
    labelBox->setValue(-1);
    labelBox->setEnabled(false);
}

void Interface::changeTitle(int n)
{
    char buf[256];
    sprintf(buf, "Image %3d",n);
    statuslabel->setText(*(new QString(buf)));
}

// Add a new DrawableSpline2D to the viewer
void Interface::newSpline()
{
    viewer->newSpline();
    // change display properties of the newly added DrawableObject2D
    viewer->getCurrentObject2D()->setProperties(QColor("black"),
                                              QColor("red"),
                                              QColor("yellow"),
                                              PoLAR::DrawableObject2D::NONE,
                                              PoLAR::DrawableObject2D::CIRCLE,
                                              PoLAR::DrawableObject2D::SQUARE,
                                              5, 10, 2);
}

void Interface::saveMarkers()
{
    QString fname = QFileDialog::getSaveFileName( this,
                                                "Choose a filename to save the markers",
                                                QString::null);
    if (!fname.isEmpty()) viewer->saveObject2D(fname);

}


// Add key bindings
void Interface::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_H:
    {
        // Show the help meassage box
        mb->show();
        break;
    }
    case Qt::Key_Q:
    {
        exit();
        break;
    }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // key bindings connected to slots managing the edition of the PoLAR::object3D pointed by _object3D//
        //
        // All transformations are made in the transformation frame of the object3D, default is world frame, but you can change it (see key binding Qt::Key_C for an example)
    case Qt::Key_Up:
    {
        _object3D->rotateYSlot(0.5);
        break;
    }
    case Qt::Key_Down:
    {
        _object3D->rotateYSlot(-0.5);
        break;
    }
    case Qt::Key_Right:
    {
        _object3D->rotateXSlot(0.5);
        break;
    }
    case Qt::Key_Left:
    {
        _object3D->rotateXSlot(-0.5);
        break;
    }
    case Qt::Key_PageUp:
    {
        _object3D->scale(1.0,1.5,1.0);
        break;
    }
    case Qt::Key_PageDown:
    {
        _object3D->scale(1.0,0.5,1.0);
        break;
    }
    case Qt::Key_X:
    {
        _object3D->translateXSlot(0.5);
        break;
    }
    case Qt::Key_T:
    {
        _object3D->translateYSlot(-0.5);
        break;
    }
    case Qt::Key_Y:
    {
        _object3D->translateYSlot(0.5);
        break;
    }
    case Qt::Key_Z:
    {
        _object3D->translateZSlot(0.5);
        break;
    }
    case Qt::Key_A:
    {
        _object3D->translateZSlot(-0.5);
        break;
    }
    case Qt::Key_R:
    {
        // Reset all the the transformation which were made in the object3D's current frame
        _object3D->resetAllManipulations();
        break;
    }
    case Qt::Key_C:
    {
        // Assigned a frame to the object3D in which further 3D editions will be made
        _object3D->setFrameMatrix(osg::Matrixd::rotate(M_PI/5, 0.0,1.0,0.0));
        break;
    }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
    case Qt::Key_O:
    {
        // Hide all object3D of the scene graph
        std::list<osg::ref_ptr<PoLAR::Object3D> > list = viewer->getObjects3D();
        std::list<osg::ref_ptr<PoLAR::Object3D> >::iterator itrL;
        for (itrL=list.begin(); itrL != list.end(); ++itrL) (*itrL)->setDisplayOff();
        break;
    }
    case Qt::Key_P:
    {
        // Display all object3D of the scene graph
        std::list<osg::ref_ptr<PoLAR::Object3D> > list = viewer->getObjects3D();
        std::list<osg::ref_ptr<PoLAR::Object3D> >::iterator itrL;
        for (itrL=list.begin(); itrL != list.end(); ++itrL) (*itrL)->setDisplayOn();
        break;
    }
    case Qt::Key_D:
    {
        // Hide/display background image
        viewer->bgImageToggle();
        break;
    }
    case Qt::Key_I:
    {
        // Set the image interaction to be activ
        viewer->startEditImageSlot();
        break;
    }
    case Qt::Key_M:
    {
        newSpline();
        break;
    }
    case Qt::Key_W:
    {
        // Set the scene graph interaction to be activ
        viewer->startManipulateSceneSlot();
        break;
    }
    case Qt::Key_G:
    {
        QString fname=QFileDialog::getSaveFileName( this,
                                                    "Choose a filename to save the snapshot image",
                                                    QString::null,
                                                    "PNG images (*.png)");
        if (!fname.isEmpty()) viewer->grab().save(fname, "PNG");
        break;
    }

    default:
        viewer->notify(event);
    }
}
