#ifndef _MYVIEWER_
#define _MYVIEWER_

#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QtCore/QObject>
#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)

#include "Viewer.h"
#include "DrawableObject2D.h"
#include "BlendDialog.h"
#include "Util.h"
#include "SimulationManager.h"
#include "PhysicsObject3D.h"
#include "DefaultPickHandler.h"
#include "VertexSelectionHandler.h"



class MyViewerDeformation : public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewerDeformation(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f, true, false), // MyViewerDeformation inherits from PoLAR::Viewer
        // Help messageBox creation
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("I: edit image mode (pan/zoom)\n")+
           QString("M: add/edit markers mode (blobs)\n")+
           QString("W: manipulate scene mode\n")+
           QString("D: toggle background image display\n")+
           QString("G: grab a window snapshot\n")+
           QString("B: show blend dialog\n")+
           QString("P: print projection matrix in console\n")+
           QString("T: testkey\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this),
        _simMan(NULL),
        _paused(true)
    {
        //bgImageOn();
        // Add native OpenSceneGraph stats handler
        addEventHandler(new osgViewer::StatsHandler);
        /*PoLAR::VertexSelectionHandler* handler = new PoLAR::VertexSelectionHandler(true);
        handler->setCharacterSize(12);
        addEventHandler(handler);*/
        //addEventHandler(new PoLAR::DefaultPickHandler);

        mb.setWindowModality(Qt::NonModal);
    }



    void setSimMan(PoLAR::SimulationManager *simMan)
    {
        if(simMan)
        {
            _simMan = simMan;
        }
    }


public slots:
    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            mb.show(); // Show the help dialog
            break;
        case Qt::Key_Q:
            this->close();
            break;
        case Qt::Key_D:
        {
            bgImageToggle(); // Toggle display of the background image (here we can call directly PoLAR::ViewerStatic methods as MyViewer is a PoLAR::ViewerStatic)
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot(); // Set the image interaction active
            break;
        }
        case Qt::Key_M:
        {
            newMarkers(); // Add markers
            break;
        }
        case Qt::Key_W:
        {
            startManipulateSceneSlot();// Set the 3D interaction active
            break;
        }
        case Qt::Key_G:
        { // Grab an image
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");

            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }
        case Qt::Key_B:
        {
            newBlob();
            break;
        }
        case Qt::Key_P:
        {
            osg::Matrixd P = getProjection();
            std::cout << P << std::endl;
            if (!P.isNaN())
            {
                QString s=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the current projection matrix",
                                                        QString::null,
                                                        "Projection matrices (*.mat)");
                if (!s.isEmpty())
                {
                    char *out=new char[s.length()+1];
                    strcpy(out, s.toLatin1().data());
                    PoLAR::Util::saveProjectionMatrix(P, out);
                    delete []out;
                }
            }
            break;
        }
        case Qt::Key_T:
        {
            osg::ref_ptr<PoLAR::Object3D> o=getObject3D(0);
            removeObject3D(o.get());
            return;
            break;
        }
        case Qt::Key_F:
        {
            if(_simMan)
            {
                if(!_paused)
                {
                    _simMan->pause();
                    _paused = true;
                }
                else
                {
                    _simMan->play();
                    _paused = false;
                }
            }
            break;
        }
        case Qt::Key_X:
        {
            if(_simMan)
            {
                if(_simMan->isFramerateDisplayed())
                    _simMan->displayFramerate(false, this);
                else
                    _simMan->displayFramerate(true, this);
            }
            break;
        }

        case Qt::Key_L:
        {
            getBgImage()->resetWindowLevel();
            break;
        }
        case Qt::Key_R: // reset the simulation state of each object
        {
            std::list<osg::ref_ptr<PoLAR::Object3D> > list = getObjects3D();
            std::list<osg::ref_ptr<PoLAR::Object3D> >::iterator it;

            for(it=list.begin(); it!=list.end(); ++it)
            {
                PoLAR::PhysicsObject3D *phobj = dynamic_cast<PoLAR::PhysicsObject3D*>((it)->get());
                if(phobj)
                {
                    phobj->resetSimulation();
                }
            }
            break;
        }

        default: Viewer::keyPressEvent(event);
        }
    }
private:
    QMessageBox mb;
    PoLAR::SimulationManager *_simMan;
    bool _paused;
};

#endif // _MYVIEWER_
