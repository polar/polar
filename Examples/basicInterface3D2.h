/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef BASICINTERFACE3D2_H
#define BASICINTERFACE3D2_H

#include <QStringList>
#include <QMessageBox>
#include <QMainWindow>
#include <QToolBar>
#include <QToolButton>
#include <QSlider>
#include <QScrollBar>
#include <QInputDialog>
#include <QSpinBox>
#include <QLabel>
#include <QSplitter>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Viewer.h>
#include <PoLAR/SceneGraph.h>
#include <PoLAR/Object3DPickHandler.h>
#include <PoLAR/Object3D.h>
#include <PoLAR/Light.h>
#include <PoLAR/LightDialog.h>
#include <PoLAR/BasicLightDialog.h>
#include <PoLAR/ViewableCamera.h>
#include <PoLAR/FrameAxis.h>
#include <PoLAR/TabBoxPickHandler.h>
#include <PoLAR/TranslateAxisPickHandler.h>
#include <PoLAR/Image.h>
#include <QApplication>

using PoLAR::Marker2D;

class KeyPressManager : public QObject
{
	Q_OBJECT
public:
	KeyPressManager(QMainWindow *inter) :
		_interface(inter)
	{
	}

protected:
	bool eventFilter(QObject *obj, QEvent *event)
	{
		if (event->type() == QEvent::KeyPress)
		{
			QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
			qApp->notify(_interface, keyEvent);
			return false; // we return false in order to forward the event to the filtered viewer
		}
		else
		{
			// standard event processing
			return QObject::eventFilter(obj, event);
		}
	}

	QMainWindow *_interface;
};


class Interface : public QMainWindow
{
	Q_OBJECT

public:
	Interface();
	~Interface();

	void addImageLeft(PoLAR::BaseImage* image, int nima)
	{
		wL = image->getWidth(), hL = image->getHeight();
		dL = image->getDepth();
		nimaL = nima;
		ViewerL->resize(wL, hL);
        ViewerL->setBgImage(image);
		scrollbar->setMaximum(nimaL - 1);
		ViewerL->bgImageOn();
		ViewerL->startEditImageSlot();
		seqL = (float*)NULL;
	}

    void addImageRight(PoLAR::BaseImage* image, int nima)
    {
        wR = image->getWidth(), hR = image->getHeight();
        dR = image->getDepth();
        nimaR = nima;
        ViewerR->resize(wR, hR);
        ViewerR->setBgImage(image);
        ViewerR->bgImageOn();
        ViewerR->startEditImageSlot();
        seqR = (float*)NULL;
    }

	void addSequenceLeft(float* sequence, int w, int h, int d, int nima)
	{
		ViewerL->resize(w, h);
		ViewerL->setBgImage<float>(sequence, w, h, d, nima, true, true);
		seqL = sequence;
        wL = w, hL = h, dL = d, nimaL = nima;
		scrollbar->setMaximum(nimaL - 1);

		ViewerL->bgImageOn();
		ViewerL->startEditImageSlot();
	}

	void addSequenceRight(float* sequence, int w, int h, int d, int nima)
	{
		ViewerR->resize(w, h);
		ViewerR->setBgImage<float>(sequence, w, h, d, nima, true, true);
		seqR = sequence;
		wR = w, hR = h, dR = d, nimaR = nima;
		scrollbar->setMaximum(nimaR - 1);

        ViewerR->bgImageOn();
		ViewerR->startEditImageSlot();

	}

	void addNode(PoLAR::Object3D *object3D)
	{
        ViewerL->addObject3D(object3D);
        if(!object3D->isAPhantom()) ViewerR->addObject3D(object3D);
	}

	void setProjection(PoLAR::Viewer* Viewer, osg::Matrixd& P, PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION) { Viewer->setProjection(P, pt); }
	void setDefaultProjection(PoLAR::Viewer* Viewer) { Viewer->setProjection(); }
	void resizeWell() { resize(2 * width(), 2 * height()); }

	inline PoLAR::Viewer* getLeftViewer() { return ViewerL; }
	inline PoLAR::Viewer* getRightViewer() { return ViewerR; }


signals:
	void startEditImageSignal();
	void startEditMarkersSignal();

	private slots:
	void exit() { this->close(); }
	void activateLabelBox(Marker2D *m);
	void deactivateLabelBox();
	void changeTitle(int n);
	void newSpline();
    void saveMarkers();
	inline void removeCurrentObject3DPickHandler()
	{
        osgViewer::View::EventHandlers::iterator it = std::find(ViewerL->getEventHandlers().begin(), ViewerL->getEventHandlers().end(), _pick);
        if (it != ViewerL->getEventHandlers().end()) ViewerL->getEventHandlers().erase(it);
	}
	inline void translateaxisDragger() { _pick->cleanUp(ViewerL); removeCurrentObject3DPickHandler(); _pick = new PoLAR::TranslateAxisPickHandler; ViewerL->addEventHandler(_pick); }
	inline void tabboxDragger() { _pick->cleanUp(ViewerL); removeCurrentObject3DPickHandler(); _pick = new PoLAR::TabBoxPickHandler; ViewerL->addEventHandler(_pick); }
	void pickedObjectPhantom();
	void pickedObjectFrame();
	virtual void keyPressEvent(QKeyEvent *event);
	virtual void startEditImageSlot();
    virtual void changeMarkerLabelSlot(int l);
	virtual void gotoImageSlot(int i);
	virtual void startEditMarkersSlot();
	virtual void startManipulateSceneSlot();


private:
	QToolBar *toolbar, *scrollbarTB;
	QPixmap *quitPixmap, *imagePixmap, *markerPixmap, *savePixmap, *scenePixmap;
	QToolButton *quitButton, *imageButton, *markerButton, *saveButton, *editSceneButton;
	QSpinBox *labelBox;
	QScrollBar *scrollbar;
	QHBoxLayout *imglayout;
	QSplitter *imgframe;
	PoLAR::Viewer *ViewerL;
	PoLAR::Viewer *ViewerR;
	QLabel *statuslabel;
	QMessageBox *mb;
    osg::ref_ptr<PoLAR::Light> _light, _light2;
	float *seqL;
	float *seqR;
	int wR, hR, wL, hL, dR, dL, nimaL, nimaR;
	PoLAR::Object3D* _obj;
	PoLAR::Object3DPickHandler* _pick;
};

#endif // BASICINTERFACE3D2_H
