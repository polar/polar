1. Compiling the examples

    The PoLAR Examples can be compiled with PoLAR. To compile them, the CMake variable BUILD_PoLAR_EXAMPLES must be set to ON. Then re-running the cmake and make
    in PoLAR's dir will compile the examples.

2. Running the examples

   You can run the examples as follow :

   A basic image viewer :
   ./svisu <image>

   A basic image viewer with image distortion management :
   ./svisuDisto <image>

   An image, markers and 3D objects viewer with key bindings (including examples of PoLAR library use for OBJECT3D MANIPULATION):
   ./svisu3 <image> <projection file> <3D model> <type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]

   svisu3 with an overpainting thanks to Qt:
   ./svisu3bis <image> <projection file>  <3D model> <type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]

   A basic markers and 3D objects viewer with key bindings:
   ./svisu4 <3D model> [<projection file>] <type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]

   An example showing the possibility of double visualisation windows
   ./svisuDouble <image> <projection file> <3D model> <type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]

   A basic markers and sequence images viewer with key bindings (including some examples of PoLAR library use for OBJECT2D MANIPULATION):
   the image sequence should be a sequence from MRI. The offset is the size of the header of the file (before the beginning of the actual data).
   Data type = f (float), c (char), uc (unsigned char), i (int), ui (unsigned int), d (double), s (short)
   ./visuseq <data> <width> <height> <nima> <header offset> <pixel type> [<proj> <off> <projType> <savename>]
   
   Same as visuseq but with full access to all the 2D objects
   ./demo2D <data> <width> <height> <nima> <header offset> <pixel type> [<proj> <off> <projType> <savename>]

   visuseq with the sequence mapped on a 3D quad which position is coherent with the corresponding data volume
   ./visuseq3D <image sequence> <3D model>

    An example of using custom dialog boxes in which a colored threshold can be applied on an image
    ./demoThreshold <image> [<file.grx>]
   
   An example with the camera images flow (type up arrow to launch the images flow treatment, and then type 'd' to display the flow)
   ./svisucam
   
   An example displaying a video (type up arrow to launch the images flow treatment, and then type 'd' to display the flow. Down arrow once to pause, twice to stop)
   ./svisuvideo <video file>

   svisucam with the camera images flow mapped on a 3D quad
   ./svisucam3D

   An example with a Qt interface to show simple manipulation (edition) of a 3D object
   ./testvisucolor3D <image> <projection file> <3D model> [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]

   An example of double windows with a Qt interface
   This more advanced example is to show another possibility of the library,
   The interest here, is that you can visualize in the right window the camera movement according to the manipulation made in the left window.
   ./testvisucolor3D2 <image> <projection file> <3D model> [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]

   An example to show picking and interactive manipulation (edition) of a 3D object
   ./testpick <image> <projection file> <3D model> [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]
   
   Bullet example
   ./physicsExample <background image> <deformable 3D object> <solid 3D object> <flag texture> <projection file>
   
   Vega example
   ./deformationVega <.veg> <.obj>

   
3. Interaction

   Image interaction : Mouse LeftButton : to pan the image. 
		       Mouse MiddleButton : to change the window level
		       Mouse RightButton : to zoom the image.

   2D objects interaction : Mouse LeftButton : to select and move a marker for the active object.
			    Mouse MiddleButton : to create or select (no motion) a marker.
			    Mouse RightButton : to remove a marker.
			    Shift + Mouse LeftButton : to select (activate) an object among all the displayed objects.

   3D objects interaction :
 
			in scene graph/camera manipulation mode (available by pressing key 0) :
				Mouse LeftButton : to rotate around a center defined by the user.
			    	Mouse MiddleButton : to do a 3D translation parallel to the image plane.
			    	Mouse RightButton : to do a 3D translation orthogonal to the image plane.
			    	Spacebar : to reset the view at the initial extrinsics.

			in object3D edition mode (available by pressing key 1, only if an Object3DPickHandler is set to the viewer) :
				See Object3DPickHandler class doxygen corresponding file for more details or file Object3DPickHandler.h

			in object3D geometry edition mode (available by pressing key 2, only if an GeometryPickHandler is set to the viewer) :
				See GeometryPickHandler class doxygen corresponding file for more details or file GeometryPickHandler.h 


   KeyBindings : Press 'H' to have them displayed.
		 Press 'S' to have the frame rate displayed (toggle key).

		 
		 
4. Examples with the provided assets. These commands have to be run from a "build" directory

   A basic image viewer :
   bin/svisu ../Examples/data/example1.ppm

   A basic image viewer with image distortion management :
   bin/svisuDisto ../Examples/data/example1.ppm

   An image, markers and 3D objects viewer with key bindings (including examples of PoLAR library use for OBJECT3D MANIPULATION):
   bin/svisu3 ../Examples/data/example1.ppm ../Examples/data/example1.proj ../Examples/data/armchair.obj

   svisu3 with an overpainting thanks to Qt:
   bin/svisu3bis ../Examples/data/example1.ppm ../Examples/data/example1.proj ../Examples/data/armchair.obj

   A basic markers and 3D objects viewer with key bindings:
   bin/svisu4 ../Examples/data/armchair.obj [../Examples/data/example1.proj]
   
   An example showing the possibility of double visualisation windows
   bin/svisuDouble ../Examples/data/example1.ppm ../Examples/data/example1.proj ../Examples/data/armchair.obj

   A basic markers and sequence images viewer with key bindings (including examples of PoLAR library use for OBJECT2D MANIPULATION):
   bin/visuseq ../Examples/data/ou.short 256 256 31 0 s
   
   Same as visuseq but with full access to all the 2D objects and zoom made by scrolling
   bin/demo2D ../Examples/data/ou.short 256 256 31 0 s
   bin/demo2D ../Examples/data/example1.ppm

   visuseq with the sequence mapped on a 3D quad which position is coherent with the corresponding data volume
   bin/visuseq3D ../Examples/data/ou.short ../Examples/data/ou.off


   An example of using custom dialog boxes in which a colored threshold can be applied on an image
   bin/demoThreshold ../Examples/data/example1.ppm
   
   An example with the camera images flow (type up arrow to launch the images flow treatment, and then type 'd' to display the flow)
   bin/svisucam
   
   An example displaying a video (type up arrow to launch the images flow treatment, and then type 'd' to display the flow. Down arrow once to pause, twice to stop)
   The video file is not provided in the assets.
   bin/svisuvideo video.mp4

   svisucam with the camera images flow mapped on a 3D quad
   bin/svisucam3D

   An example with a Qt interface to show simple manipulation (edition) of a 3D object
   bin/testvisucolor3D ../Examples/data/example1.ppm ../Examples/data/example1.proj ../Examples/data/armchair.obj

   An example of double windows with a Qt interface
   This more advanced example is to show another possibility of the library,
   The interest here, is that you can visualize in the right window the camera movement according to the manipulation made in the left window.
   bin/testvisucolor3D2 ../Examples/data/example1.ppm ../Examples/data/example1.proj ../Examples/data/armchair.obj

   An example to show picking and interactive manipulation (edition) of a 3D object
   bin/testpick ../Examples/data/example1.ppm ../Examples/data/example1.proj ../Examples/data/armchair.obj
   
   An example of animated character 
   bin/animation ../Examples/data/example2.png ../Examples/data/example2.proj ../Examples/data/anim.osgt
   
   Bullet example
   bin/demoBullet ../Examples/data/example1.ppm ../Examples/data/sphere.obj ../Examples/data/cube.off ../Examples/data/inria.jpg ../Examples/data/test2.mat 
   
   Vega example
   bin/demoVega ~/svn/Bibliotheque/data/lung/lung.veg ~/svn/Bibliotheque/data/lung/lung.obj

   
