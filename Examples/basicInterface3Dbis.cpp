/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/


#include <QColorDialog>
#include "basicInterface3Dbis.h"
#include "props.h"
//#include "EpipolarPickHandler.h"

using namespace PoLAR;

Interface::Interface(): QMainWindow()
{
    QString dir = dataPath() + "icons/";

    // Create a Viewer with shadows management
    viewer = new PoLAR::Viewer(this, "viewer", 0, true);
    viewer->addEventHandler(new osgViewer::StatsHandler);// add stats handler

    _pick = new PoLAR::TabBoxPickHandler; // create pick handler
    _pick->setCharacterSize(15);
    viewer->addEventHandler(_pick);// add pick handler

    PoLAR::DefaultPickHandler *geomPick = new PoLAR::DefaultPickHandler;
    //PoLAR::EpipolarPickHandler *geomPick = new PoLAR::EpipolarPickHandler;
    geomPick->setCharacterSize(15);
    viewer->addEventHandler(geomPick);// add geometry pick handler
    setCentralWidget(viewer);
    toolbar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea, toolbar);
    toolbar->setOrientation(Qt::Vertical);

    statuslabel = new QLabel(toolbar);
    statuslabel->setText(*(new QString("Tools")));

    toolbar->addAction(QIcon(dir+"exit.png"), QString("Quit"), this, SLOT(exit()));

    QAction *editImgAct = new QAction(QIcon(dir+"img.png"), tr("Edit image"), this);
    editImgAct->setStatusTip(tr("Edit Image"));
    QObject::connect(editImgAct, SIGNAL(triggered()), viewer, SLOT(startEditImageSlot()));
    QObject::connect(editImgAct, SIGNAL(triggered()), this, SLOT(deactivateLabelBox()));
    toolbar->addAction(editImgAct);

    toolbar->addAction(QIcon(dir+"edit.png"), QString("Edit Markers"), this, SLOT(newSpline()));

    toolbar->addAction(QIcon(dir+"save.png"), QString("Save Markers"), this, SLOT(saveMarkers()));

    labelBox = new QSpinBox(this);
    labelBox->setRange(-1,19);
    labelBox->setSingleStep(1);
    toolbar->addWidget(labelBox);

    deactivateLabelBox();
    QObject::connect(labelBox, SIGNAL(valueChanged(int)), viewer, SLOT(changeMarkerLabelSlot(int)));
    QObject::connect(viewer, SIGNAL(selectMarkerSignal(Marker2D*)), this, SLOT(activateLabelBox(Marker2D*)));
    QObject::connect(viewer, SIGNAL(unselectMarkerSignal()), this, SLOT(deactivateLabelBox()));

    scrollbarTB = new QToolBar(this);
    scrollbarTB->setOrientation(Qt::Vertical);

    scrollbar = new QScrollBar(this);
    scrollbar->setMinimum(0);
    scrollbar->setMaximum(0);
    scrollbar->setPageStep(10);
    scrollbar->setValue(0);
    scrollbar->setFixedHeight(height());
    scrollbar->setOrientation(Qt::Vertical);
    scrollbarTB->addWidget(scrollbar);


    QObject::connect(scrollbar, SIGNAL(valueChanged(int)), viewer, SLOT(gotoImageSlot(int)));
    QObject::connect(scrollbar, SIGNAL(valueChanged(int)), this, SLOT(changeTitle(int)));

    scrollbarTB->update();
    addToolBar(Qt::RightToolBarArea, scrollbarTB);

    move(0,0);

    QObject::connect(this, SIGNAL(startEditImageSignal()), viewer, SLOT(startEditImageSlot()));
    QObject::connect(this, SIGNAL(startEditMarkersSignal()), viewer, SLOT(startEditMarkersSlot()));

    toolbar->addAction(QIcon(dir+"scene.png"), QString("Manipulate Scene"), viewer, SLOT(startManipulateSceneSlot()));

    // get the default headlight already attached to scene graph
    PoLAR::Light *light = getViewer()->getLightSource(0);

    // Manage the on/off state of the headlight
    QCheckBox *lightCheck = new QCheckBox(QString("On/Off"), this);
    toolbar->addWidget(lightCheck);
    lightCheck->setChecked(true);
    QObject::connect(lightCheck, SIGNAL(stateChanged(int)), light, SLOT(onOffSlot(int)));

    // some green light
    osg::ref_ptr<PoLAR::Light> light3 = new PoLAR::Light(osg::Vec4(1.0, 1.0, 1.0, 1.0), osg::Vec4(0.1, 0.1, 0.1, 1.0),  osg::Vec4(0.0, 0.5, 0.0, 1.0), osg::Vec4(0.0,0.2,0.0,1.0));
   // light3->setSpotCutoff(45.0f);
   // light3->setDirection(0.f, 0.f, 1.f);
   // light3->setSpotExponent(0.8);
    getViewer()->addLightSource(light3.get());

    // create a PoLAR::Light corresponding to a world light positionned in (5.0,2.0,5.0)
    PoLAR::Light *light2 = getViewer()->addLightSource(5.0, 2.0, 5.0, true, false);

    // connect it to the custom PoLAR::LightDialog to be able to manage its parameters
    lightDialog = new PoLAR::LightDialog(viewer, light2, this);
    QPushButton *moreButton2 = new QPushButton(QString("Worldlight parameters"), this);
    toolbar->addWidget(moreButton2);
    QObject::connect(moreButton2, SIGNAL(pressed()), lightDialog, SLOT(show()));

    QPushButton *moreButton3 = new QPushButton(QString("Set the current picked object in red"), this);
    toolbar->addWidget(moreButton3);

    QObject::connect(moreButton3, SIGNAL(pressed()), this, SLOT(pickedObjectInRed()));

    QPushButton *moreButton4 = new QPushButton(QString("Change the frame in which the current \n picked object is edited \n (default is worldframe)"), this);
    toolbar->addWidget(moreButton4);

    QObject::connect(moreButton4, SIGNAL(pressed()), this, SLOT(pickedObjectFrame()));

    QPushButton *moreButton5 = new QPushButton(QString("Assign a transformation\n to the current picked"), this);
    toolbar->addWidget(moreButton5);

    QObject::connect(moreButton5, SIGNAL(pressed()), this, SLOT(pickedObjectTransformation()));

    QPushButton *moreButton6 = new QPushButton(QString("Reset the transformations assigned  \n to the current picked \n (only the transformations made \n since the last frame change are undone)"), this);
    toolbar->addWidget(moreButton6);

    QObject::connect(moreButton6, SIGNAL(pressed()), this, SLOT(pickedObjectReset()));

    // Combo Box to permit to choose the type of dragger to use for object edition
    draggerCombo = new QComboBox(this);
    draggerCombo->addItem("TabBox dragger");
    draggerCombo->addItem("Trackball dragger");
    draggerCombo->addItem("TranslateAxis dragger");
    draggerCombo->addItem("ScaleAxis dragger");
    draggerCombo->addItem("RotateSphere dragger");
    toolbar->addWidget(draggerCombo);

    QObject::connect(draggerCombo, SIGNAL(activated(int)), this, SLOT(draggerComboSlot(int)));

    // add focus to this interface in order to get the keyboard events
    this->setFocusPolicy(Qt::StrongFocus);
    // remove the focus from the Viewer, otherwise it will bypass the interface's events
    viewer->setFocusPolicy(Qt::NoFocus);

    mb = new QMessageBox(
                QMessageBox::Information,
                QString("Help: shortkeys"),
                QString("Shortkeys:\n")+
                QString("H: this help\n")+
                QString("Q: quit the application\n")+
                QString("I: edit image mode (pan/zoom/window level)\n")+
                QString("M: add/edit markers mode (spline)\n")+
                QString("W: manipulate scene mode\n")+
                QString("D: toggle background image display\n")+
                QString("G: grab a window snapshot\n")+
                QString("0: switch in camera manipulation mode, only active in 3D manipulation mode (default)\n")+
                QString("1: switch in picking mode, only active in 3D manipulation mode\n")+
                QString("2: switch in geometry picking mode, only active in 3D manipulation mode\n")+
                QString("F1: use TABBOX dragger\n")+
                QString("F2: use TRACKBALL dragger\n")+
                QString("F3: use TRANSLATEAXIS dragger\n")+
                QString("F4: use SCALEAXIS dragger\n")+
                QString("F5: use ROTATESPHERE dragger\n")+
                QString("F9: Set picked object as shadow receiver\n")+
                QString("F10: Set picked object as non shadow receiver\n")+
                QString("F11: Set picked object as shadow caster\n")+
                QString("F12: Set picked object as non shadow caster\n")+
                QString("C: set the current picked object in red\n")+
                QString("F: set frame edition to a custom frame for the current picked object\n")+
                QString("T: set a transformation to the current picked object\n")+
                QString("R: reset the current picked object's transformations made in the current frame \n")+
                QString("else default openscenegraph keys\n"),
                QMessageBox::NoButton,
                this);
    mb->setWindowModality(Qt::NonModal);
}

Interface::~Interface()
{
    delete lightDialog;
    delete mb;
}

void Interface::activateLabelBox(Marker2D *m)
{
    labelBox->setEnabled(true);
    if (m) labelBox->setValue((int)m->label());
}

void Interface::deactivateLabelBox()
{
    labelBox->setValue(-1);
    labelBox->setEnabled(false);
}

void Interface::changeTitle(int n)
{
    char buf[256];
    sprintf(buf, "Image %3d",n);
    statuslabel->setText(*(new QString(buf)));
}

// Add a new DrawableSpline2D to the viewer
void Interface::newSpline()
{
    viewer->newSpline();
    // change display properties of the newly added DrawableObject2D
    viewer->getCurrentObject2D()->setProperties(QColor("black"), QColor("red"),
                                                QColor("yellow"),
                                                PoLAR::DrawableObject2D::NONE,
                                                PoLAR::DrawableObject2D::CIRCLE,
                                                PoLAR::DrawableObject2D::SQUARE,
                                                5, 10, 2);
}

void Interface::saveMarkers()
{
    QString fname = QFileDialog::getSaveFileName( this,
                                                  "Choose a filename to save the markers",
                                                  QString::null);
    if (!fname.isEmpty()) viewer->saveObject2D(fname);

}

// Set the picked object to be a shadow receiver
void Interface::pickedObjectReceiveShadOn()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->setShadowReceiverOn();
}

// Set the picked object not to be a shadow receiver
void Interface::pickedObjectReceiveShadOff()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->setShadowReceiverOff();
}

// Set the picked object to be a shadow caster
void Interface::pickedObjectCastShadOn()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->setShadowCasterOn();
}

// Set the picked object not to be a shadow caster
void Interface::pickedObjectCastShadOff()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->setShadowCasterOff();
}

// Change the color of the picked object
void Interface::pickedObjectInRed()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->setAmbientColor(1.0,0.0,0.0);
}

// Change the frame in which the picked object will be edited
void Interface::pickedObjectFrame()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->setFrameMatrix(osg::Matrixd::rotate(M_PI/5, 0.0,1.0,0.0));
}

// Assign a transformation to the picked object
void Interface::pickedObjectTransformation()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->setTransformationMatrix(osg::Matrixd::rotate(M_PI/5, 0.0,1.0,0.0));
}

// Reset the the picked object's transformations made in the current frame
void Interface::pickedObjectReset()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid()) obj->resetAllManipulations();
}

// Phantomize the picked object
void Interface::pickedObjectPhantom()
{
    osg::ref_ptr<PoLAR::Object3D> obj = viewer->getCurrentPickedObject();
    if (obj.valid())
    {
        if (obj->isAPhantom()) obj->setPhantomOff();
        else obj->setPhantomOn();
    }
}

void Interface::draggerComboSlot(int index)
{
    switch (index)
    {
    case 0 : tabboxDragger(); break;
    case 1 : trackballDragger(); break;
    case 2 : translateaxisDragger(); break;
    case 3 : scaleaxisDragger(); break;
    case 4 : rotatesphereDragger(); break;
    default : tabboxDragger(); break;
    }
}


// Add key bindings
void Interface::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Q: this->exit(); break;
    case Qt::Key_H:
    {
        // Show the help message box
        mb->show();
        break;
    }
    case Qt::Key_A:
    {
        PoLAR::Light* light = viewer->getLightSource(1);
        light->setDirection(light->getDirection() + osg::Vec3(0.1,0.1,0.1));
        break;
    }
    case Qt::Key_D:
    {
        // Hide/display background image
        viewer->bgImageToggle();
        break;
    }
    case Qt::Key_I:
    {
        // Set the image interaction to be active
        viewer->startEditImageSlot();
        break;
    }
    case Qt::Key_M:
    {
        newSpline();
        break;
    }
    case Qt::Key_W:
    {
        // Set the scene graph interaction to be active
        viewer->startManipulateSceneSlot();
        break;
    }
    case Qt::Key_F:
    {
        pickedObjectFrame();
        break;
    }
    case Qt::Key_P:
    {
        pickedObjectPhantom();
        break;
    }
    case Qt::Key_T:
    {
        pickedObjectTransformation();
        break;
    }
    case Qt::Key_C:
    {
        pickedObjectInRed();
        break;
    }
    case Qt::Key_R:
    {
        pickedObjectReset();
        break;
    }
    case Qt::Key_F1:
    {
        tabboxDragger();
        break;
    }
    case Qt::Key_F2:
    {
        trackballDragger();
        break;
    }
    case Qt::Key_F3:
    {
        translateaxisDragger();
        break;
    }
    case Qt::Key_F4:
    {
        scaleaxisDragger();
        break;
    }
    case Qt::Key_F5:
    {
        rotatesphereDragger();
        break;
    }
    case Qt::Key_F9:
    {
        pickedObjectReceiveShadOn();
        break;
    }
    case Qt::Key_F10:
    {
        pickedObjectReceiveShadOff();
        break;
    }
    case Qt::Key_F11:
    {
        pickedObjectCastShadOn();
        break;
    }
    case Qt::Key_F12:
    {
        pickedObjectCastShadOff();
        break;
    }
    case Qt::Key_G:
    {
        QString fname=QFileDialog::getSaveFileName( this,
                                                    "Choose a filename to save the snapshot image",
                                                    QString::null,
                                                    "PNG images (*.png)");
        if (!fname.isEmpty()) viewer->grab().save(fname, "PNG");

        break;
    }
    case Qt::Key_E:
    {
        PoLAR::Light* light = getViewer()->getLightSource(1);
        getViewer()->removeLightSource(light);
    }

    default:
        viewer->notify(event);
    }
}

void Interface::keyReleaseEvent(QKeyEvent *event)
{
    //simply forwarding to the viewer
    viewer->notify(event);
}
