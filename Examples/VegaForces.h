#include "VegaObject3D.h"

using namespace std;

class TestForce: public PoLAR::VegaInteraction
{
public:
    TestForce(unsigned int index):
        startingIndex(index)
    {}

    ~TestForce()
    {
        free(u);
        free(f_ext);
        free(f_extBase);
    }

    void initializeForce(PoLAR::VegaObject3D *object)
    {
      /*        x = object->getVolumetricMesh()->getVertex(startingIndex)[0][0];
        y = object->getVolumetricMesh()->getVertex(startingIndex)[0][1];
        z = object->getVolumetricMesh()->getVertex(startingIndex)[0][2];*/
        x = object->getVolumetricMesh()->getVertex(startingIndex)[0];
        y = object->getVolumetricMesh()->getVertex(startingIndex)[1];
        z = object->getVolumetricMesh()->getVertex(startingIndex)[2];
        x_position = x;
        cout << "Initial point to deform: " << x << " " << y << " " << z << endl;
        numVertices = object->getVolumetricMesh()->getNumVertices();
        int r = 3 * numVertices;
        u = (double*) calloc (r, sizeof(double));
        f_extBase  = (double*) calloc (r, sizeof(double));
        f_ext = (double*) calloc (r, sizeof(double));
    }


    double* applyForce(PoLAR::VegaObject3D *object)
    {
        int r = 3 * numVertices;
        memcpy(u, object->getIntegrator()->Getq(), sizeof(double) * r);

        // reset external forces (usually to zero)
        memcpy(f_ext, f_extBase, sizeof(double) * r);

        x_position+=2;

        int pulledVertex = startingIndex;
        int forceNeighborhoodSize = 3;
        if (x_position<-100)
        {
            //spring stiffness
            int k=15;
            //spring force
	    //            double currentXPos = object->getVolumetricMesh()->getVertex(pulledVertex)[0][0] + u[3*pulledVertex+0];
            double currentXPos = object->getVolumetricMesh()->getVertex(pulledVertex)[0] + u[3*pulledVertex+0];
            double forceX = k*(x_position - currentXPos);

            cout << x_position << " / " << currentXPos << "->"<< forceX << endl;

            // 3D force
            double externalForce[3] = {forceX,0,0};
            int deformableObjectCompliance = 100000;
            // apply a compliance on the 3D force
            for(int j=0; j<3; j++)
                externalForce[j] *= deformableObjectCompliance;

            // register force on the pulled vertex
            f_ext[3*pulledVertex+0] += externalForce[0];
            f_ext[3*pulledVertex+1] += externalForce[1];
            f_ext[3*pulledVertex+2] += externalForce[2];

            // distribute force over the neighboring vertices
            set<int> affectedVertices;
            set<int> lastLayerVertices;

            affectedVertices.insert(pulledVertex);
            lastLayerVertices.insert(pulledVertex);

            for(int j=1; j<forceNeighborhoodSize; j++)
            {
                // linear kernel
                double forceMagnitude = 1.0 * (forceNeighborhoodSize - j) / forceNeighborhoodSize;

                set<int> newAffectedVertices;
                for(set<int> :: iterator iter = lastLayerVertices.begin(); iter != lastLayerVertices.end(); iter++)
                {
                    // traverse all neighbors and check if they were already previously inserted
                    int vtx = *iter;
                    int deg = object->getMeshGraph()->GetNumNeighbors(vtx);
                    for(int k=0; k<deg; k++)
                    {
                        int vtxNeighbor = object->getMeshGraph()->GetNeighbor(vtx, k);

                        if (affectedVertices.find(vtxNeighbor) == affectedVertices.end())
                        {
                            // discovered new vertex
                            newAffectedVertices.insert(vtxNeighbor);
                        }
                    }//cout << "vertex found: "<<deg<<endl;
                }

                lastLayerVertices.clear();
                for(set<int>::iterator iter = newAffectedVertices.begin(); iter != newAffectedVertices.end(); iter++)
                {
                    // apply force
                    f_ext[3* *iter + 0] += forceMagnitude * externalForce[0];
                    f_ext[3* *iter + 1] += forceMagnitude * externalForce[1];
                    f_ext[3* *iter + 2] += forceMagnitude * externalForce[2];

                    // generate new layers
                    lastLayerVertices.insert(*iter);
                    affectedVertices.insert(*iter);
                }
            }

        }

        return f_ext;
    }

    void resetForce(PoLAR::VegaObject3D *)
    {
        free(u);
        free(f_ext);
        free(f_extBase);
        int r = 3 * numVertices;
        u = (double*) calloc (r, sizeof(double));
        f_extBase  = (double*) calloc (r, sizeof(double));
        f_ext = (double*) calloc (r, sizeof(double));
        x_position = x;
    }

protected:
    int x_position;
    int x, y, z;
    unsigned int numVertices;
    unsigned int startingIndex;
    double *u;
    double *f_extBase;
    double *f_ext;

};

class DraggedVertexForce: public QObject, public PoLAR::VegaInteraction
{
    Q_OBJECT

public:

    DraggedVertexForce():
        QObject(),
        _forceX(0.0f),
        _forceY(0.0f),
        _forceZ(0.0f),
        _isClicked(false),
        _deformableObjectCompliance(1.0f),
        _forceNeighborhoodSize(5)
    {}

    void initializeForce(PoLAR::VegaObject3D *object)
    {
        numVertices = object->getVolumetricMesh()->getNumVertices();
        int r = 3 * numVertices;
        f_ext = (double*) calloc (r, sizeof(double));
        f_extBase  = (double*) calloc (r, sizeof(double));
    }

    ~DraggedVertexForce()
    {
        free(f_ext);
        free(f_extBase);
    }

    double* applyForce(PoLAR::VegaObject3D *object)
    {
        if(_isClicked)
        {
            int r = 3 * numVertices;

            // reset external forces (usually to zero)
            memcpy(f_ext, f_extBase, sizeof(double) * r);

            int pulledVertex = object->getVolumetricMesh()->getClosestVertex(_pulledVertex);

            std::cout << "pulled vertex" << pulledVertex << std::endl;

            double externalForce[3];
            externalForce[0] = _forceX * _deformableObjectCompliance;
            externalForce[1] = _forceY * _deformableObjectCompliance;
            externalForce[2] = _forceZ * _deformableObjectCompliance;


            // register force on the pulled vertex
            f_ext[3*pulledVertex+0] += externalForce[0];
            f_ext[3*pulledVertex+1] += externalForce[1];
            f_ext[3*pulledVertex+2] += externalForce[2];

            std::cout << "force " << externalForce[0] << " " << externalForce[1] << " " << externalForce[2] << std::endl;

            // distribute force over the neighboring vertices
            set<int> affectedVertices;
            set<int> lastLayerVertices;

            affectedVertices.insert(pulledVertex);
            lastLayerVertices.insert(pulledVertex);

            for(int j=1; j<_forceNeighborhoodSize; j++)
            {
                // linear kernel
                double forceMagnitude = 1.0 * (_forceNeighborhoodSize - j) / _forceNeighborhoodSize;

                set<int> newAffectedVertices;
                for(set<int> :: iterator iter = lastLayerVertices.begin(); iter != lastLayerVertices.end(); iter++)
                {
                    // traverse all neighbors and check if they were already previously inserted
                    int vtx = *iter;
                    int deg = object->getMeshGraph()->GetNumNeighbors(vtx);
                    for(int k=0; k<deg; k++)
                    {
                        int vtxNeighbor = object->getMeshGraph()->GetNeighbor(vtx, k);

                        if (affectedVertices.find(vtxNeighbor) == affectedVertices.end())
                        {
                            // discovered new vertex
                            newAffectedVertices.insert(vtxNeighbor);
                        }
                    }
                }

                lastLayerVertices.clear();
                for(set<int> :: iterator iter = newAffectedVertices.begin(); iter != newAffectedVertices.end(); iter++)
                {
                    // apply force
                    f_ext[3* (*iter) + 0] += forceMagnitude * externalForce[0];
                    f_ext[3* (*iter) + 1] += forceMagnitude * externalForce[1];
                    f_ext[3* (*iter) + 2] += forceMagnitude * externalForce[2];

//                    std::cout << "iter " << (*iter) << std::endl;
//                    f_ext[3* (*iter) + 0] += 1000;
//                    f_ext[3* (*iter) + 1] += 1000;
//                    f_ext[3* (*iter) + 2] += 0;

                    // generate new layers
                    lastLayerVertices.insert(*iter);
                    affectedVertices.insert(*iter);
                }
            }
        }
        return f_ext;
    }

    void resetForce(PoLAR::VegaObject3D *)
    {

    }

public slots:
    void push(float mouseX, float mouseY, osg::Vec3f vertex)
    {
        _mouseX = mouseX;
        _mouseY = mouseY;
        _pulledVertex = Vec3d(vertex[0], vertex[1], vertex[2]);
        std::cout << _mouseX << " " << _mouseY << " " << vertex << std::endl;
        _isClicked = true;
    }

    /* void drag(float deltaX, float deltaY)
    {
        std::cout << deltaX << " " << deltaY << std::endl;
        _forceX = deltaX;
        _forceY = deltaY;
    }*/

    void drag(float x, float y, float z)
    {
        _forceX = x;
        _forceY = y;
        _forceZ = z;
    }

    void release()
    {
        _isClicked = false;
    }


protected:

    float _mouseX;
    float _mouseY;
    float _forceX, _forceY, _forceZ;
    Vec3d _pulledVertex;
    unsigned int numVertices;
    bool _isClicked;
    float _deformableObjectCompliance;
    int _forceNeighborhoodSize;
    double *f_ext;
    double *f_extBase;
};
