/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>

#include "basicInterface3D.h"
#include <PoLAR/Image.h>
#include <PoLAR/Util.h>

#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/ColorMask>
#include <osg/PolygonMode>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/Texture2D>
#include <iostream>


int main(int argc,char ** argv)
{
    QApplication app(argc,argv);
    Interface inter;
    osg::ref_ptr<PoLAR::Image_uc> image;

    // Read the image given in parameter
    if (argc >1)
    {
        if(PoLAR::Util::fileExists(argv[1]))
            image = new PoLAR::Image_uc(argv[1], true);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }

        // Add the image read as background image
        inter.addImage(image.get(), 1);

        PoLAR::Viewer2D3D::ProjectionType pt=PoLAR::Viewer2D3D::VISION;
        if (argc > 4)
        {
            switch(atoi(argv[4]))
            {
            case 1: pt=PoLAR::Viewer2D3D::ANGIO; break;
            case 2: pt=PoLAR::Viewer2D3D::ANGIOHINV; break;
            default: pt=PoLAR::Viewer2D3D::VISION;
            }
        }

        // Load the projection given in parameter
        if (argc > 2)
        {
            osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[2]);
            inter.setProjection(P, pt);
        }

        { // A sphere
            osg::ref_ptr<osg::Sphere> unitSphere = new osg::Sphere(osg::Vec3(1.0, 1.0, 0.3), 0.2);
            osg::ref_ptr<osg::ShapeDrawable> unitSphereDrawable = new osg::ShapeDrawable(unitSphere);
            osg::ref_ptr<osg::Geode> sphereGeode = new osg::Geode();
            sphereGeode->addDrawable(unitSphereDrawable);

            // create an Object3D with a default material (which is non editable and non pickable)
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(sphereGeode, true);
            object3D->setName("Sphere");
            object3D->setAmbientColor(0.0,0.5,1.0);
            inter.addNode(object3D, true);
        }

        // Load the model given in parameter
        if (argc > 3)
        {
            if(!PoLAR::Util::fileExists(argv[3]))
            {
                std::cerr << "Unable to load model" << std::endl;
                exit(0);
            }
            else
            {
                // create an Object3D with a default material, which editable and pickable
                osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(argv[3], true, true, true);
                osgUtil::SmoothingVisitor smooth;
                osgUtil::Optimizer optimizer;
                object3D->accept(smooth);
                optimizer.optimize(object3D.get());
                object3D->setName("Loaded model");
                osg::Matrixd m = osg::Matrixd::rotate(M_PI/3,0.0,0.0,1.0) * osg::Matrixd::translate(0.5,0.0,0.005); // initialisation of matrix m with a custom tranformation
                object3D->setTransformationMatrix(m);
                inter.addNode(object3D, false, true, true);
                inter.getViewer()->setTrackNode(object3D.get());
            }
            // create a ground
            {
                osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
                osg::ref_ptr<osg::ShapeDrawable> shape;
                shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 5.0f, 5.0f, 0.00001f));
                floorGeode->addDrawable(shape.get());
                osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get());
                object3D->setName("Floor");
                object3D->setEditOn();
                object3D->setPhantomOn();
                inter.addNode(object3D.get());
            }
        }

        inter.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image> <projection> [<3D model>] [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]" <<std::endl;
        exit(0);
    }

    return app.exec();
}
