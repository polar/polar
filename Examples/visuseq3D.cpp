/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <iostream>

#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <PoLAR/Object3D.h>
#include <PoLAR/Image.h>
#include "MyViewer2.h"
#include "ImageSequence.h"


int main(int argc,char ** argv)
{
    if (argc < 3)
    {
        std::cerr << "Syntax: " << argv[0] << " <data> <off>\n";
        exit(0);
    }
    QApplication app(argc, argv);

    // data for OU MRI (ASPI data)
    int width=256;
    int height=256;
    int nima=31;
    int offset=0;
    std::string f = "s";
    float *sequence = readData(argv[1],width,height,nima,offset,f.c_str());


    // scene creation
    MyViewer2 viewer;
    viewer.resize(width,height);

    viewer.setBgImage<unsigned char>(NULL, width, height, 1, 1, true); // optional, is made per default in the library, but to redo this here permits a better windows adjustement for the visualisation

    osg::ref_ptr< PoLAR::Image<float> > img = new PoLAR::Image<float>(sequence, width, height, 1, nima) ;

    viewer.setImage(img.get(), 0.625, 0.625);
    viewer.startEditImageSlot();

    // load the model
    osg::ref_ptr<osg::Node> loadedModel;
    loadedModel = osgDB::readNodeFile(argv[2]);
    osgUtil::SmoothingVisitor smooth;
    osgUtil::Optimizer optimizer;

    if(!loadedModel)
    {
        std::cerr << "Unable to load model" << std::endl;
        exit(0);
    }

    optimizer.optimize(loadedModel.get());
    loadedModel->accept(smooth);

    PoLAR::Object3D *object3D = new PoLAR::Object3D(loadedModel.get(), true, true);
    osg::Matrixd s = osg::Matrixd::scale(0.5,0.5,0.5);
    object3D->setTransformationMatrix(s);
    // Set the object to be transparent
    object3D->setTransparencyOn();
    // Set the transparency degree
    object3D->updateTransparency(0.6);

    // set visual custom parameters
    object3D->setShininess(0.1);
    object3D->setAmbientColor(1,1,1);
    object3D->setDiffuseColor(0.7, 0.2, 0.2);
    object3D->setSpecularColor(0,0,0);
    viewer.addObject3D(object3D);

    viewer.changeIndexSlot(0);

    // add a zoom on the scene
    viewer.resize(512,512);
    viewer.setZoom(2);

    viewer.center();
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
