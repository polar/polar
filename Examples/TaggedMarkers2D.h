/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef TAGGED_MARKERS_H
#define TAGGED_MARKERS_H

#include <PoLAR/Marker2D.h>
#include <PoLAR/Markers2D.h>
#include <PoLAR/DrawableMarkers2D.h>
#include <PoLAR/Viewer.h>
#include <list>
#include <QString>

using namespace PoLAR;

/**
 * Custom class extending the PoLAR::Markers2D class
 *
 */
class TaggedMarkers2D: public Markers2D
{
public:
    /**
     * Default constructor
     */
    TaggedMarkers2D():
        Markers2D() {}

    /**
     * Constructor from a list of control points.
     * Used for loading from a file
     */
    TaggedMarkers2D(std::list<Marker2D*> list):
        Markers2D(list)
    {
       updateIndices();
    }

    /**
     * Copy constructor
     */
    TaggedMarkers2D(TaggedMarkers2D &obj):
        Markers2D(obj)
    {
        std::list<Marker2D*>::iterator mine = getMarkers().begin();
        std::list<Marker2D*>::iterator other = obj.getMarkers().begin();
        while(mine != getMarkers().end())
        {
            (*mine)->setLabel((*other)->label());
            ++mine, ++other;
        }
    }

    /**
     * Clone method
     * @note uses the copy constructor
     */
    virtual TaggedMarkers2D* clone()
    {
        return new TaggedMarkers2D(*this);
    }

    /**
     * called when a marker (control point) is added
     */
    void addMarker(Marker2D *marker)
    {
        Markers2D::addMarker(marker);
        setCurrentIndex(nbMarkers()-1);
    }


    /**
     * called when a marker (control point) is deleted
     */
    void removeMarker(Marker2D *marker)
    {
        Markers2D::removeMarker(marker);
        updateIndices();
    }

private:
    void setCurrentIndex(int i)
    {
        if (getCurrentMarkerIterator() != getMarkers().end())
        {
            float real(i);
            getCurrentMarker()->setLabel(real);
        }
    }

    void updateIndices()
    {
        std::list<Marker2D*>::iterator it = getMarkers().begin();
        int newVal = nbMarkers()-1;
        while(it != getMarkers().end())
        {
            float real(newVal);
            (*it)->setLabel(real);
            ++it;
            --newVal;
        }
    }

};


/**
 * Custom class extending the PoLAR::DrawableMarkers2D class
 */
class DrawableTaggedMarkers2D: public DrawableMarkers2D
{
public:
    DrawableTaggedMarkers2D(std::list<Marker2D *> list, Viewer2D *viewer):
        DrawableMarkers2D(list, viewer)
    {
        // overwrite the default object created by the base class
        TaggedMarkers2D *obj = new TaggedMarkers2D(list);
        setObject(obj);
    }

    DrawableTaggedMarkers2D(Viewer2D* viewer, TaggedMarkers2D *obj):
        DrawableMarkers2D(viewer, static_cast<Markers2D*>(obj))
    {
    }


    /** overriding the className() method from the Markers2D class
     */
    virtual const char* className() const
    {
        return "DrawableTaggedMarkers2D";
    }

    /** overriding the paint() method from the Markers2D class
     */
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
    {
        // draw the base markers
        DrawableMarkers2D::paint(painter, option, widget);

        // add custom drawing
        TaggedMarkers2D *object = dynamic_cast<TaggedMarkers2D*>(getObject());
        if(object)
        {
            // get access to the list of markers
            std::list<Marker2D*> list = object->getMarkers();
            // get access to the marker that is currently selected
            std::list<Marker2D*>::iterator current = object->getCurrentMarkerIterator();

            // select the QPen accordingly to the object state: selected or not
            QPen pen = (isSelected() && isEditable() ? getPen(DrawableObject2D::Selected) : getPen(DrawableObject2D::Unselected));

            // loop through the list of markers
            for(std::list<Marker2D*>::iterator it = list.begin(); it != list.end(); ++it)
            {
                // check if control point is the currently selected one so that we adapt our QPen accordingly
                if(*it == *current)
                {
                    painter->setPen(getPen(DrawableObject2D::MarkerSelected));
                }
                else
                    painter->setPen(pen);

                // our custom text
                std::stringstream sstm;
                sstm << (*it)->label();
                double x, y;
                // get the control point coordinates from absolute reference to Qt scene reference
                transformCoord((*it)->x() + 5, (*it)->y() - 5, x, y);
                // draw the text thanks to the given painter
                painter->drawText(x, y, QString::fromStdString(sstm.str()));
            }
        }
    }

protected:

    // reimplement this method to add custom options loading
    virtual void load(std::istream& in)
    {
        DrawableMarkers2D::load(in);
    }


};


// This macro ensures our custom class is registered and so available for saving to/loading from a file
REGISTER_TYPE(DrawableTaggedMarkers2D)



#endif //TAGGED_MARKERS_H
