/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

template<typename T>float *toFloatArray(const T *i_array, const int dim)
{
    float* floatArray;
    const T *ta;
    /* params validity check */
    if (!i_array || dim <= 0) return (float*)NULL;

    if (!(floatArray=new float[dim])) return (float*)NULL;
    floatArray += dim, ta = i_array + dim;
    while (ta-- > i_array) *--floatArray = (float)*ta;
    return floatArray;
}

template<typename T> float *readFromStream(const int dim, FILE *fp, const int offset)
{
    T *buf;
    float *res;
    if (!fp || dim <= 0) return (float*)NULL;

    buf = (T *)malloc(dim*sizeof(T));
    if (!buf) return (float*)NULL;
    fseek(fp, offset, SEEK_CUR);
    if (!fread(buf, dim*sizeof(T), 1, fp))
    {
        free(buf);
        return (float*)NULL;
    }
    if(!(res = toFloatArray<T>(buf, dim)))
    {
        free(buf);
        return (float*)NULL;
    }
    free(buf);

    return res;
}

template<typename T> float *readFromFile(const int dim, const char *filename, const int offset)
{
    float *res;
#if defined(WIN32) || defined(_WIN32)
    FILE *f;
    errno_t err = fopen_s(&f, filename, "rb");
    if (err)
#else
    FILE *f = fopen(filename, "rb");
    if (!f)
#endif
        return (float*)NULL;

    res = readFromStream<T>(dim, f, offset);
    fclose(f);
    return res;
}


float *readData(char *fname, int w, int h, int n, int o, const char *t)
{
    if (!strcmp(t, "uc")) return readFromFile<unsigned char>(w*h*n, (const char*)fname, o);
    else if (!strcmp(t,"c")) return readFromFile<char>(w*h*n, (const char*)fname, o);
    else if (!strcmp(t,"us")) return readFromFile<unsigned short>(w*h*n, (const char*)fname, o);
    else if (!strcmp(t,"s")) return readFromFile<short>(w*h*n, (const char*)fname, o);
    else if (!strcmp(t,"ui")) return readFromFile<unsigned int>(w*h*n, (const char*)fname, o);
    else if (!strcmp(t,"i")) return readFromFile<int>(w*h*n, (const char*)fname, o);
    else if (!strcmp(t,"f")) return readFromFile<float>(w*h*n, (const char*)fname, o);
    else if (!strcmp(t,"d")) return readFromFile<double>(w*h*n, (const char*)fname, o);
    return (float*)NULL;
}
