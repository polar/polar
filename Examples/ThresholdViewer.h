/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _THRESHOLDVIEWER_
#define _THRESHOLDVIEWER_


#include <QFileDialog>
#include <QMessageBox>
#include <QObject>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>
#include <PoLAR/DialogThreshold.h>

class ThresholdViewer : public PoLAR::Viewer
{
    Q_OBJECT
public:
    ThresholdViewer(QWidget * parent = 0, const char * name = 0, Qt::WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f),
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("I: edit image mode (pan/zoom)\n")+
           QString("M: add/edit markers mode (blobs)\n")+
           QString("D: toggle background image display\n")+
           QString("G: grab a window snapshot\n")+
           QString("T: pops up dialog for threshold\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this)
    {
        addEventHandler(new osgViewer::StatsHandler);
        mb.setWindowModality(Qt::NonModal);

        thresholdPopup.addFrame(this);
        QObject::connect(&thresholdPopup, SIGNAL(getThresholds(int,int)), this, SLOT(threshold(int,int)));
    }
public slots:
    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            mb.show();
            break;
        case Qt::Key_Q:
            close();
            break;
        case Qt::Key_D:
        {
            bgImageToggle();
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot();
            break;
        }
        case Qt::Key_M:
        {
            newBlob();
            break;
        }
        case Qt::Key_T:
        {
            thresholdPopup.show();
            break;
        }
        case Qt::Key_G:
        {
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");
            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }
        default: Viewer::keyPressEvent(event);
        }
    }


private slots:
    void threshold(int min, int max)
    {
        // put anything you want here... Just to show how to get the actual threshold related to the
        // true original pixel values
        std::cout << "Real threshold values: " << getBgImage()->getRealPixelValue(min)
                  << ", " << getBgImage()->getRealPixelValue(max) << std::endl;
    }

private:
    QMessageBox mb;
    PoLAR::DialogThreshold thresholdPopup;
};

#endif // _MYVIEWER_
