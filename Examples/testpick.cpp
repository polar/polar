/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>

#include "basicInterface3Dbis.h"
#include "props.h"
#include <PoLAR/Image.h>
#include <PoLAR/Util.h>

#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/ColorMask>
#include <osg/PolygonMode>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/Texture2D>
#include <iostream>


int main(int argc,char ** argv)
{
    QApplication app(argc,argv);
    Interface inter;
    osg::ref_ptr<PoLAR::Image_uc> myImage;

    //osg::setNotifyLevel(osg::DEBUG_FP);

    // Read the image given in parameter
    if (argc >1)
    {
        if(PoLAR::Util::fileExists(argv[1]))
            myImage = new PoLAR::Image_uc(argv[1], true);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }

        // Add the image read as background image
        inter.addImage(myImage, 1);

        PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION;
        if (argc > 4)
        {
            switch(atoi(argv[4]))
            {
            case 1: pt=PoLAR::Viewer2D3D::ANGIO; break;
            case 2: pt=PoLAR::Viewer2D3D::ANGIOHINV; break;
            default: pt=PoLAR::Viewer2D3D::VISION;
            }
        }

        // Load the projection given in parameter
        if (argc > 2)
        {
            osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[2]);
            inter.setProjection(P, pt);
        }

        // Load the model given in parameter
        if (argc > 3)
        {
            if(!PoLAR::Util::fileExists(argv[3]))
            {
                std::cerr << "Unable to load model" << std::endl;
                exit(0);
            }
            else
            {
                // create a pickable and editable PoLAR::Object3D
                osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(argv[3], true, true, true);
                osgUtil::SmoothingVisitor smooth;
                osgUtil::Optimizer optimizer;
                optimizer.optimize(object3D.get(), osgUtil::Optimizer::ALL_OPTIMIZATIONS);
                object3D->accept(smooth);

                osg::Matrixd m = osg::Matrixd::rotate(M_PI/3,0.0,0.0,1.0) * osg::Matrixd::translate(0.5,0.0,0.005); // initialisation of matrix m with a custom tranformation
                object3D->setName("loaded Model");
                object3D->setTransformationMatrix(m);
                inter.addNode(object3D);
                inter.getViewer()->setTrackNode(object3D.get()); // set this object to be tracked by the manipulator
            }
        }

        { // A sphere
            osg::ref_ptr<osg::Sphere> unitSphere = new osg::Sphere(osg::Vec3(-0.5,1.5,2.0), 0.4);
            osg::ref_ptr<osg::ShapeDrawable> unitSphereDrawable = new osg::ShapeDrawable(unitSphere.get());
            osg::ref_ptr<osg::Geode> sphereGeode = new osg::Geode();
            sphereGeode->addDrawable(unitSphereDrawable.get());
            // create a pickable but non editable PoLAR::Object3D
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(sphereGeode, true, false, true);
            object3D->setName("Sphere");
            inter.addNode(object3D.get());
        }

        { // A textured floor : a carpet !
            osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
            osg::ref_ptr<osg::ShapeDrawable> shape;
            shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0001f), 1.0f, 2.0f, 0.00006f));
            floorGeode->addDrawable(shape.get());
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get(), false, true, true);
            osg::ref_ptr<PoLAR::Image_uc> texture = new PoLAR::Image_uc(QString(dataPath() + "carpet.jpg").toStdString());
            //object3D->setObjectTextureState(QString(dataPath() + "carpet.jpg").toStdString());
            object3D->setObjectTextureState(texture.get());
            object3D->setName("Carpet");
            object3D->setEditOn();
            inter.addNode(object3D.get());
        }

        { // A colored pyramid
            osg::ref_ptr<osg::Geode> pyramidGeode = new osg::Geode();
            osg::ref_ptr<osg::Geometry> pyramidGeometry = new osg::Geometry();
            pyramidGeode->addDrawable(pyramidGeometry);
            osg::ref_ptr<osg::Vec3Array> pyramidVertices = new osg::Vec3Array;
            pyramidVertices->push_back( osg::Vec3( 0, 2.5, 0) ); // front left
            pyramidVertices->push_back( osg::Vec3(0.5, 2.5, 0) ); // front right
            pyramidVertices->push_back( osg::Vec3(0.5,3.0, 0) ); // back right
            pyramidVertices->push_back( osg::Vec3( 0,3.0, 0) ); // back left
            pyramidVertices->push_back( osg::Vec3( 0.25, 2.75,0.5) ); // peak

            pyramidGeometry->setVertexArray( pyramidVertices.get() );

            osg::ref_ptr<osg::DrawElementsUInt> pyramidBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
            pyramidBase->push_back(3);
            pyramidBase->push_back(2);
            pyramidBase->push_back(1);
            pyramidBase->push_back(0);
            pyramidGeometry->addPrimitiveSet(pyramidBase.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceOne = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceOne->push_back(0);
            pyramidFaceOne->push_back(1);
            pyramidFaceOne->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceOne.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceTwo = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceTwo->push_back(1);
            pyramidFaceTwo->push_back(2);
            pyramidFaceTwo->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceTwo.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceThree = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceThree->push_back(2);
            pyramidFaceThree->push_back(3);
            pyramidFaceThree->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceThree.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceFour = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceFour->push_back(3);
            pyramidFaceFour->push_back(0);
            pyramidFaceFour->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceFour.get());


            osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
            colors->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 0.25f) ); //index 0 red
            colors->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 0.25f) ); //index 1 green
            colors->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 0.25f) ); //index 2 blue
            colors->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 0.25f) ); //index 3 white
            colors->push_back(osg::Vec4(0.0f, 0.0f, 0.0f, 0.25f) ); //index 4 black

            pyramidGeometry->setColorArray(colors.get(), osg::Array::BIND_PER_VERTEX);
            osgUtil::SmoothingVisitor sv;
            pyramidGeode->accept(sv);
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(pyramidGeode, false, true, true);
            object3D->setName("Pyramide");
            inter.addNode(object3D.get());
        }

        {
            // create a ground for receiving shadows
            osg::ref_ptr<PoLAR::Object3D> ground = PoLAR::Object3D::createGround();
            ground->setMaterial();
            inter.addNode(ground.get());
            ground->setPhantomOn();
        }


        { // create the wardrobe
            osg::ref_ptr<osg::Geode> boxGeode = new osg::Geode;
            osg::ref_ptr<osg::ShapeDrawable> shape;
            shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 1.0f, 1.0f, 1.0f));
            boxGeode->addDrawable(shape.get());
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(boxGeode.get(), false, true, false);
            object3D->setEditOn();
            object3D->setName("wardrobe");
            osg::Matrixd m =  osg::Matrixd::rotate(osg::Quat(0,0,0,0.94))
                    * osg::Matrixd::scale(0.41,0.56,1.56) * osg::Matrixd::translate(-0.57,0.33,0.81);
            object3D->setTransformationMatrix(m);
            inter.addNode(object3D.get());
            object3D->setPhantomOn();
        }

        inter.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image> <projection> [<3D model>] [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]" <<std::endl;
        exit(0);
    }

    return app.exec();
}
