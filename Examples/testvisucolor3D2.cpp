/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>

#include "basicInterface3D2.h"
#include "props.h"
#include <PoLAR/Image.h>
#include <PoLAR/Util.h>

#include <iostream>

#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/ColorMask>
#include <osg/PolygonMode>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/Texture2D>
#include <osg/Node>
#include <osg/Group>
#include <osg/StateSet>
#include <osg/BlendColor>
#include <osg/ColorMask>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/Matrixd>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/LineWidth>
#include <osg/PolygonMode>



int main(int argc,char ** argv)
{
    QApplication app(argc,argv);
    app.setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);

    Interface inter;

    int width;
    int height;

    osg::ref_ptr<PoLAR::Image_uc> image;


    // Read the image
    if (argc >1)
    {
        if(PoLAR::Util::fileExists(argv[1]))
            image = new PoLAR::Image_uc(argv[1], true);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
        width = image->getWidth();
        height = image->getHeight();

        // Add the image to the viewer
        inter.addImageLeft(image, 1);
        inter.addSequenceRight(NULL, width, height, 3, 1);

        // Load the projection
        PoLAR::Viewer2D3D::ProjectionType pt=PoLAR::Viewer2D3D::VISION;
        if (argc > 4)
        {
            switch(atoi(argv[4]))
            {
            case 1: pt=PoLAR::Viewer2D3D::ANGIO; break;
            case 2: pt=PoLAR::Viewer2D3D::ANGIOHINV; break;
            default: pt=PoLAR::Viewer2D3D::VISION;
            }
        }
        if (argc > 2)
        {
            osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[2]);
            inter.setProjection(inter.getLeftViewer(), P, pt);
        }

        // 3d objects creation and addition to the viewer

        { // A sphere
            osg::ref_ptr<osg::Sphere> unitSphere = new osg::Sphere( osg::Vec3(-0.5,1.5,2.0), 0.4);
            osg::ref_ptr<osg::ShapeDrawable> unitSphereDrawable = new osg::ShapeDrawable(unitSphere);
            osg::ref_ptr<osg::Geode> sphereGeode = new osg::Geode();
            sphereGeode->addDrawable(unitSphereDrawable.get());
            PoLAR::Object3D *object3D = new PoLAR::Object3D(sphereGeode, true, false, true);
            object3D->setName("Sphere");
            inter.addNode(object3D);
        }

        { // A textured floor : a carpet !
            osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
            osg::ref_ptr<osg::ShapeDrawable> shape;
            shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.01f), 1.0f, 2.0f, 0.00001f));
            floorGeode->addDrawable(shape.get());
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get(), false, true, true);
            object3D->setObjectTextureState(QString(dataPath() + "carpet.jpg").toStdString());
            object3D->setName("Carpet");
            inter.addNode(object3D.get());
        }

        { // a ground for receiving shadows
            osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
            osg::ref_ptr<osg::ShapeDrawable> shape;
            shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 5.0f, 5.0f, 0.00001f));
            floorGeode->addDrawable(shape.get());
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get(), true, false, false);
            object3D->setName("Floor");
            object3D->setEditOn();
            object3D->setPhantomOn();
            inter.addNode(object3D.get());
        }

        { // A colored pyramid
            osg::ref_ptr<osg::Geode> pyramidGeode = new osg::Geode();
            osg::ref_ptr<osg::Geometry> pyramidGeometry = new osg::Geometry();
            pyramidGeode->addDrawable(pyramidGeometry);
            osg::ref_ptr<osg::Vec3Array> pyramidVertices = new osg::Vec3Array;
            pyramidVertices->push_back( osg::Vec3( 0, 2.5, 0) ); // front left
            pyramidVertices->push_back( osg::Vec3(0.5, 2.5, 0) ); // front right
            pyramidVertices->push_back( osg::Vec3(0.5,3.0, 0) ); // back right
            pyramidVertices->push_back( osg::Vec3( 0,3.0, 0) ); // back left
            pyramidVertices->push_back( osg::Vec3( 0.25, 2.75,0.5) ); // peak

            pyramidGeometry->setVertexArray( pyramidVertices.get() );

            osg::ref_ptr<osg::DrawElementsUInt> pyramidBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
            pyramidBase->push_back(3);
            pyramidBase->push_back(2);
            pyramidBase->push_back(1);
            pyramidBase->push_back(0);
            pyramidGeometry->addPrimitiveSet(pyramidBase.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceOne = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceOne->push_back(0);
            pyramidFaceOne->push_back(1);
            pyramidFaceOne->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceOne.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceTwo = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceTwo->push_back(1);
            pyramidFaceTwo->push_back(2);
            pyramidFaceTwo->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceTwo.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceThree = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceThree->push_back(2);
            pyramidFaceThree->push_back(3);
            pyramidFaceThree->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceThree.get());

            osg::ref_ptr<osg::DrawElementsUInt> pyramidFaceFour = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
            pyramidFaceFour->push_back(3);
            pyramidFaceFour->push_back(0);
            pyramidFaceFour->push_back(4);
            pyramidGeometry->addPrimitiveSet(pyramidFaceFour.get());


            osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
            colors->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 0.25f) ); //index 0 red
            colors->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 0.25f) ); //index 1 green
            colors->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 0.25f) ); //index 2 blue
            colors->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 0.25f) ); //index 3 white
            colors->push_back(osg::Vec4(0.0f, 0.0f, 0.0f, 0.25f) ); //index 4 black

            pyramidGeometry->setColorArray(colors.get(), osg::Array::BIND_PER_VERTEX);
            osgUtil::SmoothingVisitor sv;
            pyramidGeode->accept(sv);
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(pyramidGeode, false, true, true);
            object3D->setName("Pyramide");
            inter.addNode(object3D.get());
        }


        // Load the model given in argument
        if (argc > 3)
        {
            if (!PoLAR::Util::fileExists(argv[3]))
            {
                std::cerr << "Unable to load model" << std::endl;
                exit(0);
            }
            else
            {
                osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(argv[3], true, true, true);
                osgUtil::SmoothingVisitor smooth;
                osgUtil::Optimizer optimizer;
                optimizer.optimize(object3D.get(), osgUtil::Optimizer::ALL_OPTIMIZATIONS);
                object3D->accept(smooth);
                osg::Matrixd m = osg::Matrixd::rotate(M_PI/3,0.0,0.0,1.0) * osg::Matrixd::translate(0.5,0.0,0.005); // initialisation of matrix m with a custom tranformation
                object3D->setTransformationMatrix(m);
                object3D->setName("Loaded model");
                inter.addNode(object3D.get());
                inter.getLeftViewer()->setTrackNode(object3D.get());
            }
        }

        inter.setDefaultProjection(inter.getRightViewer());
        inter.resizeWell();
        inter.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image> <projection> [<3D model>] [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]" <<std::endl;
        exit(0);
    }


    return app.exec();
}
