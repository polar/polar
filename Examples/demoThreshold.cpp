/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include "ThresholdViewer.h"
#include <PoLAR/Image.h>

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    int width;
    int height;

    // Create the viewer
    ThresholdViewer viewer;

    // Read the image given in parameter
    if (argc > 1)
    {
        osg::ref_ptr<PoLAR::Image<unsigned char> > myImage;
        if(PoLAR::Util::fileExists(argv[1]))
            myImage = new PoLAR::Image<unsigned char>(argv[1], 1);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
        width = myImage->getWidth();
        height = myImage->getHeight();

        viewer.resize(width, height);

        // Assign the read image as background
        viewer.setBgImage(myImage);

        // Show the viewer
        viewer.bgImageOn();
        viewer.startEditImageSlot();

        if (argc > 2) viewer.readGrx(argv[2]);
        std::cout << "TEST: " << viewer.getNumObjects2D() << std::endl;
        viewer.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <file.ppm> [<file.grx>]" <<std::endl;
        exit(0);
    }

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
