/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWERCAMERAQT_
#define _MYVIEWERCAMERAQT_

#include <QFileDialog>
#include <QMessageBox>

#include <PoLAR/Viewer.h>
#include <PoLAR/VideoPlayer.h>
#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)


class MyViewerDyn : public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewerDyn(PoLAR::VideoPlayer* camera, QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f),
        _mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("I: edit image mode (pan/zoom/window level)\n")+
           QString("M: add/edit markers mode (blobs)\n")+
           QString("W: manipulate scene mode\n")+
           QString("Up: start camera\n")+
           QString("Down: stop camera\n")+
           QString("Right: pause camera\n")+
           QString("Left: play forward x2\n")+
           QString("D: toggle background image display\n")+
           QString("G: grab a window snapshot\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this),
        _camera(camera)
    {
        addEventHandler(new osgViewer::StatsHandler);
        _mb.setWindowModality(Qt::NonModal);
    }

    ~MyViewerDyn()
    {
    }

    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            _mb.show();
            break;
        case Qt::Key_Q:
        {
            close();
            break;
        }
        case Qt::Key_D:
        {
            bgImageToggle();
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot();
            break;
        }
        case Qt::Key_M:
        {
            newBlob();
            break;
        }
        case Qt::Key_W:
        {
            startManipulateSceneSlot();
            break;
        }
        case Qt::Key_Up:
        {
            _camera->play();
            bgImageOn();
            startEditImageSlot();
            break;
        }
        case Qt::Key_Down:
        {
            _camera->stop();
            break;
        }
        case Qt::Key_Right:
        {
            _camera->pause();
            break;
        }
        case Qt::Key_Left:
        {
            _camera->forward(2.0);
            break;
        }
        case Qt::Key_G:
        {
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");

            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }
        default: Viewer::keyPressEvent(event);
        }
    }

private:
    QMessageBox _mb;
    PoLAR::VideoPlayer *_camera;
};

#endif // _MYVIEWERCAMERAQT_
