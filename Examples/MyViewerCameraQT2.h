/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef MYVIEWERCAMERAQT2_H
#define MYVIEWERCAMERAQT2_H

#include <osg/Geode>
#include <osg/ShapeDrawable>
#include <osg/io_utils>
#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Viewer.h>
#include <PoLAR/Image.h>
#include <PoLAR/ShaderManager.h>
#include <PoLAR/VideoPlayer.h>


struct PathCallback : public PoLAR::Object3DCallback
{
    void update(PoLAR::Object3D *object)
    {
        object->rotate(0.005, 0., 1., 0.);
    }
};


class MyViewerDyn2: public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewerDyn2(PoLAR::VideoPlayer *camera, QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        Viewer(parent, name, f), _rotation(false),
        _camera(camera),
        _mb(QMessageBox::Information,
            QString("Help: shortkeys"),
            QString("Shortkeys:\n")+
            QString("H: this help\n")+
            QString("Q: quit the application\n")+
            QString("I: edit image mode (pan/zoom/window level)\n")+
            QString("W: manipulate scene mode\n")+
            QString("R: toggle plane rotation\n")+
            QString("Up: start camera\n")+
            QString("Down: stop camera\n")+
            QString("Right: pause camera\n")+
            QString("D: toggle background image display\n")+
            QString("G: grab a window snapshot\n")+
            QString("else default openscenegraph keys\n"),
            QMessageBox::NoButton,
            this)
    {
        addEventHandler(new osgViewer::StatsHandler);
        setBackgroundColor(0.2,0.2,0.8);
        _mb.setWindowModality(Qt::NonModal);
    }

    /**
     * overwrite the viewer's background image and attach it to a custom geode added in the scenegraph
     */
    void setImage(PoLAR::BaseImage *ima)
    {
        osg::ref_ptr<osg::Geode> geode = new osg::Geode;
        if (ima)
        {
            _image = ima;
            int w, h;
            if(_camera)
            {
                w = _camera->getRequestedWidth();
                h = _camera->getRequestedHeight();
            }
            else
            {
                w=ima->getWidth();
                h=ima->getHeight();
            }
            float wT = 1.0, hT = 1.0;
            if (geode->getNumDrawables() == 0)
                geode->addDrawable(osg::createTexturedQuadGeometry( osg::Vec3(-w/2,-h/2,0),
                                                                    osg::Vec3(w,0,0),
                                                                    osg::Vec3(0,h,0),
                                                                    wT,hT
                                                                    )
                                   );
            else
                geode->setDrawable(0,osg::createTexturedQuadGeometry( osg::Vec3(-w/2,-h/2,0),
                                                                      osg::Vec3(w,0,0),
                                                                      osg::Vec3(0,h,0),
                                                                      wT,hT
                                                                      )
                                   );
            osg::ref_ptr<osg::Texture2D> t = ima->makeTexture();
            t->setResizeNonPowerOfTwoHint(false);

            osg::ref_ptr<osg::StateSet> stateSet = geode->getOrCreateStateSet();
            stateSet->setTextureAttributeAndModes(0,t.get(), osg::StateAttribute::ON);
            stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

            // Adding a shader system for window level
            PoLAR::ShaderManager::addWindowLevel(geode.get(), ima);

            QObject::connect(ima, SIGNAL(updateSignal()), this, SLOT(update()));

            osg::Matrixd P = createVisionProjection(w, h, osg::Vec3f(0, 0, 0), w/2);
            setProjection(P);

            _plane = new PoLAR::Object3D(geode.get());
            _plane->setEditOn();
            addObject3D(_plane);
            setTrackNode(_plane.get());
        }
    }

    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch (event->key())
        {
        case Qt::Key_Q:
        {
            _camera->stop();
            this->close();
            break;
        }

        case Qt::Key_Up:
            _camera->play();
            startEditImageSlot();
            break;
        case Qt::Key_Down:
        {
            _camera->stop();
            break;
        }
        case Qt::Key_Right:
        {
            _camera->pause();
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot();
            break;
        }
        case Qt::Key_R:
            if(_plane.valid())
            {
                if(_rotation)
                {
                    _plane->setUpdateCallback(NULL);
                    _rotation = false;
                }
                else
                {
                    _plane->setUpdateCallback(new PathCallback());
                    _rotation = true;
                }
            }
            break;

        case Qt::Key_W:
        {
            startManipulateSceneSlot();

            break;
        }

        default: Viewer::keyPressEvent(event);
        }
    }

private:
    osg::ref_ptr<PoLAR::Object3D> _plane;
    bool _rotation;
    PoLAR::VideoPlayer *_camera;
    QMessageBox _mb;
};

#endif // MYVIEWERCAMERAQT2_H
