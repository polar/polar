/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include <osg/io_utils>

#include <PoLAR/Image.h>
#include <PoLAR/Util.h>
#include <PoLAR/Object3D.h>
#include "MyViewer3D.h"

int main(int argc,char ** argv)
{
    //osg::setNotifyLevel(osg::DEBUG_FP);
    QApplication app(argc, argv);
    int width;
    int height;
    osg::ref_ptr<PoLAR::Image_uc> myImage;

    // Create the viewer
    MyViewer3D viewer;

    // Read the image given in parameter
    if (argc >1)
    {
        if(PoLAR::Util::fileExists(argv[1]))
            myImage = new PoLAR::Image_uc(argv[1], true, 1);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
        width = myImage->getWidth();
        height = myImage->getHeight();

        viewer.resize(width,height);

        // Add the image read as background image
        viewer.setBgImage(myImage);

        // Show it
        viewer.bgImageOn();

        // Have the image interaction activ
        viewer.startEditImageSlot();

        // Read the type of projection used
        PoLAR::Viewer::ProjectionType pt=PoLAR::Viewer2D3D::VISION;
        if (argc > 4)
        {
            switch(atoi(argv[4]))
            {
            case 1: pt=PoLAR::Viewer::ANGIO; break;
            case 2: pt=PoLAR::Viewer::ANGIOHINV; break;
            default: pt=PoLAR::Viewer::VISION;
            }
        }

        // Load the projection given in parameter
        if (argc > 2)
        {
            osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[2]);
            viewer.setProjection(P, pt);
        }

        // Load the model given in parameter
        if (argc > 3)
        {
            if(!PoLAR::Util::fileExists(argv[3]))
            {
                std::cerr << "Unable to load model" << std::endl;
                exit(0);
            }
            osg::ref_ptr<PoLAR::Object3D> obj = new PoLAR::Object3D(argv[3], true, true);
            obj->setName("object");
            obj->optimize();
            osg::Matrixd m =  osg::Matrixd::translate(0.5,0.0,0.005); // initialisation of matrix m with a custom tranformation
            obj->setTransformationMatrix(m);
            // Add the object3D created from loaded model to the viewer
            viewer.addObject3D(obj.get());
            viewer.addBlendDialog(obj.get());
            viewer.setTrackNode(obj.get());
        }

        // create a phantom ground to receive shadows
        {
            viewer.addLightSource(5,2,5, true);
            osg::ref_ptr<PoLAR::Object3D> ground = PoLAR::Object3D::createGround();
            ground->setPhantomOn();
            viewer.addObject3D(ground.get());
        }

        // Show the widget
        viewer.center();
        viewer.show();

    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image> <projection> <3D model> [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]" <<std::endl;
        exit(0);
    }

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
