/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWER2D_
#define _MYVIEWER2D_

#include <QFileDialog>
#include <QMessageBox>
#include <QObject>

#include <iostream>
#include <fstream>
#include <string>
#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>
#include <PoLAR/Util.h>


class CalibViewer : public PoLAR::Viewer
{
    Q_OBJECT
public:
    CalibViewer(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f),
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("CTRL held: edit image mode (pan/zoom/window level)\n")+
           QString("R: reset window level to default value\n")+
           QString("M: add/edit markers mode (markers)\n")+
           QString("B: add/edit markers mode (blobs)\n")+
           QString("A: add/edit markers mode (arrows)\n")+
           QString("P: add/edit markers mode (polygon)\n")+
           QString("L: add/edit markers mode (polyline)\n")+
           QString("S: save the markers coordinates in a file\n")+
           QString("F: change the output file to save the markers coordinates\n")+
           QString("K: change color of selected 2D object\n")+
           QString("D: toggle background image display\n")+
           QString("G: save the 2D obect as SVG\n")+
           QString("F1: print out current 2D object information\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this)
    {
        mb.setWindowModality(Qt::NonModal);
        // set some custom manipulation shortcuts
        this->setZoomManipulation(Qt::ControlModifier, Qt::RightButton);
        this->setPanManipulation(Qt::ControlModifier, Qt::LeftButton);
        this->setWindowLevelManipulation(Qt::ControlModifier, Qt::MiddleButton);
    }

    void setSaveFName(QString s) { _fname = s; std::cout << "FILE NAME " << _fname.toLatin1().data() << std::endl; }

public slots:

    virtual void keyReleaseEvent(QKeyEvent *event)
    {
        if(!(event->modifiers() & Qt::ControlModifier) && (_lastMetaPressed & Qt::ControlModifier))
        {
            startEditMarkersSlot();
            if(getNumObjects2D()) getObject2DByIndex(0)->activate();
        }
        else
            Viewer::keyReleaseEvent(event);
    }

    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        _lastMetaPressed = event->modifiers();
        if(event->modifiers() & Qt::ControlModifier)
        {
            startEditImageSlot();
        }

        switch(event->key())
        {
        case Qt::Key_A:
        {
            newArrows();
            break;
        }
        case Qt::Key_B:
        {
            newBlob(); // Add blobs
            break;
        }
        case Qt::Key_D:
        {
            bgImageToggle(); // Toggle display of the background image
            break;
        }
        case Qt::Key_F:
        {
            QString fname = QFileDialog::getSaveFileName( this,
                                                          "Choose a filename to save the snapshot image",
                                                          QString::null);
            if(!fname.isEmpty()) setSaveFName(fname);
            break;
        }

        case Qt::Key_G:
        { // Grab an image
            QString fname = QFileDialog::getSaveFileName( this,
                                                          "Choose a filename to save the snapshot image",
                                                          QString::null,
                                                          "SVG images (*.QVG)");
            if (!fname.isEmpty())
                saveObject2DAsSvg(fname);
            break;
        }
        case Qt::Key_H:
            mb.show(); // Show the help dialog
            break;
        case Qt::Key_I:
        {
            startEditImageSlot(); // Set the image interaction active
            break;
        }
        case Qt::Key_M:
        {
            newMarkers(); // Add markers
            break;
        }

        case Qt::Key_K:
        {
            PoLAR::DrawableObject2D * obj = getCurrentObject2D();
            if(obj)
            {
                QPen select(QColor(255,0,0));
                select.setWidth(5);
                QPen unselect(QColor(0,255,0));
                unselect.setWidth(3);
                QPen selectMarker(QColor(255,255,255));
                obj->setPens(select, unselect, selectMarker);
            }
            break;
        }
        case Qt::Key_L:
        {
            newPolyLine();
            break;
        }

        case Qt::Key_P:
        {
            newPolygon();
            break;
        }

        case Qt::Key_Q:
            this->close();
            break;

        case Qt::Key_S:
        {
            PoLAR::DrawableObject2D* currentObj = getObject2DByIndex(0);
            if(currentObj)
            {
                std::ofstream out;
                if(_fname.isEmpty()) break;
                out.open(_fname.toStdString());
                std::list<PoLAR::Marker2D*>::iterator it = currentObj->getMarkers().begin();
                while(it != currentObj->getMarkers().end())
                {
                    PoLAR::Marker2D* m = (*it);
                    out << std::setprecision(6) << std::fixed << m->x() << ", " << m->y() << std::endl;
                    ++it;
                }
                out.close();
                std::cout << "File " << _fname.toStdString() << " saved" << std::endl;
            }
            break;
        }


        case Qt::Key_R:
        {
            getBgImage()->resetWindowLevel();
            break;
        }

        case Qt::Key_F1:
        {
            PoLAR::DrawableObject2D *obj2D = getCurrentObject2D(); // get the current selected DrawableObject2D
            if (obj2D != NULL) std::cout << (*obj2D); // print it out (you can redirect the obj2D in any std::ostream, for example you could save it in file like this : std::ofstream out("savefile.txt", std::ios::out); out << (*object2D);)
            break;
        }

        default: Viewer::keyPressEvent(event);
        }
    }
private:
    int _lastMetaPressed;
    QMessageBox mb;
    QString _fname;
};

#endif // _MYVIEWER2D_
