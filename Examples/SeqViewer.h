#ifndef _SEQVIEWER_
#define _SEQVIEWER_


#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>

#include <ViewerCameraOffline.h>
#include <cameraOffline.h>
#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)

class SeqViewer : public PoLAR::ViewerCameraOffline
{
    Q_OBJECT
public:
    SeqViewer(int depth, char * fileSeq, char *fileGrx, int startFrame, int stepFrame, QWidget *parent=0, const char *name=0, const QGLWidget *share=0, WindowFlags f=0):
        PoLAR::ViewerCameraOffline(depth, fileSeq, startFrame, stepFrame, parent, name, share, f),
        fgrx(new char[2048]),
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("Down: goto next image\n")+
           QString("D: toggle background image display\n")+
           QString("G: grab a window snapshot\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this)
    {
        strcpy(fgrx, fileGrx);
        addEventHandler(new osgViewer::StatsHandler);
        QObject::connect(this, SIGNAL(keyPressSignal(QKeyEvent *)), this, SLOT(keyPressSlot(QKeyEvent*)));
        QObject::connect(this, SIGNAL(keyReleaseSignal(QKeyEvent *)), this, SLOT(keyReleaseSlot(QKeyEvent*)));
        QObject::connect(this, SIGNAL(newCapture()), this, SLOT(loadGrx()));
        QObject::connect(this, SIGNAL(initCapture()), this, SLOT(loadGrx()));
        captureInitSlot();
        captureStopSlot();
        mb.setWindowModality(Qt::NonModal);
    }
    ~SeqViewer() {if (fgrx) delete []fgrx;}
public slots:
    virtual void keyPressSlot(QKeyEvent *event);
    virtual void loadGrx();

private:
    char *fgrx;
    QMessageBox mb;
};

#endif // _MYVIEWER_
