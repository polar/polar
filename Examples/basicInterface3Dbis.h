/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef BASICINTERFACEDSA3D_H
#define BASICINTERFACEDSA3D_H

#include <QStringList>
#include <QMessageBox>
#include <QMainWindow>
#include <QToolBar>
#include <QToolButton>
#include <QSlider>
#include <QScrollBar>
#include <QInputDialog>
#include <QSpinBox>
#include <QLabel>
#include <QComboBox>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <osgGA/TrackballManipulator>

#include <PoLAR/Viewer.h>
#include <PoLAR/SceneGraph.h>
#include <PoLAR/TabBoxPickHandler.h>
#include <PoLAR/TrackBallPickHandler.h>
#include <PoLAR/TranslateAxisPickHandler.h>
#include <PoLAR/ScaleAxisPickHandler.h>
#include <PoLAR/RotateSpherePickHandler.h>
#include <PoLAR/DefaultPickHandler.h>
#include <PoLAR/Object3D.h>
#include <PoLAR/Light.h>
#include <PoLAR/BasicLightDialog.h>
#include <PoLAR/LightDialog.h>
#include <PoLAR/BaseImage.h>
#include <PoLAR/Util.h>

using PoLAR::Marker2D;

class Interface : public QMainWindow  
{
    Q_OBJECT

public:
    Interface();

    ~Interface();

    void addImage(PoLAR::BaseImage* image, int nima)
    {
        viewer->setBgImage(image);
        scrollbar->setMaximum(nima-1);
        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }


    void addSequence(float* sequence, int w, int h, int d, int nima)
    {
        viewer->setBgImage<float>(sequence, w, h, d, nima);
        scrollbar->setMaximum(nima-1);

        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }

    void addNode(PoLAR::Object3D *object)
    {
        viewer->addObject3D(object);
    }

    void setProjection(osg::Matrixd& P, PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION) {viewer->setProjection (P, pt);}
    void setProjection() {viewer->setProjection();} // default projection

    inline PoLAR::Viewer* getViewer() {return viewer;}


signals:
    void startEditImageSignal();
    void startEditMarkersSignal();


private slots:
    void exit() {close();}
    void startEditImage() {emit startEditImageSignal();}
    void startEditMarkers() {emit startEditMarkersSignal();}
    void activateLabelBox(Marker2D *m);
    void deactivateLabelBox();
    void changeTitle(int n);
    void newSpline();
    void saveMarkers();
    void pickedObjectInRed();
    void pickedObjectFrame();
    void pickedObjectTransformation();
    void pickedObjectReset();
    void pickedObjectPhantom();
    void pickedObjectReceiveShadOn();
    void pickedObjectReceiveShadOff();
    void pickedObjectCastShadOn();
    void pickedObjectCastShadOff();
    void draggerComboSlot(int);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);

    inline void removeCurrentObject3DPickHandler()
    {
        osgViewer::View::EventHandlers::iterator it = std::find(viewer->getEventHandlers().begin(), viewer->getEventHandlers().end(), _pick);
        if (it != viewer->getEventHandlers().end()) viewer->getEventHandlers().erase(it);
    }
    // choose tabbox dragger for object3D edition
    inline void tabboxDragger() {_pick->cleanUp(viewer); removeCurrentObject3DPickHandler(); _pick=new PoLAR::TabBoxPickHandler; viewer->addEventHandler(_pick);}

    // choose trackball dragger for object3D edition
    inline void trackballDragger() {_pick->cleanUp(viewer); removeCurrentObject3DPickHandler(); _pick=new PoLAR::TrackBallPickHandler; viewer->addEventHandler(_pick);}

    // choose translateaxis dragger for object3D edition
    inline void translateaxisDragger() {_pick->cleanUp(viewer); removeCurrentObject3DPickHandler(); _pick=new PoLAR::TranslateAxisPickHandler; viewer->addEventHandler(_pick);}

    // choose scaleaxis dragger for object3D edition
    inline void scaleaxisDragger() {_pick->cleanUp(viewer); removeCurrentObject3DPickHandler(); _pick=new PoLAR::ScaleAxisPickHandler; viewer->addEventHandler(_pick);}

    // choose rotatesphere dragger for object3D edition
    inline void rotatesphereDragger() {_pick->cleanUp(viewer); removeCurrentObject3DPickHandler(); _pick=new PoLAR::RotateSpherePickHandler; viewer->addEventHandler(_pick);}

private:
    QToolBar *toolbar, *scrollbarTB;
    QPixmap *quitPixmap, *imagePixmap, *markerPixmap, *savePixmap, *scenePixmap;
    QToolButton *quitButton, *imageButton, *markerButton, *saveButton, *editSceneButton;
    QSpinBox *labelBox;
    QScrollBar *scrollbar;
    PoLAR::Viewer *viewer;
    QLabel *statuslabel;
    QMessageBox *mb;
    PoLAR::LightDialog *lightDialog;
    QCheckBox *displayCheckBox;
    QComboBox *draggerCombo;
    osg::ref_ptr<PoLAR::Object3D> _object3D;
    PoLAR::Object3DPickHandler *_pick;
};

#endif // BASICINTERFACE_H
