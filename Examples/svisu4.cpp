/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>

#include <PoLAR/Util.h>
#include <PoLAR/Image.h>
#include <PoLAR/ShaderManager.h>
#include "MyViewer.h"


int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    int width=561;
    int height=619;
    //osg::setNotifyLevel(osg::DEBUG_FP);

    // scene creation
    MyViewer viewer(0, "MyViewer");
    std::cout << "VIEWER: " << viewer.getName().toStdString() << std::endl;

    viewer.resize(width,height);
    viewer.startEditImageSlot();


    // Load the model given in parameter
    if (argc > 1)
    {
        if(!PoLAR::Util::fileExists(argv[1]))
        {
            std::cerr << "Unable to load model" << std::endl;
            exit(0);
        }

        osg::ref_ptr<PoLAR::Object3D> obj = new PoLAR::Object3D(argv[1], false);
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;
        optimizer.optimize(obj.get());
        obj->accept(smooth);

        viewer.addObject3D(obj.get());
        viewer.setTrackNode(obj.get());
        osg::Matrixd P;
        if (argc < 3)
        {
            P = viewer.createVisionProjection(width, height);   // Create a default projection
        }
        else P = PoLAR::Util::readProjectionMatrix(argv[2]); // Load the projection given in parameter

        PoLAR::Viewer::ProjectionType pt = PoLAR::Viewer::VISION;
        if (argc > 3)
        {
            switch(atoi(argv[3]))
            {
            case 1: pt=PoLAR::Viewer::ANGIO; break;
            case 2: pt=PoLAR::Viewer::ANGIOHINV; break;
            default: pt=PoLAR::Viewer::VISION;
            }
        }

        // Set the projection to the viewer
        viewer.setProjection(P, pt);
        viewer.center();
        viewer.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << "<3D model> [<projection>] [<type_of_projection> = 1(ANGIO) or 2(ANGIOHINV), default(VISION)]" << std::endl;
        exit(0);
    }

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
