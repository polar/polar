/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef THDIALOG_H
#define THDIALOG_H

#include <QVariant>
#include <QDialog>

class QVBoxLayout;
class QSlider;
class QFrame;
class QPushButton;

class THDialog : public QDialog
{
    Q_OBJECT

public:
    THDialog( QWidget* parent = 0, Qt::WindowFlags fl = 0);
    ~THDialog();
    virtual QSlider* addSlider(int i);

protected:
    QPushButton *OKbutton;
    QVBoxLayout *THDialogLayout;
    QVBoxLayout *frameLayout;
    QFrame *frame;

protected slots:
    virtual void languageChange();
};

#endif // THDIALOG_H
