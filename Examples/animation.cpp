/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>

#include "MyViewer.h"
#include <PoLAR/AnimatedObject3D.h>
#include <PoLAR/Util.h>
#include <PoLAR/Image.h>
#include <PoLAR/Notify.h>

#define ROTATION_STEP 10
#define WALK_LENGTH 2

struct PathCallback : public PoLAR::Object3DCallback
{
    PathCallback():
        _pathArea(0), _x(0.0), _y(-0.01), _rotInc(0.0), _rotateEnded(false)
    {
        _timer1.setStartTick();
    }


    void update(PoLAR::Object3D *object)
    {
        double dt = _timer1.time_s();
        int seconds(floor(dt));
        int length = (_pathArea == 0 || _pathArea == 2) ? WALK_LENGTH*2 :WALK_LENGTH;
        if(seconds >= length)
        {
            if(_rotInc == -1.0)
            {
                _timer2.setStartTick();
                _rotInc = 0.0;
            }
            double ms  = _timer2.time_m();
            if(ms >= 20.0)
            {
                double rot = M_PI/(2*ROTATION_STEP);
                _rotInc += rot;
                object->rotate(rot, 0, 0, 1);
                if(PoLAR::Util::Null((_rotInc - M_PI/2)))
                {
                    _rotInc = -1.0;
                    _rotateEnded = true;
                }
            }

            if(_rotateEnded)
            {
                _pathArea++;
                if(_pathArea > 3) _pathArea = 0;
                switch(_pathArea)
                {
                case 0:
                    _x = 0.0;
                    _y = -0.01;
                    break;
                case 1:
                    _x = 0.01;
                    _y = 0.0;
                    break;
                case 2:
                    _x = 0.0;
                    _y = 0.01;
                    break;
                case 3:
                    _x = -0.01;
                    _y = 0.0;
                    break;
                default:
                    break;
                }
                _rotateEnded = false;
                _timer1.setStartTick();
            }
        }
        object->translate(osg::Vec3d(_x,_y,0.0));
    }

    osg::Timer _timer1;
    osg::Timer _timer2;
    int _pathArea;
    double _x, _y;
    double _rotInc;
    bool _rotateEnded;
};

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);

    MyViewer viewer(0, "MyViewer");

    if(argc > 1)
    {
        if(PoLAR::Util::fileExists(argv[1]))
        {
            osg::ref_ptr<PoLAR::Image_uc> img = new PoLAR::Image_uc(argv[1], true);
            viewer.setBgImage(img.get());
            viewer.resize(img->getWidth(),img->getHeight());
            viewer.startEditImageSlot();
        }

        if(argc > 2)
        {
            osg::Matrix P = PoLAR::Util::readProjectionMatrix(argv[2]); // Load the projection given in parameter
            // Set the projection to the viewer
            viewer.setProjection(P);

        }

        // Load the model given in parameter
        if (argc > 3)
        {
            if(!PoLAR::Util::fileExists(argv[3]))
            {
                std::cerr << "Unable to load model" << std::endl;
                exit(0);
            }

            // load the model as animated model
            osg::ref_ptr<PoLAR::AnimatedObject3D> obj = new PoLAR::AnimatedObject3D(argv[3], true);
            obj->optimize();
            obj->setEditOn();
            obj->scale(0.2);
            obj->translate(0.0, 2.5, -5.6);

            // add callback to manage translation and rotation
            obj->setUpdateCallback(new PathCallback);
            viewer.addObject3D(obj.get());
            viewer.setTrackNode(obj.get());

            // print the names of the animations available
            obj->printAnimationList();

            // play loaded animation
            obj->setAnimationSpeed(1.5);
            obj->play();
        }

        {
            // add ground
            viewer.addLightSource(5,-5, 5.5, true);
            osg::ref_ptr<PoLAR::Object3D> ground = PoLAR::Object3D::createGround(7.0f, 13.0f);
            ground->setName("Table");
            ground->translate(1.0, 2.0, 0.0);
            ground->setPhantomOn();
            viewer.addObject3D(ground.get());
        }

        viewer.center();
        viewer.show();

    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image> [<projection> <3D model>]" << std::endl;
        return 0;
    }

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
