/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWER3D_
#define _MYVIEWER3D_

#include <QFileDialog>
#include <QMessageBox>
#include <QObject>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>
#include <PoLAR/BlendDialog.h>

class MyViewer3D : public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewer3D(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f, true), // MyViewer inherits from PoLAR::Viewer
        // Help messageBox creation
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("I: edit image mode (pan/zoom/window level)\n")+
           QString("M: add/edit markers mode\n")+
           QString("W: manipulate scene mode\n")+
           QString("D: toggle background image display\n")+
           QString("F1: toggle 3D object display\n")+
           QString("F2: change colour of the 3D object\n")+
           QString("F3: scale and translate the 3D object\n")+
           QString("G: grab a window snapshot\n")+
           QString("B: show blend dialog\n")+
           QString("P: print projection matrix\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this),
        _blendDialog(this)
    {
        // Add native OpenSceneGraph stats handler
        addEventHandler(new osgViewer::StatsHandler);
        mb.setWindowModality(Qt::NonModal);
    }

    void addBlendDialog(PoLAR::Object3D *o)
    {
        // Set the blend dialog to be active on the given object
        _blendDialog.setObject(o);
    }

public slots:
    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            mb.show(); // Show the help dialog
            break;
        case Qt::Key_Q:
        {
            close();
            break;
        }
        case Qt::Key_D:
        {
            bgImageToggle(); // Toggle display of the background image (here we can call directly PoLAR::ViewerStatic methods as MyViewer is a PoLAR::ViewerStatic)
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot(); // Set the image interaction active
            break;
        }
        case Qt::Key_M:
        {
            newMarkers(); // Add markers
            break;
        }
        case Qt::Key_W:
        {
            startManipulateSceneSlot(); // Set the 3D interaction active
            break;
        }
        case Qt::Key_F1:
        {
            osg::ref_ptr<PoLAR::Object3D> o=getObject3D(0); // get an Object3D thanks to his index (here index 0 = the first added in the scene)
            if (o.valid())
            { // toggle the display of the Object3D
                if (o->isDisplayed()) o->setDisplayOff();
                else o->setDisplayOn();
            }
            break;
        }
        case Qt::Key_F2:
        {
            osg::ref_ptr<PoLAR::Object3D> o=getObject3D(0); // get an Object3D thanks to his index (here index 0 = the first added in the scene)
            if (o->hasMaterial())
            { // change the different colour component of the Object3D
                o->setAmbientColor(0.7,0.0,0.0);
                o->setDiffuseColor(0.2,0.2,0.2);
                o->setSpecularColor(0.5,0.5,1.0);
            }
            break;
        }
        case Qt::Key_F3:
        {
            osg::ref_ptr<PoLAR::Object3D> o=getObject3D(0); // get an Object3D thanks to his index (here index 0 = the first added in the scene)
            o->setEditOn(); // set the object to accept edition
            o->scale(2.5,1.0,1.0); // scale the object along x axis (in world frame)
            o->translate(0.0,0.0,1.0); // translate the object along z axis (in world frame)
            break;
        }
        case Qt::Key_G:
        { // Grab an image
           QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");
            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }
        case Qt::Key_B:
        { // Show the blend dialog if hidden
            if (_blendDialog.isHidden()) _blendDialog.show();
            break;
        }
        case Qt::Key_P:
        {
            osg::Matrixd p = getProjection();
            std::cout << p << std::endl;
            break;
        }
        default: Viewer::keyPressEvent(event);
        }
    }
private:
    QMessageBox mb;
    PoLAR::BlendDialog _blendDialog;
};

#endif // _MYVIEWER3D_
