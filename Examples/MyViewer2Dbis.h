/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWER2D_
#define _MYVIEWER2D_

#include <QFileDialog>
#include <QMessageBox>
#include <QObject>

#include <iostream>
#include <vector>
#include <string>
#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>
#include <PoLAR/Util.h>

#include <PoLAR/DrawableArrows2D.h>
#include <PoLAR/DrawableText2D.h>
#include "TaggedMarkers2D.h" // a custom class of 2D objects to show the possibilities of extension

class MyViewer2D : public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewer2D(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f), // MyViewer2D inherits from PoLAR::Viewer
        // Help messageBox creation
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("I: edit image mode (pan/zoom/window level)\n")+
           QString("R: reset window/level\n")+
           QString("M: add/edit markers mode (markers)\n")+
           QString("B: add/edit markers mode (blobs)\n")+
           QString("A: add/edit markers mode (arrows)\n")+
           QString("P: add/edit markers mode (polygon)\n")+
           QString("E: add/edit markers mode (splines)\n")+
           QString("T: add/edit markers mode (text)\n")+
           QString("W: add/edit markers mode (custom tagged markers)\n")+
           QString("L: add/edit markers mode (polyline)\n")+
           QString("K: change color of selected 2D object\n")+
           QString("D: toggle background image display\n")+
           QString("Down: goto next image (if exists)\n")+
           QString("Up: goto previous image (if exists)\n")+
           QString("C: copy 2D objects from image 0 to all other images of the sequence\n")+
           QString("X: copy current 2D object to all other images\n")+
           QString("U: copy 2D objects from image 0 to image 1\n")+
           QString("F1: save all the markers of the sequence\n")+
           QString("F2: load markers\n")+
           QString("F3: save all the markers of the first image of the sequence\n")+
           QString("F4: hide all DrawableMarkers2D of the sequence\n")+
           QString("F5: change unselectColour of the currently selected DrawableObject2D in the current image\n")+
           QString("F6: print out the current selected DrawableObject2D in the current image\n")+
           QString("G: grab a window snapshot\n")+
           QString("P: display projection matrix\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this),
        toggleMarkerDisplay(true)
    {
        // Add native OpenSceneGraph stats handler
        addEventHandler(new osgViewer::StatsHandler);

        mb.setWindowModality(Qt::NonModal);
    }
    void setSaveFName(QString s) { _fname = s; std::cout << "SAVEFNAME " << _fname.toLatin1().data() << std::endl; }
    void addObject3D(PoLAR::Object3D *o)
    {
        // Add an Object3D to this viewer
        Viewer2D3D::addObject3D(o);
    }
public slots:

    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_A:
        {
            PoLAR::DrawableObject2D* obj = newArrows(); //Add arrows
            PoLAR::DrawableArrows2D* arrow = dynamic_cast<PoLAR::DrawableArrows2D*>(obj);
            if(arrow)
            {
                arrow->setArrowShape(true); // activate the '>" shape at the arrow's end
            }
            break;
        }
        case Qt::Key_B:
        {
            newBlob(); // Add blobs
            break;
        }
        case Qt::Key_D:
        {
            bgImageToggle(); // Toggle display of the background image (here we can call directly PoLAR::ViewerStatic methods as MyViewer is a PoLAR::ViewerStatic)
            break;
        }
        case Qt::Key_E:
        {
            newSpline();
            break;
        }
        case Qt::Key_F:
        {
            QString fname = QFileDialog::getSaveFileName( this,
                                                          "Choose a filename to save the snapshot image",
                                                          QString::null,
                                                          "SVG images (*.svg)");
            if (!fname.isEmpty())
                saveObject2DAsSvg(fname);
            break;
        }

        case Qt::Key_G:
        { // Grab an image
            QString fname = QFileDialog::getSaveFileName( this,
                                                          "Choose a filename to save the snapshot image",
                                                          QString::null,
                                                          "SVG images (*.SVG)");
            if (!fname.isEmpty())
                saveAsSvg(fname);
            break;
        }
        case Qt::Key_H:
            mb.show(); // Show the help dialog
            break;
        case Qt::Key_I:
        {
            startEditImageSlot(); // Set the image interaction active
            break;
        }
        case Qt::Key_M:
        {
            newMarkers(); // Add markers
            break;
        }
        case Qt::Key_T:
        {
            int nbObj = getObjects2D(getImageIndex()).size();
            std::stringstream sstm;
            sstm << "Object" << nbObj;
            newText(QString::fromStdString(sstm.str()));
            PoLAR::DrawableText2D* textItem = static_cast<PoLAR::DrawableText2D*>(getCurrentObject2D());
            textItem->setSelectMarkerShape(PoLAR::DrawableObject2D::NONE);
            textItem->setUnselectShape(PoLAR::DrawableObject2D::NONE);
            textItem->setSelectShape(PoLAR::DrawableObject2D::NONE);
            textItem->setTextFont(QFont("Helvetica [Cronyx]", 12, QFont::Bold));
            break;
        }
        case Qt::Key_W:
        {
            // cf TaggedMarkers2D.h
            TaggedMarkers2D *customObject = new TaggedMarkers2D();
            DrawableTaggedMarkers2D *customDrawableObject = new DrawableTaggedMarkers2D(this, customObject);
            customDrawableObject->setName("MyCustomObject2D");
            addObject2D(customDrawableObject);
            startEditMarkersSlot();
            break;
        }

        case Qt::Key_K:
        {
            PoLAR::DrawableObject2D * obj = getCurrentObject2D();
            QPen select(QColor(255,0,0));
            select.setWidth(5);
            QPen unselect(QColor(0,255,0));
            unselect.setWidth(3);
            QPen selectMarker(QColor(255,255,255));
            obj->setPens(select, unselect, selectMarker);
            break;
        }
        case Qt::Key_L:
        {
            newPolyLine();
            break;
        }

        case Qt::Key_P:
        {
            newPolygon();
            break;
        }

        case Qt::Key_Q:
            this->close();
            break;
        case Qt::Key_Z:
            setZoom(4);
            break;

        case Qt::Key_R:
            getBgImage()->resetWindowLevel();
            break;

        case Qt::Key_U:
            copyObjects2D(0, 1, false);
            break;
        case Qt::Key_V:
        {
            osg::Matrixd v = getCamera()->getViewMatrix();
            osg::Matrixd i = getPanZoom()->getIntrinsics();

            std::cout << "VIEW:\n" << v << std::endl;
            std::cout << "INTRINSICS:\n" << i << std::endl;

            osg::Matrixd proj = getProjection();
            if (!proj.isNaN())
            {
                QString s=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the current projection matrix",
                                                        QString::null,
                                                        "Projection matrices (*.mat)");
                if (!s.isEmpty())
                {
                    PoLAR::Util::saveProjectionMatrix(proj, s);
                }
            }
        }
            break;

        case Qt::Key_C:
            copyObjects2D(0, false, true);
            break;

        case Qt::Key_X:

        {
            copyCurrentObject2D(false);
            break;
        }

        case Qt::Key_F1:
        {
            //setSaveFilename("toto.%06d"); // choose a filename for saving markers
            //saveMarkersSlot(); // save all the markers from the sequence, in PoLAR::DrawableObject2D save format (c.f. doxygen)
            QString fname = QFileDialog::getSaveFileName( this,
                                                          "Choose a filename to save markers",
                                                          QString::null);
            if (!fname.isEmpty())
                saveObject2D(fname);
            break;
        }
        case Qt::Key_F2:
        {
            //setLoadFilename("toto.%06d");// choose a filename for loading markers
            //loadMarkersSlot();// load markers from the sequence, the filename must be in PoLAR::DrawableObject2D format (c.f. doxygen)
            QString fname = QFileDialog::getOpenFileName( this,
                                                          "Choose a filename to load markers",
                                                          QString::null);
            if (!fname.isEmpty())
                loadObject2D(fname);
            startEditImageSlot(); // set image interaction active
            break;
        }
        case Qt::Key_F3:
        {
            setSaveFilename("titi.%04d.p2d"); // choose a filename for saving markers
            saveMarkersSlot(0); // save all the markers of the first image of the sequence (indexed 0), in PoLAR::DrawableObject2D save format (c.f. doxygen)
            break;
        }
        case Qt::Key_F4:
        {
            std::list<PoLAR::DrawableObject2D*> list = Viewer2D::getObjects2DByType('M'); // get all the PoLAR::DrawableObject2D which are typed 'M' (aka DrawableMarkers2D)
            // Set all these DrawableObject2D not to be displayed
            std::list<PoLAR::DrawableObject2D*>::iterator itrL;
            for (itrL=list.begin(); itrL != list.end(); ++itrL)
                if(toggleMarkerDisplay) (*itrL)->displayOff();
                else (*itrL)->displayOn();
            toggleMarkerDisplay = !toggleMarkerDisplay;
            startEditImageSlot();  // set image interaction active
            break;
        }
        case Qt::Key_F5:
        {
            PoLAR::DrawableObject2D *obj2D = getCurrentObject2D(); // get the current selected DrawableObject2D
            if (obj2D != NULL)
            {
                obj2D->setUnselectColor(QColor(128,128,128)); // change the unselected colour of the object2D
            }
            startEditImageSlot(); // set image interaction active
            break;
        }
        case Qt::Key_F6:
        {
            PoLAR::DrawableObject2D *obj2D = getCurrentObject2D(); // get the current selected DrawableObject2D
            if (obj2D != NULL) std::cout << (*obj2D); // print it out (you can redirect the obj2D in any std::ostream, for example you could save it in file like this : std::ofstream out("savefile.txt", std::ios::out); out << (*object2D);)
            break;
        }
        case Qt::Key_Up:
        { // change image number in the image sequence
            if (getImageIndex() == 0) gotoImageSlot(getNbImages()-1);
            else gotoImageSlot(getImageIndex()-1);
            break;
        }
        case Qt::Key_Down:
        { // change image number in the image sequence
            if (getImageIndex() == getNbImages()-1) gotoImageSlot(0);
            else gotoImageSlot(getImageIndex()+1);
            break;
        }

        default: Viewer::keyPressEvent(event);
        }
    }
private:
    QMessageBox mb;
    QString _fname;
    bool toggleMarkerDisplay;
};

#endif // _MYVIEWER2D_
