/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "MyViewer2.h"
const float MyViewer2::_sl[32] = { -33.6643,
				   -31.6363,
				   -28.6896,
				   -26.6616,
				   -23.7148,
				   -21.6869,
				   -18.7401,
				   -16.7121,
				   -13.7654,
				   -11.7374,
				   -8.79063,
				   -6.76268,
				   -3.8159,
				   -1.78795,
				   1.15884,
				   3.18679,
				   6.13357,
				   8.16152,
				   11.1083,
				   13.1363,
				   16.083,
				   18.111,
				   21.0578,
				   23.0857,
				   26.0325,
				   28.0605,
				   31.0072,
				   33.0352,
				   35.982,
				   38.0099,
				   40.9567,
				   42.99};
