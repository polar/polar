// main.cpp: implementation of the main class.
//
//////////////////////////////////////////////////////////////////////

#include <QtWidgets/QApplication>
#include "SeqViewer.h"
#include <iostream>


int main(int argc,char ** argv)
{
  if (argc < 3) {std::cerr << "Syntax: visualise <grx> <pgm> [<init> <step>]\n";  exit(0);}
  int init=0;
  int step=1;
  if (argc > 3)
    init = atoi(argv[3]);
  if (argc > 4)
    step = atoi(argv[4]);

  QApplication app(argc, argv);

  // scene creation
  SeqViewer *viewer=new SeqViewer(1, argv[2], argv[1], init, step);
  viewer->setWindowTitle("Visualise");

  viewer->show();

  app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

  return app.exec();
}
