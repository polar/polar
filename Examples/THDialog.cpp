/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "THDialog.h"

#include <QVariant>
#include <QSlider>
#include <QPushButton>
#include <QLayout>
#include <QToolTip>
#include <QWhatsThis>
#include <QGroupBox>
#include <QLabel>

/*
 *  Constructs a MyDialog as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */

THDialog::THDialog( QWidget* parent, Qt::WindowFlags fl )
    : QDialog( parent, fl )
{
    THDialogLayout = new QVBoxLayout( this);

    frame = new QFrame( this );
    frameLayout = new QVBoxLayout(frame);

    THDialogLayout->addWidget( frame );

    OKbutton = new QPushButton( "OK", this );
    THDialogLayout->addWidget( OKbutton );

    languageChange();

    // signals and slots connections
    connect( OKbutton, SIGNAL( pressed() ), this, SLOT( hide() ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
THDialog::~THDialog()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void THDialog::languageChange()
{
    setWindowTitle( tr( "Mode Dialog" ) );
    OKbutton->setText( tr( "Close" ) );
}

QSlider *THDialog::addSlider(int i)
{
    QGroupBox *groupbox = new QGroupBox(frame);
    groupbox->setAlignment(Qt::Vertical );
    QVBoxLayout *layout=new QVBoxLayout(groupbox);
    groupbox->setLayout(layout);
    groupbox->layout()->setSpacing( 6 );
    groupbox->layout()->setMargin( 11 );
    groupbox->setTitle( QString("Mode %1").arg(i) );
    QLabel *l=new QLabel(groupbox);

    //  QVBoxLayout *layout=new QVBoxLayout(*groupbox->layout());

    QSlider *s=new QSlider(Qt::Horizontal,groupbox);
    s->setMinimum(-100);
    s->setMaximum(100);
    s->setPageStep(10);

    s->setTracking(true);
    l->setNum(s->value());
    QObject::connect(s, SIGNAL(valueChanged(int)), l, SLOT(setNum(int)));
    layout->addWidget( l );
    layout->addWidget( s );

    frameLayout->addWidget(groupbox);

    return s;
}
