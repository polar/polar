/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include <osgDB/ReadFile>
#include <BulletSoftBody/btSoftBodyHelpers.h>

#include "MyViewerDeformation.h"
#include "Image.h"
#include "Util.h"
#include "SimulationManager.h"
#include "BulletWorker.h"
#include "BulletObject3D.h"
#include "BulletSoftObject3D.h"
#include "BulletUtil.h"



btSoftBody *makeFlag()
{
    const short resX(12), resY(9);
    const osg::Vec3 llCorner(-2., 0., 5.);
    const osg::Vec3 uVec(4., 0., 0.);
    const osg::Vec3 vVec(0., 0.1, 3.); // Must be at a slight angle for wind to catch it.

    // Create the soft body using a Bullet helper function.
    btSoftBodyWorldInfo& worldInfo = *(PoLAR::Bullet::DynamicsWorld::getWorldInfo());
    btSoftBody* softBody( btSoftBodyHelpers::CreatePatch(worldInfo,
                                                         PoLAR::Bullet::asBtVector3(llCorner),
                                                         PoLAR::Bullet::asBtVector3(llCorner + uVec),
                                                         PoLAR::Bullet::asBtVector3(llCorner + vVec),
                                                         PoLAR::Bullet::asBtVector3(llCorner + uVec + vVec),
                                                         resX, resY, 1+4, true));

    // Configure the soft body for interaction with the wind.
    softBody->getCollisionShape()->setMargin(0.1);
    softBody->m_materials[0]->m_kLST = 0.3;
    softBody->generateBendingConstraints(2, softBody->m_materials[0]);
    softBody->m_cfg.kLF = 0.05;
    softBody->m_cfg.kDG = 0.01;
    softBody->m_cfg.piterations = 2;
    softBody->m_cfg.aeromodel = btSoftBody::eAeroModel::V_TwoSidedLiftDrag;
    softBody->setWindVelocity(btVector3(50., 0., 0.));
    softBody->setTotalMass(1.);
    return softBody;
}



int main(int argc,char ** argv)
{
    //osg::ArgumentParser arguments( &argc, argv );
    //const bool debugDisplay( arguments.find( "--debug" ) > 0 );

    if (argc < 5)
    {
        std::cerr << "Syntax: " << argv[0] << " <file.ppm> <sphere file> <cube file> <flag texture> <projection>" <<std::endl;
        return 0;
    }

    QApplication app(argc, argv);

    int width;
    int height;
    osg::ref_ptr<PoLAR::Image_uc> myImage;

    // Create the viewer
    MyViewerDeformation viewer;

    // Read the image given in parameter
    if(PoLAR::Util::fileExists(argv[1]))
        myImage = new PoLAR::Image_uc(argv[1], true);
    else
    {
        std::cerr << argv[1] << ": file not found" << std::endl;
        return 1;
    }
    width = myImage->getWidth();
    height = myImage->getHeight();
    viewer.resize(width,height);

    // Add the image read as background image
    viewer.setBgImage(myImage);

    // Show it
    viewer.bgImageOn();

    // Have the image interaction active
    viewer.startEditImageSlot();

    // add a light for the shadowing
    viewer.addLightSource(-3.0, -10.0, 30.0, true, false, 0, 0, 0, 0.2, 0.2, 0.2);

    // Bullet world
    btSoftRigidDynamicsWorld* bw = PoLAR::Bullet::DynamicsWorld::getDynamicsWorld();

    // create a flag
    btSoftBody *flagSoftBody = makeFlag();
    if(flagSoftBody)
    {
        // generate a graphical object from its physical representation
        osg::ref_ptr<PoLAR::BulletSoftObject3D> flag = new PoLAR::BulletSoftObject3D(flagSoftBody);

        // add a texture on the flag
        if(PoLAR::Util::fileExists(argv[4]))
        {
            osg::ref_ptr<PoLAR::Image_uc> flagTexture = new PoLAR::Image_uc(argv[4]);
            flag->generateTextureCoordinates();
            flag->setObjectTextureState(flagTexture.get());
        }
        flag->translate(2,13,0);
        flag->setName("flag");
        bw->addSoftBody(flag->getBtSoftBody());
        viewer.addObject3D(flag.get());

        // create the flag stick
        osg::ref_ptr<osg::Geode> stickGeode = new osg::Geode;
        osg::ref_ptr<osg::ShapeDrawable> shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.0f, 0.0f, 4.0f), 0.1f, 0.1f, 4.0f));
        stickGeode->addDrawable(shape.get());
        osg::ref_ptr<PoLAR::Object3D> stick = new PoLAR::Object3D(stickGeode.get(), false, true, true);
        stick->translate(0,13,0);
        viewer.addObject3D(stick.get());
        stick->scale(2);
        stick->setMaterial(0.1,0.1,0.1,0.,0.,0.,0.05,0.05,0.05);
    }

    // create the sphere
    if(PoLAR::Util::fileExists(argv[2]))
    {
        osg::ref_ptr<PoLAR::BulletSoftObject3D> sphere = new PoLAR::BulletSoftObject3D(argv[2]);
        sphere->getDrawable(0)->asGeometry()->setUseVertexBufferObjects(true);
        sphere->setName("sphere");
        //sphere->scale(3.);
        sphere->translate(6.,8.,11.);
        //sphere->rotate(5,1,1,1);
        sphere->createSoftBody();
        btSoftBody *softbody = sphere->getBtSoftBody();
        btSoftBody::Material* pm = softbody->appendMaterial();
        pm->m_kLST = 0.45;
        softbody->generateBendingConstraints(5, pm);
        //softbody->m_cfg.kVC = 200.; //Volume conservation coeff
        //softbody->setPose(true, false);
        //softbody->m_cfg.kDF = 1.; // dynamic friction coeff
        bw->addSoftBody(softbody);
        viewer.addObject3D(sphere.get());
    }
    else
    {
        osg::notify(osg::FATAL) << "Cannot open model " << argv[2] << std::endl;
    }

    // create the cube
    if(PoLAR::Util::fileExists(argv[3]))
    {
        osg::ref_ptr<PoLAR::BulletObject3D> rigidCube = new PoLAR::BulletObject3D(argv[3], 10);
        rigidCube->setEditOn();
        rigidCube->setName("rigidCube");
        rigidCube->scale(5.0);
        rigidCube->translate(6.,8.,5.);
        //staticCube->rotate(0.5,1,1,1);
        rigidCube->createRigidBody();
        viewer.addObject3D(rigidCube.get());
        bw->addRigidBody(rigidCube->getBtRigidBody());
    }
    else
    {
        osg::notify(osg::FATAL) << "Cannot open model " << argv[3] << std::endl;
    }

    // Add ground
    {
        const osg::Vec4 plane(0., 0., 1., 0.);
        osg::ref_ptr<PoLAR::BulletObject3D> ground = new PoLAR::BulletObject3D(PoLAR::Util::generatePlaneFromNormal(plane), 0.f);
        ground->setName("ground");
        const btVector3 planeNormal(plane.x(), plane.y(), plane.z());
        ground->createRigidBody(new btStaticPlaneShape(planeNormal, plane.w()));

        bw->addRigidBody(ground->getBtRigidBody());
        viewer.addObject3D(ground);
        ground->setEditOn();
        ground->setPhantomOn();
    }

    // Prepare the simulation launch
    osg::ref_ptr<PoLAR::BulletWorker> worker = new PoLAR::BulletWorker(bw);
    osg::ref_ptr<PoLAR::SimulationManager> manager = new PoLAR::SimulationManager(worker, 33.0, false);
    manager->launch();
    viewer.setSimMan(manager.get());

    // set the projection
    PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION;
    osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[5]);
    viewer.setProjection(P, pt);


    // Show the widget
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
