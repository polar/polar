/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWEROVERPAINTED_
#define _MYVIEWEROVERPAINTED_


#include <QFileDialog>
#include <QMessageBox>
#include <QObject>
#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <iostream>
#include <fstream> 
#include <PoLAR/Viewer.h>

class MyViewerOverPainted : public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewerOverPainted(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f, true),
        _mb(QMessageBox::Information,
            QString("Help: shortkeys"),
            QString("Shortkeys:\n")+
            QString("H: this help\n")+
            QString("Q: quit the application\n")+
            QString("I: edit image mode (pan/zoom)\n")+
            QString("M: add/edit markers mode (splines)\n")+
            QString("W: manipulate scene mode\n")+
            QString("T: Toggle overlay\n")+
            QString("Down: goto next image\n")+
            QString("Up: goto previous image\n")+
            QString("D: toggle background image display\n")+
            QString("G: grab a window snapshot\n")+
            QString("else default openscenegraph keys\n"),
            QMessageBox::NoButton,
            this),
        _overlayOn(true)
    {
        addEventHandler(new osgViewer::StatsHandler);

        setOverlay(true); // this method connects the paintOverlay() signal from the drawing scene to the overpainting2D() virtual method.
        _mb.setWindowModality(Qt::NonModal);
    }


public slots:
    /**
     * @brief Reimplement this method to draw on overlay of the viewer, using the given painter
     */
    void overpainting2D( QPainter *painter, const QRectF&)
    {
        QString text = tr("Example text to test Qt overpainting. "
                          "it rocks.");
        QFontMetrics metrics = QFontMetrics(font());
        int border = qMax(4, metrics.leading());

        QRect rect = metrics.boundingRect(0, 0, width() - 2*border, int(height()*0.125),
                                          Qt::AlignCenter | Qt::TextWordWrap, text);
        painter->setRenderHint(QPainter::TextAntialiasing);
        painter->setPen(Qt::white);
        painter->fillRect(QRect(0, 0, width(), rect.height() + 2*border),
                          QColor(0, 0, 0, 127));
        painter->drawText((width() - rect.width())/2, border,
                          rect.width(), rect.height(),
                          Qt::AlignCenter | Qt::TextWordWrap, text);

        QString magritText = tr("Copyright Magrit project INRIA");
        QFont serifFont("Times", 30, QFont::Bold);
        QColor color(128, 0, 128, 180);
        painter->setFont(serifFont);
        painter->setPen(color);
        painter->drawText(0, 0, width(), height(), Qt::AlignCenter | Qt::TextWordWrap, magritText);

        painter->setPen(Qt::red);
        painter->drawRect(0,1,width()-1,height()-1);
    }



    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            _mb.show();
            break;
        case Qt::Key_Q:
            close();
            break;
        case Qt::Key_D:
        {
            bgImageToggle();
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot();
            break;
        }
        case Qt::Key_M:
        {
            newSpline();
            break;
        }
        case Qt::Key_W:
        {
            startManipulateSceneSlot();
            break;
        }
        case Qt::Key_T:
        {
            if(_overlayOn)
                setOverlay(false);
            else
                setOverlay(true);
            _overlayOn = !_overlayOn;
        }
        case Qt::Key_Up:
        {
            if (getImageIndex() == 0) gotoImageSlot(getNbImages()-1);
            else gotoImageSlot(getImageIndex()-1);
            startEditImageSlot();
            break;
        }
        case Qt::Key_Down:
        {
            if (getImageIndex() == getNbImages()-1) gotoImageSlot(0);
            else gotoImageSlot(getImageIndex()+1);
            startEditImageSlot();
            break;
        }
        case Qt::Key_G:
        {
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");
            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }
        default: Viewer::keyPressEvent(event);
        }
    }
private:
    QMessageBox _mb;
    bool _overlayOn;
};

#endif // _MYVIEWEROVERPAINTED_
