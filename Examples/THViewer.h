/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _THVIEWER_
#define _THVIEWER_

#include <QFileDialog>
#include <QMessageBox>
#include <QObject>
#include <QSlider>
#include <QApplication>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>
#include "THDialog.h"

const float _dec=1;


class THViewer : public PoLAR::Viewer
{
    Q_OBJECT
public:
    THViewer(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f),
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("I: edit image mode (pan/zoom)\n")+
           QString("W: manipulate scene mode\n")+
           QString("X: toggle 3D display\n")+
           QString("G: grab a window snapshot\n")+
           QString("M: show the mode dialog\n")+
           QString("P: play a sequence of PCA coeffs\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this),
        current_mode(0)
    {
        addEventHandler(new osgViewer::StatsHandler);
        mb.setWindowModality(Qt::NonModal);
    }
    void addMode(osg::Geometry *g)
    {
        modes.push_back(dynamic_cast<osg::Vec3Array*>(g->getVertexArray()->clone(osg::CopyOp::DEEP_COPY_ARRAYS)));
        coefs.push_back(0);
        QSlider *s=modeDialog.addSlider(sliders.size());
        sliders.push_back(s);
        QObject::connect(s,SIGNAL(valueChanged(int)), this, SLOT(updateMode(int)));
        QObject::connect(s,SIGNAL(sliderPressed()), this, SLOT(updateCurrentMode()));
    }
    void setCoefs(std::vector<float> c)
    {
        if (c.size() != coefs.size()) return;
        coefs = c;
        updateModel();
    }
    void setSaveFileName(QString f) {savefname=f;}

public slots:
    void updateCurrentMode()
    {
        current_mode=0;
        while (sliders[current_mode] != (QSlider*)sender()) current_mode++;
        std::cout << "CURRENT MODE " << current_mode << std::endl;
    }
    void updateMode(int val)
    {
        float dv=val-coefs[current_mode];
        osg::Vec3Array *v=dynamic_cast<osg::Vec3Array*>(model->getVertexArray());
        unsigned int i=0;
        for (i=0; i<v->size(); i++)
            (*v)[i] += (*modes[current_mode])[i]*dv;
        coefs[current_mode] += dv;
        model->dirtyDisplayList();
        update();
        model->dirtyBound();

    }

    void updateModel()
    {
        osg::Vec3Array *v=dynamic_cast<osg::Vec3Array*>(model->getVertexArray());
        unsigned int i=0;
        (*v)=*average;
        for (i=0; i<coefs.size(); i++)
        {
            osg::Vec3Array::iterator vit=v->begin();
            osg::Vec3Array::iterator mit=modes[i]->begin();
            for (;vit != v->end(); ++vit, ++mit)
                (*vit) += (*mit)*coefs[i];
        }
        model->dirtyDisplayList();
        update();
    }
    void setModel(PoLAR::Object3D *o)
    {
        addObject3D(o);
        model = dynamic_cast<osg::Geometry*>(dynamic_cast<osg::Geode*>(o->getOriginalNode())->getDrawable(0));
        average = dynamic_cast<osg::Vec3Array*>(model->getVertexArray()->clone(osg::CopyOp::DEEP_COPY_ARRAYS));
    }
    
    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            mb.show();
            break;
        case Qt::Key_Q: ::exit(0);
        case Qt::Key_I:
        {
            startEditImageSlot();
            break;
        }
        case Qt::Key_W:
        {
            startManipulateSceneSlot();
            break;
        }
        case Qt::Key_M:
        {
            modeDialog.show();
            break;
        }
        case Qt::Key_G:
        {
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");
            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }
        case Qt::Key_X:
        {
            osg::ref_ptr<PoLAR::Object3D> o=getObject3D(0);
            if (o.valid())
            {
                if (o->isDisplayed()) o->setDisplayOff();
                else o->setDisplayOn();
            }
            break;
        }
        case Qt::Key_P:
        {
            QString fname=QFileDialog::getOpenFileName( this,
                                                        "Choose which coefficient sequence to load",
                                                        QString::null,
                                                        "Sequences files (*.coef);;All files (*.*)");
            if (!fname.isEmpty())
            {
                QFile cfile(fname);
                unsigned int n=0;
                cfile.open(QIODevice::ReadOnly);
                while(!cfile.atEnd())
                {
                    char buf[2048];
                    cfile.readLine(buf, 2048);
                    QString l(buf);
                    unsigned int i;
                    for (i=0; i<coefs.size(); i++)
                    {
                        double val=l.section(" ", i,i,QString::SectionSkipEmpty).toFloat();
                        coefs[i] = val;
                        //std::cout << val << " ";
                    }
                    //		    std::cout << std::endl;
                    updateModel();
                    QApplication::processEvents();
                    if (!savefname.isEmpty())
                    {
                        QString tbuf=savefname;
                        grab().save(tbuf.sprintf(tbuf.toLatin1(),n).append(".png"), "PNG");
                    }
                    n++;
                }
                cfile.close();
            }
            break;
        }
        default: Viewer::keyPressEvent(event);
        }
    }
private:
    
    QMessageBox mb;
    osg::Geometry *model;
    std::vector<osg::Vec3Array*> modes;
    std::vector<float> coefs;
    int current_mode;
    osg::Vec3Array *average;
    std::vector<QSlider *> sliders;
    THDialog modeDialog;
    QString savefname;
};

#endif // _THVIEWER_
