/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "basicInterface3D2.h"
#include "props.h"
#include <QColorDialog>

using namespace std;

Interface::Interface()
{
    QString dir = dataPath() + "icons/";
    imgframe = new QSplitter;
    imgframe->setOrientation(Qt::Horizontal);
    setCentralWidget(imgframe);

    // Create a Viewer with shadows management
    ViewerL = new PoLAR::Viewer(0, "ViewerL", 0, true);

    // Create a Viewer without shadows management
    ViewerR =  new PoLAR::Viewer(0, "ViewerR");

    ViewerL->addEventHandler(new osgViewer::StatsHandler);// add stats handler
    ViewerR->addEventHandler(new osgViewer::StatsHandler);// add stats handler
    _pick = new PoLAR::TabBoxPickHandler; // create pick handler
    _pick->setCharacterSize(15);
    ViewerL->addEventHandler(_pick);// add pick handler

    // Create an object3D from frameaxis class and add it to the viewer (here we add a 3D representation of the world axis)
    ViewerR->addObject3D(new PoLAR::Object3D(new PoLAR::FrameAxis(), false, false, true));

    imgframe->addWidget(ViewerL);
    imgframe->addWidget(ViewerR);

    toolbar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea, toolbar);

    toolbar->setOrientation(Qt::Vertical);

    statuslabel = new QLabel(toolbar);
    statuslabel->setText(*(new QString("Tools")));

    toolbar->addAction(QIcon(dir+"exit.png"), QString("Exit"), this, SLOT(exit()));

    QAction *editImgAct = new QAction(QIcon(dir+"img.png"), tr("edit image"), this);
    editImgAct->setStatusTip(tr("Edit Image"));
    QObject::connect(editImgAct, SIGNAL(triggered()), this, SLOT(startEditImageSlot()));
    QObject::connect(editImgAct, SIGNAL(triggered()), this, SLOT(deactivateLabelBox()));
    toolbar->addAction(editImgAct);

    toolbar->addAction(QIcon(dir+"edit.png"), QString("Edit Markers"), this, SLOT(newSpline()));

    toolbar->addAction(QIcon(dir+"save.png"), QString("Save Markers"), this, SLOT(saveMarkers()));

    labelBox = new QSpinBox(this);
    labelBox->setRange(-1,19);
    labelBox->setSingleStep(1);
    toolbar->addWidget(labelBox);

    deactivateLabelBox();
    QObject::connect(labelBox, SIGNAL(valueChanged(int)), this, SLOT(changeMarkerLabelSlot(int)));
    QObject::connect(ViewerL, SIGNAL(selectMarkerSignal(Marker2D*)), this, SLOT(activateLabelBox(Marker2D*)));
    QObject::connect(ViewerL, SIGNAL(unselectMarkerSignal()), this, SLOT(deactivateLabelBox()));
    QObject::connect(ViewerR, SIGNAL(selectMarkerSignal(Marker2D*)), this, SLOT(activateLabelBox(Marker2D*)));
    QObject::connect(ViewerR, SIGNAL(unselectMarkerSignal()), this, SLOT(deactivateLabelBox()));

    scrollbarTB = new QToolBar(this);
    scrollbarTB->setOrientation(Qt::Vertical);

    scrollbar = new QScrollBar(this);
    scrollbar->setMinimum(0);
    scrollbar->setMaximum(0);
    scrollbar->setPageStep(10);
    scrollbar->setValue(0);
    scrollbar->setFixedHeight(height());
    scrollbar->setOrientation(Qt::Vertical);
    scrollbarTB->addWidget(scrollbar);

    QObject::connect(scrollbar, SIGNAL(valueChanged(int)), this, SLOT(gotoImageSlot(int)));
    QObject::connect(scrollbar, SIGNAL(valueChanged(int)), this, SLOT(changeTitle(int)));

    scrollbarTB->update();
    addToolBar(Qt::RightToolBarArea, scrollbarTB);

    move(0,0);

    QObject::connect(this, SIGNAL(startEditImageSignal()), this, SLOT(startEditImageSlot()));
    QObject::connect(this, SIGNAL(startEditMarkersSignal()), this, SLOT(startEditMarkersSlot()));

    toolbar->addAction(QIcon(dir+"scene.png"), QString("Manipulate Scene"), this, SLOT(startManipulateSceneSlot()));

    // In the following configuration, we have a world light active in both windows, and a headlight only active in the right one

    // create a PoLAR::Light with the default headlight (number 0) already attached to scene graph of the left window
    osg::ref_ptr<PoLAR::Light> headlight = ViewerL->getLightSource(0);
    // switch it off
    headlight->setOff();

    // create a PoLAR::Light with the default headlight (number 0) already attached to scene graph of the right window
    _light = ViewerR->getLightSource(0);

    // connect it to a basic light dialog
    PoLAR::BasicLightDialog *basicLightDialog = new PoLAR::BasicLightDialog(ViewerR, _light, this);
    QPushButton *moreButton2 = new QPushButton(QString("Headlight parameters \n(only active in  \nthe right window)"), this);
    toolbar->addWidget(moreButton2);

    QObject::connect(moreButton2, SIGNAL(pressed()), basicLightDialog, SLOT(show()));

    // create a PoLAR::Light corresponding to a world light positionned in (5.0,2.0,5.0) with a 3D representation in right frame
    _light2 = ViewerL->addLightSource(5.0, 2.0, 5.0, true);
    _light2->addViewableObject(ViewerR->getSceneGraph());
    // use this light to light up in the left frame too (respect the coherency of the scene since _light2 is a world light, in opposition to _light which is a headlight and is related to the frame's camera)
    ViewerR->addLightSource(_light2);

    // connect it to a light dialog
    PoLAR::LightDialog *lightDialog2 = new PoLAR::LightDialog((PoLAR::Viewer2D3D*)ViewerR, _light2, this);
    QPushButton *moreButton3 = new QPushButton(QString("Worldlight parameters"), this);
    toolbar->addWidget(moreButton3);

    QObject::connect(moreButton3, SIGNAL(pressed()), lightDialog2, SLOT(show()));

    // To see the camera movement according to the manipulation in the left window in the right one
    osg::ref_ptr<PoLAR::ViewableCamera> viewCam = new PoLAR::ViewableCamera(ViewerL->getCamera(), true);
    ViewerR->addObject3D(new PoLAR::Object3D(viewCam, false, false, true));
    QObject::connect(ViewerL, SIGNAL(repainted()), viewCam, SLOT(update()));

    // connect the right viewer update request signal on the left viewer updated signal so that the right viewer is updated too
    QObject::connect(ViewerL, SIGNAL(repainted()), ViewerR, SLOT(requestRedraw()));

    QPushButton *resetPathButton = new QPushButton(QString("Delete camera path"), this);
    toolbar->addWidget(resetPathButton);

    QObject::connect(resetPathButton, SIGNAL(pressed()), viewCam, SLOT(resetPath()));

    // add event filter to this interface in order to get the keyboard events
    KeyPressManager *keyPressManager = new KeyPressManager(this);
    ViewerL->installEventFilter(keyPressManager);
    ViewerR->installEventFilter(keyPressManager);

    mb = new QMessageBox(
                QMessageBox::Information,
                QString("Help: shortkeys"),
                QString("Shortkeys:\n")+
                QString("H: this help\n")+
                QString("Q: quit the application\n")+
                QString("I: edit image mode (pan/zoom/window level)\n")+
                QString("M: add/edit markers mode (Spline)\n")+
                QString("W: manipulate scene mode\n")+
                QString("D: toggle background image display\n")+
                QString("G: grab a window snapshot\n")+
                QString("0: switch in camera manipulation mode, only active in 3D manipulation mode (default)\n")+
                QString("1: switch in picking mode, only active in 3D manipulation mode\n")+
                QString("F: set a specific edition frame for the current picked object\n")+
                QString("P: toggle phantom state of the current picked object\n")+
                QString("F1: change dragger type : translate axis\n")+
                QString("F2: change dragger type : tabbbox\n")+
                QString("else default openscenegraph keys\n"),
                QMessageBox::NoButton,
                this);
    mb->setWindowModality(Qt::NonModal);
}

Interface::~Interface()
{
}


void Interface::activateLabelBox(Marker2D *m)
{
    labelBox->setEnabled(true);
    if (m) labelBox->setValue((int)m->label());
}

void Interface::deactivateLabelBox()
{
    labelBox->setValue(-1);
    labelBox->setEnabled(false);
}

void Interface::changeTitle(int n)
{
    char buf[256];
    sprintf(buf, "Image %3d",n);
    statuslabel->setText(*(new QString(buf)));
}

void Interface::newSpline()
{
    ViewerL->newSpline();
    ViewerL->getCurrentObject2D()->setProperties(QColor("black"),
                                                 QColor("red"),
                                                 QColor("yellow"),
                                                 PoLAR::DrawableObject2D::NONE,
                                                 PoLAR::DrawableObject2D::CIRCLE,
                                                 PoLAR::DrawableObject2D::SQUARE,
                                                 5, 10, 2);
}

void Interface::saveMarkers()
{
    QString fname = QFileDialog::getSaveFileName( this,
                                                  "Choose a filename to save the markers",
                                                  QString::null);
    if (!fname.isEmpty()) ViewerL->saveObject2D(fname);

}

void Interface::startEditImageSlot()
{
    // To determine which Viewer is active
    if (ViewerL->hasFocus()) ViewerL->startEditImageSlot();
    if (ViewerR->hasFocus()) ViewerR->startEditImageSlot();
}

void Interface::changeMarkerLabelSlot(int l)
{
    // To determine which Viewer is active
    if (ViewerL->hasFocus()) ViewerL->changeMarkerLabelSlot(l);
    if (ViewerR->hasFocus()) ViewerR->changeMarkerLabelSlot(l);
}

void Interface::gotoImageSlot(int i)
{
    // To determine which Viewer is active
    if (ViewerL->hasFocus()) ViewerL->gotoImageSlot(i);
    if (ViewerR->hasFocus()) ViewerR->gotoImageSlot(i);
}

void Interface::startEditMarkersSlot()
{
    // To determine which Viewer is active
    if (ViewerL->hasFocus()) ViewerL->startEditMarkersSlot();
    if (ViewerR->hasFocus()) ViewerR->startEditMarkersSlot();
}

void Interface::startManipulateSceneSlot()
{
    // To determine which Viewer is active
    if (ViewerL->hasFocus()) ViewerL->startManipulateSceneSlot();
    if (ViewerR->hasFocus()) ViewerR->startManipulateSceneSlot();
}

// Phantomize the picked object
void Interface::pickedObjectPhantom()
{
    osg::ref_ptr<PoLAR::Object3D> obj = ViewerL->getCurrentPickedObject();
    if (obj!=NULL)
    {
        if (obj->isAPhantom()) obj->setPhantomOff();
        else obj->setPhantomOn();
    }
}

// Change the frame in which the picked object will be edited
void Interface::pickedObjectFrame()
{
    osg::ref_ptr<PoLAR::Object3D> obj = ViewerL->getCurrentPickedObject();
    if (obj!=NULL) obj->setFrameMatrix(osg::Matrixd::translate(0.0,0.0,0.0));
}

void Interface::keyPressEvent(QKeyEvent *event)
{
    // To determine which Viewer is active
    PoLAR::Viewer* viewer = NULL;
    if (ViewerL->hasFocus()) viewer = ViewerL;
    if (ViewerR->hasFocus()) viewer = ViewerR;

    if(viewer)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            mb->show();
            break;
        case Qt::Key_Q:
        {
            close();
            break;
        }
        case Qt::Key_D:
        {
            viewer->bgImageToggle();
            break;
        }
        case Qt::Key_I:
        {
            viewer->startEditImageSlot();
            break;
        }
        case Qt::Key_M:
        {
            newSpline();
            break;
        }
        case Qt::Key_W:
        {
            viewer->startManipulateSceneSlot();
            break;
        }
        case Qt::Key_F:
        {
            pickedObjectFrame();
            break;
        }
        case Qt::Key_P:
        {
            pickedObjectPhantom();
            break;
        }
        case Qt::Key_F1:
        {
            translateaxisDragger();
            break;
        }
        case Qt::Key_F2:
        {
            tabboxDragger();
            break;
        }
        case Qt::Key_G:
        {
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");

            if (!fname.isEmpty()) viewer->grab().save(fname, "PNG");
            break;
        }

        default:
            //viewer->notify(event);
            break;
        }
    }
}
