/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>

#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>

#include <iostream>
#include <PoLAR/Image.h>
#include "ImageSequence.h"
#include "MyViewer2D.h"


int main(int argc,char ** argv)
{
    //osg::setNotifyLevel(osg::DEBUG_FP);
    if (argc < 7)
    {
        std::cerr << "Syntax: " << argv[0] << " <data> <width> <height> <nima> <header offset> <pixel type> [<proj> <3d model> <projType> <savename>]\n";
        exit(0);
    }

    QApplication app(argc, argv);
    int width=atoi(argv[2]);
    int height=atoi(argv[3]);
    int nima=atoi(argv[4]);
    int offset=atoi(argv[5]);
    float* sequence = readData(argv[1],width,height,nima,offset,argv[6]);
    if (!sequence) std::cerr << "Could not read image data" << std::endl;

    // scene creation
    MyViewer2D viewer;
    unsigned int zoom = 2;
    viewer.resize(width*zoom, height*zoom);
    viewer.setZoom(zoom);

    viewer.setBgImage<float>(sequence, width, height, 1, nima);
    viewer.bgImageOn();
    viewer.startEditImageSlot();

    PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION;
    if (argc > 9)
    {
        switch(atoi(argv[9]))
        {
        case 1: pt=PoLAR::Viewer2D3D::ANGIO; break;
        case 2: pt=PoLAR::Viewer2D3D::ANGIOHINV; break;
        default: pt=PoLAR::Viewer2D3D::VISION;
        }
    }

    // Load the projection given in parameter
    if (argc > 7)
    {
        osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[7]);
        viewer.setProjection(P, pt);
    }

    if (argc > 8)
    {
        osg::ref_ptr<osg::Node> loadedModel;
        loadedModel = osgDB::readNodeFile(argv[8]);
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;
        if(!loadedModel)
        {
            std::cerr << "Unable to load model" << std::endl;
            exit(0);
        }
        optimizer.optimize(loadedModel.get());
        loadedModel->accept(smooth);

        PoLAR::Object3D *object3D = new PoLAR::Object3D(loadedModel.get(), true);
        object3D->setShininess(0.7);
        object3D->setAmbientColor(1,1,1);
        object3D->setDiffuseColor(0.7, 0.7, 0.7);
        object3D->setSpecularColor(1,1,1);
        viewer.addObject3D(object3D);

        viewer.setTrackNode(loadedModel.get());
    }

    if (argc > 10) viewer.setSaveFName(argv[10]);

    viewer.center();
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}

