/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <PoLAR/DrawableBlob2D.h>
#include <fstream>

int main(int argc, char *argv[])
{
    if(argc != 4)
    {
        std::cerr << "syntax: " << argv[0] << " <in file> <nb of samples> <out file>" << std::endl;
    }
    std::ifstream in(argv[1],std::ios::in);
    PoLAR::DrawableBlob2D *db=(PoLAR::DrawableBlob2D *)PoLAR::DrawableObject2D::load(in,0);
    in.close();
    PoLAR::Blob2D *b=(PoLAR::Blob2D *)db->getObject();

    std::list<PoLAR::Marker2D*> l = b->resample(atoi(argv[2]));

    delete db;
    PoLAR::DrawableBlob2D db2(l,0);
    std::ofstream out(argv[3], std::ios::out);
    db2.save(out);
    out.close();
    return 0;
}
