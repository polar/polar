/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWER_
#define _MYVIEWER_

#include <QFileDialog>
#include <QMessageBox>
#include <QObject>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>
#include <PoLAR/BlendDialog.h>
#include <PoLAR/Util.h>

class MyViewer : public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewer(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f, true), // MyViewer inherits from PoLAR::Viewer
        // Help messageBox creation
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("I: edit image mode (pan/zoom/window level)\n")+
           QString("M: add/edit markers mode\n")+
           QString("W: manipulate scene mode\n")+
           QString("D: toggle background image display\n")+
           QString("G: grab a window snapshot\n")+
           QString("B: show blend dialog\n")+
           QString("P: print projection matrix in console / save it in file\n")+
           QString("T: remove 3D object of index 0\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this),
        _blendDialog(this)
    {
        // Add native OpenSceneGraph stats handler
        addEventHandler(new osgViewer::StatsHandler);

        mb.setWindowModality(Qt::NonModal);
    }

    void addObject3D(PoLAR::Object3D *o)
    {
        if (!o) std::cout << "NO OBJECT\n";
        // Add an Object3D to this viewer
        Viewer::addObject3D(o);

        // Set the blend dialog to be active on the added object
        _blendDialog.setObject(o);
    }

public slots:
    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            mb.show(); // Show the help dialog
            break;
        case Qt::Key_Q:
        {
            close();
            break;
        }
        case Qt::Key_D:
        {
            bgImageToggle(); // Toggle display of the background image (here we can call directly PoLAR::ViewerStatic methods as MyViewer is a PoLAR::ViewerStatic)
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot(); // Set the image interaction active
            break;
        }
        case Qt::Key_M:
        {
            newMarkers(); // Add markers
            break;
        }
        case Qt::Key_W:
        {
            startManipulateSceneSlot();// Set the 3D interaction active
            break;
        }
        case Qt::Key_G:
        { // Grab an image
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");

            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }

        case Qt::Key_B:
        { // Show the blend dialog if hidden
            if (_blendDialog.isHidden()) _blendDialog.show();
            break;
        }
        case Qt::Key_P:
        {
            osg::Matrixd P = getProjection();
            std::cout << P << std::endl;
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null);

            if (!fname.isEmpty()) PoLAR::Util::saveProjectionMatrix(P, fname);
            break;
        }
        case Qt::Key_T:
        {
            osg::ref_ptr<PoLAR::Object3D> o=getObject3D(0);
            if(o.valid())
            {
                setTrackNode(getSceneGraph()->getScene());
                removeObject3D(o.get());
            }
            return;
        }
        default: Viewer::keyPressEvent(event);
        }
    }
private:
    QMessageBox mb;
    PoLAR::BlendDialog _blendDialog;
};

#endif // _MYVIEWER_
