/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <iostream>
#include <PoLAR/Image.h>
#include <PoLAR/Viewer.h>
#include <osgDB/ReadFile>
#include <osg/Stats>

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    int width;
    int height;
    osg::ref_ptr<PoLAR::Image_uc> myImage;

    // Create the viewer, here the widget used to display the image read is a native PoLAR viewer
    PoLAR::Viewer viewer(0, "MyVerySimpleViewer", 0, false, false);

    // Read the image given in parameter
    if (argc == 2)
    {
        if(PoLAR::Util::fileExists(argv[1]))
            myImage = new PoLAR::Image_uc(argv[1], true, 1);
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
        width = myImage->getWidth();
        height = myImage->getHeight();

        // Resize the widget function of the size of the image read
        viewer.resize(width, height);

        // Add the image read as background image
        viewer.setBgImage(myImage.get());

        // Show it
        viewer.bgImageOn();

        // Have the image interaction active
        viewer.startEditImageSlot();

        // Show the widget
        viewer.center();
        viewer.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image>" << std::endl;
        exit(0);
    }

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
