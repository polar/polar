/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <iostream>
#include <PoLAR/Image.h>
#include <PoLAR/VideoPlayer.h>
#include "MyViewerCameraQT2.h"

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);

    osg::ref_ptr<PoLAR::Image_uc> myImage;
    osg::ref_ptr<PoLAR::VideoPlayer> camera;
    int width = 640;
    int height = 480;
    int depth = 3;
    int cameraNumber = 0; // the device number: /dev/videoX

    camera = new PoLAR::VideoPlayer(cameraNumber, width, height, depth);
    if(!camera.get())
    {
        std::cerr << "Camera initialisation failed" << std::endl;
        exit(0);
    }
    myImage = new PoLAR::Image_uc(camera.get());

#if defined(WIN32) || defined(_WIN32)
    camera->mirrored();
#endif

    MyViewerDyn2 viewer(camera.get());
    viewer.setImage(myImage.get());

    viewer.center();
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
