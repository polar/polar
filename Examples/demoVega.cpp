/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <osgDB/ReadFile>
#include <forceModel.h>
#include <corotationalLinearFEM.h>
#include <corotationalLinearFEMForceModel.h>
#include <generateMassMatrix.h>
#include <implicitNewmarkSparse.h>

#include "MyViewerDeformation.h"
#include "SceneGraphManipulator.h"
#include "Util.h"
#include "VegaWorker.h"
#include "VegaObject3D.h"
#include "VertexSelectionHandler.h"
#include "VegaForces.h"


class MyVegaWorker: public PoLAR::VegaWorker
{
public:
    MyVegaWorker(PoLAR::VegaObject3D* object, IntegratorBase* integrator):
        VegaWorker(integrator), _object(object)
    {}

     ~MyVegaWorker() {}

protected:

    void initialize()
    {
        _object->initForces();
    }

    void run()
    {
        // get the accumulation of forces applied to the object
        double* forces = _object->applyForces();

        // apply the forces set to the integrator
        setExternalForcesToIntegrator(forces);

        // perform one timestep
        int code = doTimestep();
        if (code != 0)
        {
            cerr << "The integrator went unstable. Reduce the timestep, or increase the number of substeps per timestep.\n";
        }

        // request update of the visual object
        _object->requestMeshUpdate();
    }

    osg::ref_ptr<PoLAR::VegaObject3D> _object;
};



IntegratorBase* initPhysics(PoLAR::VegaObject3D *obj)
{

    TetMesh *tetraMesh = (TetMesh*)obj->getVolumetricMesh();
    CorotationalLinearFEM * deformableModel = new CorotationalLinearFEM(tetraMesh);

    //Apply force, generate matrices
    // create the class to connect the deformable model to the integrator
    ForceModel * forceModel = new CorotationalLinearFEMForceModel(deformableModel);
    int numTetraVertices = 3 * tetraMesh->getNumVertices(); // total number of DOFs

    SparseMatrix *massMatrix;
    // create consistent (non-lumped) mass matrix
    GenerateMassMatrix::computeMassMatrix(tetraMesh, &massMatrix, true);
    // This option only affects PARDISO and SPOOLES solvers, where it is best
    // to keep it at 0, which implies a symmetric, non-PD solve.
    // With CG, this option is ignored.
    int positiveDefiniteSolver = 0;
    int numConstrainedDOFs = 0;
    int *constrainedDOFs = NULL;
    // (tangential) Rayleigh damping
    double dampingMassCoef = 1.0; // "underwater"-like damping (here turned off)
    double dampingStiffnessCoef = 0.01; // (primarily) high-frequency damping

    // initialize the integrator
    double timeStep = 0.2; // the timestep, in seconds
    float newmarkBeta = 0.25;
    float newmarkGamma = 0.5;
    int maxIterations = 1;
    double epsilon = 1E-6;
    int numSolverThreads = 1;
    ImplicitNewmarkSparse *integrator = new ImplicitNewmarkSparse(numTetraVertices, timeStep, massMatrix, forceModel,
                                                                  //positiveDefiniteSolver,
								  numConstrainedDOFs, constrainedDOFs,
                                                                  dampingMassCoef, dampingStiffnessCoef, maxIterations, epsilon, newmarkBeta, newmarkGamma, numSolverThreads);
    return integrator;
}



int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    int width=800;
    int height=600;

    if (argc<3)
    {
        cout << "usage : ./deformationVega <veg> <obj>" << endl;
        exit(0);
    }

    const char *vegFile = argv[1];
    const char *objFile = argv[2];

    //osg::setNotifyLevel(osg::DEBUG_FP);

    // scene creation
    MyViewerDeformation viewer;
    viewer.resize(width,height);

    // Load the model given in parameter
    osg::ref_ptr<osg::Node> loadedModel;
    osgDB::Options* options = new osgDB::Options;
    options->setOptionString("noRotation");
    loadedModel = osgDB::readNodeFile(objFile, options);
    if(!loadedModel)
    {
        std::cerr << "Unable to load model" << std::endl;
        exit(0);
    }

    loadedModel->setName("DeformationOBJ");
    osg::ref_ptr<PoLAR::VegaObject3D> obj3D = new PoLAR::VegaObject3D(loadedModel.get(), vegFile);
    obj3D->optimize();
    viewer.addObject3D(obj3D.get());
    obj3D->setEditOn();
    obj3D->setPickOn();
    obj3D->rotateXSlot(-M_PI/2);
    obj3D->setMaterial(0., 0., 0., 1., 0., 0., 0., 0., 0.);

    IntegratorBase* integrator = initPhysics(obj3D.get());
    obj3D->setIntegrator(integrator);

    TestForce *myForce = new TestForce(1769);
    obj3D->addForce(myForce);

    //TestForce *myForce2 = new TestForce(1500);
    //obj3D->addForce(myForce2);

#if 0
    DraggedVertexForce *dragForce = new DraggedVertexForce;
    obj3D->addForce(dragForce);

    PoLAR::VertexSelectionHandler* vertexHandler = new PoLAR::VertexSelectionHandler(true);
    vertexHandler->setCharacterSize(10);
    viewer.addEventHandler(vertexHandler);
    QObject::connect(vertexHandler, SIGNAL(push(float,float,osg::Vec3f)), dragForce, SLOT(push(float,float,osg::Vec3f)));
    // QObject::connect(vertexHandler, SIGNAL(drag(float,float)), dragForce, SLOT(drag(float,float)));
    QObject::connect(vertexHandler, SIGNAL(force(float,float,float)), dragForce, SLOT(drag(float,float,float)));
    QObject::connect(vertexHandler, SIGNAL(release()), dragForce, SLOT(release()));
#endif

    osg::ref_ptr<MyVegaWorker> vegaSim = new MyVegaWorker(obj3D.get(), integrator);
    osg::ref_ptr<PoLAR::SimulationManager> simMan = new PoLAR::SimulationManager(vegaSim.get(), 33.0, false);

    simMan->launch();
    viewer.show();
    //viewer.setFramerate(30);
    viewer.setSimMan(simMan.get());
    viewer.startManipulateSceneSlot();

    // Create a camera manipulator with custom mouse buttons
    PoLAR::SceneGraphManipulator* manip = new PoLAR::SceneGraphManipulator;
    manip->setRotateCameraMouseButton(osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON);
    manip->setTranslateZMouseButton(osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON);
    manip->setTranslateXYMouseButton(osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON, osgGA::GUIEventAdapter::KEY_Control_L);
    viewer.setCameraManipulator(manip);

    // Create a default projection
    osg::Matrixd P = viewer.createVisionProjection(width, height);
    // Set the projection to the viewer
    viewer.setProjection(P);
    //viewer.setBackgroundColor(1.0,1.0,1.0);

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
