/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _MYVIEWER2D_
#define _MYVIEWER2D_

#include <QFileDialog>
#include <QMessageBox>
#include <QObject>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <iostream>
#include <PoLAR/DrawableObject2D.h>
#include <PoLAR/Viewer.h>
#include <PoLAR/Util.h>

class MyViewer2D : public PoLAR::Viewer
{
    Q_OBJECT
public:
    MyViewer2D(QWidget * parent = 0, const char * name = 0, WindowFlags f = 0):
        PoLAR::Viewer(parent, name, f),
        // Help messageBox creation
        mb(QMessageBox::Information,
           QString("Help: shortkeys"),
           QString("Shortkeys:\n")+
           QString("H: this help\n")+
           QString("Q: quit the application\n")+
           QString("Z: set a zoom value of 4\n")+
           QString("D: toggle background image display\n")+
           QString("I: edit image mode (pan/zoom/window level)\n")+
           QString("M: add/edit markers mode (markers)\n")+
           QString("B: add/edit markers mode (blobs)\n")+
           QString("P: add/edit markers mode (polygons)\n")+
           QString("A: add/edit markers mode (arrows)\n")+
           QString("G: grab a window snapshot\n")+
           QString("G: grab a window snapshot and save it in the file set by setSaveFName()\n")+
           QString("X: toggle 3D object display\n")+
           QString("Down: goto next image (if exists)\n")+
           QString("Up: goto previous image (if exists)\n")+
           QString("F1: save all the markers in the sequence\n")+
           QString("F2: load markers\n")+
           QString("F3: save all the markers of the first image of the sequence\n")+
           QString("F4: hide all DrawableMarkers2D of the sequence\n")+
           QString("F5: change unselectColour of the currently selected DrawableObject2D in the current image\n")+
           QString("F6: print out the currently selected DrawableObject2D in the current image\n")+
           QString("else default openscenegraph keys\n"),
           QMessageBox::NoButton,
           this)
    {
        // Add native OpenSceneGraph stats handler
        addEventHandler(new osgViewer::StatsHandler);

        mb.setWindowModality(Qt::NonModal);
    }
    void setSaveFName(QString s) { _fname = s; std::cout << "SAVEFNAME " << _fname.toLatin1().data() << std::endl;}

public slots:
    // add key bindings
    virtual void keyPressEvent(QKeyEvent *event)
    {
        switch(event->key())
        {
        case Qt::Key_H:
            mb.show(); // Show the help dialog
            break;
        case Qt::Key_Q: close(); break;
        case Qt::Key_Z: setZoom(4); break;
        case Qt::Key_D:
        {
            bgImageToggle(); // Toggle display of the background image (here we can call directly PoLAR::ViewerStatic methods as MyViewer is a PoLAR::ViewerStatic)
            break;
        }
        case Qt::Key_I:
        {
            startEditImageSlot(); // Set the image interaction activ
            break;
        }
        case Qt::Key_W:
        {
            startManipulateSceneSlot();
            break;
        }
        case Qt::Key_M:
        {
            newMarkers(); // Add markers
            PoLAR::DrawableObject2D *o=Viewer2D::getObject2DByIndex(getNumObjects2D()-1);
            o->setSelectMarkerShape(PoLAR::DrawableObject2D::SQUARE);
            break;
        }
        case Qt::Key_B:
        {
            newBlob(); // Add blobs
            break;
        }
        case Qt::Key_P:
        {
            newPolygon(); // Add blobs
            break;
        }
        case Qt::Key_A:
        {
            newArrows();
            break;
        }
        case Qt::Key_Up:
        { // change image number in the image sequence
            nextImageSlot();
            break;
        }
        case Qt::Key_Down:
        { // change image number in the image sequence
            previousImageSlot();
            break;
        }
        case Qt::Key_X:
        {
            osg::ref_ptr<PoLAR::Object3D> o=getObject3D(0);
            if (o.valid())
            {
                if (o->isDisplayed()) o->setDisplayOff();
                else o->setDisplayOn();
            }
            break;
        }
        case Qt::Key_F1:
        {
            setSaveFilename("toto"); // choose a filename for saving markers
            saveMarkersSlot(); // save all the markers from the sequence, in PoLAR::DrawableObject2D save format (c.f. doxygen)
            break;
        }
        case Qt::Key_F2:
        {
            //setLoadFilename("toto.%06d.p2d");// choose a filename for loading markers
            setLoadFilename("toto");// choose a filename for loading markers
            loadMarkersSlot();// load markers from the sequence, the filename must be in PoLAR::DrawableObject2D format (c.f. doxygen)
            startEditImageSlot(); // set image interaction active
            break;
        }
        case Qt::Key_F3:
        {
            setSaveFilename("titi.%04d.p2d"); // choose a filename for saving markers
            saveMarkersSlot(0); // save all the markers of the first image of the sequence (indexed 0), in PoLAR::DrawableObject2D save format (c.f. doxygen)
            break;
        }
        case Qt::Key_F4:
        {
            std::list<PoLAR::DrawableObject2D*> list = getObjects2DByType('M'); // get all the PoLAR::DrawableObject2D which are typed 'M' (aka DrawableMarkers2D)
            // Set all these DrawableObject2D not to be displayed
            std::list<PoLAR::DrawableObject2D*>::iterator itrL;
            for (itrL=list.begin(); itrL != list.end(); ++itrL) (*itrL)->displayOff();
            startEditImageSlot();  // set image interaction active
            break;
        }
        case Qt::Key_F5:
        {
            PoLAR::DrawableObject2D *obj2D = getCurrentObject2D(); // get the current selected DrawableObject2D
            if (obj2D != NULL) obj2D->setUnselectColor(QColor(128,128,128)); // change the unselected colour of the object2D
            startEditImageSlot(); // set image interaction active
            break;
        }
        case Qt::Key_F6:
        {
            PoLAR::DrawableObject2D *obj2D = getCurrentObject2D(); // get the current selected DrawableObject2D
            if (obj2D != NULL) std::cout<<(*obj2D); // print it out (you can redirect the obj2D in any std::ostream, per example you could save it in file like this : std::ofstream out("savefile.txt", std::ios::out); out << (*object2D);)
            break;
        }
        case Qt::Key_G:
        { // Grab an image
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");
            if (!fname.isEmpty()) grab().save(fname, "PNG");
            break;
        }
        case Qt::Key_Y:
        { // Grab an image
            if (!_fname.isEmpty())
            {
                grab().save(_fname, "PNG");
            }
            break;
        }
        default: Viewer::keyPressEvent(event);
        }
    }
private:
    QMessageBox mb;
    QString _fname;
};

#endif // _MYVIEWER2D_
