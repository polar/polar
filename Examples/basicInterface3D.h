/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef BASICINTERFACEDSA3D_H
#define BASICINTERFACEDSA3D_H

#include <QStringList>
#include <QMessageBox>
#include <QMainWindow>
#include <QToolBar>
#include <QToolButton>
#include <QSlider>
#include <QScrollBar>
#include <QInputDialog>
#include <QSpinBox>
#include <QLabel>
#include <QGLWidget>

#include <PoLAR/Viewer.h>
#include <PoLAR/SceneGraph.h>
#include <PoLAR/Object3DPickHandler.h>
#include <PoLAR/Object3D.h>
#include <PoLAR/Light.h>
#include <PoLAR/BlendDialog.h>
#include <PoLAR/BasicLightDialog.h>
#include <PoLAR/LightDialog.h>
#include <PoLAR/Image.h>
#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <osgGA/TrackballManipulator>

using Qt::WindowFlags;
using QGL::StencilBuffer;
using QGL::AlphaChannel;
using QGL::AccumBuffer;
using PoLAR::Marker2D; // to avoid problems with signals/slots from different namespaces



class Interface : public QMainWindow  
{
    Q_OBJECT

public:
    Interface();
    ~Interface();

    void addImage(PoLAR::BaseImage* image, int nima)
    {
        viewer->setBgImage(image);
        scrollbar->setMaximum(nima-1);
        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }

    void addSequence(float* sequence, int w, int h, int d, int nima)
    {
        //     viewer->resize(w,h);
        viewer->setBgImage<float>(sequence, w, h, d, nima);
        scrollbar->setMaximum(nima-1);
        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }

    void addNode(PoLAR::Object3D *object, bool displaymg=false, bool editable=false, bool blend=false)
    {
        // add the object3D to the viewer
        viewer->addObject3D(object);

        // example of how to connect a widget to a PoLAR::Object3D slot (here the display management)
        if (displaymg) QObject::connect(displayCheckBox, SIGNAL(stateChanged(int)), object, SLOT(displayOnOffSlot(int)));

        // set a PoLAR::Object3D pointer (_object3D is used in the key event handler, see keyPressSlot method)
        if (editable) _object3D = object;

        // use blend dialog on this object
        if (blend) _blendDialog->setObject(object);
    }


    void setProjection(osg::Matrixd& P, PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION) {viewer->setProjection (P, pt);}
    void setProjection() {viewer->setProjection ();} // default projection


    inline PoLAR::Viewer* getViewer() {return viewer;}


signals:
    void startEditImageSignal();
    void startEditMarkersSignal();


private slots:
    void exit() {
        this->close();
    }
    void startEditImage() {emit startEditImageSignal();}
    void startEditMarkers() {emit startEditMarkersSignal();}
    void activateLabelBox(Marker2D *m);
    void deactivateLabelBox();
    void changeTitle(int n);
    void newSpline();
    void saveMarkers();
    virtual void keyPressEvent(QKeyEvent *event);


private:
    QToolBar *toolbar, *scrollbarTB;
    QPixmap *quitPixmap, *imagePixmap, *markerPixmap, *savePixmap, *scenePixmap;
    QToolButton *quitButton, *imageButton, *markerButton, *saveButton, *editSceneButton;
    QSpinBox *labelBox;
    QScrollBar *scrollbar;

    PoLAR::Viewer *viewer;

    QLabel *statuslabel;
    QMessageBox *mb;
    PoLAR::BlendDialog *_blendDialog;
    QCheckBox *displayCheckBox;
    osg::ref_ptr<PoLAR::Object3D> _object3D;
};

#endif // BASICINTERFACE_H
