#include "SeqViewer.h"
#include <PGM.h>
void SeqViewer::keyPressSlot(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_H:
        mb.show();
        break;
    case Qt::Key_Q: ::exit(0);
    case Qt::Key_D:
    {
        bgImageToggle();
        break;
    }
    case Qt::Key_Up:
    {
        if (_camera)
        {
            blockUpdateGL();
            capturePrevSlot();
            unblockUpdateGL();
        }
        break;
    }
    case Qt::Key_Down:
    {
        if (_camera)
        {
            blockUpdateGL();
            captureNextSlot();
            unblockUpdateGL();
        }
        break;
    }
    case Qt::Key_G:
    {
        QString fname=QFileDialog::getSaveFileName( this,
                                                    "Choose a filename to save the snapshot image",
                                                    QString::null,
                                                    "PNG images (*.png)");

        if (!fname.isEmpty()) grab().save(fname, "PNG");
        break;
    }
    default: PoLAR::BaseViewer::keyPressSlot(event);
    }
}

void SeqViewer::loadGrx()
{

    if (_qgllist)
    {
        delete _qgllist;
        _qgllist = new PoLAR::ListDrawableObject2D(this, _image->getWidth(), _image->getHeight());
    }

    char fname[2048];
    sprintf(fname, fgrx, ((PoLAR::CameraOffline*)_camera)->imageIndex());
    readGrx(fname);
    this->setWindowTitle(fname);

    bgImageOn();
    //   setProperty2D(DrawableObject2D::SIZE, (void*)10);
    //   resetProperty2D(DrawableObject2D::SIZE);
    startEditImageSlot();
}




