/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>

#include <iostream>
#include <PoLAR/Image.h>
#include <PoLAR/VideoPlayer.h>
#include "MyViewerCameraQT.h"

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);

    // scene creation
    osg::ref_ptr<PoLAR::Image_uc> myImage;
    osg::ref_ptr<PoLAR::VideoPlayer> camera;
    int width = 640;
    int height = 480;

    if(argc > 1)
    {
        camera = new PoLAR::VideoPlayer(argv[1]);
        if(!camera.get())
        {
            std::cerr << "Camera initialisation failed" << std::endl;
            exit(0);
        }
        myImage = new PoLAR::Image_uc(camera.get());
    }

    else
    {
        std::cerr << "Syntax: " << argv[0] << " <video file>" <<std::endl;
        exit(0);
    }

    MyViewerDyn viewer(camera.get());
    viewer.resize(width, height);
    viewer.setBgImage(myImage.get());

    viewer.center();
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
