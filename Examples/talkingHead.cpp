/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>

#include "THViewer.h"

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    int width=1024;
    int height=1024;

    // scene creation
    THViewer viewer;

    //  viewer->requestContinuousUpdate(false);
    viewer.resize(width,height);

    // Load the model given in parameter
    int i=2;
    if (argc > 1)
    {
        osg::ref_ptr<osg::Node> loadedModel;
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;

        loadedModel = osgDB::readNodeFile(argv[1]);
        if(!loadedModel)
        {
            std::cerr << "Unable to load model" << std::endl;
            exit(0);
        }

        optimizer.optimize(loadedModel.get());
        loadedModel->accept(smooth);


        viewer.setModel(new PoLAR::Object3D(loadedModel.get()));

        osg::Matrixd P = viewer.createVisionProjection(width, height);   // Create a default projection

        PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION;

        // Set the projection to the viewer
        viewer.setProjection(P, pt);

        viewer.startManipulateSceneSlot();

        viewer.setTrackNode(loadedModel.get());
        viewer.center();
        viewer.show();
    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << "<3D model> [mode1] [mode2] ...\n";
        exit(0);
    }

    // load modes
    while (i < argc)
    {
        osg::ref_ptr<osg::Node> mode;
        mode = osgDB::readNodeFile(argv[i]);
        if (!mode)
        {
            std::cerr << "Could not read the mode from file " << argv[i] <<"\n" ;
            std::exit(0);
        }

        osg::ref_ptr<osg::Geode> geode = dynamic_cast<osg::Geode*> (mode.get());
        osg::ref_ptr<osg::Geometry> g = dynamic_cast<osg::Geometry*> (geode->getDrawable(0));
        viewer.addMode(g.get());
        i++;
    }

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
