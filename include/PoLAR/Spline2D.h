/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Spline2D.h
\brief Management of 2D splines

\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/Spline2D.cpp
*/
/** @} */

#ifndef _POLAR_SPLINE2D_H_
#define _POLAR_SPLINE2D_H_

#include "PolyLine2D.h"

namespace PoLAR
{

/** \brief Management of 2D splines

*/


class PoLAR_EXPORT Spline2D : public PolyLine2D
{
public:

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param list markers list which makes the 2D spline
  */
    Spline2D(std::list<Marker2D*> &list);

    /**
     * default constructor
     */
    Spline2D();

    /**
     * copy constructor
     */
    Spline2D(Spline2D &obj);

    /** destructor
  */
    ~Spline2D();

    /** set the list of markers
    \param list list to set
    \param destroyMarkers if true, the old markers are deallocated. If false, they are not deleted (in case they are shared with other objects for example)
  */
    virtual void setMarkers(std::list<Marker2D*> &list, bool destroyMarkers=true);

    /** add a marker
    \param marker the marker to add
  */
    void addMarker(Marker2D *marker);

    /** remove a marker
    \param marker the marker to remove
                      if marker == NULL, remove currently selected marker
  */
    void removeMarker(Marker2D *marker=(Marker2D *)NULL);

    /** move current marker to new position
    \param nx abscissa of the new position
    \param ny ordinate of the new position
  */
    void moveCurrentMarker(double nx, double ny);

    /** get the array of Bezier control points (used for drawing)
    \return a pointer to the array of Bezier control points (used for drawing)
  */
    double *getControlPoints() { return ctrlpts;}

    /** get the total length of the curve
    \return the total length of the curve
  */
    virtual double length() const;

    /** get a marker on the curve with a given curved abscissa
    \return the corresponding marker
    \param u abscissa given, it varies between 0 and length() (modulo is taken in the function)
  */
    Marker2D *getPoint(double u);

    /** get a list markers that lie on the curve at equidistant curved abscissa
    \return the corresponding list markers
    \param N number of markers in the returned list
  */
    std::list<Marker2D*> resample(int N);

    /** to update the control points
  */
    inline void updateCtrlPoints() {_updateCtrlPoints();}

    /**
     * @brief returns a copy of this object
     */
    virtual Spline2D* clone()
    {
        return new Spline2D(*this);
    }

    /** @} */


private:


    /** generate the control points of the current Bezier curve
  */
    void _makeCtrlPoints();

    /** update Bezier control points \n
      interpolation is made using cardinal splines \n
      the control points for the equivalent Bezier curve are determined and stored in ctrlpts
  */
    void _updateCtrlPoints();


    /** pointer to the array of Bezier control points (used for drawing)
  */
    double *ctrlpts;

    /** length of each arc of the curve
  */
    double *lengths;
};

}

#endif // _POLAR_SPLINE2D_H_
