/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef SHADOWGENERATOR_H
#define SHADOWGENERATOR_H

#include <osg/Group>
#include <osg/Texture2D>
#include "export.h"


namespace PoLAR
{

class Light;

/**
 * @brief When not using osgShadow, it is possible to use our custom shadow map generation
 */

class PoLAR_EXPORT ShadowGenerator: public osg::Group
{
public:
    ShadowGenerator();

    /** create a scene providing shadowing management
        \return that scene
    */
    void createShadowedScene();

    /**
    * disable shadow generation
    */
    void setShadowsOff();

    /** update shadows for this scene graph functions of a light source
       \param lightNum the number of the light source which generates shadows
   */
    bool computeShadowViewAndProjection();

    /**
    * @brief set the light used to generate shadows
    */
    void setShadowerLight(PoLAR::Light* light);

    /**
     * @brief set the texture unit of the shadow map
     */
    void setTextureUnit(unsigned int i);

    /**
     * @brief returns the texture unit of the shadow map
     */
    unsigned int getTextureUnit()
    {
        return _unit;
    }

    /**
     * @brief set ambient bias, used in shaders to apply the shadow. Should be between 0.0 (fully black shadow) and 1.0 (fully lighted shadow)
     */
    void setAmbientBias(float i)
    {
        _ambientBias = i;
        setUniforms();
    }

    float getAmbientBias()
    {
        return _ambientBias;
    }

protected:


    void setDrawCallbacks();

    void setUniforms();

    /**
    * pointer to the light that creates shadows
    */
    PoLAR::Light* _shadowerLight;

    /** pointer to the shadows camera
   */
    osg::ref_ptr<osg::Camera> _shadowsCamera;

    /** pointer to the texture corresponding to the shadows
   */
    osg::ref_ptr<osg::Texture2D> _texture;

    /** number of the texture to use for shadowing
   */
    unsigned int _unit;

    /**
     * ambient bias applied on the shadow color
     */
    float _ambientBias;

    /**
    * pointer to the group's stateSet
    */
    osg::ref_ptr<osg::StateSet> _stateSet;

    /**
    * pointer to the prorgam of shadow map generation textures
    */
    osg::ref_ptr<osg::Program> _program;

};

}

#endif // SHADOWGENERATOR_H
