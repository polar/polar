/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Image.h
\brief Base class for the management of the display of image sequences (virtual).

\author Erwan Kerrien, Pierre-Jean Petitprez
\date year 2014
*/
/** @} */

#ifndef POLARBASEIMAGE_H
#define POLARBASEIMAGE_H

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif
#include "export.h"

#include <QtCore/QObject>
#include <osg/Image>
#include <osg/Texture2D>
#include "Util.h"


namespace PoLAR
{

/**
 * @brief Base class for the management of the display of image sequences (virtual).
 */
class PoLAR_EXPORT BaseImage : public QObject, public osg::Image
{
    Q_OBJECT
public:
    /** \name Public members */
    /** @{ */

    /**
     * Default constructor
     */
    BaseImage();

    /** Constructor
    @param Width width of the image
    @param Height height of the image
    @param Depth depth of the image (grey leveled(depth=1) or coloured(depth=3))
  */
    BaseImage(int Width, int Height, int Depth);

    /**
     * Constructor
   * @param fileName the name of the file from which to load the image
   * @param flipVertical set it to true if the image must be flipped vertically
   * @param nbImages number of images in the data
     */
    BaseImage(const std::string& fileName, bool flipVertical, int nbImages);

    /** Copy constructor using osg::CopyOp to manage deep vs shallow copy.*/
    BaseImage(const BaseImage&,const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);


    /** return the name of the node's library.*/
    virtual const char* libraryName() const { return "PoLAR"; }


    /** return the name of the class.*/
    virtual const char* className() const { return "PoLAR::BaseImage"; }


    /** get the width of the original image
    \return the width of the original image
  */
    int getWidth() const {return _width;}

    /** get the height of the original image
    \return the height of the original image
  */
    int getHeight() const {return _height;}

    /** get the depth of the original image
    \return the depth of the original image
  */
    int getDepth() const {return _depth;}

    /** get this image in grey levels
   */
    virtual unsigned char *getMonoImage(unsigned char *out=(unsigned char *)NULL) const = 0;

    /** get the corresponding texture in osg::Texture2D format */
    osg::Texture2D * makeTexture() { return new osg::Texture2D(this); }

    /** given a displayed value 'level', give the actual grey value (aka invert the various LUTs)
      \sa getDisplayPixelValue()
   */
    virtual float getRealPixelValue(unsigned char level) const {return level;}

    /** get the displayed value (between 0 and 255), given an actual pixel value (aka applies all the various LUTs)
      \sa getRealPixelValue()
  */
    virtual int getDisplayedPixelValue(float value) const {return range(value); }

    /**
     * get the _colorWidth for window level shader update
     * @return the color width parameter of the LookUp table
     */
    int getColorWidth() { return _colorWidth; }

    /**
     * get the _colorHeight for window level shader update
     * @return the color height parameter of the LookUp table
     */
    int getColorHeight() { return _colorHeight; }

    /**
     * get the _wDef for window level shader update
     * @return the wDef parameter of the LookUp table
     */
    int getWDef() { return _wDef; }

    /**
     * get the _hDef for window level shader update
     * @return the hDef parameter of the LookUp table
     */
    int getHDef() { return _hDef; }

    /**
     * reset the window level of the image to the original values
     */
    void resetWindowLevel()
    {
        _colorWidth = 2 * _wDef;
        _colorHeight = 4 * _hDef;
    }

    /** get the index of the image currently displayed
        \return the index of the image currently displayed
    */
    virtual int imageIndex() const = 0;

    /** \return the number of images in the sequence
     */
    virtual int getNbImages() const = 0;


    /**
     * allocate a pixel block of specified size and type in the image (in osg::Image format)
     */
    virtual void allocateImage() { osg::Image::allocateImage(_width, _height, _depth, _GLformat, GL_UNSIGNED_BYTE); }
    using osg::Image::allocateImage;


    /**
     * allocate a pixel block of specified size and type in the image (in osg::Image format)
     * @param width the new width
     * @param height the new height
     * @param depth the new depth
     */
    virtual void allocateImage(int width, int height, int depth)
    {
        if (!isDepthSupported(depth)) depth = 1;
        setGLFormat(depth);
        osg::Image::allocateImage(width, height, depth, _GLformat, GL_UNSIGNED_BYTE);
        _width = width;
        _height = height;
        _depth = depth;
        _imageDimension = width*height*depth;
    }

    /**
     * erase the previous image (which is in osg::Image format) and create a new empty one
     */
    virtual void deleteImage() { this->deallocateData(); _imageDimension = 0; }

    /** @} */


public slots:

    /** \name Public Slots */
    /** @{ */

    /** manage the mouse press event
      */
    void mousePressSlot(QMouseEvent *e);

    void setMousePos(double x, double y);

    /** manage the mouse release event
      */
    void mouseReleaseSlot(QMouseEvent *e);

    /** manage the window level
      */
    void windowLevelSlot(QMouseEvent *e);

    void windowLevelSlot(double x, double y);

    /** manage the image sequence, go to next image in the sequence
          \param wrap if true, goto the first image after the last image has been reached. Else, does not go beyond the last image.
          \note emits indexSignal(int)
          \note pure virtual member
      */
    virtual void nextImageSlot(bool wrap=true) = 0;

    /** manage the image sequence, go to previous image in the sequence
          \param wrap if true, goto the last image after the firts image has been reached. Else, does not go before the first image.
          \note emits indexSignal(int)
          \note pure virtual member
      */
    virtual void previousImageSlot(bool wrap=true) = 0;

    /** manage the image sequence, go to a specific image in the sequence
        \note emits indexSignal(int)
        \note pure virtual member
      */
    virtual void gotoImageSlot(unsigned int=0) = 0;

    /**
     * display the next frame in case of dynamic image (e.g. from a camera)
     */
    virtual void displayNextFrame(unsigned char*) = 0;

    /**
     * display the next frame in case of dynamic image (from a camera)
     */
    virtual void displayNextFrame(unsigned char*, int , int , int=3) = 0;

    /**
     * @brief set a custom mouse button to manage the window level of this image
     */
    void setWindowLevelMouseButton(Qt::MouseButton button, Qt::KeyboardModifiers modifier=Qt::NoModifier);

    /** @} */


signals:
    /** \name Signals */
    /** @{ */

    /** this signal is emitted when repainting is necessary */
    void updateSignal();

    /** emitted when new image data are displayed */
    void newImageSignal();

    /** emitted when the image index changes, with the new current image index as param */
    void indexSignal(int);

    /**
     * emitted when the resolution of the image has changed, for example when switching between camera streams
     */
    void resolutionChanged();

    /** @} */


protected:
    /** \name Protected members */
    /** @{ */

    /** destructor
     */
    ~BaseImage();

    /**
     * @brief set the internal GL format according to the depth
     */
    void setGLFormat(int depth);

    /**
     * @brief tests is the given depth is managed by PoLAR images
     * @return true is the depth is supported, false otherwise
     */
    bool isDepthSupported(int depth)
    {
        return (depth == 1 || depth == 3 || depth == 4);
    }

    /** set a value between 0 and 255
     */
    static int range(float val) {return val < 0 ? 0 : (val >255 ? 255 : (int)val);}

    /**
     * initiliaze the MouseShortcut used to manage window level to default values \n
     * button = MiddleButton \n
     * modifier = NoModifier
     */
    void initMouseShortcuts();

    /** original width
    */
    int _width;

    /** original height
    */
    int _height;

    /** original depth
    */
    int _depth;

    /** dimension of one image
    */
    int _imageDimension;

    /** GL_LUMINANCE, or GL_RGB, or GL_RGBA
    */
    GLenum _GLformat;

    /** parameter of the LookUp Table
    */
    int _colorWidth;

    /** parameter of the LookUp Table
    */
    int _colorHeight;

    /** parameter of the LookUp Table \n
     * Default value is 256
     */
    const static int _wDef;

    /** parameter of the LookUp Table
     * Default value is 128
     */
    const static int _hDef;

    /** position of the mouse in Qt's frame along x axis
    */
    int _mouseX;

    /** position of the mouse in Qt's frame along y axis
    */
    int _mouseY;

    /**
     * mouse button used to manage the window level
     */
    MouseShortcut _windowLevelMouseButton;

private:

    /**
     * @brief delegate constructor to manage loading from a file
     */
    BaseImage(const osg::Image& image,const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);


     /** @} */
};

}

#endif // POLARBASEIMAGE_H
