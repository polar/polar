/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file DrawableText2D.h
\brief Management of the display of \ref Text2D

\author Pierre-Jean Petitprez
\date 2015
\note
Corresponding code: Object2D/DrawableText2D.cpp
*/
/** @} */


#ifndef POLAR_DRAWABLETEXT2D_H
#define POLAR_DRAWABLETEXT2D_H

#include "DrawableObject2D.h"
#include "Text2D.h"
#include <QPen>
#include <QFont>

namespace PoLAR
{

/**
 * @brief Management of the display of Text2D
 * Works exactly like all the other drawable objects, except that instead of a curve, it is a text that is drawn.
 * The text is drawn in the same colors as if it were a curve (see DrawableBlob2D).
 */
class PoLAR_EXPORT DrawableText2D : public DrawableObject2D
{
    Q_OBJECT
public:

    /** constructor
    \param list markers list
    \param Parent parent viewer
    \param text text to set
    \param unselCol colour of an unselected segment, default is blue
    \param selCol colour of a selected segment, default is yellow
    \param selmarkCol colour of a selected marker, default is yellow
    \param unselShape shape of an unselected marker
    \param selShape shape of a marker when object is selected
    \param selmarkShape shape of a selected marker
    \param Size size of a marker
    \param Precision pointing precision for selection
    \param LineWidth line width
  */
    DrawableText2D(std::list<Marker2D *> list, Viewer2D *Parent, QString text=QString(""),
                      QColor unselCol=defaultUnselCol,
                      QColor selCol=defaultSelCol,
                      QColor selmarkCol=defaultSelmarkCol,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision,
                      int LineWidth=defaultLineWidth);

    /** default constructor
    \param Parent parent viewer
    \param text text to set
    \param unselCol colour of an unselected segment, default is blue
    \param selCol colour of a selected segment, default is yellow
    \param selmarkCol colour of a selected marker, default is yellow
    \param unselShape shape of an unselected marker
    \param selShape shape of a marker when object is selected
    \param selmarkShape shape of a selected marker
    \param Size size of a marker
    \param Precision pointing precision for selection
    \param LineWidth line width
  */
    DrawableText2D(Viewer2D *Parent=NULL, QString text="",
                      QColor unselCol=defaultUnselCol,
                      QColor selCol=defaultSelCol,
                      QColor selmarkCol=defaultSelmarkCol,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision,
                      int LineWidth=defaultLineWidth);

    /** defautl constructor
    \param Parent parent viewer
    \param object the Object2D to bind to this graphics representation
    \param unselCol colour of an unselected segment, default is blue
    \param selCol colour of a selected segment, default is yellow
    \param selmarkCol colour of a selected marker, default is yellow
    \param unselShape shape of an unselected marker
    \param selShape shape of a marker when object is selected
    \param selmarkShape shape of a selected marker
    \param Size size of a marker
    \param Precision pointing precision for selection
    \param LineWidth line width
  */
    DrawableText2D(Viewer2D *Parent, Text2D *object,
                      QColor unselCol=defaultUnselCol,
                      QColor selCol=defaultSelCol,
                      QColor selmarkCol=defaultSelmarkCol,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision,
                      int LineWidth = defaultLineWidth);

    /**
     * constructor
     * @param list markers list contained by the 2D object
     * @param Parent parent viewer
     * @param text text to set
     * @param unselectPen QPen for an unselected segment
     * @param selectPen QPen for a selected segment
     * @param selectMarkerPen QPen for a selected marker
     * @param unselShape shape of an unselected marker
     * @param selShape shape of a marker when object is selected
     * @param selmarkShape shape of a selected marker
     * @param Size size of a marker
     * @param Precision pointing precision for selection \n
     * if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
     */
    DrawableText2D(std::list<Marker2D*> list, Viewer2D *Parent, QString text,
                      QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision);

    /**
     * constructor
     * @param Parent parent viewer
     * @param text text to set
     * @param unselectPen QPen for an unselected segment
     * @param selectPen QPen for a selected segment
     * @param selectMarkerPen QPen for a selected marker
     * @param unselShape shape of an unselected marker
     * @param selShape shape of a marker when object is selected
     * @param selmarkShape shape of a selected marker
     * @param Size size of a marker
     * @param Precision pointing precision for selection \n
     * if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
     */
    DrawableText2D(Viewer2D *Parent, QString text,
                      QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision);


    /**
     * constructor
     * @param Parent parent viewer
     * @param object the Object2D to bind to this graphics representation
     * @param unselectPen QPen for an unselected segment
     * @param selectPen QPen for a selected segment
     * @param selectMarkerPen QPen for a selected marker
     * @param unselShape shape of an unselected marker
     * @param selShape shape of a marker when object is selected
     * @param selmarkShape shape of a selected marker
     * @param Size size of a marker
     * @param Precision pointing precision for selection \n
     * if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
     */
    DrawableText2D(Viewer2D *Parent, Text2D *object,
                      QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision);

    /**
     * constructor by deep copy
     * @param obj the object to copy
     * @param deepCopy if true, the linked object2D will be copied. If false, the object2D pointer will point to the copied object's object2D
     */
    DrawableText2D(DrawableText2D &obj, bool deepCopy=true);


    /**
      * Destructor
      */
    ~DrawableText2D();


    /**
     * @brief returns the class name
     * @note used for serializing the class (for saving/loading)
     * @note this method must be overriden in subclasses
     */
    virtual const char* className() const
    {
        return "DrawableText2D";
    }

    /**
     * @brief Set a new text
     * @param text the string to set
     */
    void setText(QString &text);

    /**
     * @brief get the text contained in this object
     */
    QString& getText();


    /**
     * @brief set the font to draw the text (as a QFont)
     */
    void setTextFont(QFont font)
    {
        _textFont = font;
        _fontSize = font.pointSizeF();
    }


    void setResizeWithZoom(bool b=true)
    {
        _resizeWithZoom = b;
    }

    /**
   * Redefinition of the virtual function to draw the object
   */
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);


    /**
     * @brief Returns a copy of this object
     * @param deepCopy if true, the linked object2D will be copied. If false, the object2D pointer will point to the copied object's object2D
     * @return a pointer to a new DrawableObject2D with the same specs than this object
     */
    DrawableText2D *clone(bool deepCopy=true)
    {
        return new DrawableText2D(*this, deepCopy);
    }

    /**
     * @brief swap implementation of the copy-and-swap idiom
     */
    void swap(DrawableText2D &first, DrawableText2D &second);

    /**
     * @brief Redefinition of the assignment operator
     */
    DrawableText2D& operator=(DrawableText2D obj);

private:

    /**
     * custom font used to draw the text
     */
    QFont _textFont;

    /**
     * original font size
     */
    float _fontSize;

    /**
     * If true, text will be resized following the PanZoom applied to the background image. If false, the font size will remain the same independently from the zoom.
     */
    bool _resizeWithZoom;

};

}
#endif // POLAR_DRAWABLETEXT2D_H
