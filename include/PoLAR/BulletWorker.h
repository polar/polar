#ifndef BULLERWORKER_H
#define BULLERWORKER_H

#include <btBulletDynamicsCommon.h>
#include "SimulationWorker.h"

namespace PoLAR
{

/**
 * @brief Worker for Bullet scenes
 */
class BulletWorker: public PoLAR::SimulationWorker
{
public:
    BulletWorker(btDynamicsWorld *bulletWorld, bool debug=false):
        _bulletWorld(bulletWorld), _debugMode(debug)
    {}

    ~BulletWorker();

    btDynamicsWorld* getBulletWorld()
    {
        return _bulletWorld;
    }

    void setBulletWorld(btDynamicsWorld* world)
    {
        _bulletWorld = world;
    }


protected:
    void initialize() {}

    void run();

    btDynamicsWorld *_bulletWorld;
    bool _debugMode;
};

}

#endif // BULLERWORKER_H
