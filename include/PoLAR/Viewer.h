/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Viewer.h
\brief Class for the management of display of OpenSceneGraph scene graphs with previously acquired image sequences (instantiable)

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2014
\note
Corresponding code: Viewer/Viewer.cpp
*/
/** @} */


#ifndef _POLAR_IMAGEVIEWER_H_
#define _POLAR_IMAGEVIEWER_H_

#include "Viewer2D3D.h"
#include "BaseImage.h"
#include <map>
#include <list>
#include <string>

namespace PoLAR
{

/** \brief Instanciable class that creates a QWidget. It allows displaying images, 2d objects (thanks to Qt) and 3d objects (thanks to OpenSceneGraph)
*/
class PoLAR_EXPORT Viewer : public Viewer2D3D
{
    Q_OBJECT
public:

    /** \name Public Member Functions */
    /** @{ */


    enum EditionMode {
        NOMODE,
        IMAGE,
        OBJECTS2D,
        SCENE3D
    };

    /** constructor
      \param parent parent Qwidget
      \param name assigned to the widget
      \param f flags to specify various window-system properties for the widget
      \param shadows boolean to specify if scene graph to display with this viewer must include shadows management
      \param distortion boolean to specify if distortion management must be included with this viewer
  */
    Viewer(QWidget *parent=0, const char *name=0, WindowFlags f=0, bool shadows=false, bool distortion=false);


    /** destructor
    */
    virtual ~Viewer();

    /**
   * set the background image sequence
   * @param img the PoLAR::BaseImage
   * @param ResizeParent boolean to specify if the parent must be resized or not
   * @param ShareObjects boolean to specify if the images must share the same list of 2D objects
   */
    void setBgImage(BaseImage *img=(BaseImage *)NULL, bool resizeParent=true, bool shareObjects=false, bool resetObjects=true);

    /** set the background image sequence
    \param sequence pixel data
    \param w width of the image
    \param h height of the image
    \param d depth of the image (grey leveled(depth=1) or coloured(depth=3))
    \param nima number of images in the sequence
    \param ResizeParent boolean to specify if the parent must be resized or not
    \param ShareObjects boolean to specify if the images must share the same list of 2D objects
  */
    template<typename T> void setBgImage(T* sequence, int w, int h, int d, int nima, bool resizeParent=true, bool shareObjects=false, bool resetObject=true);

    /**
     * @brief returns the current edition mode, between Image edition, 2D objects edition, 3D scene edition, or no mode selected
     */
    EditionMode getEditionMode() const
    {
        return _currentMode;
    }

    /**
     * @brief returns true if the current edition mode is IMAGE (i.e. image edition mode)
     */
    bool isInImageEditionMode() const
    {
        return (_currentMode == IMAGE ? true : false);
    }

    /**
     * @brief returns true if the current edition mode is OBJECTS2D (i.e. 2D objects edition mode)
     */
    bool isIn2DObjectsEditionMode() const
    {
        return (_currentMode == OBJECTS2D ? true : false);
    }

    /**
     * @brief returns true if the current edition mode is SCENE3D (i.e. 3D scene edition mode)
     */
    bool isIn3DSceneEditionMode() const
    {
        return (_currentMode == SCENE3D ? true : false);
    }

    /**
     * @brief helper method for modifying the interaction method for manipulating the background image panning
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setPanManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief helper method for modifying the interaction method for manipulating the background image zoom
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setZoomManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief helper method for modifying the way the background image zoom is manager: by dragging the mouse or by scrolling the wheel
     * @param mode the chosen mode, between PoLAR::PanZoom::DRAG and PoLAR::PanZoom::SCROLL
     * @param precision in the case of PoLAR::PanZoom::SCROLL, it can be useful to set a bigger precision (e.g. 10.0f), depending on the wheel precision
     */
    void setZoomMode(PanZoom::ZoomMode mode, float precision = 1.0f);

    /**
     * @brief helper method for modifying the interaction method for manipulating the background image window level
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setWindowLevelManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief helper method for modifying the interaction method for selecting a 2D object in OBJECT2D mode
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setSelectObject2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief helper method for modifying the interaction method for deleting a 2D object in OBJECT2D mode
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setDeleteObject2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief helper method for modifying the interaction method for adding a control point in OBJECT2D mode
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setAddMarker2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief helper method for modifying the interaction method for selecting a control point in OBJECT2D mode
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setSelectMarker2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief helper method for modifying the interaction method for deleting a 2D object in OBJECT2D mode
     * @param m Qt::Modifier (shift, ctrl, alt or none - see Qt documentation)
     * @param b Qt::MouseButton (left, right or middle - see Qt documentation)
     */
    void setDeleteMarker2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b);

    /**
     * @brief reset all the interaction methods that have been customized to default
     */
    void resetInteractionMethodsToDefault();

    /** @} */


signals:

    /**
     * @brief emited when the edition mode has changed
     */
    void editionModeChanged();

public slots:

    /** \name Public Slots */
    /** @{ */


    /**
     * allows screen update when the mobile device is rotated
     */
    void onRotate(Qt::ScreenOrientation);

    /** start window level interaction on the Image passed in parameter
     */
    void startWindowLevelSlot(BaseImage *image);

    /** stop window level interaction on the Image passed in parameter
     */
    void stopWindowLevelSlot(BaseImage *image);

    /** start the image edition
     */
    void startEditImageSlot();

    /** stop the image edition
     */
    void stopEditImageSlot();

    /** start the markers edition
     */
    void startEditMarkersSlot();

    /** start the 3D scene manipulation
     */
    void startManipulateSceneSlot();


    /** stop all the current editions (image / 2D objects / 3D scene)
     */
    void stopEditionSlot();

    /**
     * start the edition mode set in parameter
     */
    void startEditionModeSlot(EditionMode mode);
    /** @} */



protected:

    /**
     * Management of the key press events
     * @note forwards the event to parent class
     */
    virtual void keyPressEvent(QKeyEvent *event);

    /**
     * Management of the key release events
     * @note forwards the event to the parent class
     */
    virtual void keyReleaseEvent(QKeyEvent* event);

    /**
     * @brief manages all the viewport events (inherited)
     * @param event the event to manage
     * @return true when handled, false otherwise
     */
    virtual bool viewportEvent(QEvent *event);

    virtual bool event(QEvent *event);

    /**
     * @brief manages show event
     * @note instanciates a default (empty) background image and a default projection if needed
     */
    virtual void showEvent(QShowEvent*event);

    /**
     * the current mode of edition, between image edition, 2D objects edition, or 3D scene edition
     * Default = NOMODE (no mode activated)
     */
    EditionMode _currentMode;


private:

    /**
     * structure for representing Qt events in a more generic way
     */
    struct PolarEvent
    {
        PolarEvent(Viewer::EditionMode m, QEvent::Type t, Qt::KeyboardModifier km, int k, void (Viewer::*f)(double, double)):
            mode(m), type(t), modifier(km), keyOrButton(k), fct(f)
        {
        }

        Viewer::EditionMode mode;
        QEvent::Type type;
        int modifier;
        int keyOrButton;
        void (Viewer::*fct)(double, double);
    };

    /**
     * @brief update a PolarEvent event, thanks to its string name
     * @param key name of the event to find it in the map
     * @param m the key modifier to which the event should correspond
     * @param b the key or mouse button to which the event should correspond
     */
    void updateEventManipulation(std::string key, Qt::KeyboardModifier m, int b);

    /**
     * @brief creates the map referring the managed events (2D objects manipulation, background image pan, zoom and window level)
     */
    void createEventsMap();

    /**
     * @brief deletes the events stored in the events map
     */
    void clearEventsMap();

    /**
     * @brief creates the map allowing correspondence between the events and their type (QEvent:Type)
     */
    void updateTypesMap();


    /**
     * @brief filters and manages the events received by the viewer
     * @param event the event to filter
     */
    virtual bool filterEvent(QEvent* event);


    /**
     * map of events (2D objects manipulation, background image pan, zoom and window level) registered by name
     */
    std::map<std::string, PolarEvent*> _eventsMap;

    /**
     * map of events (2D objects manipulation, background image pan, zoom and window level) registered by type (QEvent::Type)
     */
    std::map<QEvent::Type, std::list<PolarEvent*> > _typesMap;

    /**
     * some static strings used as keys for the events maps
     */
    static const std::string _selectObj2D;
    static const std::string _deleteObj2D;
    static const std::string _addMarker;
    static const std::string _deleteMarker;
    static const std::string _selectMarker;
    static const std::string _moveMarker;
    static const std::string _releaseMarker;
    static const std::string _panMove;
    static const std::string _panPress;
    static const std::string _zoomMove;
    static const std::string _zoomPress;
    static const std::string _wlMove;
    static const std::string _wlPress;

};

}

#endif // _POLAR_IMAGEVIEWER_H_
