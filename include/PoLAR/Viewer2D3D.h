/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Viewer2D3D.h
\brief Base class for the management of display of OpenSceneGraph scene graphs (virtual)

\author Frédéric Speisser, Erwan Kerrien, Pierre-Jean Petitprez
\date 2007, 2015
\note
Corresponding code: Viewer/Viewer2D3D.cpp
*/
/** @} */


#ifndef _POLAR_VIEWER2D3D_H_
#define _POLAR_VIEWER2D3D_H_

#include "Viewer2D.h"
#include "Object3D.h"
#include "Light.h"
#include "GeometryPickHandler.h"
#include "Object3DPickHandler.h"


namespace PoLAR
{

/** \brief Base class for the management and display of OpenSceneGraph scene graphs (virtual)

At instantiation, a scene graph with a base structure (SceneGraph) is constructed.
The default structure depends on whether the scenegraph has to manage shadowing or not, this is specified thanks to a boolean in the Viewer2D3D constructor.
In all cases, scene graph base structure provides the possibility to add 3D objects (PoLAR::Object3D) and light sources (PoLAR::Light) to it.
A default headlight is present in the scene graph base structure.

*/

class PoLAR_EXPORT Viewer2D3D : public Viewer2D
{
    Q_OBJECT
public:


    /** the different types of projection \n \n
      * type UNKNOWN will be treated as VISION when needed \n
      * type VISION must be used for a standard vision projection \n
      * type ANGIO must be used for an angiography projection (medical modality) \n
      * type ANGIOHINV must be used for an angiography projection with horizontal inversion (medical modality)
      */
    enum ProjectionType {UNKNOWN=-1, VISION, ANGIO, ANGIOHINV};

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param parent parent Qwidget
    \param name assigned to the widget
    \param f flags to specify various window-system properties for the widget
    \param shadows boolean to specify if scene graph to display with this viewer must include shadows management
    \param distortion boolean to specify if distortion management must be included with this viewer
    */
    Viewer2D3D(QWidget *parent, const char *name, WindowFlags f, bool shadows, bool distortion);


    /** Create a VISION typed projection matrix, the projection is calculated in order to have all the 3D objects of this viewer's related scene graph displayed, and have all these objects well centered in this viewer's window
    \param imgWidth image width in pixels
    \param imgHeight image height in pixels
    \return a projection matrix
    */
    osg::Matrixd createVisionProjection(int imgWidth, int imgHeight);

    /** Create a ANGIO typed projection matrix, the projection is calculated in order to have all the 3D objects of this viewer's related scene graph displayed, and have all these objects well centered in this viewer's window
    \param imgWidth image width in pixels
    \param imgHeight image height in pixels
    \return a projection matrix
    */
    osg::Matrixd createAngioProjection(int imgWidth, int imgHeight);


    /** set the projection associated to the viewer, to use for display, with a default typed projection \n
    The projection is calculated in order to have all the 3D objects of this viewer's related scene graph displayed, and have all these objects well centered in this viewer's window
    \note the projection is calculated when this method is called, so the projection does not take account of the objects which are added after the projection was set, in other words the projection is not updated dynamically.
    */
    void setDefaultProjection(ProjectionType pt=VISION);

    /** set the projection associated to the viewer, to use for display
    \param M projection to set
    \param pt type of the projection
    */
    void setProjection(osg::Matrixd M, ProjectionType pt=VISION);
    void setProjection() { setDefaultProjection(); }
    
    /** get the projection associated to the viewer
    \return the projection
    */
    osg::Matrixd getProjection() const;

    /** get the type of the projection associated to this viewer
    \return the type of the projection
    */
    inline ProjectionType getProjectionType() const {return _projectionType;}

    /** get the pose of the camera associated to this viewer
    \return the pose
    */
    osg::Matrixd getPose() const;

    /**
     * set a pose to the camera (extrinsics matrix)
     */
    void setPose(osg::Matrixd P);

    /**
     * set the intrinsics matrix to the camera
     */
    inline void setIntrinsics(osg::Matrixd P);

    /** intializes the manipulator according to the extrinsics so that it acts within the intrinsic coordinate frame \n
    and defines the manipulator that moves the view's master camera position in response to events
    \param m manipulator to set
    */
    virtual void setCameraManipulator(osgGA::CameraManipulator *m);

    /** set the node to be tracked, actually set the center of the rotation as the center of the node's bounding box.
      *@note node tracking only works with a PoLAR::SceneGraphManipulator
      */
    void setTrackNode(osg::Node* node);

    /** add a PoLAR::Object3D to the scene graph
        \param object3D object to add
    \return the index of the object if the object3D given in parameter was added successfully to this scenegraph else -1
   */
    int addObject3D(PoLAR::Object3D *object3D);

    /** remove a PoLAR::Object3D from the scene graph
        \param object3D object to remove
    \return the index of the object (before removing) if the object3D given in parameter was removed successfully from this scenegraph else -1
    \note the index corresponds to the position of the object in the 'addition list' (the first PoLAR::Object3D added to the scene graph has index 0, the second has index 1, etc..)
    \note when a removing of an object is done all the index of the followings object in the 'addition list' will change in that way : index = index-1 \n
  */
    int removeObject3D(PoLAR::Object3D *object3D);

    /** replace a PoLAR::Object3D in the scene graph
        \param origObject3D object to remove from this scene graph, it will be replaced by newObject3D
        \param newObject3D object to add to this scene graph, it will replace origObject3D
    \return the index of the object if the replacement was successfull else -1
    \note the index corresponds to the position of the object in the 'addition list' (the first PoLAR::Object3D added to the scene graph has index 0, the second has index 1, etc..)
  */
    int replaceObject3D(PoLAR::Object3D *origObject3D, PoLAR::Object3D *newObject3D);

    /** get a PoLAR::Object3D indicating his position in the list of all the object3D added to the scene graph
    @param index position of the object in the 'addition list' (the first PoLAR::Object3D added to the scene graph has position 0, the second has position 1 etc..)
    @return the searched object3D if the index given in parameter is correct, NULL otherwise
    @note the method does not return phantom objects
  */
    Object3D *getObject3D(int index);


    /** get a PoLAR::Object3D which has the phantom option indicating his position in the list of all the phantom objects added to the scene graph
    @param index position of the object in the 'addition list' (the first PoLAR::Object3D added to the scene graph has position 0, the second has position 1 etc..)
    @return the searched phantom object if the index given in parameter is correct, NULL otherwise
    @note the method only returns phantom objects
  */
    Object3D *getPhantomObject3D(int index);

    /** get a list of all the object3D added to the scene graph
    @return the computed list
    @note phantom objects are not included
  */
    std::list< osg::ref_ptr<Object3D> > getObjects3D();

    /** get a list of all the phantom objects added to the scene graph
    @return the computed list
    @note only phantom objects are included
  */
    std::list< osg::ref_ptr<Object3D> > getPhantomObjects3D();

    /** add a light source in the scene graph (creation)
    \param posX position of the light source along X axis
    \param posY position of the light source along Y axis
    \param posZ position of the light source along Z axis
    \param shadower boolean to specify if the light source generates shadows (if yes the scene graph to which this light source will be attached must have been set for shadowing management)
    \param viewable boolean to specify if the light source has a 3D representation in the scene graph
    \param ambientR ambient red component of the light source
    \param ambientG ambient green component of the light source
    \param ambientB ambient blue component of the light source
    \param diffuseR diffuse red component of the light source
    \param diffuseG diffuse green component of the light source
    \param diffuseB diffuse blue component of the light source
    \param specularR specular red component of the light source
    \param specularG specular green component of the light source
    \param specularB specular blue component of the light source
    \param videoFrame boolean to specify in which frame (world or video) the light source has to be positionned
    \return the created light source if success, NULL otherwise
    \note when using this method the light source is created and added to the related scene graph
    \note the number of light sources is limited to 8 (openGL limitation)
  */
    PoLAR::Light* addLightSource (float posX, float posY, float posZ, bool shadower=false, bool viewable=false,
                                  float ambientR=0.5, float ambientG=0.5, float ambientB=0.5,
                                  float diffuseR=0.6, float diffuseG=0.6, float diffuseB=0.6,
                                  float specularR=0.8, float specularG=0.8, float specularB=0.8,
                                  bool videoFrame=false  );

    /** add a light source in the scene graph (no creation)
    \param lightSource the PoLAR::Light encapsulating the light source to add
    \param viewable boolean to specify if the light source has a 3D representation in the scene graph
    \return true if no problem else false
    \note when using this method the light source given in parameter is used to light up the scene graph
    \note typical use of this method is for using the same light source in differents scene graph
    \note for instance we have the same scene displayed in two differents viewers and we want to have the same lighting parameters in both view updated dynamically, since there is one scene graph per viewer, we have to share the light source we want to light up the two scene graphs. \n
 if we simply add one light in one viewer and another light in the other viewer with the same characteristics we have the same lighting only if no changes affect the two light sources.
  */
    bool addLightSource (Light *lightSource);

    /** get a light source already attached to the scene graph encapsulated in a PoLAR::Light
    \param lightNum number of the light source to get
    \return the created PoLAR::Light
    \note when using this method the light source must already be created and attached to the scene graph, typical use is for the default headlight (with light number 0, it is first created) which is present in the base structure of a SceneGraph
  */
    Light* getLightSource(int lightNum) const;

    /** remove a light source from the scene graph
    \param lightSource to remove
    \return true is success, false otherwise
  */
    bool removeLightSource(Light *lightSource);

    /**
     * get the OpenGL light number of the given light
     * @param lightSource the given light
     * @return the light number of the given light
     */
    int getLightNumber(Light* lightSource);

    /** get the current picked object related to this viewer
    \return current picked object
  */
    Object3D *getCurrentPickedObject();

    /** set the current picked object related to this viewer
    \param object3D object to set as currently picked
  */
    inline void setCurrentPickedObject(Object3D *object3D) {_currentPickedObject = object3D;}

    /**
     * enable shadows on the 3D scene
     */
    inline void setShadowsOn() { if(_sceneGraph.valid()) _sceneGraph->setShadowsOn(); }

    /**
     * disable shadows on the 3D scene
     */
    inline void setShadowsOff() { if(_sceneGraph.valid()) _sceneGraph->setShadowsOff(); }

#ifdef USE_OSG_SHADOWS
    /**
     * @brief set a custom shadow technique to replace the default soft shadow map
     * @param technique the technique to set
     */
    inline void setShadowTechnique(osgShadow::ShadowTechnique* technique)
    {
        if(_sceneGraph.valid()) _sceneGraph->setShadowTechnique(technique);
    }
#endif

    /** intializes the manipulator according to the extrinsics so that it acts within the intrinsic coordinate frame
        \param m manipulator to set
  */
    void setupManipulator(osgGA::CameraManipulator *m);

    /** add a handler to this viewer
    \param eventHandler handler to add
  */
    void addEventHandler(osgGA::GUIEventHandler *eventHandler);

    /** get the geometry pick handler attached to this viewer
    \return the geometry pick handler if one was added to this viewer else NULL
  */
    GeometryPickHandler* getGeometryPickHandler() const;

    /** get the object3D pick handler attached to this viewer
    \return the object3D pick handler if one was added to this viewer else NULL
  */
    Object3DPickHandler* getObject3DPickHandler() const;

    /**
     * return the scene graph attached to this viewer
     * @return the scene graph attached to this viewer
     */
    PoLAR::SceneGraph* getSceneGraph() {return _sceneGraph.get();}

    /**
     * @brief get the 3D scene from the scenegraph
     * @return the node to which all 3D objects are linked
     * @note it does not include the background image
     */
    osg::Group* getScene3D()
    {
        if(_sceneGraph.valid()) return _sceneGraph->getScene();
        return NULL;
    }

    /**
     * @brief get the group of 3D objects from the scene graph
     */
    osg::Group* getObjects3DGroup()
    {
        if(_sceneGraph.valid()) return _sceneGraph->getShadowedObjectsNode();
        return NULL;
    }

    /**
     * @brief get the group of phantom objects from the scene graph
     */
    osg::Group* getPhantomObjectsGroup()
    {
        if(_sceneGraph.valid()) return _sceneGraph->getPhantomObjectsNode();
        return NULL;
    }

    /**
     * @brief get the group of lights from the scene graph
     */
    osg::Group* getLightGroup()
    {
        if(_sceneGraph.valid()) return _sceneGraph->getLightGroup();
        return NULL;
    }

    /**
     * @brief set a 3D scene to the scenegraph. Useful for example to share scenes between two viewers
     * @param scene the node to which the 3D objects are linked
     * @return true on success, false otherwise
     * @note the scene must not include the background image
     */
    bool setScene3D(osg::Node* scene);

    /**
     * @brief set a group of 3D objects to the scenegraph. Useful for example to share 3D objects between two viewers
     * @param node the group of 3D objects
     * @return true on success, false otherwise
     * @note the group must only represent the shadowed objects (no phantoms)
     */
    bool setObjects3DGroup(osg::Node* node);

    /**
     * @brief set a group of phantom objects to the scenegraph. Useful for example to share phantom objects between two viewers
     * @param node the group of phantom objects
     * @return true on success, false otherwise
     * @note the group must only represent the phantom objects (no classic objects)
     */
    bool setPhantomObjectsGroup(osg::Node* node);

    /**
     * @brief set a group of lights to the scenegraph. Useful for example to share lights between two viewers
     * @param node the group of lights
     * @return true on success, false otherwise
     */
    bool setLightGroup(osg::Node* node);

    /**
     * @brief share a 3D scene with another PoLAR::Viewer
     * @param viewer the viewer to share the scene with
     * @return true on success, false otherwise
     */
    bool shareScene3DWith(Viewer2D3D* viewer)
    {
        return viewer->setScene3D(this->getScene3D());
    }

    /**
     * @brief share the group of 3D objects with another PoLAR::Viewer
     * @param viewer the viewer to share the group with
     * @return true on success, false otherwise
     */
    bool shareObjects3DWith(Viewer2D3D* viewer)
    {
        return viewer->setObjects3DGroup(this->getObjects3DGroup());
    }

    /**
     * @brief share the group of lights with another PoLAR::Viewer
     * @param viewer the viewer to share the group with
     * @return true on success, false otherwise
     */
    bool shareLightsWith(Viewer2D3D* viewer)
    {
        return viewer->setLightGroup(this->getLightGroup());
    }

    /**
     * @brief share the group of phantom objects with another PoLAR::Viewer
     * @param viewer the viewer to share the group with
     * @return true on success, false otherwise
     */
    bool sharePhantomObjectsWith(Viewer2D3D* viewer)
    {
        return viewer->setPhantomObjectsGroup(this->getPhantomObjectsGroup());
    }

    /** @} */


    /** \name Static Public Member Functions */
    /** @{ */

    /** Create a VISION typed projection matrix
      \param imgWidth image width in pixels
      \param imgHeight image height in pixels
      \param center of a (sub) scene graph bounding sphere
      \param radius of a (sub) scene graph bounding sphere
      \return a projection matrix
      \note to get the boundingsphere (bs) of a (sub) graph use osg::Node::getBound() method on the root node (rootNode) of the (sub) graph like this : \n
    bs = rootNode->getBound();\n \n
    to get the center bs.center(); \n
    to get the radius bs.radius();  \n \n
    For instance you can use the following method like this: \n
    Projection P = Viewer2D3D::createVisionProjection(512, 512, rootNode->getBound().center(), rootNode->getBound().radius());
  */
    static osg::Matrixd createVisionProjection(int imgWidth, int imgHeight, osg::Vec3f center, float radius);

    /** Create a ANGIO typed projection matrix
      \param imgWidth image width in pixels
      \param imgHeight image height in pixels
      \param center of a (sub) scene graph bounding sphere
      \param radius of a (sub) scene graph bounding sphere
      \return a projection matrix
      \note to get the boundingsphere (bs) of a (sub) graph use osg::Node::getBound() method on the root node (rootNode) of the (sub) graph like this : \n
    bs = rootNode->getBound();\n \n
    to get the center bs.center(); \n
    to get the radius bs.radius();  \n \n
    For instance you can use the following method like this: \n
    Projection P = Viewer2D3D::createAngioProjection(512, 512, rootNode->getBound().center(), rootNode->getBound().radius());
  */
    static osg::Matrixd createAngioProjection(int imgWidth, int imgHeight, osg::Vec3f center, float radius);

    /** @} */

public slots:

    /** \name Public Slots */
    /** @{ */

    /** connect mouse events to the 3D interaction event manager
   */
    virtual void startManipulateSceneSlot();

    /** disconnect mouse events from the 3D interaction event manager
   */
    virtual void stopManipulateSceneSlot();


    /** @} */



protected:

    /** destructor
   */
    virtual ~Viewer2D3D();

    /** setup the camera manipulator (creates a default one is not already existing)
     */
    void createOrSetupCameraManipulator();

    /**
     * @brief get access to the extrinsics matrix
     */
    osg::Matrixd& getExtrinsics()
    {
        return _extrinsicMat;
    }

private:

    /** scene graph associated to this viewer
  */
    osg::ref_ptr<PoLAR::SceneGraph> _sceneGraph;


    /** view matrix of the scene graph manipulator
  */
    osg::Matrixd _extrinsicMat;


    /** type of the projection associated to this viewer
  */
    ProjectionType _projectionType;


    /** current picked object
  */
    osg::ref_ptr<PoLAR::Object3D> _currentPickedObject;
};

}

#endif // _POLAR_VIEWER2D3D_H_

