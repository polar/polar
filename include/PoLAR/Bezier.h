/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Bezier.h
\brief Management of Bezier arcs

Couple of functions to handle Bezier arcs each time, pts is an array of 3*4 floats
that allows to store 4 3D points (see Blob2D and Spline2D files: that corresponds to slices in ctrlpts member of these classes).\n
In practice, for a full curve (Blob2D or Spline2D), points will be at &ctrlpts[9*i] where i is the index of the arc on the curve.

\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/Bezier.cpp
*/
/** @} */

#ifndef _POLAR_BEZIER_H_
#define _POLAR_BEZIER_H_

#include "Marker2D.h"

namespace PoLAR
{

/** \brief Management of Bezier arcs
*/
namespace Bezier
{
PoLAR_EXPORT double _bezierArcLengthPolygon(double p[]);

PoLAR_EXPORT double _bezierArcLengthRecursive(double p[], double prec);

/** \name Operators */
/** @{ */

/** length of a Bezier arc
 */
PoLAR_EXPORT double bezierArcLength(double *pts);

/** point on a Bezier arc, given its curved abscissa u (between 0 and 1)
 *\note we use de Casteljau algorithm
 */
PoLAR_EXPORT Marker2D *bezierPoint(double *pts, double u);

/** @} */
}

}
#endif // _POLAR_BEZIER_H_
