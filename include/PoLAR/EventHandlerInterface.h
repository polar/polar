/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef EVENTHANDLERINTERFACE_H
#define EVENTHANDLERINTERFACE_H

#include "export.h"
#include "SceneGraph.h"
#include <osg/Node>
#include <osgText/Text>

namespace PoLAR
{

/**
 * @brief This class is meant to be inherited when implementing custom osgGA::GUIEventHandler subclasses. It adds everything needed to access the PoLAR scenegraph.
 * It also adds the minimal tools to create a HUD on which to write everything the programmer wants.
 */
class PoLAR_EXPORT EventHandlerInterface
{
public:
    EventHandlerInterface(bool displayInfo=true);

    ~EventHandlerInterface();

    /** initialise the handler */
    virtual void init(SceneGraph* sg)
    {
        _sceneGraph = sg;
    }

    /** set the character size of the displayed text
     */
    void setCharacterSize(float s) {_hudText->setCharacterSize(s);}

protected:

    /**
     * remove the HUD from the scenegraph
     */
    virtual void cleanUp();

    /**
     * add the HUD node to the scenegraph
     */
    void addHUD(float w, float h);

    /** update the text which will be displayed on mouse pointing */
    void setLabel(const std::string& name) {if (_hudText.get()) _hudText->setText(name);}

    /** pointer to the scene graph to which the HUD will be atttached */
    osg::ref_ptr<SceneGraph> _sceneGraph;

    /** pointer to the node containing the hud */
    osg::ref_ptr<osg::Node> _hudNode;

    /** pointer to the text containing the informations of picked nodes which is displayed in the display area */
    osg::ref_ptr<osgText::Text> _hudText;

    /** boolean to store if the info display on screen is chosen or not
    */
    bool _display;


};

}

#endif // EVENTHANDLERINTERFACE_H
