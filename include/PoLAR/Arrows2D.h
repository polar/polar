/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Arrows2D.h
\brief Management of arrows of 2D objects

\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/Arrows2D.cpp
*/
/** @} */

#ifndef _POLAR_ARROWS2D_H_
#define _POLAR_ARROWS2D_H_

#include "Object2D.h"


namespace PoLAR
{

/** \brief Management of arrows of 2D objects

*/

class PoLAR_EXPORT Arrows2D : public Object2D
{
public:

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
  */
    Arrows2D(std::list<Marker2D *> list);

    /**
     * default constructor
     */
    Arrows2D();

    Arrows2D(Arrows2D &obj);

    /**  general object selection (using points)
  */
    float select(double x, double y, double precision) {return selectMarker(x, y, precision);}

    /**
     * @brief returns a copy of this object
     */
    virtual Arrows2D* clone()
    {
        return new Arrows2D(*this);
    }

    /** @} */
};

}

#endif // _POLAR_ARROWS2D_H_


