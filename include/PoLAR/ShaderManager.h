/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file ShaderManager.h
\brief Quick implementation for adding shaders on PoLAR objects.

\author Pierre-Jean Petitprez
\date 2014

*/
/** @} */

#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif
#include "export.h"

#include <osgDB/FileUtils>
#include <osgDB/ReadFile>
#include <osgUtil/ShaderGen>
#include "BaseImage.h"


namespace PoLAR
{


/**
 * A particular namespace for the management of shaders inside PoLAR
 */
namespace ShaderManager
{

/**
 * @brief Method to load a shader from a source file
 * @param obj the osg::Shader
 * @param fileName the file name of the shader source code
 * @return true if success, false otherwise
 */
PoLAR_EXPORT bool loadShaderSource(osg::Shader* obj, const std::string& fileName);

/**
 * @brief Load the sources of the window level shaders
 * @param program the osg::Program with which to link the shaders
 */
PoLAR_EXPORT void instanciateWLShaderSources(osg::Program* program);

/**
 * @brief Link the uniforms to the window level shaders
 * @param stateSet the osg::StateSet with which to link the uniforms
 * @param image the PoLAR::Image from which to load the uniforms
 */
PoLAR_EXPORT void instanciateWLShaderUniforms(osg::StateSet* stateSet, BaseImage* image);

/**
 * @brief Attach the window level shaders to the given node
 * @param node the node on which the shaders are applied
 * @param img the image used as texture in the shaders (on which the window level is applied)
 */
PoLAR_EXPORT void addWindowLevel(osg::Node *node, BaseImage *img);


/**
 * @brief Load the sources of classic objects shaders
 * @param program the osg::Program with which to link the shaders
 */
PoLAR_EXPORT void instanciateDefaultShaders(osg::Program* program);

/**
 * @brief Load the sources of the phantom objects shaders
 * @param program the osg::Program with which to link the shaders
 */
PoLAR_EXPORT void instanciatePhantomShaders(osg::Program* program);

/**
 * @brief Attach the default objects shaders to the given node
 */
PoLAR_EXPORT osg::Program* addDefaultShaders(osg::Node *node);

/**
 * @brief Attach the phantom objects shaders to the given node
 */
PoLAR_EXPORT osg::Program* addPhantomShaders(osg::Node *node);


PoLAR_EXPORT void instanciateShadowShaders(osg::Program* program);


/**
 * @brief A callback class for updating uniform variables sent to the shader
 */
class PoLAR_EXPORT updateHeight : public osg::Uniform::Callback
{
public:

    updateHeight(PoLAR::BaseImage* img) : _image(img)
    {}

    ~updateHeight() {}

    virtual void operator()
    (osg::Uniform* uniform, osg::NodeVisitor*)
    {
        uniform->set(_image->getColorHeight());
    }

    osg::ref_ptr<PoLAR::BaseImage> _image;

};


/**
 * @brief A callback class for updating uniform variables sent to the shader
 */
class PoLAR_EXPORT updateWidth : public osg::Uniform::Callback
{
public:

    updateWidth(PoLAR::BaseImage* img) : _image(img)
    {}

    ~updateWidth() {}

    virtual void operator()
    (osg::Uniform* uniform, osg::NodeVisitor*)
    {
        uniform->set(_image->getColorWidth());
    }

    osg::ref_ptr<PoLAR::BaseImage> _image;

};

}

}
#endif // SHADERMANAGER_H
