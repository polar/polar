/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef ANIMATEDOBJECT3D_H
#define ANIMATEDOBJECT3D_H

#include "Object3D.h"
#include <osgAnimation/BasicAnimationManager>

namespace PoLAR
{

/**
 * @brief visitor for extracting animations from a loaded node
 */
struct AnimationManagerFinder: public osg::NodeVisitor
{
    AnimationManagerFinder():
        osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN)
    {}

    void apply(osg::Node& node)
    {
        if (_am.valid())
            return;
        if (node.getUpdateCallback())
        {
            osgAnimation::AnimationManagerBase* b = dynamic_cast<osgAnimation::AnimationManagerBase*>(node.getUpdateCallback());
            if (b)
            {
                _am = new osgAnimation::BasicAnimationManager(*b);
                return;
            }
        }
        traverse(node);
    }

    osg::ref_ptr<osgAnimation::BasicAnimationManager> _am;
};


/**
 * @brief PoLAR 3D object which manages animations. These animations can be created with external software like Blender (www.blender.org)
 */
class AnimatedObject3D: public Object3D
{
public:

    /**
     * @brief constructor
     * @param filename source file of the model to load
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not (see testpick example)
     */
    AnimatedObject3D(const std::string& filename, bool material = false, bool editable = false, bool pickable = false);


    /**  constructor
    \param node node to encapsulate in this object
    \param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
    \param editable boolean to specify if this object can be dynamically modified or not
    \param pickable boolean to specify if this object can be picked or not (see testpick example)
    \note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
  */
    AnimatedObject3D(osg::Node *node, bool material = false, bool editable = false, bool pickable = false);


    /**
     * @brief copy constructor using osg::CopyOp for managing deep vs shallow copy
     */
    AnimatedObject3D(const AnimatedObject3D& object, const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);


    /**
     * @brief clone method using osg::CopyOp for managing deep vs shallow copy
     */
    virtual osg::Object* clone(const osg::CopyOp &copyop) const;

    /**
     * @brief chekc if the object has animations available
     * @return true if the object is animated
     */
    bool hasAnimations();

    /**
     * @brief get the number of animations available for this object
     * @return the number of animations
     */
    unsigned int getNumAnimations();

    /**
     * @brief print the name list of animations for this object
     */
    void printAnimationList();

    /**
     * @brief play the currently selected animation
     * @return true on success
     */
    bool play();

    /**
     * @brief stop the currently selected animation
     * @return true on success
     */
    bool stop();

    /**
     * @brief checks if the animation whose name is given is playing
     * @param name the name of the animation
     * @return true if animation is playing
     */
    bool isPlaying(const std::string& name);

    /**
     * @brief checks if the currently selected animation is playing
     * @return true if animation is playing
     */
    bool isPlaying() const;

    /**
     * @brief change current animation speed
     * @param speed the new speed
     * @note speed cannot be null or negative. To stop an animation, see stop() method
     */
    void setAnimationSpeed(float speed);

    /**
     * @brief select next animation in the list of animations
     */
    void selectNextAnimation();

    /**
     * @brief select previous animation in the list of animations
     */
    void selectPreviousAnimation();

    /**
     * @brief select animation whose name is given in parameter
     * @return true if found, false otherwise
     */
    bool selectByName(const std::string& name);

    /**
     * @brief play animation whose name is given in parameter
     * @return true on success, false otherwise
     */
    bool playByName(const std::string& name);

    /**
     * @brief change the current animation play mode
     * @param mode the new play mode. Choices are ONCE, STAY, LOOP, PPONG
     */
    void setAnimationPlayMode(osgAnimation::Animation::PlayMode mode);

    /**
     * @brief get the current animation play mode
     * @return the animation play mode
     */
    osgAnimation::Animation::PlayMode getAnimationPlayMode() const;

    /**
     * @brief get the name of the currently selected animatio,
     * @return the name of the currently selected animation
     */
    const std::string& getCurrentAnimationName() const;

    /**
     * @brief get the list of animations available for this object
     * @return the list of animations names for this object
     */
    const std::vector<std::string>& getAnimationsList() const;


protected:

    /**  destructor
  */
    virtual ~AnimatedObject3D();

    /**
     * animation manager
     */
    osg::ref_ptr<osgAnimation::BasicAnimationManager> _manager;

    /**
     * map of correspondence between an animation and its name
     */
    osgAnimation::AnimationMap _map;

    /**
     * list of animations names
     */
    std::vector<std::string> _amv;

    /**
     * index to the currently selected animation
     */
    unsigned int _currentAnimation;

};

}

#endif // ANIMATEDOBJECT3D_H
