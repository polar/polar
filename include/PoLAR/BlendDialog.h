/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file BlendDialog.h
\brief Simple custom widget to manage the blending of an Object3D

\author Frédéric Speisser, Pierre-Jean Petitprez
\date year 2007
\note 
Corresponding code: Dialog/BlendDialog.cpp
*/
/** @} */


#ifndef _POLAR__BLENDDIALOG_H_
#define _POLAR__BLENDDIALOG_H_

#include "export.h"
#include "Viewer2D3D.h"
#include "Object3D.h"

#include <QFile>
#include <QWidget>
#include <QFileDialog>
#include <QLabel>
#include <QLayout>
#include <QSpinBox>
#include <QMenuBar>
#include <QMenu>
#include <QPushButton>
#include <QDialog>
#include <QButtonGroup>
#include <QCheckBox>
#include <QSlider>
#include <QDomDocument>

using Qt::WindowFlags;


namespace PoLAR
{

/** \brief Simple custom widget to manage the blending of an Object3D
*/


class PoLAR_EXPORT BlendDialog : public QDialog
{
    Q_OBJECT

public :
    BlendDialog(PoLAR::Viewer2D3D *viewer, PoLAR::Object3D *object=NULL, QWidget * parent = 0, bool modal = false, WindowFlags f = 0);
    void setObject(PoLAR::Object3D *object);
    void saveFileDialog(const QString&);
    void loadFileDialog(const QString&);


public slots :
    void loadSlot();
    void saveSlot();
    void defaultSettingsSlot();
    void valueSetBlendSlot(int value) {if (_object) _object->setTransparencyValueSlot((float)(value) / (float)(_blendValueMax));}



private:
    PoLAR::Viewer2D3D *_viewer;
    PoLAR::Object3D *_object;
    QCheckBox *blendCheck;
    QSlider *blendSlider;
    QSpinBox *blendSpinBox;

    QVBoxLayout *mainLayout, *blendLayout;
    QHBoxLayout *blendSliderLayout, *buttonsLayout;
    QLabel *blendLabel;

    QMenuBar *menu;
    QMenu *file;
    QPushButton *cancelButton, *defaultSettingsButton;

    int _blendValueMax;
    int _blendInit;

};

}

#endif //_POLAR__BLENDDIALOG_H_
