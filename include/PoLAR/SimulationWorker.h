#ifndef SIMULATIONWORKER_H
#define SIMULATIONWORKER_H

#include <QObject>
#include <QTimer>
#include <QMutex>
#include <osg/Referenced>
#include <osg/Timer>
#include <osgText/Text>
#include "export.h"

namespace PoLAR
{

/**
 * Abstract class for the management of physics simulations.
 * This class must be inherited. The inherited class should only override the initialize() and run() methods
 */
class PoLAR_EXPORT SimulationWorker: public QObject, public osg::Referenced
{
    Q_OBJECT

public:
    /**
     * Constructor
     */
    SimulationWorker();

    /**
      * Destructor
      */
    ~SimulationWorker();

    /**
     * Set a new time step to the simulation
     * @param timestep the value to set (in ms)
     */
    void setTimeStep(double timestep);


    /**
     * get the current timestep of the simulation
     * @return the current timestep (in ms)
     */
    double getTimeStep()
    {
        return _timestep;
    }


    /**
     * set the behaviour of the worker: if set to true, the simulation will be launched and run directly after the start.
     * If set to false, the user will have to call a play() on the SimulationManager which is managing this worker in order to launch the simulation.
     * @param launch the boolean to set the launch on start to true or false
     */
    void setLaunchOnStart(bool launch=true)
    {
        _mutex.lock();
        _launchOnStart = launch;
        _mutex.unlock();
    }

    /**
     * launch the simulation
     */
    void play();

    /**
     * pause the simulation
     */
    void pause();

    /**
     * requests a stop of the timer, and thus of the thread
     * @note emits the stopTimer() signal
     */
    void stop();

    /**
     * returns true is the worker is currently working, false otherwise
     */
    bool isWorking() const
    {
        return _isWorking;
    }

    /**
     * returns the current FPS of the thread
     */
    double currentFramerate() const
    {
        return _currentFramerate;
    }

    /**
     * set to true to display the FPS
     */
    void displayFramerate(bool b=true)
    {
        _calculateFramerate = b;
    }

    /**
     * returns true if FPS are displayed
     */
    bool isFramerateDisplayed() const
    {
        return _calculateFramerate;
    }


public slots:
    /**
     * slot to which the working thread connects when it starts. Used for the timer initialization
     * @note calls the initialize() overriden method
     */
    void init();


signals:
    /**
     * signal sent when the work is stopped and finished. Connected to the quit() slot of the working thread
     */
    void finished();

    /**
     * signal sent when a timer stop is requested
     */
    void stopTimer();

    /**
     * sent when the framerate is updated
     */
    void updateFramerate(double fps);

protected:
    /**
     * Pure virtual method. Needs to be overridden in the child class.
     * This method can be used to initialize the physics world before the simulation is launched
     */
    virtual void initialize() = 0;

    /**
     * Pure virtual method. Needs to be overridden in the child class.
     * This method must be used to implement the work to be done at each simulation loop.
     * @note called regularly by the timer with the timestep that has been set in the constructor or by setTimeStep()
     */
    virtual void run() = 0;

private slots:
    /**
     * stop and close the simulation
     * @note emits the finished() signal
     */
    void timerStopSlot();

private:

    virtual void timerEvent(QTimerEvent *);

    /**
     * calculate the current framerate
     */
    void calculateFramerate();

    /**
     * the timestep used by the timer
     */
    double _timestep;

    /**
     * boolean to check if the worker is working or paused
     */
    bool _isWorking;

    /**
     * a mutex for the management of shared resources
     */
    QMutex _mutex;

    /**
     * If set to true, the simulation will be launched and run directly after the start.
     * If set to false, the user will have to call a play() on the SimulationManager which is managing this worker.
     */
    bool _launchOnStart;


    /**
     * if true, framerate is displayed on screen
     */
    bool _calculateFramerate;

    /**
     * osg::Timer used for measuring framerate
     */
    osg::Timer _lastFrameStartTime;

    /**
     * current framerate of the thread
     */
    double _currentFramerate;

    /**
     * ID of the timer
     */
    int _timerId;

};

}

#endif // SIMULATIONWORKER_H
