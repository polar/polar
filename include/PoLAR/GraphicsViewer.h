/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file GraphicsViewer.h
\brief Base class for the management of drawable 2D objects on top of a QGL/OSG widget

\author Pierre-Jean Petitprez
\date 2014 - 2015
\note
Corresponding code: Viewer/GraphicsViewer.cpp
*/
/** @} */

#ifndef _POLAR_GRAPHICSVIEWER_H
#define _POLAR_GRAPHICSVIEWER_H

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif

#include "export.h"

#include <QGraphicsView>
#include <osgViewer/View>
#include "BaseViewer.h"
#include "GraphicsScene.h"
#include "PanZoom.h"
#include "BaseImage.h"
#include "Util.h"

namespace PoLAR
{

/**
 * This class is the base class for all the viewers. Each viewer must derive from GraphicsViewer.
 * It derives from QGraphicsView in order to manage 2D objects (Qt QGraphicsItem. See DrawableObject2D) through a QGraphicsScene
 * This class also creates and manages the BaseViewer widget (which makes the binding between OpenSceneGraph and Qt). This widget is added to the QGraphicsScene under the 2D items.
 * At instantiation, base structure for background image display is set and an instantiation of PanZoom class is constructed to permit users to interact with the background image.
 */
class PoLAR_EXPORT GraphicsViewer : public QGraphicsView, public osg::Referenced
{
    Q_OBJECT

public:
    /** \name Public Member Functions */
    /** @{ */

    /**
     * @brief Constructor
     * @param format enables the buffers given in arguments (forwarded to the GLWidget)
     * @param parent the parent QWidget
     * @param name the name of the widget
     * @param shareWidget widget to share openGL context with (forwarded to the GLWidget)
     * @param f flags to specify various window-system properties for the widget
     */
    GraphicsViewer(QWidget *parent=0, const char *name=0, WindowFlags f=0, bool distortion=false);


    /** add a handler to the attached OSG viewer
     * @param eventHandler handler to add
     */
    virtual void addEventHandler(osgGA::GUIEventHandler *eventHandler);

    /** Get the Viewer's list of EventHandlers.*/
    osgViewer::View::EventHandlers& getEventHandlers() { return _osgQtViewer->getEventHandlers(); }

    /** Get the const Viewer's list of EventHandlers.*/
    const  osgViewer::View::EventHandlers& getEventHandlers() const { return _osgQtViewer->getEventHandlers(); }

    /** intializes the manipulator according to the extrinsics so that it acts within the intrinsic coordinate frame \n
        and defines the manipulator that moves the view's master camera position in response to events
      * @param m manipulator to set
      */
    virtual void setCameraManipulator(osgGA::CameraManipulator *m);

    /** Get the osgViewer's CameraManipulator.*/
    osgGA::CameraManipulator* getCameraManipulator() const { return _osgQtViewer->getCameraManipulator(); }

    /**
     * resize this viewer and resize its parent window at the same time so that it fits the viewer
     */
    void resizeWithParent(int w, int h);

    /** translate mouse coordinates from the Qt frame to the image frame
    */
    void QtToImage(double xqt, double yqt, double &xima, double &yima) {_panZoom->QtToImage(xqt, yqt, xima, yima);}

    /** translate mouse coordinates from the image frame to the Qt frame
    */
    void ImageToQt(double xima, double yima, double &xqt, double &yqt) {_panZoom->ImageToQt(xima, yima, xqt, yqt);}

    /** get the zoom of the image attached to this viewer
    \return the value of the zoom of the image; if no image return 1
    */
    float getZoom() const { return _panZoom.valid() ? _panZoom->getZoom() : 1.0; }

    /**
     * get the panZoom
     * @return the panZoom
     */
    PanZoom *getPanZoom() { return _panZoom.get(); }

    /** set the zoom of the image attached to this viewer
    \param z the zoom to set to the image
    */
    void setZoom(GLdouble z) { if (_panZoom.valid()) _panZoom->setZoom(z); }

    /** get the pan(2D translation in the image frame) of the image attached to this viewer
    \param Dx pointer to the result, storage for the x component of the pan
    \param Dy pointer to the result, storage for the y component of the pan
    */
    void getPan(float *Dx, float *Dy) const {if (_panZoom.valid()) *Dx=_panZoom->getPanX(), *Dy=_panZoom->getPanY();}

    /** set the pan(2D translation in the image frame) of the image attached to this viewer
    \param Dx x component of the pan to set
    \param Dy y component of the pan to set
    */
    void setPan(float Dx, float Dy) {if (_panZoom.valid()) _panZoom->setPan(Dx,Dy);}

    /** get the Image attached to this viewer
    \return a pointer to the Image attached to this viewer
  */
    BaseImage *getBgImage() const {return _image.get();}

    /** set the background image sequence
    * @param img the BaseImage
    * @param ResizeParent boolean to specify if the parent must be resized or not
    */
    void setBgImage(BaseImage *image=(BaseImage *)NULL, bool resizeParent=true);

    /** check if background image is displayed
  */
    bool isBgImageOn() const {return _swImage->getValue(0);}

    /**
    \return the number of images in the sequence
  */
    int getNbImages() const { return _nbima; }

    /**
    \return the index of the image currently displayed
  */
    int getImageIndex() const { if(_image.valid()) return getBgImage()->imageIndex(); else return 0; }


    /** get a pointer to the camera used for background image projection
  */
    virtual osg::observer_ptr<osg::Camera> getOrthoCamera() {return _panZoom->getCamera();}

    /**
     * allows the scene to draw overlays or not
     * @param overlay boolean to switch overlay on/off
     */
    void setOverlay(bool overlay);

    /**
     * @brief change the background color
     * @param r red component (0..1)
     * @param g green component (0..1)
     * @param b blue component (0..1)
     * @param a alpha component (0..1). Default is 1.0f.
     */
    void setBackgroundColor(float r, float g, float b, float a=1.0f);

    /**
     * @brief change the background color
     * @param color the color to set as background
     */
    void setBackgroundColor(osg::Vec4 color);

    /** set the distortion to be used for displaying image background
    \param lambda1 is the first factor of radial polynomial distortion \n
    \param lambda2 is the second factor of radial polynomial distortion  \n
    \param cx is the abscissa in pixel of the center of distortion \n
    \param cy is the ordinate in pixel of the center of distortion \n
  */
    void setDistortion(GLfloat lambda1, GLfloat lambda2, GLfloat cx, GLfloat cy);

    /**
     * @brief get the name of the viewer
     */
    inline QString getName() const
    {
        return objectName();
    }

    /** Get the master camera of the view. */
    osg::Camera* getCamera() { return _osgQtViewer->getCamera(); }

    /** Get the const master camera of the view. */
    const osg::Camera* getCamera() const { return _osgQtViewer->getCamera(); }

    /**
     * @brief get the attached BaseViewer
     * @return the attached BaseViewer
     */
    BaseViewer *getGLViewer() const { return _osgQtViewer.get(); }

    /**
     * @brief get the attached BaseViewer casted as osgViewer::Viewer
     * @return the attached BaseViewer
     */
    osgViewer::Viewer *getOsgViewer() const { return _osgQtViewer.get(); }

    /**
     * @brief set the zoom mode
     * @param mode the mode to set between DRAG (drag the mouse with a mouse button clicked) and SCROLL (use the mouse wheel)
     * @param precision the precision of the zoom. Used only with the SCROLL mode.
     */
    void setZoomMode(PanZoom::ZoomMode mode, float precision=1.0f)
    {
        if(_panZoom.valid()) _panZoom->setZoomMode(mode, precision);
    }

    /**
     * @brief take screenshot of the viewer in QPixmap format
     */
    virtual QPixmap grab();

    /**
     * @brief places the widget at the center of the screen
     */
    void center();

    /**
     * @brief This method connects the changes of resolution of the background image to a resize of the viewer.
     * It is useful to automatically resize the viewer for example when using a PoLAR::VideoPlayer: the viewer will be resized when a new resolution of the frames is detected.
     * @param b if true, the viewer is set to be automatically resized when the resolution of the background image changes. If false, the feature is deactivated.
     */
    void setResizeOnResolutionChanged(bool b=true)
    {
        _resizeOnResolutionChanged = b;
    }

    /**
     * @brief calculate and apply the optimal zoom to get the background image maximized in the viewer
     */
    void setOptimalZoom();


    /**
     * @brief set the viewer to idle state or active state following @param b
     * @note Idle state means frames are drawn only on user interaction (mouse, keyboard, etc.) and never automatically. It is intended to reduce CPU usage when it is useless to redraw the frames everytime
     */
    void setIdle(bool b=true);

    /**
     * @brief set a target framerate. It is only a hint and not guaranteed to be effective
     * @note a value of 0 means the viewer will try to update as often as possible (i.e. a new frame is drawn as soon as the previous is finished)
     * @note this method has no effect in idle mode (@sa setIdle())
     */
    void setFramerate(unsigned int n);

    /** @} */


public slots:
    /** \name Public Slots */
    /** @{ */

    /** undisplay the background image
    */
    virtual void bgImageOff();

    /** display the background image
    */
    virtual void bgImageOn();

    /** toggle display of the background image
    */
    virtual void bgImageToggle();

    /** manage the image sequence, go to a specific image in the sequence
    \param n number of the image in the image sequence to go to
        \note emits imageIndexSignal(int)
    */
    virtual void gotoImageSlot(int n);

    /** manage the image sequence, go to the next image in the sequence.
      \param wrap if true, goto the first image after the last image has been reached. Else, does not go beyond the last image.
      \note emits imageIndexSignal(int)
    */
    virtual void nextImageSlot(bool wrap=true);

    /** manage the image sequence, go to the previous image in the sequence.
      \param wrap if true, goto the last image after the firts image has been reached. Else, does not go before the first image.
      \note emits imageIndexSignal(int)
    */
    virtual void previousImageSlot(bool wrap=true);

    /**
     * @brief method to be overwritten in a subclass used to draw on overlay of the viewer
     * @param painter the painter to use for the drawing
     */
    virtual void overpainting2D(QPainter*, const QRectF&);

    /**
     * @brief notify this viewer with a new event
     * @param event the event to notify
     * @return true if the event has been handled, false otherwise
     */
    virtual bool notify(QEvent *event);

    /**
     * @brief if the background image's resolution has changed, this slot allows to update the widget according to the new image resolution
     */
    void bgImgResolutionChanged();

    /**
     * @brief reimplementation of QWidget's update method to handle the update of the OSG viewer
     */
    virtual void update();

    /**
     * @brief requests a redraw of the OSG scene
     */
    void requestRedraw();

    /**
     * @brief if viewer is in idle mode, this method will force a redraw. Otherwise it is a simple requestRedraw() call
     */
    void requestRedrawOrUpdate();

    /** @} */


signals:
    /** \name Signals */
    /** @{ */

    /** signal emitted when one button of the mouse is pressed
     */
    void mousePressSignal(QMouseEvent*);

    /** signal emitted when one button of the mouse is pressed
    \param x position of the mouse along x axis in image frame
    \param y position of the mouse along y axis in image frame
    \param b state of mouse button pressed
    \param s mouse button state after the event
     */
    void mousePressSignal(double x, double y, int b, int s);

    /** signal emitted when the mouse has moved
     */
    void mouseMoveSignal(QMouseEvent*);

    /** signal emitted when the mouse has moved
    \param x position of the mouse along x axis in image frame
    \param y position of the mouse along y axis in image frame
     */
    void mouseMoveSignal(double x, double y);

    /** signal emitted when one button of the mouse is released
     */
    void mouseReleaseSignal(QMouseEvent*);

    /** signal emitted when the mouse wheel is scrolled
     */
    void mouseWheelSignal(QWheelEvent*);

    /** signal emitted when the viewer (and its graphics scene) has been repainted
     */
    void repainted();

    /** emitted with the background image index changes
     */
    void imageIndexSignal(int);

    /** @} */


protected:

    /**
      * Destructor
      */
    virtual ~GraphicsViewer();

    /**
     * Management of the key press events
     * @note forwards the event to the attached BaseViewer
     */
    virtual void keyPressEvent(QKeyEvent *event);

    /**
     * Management of the key release events
     * @note forwards the event to the attached BaseViewer
     */
    virtual void keyReleaseEvent(QKeyEvent* event);

    /**
     * @brief manages the resizing of the widget
     * @param event the QResizeEvent
     * @note forwards the instruction to the attached BaseViewer
     */
    virtual void resizeEvent(QResizeEvent* event);

    /**
     * @brief manages all the viewport events (keybord, mouse...) (inherited)
     * @param event the event to manage
     * @return true when handled, false otherwise
     * @note forwards the instruction to the attached BaseViewer
     */
    virtual bool viewportEvent(QEvent* event);

    /**
     * @brief manages the mouse wheel events.
     * @note forwards the event to the BaseViewer attached to this viewer
     * @param event the mouse wheel event
     */
    virtual void wheelEvent(QWheelEvent* event);

    /** manage the movement events of the mouse
    \note emits mouseMoveSignal(QMouseEvent*) and mouseMoveSignal(float x, float y)
     */
    virtual void mouseMoveEvent(QMouseEvent* event);

    /** manage the press events of the mouse
    \note emits mousePressSignal(QMouseEvent*) and mousePressSignal(float x, float y, float b, float s)
     */
    virtual void mousePressEvent(QMouseEvent* event);

    /** manage the release events of the mouse
    \note emits mouseReleaseSignal(QMouseEvent*)
    */
    virtual void mouseReleaseEvent(QMouseEvent* event);

    /** manage the double click events */
    virtual void mouseDoubleClickEvent(QMouseEvent *event);

    /**
      * @brief set the pan movement default position to (x,y) (needed for a correct pan without flickering)
      */
    void setPanPress(double x, double y);

    /**
     * @brief pan the backround image to coordinates (x,y)
     */
    void setPanMove(double x, double y);

    /**
      * @brief set the zoom movement default position to (x,y) (needed for a correct zoom without flickering)
      */
    void setZoomPress(double x, double y);

    /**
     * @brief zoom the backround image according to values (x,y) (creating a zoom in or out following their values)
     */
    void setZoomMove(double x, double y);

    /**
      * @brief set the window level default position to (x,y) (needed for a correct W/L without flickering)
      */
    void setWindowLevelPress(double x, double y);

    /**
     * @brief change the window level of the backround image according to values (x,y) (following their values)
     */
    void setWindowLevelMove(double x, double y);


    /**
     * the BaseViewer drawing the OSG scene in background
     */
    osg::ref_ptr<BaseViewer> _osgQtViewer;

    /**
     * the scene on which to draw the 2D objects
     */
    GraphicsScene *_scene2d;

    /** pointer to the Image attached to this viewer
     */
    osg::ref_ptr<BaseImage> _image;

    /** pointer to the PanZoom attached to this viewer
     */
    osg::ref_ptr<PanZoom> _panZoom;

    /** pointer to the switch node necessary for background image display/undisplay
     */
    osg::ref_ptr<osg::Switch> _swImage;

    /** pointer to the geode necessary to store the background image
     */
    osg::ref_ptr<osg::Geode> _bgGeode;

    /**
     * if true, when the background image is resized, the widget will be resized accordingly
     */
    bool _resizeWithParent;

    /** number of images in the sequence
     */
    int _nbima;

private:

    virtual void timerEvent(QTimerEvent *);

    /**
     * @brief performs all the initialization job when creating a new instance
     */
    void initViewer();


    /** boolean to specify if this viewer is managing distortion
  */
    bool _withDistortion;

    /** first factor of radial polynomial distortion
  */
    GLfloat _lambda1;

    /** second factor of radial polynomial distortion
  */
    GLfloat _lambda2;

    /** abscissa in pixel of the center of distortion
  */
    GLfloat _cx;

    /** ordinate in pixel of the center of distortion
  */
    GLfloat _cy;

    /** is true, the viewer is resized when the resolution of the background image changed (e.g. when frame resolution changes on webcams)
    */
    bool _resizeOnResolutionChanged;

    /**
     * ID of the timer used for frame drawing
     */
    int _timerId;

    /**
     * records the time between 2 successive frames
     */
    osg::Timer _lastFrameStartTime;

    /**
     * if true, the viewer is in idle mode, i.e. frames are drawn only on GUI events
     */
    bool _idle;

    /**
     * storage for framerate set by a user with setFramerate()
     */
    int _currentFramerateTarget;

};

}

#endif // _POLAR_GRAPHICSVIEWER_H
