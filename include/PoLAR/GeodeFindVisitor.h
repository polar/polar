/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef POLAR_GEODEFINDVISITOR_H
#define POLAR_GEODEFINDVISITOR_H

#include <osg/NodeVisitor>
#include "export.h"

namespace PoLAR
{

/**
 * Visitor used to find the geodes in a given node's subgraph
 */
class PoLAR_EXPORT GeodeFindVisitor: public osg::NodeVisitor
{
public:
    /**
     * @brief Constructor
     */
    GeodeFindVisitor();

    using osg::NodeVisitor::apply;

    /**
     * @brief apply method of the visitor
     * @param searchNode the node on which to apply the visitor
     */
    void apply(osg::Node &searchNode);

    /**
     * @brief returns the first geode found
     * @return the pointer to the first geode found
     */
    osg::Geode *getFirst() const;

    /**
     * @brief returns the list of geodes found
     * @return the list of geodes found
     */
    std::vector<osg::Geode *> &getGeodeList();


private:
    /**
     * the list of geodes found
     */
    std::vector<osg::Geode*>  _foundGeodes;
};

}


#endif // POLAR_GEODEFINDVISITOR_H
