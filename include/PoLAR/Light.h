/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _POLAR_LIGHT_H_
#define _POLAR_LIGHT_H_

#include "export.h"
#include <QObject>
#include <osg/Light>
#include <osg/LightSource>

namespace PoLAR
{

class Object3D;
class SceneGraph;

/** \brief Encapsulation of OpenSceneGraph lights

This class provides a higher level of abstraction to easily manage light sources: lighting state (on/off), position, lighting parameters (color, intensity, direction, ...), shadows generation (yes/no), 3D representation (yes/no). \n
The encapsulation also provides different slots easily connectable to custom widgets.

*/

class PoLAR_EXPORT Light: public QObject, public osg::Referenced
{
    Q_OBJECT

public:

    /**
     * @brief Default constructor
     */
    Light();

    /**
     * @brief Constructor
     * @param position position of the light
     * @param ambient ambient color of the light
     * @param diffuse diffuse color of the light
     * @param specular specular color of the light
     * @param shadowCaster if true, the light is set to cast shadows
     * @param viewable if true, a 3D representation of the light is added to the scene
     * @param videoFrame if true, the light is set to absolute reference (linked to the main camera)
     */
    Light(osg::Vec4 position, osg::Vec4 ambient, osg::Vec4 diffuse, osg::Vec4 specular, bool shadowCaster=false, bool viewable=false, bool videoFrame=false);

    /**
     * @brief Constructor
     * @param source the OSG lightsource to wrap in this object
     * @param viewable if true, a 3D representation of the light is added to the scene
     * @param videoFrame if true, the light is set to absolute reference (linked to the main camera)
     */
    Light(osg::LightSource* source, bool shadowCaster=false, bool viewable=false);

    /**
     * @brief Copy constructor using osg::CopyOp for managing deep vs shallow copy
     */
    Light(const Light& light, const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);

    /**
     * @brief clone method using osg::CopyOp for managing deep vs shallow copy
     */
    virtual Light* clone(const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY) const;

    /**
     * @brief add a scenegraph to the list of scenegraphs which contain this object
     * @param sg the scenegraph to add in the list
     */
    void addSceneGraph(SceneGraph* sg);

    /**
     * @brief remove a scenegraph from the list of scenegraphs which contain this object
     * @param sg the scenegraph to remove from the list
     */
    void removeSceneGraph(SceneGraph* sg);

    /**
     * @brief set the light on
     */
    void setOn();

    /**
     * @brief set the light off
     */
    void setOff();

    /**
     * @brief check if the light is on
     * @return true if the light is on, false otherwise
     */
    inline bool isOn() { return _lightIsOn; }

    /**
     * @brief check if the light has a 3D representation
     * @return  true if the light has a 3D representation, false otherwise
     */
    inline bool isViewable() { return _isViewable; }

    /**
     * @brief set viewable or not
     * @param b If true, the light will have a 3D viewable representation
     */
    void setViewable(bool b=true);

    /**
     * @brief add a viewable 3D representation of the light into the give scenegraph
     * @param scenegraph the scenegraph in which to add the 3D representation
     * @return true on success
     */
    bool addViewableObject(SceneGraph* scenegraph);

    /**
     * @brief remove the viewable 3D representation of the light from the given scenegraph
     * @param scenegraph the scenegraph from which to remove the 3D representation
     * @return true if success
     */
    bool removeViewableObject(SceneGraph* scenegraph);

    /**
     * @brief set the light as a shadow caster or not
     * @param b If true, the light is set as a shadow caster
     * @note due to the OSG shadow map implementation, only one light can be set as a shadow caster. The others will be ignored
     */
    void setShadowCaster(bool b=true);

    /**
     * @brief check if the light is set as a shadow caster
     * @return true if the light is a shadow caster, false otherwise
     */
    inline bool isShadowCaster() { return _shadower; }

    /**
     * @brief get the OSG LightSource encapsulated in this object
     * @return the OSG LightSource
     */
    inline osg::LightSource* getLightSource() { return _lightSource.get(); }

    /**
     * @brief get a pointer to the 3D object representing the light (if set to viewable)
     * @return the object if it exists, NULL otherwise
     */
    inline Object3D* getViewableObject() { return _viewableLight.get(); }

    /** Set which OpenGL light to operate on. */
    inline void setLightNum(int num)
    {
        _lightSource->getLight()->setLightNum(num);
        refresh();
    }

    /** Get which OpenGL light this Light operates on. */
    inline int getLightNum() const { return _lightSource->getLight()->getLightNum(); }

    /** Set the ambient component of the light. */
    inline void setAmbient(const osg::Vec4& ambient) { _lightSource->getLight()->setAmbient(ambient); refresh();}

    inline void setAmbient(float r, float g, float b, float a=1.0f) { setAmbient(osg::Vec4(r, g, b, a));}

    /** Get the ambient component of the light. */
    inline const osg::Vec4& getAmbient() const { return _lightSource->getLight()->getAmbient(); }

    /** Set the diffuse component of the light. */
    inline void setDiffuse(const osg::Vec4& diffuse) { _lightSource->getLight()->setDiffuse(diffuse); refresh();}

    inline void setDiffuse(float r, float g, float b, float a=1.0f) { setDiffuse(osg::Vec4(r, g, b, a)); }

    /** Get the diffuse component of the light. */
    inline const osg::Vec4& getDiffuse() const { return _lightSource->getLight()->getDiffuse(); }

    /** Set the specular component of the light. */
    inline void setSpecular(const osg::Vec4& specular) { _lightSource->getLight()->setSpecular(specular); refresh();}

    inline void setSpecular(float r, float g, float b, float a=1.0f) { setSpecular(osg::Vec4(r, g, b, a));}

    /** Get the specular component of the light. */
    inline const osg::Vec4& getSpecular() const { return _lightSource->getLight()->getSpecular(); }

    /** Set the position of the light.
     * @note the W coordinate set to 0 sets the light as directional
     */
    void setPosition(const osg::Vec4& position);

    void setPosition(float x, float y, float z, float w=1.0f);

    /** Get the position of the light. */
    inline const osg::Vec4& getPosition() const { return _lightSource->getLight()->getPosition(); }

    /** Set the direction of the light. */
    inline void setDirection(const osg::Vec3& direction) { _lightSource->getLight()->setDirection(direction); refresh(); }

    inline void setDirection(float x, float y, float z) { setDirection(osg::Vec3(x, y, z)); }

    /** Get the direction of the light. */
    inline const osg::Vec3& getDirection() const { return _lightSource->getLight()->getDirection(); }

    /** TODO:  Need to compute the half vector correctly */
    inline const osg::Vec4 getHalfVector() const { osg::Vec4 half(getDirection(), 1.0); return half; }

    /** Set the constant attenuation of the light. */
    inline void setConstantAttenuation(float constant_attenuation) { _lightSource->getLight()->setConstantAttenuation(constant_attenuation); refresh(); }

    /** Get the constant attenuation of the light. */
    inline float getConstantAttenuation() const { return _lightSource->getLight()->getConstantAttenuation(); }

    /** Set the linear attenuation of the light. */
    inline void setLinearAttenuation (float linear_attenuation) { _lightSource->getLight()->setLinearAttenuation(linear_attenuation); refresh(); }

    /** Get the linear attenuation of the light. */
    inline float getLinearAttenuation () const { return _lightSource->getLight()->getLinearAttenuation(); }

    /** Set the quadratic attenuation of the light. */
    inline void setQuadraticAttenuation (float quadratic_attenuation) { _lightSource->getLight()->setQuadraticAttenuation(quadratic_attenuation); refresh(); }

    /** Get the quadratic attenuation of the light. */
    inline float getQuadraticAttenuation() const { return _lightSource->getLight()->getQuadraticAttenuation(); }

    /** Set the spot exponent of the light. */
    inline void setSpotExponent(float spot_exponent) { _lightSource->getLight()->setSpotExponent(spot_exponent); refresh(); }

    /** Get the spot exponent of the light. */
    inline float getSpotExponent() const { return _lightSource->getLight()->getSpotExponent(); }

    /** Set the spot cutoff of the light. */
    inline void setSpotCutoff(float spot_cutoff) { _lightSource->getLight()->setSpotCutoff(spot_cutoff); refresh(); }

    /** Get the spot cutoff of the light. */
    inline float getSpotCutoff() const { return _lightSource->getLight()->getSpotCutoff(); }

    inline float getSpotCosCutoff() const { return cos(getSpotCutoff()); /*return getSpotCutoff()/(180.0/M_PI);*/ }

    /** set this light to be directional
     */
    void setAsDirectional();

    /** set this light to be positional
     */
    void setAsPointLight();

    /** set the reference frame of this light to video frame (camera)
  */
    void setReferenceFrameVideo();

    /** set the reference frame of this light to world frame
  */
    void setReferenceFrameWorld();

    /** get if this light is positionned in video frame
    \return true if this light is positionned in video frame else false
  */
    bool isReferenceFrameVideo();

    /** get if this light is positionned in world frame
    \return true if this light is positionned in world frame else false
  */
    bool isReferenceFrame_World();

signals:

    /** \name Signals */
    /** @{ */

    /** signal is emitted when repainting is necessary
  */
    void updateSignal();

    /** @} */

public slots:

    /** when repainting is necessary \n
    \note emits updateSignal()
  */
    void refresh();

    /** used to update the uniform variables relative to this light in GLSL shaders
     */
    void updateUniforms();

    /** manage the status (on/off) of this light
    \param state state of the widget associated
    \note if state=0 off \n
              if state=1 no change  \n
              if state=2 on  \n
              to use typically with a Qt::QButton
  */
    void onOffSlot(int state);

    /** manage the ambient red component of this light
    \param value value to set
  */
    void ambientRedSlot(float value);

    /** manage the ambient green component of this light
    \param value value to set
  */
    void ambientGreenSlot(float value);

    /** manage the ambient blue component of this light
    \param value value to set
  */
    void ambientBlueSlot(float value);

    /** manage the diffuse red component of this light
    \param value value to set
  */
    void diffuseRedSlot(float value);

    /** manage the diffuse green component of this light
    \param value value to set
  */
    void diffuseGreenSlot(float value);

    /** manage the diffuse blue component of this light
    \param value value to set
  */
    void diffuseBlueSlot(float value);

    /** manage the specular red component of this light
    \param value value to set
  */
    void specularRedSlot(float value);

    /** manage the specular green component of this light
    \param value value to set
  */
    void specularGreenSlot(float value);

    /** manage the specular blue component of this light
    \param value value to set
  */
    void specularBlueSlot(float value);

    /** manage the direction of this light along X axis
    \param value value to set
  */
    void positionXSlot(float value);

    /** manage the position of this light along Y axis
    \param value value to set
  */
    void positionYSlot(float value);

    /** manage the position of this light along Z axis
    \param value value to set
  */
    void positionZSlot(float value);

    /** manage the position of this light along X axis
    \param value value to set
  */
    void directionXSlot(float value);

    /** manage the direction of this light along Y axis
    \param value value to set
  */
    void directionYSlot(float value);

    /** manage the direction of this light along Z axis
    \param value value to set
  */
    void directionZSlot(float value);

    /** manage the constant attenuation of this light
    \param value value to set
  */
    void constantAttenuationSlot(float value);

    /** manage the linear attenuation of this light
    \param value value to set
  */
    void linearAttenuationSlot(float value);

    /** manage the quadratic attenuation of this light
    \param value value to set
  */
    void quadraticAttenuationSlot(float value);

    /** manage the spot exponent of this light
    \param value value to set
  */
    void spotExponentSlot(float value);

    /** manage the spot cut off of this light
    \param value value to set
  */
    void spotCutoffSlot(float value);


protected:

    /**
      * Destructor
      */
    ~Light();

    /**
     * @brief Generate a 3D representation of the light
     */
    void generateViewableObject();

    /**
     * @brief Update the position of the viewable 3D representation of the light following the current position of the light
     */
    void updateViewableObjectPosition();

    /**
     * @brief Update the reference frame of the viewable 3D representation of the light following the current reference frame of the light
     */
    void updateViewableObjectReference();

    /**
     * @brief Check if the light is a pointlight, a spotlight or directional
     */
    void checkLightForm();

    /**
     * @brief remove the viewable 3D representation of the light from all linked scenegraphs
     * @return true if success
     */
    bool removeViewableObject();

    /**
     * @brief add a viewable 3D representation of the light into all linked scenegraphs
     * @return true on success
     */
    bool addViewableObject();


private:

    /** pointer to the osg::LightSource encapsulated in this class
    */
    osg::ref_ptr<osg::LightSource> _lightSource;

    /** pointer to the 3D object representing the light in the scene when set to viewable
    */
    osg::ref_ptr<Object3D> _viewableLight;

    /** container to store pointers to every scenegraph to which this object is added
     */
    std::list<SceneGraph*> _sgList;

    /** boolean to check if the light must be represented using a viewable 3D model or not
    */
    bool _isViewable;

    /** boolean to check if the light is directional or not (true: directional; false: point of spot)
    */
    bool _isDirectional;

    /** boolean to check if the light is a spotlight of not
    */
    bool _isASpotLight;

    /** boolean to store the state of this light (on or off)
    */
    bool _lightIsOn;

    /** boolean to store the shadower state of this light (if this light generates shadows or not)
    */
    bool _shadower;

};

}
#endif // _POLAR_LIGHT_H_
