/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file DialogThreshold.h
\brief Custom widget to manage image thresholding

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2007, 2015
\note 
Corresponding code: Dialog/DialogThreshold.cpp
*/
/** @} */


#ifndef _POLAR_DIALOGTHRESHOLD_H_
#define _POLAR_DIALOGTHRESHOLD_H_

#include "export.h"
#include "Viewer2D3D.h"

#include <QSlider>
#include <QLabel>
#include <QDialog>
#include <list>

namespace PoLAR
{

class ThresholdFrame;

/** \brief Custom widget to manage image thresholding
*/
class PoLAR_EXPORT DialogThreshold : public QDialog
{
    Q_OBJECT
    
public:
    DialogThreshold();
    ~DialogThreshold();
    void addFrame(Viewer2D3D *viewer);
    void showEvent(QShowEvent *e);
    void hideEvent(QHideEvent *e);
    void closeEvent(QCloseEvent *e);

signals:
    void getThresholds(int min, int max);
    void showSignal();
    void hideSignal();
    void closeSignal();

private slots:
    void sendThresholds();
    void preview();
    void setMinValue(int);
    void setMaxValue(int);
    void updateMinValue(int);
    void updateMaxValue(int);
    void setAlpha(int);
    void changeBgColor();
    void updateImage();

private:
    QSlider *minSlider, *maxSlider, *alphaSlider;
    QLabel *alphaLabel, *minLabel, *maxLabel;
    std::list<ThresholdFrame*> frames;
    QRgb color;
};


class PoLAR_EXPORT ThresholdFrame : QObject
{
    Q_OBJECT
public:
    ThresholdFrame(Viewer2D3D *v, QRgb color, float alpha):
        _viewer(v),
        _switch(new osg::Switch),
        _image(new osg::Image),
        _stateSet((osg::StateSet *)NULL),
        _data((unsigned char *)NULL),
        _width(0),
        _height(0),
        _color(RGBA(color)),
        _alpha(alpha)
    {
    }

    ~ThresholdFrame()
    {
        if (_data) delete []_data;
    }

    void allocate();
    void setColor(QRgb c);
    void setAlpha(float alpha);
    void showOverlay(bool b=true);
    Viewer2D3D *getViewer() const {return _viewer;}

public slots:
    void updateImage(int tmin, int tmax);

private:
    Viewer2D3D *_viewer;
    osg::ref_ptr<osg::Switch> _switch;
    osg::ref_ptr<osg::Image> _image;
    osg::StateSet *_stateSet;
    unsigned char *_data;
    int _width, _height;
    QRgb _color;
    float _alpha;
    unsigned int RGBA(QRgb c) { return qRgb(qBlue(c), qGreen(c), qRed(c));}
};

}

#endif // _POLAR_DIALOGTHRESHOLD_H_
