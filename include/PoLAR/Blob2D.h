/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Blob2D.h
\brief Management of 2D blobs
\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/Blob2D.cpp
*/
/** @} */

#ifndef _POLAR_BLOB2D_H_
#define _POLAR_BLOB2D_H_

#include "Polygon2D.h"


namespace PoLAR
{

/** \brief Management of 2D blobs

*/


class PoLAR_EXPORT Blob2D : public Polygon2D
{
public:

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
  */
    Blob2D(std::list<Marker2D *> list);

    /**
     * default constructor
     */
    Blob2D();

    Blob2D(Blob2D &obj);

    /** destructor
  */
    ~Blob2D();

    /** set the list of markers
    \param list list to set
    \param destroyMarkers if true, the old markers are deallocated. If false, they are not deleted (in case they are shared with other objects for example)
  */
    virtual void setMarkers(std::list<Marker2D *> &list, bool destroyMarkers=true);

    /** add a marker to the markers list
    \param marker marker to add
  */
    void addMarker(Marker2D *marker);

    /** remove a marker from the markers list
    \param marker marker to remove, if marker == NULL remove currently selected marker
  */
    void removeMarker(Marker2D *marker=(Marker2D *)NULL);

    /** move current marker to new position (nx, ny)
    \param nx
    \param ny
  */
    void moveCurrentMarker(double nx, double ny);

    /** get the array of Bezier control points (used for drawing)
    \return a pointer to the array of Bezier control points (used for drawing)
  */
    double *getControlPoints() { return ctrlpts; }

    /** get the total length of the curve
    \return the total length of the curve
  */
    virtual double length() const;

    /** get a marker on the curve with curved abscissa u
    \return the corresponding marker
    \param u abcissa u vary between 0 and length() (modulo is taken in the function)
  */
    Marker2D *getPoint(double u) const;

    /** get a list of N markers that lie on the curve at equidistant curved abscissae
    \return the corresponding list
    \param N number of markers
  */
    std::list<Marker2D *> resample(int N) const;

    /** test function
  */
    void dumpCtrlPts(char *fname) const;

    /** test function
  */
    void dumpCtrlPts(FILE *f) const;

    /** to update the control points
  */
    inline void updateCtrlPoints() { _updateCtrlPoints(); }

    /**
     * @brief returns a copy of this object
     */
    virtual Blob2D* clone()
    {
        return new Blob2D(*this);
    }

    /** @} */


private:

    /** generate the control points of the current Bezier curve
  */
    void _makeCtrlPoints();

    /** update the control points of the current Bezier curve
  */
    void _updateCtrlPoints();


    /** pointer to the array of Bezier control points
  */
    double *ctrlpts;

    /** length of each arc of the curve
  */
    double *lengths;

};

}

#endif // _POLAR_BLOB2D_H_
