#ifndef _MOTIONSTATE_H_
#define _MOTIONSTATE_H_

#include <osg/MatrixTransform>
#include <btBulletCollisionCommon.h>
#include "PhysicsObject3D.h"


namespace PoLAR
{

/**
 * @brief The MotionState interface class allows the dynamics world to synchronize and interpolate the updated world transforms with graphics
 */
class PoLAR_EXPORT MotionState : public btMotionState
{
public:

    /**
     * @brief Constructor
     * @param object the PoLAR::Object3D corresponding to this physical representation
     * @param centerOfMass the center of mass of the 3D object
     */
    MotionState(PhysicsObject3D *object, const osg::Vec3d &centerOfMass = osg::Vec3(0.,0.,0.));

    /** \brief Destructor. */
    virtual ~MotionState(){}

    /**
     * @brief Method called by Bullet. Used to update the transformation matrix in the OSG world from the one in the Bullet one
     * @param worldTrans the transformation matrix in the Bullet world
     */
    virtual void setWorldTransform(const btTransform& worldTrans);

    /**
     * @brief Method called by Bullet. Used to sync the transformation matrix from OSG world to Bullet world.
     * @param worldTrans the transformation matrix (Bullet-side)
     */
    virtual void getWorldTransform(btTransform& worldTrans) const;

    /**
     * @brief initilialize all the calculations and local variables for the OSG <-> Bullet computation
     */
    void initTransform();


    /**
     * @brief set the center of mass of the object. By default, the CoM of the object is its bounding center.
     * @param centerOfMass the position of the new center of mass
     */
    void setCenterOfMass(osg::Vec3d centerOfMass)
    {
        _centerOfMass = centerOfMass;
    }

    /**
     * @brief set the scale of the object.
     * @param scale the new scale to apply
     */
    void setScale(osg::Vec3d scale)
    {
        _scale = scale;
    }


private:

    /**
     * Quick hack to get rid of the "btInfinityMask variable unused" while compiling
     */
  /*  inline int bullet_btInfinityMask()
    {
        return btInfinityMask;
    }
*/
    /**
     * the osg::Node which represents the drawn object (OSG side)
     */
    PhysicsObject3D *_object;

    /**
     * the transformation matrix of the OSG object
     */
    osg::Matrixd _parentTransform;

    /**
     * the transformation matrix of the Bullet object
     */
    btTransform _transform;

    /**
     * the center of mass of the OSG object. Used for the equivalency in translations between OSg and Bullet
     */
    osg::Vec3d _centerOfMass;

    /**
     * the scale applied to the OSG object. Used for the equivalency in translations between OSg and Bullet
     */
    osg::Vec3d _scale;

    /**
     * the previous translation that was applied to the object. Used to convert absolute translation into relative translation.
     */
    osg::Vec3d _previousTranslation;

    /**
     * the previous rotation that was applied to the object. Used to convert absolute rotation into relative rotation.
     */
    osg::Quat _previousRotation;

    /**
     * matrix to change the frame of reference from OSG world to Bullet world.
     * This matrix is calculated in the initTransform() method.
     */
    osg::Matrixd _osgToBulletMatrix;

    /**
     * matrix to change the frame of reference from Bullet world to OSG world.
     * This matrix is calculated in the initTransform() method.
     */
    osg::Matrixd _bulletToOsgMatrix;

};



}

#endif
