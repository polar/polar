/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file BaseViewer.h
\brief Base class for the display of image sequences, 2D objects and 3D objects

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2014, 2015
\note 
Corresponding code: Viewer/BaseViewer.cpp
*/
/** @} */

#ifndef _POLAR_BASEVIEWER_H_
#define _POLAR_BASEVIEWER_H_

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif

#include "export.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>

#include <osg/ref_ptr>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <QTouchEvent>

#ifdef OSG_USE_FBO
#include "GraphicsWindowEx.h"
#else
#include <osgViewer/GraphicsWindow>
#endif

using Qt::WindowFlags;

namespace PoLAR
{

class GraphicsViewer;

/**
 * QWidget managing the binding between OpenSceneGraph and Qt.
 * This widget is entirely managed by the GraphicsViewer class.
 */
class PoLAR_EXPORT BaseViewer : public QOpenGLWidget, public osgViewer::Viewer, protected QOpenGLFunctions
{
    Q_OBJECT

    friend class GraphicsViewer;

public:
    /** \name Public Member Functions */
    /** @{ */

    /** constructor
      \param format enables the buffers given in arguments
      \param parent parent Qwidget
      \param name assigned to the widget
      \param shareWidget widget to share openGL context with
      \param f flags to specify various window-system properties for the widget
  */
    BaseViewer(GraphicsViewer *parent, WindowFlags f = 0);

    /**
     * destructor
     */
    virtual ~BaseViewer();

    /**
     * @brief reimplementation of QOpenGLWidget's initializeGL
     */
    virtual void initializeGL();

    /**
     * @brief reimplementation of QOpenGLWidget's paintGL
     */
    virtual void paintGL();

    /**
     * @brief reimplementation of QOpenGLWidget's resizeGL
     */
    virtual void resizeGL(int width, int height);

    /**
     * @brief reimplementation of osgViewer::Viewer frame() method
     */
    virtual void frame(double simulationTime=USE_REFERENCE_TIME);

    /** get a pointer to the OpenSceneGraph graphic window
  */
    osgViewer::GraphicsWindow* getGraphicsWindow() { return _gw.get(); }

    /** get a constant pointer to the OpenSceneGraph graphic window
  */
    const osgViewer::GraphicsWindow* getGraphicsWindow() const { return _gw.get(); }

    /**
     * Return the GraphicsView managing this viewer
     */
    GraphicsViewer *getGraphicsViewer()
    {
        return _parentViewer;
    }

    /**
     * @brief helper method to connect the key/mouse events to the key/mouse slots (in order to propagate the events to OSG)
     */
    void connectKeyEventSlots();

    /**
     * @brief helper method to disconnect the key/mouse events from the key/mouse slots (in order to stop propagating the events to OSG)
     */
    void disconnectKeyEventSlots();

    /**
     * redefinition of QOpenGLWidget's paintEvent
     */
    virtual void paintEvent(QPaintEvent *e);

#ifdef OSG_USE_FBO
    /** add a handler to this viewer
     * @param eventHandler handler to add
     * @note overwrites the method osgViewer::Viewer::addEventHandler(...)
     */
    void addEventHandler(osgGA::GUIEventHandler *eventHandler);
#endif


    /** @} */


signals:

    /** \name Signals */
    /** @{ */

    /** signal emitted when one button of the mouse is pressed
    */
    void mousePressSignal(QMouseEvent*);

    /** signal emitted when one button of the mouse is pressed
    \param x position of the mouse along x axis in image frame
    \param y position of the mouse along y axis in image frame
    \param b state of mouse button pressed
    \param s mouse button state after the event
    */
    void mousePressSignal(float x, float y, int b, int s);

    /** signal emitted when the mouse has moved
    */
    void mouseMoveSignal(QMouseEvent*);

    /** signal emitted when the mouse has moved
    \param x position of the mouse along x axis in image frame
    \param y position of the mouse along y axis in image frame
    */
    void mouseMoveSignal(float x, float y);

    /** signal emitted when one button of the mouse is released
    */
    void mouseReleaseSignal(QMouseEvent*);


    void touchEventSignal(QEvent*);

    /** signal emitted when one key is pressed
    */
    void keyPressSignal(QKeyEvent*);

    /** signal emitted when one key is released
    */
    void keyReleaseSignal(QKeyEvent*);

    void repaintRequested();

    /** @} */


public slots:
    /** \name Public Slots */
    /** @{ */

    /** default slot for key press events, transmitted to OpenSceneGraph \n
    CAUTION : event transmission is not valid when meta keys are being pressed (see Qt's QString QKeyEvent::text() documentation for more informations about that). Only Shift, Control and Alt are managed thanks to specifics treatments (see source)
    */
    virtual void keyPressSlot(QKeyEvent *e);

    /** default slot for key release events, transmitted to OpenSceneGraph\n
    CAUTION : event transmission is not valid when meta keys are being released (see Qt's QString QKeyEvent::text() documentation for more informations about that). Only Shift, Control and Alt are managed thanks to specifics treatments (see source)
    */
    virtual void keyReleaseSlot(QKeyEvent *e);

    /** default slot for mouse press events, transmitted to OpenSceneGraph
    */
    virtual void mousePressSlot(QMouseEvent *e);

    /** default slot for mouse release events, transmitted to OpenSceneGraph
    */
    virtual void mouseReleaseSlot(QMouseEvent *e);

    /** default slot for mouse move events, transmitted to OpenSceneGraph
    */
    virtual void mouseMoveSlot(QMouseEvent *e);

    /** default slot for touch events, transmitted to OpenSceneGraph
    */
    virtual void touchEventSlot(QEvent *event);

    /**
     * slot to the osgViewer version of requestRedraw, asking for a redraw of the OSG scene
     */
    virtual void requestRedraw();


    /** @} */


private:

    /** manage the press events of the keyboard
    \note emits keyPressSignal(QKeyEvent*)
    */
    virtual void keyPressEvent(QKeyEvent* event);

    /** manage the release events of the keyboard
    \note emits keyReleaseSignal(QKeyEvent*)
    */
    virtual void keyReleaseEvent(QKeyEvent* event);

    /** manage the movement events of the mouse
    \note emits mouseMoveSignal(QMouseEvent*) and mouseMoveSignal(float x, float y)
   */
    virtual void mouseMoveEvent(QMouseEvent* event);

    /** manage the press events of the mouse
    \note emits mousePressSignal(QMouseEvent*) and mousePressSignal(float x, float y, float b, float s)
     */
    virtual void mousePressEvent(QMouseEvent* event);

    /** manage the release events of the mouse
    \note emits mouseReleaseSignal(QMouseEvent*)
    */
    virtual void mouseReleaseEvent(QMouseEvent* event);

    /** manage the mouse double click events */
    virtual void mouseDoubleClickEvent(QMouseEvent *event);

    /**
     * manage the events of the mouse wheel
     */
    virtual void wheelEvent(QWheelEvent* event);

    /**
     * manage all other events
     */
    virtual bool event(QEvent* event);

    /**
     * find and return which button has been pressed from the Qt event passed in parameter:
     * 1 = left mouse button
     * 2 = middle mouse button
     * 3 = right mouse button
     */
    unsigned int button(QMouseEvent *event);

    /**
     * find and return which meta key has been pressed / released from the Qt event passed in paramter:
     * KEY_Shift_L / KEY_Control_L /KEY_Alt_L (in OSG notation)
     */
    unsigned int modifierKey(QKeyEvent *event);

    /**
     * get the event queue from the attached graphics window
     */
    osgGA::EventQueue* getEventQueue() const;

    /** pointer to the OpenSceneGraph graphic window
    */
#ifdef OSG_USE_FBO
    osg::ref_ptr<GraphicsWindowEx> _gw;
#else
    osg::ref_ptr<osgViewer::GraphicsWindow> _gw;
#endif

    /**
     * pointer to the GraphicsViewer which manages this widget.
     * useful in OSG event handlers to access to methods implemented in GraphicsViewer and inherited classes
     */
    GraphicsViewer * _parentViewer;

};


}

#endif // _POLAR_BASEVIEWER_H_
