/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file FrameAxis.h
\brief Class for a representation of frame axis

\author Frédéric Speisser
\date year 2007
\note
Corresponding code: Object3D/FrameAxis.cpp
*/
/** @} */

#ifndef _POLAR_FRAMEAXIS_H_
#define _POLAR_FRAMEAXIS_H_

#include "export.h"
#include <osg/Group>
#include <osg/Material>

namespace PoLAR
{

/** @brief Class for a representation of frame axis

The aim of this class is to provide a basic representation of the three frame axis (x,y,z), but also to show how to create classes for custom 3D objects which can be afterwards added to scene graphs
    @sa ViewableCamera
    @note The frame axis colors are: red for X axis, green for Y axis, blue for Z axis.

*/
class PoLAR_EXPORT FrameAxis : public osg::Group
{

public :

    /** @name Public Member Functions */
    /** @{ */

    /**  constructor */
    FrameAxis();

    /**
     * convenient method to activate shading on the frame axes
     */
    void setLightingOn();

    /**
     * convenient method to deactivate shading on the frame axes
     */
    void setLightingOff();

    /** @} */


protected :

    /**  destructor */
    virtual ~FrameAxis();

    /** create a 3D representation of axis set by x, y or z\n
     * Called with x, y, z set to 1.0, 0.0, 0.0, then 0.0, 1.0, 0.0, then 0.0, 0.0, 1.0
    */
    osg::Group* createFrameAxis(float x, float y, float z);

    /**
     * creates a proper material for the group given in parameter, following x, y, or z\n
     * Called with x, y, z set to 1.0, 0.0, 0.0, then 0.0, 1.0, 0.0, then 0.0, 0.0, 1.0
     */
    void createMaterial(osg::Group* group, float x, float y, float z);

    /**
     * creates uniforms to replace fixed-function material when not using fixed-function pipeline
     */
    void createMaterialUniforms(osg::Group *group, osg::Material *material);

    /**
     * stores the lighting state (on or off)
     */
    bool _lighting;

};

}

#endif // _POLAR_FRAMEAXIS_H_
