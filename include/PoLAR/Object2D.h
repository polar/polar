/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Object2D.h
\brief Management of 2D objects

\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/Object2D.cpp
*/
/** @} */

#ifndef _POLAR_OBJECT2D_H_
#define _POLAR_OBJECT2D_H_

#include "export.h"
#include "Marker2D.h"
#include <osg/Referenced>
#include <list>


namespace PoLAR
{

/** \brief Management of 2D objects

*/


class PoLAR_EXPORT Object2D: public osg::Referenced
{
public:

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param list list of markers contained by the 2D object
  */
    Object2D(std::list<Marker2D*> list);

    /**
     * default constructor
     */
    Object2D();

    /**
     * copy contructor
     */
    Object2D(Object2D &obj);


    /** destructor
     * The markers in the list of markers are deleted
     */
    virtual ~Object2D();

    /** set the list of markers
    \param list list to set
    \param destroyMarkers if true, the old markers are deallocated. If false, they are not deleted (in case they are shared with other objects for example)
  */
    virtual void setMarkers(std::list<Marker2D *> &list, bool destroyMarkers=true);

    /** save the private list of markers in a file
    \param fname name of the file in which the markers will be saved
  */
    void saveMarkers(char *fname);

    /** select a marker
    \param x abscissa of the point pointed
    \param y ordinate of the point pointed
    \param precision precision of the selection
    \return the square distance between point (x,y) and closest marker
    if precision < 0 -> select closest marker at any distance
  */
    double selectMarker(double x, double y, double precision);

    /** unselect the markers (deactivate all of them)
  */
    void unselectMarker();

    /** general object selection, rewritten in subclasses
  */
    virtual float select(double x, double y, double precision)=0;

    /** get the number of markers in the private list
    \return the number of markers in the private list
  */
    unsigned int nbMarkers() const { return _listMarkers.size(); }

    /** get currently selected marker iterator
    \return currently selected marker iterator
  */
    std::list<Marker2D*>::iterator& getCurrentMarkerIterator()
    {
        return _currentMarker;
    }

    /**
     * @brief returns a pointer to the marker at position i
     */
    Marker2D* getMarker(unsigned int i);

    /** move current marker to new position
       \param nx abscissa of the new position
       \param ny ordinate of the new position
     */
    virtual void moveCurrentMarker(double nx, double ny)
    {
        if (_currentMarker != _listMarkers.end())
        {
            (*_currentMarker)->setX(nx);
            (*_currentMarker)->setY(ny);
        }
    }

    /** add a marker to the list of markers
  */
    virtual void addMarker(Marker2D *marker);

    /** remove a marker of the list of markers
      if marker == NULL, remove currently selected marker
  */
    virtual void removeMarker(Marker2D *marker=(Marker2D *)NULL);


    /** get the private list of markers
    \return the private list of markers
  */
    std::list<Marker2D*>& getMarkers() { return _listMarkers; }
    const std::list<Marker2D*>& getMarkers() const { return _listMarkers; }

    /** get the current marker. To get it as std::list<Marker2D*>::iterator, see getCurrentMarkerIterator()
    \return the current marker
  */
    Marker2D *getCurrentMarker() { if(_currentMarker != _listMarkers.end()) return *_currentMarker; else return NULL; }

    /** compute the bounding box as a 2D rectangle (coordinates in double)
    \param xll abscissa of the lower left corner
    \param yll ordinate of the lower left corner
    \param xur abscissa of the upper right corner
    \param yur abscissa of the upper right corner
  */
    virtual void boundingBox(double *xll, double *yll, double *xur, double *yur);

    /** compute the bounding box as a 2D rectangle (results in two points)
    \param ll the lower left corner
    \param ur the upper right corner
  */
    virtual void boundingBox(QPointF *ll, QPointF *ur)
    {
        double xlld, ylld, xurd, yurd;
        boundingBox(&xlld, &ylld, &xurd, &yurd);
        ll->setX(xlld), ll->setY(ylld);
        ur->setX(xurd), ur->setY(yurd);
     }

    /**
     * @brief returns a copy of this object
     */
    virtual Object2D* clone()=0;

    /** @} */


protected:

    /** set the private list of markers to NULL
  */
    void resetPointers();

    /**
     * little helper to be sure that the number of markers represented by _nbMarkers is coherent with the actual number of markers
     */
    void updateNbMarkers()
    {
        _nbMarkers = _listMarkers.size();
    }

    /**
     * Destroy all the current markers (they are deallocated)
     */
    void destroyCurrentList();

    /** the list of markers
      \note this is a LIFO list
   */
    std::list<Marker2D*> _listMarkers;

    /** iterator to the current marker
    */
    std::list<Marker2D*>::iterator _currentMarker;

    /**
     * index of the iterator to the current marker
     */
    int _currentMarkerIndex;

    /** the number of markers in the list of markers
  */
    int _nbMarkers;


};

}

#endif // _POLAR_OBJECT2D_H_
