/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file PolyLine2D.h
\brief Management of 2D polylines

\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/PolyLine2D.cpp
*/
/** @} */

#ifndef _POLAR_POLYLINE2D_H_
#define _POLAR_POLYLINE2D_H_

#include "Object2D.h"

namespace PoLAR
{

/** \brief Management of 2D polylines

*/


class PoLAR_EXPORT PolyLine2D : public Object2D
{
public:
    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param list markers list which makes the 2D polyline
  */
    PolyLine2D(std::list<Marker2D *> &list);

    /**
     * default constructor
     */
    PolyLine2D();

    /**
     * copy constructor
     */
    PolyLine2D(PolyLine2D &obj);

    /** set _currentList so that it points to one marker in the segment and
      _currentList->next (or _listMarkers) points to the other one
    \return -1 if no point is present in the polygon
  */
    float selectSegment(double x, double y, double precision);

    /** set _currentList to NULL (no selection)
  */
    void unselectSegment();

    /** set _currentList to NULL (no selection)
  */
    float select(double x, double y, double precision) {return selectSegment(x,y,precision);}

    /** get the total length of the curve
    \return the total length of the curve
  */
    virtual double length() const;

    /**
     * @brief returns a copy of this object
     */
    virtual PolyLine2D* clone()
    {
        return new PolyLine2D(*this);
    }

    /** @} */


private:

    /** get the segment the closest to a point
    \return the segment the closest to a point
    \param x abscissa of the point pointed
    \param y ordinate of the point pointed
  */
    Marker2D *getClosestSegment(double x, double y);

    /** get the segment the closest to a marker
    \return the segment the closest to a marker
    \param marker the marker pointed
  */
    Marker2D *getClosestSegment(Marker2D *marker);
};

}

#endif // _POLAR_POLYLINE2D_H_
