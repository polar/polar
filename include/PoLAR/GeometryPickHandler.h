/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file GeometryPickHandler.h
\brief Pick handler of a geometry

\author Frédéric Speisser, Pierre-Jean Petitprez
\date year 2007, 2015
\note 
Corresponding code: SceneGraph/GeometryPickHandler.cpp
*/
/** @} */

#ifndef _POLAR_GEOMETRYPICKHANDLER_H_
#define _POLAR_GEOMETRYPICKHANDLER_H_

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif
#include "export.h"

#include "EventHandlerInterface.h"
#include "SceneGraph.h"
#include "Object3D.h"
#include "VertexDragger.h"

#include <osgManipulator/Projector>
#include <osgManipulator/Selection>
#include <osg/PolygonOffset>
#include <osg/io_utils>
#include <osgUtil/IntersectionVisitor>
#include <osg/PolygonMode>
#include <osg/ShapeDrawable>
#include <osg/Geometry>
#include <osg/LineWidth>
#include <osg/Quat>
#include <osg/CullFace>
#include <osg/AutoTransform>
#include <osg/Material>
#include <osgManipulator/Command>
#include <osgManipulator/CommandManager>
#include <osgUtil/DelaunayTriangulator>

#include <sstream>
#include <iostream>
#include <utility>

namespace PoLAR
{

class Viewer2D3D;
class VertexDragger;

/** \brief Pick handler of a geometry

The pick handler provides : \n
- a display of informations concerning the object pointed by the mouse, available for all the Object3D in the scene, the coordinates of  the nearest vertex pointed by the mouse if the geometry of the object pointed is available \n

For all the Object3D which have an available internal geometry and which are pickable AND editable :
- the creation of a geometry related to the picked vertex, called dragger, which permits to edit the picked vertex in an interactive way \n
- the possibility to modifiy internal geometry of the picked object in an interactive way \n
- the possibility to save in a chosen file the coordinates of the picked vertex \n \n

What you can do with the geometry pick handler and how you can do it is detailed here : \n
- Vertex edition : to edit a vertex, pick it thanks to the left mouse button; once a vertex is picked (a geometry related to this vertex is created), you can now  access to edition mode, by pressing a control key (left or right) and KEEP IT PRESSED during all the edition time. In vertex edition mode you can : \n
   \li translate the vertex along the x,y axis of the video frame, by pressing left mouse button \n
   \li translate the vertex along the z axis of the video frame or along a chosen epipolar line (depends on the type of the geometry pick handler chosen), by pressing middle mouse button \n
- Coordinates saving : After you set a file for saving with the method \ref setSaveFileName , you will be able to save the coordinates of picked vertex by typing the key 'w'. The coordinates will be saved in the chosen file, until you change the save file name saving will be made in the same file. \n

\note to be able to use the geometry pick handler the key '2' must be pressed in scene graph edition mode 

*/
class PoLAR_EXPORT GeometryPickHandler : public QObject, public osgGA::GUIEventHandler, public EventHandlerInterface
{
    Q_OBJECT

public:

    /** the different types of interaction \n
      type VIEW when picking is inactive \n
      type PICK when picking is active \n
    \note picking is activ when key '2' is pressed (in scene edition mode)
  */
    enum Mode {VIEW = 0, PICK = 2};

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param displayInfo true to display informations about the objets pointed by the mouse else false
  */
    GeometryPickHandler(bool displayInfo);

    /** destructor
  */
    virtual ~GeometryPickHandler();

    /** handle events */
    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, osg::Object*, osg::NodeVisitor*)
    {
        return handle(ea, aa);
    }

    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

    /** set the interaction mode of this pick handler */
    inline void setInteractMode(Mode mode) {_interactMode = mode;}

    /** get the interaction mode of this pick handler */
    inline Mode getInteractMode() {return _interactMode;}

    /** remove the current dragger from the scene graph */
    void stopManipulation();

    /** set a save file name for the 3D points coordinates saving */
    inline void setSaveFileName(char* name)
    {
        if (_saveFile) delete []_saveFile;
        _saveFile = new char[strlen(name)+1];
        strcpy(_saveFile, name);
    }

    /** get the save file name used for 3D points coordinates saving */
    inline char* getSaveFileName(char* name) { (void)name; return _saveFile; }

    /** get the current manipulated vertex */
    inline osg::Vec3f getVertexManipulated() { return _vertexManipulated; }


    /** set the key shortcut to go in geometry edition mode (default: '2')
     */
    void setGeometryEditionModeKey(osgGA::GUIEventAdapter::KeySymbol key)
    {
        _geometryEditionModeKey = key;
    }

    /** set the key shortcut to go in camera manipulation mode (default: '0')
     */
    void setCameraManipulationModeKey(osgGA::GUIEventAdapter::KeySymbol key)
    {
        _cameraManipulationModeKey = key;
    }

    /** set the key shortcut to save the currently manipulateed vertex info (default: 'w')
     */
    void setSaveManipulatedVertexInfoKey(osgGA::GUIEventAdapter::KeySymbol key)
    {
        _saveManipulatedVertexInfoKey = key;
    }

    /** set the key modifier to activate the vertex manipulation (default: left CTRL)
     */
    void setManipulateVertexModifier(osgGA::GUIEventAdapter::KeySymbol modifier)
    {
        _manipulateVertexModifier = modifier;
    }

    /** set the mouse button to use the 1D translation dragger (default: middle mouse button)
     */
    void setUsingTranslate1DVertexDraggerMouseButton(osgGA::GUIEventAdapter::MouseButtonMask button)
    {
        _usingTranslate1DVertexDraggerMouseButton = button;
    }

    /** set the mouse button to use the 2D translation dragger (default: left mouse button)
     */
    void setUsingTranslate2DVertexDraggerMouseButton(osgGA::GUIEventAdapter::MouseButtonMask button)
    {
        _usingTranslate2DVertexDraggerMouseButton = button;
    }

    /** set the mouse button to delete the vertex (default: right mouse button)
     * @note this functionality is currently not implemented
     */
    void setVertexDeletionMouseButton(osgGA::GUIEventAdapter::MouseButtonMask button)
    {
        _vertexDeletionMouseButton = button;
    }

    /** @} */


public slots :

    /** \name Public Slots */
    /** @{ */

    /** clean the display and the structure of the scenegraph from all elements added by this pick handler
  */
    void cleanUp();

    /** @} */


signals:

    /** \name Signals */
    /** @{ */

    /** signal is emitted when the dragger is attached to a vertex
  */
    void geometryEditionSignal();

    /** @} */

protected:

    /** create the dragger */
    virtual VertexDragger* createDragger(PoLAR::Object3D* object3D, osg::Vec3f& vertex, const osg::Matrixd& viewMatrix)=0;

    /** create the chosen dragger related to a PoLAR::Object3D and insert it in the scene graph
    \param object3D the PoLAR::Object3D to edit

    \note emits \ref geometryEditionSignal()
  */
    void manipulateVertex(PoLAR::Object3D* object3D, osg::Vec3f vertex, const osg::Matrixd viewMatrix);

    /** key shortcut to go in geometry edition mode (default: '1')
     */
    int _geometryEditionModeKey;

    /** key shortcut to go in camera manipulation mode (default: '0')
     */
    int _cameraManipulationModeKey;

    /** key shortcut to save the currently manipulated vertex info(default: 'w')
     */
    int _saveManipulatedVertexInfoKey;

    /** key modifier to activate the vertex manipulation (default: left CTRL)
     */
    int _manipulateVertexModifier;

    /** mouse button to use the 1D translation dragger (default: middle mouse button)
     */
    int _usingTranslate1DVertexDraggerMouseButton;

    /** mouse button to use the 2D translation dragger (default: left mouse button)
     */
    int _usingTranslate2DVertexDraggerMouseButton;

    /** mouse button to delete the vertex (default: right mouse button)
     * @note this functionality is currently not implemented
     */
    int _vertexDeletionMouseButton;

private:

    /** add a dragger to a node
    \param object3D the PoLAR::Object3D which has to be related to a dragger
  */
    osg::ref_ptr<osg::Group> addDraggerToVertex(PoLAR::Object3D* object3D, osg::Vec3f vertex, const osg::Matrixd viewMatrix);


    /** pointer to the dragger currently used */
    VertexDragger* _activeDragger;

    /** pointer to the structure used for storage of the intersections for one pick event */
    osgManipulator::PointerInfo _pointer;

    /** pointer to the node containing the dragger and the edited node */
    osg::ref_ptr<osg::Group> _manipulationGroup;


    /** used for storage of the mode (VIEW or PICK) */
    Mode _interactMode;

    /** vertex in current edition
  */
    osg::Vec3f _vertexManipulated;

    /** PoLAR::Object3D currently pointed
  */
    osg::ref_ptr<PoLAR::Object3D> _currentObservedObject;

    /** Original display mode of the edited object
  */
    osg::PolygonMode::Mode _origMode;

    /** boolean to store if a vertex can be edited
  */
    bool _editVertex;

    /** boolean to store if shift key is pressed
  */
    bool _shiftPressed;

    /** name of the file in which the manipulated vertex info will be saved
     */
    char *_saveFile;

};

}

#include "Viewer2D3D.h"
#endif // _POLAR_GEOMETRYPICKHANDLER_H_
