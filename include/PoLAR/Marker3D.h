/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef MARKER3D_H
#define MARKER3D_H

#include <osg/Geode>
#include <osg/Vec3>

namespace PoLAR
{

/**
 * @brief The Marker3D class provides an easy solution to create very quickly little shapes that can be used as 3D markers (to represent a 3D position, a vertex, etc.)
 */
class Marker3D: public osg::Geode
{
public:

    /**
     * @brief type of the shape
     */
    enum TYPE
    {
        CUBE,
        SPHERE
    };

    /**
     * constructor
     */
    Marker3D();

    /**
     * constructor
     * @param center the center position of the marker
     * @param size the size of the marker
     * @param type the type of the marker (see TYPE enum)
     */
    Marker3D(osg::Vec3& center, float size=0.1f, TYPE type=CUBE);

    /**
     * copy constructor
     * @param marker marker to copy
     * @param copyop Copy Operator used to control whether shallow or deep copy is used
     */
    Marker3D(const Marker3D& marker, const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);

protected:

    /**
     * @brief creates the associated shape
     */
    void createShape();

    /**
    * returns a cube in a drawable
     */
    osg::Drawable* createCube();

    /**
     * returns a sphere in a drawable
     */
    osg::Drawable* createSphere();

    /**
     * marker position
     */
    osg::Vec3 _center;

    /**
     * marker size
     */
    float _size;

    /**
     * marker type
     */
    TYPE _type;
};

}

#endif // MARKER3D_H
