#ifndef VEGAWORKER_H
#define VEGAWORKER_H

#include <integratorBase.h>
#include "SimulationWorker.h"

namespace PoLAR
{

/**
 * @brief Worker for Vega simulations
 */

class VegaWorker: public SimulationWorker
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     * @param integrator the integrator needed for the simulation
     */
    VegaWorker(IntegratorBase* integrator=NULL);

    /**
      * Destructor
      * */
    ~VegaWorker();

    /**
     * @brief sets the given integrator used for the simulation
     * @param integrator the integrator to use
     * @note it must be the same integrator linked to the 3D Vega object
     */
    void setIntegrator(IntegratorBase* integrator);

    /**
     * @brief set an external force to the buffer of external forces applied to the integrator
     * @param force the force to apply
     * @note this method does not check the source buffer integrity and size
     */
    void setExternalForce(double *force);

    /**
     * @brief add an external force to the buffer of external forces applied to the integrator
     * @param force the force to add
     * @note this method does not check the source buffer integrity and size
     */
    void addExternalForce(double* force);

    /**
     * @brief applies the external forces to the integrator
     */
    void setExternalForcesToIntegrator();

    /**
     * @brief applies the given force directly to the integrator
     */
    void setExternalForcesToIntegrator(double* force);

    /**
     * @brief resets the external forces to zero and recalculates the number of vertices
     * @note the forces currently applied to the integrator are also reset to zero
     */
    void resetExternalForces();

    /**
     * @brief resets the external forces to the default base force
     * @note generally called at the beginning of the run() method to clear the array of external forces
     */
    void setExternalForcesToBaseForce();

    /**
     * @brief sets a default base external force to a particular value
     * @param force the force to set as default base force
     * @note by default the base force is set to zero
     */
    void setBaseExternalForce(double *force);

    /**
     * @brief perform one timestep of the simulation
     * @return 0 on success, 1 on failure
     */
    int doTimestep();

private:

    /**
     * @brief allocates the arrays used to store the external forces
     */
    void allocFExt();

    /**
     * the integrator used for the simulation
     */
    IntegratorBase* _integrator;

    /**
     * array of the default base external force, used to reset the external forces to a default value (by default set to zero)
     */
    double *_f_extBase;

    /**
     * array containing the current external forces applied to the integrator
     */
    double *_f_ext;

    /**
     * the number of values (vertices) in the simulated model, used to store the number of values in the array of external forces
     */
    int _r;
};


}
#endif // VEGAWORKER_H
