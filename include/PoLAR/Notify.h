#ifndef POLARNOTIFY_H
#define POLARNOTIFY_H

#include <osg/Notify>

namespace PoLAR
{

/** Redirects OpenSceneGraph notification stream to qWarning (severity <= WARN) or qDebug (severity > WARN).
  * Useful to debug OSG-related code on mobile platform (where only qDebug out put is possible)
  * @see osg::setNotifyHandler
  */
class QDebugNotifyHandler: public osg::NotifyHandler
{
public:
     void notify(osg::NotifySeverity severity, const char *message);

};

}

#endif // POLARNOTIFY_H
