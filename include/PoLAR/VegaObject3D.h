#ifndef VEGAOBJECT3D_H
#define VEGAOBJECT3D_H

#include "PhysicsObject3D.h"
#include "volumetricMeshLoader.h"
#include "volumetricMesh.h"
#include "graph.h"
#include "integratorBase.h"
#include <list>

namespace PoLAR
{


class VegaObject3D;

/**
 * Class from which to derivate to create a force and apply it on a VegaObject3D
 */
class VegaInteraction
{
public:

    /**
     * @brief Virtual destructor. All the forces are deleted when the 3d object is deleted.
     */
    virtual ~VegaInteraction(){}

    /**
     * @brief Initialize the force
     * @param object the object on which the force will be applied
     */
    virtual void initializeForce(VegaObject3D *object)=0;

    /**
     * @brief apply the force
     * @param object the object on which the force will be applied
     * @return a pointer to the buffer containing the values of the force
     */
    virtual double* applyForce(VegaObject3D *object)=0;

    /**
     * @brief reset the force to the start state
     * @param object the object on which the force will be applied
     */
    virtual void resetForce(VegaObject3D *object)=0;
};


/**
 * Specialization of Object3D to represent deformable objects usign the Vega FEM engine
 */
class VegaObject3D: public PhysicsObject3D
{
    Q_OBJECT

public:
    /**
     * @brief Constructor
     * @param node the osg::Node to encapsulate in this object
     * @param vegaFile the .veg file containing the tetrahedral data
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    VegaObject3D(osg::Node *node, const char* vegaFile, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief Constructor
     * @param filename source file of the model to load
     * @param vegaFile the .veg file containing the tetrahedral data
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    VegaObject3D(const std::string& filename, const char* vegaFile, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief returns the integrator used by this object
     * @return the integrator used by the object
     */
    IntegratorBase *getIntegrator()
    {
        return _integrator;
    }

    /**
     * @brief sets a Vega integrator to this object. It overwrites the default integrator created at the object's creation
     * @param integrator the integrator to set to this object
     */
    void setIntegrator(IntegratorBase *integrator);

    /**
     * @brief returns the tetrahedral volumetric mesh representing this object Vega-side
     * @return the tetrahedral volumetric mesh
     */
    VolumetricMesh *getVolumetricMesh()
    {
        return _voluMesh;
    }

    /**
     * @brief returns the Vega graph representing the tetrahedral mesh (used for fast neighbors search)
     * @return the graph representing the tetrahedral mesh
     */
    Graph *getMeshGraph()
    {
        return _meshGraph;
    }


    /**
     * @brief returns the array of equivalency between OSG indices and Vega tetrahedral mesh vertex indices
     * @return the array of equivalency between OSG and Vega indices
     */
    unsigned int * getIndexInSoftBody()
    {
        return _indexInSoftBody;
    }

    /**
     * @brief returns the array of equivalency between Vega tetrahedral mesh vertex indices and OSG indices
     * @return the array of equivalency between Vega and OSG indices
     */
    unsigned int * getIndexInOsg()
    {
        return _indexInOsg;
    }

    /**
     * @brief returns the index of a vertex in the Vega tetrahedral mesh vertex from its counterpart in the OSG graphical representation
     * @param index of the vertex in the OSG representation
     * @return index of the vertex in the Vega tetrahedral mesh
     */
    int getVegaIndexFromOsgIndex(unsigned int indexInOsg)
    {
        if(indexInOsg<_verticesArray->size())
            return int(_indexInSoftBody[indexInOsg]);
        return -1;
    }


    //void updateOsgMesh();

    /**
     * @brief reset all the simulation, that means the object will be replaced to its original place, the forces will be reset to the rest position, etc.
     */
    void resetSimulation();

    /**
     * @brief adds a force to this object. See VegaInteraction
     * @param force the force to apply on the object
     */
    void addForce(VegaInteraction *force);

    /**
     * @brief Removes a force applied ot this this object. See VegaInteraction
     * @param force force to remove
     * @note this does not delete the force object. The user has to manage the memory deallocation himself.
     */
    void removeForce(VegaInteraction *force);

    /**
     * @brief Initializes the forces that have been added to this object.
     * @note this method should not be called manually.
     */
    void initForces();

    /**
     * @brief Applies the forces that have been added to this object.
     * @return a pointer to the buffer of the accumulated forces
     * @note this method should not be called manually.
     */
    double* applyForces();


    void applyForceToFace();

    /**
     * @brief sets the requestMeshUpdate flag to true in order to ask for an update of the OSG mesh
     */
    void requestMeshUpdate()
    {
        _requestMeshUpdate = true;
    }


protected:
    /**
      * destructor
      */
    ~VegaObject3D();

    /**
     * @brief computes a matching table between OSG vertices and Vega vertices
     */
    void computeOsgToVegaIndices();

    /**
     * @brief loads a volumetric mesh from a Vega file
     * @param vegaFile file to load
     * @return true on success
     */
    bool loadVolumetricMesh(const char* vegaFile);

    /**
     * @brief copies the original vertex positions for reset possibilities
     */
    void copyOriginalVertices();

    /**
     * @brief allocates the buffer of external forces
     */
    void allocFExt();

    /**
     * @brief This callback updates the OSG mesh everytime the Vega representation of the object has changed
     */
    struct MeshUpdater;

    /**
     * Vega integrator linked to this object. See Vega's IntegratorBase and derived classes
     */
    IntegratorBase *_integrator;

    /**
     * pointer to the tetrahedral representation of the volumetric object
     */
    VolumetricMesh *_voluMesh;

    /**
     * pointer to the graph representing the tetrahedral volumetric mesh. Used for fast neighbour search.
     */
    Graph *_meshGraph;

    /**
     * array of equivalency between OSG vertex indices and Vega tetrahedral mesh indices
     */
    unsigned int *_indexInSoftBody;

    /**
     * array of equivalency between Vega tetrahedral mesh vertex indices and OSG indices
     */
    unsigned int *_indexInOsg;

    /**
     * pointer to the array of vertices in the OSG representation of the object
     */
    osg::Vec3Array *_verticesArray;

    /**
     * the list of VegaInteraction forces to apply to the model
     */
    std::list<VegaInteraction*> _forcesList;

    /**
     * buffer of the external forces applied on each vertex
     */
    double *_externalForces;

    /**
     * buffer of the default value of the external forces (default is zero)
     */
    double *_defaultExternalForces;

    /**
     * flag indicating when the mesh must be updated, i.e. the simulation has performed one timestep
     */
    bool _requestMeshUpdate;

};

}

#endif // VEGAOBJECT3D_H
