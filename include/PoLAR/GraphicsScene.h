/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file GraphicsScene.h
\brief Management of the drawing of 2D objects

\author Pierre-Jean Petitprez
\date 2014
\note
Corresponding code: Viewer/GraphicsScene.cpp
*/
/** @} */

#ifndef _POLAR_GRAPHICSSCENE_H_
#define _POLAR_GRAPHICSSCENE_H_

#include "export.h"
#include <QGraphicsScene>
#include "DrawableObject2D.h"
#include "ListDrawableObject2D.h"
#include <list>


namespace PoLAR
{

/**
 * @brief Class inherited from Qt QGraphicsScene in order to draw 2D objects on top of the widget.
 * It also manages the drawing of the BaseViewer (QOpenGLWidget + osgViewer::Viewer) as a background.
 */
class PoLAR_EXPORT GraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:
    /**
     * @brief Constructor
     */
    GraphicsScene();

    /**
      * Destructor
      */
    ~GraphicsScene();


    /**
     * @brief Counts the number of 2D objects being drawn
     * @return the number of 2D objects being drawn
     */
    int countItems()
    {
        return this->items().size();
    }

    /**
     * @brief Remove all the items from the scene
     */
    void removeAllItems(bool withWidget=false);


    /**
     * allows the scene to draw overlays or not
     * @param overlay boolean to switch overlay on/off
     */
    void setOverlay(bool overlay)
    {
        _overlayPaint = overlay;
    }

    /**
     * Draws the scene foreground (used for overlay). Reimplemented from QGraphicsScene.
     * @param painter the QPainter used to draw
     * @param rect the rectangle to draw in the widget
     * @note emits overlayPaint
     */
    void drawForeground(QPainter *painter, const QRectF &rect);


   // void drawBackground(QPainter* painter, const QRectF &rect);

    /**
     * set the current index of the image displayed (from a sequence of images)
     */
    void setImageIndex(int i)
    {
        _imageIndex = i;
    }

    /**
     * get the current index of the image displayed (from a sequence of images)
     * If it is different than the actual index, it means that the scene has to be updated with the list of items corresponding to the current displayed image
     */
    int getImageIndex()
    {
        return _imageIndex;
    }


   virtual  void update();


signals:

    /**
     * emitted when an overlay painting is asked
     */
    void overlayPaint(QPainter*, const QRectF&);

private:

    /**
     * Boolean to activate / deactivate the overlay text
     */
    bool _overlayPaint;

    /**
     * index of the image currently displayed (from the image sequence).
     * If it is different than the actual index, it means that the scene has to be updated with the list of items corresponding to the current displayed image
     */
    int _imageIndex;

};

}

#endif // _POLAR_GRAPHICSSCENE_H_
