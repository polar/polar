/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Polygon2D.h
\brief Management of 2D polygons

\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/Polygon2D.cpp
*/
/** @} */

#ifndef _POLAR_POLYGON2D_H_
#define _POLAR_POLYGON2D_H_

#include "Object2D.h"


namespace PoLAR
{

/**  \brief Management of 2D polygons

*/
class PoLAR_EXPORT Polygon2D : public Object2D
{
public:

    enum Orientation {CLOCKWISE, COUNTERCLOCKWISE};

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param list markers list contained by the 2D polygon
  */
    Polygon2D(std::list<Marker2D *> list);

    /**
     * default constructor
     */
    Polygon2D();

    /**
     * copy constructor
     */
    Polygon2D(Polygon2D &obj);

    /** set _currentList so that it points to one marker in the segment and
      _currentList->next (or _listMarkers) points to the other one
    \return -1 if no point is present in the polygon
  */
    double selectSegment(double x, double y, double precision);

    /** set _currentList to NULL (no selection)
  */
    void unselectSegment();

    /** general object selection
  */
    float select(double x, double y, double precision) {return selectSegment(x,y,precision);}

    /** marker adding is different since it must be added on the right segment
  */
    virtual void addMarker(Marker2D *marker);

    /** get the total length of the polygon
    \return the total length of the polygon
  */
    virtual double length() const;


    /** revert the orientation of a polygon
  */
    virtual void revertOrientation();

    /**
     * @brief returns a copy of this object
     */
    virtual Polygon2D* clone()
    {
        return new Polygon2D(*this);
    }

    /** @} */


private:

    /** get the segment the closest to a point
    \return the segment the closest to a point
    \param x abscissa of the point pointed
    \param y ordinate of the point pointed
  */
    Marker2D *getClosestSegment(double x, double y);

    /** get the segment the closest to a marker
    \return the segment the closest to a marker
    \param marker the marker pointed
  */
    Marker2D *getClosestSegment(Marker2D *marker);
};

}

#endif // _POLAR_POLYGON2D_H_
