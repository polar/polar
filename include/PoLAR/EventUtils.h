/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef EVENTUTILS_H
#define EVENTUTILS_H

#include "export.h"
#include <osgUtil/LineSegmentIntersector>
#include <osgViewer/View>
#include <osgText/Text>

namespace PoLAR
{

/** compute intersections with the items in the scene graph when picking/mouse pointing */
PoLAR_EXPORT bool computeIntersections(osgViewer::View* viewer, float x,float y, osgUtil::LineSegmentIntersector::Intersections& intersections,osg::Node::NodeMask traversalMask = 0xffffffff);


/** create a diplay area for writing information to the screen */
PoLAR_EXPORT osg::Node* createHUD(int width, int height, osgText::Text* updateText, int precision=10);

}

#endif // EVENTUTILS_H
