/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file DefaultPickHandler.h
\brief Default typed pick handler of a geometry

\author Frédéric Speisser
\date year 2007
\note 
Corresponding code: SceneGraph/DefaultPickHandler.cpp
*/
/** @} */

#ifndef _POLAR_DEFAULTPICKHANDLER_H_
#define _POLAR_DEFAULTPICKHANDLER_H_

#include "GeometryPickHandler.h"


namespace PoLAR
{

class Viewer2D3D;

/** \brief Default typed pick handler of a geometry


For general information of use take a look at \ref GeometryPickHandler class

The default pick handler provides a dragger with which you can edit the picked vertex along x,y and z axis of the camera frame \n

*/


class PoLAR_EXPORT DefaultPickHandler : public PoLAR::GeometryPickHandler
{
    Q_OBJECT

public:

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param displayInfo true to display informations about the objets pointed by the mouse else false
  */
    DefaultPickHandler(bool displayInfo=true) : GeometryPickHandler(displayInfo) {}

    /** @} */


protected:

    /** create the dragger */
    virtual PoLAR::VertexDragger* createDragger(PoLAR::Object3D* object3D, osg::Vec3f &vertex, const osg::Matrixd &viewMatrix);

};


}

#endif // _POLAR_DEFAULTPICKHANDLER_H_
