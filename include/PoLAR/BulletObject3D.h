/** @{ */
/** \file BulletObject3D.h
\brief PoLAR Object3D with an encapsulation of a rigid body using the Bullet physics engine.

\author Pierre-Jean Petitprez
\date 2015
\note
Corresponding code: Object3D/BulletObject3D.cpp
*/
/** @} */

#ifndef BULLETOBJECT3D_H
#define BULLETOBJECT3D_H

#include <btBulletDynamicsCommon.h>
#include "PhysicsObject3D.h"
#include "MotionState.h"


namespace PoLAR
{

/**
 * Specialization of Object3D to represent solid objects with collisions using the Bullet physics engine
 */
class PoLAR_EXPORT BulletObject3D: public PhysicsObject3D
{
public:

    /**
     * @brief Constructor (for a rigid body)
     * @param node pointer to the osg::Node containing the visual object
     * @param mass the mass to set to the physical representation of the object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    BulletObject3D(osg::Node *node, float mass = 1.0f, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief Constructor (for a rigid body)
     * @param filename source file of the object to load
     * @param mass the mass to set to the physical representation of the object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    BulletObject3D(const std::string& filename, float mass = 1.0f, bool material = false, bool editable = false, bool pickable = false);


    /**
     * @brief get the btRigidBody representing this object in thze physics Bullet world
     * @return the Bullet rigid body associated with this object
     */
    btRigidBody *getBtRigidBody();


    /**
     * @brief create the btRigidBody associated with this Object3D
     * @param collisionShape a collision shape that will be used instead of creating a default one
     */
    void createRigidBody(btCollisionShape *collisionShape);

    /**
     * @brief create the btRigidBody associated with this Object3D with a default collision shape
     */
    void createRigidBody();


    /**
     * @brief set a custom collision shape
     * @param shape the shape to apply for the creation of the rigid body
     */
    void setCollisionShape(btCollisionShape* shape);

    /**
     * @brief get the collision shape of the object
     * @return the collision shape of the object if already created, NULL otherwise
     */
    btCollisionShape *getCollisionShape()
    {
        return _collisionShape;
    }

    /**
     * @brief set the mass of the object
     * @param newMass the new mass of the object
     * @note before calling this method, the corresponding btRigidBody must be removed from the Bullet world and readded after it.
     */
    void setMass(float newMass);

    /**
     * @brief get the current mass of the object
     * @return the mass of the object
     */
    float getMass()
    {
        return _mass;
    }

    /**
     * @brief update the physics behaviour. Must be called after every change on the object (rotation/translation/scale)
     */
    void updatePhysicsBody();

    /**
     * @brief reset all the simulation, that means the object will be replaced to its original place.
     */
    virtual void resetSimulation();

    /**
     * @brief set the original angular velocity of the object
     * @param x x component of the velocity
     * @param y y component of the velocity
     * @param z z component of the velocity
     */
    void setAngularVelocity(double x, double y, double z)
    {
        _angularVelocity = btVector3(x,y,z);
    }

    /**
     * @brief set the original linear velocity of the object
     * @param x x component of the velocity
     * @param y y component of the velocity
     * @param z z component of the velocity
     */
    void setLinearVelocity(double x, double y, double z)
    {
        _linearVelocity = btVector3(x,y,z);
    }


    virtual void setEditOff();

protected:

    /**
      * Destructor
      */
    ~BulletObject3D();


    /**
     * @brief create a Bullet triangle mesh from the OSG mesh
     * @return the triangle mesh
     */
    //btTriangleMesh *createTriangleMesh();

    /**
      * @brief create the collision shape of this object
      * @return the create collision shape
      */
    //btConvexTriangleMeshShape *createShape();


    /**
      * @brief create a convex hull shape
      * @param withTransform if true, the convex hull shape will be translated and scaled using the scaling and the original center of the OSG mesh
      * @return the newly created convex hull collision shape
      */
    btConvexHullShape *createConvexHullShape(bool withTransform=true);

    /**
     * @brief conversion of osg::Vec3d to Bullet btVector3
     * @param v the OpenSceneGraph Vec3
     * @return the equivalent Bullet btVector3
     */
    btVector3 asBtVector3(const osg::Vec3d& v)
    {
        return btVector3(v.x(), v.y(), v.z());
    }



    /**
     * pointer to the body representation of the object
     */
    btCollisionObject *_bulletBody;


    /**
     * pointer to the collision shape of the object
     */
    btCollisionShape *_collisionShape;


    /**
     * the MotionState linked to this rigid body
     */
    MotionState *_motionState;

    /**
     * boolean used to know if the collision shape has been set by the user or computed by default
     */
    bool _hasDefaultCollisionShape;

    /**
     * the mass of the object
     */
    float _mass;

    /**
     * angular velocity to be set to the object at the start of the simulation
     */
    btVector3 _angularVelocity;

    /**
     * linear velocity to be set to the object at the start of the simulation
     */
    btVector3 _linearVelocity;

};

}

#endif // BULLETOBJECT3D_H
