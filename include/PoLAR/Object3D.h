/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Object3D.h
\brief Encapsulation of OpenSceneGraph nodes

This class provides methods for the management of the display of 3D objects (corresponding to OpenSceneGraph nodes), it provides the user with a level of abstraction from OpenSceneGraph. \n
It allows the user to easily connect Qt widgets to 3D objects

\author Frédéric Speisser, Pierre-Jean Petitprez
\date 2007, 2015
\note
Corresponding code: Object3D/Object3D.cpp
*/
/** @} */


#ifndef _POLAR__OBJECT3D_H_
#define _POLAR__OBJECT3D_H_

#include "BaseImage.h"
#include "BaseViewer.h"
#include "SceneGraph.h"
#include <QtCore/QObject>
#include <osg/Node>
#include <osg/Group>
#include <osg/Switch>
#include <osg/StateSet>
#include <osg/BlendEquation>
#include <osg/BlendFunc>
#include <osg/BlendColor>
#include <osg/Material>
#include <osg/Matrixd>
#include <osgManipulator/Selection>
#include <osgManipulator/Dragger>
#include <osg/NodeCallback>


namespace PoLAR
{

class Object3D;

/**
 * @brief Convenient struct for creating callbacks.\n
 * All the programmer needs to do is inherit from this struct and write the convenient update() method.
 * Such callback can be applied to an Object3D using:
 *   object->setUpdateCallback(new MyCallback());
 * provided that MyCallback inherits from Object3DCallback.
 */
struct PoLAR_EXPORT Object3DCallback : public osg::NodeCallback
{
    virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);

    virtual void update(Object3D* object) = 0;
};


/** \brief Encapsulation of OpenSceneGraph nodes

This class provides a higher level of abstraction to easily manage 3D objects (or group of objects) :  object appearence (color, shininess, transparency), position/orientation/size, object state (displayed, editable, pickable), scenegraph(s) to be attached to, shadowing behaviour. \n
Position, orientation and size of the object can be changed dynamically thanks to different methods. \n
The position/orientation/size can be modified under constraints, that is to say in a custom frame (thanks to the method \ref setFrameMatrix ).

The encapsulation provides also different slots easily connectable to custom widgets.

\note transparency may be not so "nice" as expected for some objects, it seems to be an OpenSceneGraph limitation (since the triangles for one object seem not to be sorted before displaying...)
*/
class PoLAR_EXPORT Object3D : public QObject, public osg::Switch
{
    Q_OBJECT

public :

    /** \name Public Member Functions */
    /** @{ */

    /**
     * @brief constructor
     * @param filename source file of the model to load
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not (see testpick example)
     */
    Object3D(const std::string& filename, bool material = false, bool editable = false, bool pickable = false);


    /**  constructor
    \param node node to encapsulate in this object
    \param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
    \param editable boolean to specify if this object can be dynamically modified or not
    \param pickable boolean to specify if this object can be picked or not (see testpick example)
    \note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
  */
    Object3D(osg::Node *node, bool material = false, bool editable = false, bool pickable = false);


    /**
     * @brief copy constructor using osg::CopyOp for managing deep vs shallow copy
     */
    Object3D(const Object3D& object, const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);


    /**
     * @brief clone method using osg::CopyOp for managing deep vs shallow copy
     */
    virtual osg::Object* clone(const osg::CopyOp &copyop) const;

    /**
     * @brief Optimize the object with default options
     * @note see osgUtil::SmoothingVisitor and osgUtil::Optimizer optimize for more info
     */
    void optimize();

    /** assign a material to this object
    \param ambientX x component of the ambient color to set (range : [0.0, 1.0])
    \param ambientY y component of the ambient color to set (range : [0.0, 1.0])
    \param ambientZ z component of the ambient color to set (range : [0.0, 1.0])
    \param diffuseX x component of the diffuse color to set (range : [0.0, 1.0])
    \param diffuseY y component of the diffuse color to set (range : [0.0, 1.0])
    \param diffuseZ z component of the diffuse color to set (range : [0.0, 1.0])
    \param specularX x component of the specular color to set (range : [0.0, 1.0])
    \param specularY y component of the specular color to set (range : [0.0, 1.0])
    \param specularZ z component of the specular color to set (range : [0.0, 1.0])
    \param shininess shininess to set (range : [0.0, 128.0])
    \note if a material is already assigned to this object, it will be used and not overriden. In that case to change the material caracteristics use the following methods : setAmbientColor, setDiffuseColor, setSpecularColor, setShininess
  */
    void setMaterial(float ambientX = 0.3, float ambientY = 0.3, float ambientZ = 0.3,
                     float diffuseX = 0.3, float diffuseY = 0.3, float diffuseZ = 0.3,
                     float specularX = 1.0, float specularY = 1.0, float specularZ = 1.0,
                     float shininess = 15.0);

    /** set the color of this object's material
    \param ambientX x component of the ambient color to set (range : [0.0, 1.0])
    \param ambientY y component of the ambient color to set (range : [0.0, 1.0])
    \param ambientZ z component of the ambient color to set (range : [0.0, 1.0])
    \note only usable (with expected result) if this object has a material
  */
    void setAmbientColor(float ambientX, float ambientY, float ambientZ);

    /** set the color of this object's material
    \param diffuseX x component of the diffuse color to set (range : [0.0, 1.0])
    \param diffuseY y component of the diffuse color to set (range : [0.0, 1.0])
    \param diffuseZ z component of the diffuse color to set (range : [0.0, 1.0])
    \note only usable (with expected result) if this object has a material
  */
    void setDiffuseColor(float diffuseX, float diffuseY, float diffuseZ);

    /** set the color of this object's material
    \param specularX x component of the specular color to set (range : [0.0, 1.0])
    \param specularY y component of the specular color to set (range : [0.0, 1.0])
    \param specularZ z component of the specular color to set (range : [0.0, 1.0])
    \note only usable (with expected result) if this object has a material
  */
    void setSpecularColor(float specularX, float specularY, float specularZ);

    /** set the shininess of this object
    \param shininess shininess to set (range : [0.0, 128.0])
    \note only usable (with expected result) if this object has a material
  */
    void setShininess(float shininess);

    /** set this object transparent
    \note only usable (with expected result) if this object is editable
  */
    void setTransparencyOn();

    void setTransparencyOn(float value);

    /** set the transparency of this object off
  */
    void setTransparencyOff();

    /** update the transparency of this object
    \param value value of opacity of this object (range : [0.0 (invisible), 1.0 (completly opaque)])
    \note the object must have been set to transparent before
  */
    void updateTransparency(float value=0.5);

    /** check if this object is in a transparent state
    \return true if this object is transparent, else false
  */
    bool isTransparent();

    /** get the level of transparency of this object
    \return the transparency value of this object
  */
    float getTransparencyValue();

    /** set a name (as a const char*) to this object
    \param name the name to set
    \note this method is already defined for a native osg::Object but since this object is an osg::Object AND also a QObject, the method is rewritten here to avoid possible conflicts
  */
    inline void setName(const char* name) {this->osg::Object::setName(name);}

    /** set a name (as a const std::string&) to this object
    \param name the name to set
    \note this method is already defined for a native osg::Object but since this object is an osg::Object AND also a QObject, the method is rewritten here to avoid possible conflicts
  */
    inline void setName(const std::string& name) {this->osg::Object::setName(name);}

    /** get the name of this object
    \return the name of this object (as a const std::string&)
    \note this method is already defined for a native osg::Object but since this object is an osg::Object AND also a QObject, the method is rewritten here.
  */
    inline const std::string& getName() const {return this->osg::Object::getName();}

    inline const QString getNameAsQString() const { return QString::fromStdString(getName()); }

    /** get the original node encapsulated by this object
    \return the original node encapsulated by this object
  */
    inline osg::Node* getOriginalNode() {return _originalNode.get();}

    /**
     * Set user data (reimplementation of the osg::Object method to avoid confusion with the QObject::setUserData method)
     * @param obj the object to set as user data
     */
    inline void setUserData(osg::Referenced *obj) {this->osg::Object::setUserData(obj);}

    /**
     * check if this object has a texture
     */
    bool hasTexture();

    /** check if this object has a material set
  */
    inline bool hasMaterial() {return _hasMaterial;}

    /** check if this object is editable
  */
    inline bool isEditable() {return _isEditable;}

    /** set this object editable
  */
    virtual inline void setEditOn() {_isEditable = true;}

    /** set this object non editable
  */
    virtual inline void setEditOff() {_isEditable = false;}

    /** check if this object is pickable
  */
    inline bool isPickable() {return _isPickable;}

    /** set this object pickable
  */
    inline void setPickOn() {_isPickable = true;}

    /** set this object non pickable
  */
    inline void setPickOff() {_isPickable = false;}

    /** check if this object is displayed
  */
    inline bool isDisplayed() {return this->getValue(0);}

    /** set this object to be displayed
  */
    void setDisplayOn();

    /** set this object not to be displayed (it is different as if it is fully transparent : in this case this object is not a part of the rendering graph)
  */
    void setDisplayOff();

    /** check if this object is in a phantom state
     \note a phantom object is typically used in augmented reality for occluding and virtual shadows reflections
  */
    inline bool isAPhantom() {return _isAPhantom;}

    /** set this object to be a phantom object (typically used in real augmented scenes for occluding and virtual shadows reflection)
  */
    void setPhantomOn();
    void setPhantomOn(SceneGraph* sceneGraph);

    /** set this object not to be a phantom object
  */
    void setPhantomOff();
    void setPhantomOff(SceneGraph *sceneGraph);

    /** check if this object is a shadow receiver
  */
    inline bool isAShadowReceiver() {return _isAShadowReceiver;}

    /** set this object to be a shadow receiver
  */
    void setShadowReceiverOn();

    /** set this object not to be a shadow receiver
  */
    void setShadowReceiverOff();

    /** check if this object is a shadow caster
  */
    inline bool isAShadowCaster() {return _isAShadowCaster;}

    /** set this object to be a shadow caster
  */
    void setShadowCasterOn();

    /** set this object not to be a shadow caster
  */
    void setShadowCasterOff();

    /**
     * @brief add a scenegraph to the list of scenegraphs which contain this object
     * @param sg the scenegraph to add in the list
     */
    void addSceneGraph(SceneGraph* sg);

    /**
     * @brief remove a scenegraph from the list of scenegraphs which contain this object
     * @param sg the scenegraph to remove from the list
     */
    void removeSceneGraph(SceneGraph* sg);

    /** set the frame in which this object will be manipulated, default is world frame \n
      can typically be used to manipulate this object under constraints
    \param matrix matrix defining the frame to set
  */
    void setFrameMatrix(osg::Matrixd matrix);

    /** get the matrix defining the frame in which this object is manipulated
    \note only usable (with expected result) if this object is editable
  */
    osg::Matrixd getFrameMatrix();

    /** undo all manipulations which were done in the current frame (defined by the matrix set by setFrameMatrix(osg::Matrixd matrix))
    \note only usable (with expected result) if this object is editable
  */
    void resetAllManipulations();

    /** assign a transformation to this object thanks to a matrix (CAUTION : all previous transformations are overridden !!)
    \param matrix the matrix to use for object transformation
    \note only usable (with expected result) if this object is editable
  */
    void setTransformationMatrix(osg::Matrixd matrix);

    /** get the transformation assigned to this object
    \note only usable (with expected result) if this object is editable
  */
    const osg::Matrixd& getTransformationMatrix();

    /** translate this object \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \param tx x component of the translation
    \param ty y component of the translation
    \param tz z component of the translation
    \note only usable (with expected result) if this object is editable
  */
    virtual void translate(double tx, double ty, double tz);

    /** translate this object \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \param trans the translation vector to apply
    \note only usable (with expected result) if this object is editable
  */
    virtual void translate(osg::Vec3d trans);

    /** scale this object \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \param sx x component of the scaling
    \param sy y component of the scaling
    \param sz z component of the scaling
    \note only usable (with expected result) if this object is editable
  */
    virtual void scale(double sx, double sy, double sz);

    /**
     * scale this object \n
     * transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
     * @param s scaling vector
     */
    virtual void scale(osg::Vec3d s);

    /**
     * scale this object with the same factor for all dimensions \n
     * transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
     * @param s scaling factor
     */
    virtual void scale(double s);

    /** rotate this object \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \param angle angle of the rotation
    \param x x component of the vector used for rotation axis
    \param y y component of the vector used for rotation axis
    \param z z component of the vector used for rotation axis
    \note only usable (with expected result) if this object is editable
  */
    virtual void rotate(double angle, double x, double y, double z);


    /**
     * rotate this object \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
     * @param q quaternion representing the rotation
     */
    virtual void rotate(osg::Quat q);


    /** get the position of this object in local frame that is to say expressed in the frame set with setFrameMatrix(osg::Matrixd matrix) (default is world frame)
       \return the position vector
     */
    osg::Vec3d getPosition() const;

    /** get the rotation of this object in local frame that is to say expressed in the frame set with setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \param angle storage for the angle of the rotation to get
    \param x storage for the x component of the vector used for rotation axis to get
    \param y storage for the y component of the vector used for rotation axis to get
    \param z storage for the z component of the vector used for rotation axis to get
    \note only usable (with expected result) if this object is editable
     */
    void getRotation(double *angle, double *x, double *y, double *z) const;

    /**
     * @brief get the rotation of this object in local frame that is to say expressed in the frame set with setFrameMatrix(osg::Matrixd matrix) (default is world frame)
     * @return the rotation as a quaternion
     */
    osg::Quat getRotation() const;

    /** get the scaling parameters of this object in local frame that is to say expressed in the frame set with setFrameMatrix(osg::Matrixd matrix) (default is world frame)
     * \return the scale vector
     */
    osg::Vec3d getScale() const;

    /**
     * get the current origin coordinates of the object
     * @return the current origin coordinates of the object
     */
    osg::Vec3d getOrigin() const
    {
        return _origin;
    }

    /**
     * compute the current position of the origin
     */
    void computeCurrentOrigin();

    /**
     * get the transform node on which the transformations are applied (rotation/translation/scale)
     * @return the transform node
     */
    osg::MatrixTransform* getTransformNode()
    {
        return _selNode.get()->asMatrixTransform();
    }

    /**
     * Set the drawable contained in the Object3D to dirty, to ask OSG to redraw it
     */
    void dirty();

    /**
     * set a texture to the original geode encapsulated in this Object3D
     * @param name the path to the texture to apply
     * @param flipVertically set to true if the texture coordinates should be vertically flipped (since OSG and Qt do not use the same internal formats for textures)
     */
    void setObjectTextureState(const std::string& name, bool flipVertically=false);

    /**
     * set a texture to the original geode encapsulated in this Object3D
     * @param image the PoLAR::Image to apply
     * @param flipVertically set to true if the texture coordinates should be vertically flipped (since OSG and Qt do not use the same internal formats for textures)
     */
    void setObjectTextureState(BaseImage *image,  bool flipVertically=false)
    {
        setObjectTextureState(image->asImage(), flipVertically);
    }

    /**
     * set a texture to the original geode encapsulated in this Object3D
     * @param image the osg::Image to apply
     * @param flipVertically set to true if the texture coordinates should be vertically flipped (since OSG and Qt do not use the same internal formats for textures)
     */
    void setObjectTextureState(osg::Image *image, bool flipVertically=false);


    /** find the vertex which is the nearest from a given 3D point
    \param point reference point
    \param foundVertex storage for the vertex found and its drawable index (in the case of an object with multiple drawables)
    \return true if a vertex was found, false otherwise
  */
    bool findNearestVertex(osg::Vec3f point, std::pair<unsigned int, osg::Vec3f> &foundVertex);


    /**
     * @brief Find and return the list of vertices of a node
     * @return the list of vertices if found, NULL otherwise
     */
    static osg::Vec3Array *getVertexList(osg::Node *node, unsigned int index);

    /**
     * @brief Find and return the list of vertices of the original node
     * @return the list of vertices if found, NULL otherwise
     */
    osg::Vec3Array *getVertexList(unsigned int index) const
    {
        return getVertexList(_originalNode.get(), index);
    }

    /**
     * @brief find a drawable with its index
     * @param node the node in which to find the drawable
     * @param index the index number of the drawable
     * @return the drawable found
     */
    static osg::Drawable *getDrawable(osg::Node *node, unsigned int index);

    /**
     * @brief find a drawable with its index in the original node associated to this 3D object
     * @param index the index number of the drawable
     * @return the drawable found
     */
    osg::Drawable *getDrawable(unsigned int index) const
    {
        return getDrawable(_originalNode.get(), index);
    }

    /**
     * @brief returns the number of Drawables of the node encapsulated in this Object3D
     */
    unsigned int getNumDrawables() const;

    /**
     * @brief add a dragger to manipulate the object
     */
    void addDragger(osgManipulator::Dragger* dragger);

    /**
     * @brief remove the dragger previously added
     */
    void removeDragger();

    void generateTextureCoordinates();

    /**
     * @brief Helper method to create a ground
     * @param sizeX the size of the ground following the X coordinate
     * @param sizeY the size of the ground following the Y coordinate
     * @param posZ the position of the ground following the Z coordinate, i.e. the height of the ground
     * @return the ground created, already encapsulated in an Object3D
     * @note by default this object is located at position (0,0,0). Object edition is enable by default. You can use the translate() methods to move the object if needed.
     * @note this object is useful in 3d scenes to receive shadows from other objects. Coupled with the setPhantomOn() option, it can display received shadows as an overlay of the background image.
     */
    static Object3D* createGround(float sizeX=5.0f, float sizeY=5.0f, float posZ=0.0f);

    /** @} */


public slots :

    /** \name Public Slots */
    /** @{ */

    /** when repainting is necessary
    \note emits updateSignal()
  */
    void refresh();

    /** manage the status (on/off) of transparency
    \param state state of the widget associated
    \note if state=0 off \n
              if state=1 no change  \n
              if state=2 on  \n
              to use typically with a Qt::QButton
  */
    void toggleTransparencySlot(int state);

    /** manage the transparency level of this object
    \param value value to set
  */
    void setTransparencyValueSlot(float value);

    /** manage the displaying of this object
    \param state state of the widget associated
    \note if state=0 off \n
              if state=1 no change  \n
              if state=2 on  \n
              to use typically with a Qt::QButton
    \note the object must have been set to transparent before
  */
    void displayOnOffSlot(int state);

    /** manage the translation of this object along X axis
    \param tx x component of the translation to use
    \note only usable (with expected result) if this object is editable
  */
    void translateXSlot(double tx);

    /** manage the translation of this object along Y axis
    \param ty y component of the translation to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void translateYSlot(double ty);

    /** manage the translation of this object along Z axis
    \param tz z component of the translation to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void translateZSlot(double tz);

    /** manage the rotation of this object in relation to X axis
    \param ax angle of the rotation to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void rotateXSlot(double ax);

    /** manage the rotation of this object in relation to Y axis
    \param ay angle of the rotation to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void rotateYSlot(double ay);

    /** manage the rotation of this object in relation to Z axis
    \param az angle of the rotation to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void rotateZSlot(double az);

    /** manage the scaling of this object along X axis
    \param sx x component of the scaling to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void scaleXSlot(double sx);

    /** manage the scaling of this object along Y axis
    \param sy y component of the scaling to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void scaleYSlot(double sy);

    /** manage the scaling of this object along Z axis
    \param sz z component of the scaling to use \n
      transformation is made in the frame defined by the matrix set by setFrameMatrix(osg::Matrixd matrix) (default is world frame)
    \note only usable (with expected result) if this object is editable
  */
    void scaleZSlot(double sz);


    /** @} */


signals:

    /** \name Signals */
    /** @{ */

    /** signal is emitted when repainting is necessary
  */
    void updateSignal();

    /** @} */

protected :

    /**  destructor
  */
    virtual ~Object3D();

    /**
     * @brief called by the constructors to perform all common construction operations
     */
    void setObjectParameters();

    /** Original node encapsulated by this object
  */
    osg::ref_ptr<osg::Node> _originalNode;

    /** pointer to the selection node of this object\n
     * osg::Selection is equivalent to osg::MatrixTransform
  */
    osg::ref_ptr<osgManipulator::Selection> _selNode;

    /** matrix used for defining the frame in which this object is manipulated
  */
    osg::Matrixd _frameMatrix;

    /** matrix used to store the transformations affected to this object before a frame change
  */
    osg::Matrixd _transformationMatrix;

    /** current coordinates of the origin of this object
  */
    osg::Vec3d _origin;

    /** original center of this object
  */
    osg::Vec3d _originalCenter;

    /** pointer to a dragger for object manipulation
     */
    osg::ref_ptr<osgManipulator::Dragger> _dragger;

private:

    /** update the node mask according to the shadow casting/receiving options
     */
    void updateNodeMask();

    /** Generates a warning message indicating the object is not editable
     */
    void printNonEditable();

    /** Generates a warning message indicating the object has no material
     */
    void printMaterialNotSet();

    /** Generates a warning message indicating the object cannot be picked
     */
    void printNonPickable();

    /** Generates a warning message indicating the object is already a shadow caster or already not one
     */
    void printShadowCaster(bool b);

    /** Generates a warning message indicating the object is already a shadow receiver or already not one
     */
    void printShadowReceiver(bool b);

    /**
     * this method creates uniform variables to be used in GLSL shaders (material specifications)
     */
    void createMaterialUniforms();

    /**
     * this method updates the uniform variables to be used in GLSL shaders (material specifications)
     */
    void updateMaterialUniforms();

    /**
     * set default material to be used when no material is set (non-fixed pipeline only)
     */
    void createDefaultMaterial();


    /** boolean to store if a material is assigned to this object
  */
    bool _hasMaterial;

    /** boolean to store editable state of this object
  */
    bool _isEditable;

    /** boolean to store pickable state of this object
  */
    bool _isPickable;

    /** boolean to store phantom state of this object
  */
    bool _isAPhantom;

    /** boolean to store shadow caster state of this object
  */
    bool _isAShadowCaster;

    /** boolean to store shadow receiver state of this object
  */
    bool _isAShadowReceiver;

    /** pointer to this object stateset
  */
    osg::ref_ptr<osg::StateSet> _objectStateSet;

    /** blending value
  */
    float _transparencyValue;

    /** container to store pointers to every scenegraph to which this object is added
     */
    std::list<SceneGraph*> _sgList;
};

}

#include "SceneGraph.h"
#endif //_POLAR__OBJECT3D_H_
