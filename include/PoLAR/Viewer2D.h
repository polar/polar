/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Viewer2D.h
\brief Base class for the management of 2D objects

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2015
\note 
Corresponding code: Viewer/Viewer2D.cpp
*/
/** @} */

#ifndef _POLAR_VIEWER2D_H_
#define _POLAR_VIEWER2D_H_

#include "GraphicsViewer.h"
#include "ListDrawableObject2D.h"
#include <QtSvg>
#include <vector>

namespace PoLAR
{

/** \brief Base class for the management of 2D objects
*/
class PoLAR_EXPORT Viewer2D : public GraphicsViewer
{
    Q_OBJECT

public:

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
      \param format enables the buffers given in arguments
      \param parent parent Qwidget
      \param name assigned to the widget
      \param shareWidget widget to share openGL context with
      \param f flags to specify various window-system properties for the widget
  */
    Viewer2D(QWidget *parent = 0, const char *name = 0, WindowFlags f = 0, bool distortion=false);


    /**
   * set the background image sequence
   * @param img the PoLAR::BaseImage
   * @param ResizeParent boolean to specify if the parent must be resized or not
   * @param ShareObjects boolean to specify if the images must share the same list of 2D objects
   */
    void setBgImage(BaseImage *img=(BaseImage *)NULL, bool resizeParent=true, bool shareObjects=false, bool resetObjects=true);


    /** set the header of the file used to save markers
    \param fname the name of the file
  */
    void setSaveFilename(const char *fname)
    {
        if (_saveFile) delete []_saveFile;
        _saveFile = new char[strlen(fname)+1];
        strcpy(_saveFile, fname);
    }

    /** set the header of the file used to load markers
    \param fname the name of the file
  */
    void setLoadFilename(const char *fname)
    {
        if (_loadFile) delete []_loadFile;
        _loadFile = new char[strlen(fname)+1];
        strcpy(_loadFile, fname);
    }

    /** get the number of 2D objects (displayed or not) in this viewer
    \return the number of 2D objects (displayed or not) in this viewer
  */
    int getNumObjects2D() const;

    /** get the list of 2D objects (ListDrawableObject2D) attached to this viewer currently drawn
    \return a pointer to the list of 2D objects (ListDrawableObject2D) attached to this viewer
  */
    ListDrawableObject2D *getDrawableObject2DList() const {return _drawableObjectsList;}

    /** get the list of 2D objects (ListDrawableObject2D) attached to this viewer at index i
    \return a pointer to the list of 2D objects (ListDrawableObject2D) attached to this viewer
  */
    ListDrawableObject2D *getDrawableObject2DList(int i) const
    {
        if(!_listDrawableObjectsArray.empty())
            return _listDrawableObjectsArray[i];
        return NULL;
    }

    /** get the currently selected object
      \return the currently selected object, if any, else NULL
  */
    DrawableObject2D *getCurrentObject2D() const {return _drawableObjectsList ? _drawableObjectsList->getCurrentObject() : (DrawableObject2D*)NULL;}


    /** add an object (DrawableObject2D) to the list of 2D objects(ListDrawableObject2D) attached to this viewer
    \param obj the object to add
  */
    void addObject2D(DrawableObject2D *obj);

    /** add a list of 2D Markers to the list of 2D objects (ListDrawableObject2D) attached to this viewer
    \param list list to add
  */
    DrawableObject2D* addMarkers(std::list<Marker2D*> list);

    /**
     * add a new empty list of markers
     */
    DrawableObject2D* addMarkers();

    /** add a polygon to the list of 2D objects (ListDrawableObject2D) attached to this viewer
    \param list polygon to add
  */
    DrawableObject2D* addPolygon(std::list<Marker2D*> list);

    /**
     * add a polygon with empty marker list
     */
    DrawableObject2D* addPolygon();

    /** add a polyline to the list of 2D objects (ListDrawableObject2D) attached to this viewer
    \param list polyline to add
  */
    DrawableObject2D* addPolyLine(std::list<Marker2D*> list);

    /**
     * add a polyline with empty marker list
     */
    DrawableObject2D* addPolyLine();

    /** add a spline to the list of 2D objects (ListDrawableObject2D) attached to this viewer
    \param list spline to add
  */
    DrawableObject2D* addSpline(std::list<Marker2D*> list);

    /**
     * add a spline with empty marker list
     */
    DrawableObject2D* addSpline();

    /** add a blob to the list of 2D objects (ListDrawableObject2D) attached to this viewer
    \param list blob to add
  */
    DrawableObject2D* addBlob(std::list<Marker2D*> list);

    /**
     * add a blob with empty marker list
     */
    DrawableObject2D* addBlob();

    /**
     * Add an Arrow to the list of 2D objects (ListDrawableObject2D) attached to this viewer
     * @param list arrow list to add
     */
    DrawableObject2D* addArrows(std::list<Marker2D*> list);

    /**
     * add an arrow with empty marker list
     */
    DrawableObject2D* addArrows();

    /**
     * Add a Text to the list of 2D objects (ListDrawableObject2D) attached to this viewer
     * @param list list of markers to add
     */
    DrawableObject2D* addText(std::list<Marker2D*> list, QString& text);

    /**
     * add a Text with empty marker list
     */
    DrawableObject2D* addText(QString text);

    /** add object given a list of 2D markers on the image with a specific index \n
      do nothing if the index is out of range
    \param obj the object to add
    \param idxima index of the image on which the object must be added
  */
    void addObject2D(DrawableObject2D *obj, int idxima);

    /** add markers given a list of 2D markers on the image with a specific index \n
      do nothing if the index is out of range
    \param list markers to add
    \param idxima index of the image on which the markers must be added
  */
    DrawableObject2D* addMarkers(std::list<Marker2D *> list, int idxima);

    /** add polygon given a list of 2D markers on the image with a specific index \n
      do nothing if the index is out of range
    \param list polygon to add
    \param idxima index of the image on which the polygon must be added
  */
    DrawableObject2D* addPolygon(std::list<Marker2D *> list, int idxima);

    /** add polyline given a list of 2D markers on the image with a specific index \n
      do nothing if the index is out of range
    \param list polyline to add
    \param idxima index of the image on which the polyline must be added
  */
    DrawableObject2D* addPolyLine(std::list<Marker2D *> list, int idxima);

    /** add spline given a list of 2D markers on the image with a specific index \n
      do nothing if the index is out of range
    \param list spline to add
    \param idxima index of the image on which the spline must be added
  */
    DrawableObject2D* addSpline(std::list<Marker2D *> list, int idxima);

    /** add blob given a list of 2D markers on the image with a specific index \n
      do nothing if the index is out of range
    \param list blob to add
    \param idxima index of the image on which the blob must be added
  */
    DrawableObject2D* addBlob(std::list<Marker2D *> list, int idxima);


    /**
     * save the Object2D currently drawn on the image index passed in parameter into a SVG file
     * @param fname file name
     * @param imgId index of the image. If -1, the function saves the currently displayed image
     */
    void saveObject2DAsSvg(QString& fname, int imgId=-1) const;


    /**
     * @brief Copy the 2D objects from an image to another one (of the same sequence)
     * @param srcImg index of the source image in the sequence
     * @param destImg index of the destination image from the sequence
     * @param deepCopy boolean to indicate if we want a deep copy (thus the copied object is a different instance) or a shallow copy (thus the copied and the original objects remain the same)
     * @param clearDestList if clearDestList is set to true the destination list will be cleared of all its current objects. If false, the copied objects will simply be added to the dest list
     * @note does not work if all images share the same object list
     */
    void copyObjects2D(int srcImg, int destImg, bool deepCopy=true, bool clearDestList=false);

    /**
     * @brief Copy the 2D objects from an image to all the other ones (of the same sequence)
     * @param srcImg index of the source image in the sequence
     * @param deepCopy boolean to indicate if we want a deep copy (thus the copied object is a different instance) or a shallow copy (thus the copied and the original objects remain the same)
     * @param clearDestList if clearDestList is set to true the destination list will be cleared of all its current objects. If false, the copied objects will simply be added to the dest list
     * @note does not work if all images share the same object list
     */
    void copyObjects2D(int srcImg, bool deepCopy=true, bool clearDestList=false);


    /**
     * @brief Copy the currently selected object from the current image to the image at index given in parameter
     * @param destImg the index of the image receiving the copied object
     * @param deepCopy boolean to indicate if we want a deep copy (thus the copied object is a different instance) or a shallow copy (thus the copied and the original objects remain the same)
     * @note does not work if all images share the same object list
     */
    void copyCurrentObject2D(int destImg, bool deepCopy=true);

    /**
     * @brief Copy the currently selected object from the current image to all the other ones (of the same sequence)
     * @param deepCopy boolean to indicate if we want a deep copy (thus the copied object is a different instance) or a shallow copy (thus the copied and the original objects remain the same)
     * @note does not work if all images share the same object list
     */
    void copyCurrentObject2D(bool deepCopy=true);


    /** read a file in grx format
    @param fname name of the file to read
    @note grx format is deprecated. Please use the Object2D format (see DrawableObject2D)
  */
    void readGrx(char *fname);

    /** display/undisplay current object (true or false)
  */
    void setCurrentObject2DDisplay(bool b);

    /** set current object in edition mode or not (true or false)
  */
    void setCurrentObject2DEdition(bool b);

    /** return a list of DrawableObject2D* selected by name and searched in the image with index idx
      \param name the name of the DrawableObject2D to get
      \return the list computed, if no DrawableObject2D are found the list returned is empty
  */
    std::list<DrawableObject2D*> getObjects2DByName(const char* name) const
    {
        if (_drawableObjectsList) return _drawableObjectsList->getObjectsByName(name);
        return std::list<DrawableObject2D*>();
    }

    /** return a list of DrawableObject2D* selected by type (the search is made in all images)
      \param type the type of the DrawableObject2D to select
      \return the list computed, if no DrawableObject2D are found the list returned is empty
      \note the type must be chosen among these possibilities : \n
      'M' for a DrawableMarkers2D \n
      'A' for a DrawableArrows2D \n
      'B' for a DrawableBlob2D \n
      'P' for a DrawablePolygon2D \n
      'L' for a DrawablePolyLine2D \n
      'S' for a DrawableSpline2D \n
      'X' for a DrawableText2D \n
  */
    std::list<DrawableObject2D*> getObjects2DByType(char type) const
    {
        if (_drawableObjectsList) return _drawableObjectsList->getObjectsByType(type);
        return std::list<DrawableObject2D*>();
    }

    /** return the list of all the objects in the current image */
    std::list<DrawableObject2D*> getObjects2D() const
    {
        if (_drawableObjectsList) return _drawableObjectsList->getObjects();
        return std::list<DrawableObject2D*>();
    }

    /** return a DrawableObject2D* thanks to his index in the \ref _drawableObjectsList list
    \param index of the DrawableObject2D to get
    \return the found DrawableObject2D, if the index is not pertinent return a NULL DrawableObject2D
  */
    DrawableObject2D* getObject2DByIndex(int index) const
    {
        if (_drawableObjectsList) return _drawableObjectsList->getObjectByIndex(index);
        return (DrawableObject2D*)NULL;
    }


    /** return the DrawableObject2D* with index ObjIdx on image with index ImaIdx
      \param ObjIdx index of the object
      \param idx index of the image
      \return the DrawableObject2D* if any, else NULL */
    DrawableObject2D* getObject2DByIndex(unsigned int ObjIdx, int idx) const
    {
        if(!_listDrawableObjectsArray.empty() && idx>=0 && idx < _nbima)
        {
            return _listDrawableObjectsArray[idx]->getObjectByIndex(ObjIdx);
        }
        return (DrawableObject2D*)NULL;
    }

    /** return a list of DrawableObject2D* selected by name and searched in the image with index idx
    \param name the name of the DrawableObject2D to get
    \param idx index of the image
    \return the list computed, if no DrawableObject2D are found the list returned is empty
  */
    std::list<DrawableObject2D*> getObjects2DByName(char* name, int idx) const
    {
        if(!_listDrawableObjectsArray.empty() && idx>=0 && idx < _nbima)
        {
            return _listDrawableObjectsArray[idx]->getObjectsByName(name);
        }
        return std::list<DrawableObject2D*>();
    }


    /** return a list of DrawableObject2D* selected by type and searched in the image with index idx
    \param type the type of the DrawableObject2D to get
    \param idx index of the image
    \return the list computed, if no DrawableObject2D are found the list returned is empty
  */
    std::list<DrawableObject2D*> getObjects2DByType(char type, int idx) const
    {
        if(!_listDrawableObjectsArray.empty() && idx>=0 && idx < _nbima)
        {
            return _listDrawableObjectsArray[idx]->getObjectsByType(type);
        }
        return std::list<DrawableObject2D*>();
    }


    /** return the list of DrawableObject2D* on image with index idx
      \param idx index of the image
      \return the whole list of 2D objects in the image, an empty list if not found
    */
    std::list<DrawableObject2D*> getObjects2D(int idx) const
    {
        if(!_listDrawableObjectsArray.empty() && idx>=0 && idx < _nbima)
        {
            return _listDrawableObjectsArray[idx]->getObjects();
        }
        return std::list<DrawableObject2D*>();
    }


    /** helpful inline to create new objects from scratch: equivalent to addMarkers with an empty list of markers
  */
    DrawableObject2D * newMarkers()
    {
        DrawableObject2D * obj = addMarkers();
        startEditMarkersSlot();
        return obj;
    }

    /** helpful inline to create new objects from scratch: equivalent to addPolygon with an empty list of markers
   */
    DrawableObject2D * newPolygon()
    {
        DrawableObject2D * obj = addPolygon();
        startEditMarkersSlot();
        return obj;
    }

    /** helpful inline to create new objects from scratch: equivalent to addPolyLine with an empty list of markers
   */
    DrawableObject2D * newPolyLine()
    {
        DrawableObject2D * obj = addPolyLine();
        startEditMarkersSlot();
        return obj;
    }

    /** helpful inline to create new objects from scratch: equivalent to addSpline with an empty list of markers
   */
    DrawableObject2D * newSpline()
    {
        DrawableObject2D * obj = addSpline();
        startEditMarkersSlot();
        return obj;
    }

    /** helpful inline to create new objects from scratch: equivalent to addBlob with an empty list of markers
   */
    DrawableObject2D * newBlob()
    {
        DrawableObject2D * obj = addBlob();
        startEditMarkersSlot();
        return obj;
    }


    /** helpful inline to create new objects from scratch: equivalent to addArrow with an empty list of markers
   */
    DrawableObject2D * newArrows()
    {
        DrawableObject2D * obj = addArrows();
        startEditMarkersSlot();
        return obj;
    }

    /** helpful inline to create new objects from scratch: equivalent to addText with an empty list of markers
   */
    DrawableObject2D * newText(QString text)
    {
        DrawableObject2D * obj = addText(text);
        startEditMarkersSlot();
        return obj;
    }

    DrawableObject2D * newText()
    {
        QString text("");
        DrawableObject2D * obj = addText(text);
        startEditMarkersSlot();
        return obj;
    }

    /**
     * removes the currently selected 2D object (if exists)
     */
    void removeCurrentObject2D();

    /**
     * removes all the 2D objects
     */
    void removeAllObjects2D();


    /** @} */

public slots:

    /** \name Public Slots */
    /** @{ */

    /** start editing the markers
  */
    virtual void startEditMarkersSlot();

    /** stop editing the markers
  */
    virtual void stopEditMarkersSlot();

    /** change the label of a marker
    \param l the new label
  */
    void changeMarkerLabelSlot(float l);

    void changeMarkerLabelSlot(int l);

    /** select a marker \n
    \note emits selectMarkerSignal(Marker2D*)
  */
    void selectMarkerSlot(Marker2D*);

    /** unselect a marker \n
    \note emits unselectMarkerSignal()
  */
    void unselectMarkerSlot();


    /** save the markers related to all images

  a name for the _saveFile must be set before ( thanks to the method void Viewer2D::setSaveFilename(char *fname) )
  check if _saveFile corresponds to a format
  in such case, markers are saved for all images, implementing the format with the image number
  (e.g. if _saveFile is points.%06d.p2d, and imgId is 3, then the file actually used for saving
  is points.000003.p2d)
  else we consider that the markers should be saved for the current image alone

  \note the loading/saving format is described in function \ref ListDrawableObject2D::load
  */
    void saveMarkersSlot() const;

    /** save the markers related to a specific image

      a name for the _saveFile must be set before ( thanks to the method Viewer2D::void setSaveFilename(char *fname) )
      In case the _saveFile is a format, it is implemented by the provided image index
      (e.g. if _saveFile is points.%06d.p2d, and imgId is 3, then the file actually used for saving
      is points.000003.p2d)
      \param imgId index of the image

      \note the loading/saving format is described in function \ref ListDrawableObject2D::load
  */
    void saveMarkersSlot(int imgId) const;


    /**
     * @brief helper method to save Object2D
     * @param fname name of the file used to save the objects
     */
    virtual void saveObject2D(const char* fname)
    {
        setSaveFilename(fname);
        saveMarkersSlot();
    }

    /**
     * @brief helper to save Object2D to a file (name in QString format)
     */
    void saveObject2D(const QString& fname)
    {
        saveObject2D(fname.toStdString().c_str());
    }

    /**
     * load the markers from a file
     * A name for the _loadFile must be set before ( thanks to the method void Viewer2D::setLoadFilename(char *fname) )
     * \note the loading/saving format is described in function \ref ListDrawableObject2D::load
     */
    virtual void loadMarkersSlot();

    /** load the markers related to a specific image
      a name for the _loadFile must be set before ( thanks to the method Viewer2D::void setLoadFilename(char *fname) )
      In case the _loadFile is a format, it is implemented by the provided image index
      (e.g. if _loadFile is points.%06d.p2d, and imgId is 3, then the file actually read
      is points.000003.p2d)
      \param imgId index of the image

      \note the loading/saving format is described in function \ref ListDrawableObject2D::load
    */
    virtual void loadMarkersSlot(int imgId);

    /**
     * @brief helper method to load Object2D
     * @param fname name of the file used to laod the objects
     */
    virtual void loadObject2D(const char *fname)
    {
        setLoadFilename(fname);
        loadMarkersSlot();
    }

    /**
     * @brief helper to load Object2D from a file (name in QString format)
     */
    void loadObject2D(const QString& fname)
    {
        loadObject2D(fname.toStdString().c_str());
    }

    /**
     * clear all the Object2D of the current list
     */
    virtual void clearObject2D()
    {
        if (_drawableObjectsList) _drawableObjectsList->clear();
    }

    /**
     * clear the markers of all lists
     */
    virtual void clearAllObject2D()
    {
        if (!_listDrawableObjectsArray.empty())
        {
            if(_shareObjects)
                _listDrawableObjectsArray[0]->clear();
            else
                for (int i=0; i<_nbima; i++) _listDrawableObjectsArray[i]->clear();
        }
    }

    /**
     * save current viewer content into a SVG file. All displayed DrawableObject2D will be saved in vectorial format
     * @param fname name of the file
     */
    virtual void saveAsSvg(QString& fname);

    /** manage the image sequence, go to a specific image in the sequence
    \param n number of the image in the image sequence to go to
        \note emits imageIndexSignal(int)
    */
    virtual void gotoImageSlot(int n);

    /** manage the image sequence, go to the next image in the sequence.
      \param wrap if true, goto the first image after the last image has been reached. Else, does not go beyond the last image.
      \note emits imageIndexSignal(int)
    */
    virtual void nextImageSlot(bool wrap=true);

    /** manage the image sequence, go to the previous image in the sequence.
      \param wrap if true, goto the last image after the firts image has been reached. Else, does not go before the first image.
      \note emits imageIndexSignal(int)
    */
    virtual void previousImageSlot(bool wrap=true);

    /** @} */


signals:

    /** \name Signals */
    /** @{ */

    /** signal used to manage the markers selection
   */
    void selectMarkerSignal(Marker2D*);

    /** signal used to manage the markers unselection
   */
    void unselectMarkerSignal();

    /**
     * signal used to interact with the drawable object 2D (marker creation, deletion, displacement following the given int)
     */
    void markerManipulationSignal(double, double, int);

    /**
     * signal used to move the selected marker
     */
    void markerMoveSignal(double, double);

    /**
     * signal used to release the selected marker (stop its manipulation)
     */
    void markerReleaseSignal();

    /** signal used to manage 2D objects selection
   */
    void selectObjectSignal(DrawableObject2D*);

    /** signal used to manage 2D objects removal
   */
    void removeObjectSignal(QObject *);

    /** signal used to manage 2D objects modification
   */
    void modifyObjectSignal(DrawableObject2D*);

    /** signal emitted when a new object is added
     */
    void addObjectSignal(DrawableObject2D*);

    /** @} */


private:

    /**
     * @brief initialization of the lists of 2D objects
     * @param w the width of the background image
     * @param h the height of the background image
     * @param nima the number of images in the sequence
     * @param shareObjects boolean to specify if the images must share the same list of 2D objects
     */
    void initShareObjects(bool shareObjects);


protected:

    /** destructor
   */
    virtual ~Viewer2D();

    /**
    * @brief select the 2D object at (x,y) coordinates
    */
    void selectObject2D(double x, double y);

    /**
    * @brief delete the 2D object at (x,y) coordinates
    */
    void removeObject2D(double x, double y);


    /**
    * @brief add a 2D control point at the (x,y) coordinates
    */
    void addMarker2D(double x, double y)
    {
        double xx, yy;
        QtToImage(x, y, xx, yy);
        emit markerManipulationSignal(xx, yy, DrawableObject2D::ADD);
    }

    /**
    * @brief delete (if existing) the 2D control point at the (x,y) coordinates
    */
    void deleteMarker2D(double x, double y)
    {
        double xx, yy;
        QtToImage(x, y, xx, yy);
        emit markerManipulationSignal(xx, yy, DrawableObject2D::REMOVE);
    }

    /**
    * @brief select (if existing) the 2D control point at the (x,y) coordinates
    */
    void selectMarker2D(double x, double y)
    {
        double xx, yy;
        QtToImage(x, y, xx, yy);
        emit markerManipulationSignal(xx, yy, DrawableObject2D::SELECT);
    }

    /**
    * @brief move the currently selected 2D control point to the (x,y) coordinates
    */
    void moveMarker2D(double x, double y)
    {
        double xx, yy;
        QtToImage(x, y, xx, yy);
        emit markerMoveSignal(xx, yy);
    }

    /**
    * @brief release the currently selected 2D control point of 2D object
    */
    void releaseMarker2D(double, double)
    {
        emit markerReleaseSignal();
    }

    /** pointer to the list of 2D objects (ListDrawableObject2D) currently drawn
     */
    ListDrawableObject2D *_drawableObjectsList;

    /** the vector of the lists of DrawableObject2D
     * one list for each image in the background image sequence
     */
    std::vector<ListDrawableObject2D*> _listDrawableObjectsArray;

    /** set to true if all the images of the sequence share the same ListDrawableObject2D
     */
    bool _shareObjects;

    /** name of the file to save the markers
     */
    char *_saveFile;

    /** name of the file to load the markers
     */
    char *_loadFile;
};

}

#endif // _POLAR_VIEWER2D_H_
