#ifndef BULLETUTIL_H
#define BULLETUTIL_H

#include "export.h"
#include <btBulletDynamicsCommon.h>
#include <BulletSoftBody/btSoftRigidDynamicsWorld.h>
#include <BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.h>
#include <osg/ref_ptr>
#include <osg/Referenced>
#include <osg/Matrixd>
#include <osg/Vec3>

namespace PoLAR
{

/**
 * The Bullet namespace contains useful methods to create Bullet scenes
 */
namespace Bullet
{

/**
 * @brief Singleton class to store the btSoftBodyWorldInfo and btSoftRigidDynamicsWorld objects needed for a Bullet simulation.
 * Thanks to a static smart pointer, the user does not have to delete it himslef.
 */

class PoLAR_EXPORT DynamicsWorld: public osg::Referenced
{
public:

    static DynamicsWorld *getInstance();

    static btSoftBodyWorldInfo* getWorldInfo();

    static btSoftRigidDynamicsWorld* getDynamicsWorld();

private:

    DynamicsWorld();

    ~DynamicsWorld();

    /**
      * @brief sets up the btSoftRigidDynamicsWorld object (used by Bullet to represent the physics world) with real life values
      */
    static void initBulletWorld();

    static void allocate();

    static osg::ref_ptr<DynamicsWorld> _ptr;

    static btSoftRigidDynamicsWorld* _dynworld;
    static btConstraintSolver* _solver;
    static btCollisionDispatcher* _dispatcher;
    static btSoftBodyRigidBodyCollisionConfiguration* _collision;
    static btBroadphaseInterface* _broadphase;
    static btSoftBodyWorldInfo* _worldinfo;
};



/**
  * @brief convert an OSG Vec3 to a Bullet btVector3
  */
inline PoLAR_EXPORT btVector3 asBtVector3(const osg::Vec3& v)
{
    return btVector3(v.x(), v.y(), v.z());
}

/**
 * @brief convert a btTransform from Bullet into an OpenSceneGraph Matrixd
 * @param t the Bullet matrix to convert
 * @return the matrix in osg::Matrixd format
 */
PoLAR_EXPORT osg::Matrixd asOsgMatrix(const btTransform& t);

/**
 * @brief convert an OpenSceneGraph Matrixd into a Bullet btTransform matrix
 * @param m the osg::Matrixd to convert
 * @return the matrix in Bullet btTransform format
 */
PoLAR_EXPORT btTransform asBulletMatrix(const osg::Matrixd &m);



}

}


#endif // BULLETUTIL_H
