/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Image.h"
#include "Util.h"
#include <iostream>

namespace PoLAR
{

template<typename T>
Image<T>::Image():
    BaseImage(0,0,1),
    _imageSequence((T*)NULL),
    _currentData((T*)NULL),
    _numImages(0),
    _currentImageIndex(0),
    _data((unsigned char *)NULL),
    _manageSequence(true)
{
}

template<typename T>
Image<T>::Image(T* sequence, int width, int height, int depth, int nbImages, bool manageSequence):
    BaseImage(width, height, depth),
    _imageSequence(sequence),
    _currentData(sequence),
    _numImages(nbImages),
    _currentImageIndex(0),
    _data((unsigned char *)NULL),
    _manageSequence(manageSequence)
{
    if (_imageSequence)
    {
        updateMinMax();
        update();
    }
}


template<typename T>
Image<T>::Image(const std::string &fileName, bool flipVertical, int nbImages):
    BaseImage(fileName, flipVertical, nbImages),
    _numImages(nbImages),
    _currentImageIndex(0),
    _data((unsigned char *)NULL),
    _manageSequence(true)
{
    _imageSequence = fromUChar(this->data(), _imageDimension*nbImages);

    if (_imageSequence)
    {
        _currentData = _imageSequence;
        updateMinMax();
    }
}


/** Copy constructor using CopyOp to manage deep vs shallow copy.*/
template<typename T>
Image<T>::Image(const Image &im, const osg::CopyOp& copyop):
    BaseImage(im,copyop),
    _imageSequence(getDataPtr()),
    _currentData(getDataPtr()),
    _numImages(im.getNbImages()),
    _currentImageIndex(im._currentImageIndex),
    _manageSequence(im._manageSequence)
{
    if (_imageSequence)
    {
        updateMinMax();
    }
}

template<typename T>
Image<T>::~Image()
{
    if(_manageSequence && _imageSequence)
        delete[] _imageSequence;
    _imageSequence = NULL;

    if(_data)
        delete[] _data;
}


template<typename T>
void Image<T>::nextImageSlot(bool wrap)
{
    if (valid() && _numImages > 1)
    {
        _currentImageIndex++;
        if (_currentImageIndex == _numImages)
        {
            if (wrap)
            {
                _currentImageIndex = 0;
                _currentData = _imageSequence;
            }
        }
        else _currentData += _imageDimension;
        update();
        emit indexSignal(_currentImageIndex);
    }
}


template<typename T>
void Image<T>::previousImageSlot(bool wrap)
{
    if (valid() && _numImages > 1)
    {
        if(_currentImageIndex == 0)
        {
            if (wrap)
            {
                _currentImageIndex = _numImages-1;
                _currentData = _imageSequence+(_numImages-1)*_imageDimension;
            }
        }
        else
        {
            _currentImageIndex--;
            _currentData -= _imageDimension;
        }
        update();
        emit indexSignal(_currentImageIndex);
    }
}


template<typename T>
void Image<T>::gotoImageSlot(unsigned int i)
{
    if (valid() && _numImages > 1)
    {
        if (i == _currentImageIndex){ return; }
        if (i>=_numImages) i = _numImages-1;
        _currentImageIndex = i;
        _currentData = _imageSequence + _currentImageIndex*_imageDimension;
        update();
        emit indexSignal(_currentImageIndex);
    }
}

template<typename T>
std::vector<T> Image<T>::getPixelData(double x, double y) const
{
    int X(floor(x));
    int Y(floor(y));
    std::vector<T> data;
    if (valid() && Y>=0 && Y<_height && X>=0 && X<_width)
    {
        T *ptr = _currentData + (Y*_width*_depth+X*_depth);
        for(int d=0; d<_depth; d++)
            data.push_back(*(ptr++));
    }
    return data;
}

template<typename T>
unsigned char * Image<T>::getMonoImage(unsigned char *out) const
{
    if (valid() && _data)
    {
        int nb = _width*_height;
        //if (_depth != 1 && _depth != 3) return (unsigned char *)NULL;
        if (!out) out = new unsigned char[nb];
        if (!out) return (unsigned char *)NULL;
        switch (_depth)
        {
        case 1:
            memcpy(out, _data, nb*sizeof(unsigned char));
            break;
        case 3:
        {
            unsigned char *tout = out;
            unsigned char *tdata = _data;
            unsigned char r,g,b;
            int i;
            for (i=0; i<nb; i++)
            {
                r = *tdata++, g=*tdata++, b=*tdata++;
                *tout++ = qGray(r, g, b);
            }
            break;
        }
        case 4:
        {
            unsigned char *tout = out;
            unsigned char *tdata = _data;
            unsigned char r,g,b;
            int i;
            for (i=0; i<nb; i++)
            {
                r = *tdata++, g=*tdata++, b=*tdata++;
                tdata++;
                *tout++ = qGray(r, g, b);
            }
            break;
        }
        default: // should not happen
        {
            qCritical("getMonoImage: This image depth (%d) is not managed", _depth);
            return (unsigned char *)NULL;
        }
        }
    }
    return out;
}


template<typename T>
void Image<T>::updateMinMax()
{
    unsigned int dim = _imageDimension*_numImages;
    _minImage=_maxImage=_imageSequence[0];
    _imageSequence += dim;
    while (--_imageSequence, dim--)
        if (*_imageSequence < _minImage) _minImage = *_imageSequence;
        else if (*_imageSequence > _maxImage) _maxImage = *_imageSequence;
    _imageSequence++;

    if (Util::Null(_maxImage-_minImage))
    {
        _scaleIntensity = 0;
        _offsetIntensity = 128;
    }
    else
    {
        _scaleIntensity = 255.0/(_maxImage-_minImage);
        _offsetIntensity = - (_minImage * _scaleIntensity);
    }

}

template<typename T>
T* Image<T>::fromUChar(const unsigned char *uc_array, const int dim)
{
    T* array;
    const unsigned char *ta;

    /* params validity check */
    if (!uc_array || dim <= 0) return (T*)NULL;
    if (!(array = new T[dim])) return (T*)NULL;

    array += dim, ta = uc_array + dim;
    while (ta-- > uc_array) *--array = (T)*ta;

    return array;
}


template<typename T>
void Image<T>::update()
{
    if (_currentData)
    {
        unsigned int dim = _imageDimension;
        if (!_data)
            _data = new unsigned char [dim];

        _data += dim, _currentData += dim;
        while (dim--)
            *--_data = getDisplayedPixelValue(*--_currentData);

        if (!valid())
            allocateImage();

        refresh();
    }
    else deleteImage();

    emit newImageSignal();
}


template<typename T>
void Image<T>::refresh()
{
    if (valid())
    {
        int i;
        int j;

        int tw = _width * _depth;
        unsigned char *tn = this->data();
        unsigned char *ti = _data;
        for (i=0; i<_height; i++)
        {
            for (j=0; j<tw; j++) *tn++ = *ti++;
        }
        this->dirty();

        emit updateSignal();
    }
}


template<typename T>
void Image<T>::displayNextFrame(unsigned char *Data)
{
    if(Data)
    {
        if (!valid()) allocateImage();
        _currentData = fromUChar(Data, sizeof(Data)/sizeof(unsigned char));
        update();
        this->dirty();
    }
    else deleteImage();

    emit newImageSignal();
}


template<typename T>
void Image<T>::displayNextFrame(unsigned char *Data, int width, int height, int depth)
{
    if(width != _width || height != _height || depth != _depth)
    {
        _width = width;
        _height = height;
        _depth = depth;
        setGLFormat(depth);
        allocateImage();

        emit resolutionChanged();
    }

    displayNextFrame(Data);
}


//////////////////////// Template specialization <unsigned char> /////////////////////////


Image<unsigned char>::Image():
    BaseImage(0,0,1),
    _imageSequence((unsigned char*)NULL),
    _currentData((unsigned char*)NULL),
    _numImages(0),
    _currentImageIndex(0),
    _manageSequence(false)
{
}


Image<unsigned char>::Image(unsigned char* sequence, int width, int height, int depth, int nbImages, bool manageSequence):
    BaseImage(width, height, depth),
    _imageSequence(sequence),
    _currentData(sequence),
    _numImages(nbImages),
    _currentImageIndex(0),
    _manageSequence(manageSequence)
{
    if (_imageSequence)
    {
        updateMinMax();
        displayNextFrame(sequence, width, height, depth);
    }
}


Image<unsigned char>::Image(VideoPlayer *camera):
    BaseImage(camera->getRealWidth(), camera->getRealHeight(), camera->getDepth()),
    _imageSequence(NULL),
    _currentData(NULL),
    _numImages(1),
    _currentImageIndex(0),
    _manageSequence(false)
{
    QObject::connect(camera, SIGNAL(newFrame(unsigned char*, int, int, int)), this, SLOT(displayNextFrame(unsigned char*, int, int, int)));
    allocateImage();
    unsigned char *data = this->data();
    memset(data, 0, _imageDimension);
    _imageSequence = this->data();

    if (_imageSequence)
    {
        _currentData = _imageSequence;
        updateMinMax();
    }
}


Image<unsigned char>::Image(const std::string& fileName, bool flipVertical, int nbImages):
    BaseImage(fileName, flipVertical, nbImages),
    _numImages(nbImages),
    _currentImageIndex(0),
    _manageSequence(false)
{
    _imageSequence = this->data();

    if (_imageSequence)
    {
        _currentData = _imageSequence;
        updateMinMax();
    }
}


/** Copy constructor using CopyOp to manage deep vs shallow copy.*/
Image<unsigned char>::Image(const Image &im, const osg::CopyOp& copyop):
    BaseImage(im,copyop),
    _imageSequence(getDataPtr()),
    _currentData(getDataPtr()),
    _numImages(im.getNbImages()),
    _currentImageIndex(im._currentImageIndex),
    _manageSequence(false)
{
    if (_imageSequence)
    {
        updateMinMax();
    }
}


Image<unsigned char>::~Image()
{
    if(_manageSequence && _imageSequence)
    {
        delete _imageSequence;
    }
}



void Image<unsigned char>::nextImageSlot(bool wrap)
{
  if (valid() && _numImages > 1)
    {
      if (_currentImageIndex == _numImages-1)
	{
	  if (wrap)
	    {
	      _currentImageIndex = 0;
	      _currentData = _imageSequence;
	    }
	  // else return; // good idea to avoid sending a signal/update?
	}
      else
	{
	  _currentImageIndex++;
	  _currentData += _imageDimension;
	}
      update();
      emit indexSignal(_currentImageIndex);
    }
}



void Image<unsigned char>::previousImageSlot(bool wrap)
{
    if (valid() && _numImages > 1)
    {
        if(_currentImageIndex == 0)
        {
            if (wrap)
            {
                _currentImageIndex = _numImages-1;
                _currentData = _imageSequence+(_numImages-1)*_imageDimension;
            }
        }
        else
        {
            _currentImageIndex--;
            _currentData -= _imageDimension;
        }
        update();
        emit indexSignal(_currentImageIndex);
    }
}


void Image<unsigned char>::gotoImageSlot(unsigned int i)
{
    if (valid() && _numImages > 1)
    {
        if (i == _currentImageIndex){ return; }
        if (i>=_numImages) i = _numImages-1;
        _currentImageIndex = i;
        _currentData = _imageSequence + _currentImageIndex*_imageDimension;
        update();
        emit indexSignal(_currentImageIndex);
    }
}


std::vector<unsigned char> Image<unsigned char>::getPixelData(double x, double y) const
{
    int X(floor(x));
    int Y(floor(y));
    std::vector<unsigned char> data;
    if (valid() && Y>=0 && Y<_height && X>=0 && X<_width)
    {
        const unsigned char *ptr = this->data() + (Y*_width*_depth+X*_depth);
        for(int d=0; d<_depth; d++)
            data.push_back(*(ptr++));
    }
    return data;
}

unsigned char * Image<unsigned char>::getMonoImage(unsigned char *out) const
{
    if (valid())
    {
        int nb = _width * _height;
        if (!out) out = new unsigned char[nb];
        if (!out) return (unsigned char *)NULL;
        switch (_depth)
        {
        case 1:
            memcpy(out, this->data(), nb*sizeof(unsigned char));
            break;
        case 3:
        {
            unsigned char *tout = out;
            unsigned char *tdata = (unsigned char *)this->data();
            unsigned char r,g,b;
            int i;
            for (i=0; i<nb; i++)
            {
                r = *tdata++, g = *tdata++, b = *tdata++;
                *tout++ = qGray(r, g, b);
            }
            break;
        }
        case 4: // we consider RGBA only
        {
            unsigned char *tout = out;
            unsigned char *tdata = (unsigned char *)this->data();
            unsigned char r,g,b;
            int i;
            for (i=0; i<nb; i++)
            {
                r = *tdata++, g = *tdata++, b = *tdata++;
                tdata++;
                *tout++ = qGray(r, g, b);
            }
            break;
        }
        default: // should never happen
        {
            qCritical("getMonoImage<uchar>: This image depth (%d) is not managed", _depth);
            return (unsigned char *)NULL;
        }
        }
    }
    return out;
}



void Image<unsigned char>::updateMinMax()
{
    // unsigned int dim = _imageDimension*_numImages;
    // _minImage=_maxImage=_imageSequence[0];
    // _imageSequence += dim;
    // while (--_imageSequence, dim--)
    //     if (*_imageSequence < _minImage) _minImage = *_imageSequence;
    //     else if (*_imageSequence > _maxImage) _maxImage = *_imageSequence;

    // _imageSequence++;

    // if (Util::Null(_maxImage-_minImage))
    // {
    //     _scaleIntensity = 0;
    //     _offsetIntensity = 128;
    // }
    // else
    // {
    //     _scaleIntensity = 255.0/(_maxImage-_minImage);
    //     _offsetIntensity = - (_minImage * _scaleIntensity);
    // }
  _scaleIntensity = 1.0;
  _offsetIntensity = 0.0;
}


void Image<unsigned char>::update()
{
    if (_currentData)
    {
        if (!valid())
            allocateImage();

        unsigned char *tn = this->data();
        for(int i=0; i<_height; i++)
            for(int j=0; j<_width*_depth; j++)
                *tn++ = getDisplayedPixelValue(*_currentData++);
        this->dirty();
    }
    else
        deleteImage();

    emit newImageSignal();
}


void Image<unsigned char>::displayNextFrame(unsigned char *data)
{
    if(data)
    {
        if (!valid()) allocateImage();
        unsigned char *ima = this->data();
        memcpy(ima, data, _imageDimension*sizeof(unsigned char));
        this->dirty();
    }
    else deleteImage();

    if(_imageSequence) updateMinMax();
    emit newImageSignal();
}



void Image<unsigned char>::displayNextFrame(unsigned char *data, int width, int height, int depth)
{
    if(width != _width || height != _height || depth != _depth)
    {
        _width = width;
        _height = height;
        _depth = depth;
        _imageDimension = _width*_height*_depth;
        setGLFormat(depth);
        allocateImage();
        _imageSequence = this->data();
        _currentData = _imageSequence;

        emit resolutionChanged();
    }
    else if(!isDepthSupported(depth))
      {
        qCritical("displayNextFrame: This image depth (%d) is not managed", _depth);
        return;
      }
    
    displayNextFrame(data);
}

}
