/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Object3DPickHandler.h
\brief Pick handler of Object3D

\author Frédéric Speisser, Pierre-Jean Petitprez
\date year 2008, 2015
\note 
Corresponding code: SceneGraph/Object3DPickHandler.cpp
*/
/** @} */

#ifndef _POLAR_OBJECT3DPICKHANDLER_H_
#define _POLAR_OBJECT3DPICKHANDLER_H_

#include "export.h"
#include "Object3D.h"
#include "EventHandlerInterface.h"
#include <osgManipulator/Dragger>
#include <osgManipulator/CommandManager>
#include <osgManipulator/TabBoxDragger>
#include <osgManipulator/TrackballDragger>
#include <osgManipulator/ScaleAxisDragger>
#include <osgManipulator/TranslateAxisDragger>

namespace PoLAR
{


class Viewer2D3D;

/** \brief Pick handler of Object3D

The pick handler provides : \n
- a display of informations concerning the object pointed by the mouse, available for all the \ref Object3D in the scene \n
- the retrieval of a pointer to the picked object thanks to the method \ref Viewer2D3D::getCurrentPickedObject(), only available for the pickable \ref Object3D \n
- the creation of a geometry related to the picked object, called dragger, which permits to edit it (depends on the pick handler chosen) in an interactive way, only available for the pickable AND editable \ref Object3D \n

The transformation(s) applied thanks to the dragger chosen for editable (and pickable) \ref Object3D are made in world frame by default. \n
If you want to edit the picked \ref Object3D under constraints (that is to say in a custom frame), you can set a custom frame, in which your edition will be made, using \ref Object3D::setFrameMatrix method. \n
To keep coherent with the dragger's concept all transformations are always made in relation to the \ref Object3D center, so if you want to rotate an object in relation to another center, set a well chosen frame matrix and use directly the methods available in class \ref Object3D . \n
Only \ref TrackBallPickHandler, \ref TranslateAxisPickHandler and \ref ScaleAxisPickHandler are pertinent for constraint edition since they are related to the frame axis.


\note to pick an object left mouse button must be pressed

\note to be able to use the Object3D pick handler the key '1' must be pressed in scene graph edition mode

*/


class PoLAR_EXPORT Object3DPickHandler : public osgGA::GUIEventHandler, public EventHandlerInterface
{

public:

    /** the different types of interaction \n
      type VIEW when picking is inactive \n
      type PICK when picking is active \n
    \note picking is active when key '1' is pressed (in scene edition mode)
  */
    enum Mode {VIEW = 0, PICK = 1};


    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param displayInfo true to display informations about the objets pointed by the mouse, false otherwise
  */
    Object3DPickHandler(bool displayInfo);

    /** handle events */
    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, osg::Object*, osg::NodeVisitor*)
    {
        return handle(ea, aa);
    }

    /** handle events */
    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

    /** clean the display and the structure of the scenegraph from all elements added by this pick handler */
    void cleanUp(Viewer2D3D *viewer);

    void cleanUp() { EventHandlerInterface::cleanUp(); }

    /** set the interaction mode of this pick handler */
    inline void setInteractMode(Mode mode) {_interactMode = mode;}

    /** get the interaction mode of this pick handler */
    inline Mode getInteractMode() {return _interactMode;}


    /** set the key shortcut to go in object edition mode (default: '1')
     */
    void setObjectEditionModeKey(osgGA::GUIEventAdapter::KeySymbol key)
    {
        _objectEditionModeKey = key;
    }

    /** set the key shortcut to go in camera manipulation mode (default: '0')
     */
    void setCameraManipulationModeKey(osgGA::GUIEventAdapter::KeySymbol key)
    {
        _cameraManipulationModeKey = key;
    }

    /** @} */


protected:

    /** create the chosen dragger */
    virtual osgManipulator::Dragger* createDragger()=0;

    /** create the chosen dragger related to a PoLAR::Object3D and insert it in the scene graph
    \param object3D the PoLAR::Object3D to edit
  */
    void manipulateObject(PoLAR::Object3D* object3D);

    /** remove the current dragger from the scene graph
  */
    void stopManipulation();

    /** key shortcut to go in object edition mode (default: '1')
     */
    int _objectEditionModeKey;

    /** key shortcut to go in camera manipulation mode (default: '0')
     */
    int _cameraManipulationModeKey;

    /**
     * structure for linking an object with its dragger without the need to use a Group an add them as children.
     * It simplifies the node path to root
     */
    struct ManipulationGroup
    {
        ManipulationGroup() { setNull(); }
        ManipulationGroup(osgManipulator::Dragger *d, Object3D *o):
            activeDragger(d), object(o) {}
        void setNull() { activeDragger = NULL, object = NULL; }
        bool isNull() { return (activeDragger == NULL && object == NULL); }
        osgManipulator::Dragger *activeDragger;
        Object3D *object;
    };

private:

    /** add a dragger to a node
    \param object3D the PoLAR::Object3D which has to be related to a dragger
  */
    osg::ref_ptr<osg::Group> addDraggerToObject(PoLAR::Object3D* object3D, ManipulationGroup &manipulationGroup);

    /** pointer to the dragger currently used */
    osgManipulator::Dragger* _activeDragger;

    /** pointer to the structure used for storage of the intersections for one pick event */
    osgManipulator::PointerInfo _pointer;

    /** pointer to the dragger currently used */
    osg::ref_ptr<osgManipulator::CommandManager> _cmdMgr;

    /** pointer to the node containing the dragger and the edited node */
    //osg::ref_ptr<osg::Group> _manipulationGroup;
    ManipulationGroup _manipulationGroup;

    /** used for storage of the mode (VIEW or PICK) */
    Mode _interactMode;
};

}

#include "Viewer2D3D.h"
#endif // _POLAR_OBJECT3DPICKHANDLER_H_
