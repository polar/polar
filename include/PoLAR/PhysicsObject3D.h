#ifndef PHYSICSOBJECT3D_H
#define PHYSICSOBJECT3D_H

#include <osg/NodeVisitor>
#include <osg/Node>
#include <osg/Geode>
#include <list>
#include <vector>
#include "Object3D.h"

namespace PoLAR
{

/**
 * @brief Extended Object3D with interface with physics capabilities
 */
class PoLAR_EXPORT PhysicsObject3D: public Object3D
{
    Q_OBJECT

public:
    /**
     * @brief Constructor
     * @param node the OSG node to encapsulate in this object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     */
    PhysicsObject3D(osg::Node *node, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief Constructor
     * @param filename source file of the model to load
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     */
    PhysicsObject3D(const std::string& filename, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief reset all the simulation, that means the object will be replaced to its original place / original form
     */
    virtual void resetSimulation() = 0;


protected:
    /**
      Destructor
      */
    ~PhysicsObject3D();

    /**
     * @brief Copy the original vertices in the _originalVertices array
     */
    void saveOriginalVertices();


    /**
     * the array of the original vertex positions
     */
    float *_originalVertices;

};

}

#endif
