/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Util.h
\brief Some functions to handle vectors and matrices

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2014
*/
/** @} */

#ifndef POLAR_UTIL_H
#define POLAR_UTIL_H

#include "export.h"
#include <osg/Uniform>
#include <osg/Matrixd>
#include <osg/Vec3>
#include <osg/Vec4>
#include <QMouseEvent>

/** define little value ... */
#define EPSILON 1e-6


/** \namespace PoLAR

The PoLAR library provides several classes to build graphical user interfaces in augmented reality and medical imaging \n
This library is based on three major libraries : Qt, OpenGL and OpenSceneGraph

*/
namespace PoLAR
{

/**
 * @brief simple structure to represent mouse button and modifier (ctrl/shift/alt)
 */
struct PoLAR_EXPORT MouseShortcut
{
    Qt::MouseButton button;
    Qt::KeyboardModifiers modifier;
};


/**
 * The Util namespace is used for accessing to functions used in the PoLAR library. Those functions are not meant to be used outside the PoLAR scope.
 * Those functions are mainly mathematical functions for vectors and matrices computations.
 * Note: PoLAR uses OpenSceneGraph matrices. The OSG matrices are treated as row-major matrices, and matrix operations use prefix notation.
 * That's why the matrices are transposed.
 */
namespace Util
{

/** Load a 3D node from a file */
PoLAR_EXPORT osg::Node* loadFromFile(const std::string& filename);


/** check if a file exists or not */
PoLAR_EXPORT  bool fileExists(const std::string& filename);


/** avoid problems of nullity with floats */
PoLAR_EXPORT inline bool Null(double val)
{
    return (fabs(val) < EPSILON);
}

/**
 * @brief square of a float value
 * @param x the value
 * @return the square value of the value given
 */
PoLAR_EXPORT inline float sqr(float x)
{
    return x*x;
}


/** compute the distance between two 3D points
  \param point1 first point
  \param point2 second point
  \return computed distance
*/
PoLAR_EXPORT double computeDist(osg::Vec3f point1, osg::Vec3f point2);


/** vector initialization: a=(x,y)

  the affectation is made using casual C cast rules.

  \param a a vector as double*
  \param x a scalar (X-coordinate)
  \param y a scalar (Y-coordinate)
  */
PoLAR_EXPORT void initvec2(double *a, double x, double y);

/** vector copy: a=b

  initvec2() is called using both coordinates

  \param a the destination vector
  \param b the source vector
  */
template<typename T1, typename T2> void copyvec2(T1 *a, T2 *b)
{
    initvec2(a, b[0], b[1]);
}

/** distance between two vectors

  \param a the first vector
  \param b the second vector
  \return the square distance between a and b
*/
PoLAR_EXPORT double sqrdist2(double *a, double *b);

/**
 * @brief Determinant of 3 vectors stored in the same matrix
 * @param M the matrix containing the 3 vectors
 * @param ind1 the column index of the first vector in the matrix
 * @param ind2 the column index of the second vector in the matrix
 * @param ind3 the column index of the third vector in the matrix
 * @return the determinant of the 3 vectors
 */
PoLAR_EXPORT double determinant3(osg::Matrixd& M, int ind1, int ind2, int ind3);

/**
 * @brief Scalar product between 2 vectors stored in the same matrix
 * @param M the matrix containing the vectors
 * @param ind1 the column index of the first vector
 * @param ind2 the column index of the second vector
 * @return the scalar product between the 2 vectors
 */
PoLAR_EXPORT double pscal3(osg::Matrixd& M, int ind1, int ind2);

/**
 * @brief Scalar product between 2 vectors stored in 2 different matrices
 * @param M1 the first matrix containing the first vector
 * @param M2 the second matrix containing the second vector
 * @param ind1 the colum index of the first vector in the first matrix
 * @param ind2 the colum index of the second vector in the second matrix
 * @return the scalar product between the 2 vectors
 */
PoLAR_EXPORT double pscal3(osg::Matrixd& M1, osg::Matrixd& M2, int ind1, int ind2);

/**
 * @brief Vectorial product between 2 vectors stored in the same matrix
 * @param M the matrix containing the vectors
 * @param ind1 the colum index of tge vector which will contain the result
 * @param ind2 the colum index of the first vector in the matrix
 * @param ind3 the colum index of the second vector in the matrix
 */
PoLAR_EXPORT void vecprod3(osg::Matrixd& M, int ind1, int ind2, int ind3);

/**
 * @brief Calculate a normal vector from 2 vectors (thus forming a triangle)
 * @param v1 the end of the first vector
 * @param v2 the start of the first vector and also the start of the second vector
 * @param v3 the end of the second vector
 * @return the calculated normal of the 2 vectors
 */
PoLAR_EXPORT osg::Vec3 calculateNormal(osg::Vec3 v1, osg::Vec3 v2, osg::Vec3 v3);

/**
 * @brief Multiply two 3x3 matrices
 * @return the calculated matrix
 */
PoLAR_EXPORT osg::Matrix3d multiply33(osg::Matrix3d &A, osg::Matrix3d &B);

/**
  *@brief returns the transpose of the matrix @param M
  */
PoLAR_EXPORT osg::Matrixd transpose(const osg::Matrixd& M);

/**
 *@brief returns the subtract of two matrices
 */
PoLAR_EXPORT osg::Matrixd subtract(osg::Matrixd& M1, osg::Matrixd& M2);

/**
 * normalize a projection matrix: make sure that \f$\sum_{i=0}^2 P[2][i]^2 = 1\f$ and the left 3x3 part of the matrix has a positive determinant

   The left 3x3 part of the projection matrix is defined as the submatrix (P[0][0] ; P[2][2]).

   The input projection is modified and returned.

  \param M projection matrix
  \return projection matrix, the input projection is modified and returned
*/
PoLAR_EXPORT void normalizeProjectionMatrix(osg::Matrixd& M);

/** compute World-coordinate-to-camera-coordinate matrix

  More exact computation of the external matrix using Toscani's set of equations. The returned matrix is sure
  to be a rigid transform.
  Toscani's guidelines were not exactly followed. Here are the differences:
  - Toscani imposes that Tz is positive. This may not hold when working with a moving camera where the world
    origin may happen to lie behind the camera somewhere in the sequence. We chose to sign-normalize the projection
    matrix by imposing that det(A) > 0 where A is the 3x3 left submatrix extracted from the projection matrix
    (P[0][0] till P[2][2], proportional to KR)
  - the top row of the external matrix (wc2vc[0]) is not computed using Toscani's equations. R2 and R3 (last two rows
    of the rotation matrix) and Ty and Tz are computed according to Toscani. Then, since Toscani showed the computed
    rotation matrix is unitary, we compute R1 as R2xR3. Tx is computed using all predetermined parameters (see
    code for more precision)
  - also note that minor simplifications have been made to compute R2 and Ty. See code for more details

    \param M projection matrix
    \param wc2vc World-coordinate-to-camera-coordinate matrix
  */
PoLAR_EXPORT void projectionWc2vcToscani(osg::Matrixd& M, osg::Matrixd& wc2vc);

/** compute camera-coordinate-to-World-coordinate matrix
    @param wc2vc World-coordinate-to-camera-coordinate matrix
    @return camera-coordinate-to-World-coordinate matrix (3 \f$ \times \f$ 4)
    @note deprecated. Consider using osg::Matrixd::invert(wc2vc) instead:

        osg::Matrixd vc2wc;

        vc2wc.invert(wc2vc);
*/
PoLAR_EXPORT osg::Matrixd projectionVc2wcFromWc2vc(osg::Matrixd& wc2vc);

/** create a projection matrix from rotation matrix and translation vector

\f[ P = \left ( \begin{array}{cccc}
                           rot[0][0] & rot[0][1] & rot[0][2] & trans[0] \\
               rot[1][0] & rot[1][1] & rot[1][2] & trans[1] \\
               rot[2][0] & rot[2][1] & rot[2][2] & trans[2] \\
\end{array} \right ) \f]

    @param P the input projection matrix
    @param rot rotation matrix, (3 \f$\times\f$ 3)
    @param trans translation vector, (3)
    @return the calculated projection matrix
    @note deprecated. Simply use 'vc2wc * P' (as osg matrices) for the same result.
*/
PoLAR_EXPORT osg::Matrixd projectionCombineRigidMotion(osg::Matrixd& P, osg::Matrixd& rot, osg::Vec3d& trans);

/** write a projection matrix in a text file

the structure of the text file looks like :
  \verbatim
-7.329724 -2.839292 -0.464510 590.930987
0.390943 -1.959532 -7.983030 381.366647
0.000408 -0.005276 -0.000540 1.000000
\endverbatim
  \param M the projection matrix to save
  \param out the name of the file to write
*/
PoLAR_EXPORT void saveProjectionMatrix(osg::Matrixd& M, const char *out);

/**
  * helper method to call saveProjectionMatrix() with a Qt string
  */
PoLAR_EXPORT void saveProjectionMatrix(osg::Matrixd& M, const QString s);

/**
 * @brief Read a projection matrix from a text file
 * @param fileName the name of the file from which to read
 * @return the matrix loaded
 */
PoLAR_EXPORT osg::Matrixd readProjectionMatrix(const char *fileName);

/**
  * helper method to call readProjectionMatrix() with a Qt string
  */
PoLAR_EXPORT osg::Matrixd readProjectionMatrix(const QString s);


/** create a projection matrix from the internal and external parameters
 *
internal or intrinsic parameters are:
\li (u0,v0): coordinates in pixels of the image center which is the projection of the camera center on the image
\li (ku,kv): scale factors of the image. The vertical and horizontal steps of digitization are 1/kv and 1/ku respectively, in mm

external or extrinsic parameters are:

\li R: rotation which gives axes of the camera in the reference coordinate system
\li T: pose in mm of the camera center in the reference coordinate system

  \param u0,v0 coordinate in pixels of the image center which is the projection of the camera center on the image
  \param alphaU vertical scale (pixels/mm)
  \param alphaV horizontal scale (pixels/mm)
  \param alpha angle with X-axis
  \param beta angle with Y-axis
  \param gamma angle with Z-axis
  \param tx translation along X-axis
  \param ty translation along Y-axis
  \param tz translation along Z-axis
  \return projection matrix
*/
PoLAR_EXPORT osg::Matrixd projectionSetParameters(double u0, double v0,
                                                  double alphaU, double alphaV,
                                                  double alpha, double beta, double gamma,
                                                  double tx, double ty, double tz);


/**
 * @brief multiply 2 vectors component-wise:
 * v3 = (v1.x * v2.x, v1.y * v2.y, v1.z * v2.z)
 * @param v1 the first vector
 * @param v2 the second vector
 * @return the multiplication component-wise of the 2 given vectors
 */
PoLAR_EXPORT osg::Vec3d multVec3(osg::Vec3d v1, osg::Vec3d v2);


/**
  * @brief generates a mesh plane from its normal vector
  * @param plane the normal vector of the plane
  * @return a node containing the mesh
  */
PoLAR_EXPORT osg::Node* generatePlaneFromNormal(const osg::Vec4& plane);


PoLAR_EXPORT bool isFixedFunctionPipelineAvailable();

}


}

#endif // POLAR_UTIL_H
