#ifndef SOFAOBJECT3D_H
#define SOFAOBJECT3D_H

#include "PhysicsObject3D.h"
#include <vector>

namespace PoLAR
{



/**
 * Specialization of Object3D to represent deformable objects usign the Vega FEM engine
 */
class SofaObject3D: public PhysicsObject3D
{
    Q_OBJECT

public:
    /**
     * @brief Constructor
     * @param node the osg::Node to encapsulate in this object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    SofaObject3D(osg::Node *node,  bool material = false, bool editable = false, bool pickable = false);


    //void updateOsgMesh();

    /**
     * @brief reset all the simulation, that means the object will be replaced to its original place, the forces will be reset to the rest position, etc.
     */
    void resetSimulation();


    /**
     * @brief sets the requestMeshUpdate flag to true in order to ask for an update of the OSG mesh
     */
    void requestMeshUpdate()
    {
        _requestMeshUpdate = true;
    }


protected:
    /**
      * destructor
      */
    ~SofaObject3D();

    /**
     * @brief computes a matching table between OSG vertices and Vega vertices
     */
    void osgToSofaIndices();


    /**
     * @brief This callback updates the OSG mesh everytime the Vega representation of the object has changed
     */
    struct MeshUpdater;


    /**
     * array of equivalency between OSG vertex indices and Vega tetrahedral mesh indices
     */
    unsigned int *_indexInSoftBody;

    /**
     * array of equivalency between Vega tetrahedral mesh vertex indices and OSG indices
     */
    unsigned int *_indexInOsg;

    /**
     * pointer to the array of vertices in the OSG representation of the object
     */
    osg::Vec3Array *_verticesArray;


    /**
     * flag indicating when the mesh must be updated, i.e. the simulation has performed one timestep
     */
    bool _requestMeshUpdate;

};

}

#endif // SOFAOBJECT3D_H
