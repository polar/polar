/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Marker2D.h
\brief Management of lists of \ref DrawableObject2D

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2014
\note
Corresponding code: Object2D/Marker2D.cpp
*/
/** @} */

#ifndef _POLAR_MARKER2D_H
#define _POLAR_MARKER2D_H

#if defined(WIN32) || defined(_WIN32)
#define _USE_MATH_DEFINES
#endif

#include <QPointF>
#include <iostream>

#include "export.h"

namespace PoLAR
{

/**
 * @brief This class defines a labelled point in 2D space. The label is a float,
 * meaning that it may be used to store a label, image number, confidence
 * criterion, etc... or even store a generic label.
 * Some functions to test the geometry of a set
 * of markers are also included.
 *
 * This class extends the Qt class QPointF.
 */
class PoLAR_EXPORT Marker2D : public QPointF
{
public:

    /** Overload of the output operator for this class
      */
    friend PoLAR_EXPORT std::ostream& operator<<(std::ostream& out, const Marker2D& marker);

    /**
     * Constructor
     * @param x x value
     * @param y y value
     * @param label the label to assign to the Marker2D
     */
    Marker2D(double x=0.0, double y=0.0, float label=0.0f);

    /**
     * copy constructor
     * @param marker the Marker2D to copy
     */
    Marker2D(Marker2D *marker);

    /**
     * constructor from a Point2D
     * @param p the Point2D from which to create the marker
     */
    Marker2D(QPointF& p);

    /**
      * Destructor
      */
    //~Marker2D();

    /**
     * Prints the Marker2D into a file
     * @param out the file into  which the Marker2D is written. Use "stdout" to print in the terminal
     */
    void fprintf(FILE *out) const;

    /**
     * Prints the Marker2D into a stream
     */
    void save(std::ostream& out) const;

    /**
     * Square Euclidian norm
     * @return the square Euclidian norm of the vector represented by the Marker2D: \f$ x^2 + y^2\f$.
     */
    double norm2() const;

    /**
     * @brief Square Euclidian distance
     * @param m1 the first Marker2D
     * @param m2 th second Marker2D
     * @return the square Euclidian distance between the two input #Marker2D's: \f$\sqrt{(x_1-x_2)^2+(y_1-y_2)^2}\f$
     */
    static double distance2(Marker2D *m1, Marker2D *m2);

    /**
     * @brief Euclidian distance
     * @param m1 the first Marker2D
     * @param m2 th second Marker2D
     * @return the Euclidian distance between the two input #Marker2D's: \f$\sqrt{(x_1-x_2)^2+(y_1-y_2)^2}\f$
     */
    static double distance(Marker2D *m1, Marker2D *m2);

    /**
     * Determinant
     * @param p1 the first Marker2D (considered as vector)
     * @param p2 the second Marker2D (considered as vector)
     * @return the determinant of the two vectors (input #Marker2D's): \f$x_1 y_2-y_1 x_2\f$
     */
    static double determinant(Marker2D *p1, Marker2D *p2);

    /**
     * @brief Difference
     * @param p1 the first Marker2D
     * @param p2 th second Marker2D
     * @param r the resulting Marker2D
     */
    static void difference(Marker2D *p1, Marker2D *p2, Marker2D *r);

    /**
     * @brief Scalar product
     * @param p1 the first input Marker2D
     * @param p2 the second input Marker2D
     * @return the scalar product f the two vectors (input #Marker2D's): \f$x_1x_2+y_1y_2\f$
     */
    static double scalarProduct(Marker2D *p1, Marker2D *p2);

    /**
     * @brief Distance between a Marker2D and a segment
     * @param m the Marker2D to test
     * @param sdeb the first extremity of the segment
     * @param sfin the second extremity of the segment
     * @return the distance from the marker to the segment defined by sdeb and sfin
     */
    static double distance2ToSegment(Marker2D *m, Marker2D *sdeb, Marker2D *sfin);

    /**
     * @brief Calculates the angle between the vector formed by the first 2 markers and the vector formed by the last 2 markers
     * @param p1 the first marker
     * @param p2 the second marker
     * @param p3 the third marker
     * @param p4 the fourth marker
     * @return the angle calculated
     */
    static double angle(Marker2D *p1, Marker2D *p2, Marker2D *p3, Marker2D *p4);

    /**
     * @brief returns the label of this marker
     */
    inline float label() const
    {
        return labelp;
    }

    /**
     * @brief returns a reference to the label of this marker.
     * Using a reference makes it possible to directly manipulate the label.
     */
    inline float& rlabel()
    {
        return labelp;
    }

    /**
     * @brief sets the label of this marker.
     * @note The label is a float,
     * meaning that it may be used to store a label, image number, confidence
     * criterion, etc... or even store a generic label.
     */
    inline void setLabel(float l)
    {
        labelp = l;
    }

protected:
    /**
     * @brief a generic label
     * @note The label is a float,
     * meaning that it may be used to store a label, image number, confidence
     * criterion, etc... or even store a generic label.
     */
    float labelp;
};

/**
  * Overload of the output operator for the Marker2D class
  */
PoLAR_EXPORT std::ostream& operator<<(std::ostream& out, const Marker2D& marker);

}
#endif // _POLAR_MARKER2D_H
