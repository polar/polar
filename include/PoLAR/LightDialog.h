/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file LightDialog.h
\brief Custom widget to manage the lighting parameters of a Light

\author Frédéric Speisser, Pierre-Jean Petitprez
\date year 2007
\note 
Corresponding code: Dialog/LightDialog.cpp
*/
/** @} */


#ifndef _POLAR_LIGHTDIALOG_H_
#define _POLAR_LIGHTDIALOG_H_

#include "export.h"
#include "Viewer2D3D.h"
#include "Light.h"

#include <QFile>
#include <QWidget>
#include <QFileDialog>
#include <QLabel>
#include <QLayout>
#include <QSpinBox>
#include <QMenuBar>
#include <QMenu>
#include <QPushButton>
#include <QDialog>
#include <QButtonGroup>
#include <QCheckBox>
#include <QSlider>
#include <QDomDocument>

using Qt::WindowFlags;


namespace PoLAR
{

/** \brief Custom widget to manage the lighting parameters of a Light
*/


class PoLAR_EXPORT LightDialog : public QDialog
{
    Q_OBJECT

public :
    LightDialog(PoLAR::Viewer2D3D *viewer, PoLAR::Light *light=NULL, QWidget * parent = 0, bool modal = false, WindowFlags f = 0);
    void setLight(PoLAR::Light *light);
    void saveFileDialog(const QString&);
    void loadFileDialog(const QString&);

signals:
    void ambientRedValueChanged(float);
    void ambientGreenValueChanged(float);
    void ambientBlueValueChanged(float);
    void diffuseRedValueChanged(float);
    void diffuseGreenValueChanged(float);
    void diffuseBlueValueChanged(float);
    void specularRedValueChanged(float);
    void specularGreenValueChanged(float);
    void specularBlueValueChanged(float);
    void posXValueChanged(float);
    void posYValueChanged(float);
    void posZValueChanged(float);


public slots :
    //     void refresh();
    void loadSlot();
    void saveSlot();
    void defaultSettingsSlot();
    void ambientRedSlot(int value) {emit ambientRedValueChanged((float)(value) / (float)(_lightValuesMax));}
    void ambientGreenSlot(int value) {emit ambientGreenValueChanged((float)(value) / (float)(_lightValuesMax));}
    void ambientBlueSlot(int value) {emit ambientBlueValueChanged((float)(value) / (float)(_lightValuesMax));}
    void diffuseRedSlot(int value) {emit diffuseRedValueChanged((float)(value) / (float)(_lightValuesMax));}
    void diffuseGreenSlot(int value) {emit diffuseGreenValueChanged((float)(value) / (float)(_lightValuesMax));}
    void diffuseBlueSlot(int value) {emit diffuseBlueValueChanged((float)(value) / (float)(_lightValuesMax));}
    void specularRedSlot(int value) {emit specularRedValueChanged((float)(value) / (float)(_lightValuesMax));}
    void specularGreenSlot(int value) {emit specularGreenValueChanged((float)(value) / (float)(_lightValuesMax));}
    void specularBlueSlot(int value) {emit specularBlueValueChanged((float)(value) / (float)(_lightValuesMax));}
    void positionXSlot(int value) {emit posXValueChanged((float)(value) / (float)(_lightValuePosStep));}
    void positionYSlot(int value) {emit posYValueChanged((float)(value) / (float)(_lightValuePosStep));}
    void positionZSlot(int value) {emit posZValueChanged((float)(value) / (float)(_lightValuePosStep));}

private:
    PoLAR::Viewer2D3D *_viewer;
    osg::ref_ptr<PoLAR::Light> _light;
    QString strLightNum;

    QCheckBox *lightCheck;
    QVBoxLayout *mainLayout, *lightLayout, *lightAmbientSlidersLayout, *lightDiffuseSlidersLayout, *lightSpecularSlidersLayout, *lightPosLayout;

    QHBoxLayout *lightSlidersLayout, *buttonsLayout;

    QHBoxLayout *lightAmbientRedLayout, *lightAmbientGreenLayout, *lightAmbientBlueLayout, *lightDiffuseRedLayout, *lightDiffuseGreenLayout, *lightDiffuseBlueLayout, *lightSpecularRedLayout, *lightSpecularGreenLayout, *lightSpecularBlueLayout, *lightPosXLayout, *lightPosYLayout, *lightPosZLayout;

    QLabel *lightLabel, *ambientLabel, *diffuseLabel, *specularLabel, *RaLabel, *GaLabel, *BaLabel, *RdLabel, *GdLabel, *BdLabel, *RsLabel, *GsLabel, *BsLabel, *posXLabel, *posYLabel, *posZLabel;

    QSlider *lightAmbientRedSlider, *lightAmbientGreenSlider, *lightAmbientBlueSlider, *lightDiffuseRedSlider, *lightDiffuseGreenSlider, *lightDiffuseBlueSlider, *lightSpecularRedSlider, *lightSpecularGreenSlider, *lightSpecularBlueSlider;

    QSpinBox *lightAmbientRedSpinBox, *lightAmbientGreenSpinBox, *lightAmbientBlueSpinBox, *lightDiffuseRedSpinBox, *lightDiffuseGreenSpinBox, *lightDiffuseBlueSpinBox, *lightSpecularRedSpinBox, *lightSpecularGreenSpinBox, *lightSpecularBlueSpinBox, *posXSpinBox, *posYSpinBox, *posZSpinBox, *posWSpinBox;

    QMenuBar *menu;
    QMenu *file;
    QPushButton *cancelButton, *defaultSettingsButton;

    int _lightValuesMax, _lightValuePosMax, _lightValuePosStep;
    int _ambientRedInit, _ambientBlueInit, _ambientGreenInit, _diffuseRedInit, _diffuseBlueInit, _diffuseGreenInit, _specularRedInit, _specularBlueInit, _specularGreenInit, _posXInit, _posYInit, _posZInit, _posWInit;

};

}

#endif //_POLAR_LIGHTDIALOG_H_
