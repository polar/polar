/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Image.h
\brief Management of the display of image sequences (precalculated or real time from camera)
\note this is a template class
\author Erwan Kerrien, Pierre-Jean Petitprez
\date year 2014
*/
/** @} */

#ifndef POLARIMAGE_H
#define POLARIMAGE_H

#include "BaseImage.h"
#include "VideoPlayer.h"

namespace PoLAR
{

/** \brief Management of the display of image sequences (instantiable)

This class is used by a Viewer to set a background image, but can also be directly used to display images on any 3D object (these can be mapped since they are stored in textures)

*/

template<typename T>
class Image : public BaseImage
{

public:
    /** \name Public Members */
    /** @{ */

    /**
     * Default constructor
     */
    Image();

    /**
     * Constructor
     * @param sequence the array of \<T\>-type containing the image data
     * @param width the width of the image
     * @param height the height of the image
     * @param depth the depth of the image
     * @param nbImages the number of images contained in the sequence
     * @param manageSequence If true, this object will take ownership of the given sequence and thus deallocate it. If set to false, the user has to take care of the sequence himself
     */
    Image(T* sequence, int width, int height, int depth, int nbImages=1, bool manageSequence=true);


    /**
     * Constructor
     * @param fileName the name of the file to load
     * @param flipVertical set it to true if the image must be flipped vertically
     * @param nbImages the number of images contained in the file
     */
    Image(const std::string& fileName, bool flipVertical=false, int nbImages=1);


    /** Copy constructor using osg::CopyOp to manage deep vs shallow copy.*/
    Image(const Image &im, const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);


    /** destructor */
    ~Image();

    /** clone an object of the same type as the node.*/
    virtual osg::Object* cloneType() const { return new Image(); }

    /** return a clone of an image, with Object* return type.*/
    virtual osg::Object* clone(const osg::CopyOp& copyop) const { return new Image(*this,copyop); }

    /** return the name of the class.*/
    virtual const char* className() const { return "PoLAR::Image"; }

    /** manage the image sequence, go to next image in the sequence
          \param wrap if true, goto the first image after the last image has been reached. Else, does not go beyond the last image.
          \note emits indexSignal(int)
      */
    virtual void nextImageSlot(bool wrap=true);

    /** manage the image sequence, go to previous image in the sequence
          \param wrap if true, goto the last image after the firts image has been reached. Else, does not go before the first image.
          \note emits indexSignal(int)
      */
    virtual void previousImageSlot(bool wrap=true);

    /** manage the image sequence, go to a specific image in the sequence
        \note emits indexSignal(int)
     */
    virtual void gotoImageSlot(unsigned int i=0);

    /** get the values of the pixel at position \param x and \param y
     * \note the size of the returning vector depends on the depth of the image (grayscale: 1 / RGB: 3 / RGBA: 4)
     */
    virtual inline std::vector<T> getPixelData(double x, double y) const;
  
    /** get the current image in grey levels
    */
    virtual unsigned char* getMonoImage(unsigned char *out) const;

    /** given a displayed value 'level', give the actual grey value (aka invert the various LUTs)
        \sa getDisplayPixelValue()
     */
    virtual float getRealPixelValue(unsigned char level) const
    {
        return (level-_offsetIntensity)/_scaleIntensity;
    }

    /** get the displayed value (between 0 and 255), given an actual pixel value (aka applies all the various LUTs)
        \sa getRealPixelValue()
    */
    virtual int getDisplayedPixelValue(float value) const
    {
        return range(_scaleIntensity*value+_offsetIntensity);
    }

    /** get the index of the image currently displayed
        \return the index of the image currently displayed
    */
    virtual int imageIndex() const {return _currentImageIndex;}

    /** \return the number of images in the sequence
     */
    virtual int getNbImages() const {return _numImages;}

    /** return the image data corresponding to a number of image in the sequence
      \param index the number of the image
    */
    virtual T* getDataPtr(unsigned int index) const
    {
        if (_numImages == 0 || !_imageSequence) return NULL;
        if (index >= _numImages) index = _numImages-1;
        return _imageSequence + index * _imageDimension;
    }

    /** return the current image data */
    virtual T* getDataPtr() const {return _currentData;}

    /**
     * display the next frame in case of dynamic image (from a camera)
     */
    virtual void displayNextFrame(unsigned char *Data);

    /**
     * display the next frame in case of dynamic image (from a camera)
     */
    virtual void displayNextFrame(unsigned char *Data, int width, int height, int depth=3);


    /** @} */

protected:
    /** \name Protected Members */
    /** @{ */

    /** call this if the image data have changed so that the pixel values min and max have changed.
        A normalization between 0 and 255 is made over the whole sequence for visualization purpose.
        If the min and max values of the sequence have changed so that min is lower OR the max is higher,
        you MUST call this function before calling update(). If they have changed but the min is higher AND
        the max is lower, you may call this function only if you want a renormalization of the visualized pixel
        values
    */
    void updateMinMax();


    /** Convert an array of unsigned chars into an array of \<T\>
    The conversion is simply made by a cast on each element.
    \param uc_array the array to convert
    \param dim the number of elements in the input and output arrays

    \return
    - the new allocated \<T\>-array resulting from the conversion
    - NULL in case of problem (invalid parameters or memory allocation failure)
    */
    T* fromUChar(const unsigned char *uc_array, const int dim);


    /** update the current image, manage the current image data to be displayed
      \note emits newImageSignal()
    */
    void update();
    using osg::Image::update;

    /** regenerate the texture of the current image
      \note emits updateSignal()
    */
    void refresh();


    /** the whole image sequence
    */
    T* _imageSequence;

    /** the current image data
    */
    T* _currentData;

    /** the original data information
    */
    unsigned int _numImages;

    /** the max over the whole sequence
    */
    T _maxImage;

    /** the min over the whole sequence
    */
    T _minImage;

    /** necessary for the image normalization to 0..255
    */
    float _scaleIntensity;

    /** necessary for the image normalization to 0..255
    */
    float _offsetIntensity;

    /** the index of the current image displayed
    */
    unsigned int _currentImageIndex;

    /** the current texture pixels
    */
    unsigned char *_data;

    /** If true, this object will take ownership of the sequence given in constructor and thus deallocate it
     */
    bool _manageSequence;

    /** @} */
};


//////////////////////// Template specialization <unsigned char> ////////////////////////

typedef Image< unsigned char >  Image_uc;

/** \brief Management of the display of image sequences (instantiable) - unsigned char specialization

This class is used by a Viewer to set a background image, but can also be directly used to display images on any 3D object (these can be mapped since they are stored in textures)

*/
template<>
class Image<unsigned char> : public BaseImage
{

public:
    /** \name Public members */
    /** @{ */

    /**
     * default constructor
     */
    inline Image();

    /**
     * Constructor
     * @param sequence the array of unsigned char containing the image data
     * @param width the width of the image
     * @param height the height of the image
     * @param depth the depth of the image
     * @param nbImages the number of images contained in the sequence
     * @param manageSequence If true, this object will take ownership of the given sequence and thus deallocate it. If set to false, the user has to take care of the sequence himself
     */
    inline Image(unsigned char* sequence, int width, int height, int depth, int nbImages=1, bool manageSequence=true);

    /**
     * Constructor
     * @param fileName the name of the file to load
     * @param flipVertical set it to true if the image must be flipped vertically
     * @param nbImages the number of images contained in the file
     */
    inline Image(const std::string &fileName, bool flipVertical=false, int nbImages=1);



    /** Copy constructor using osg::CopyOp to manage deep vs shallow copy.*/
    inline Image(const Image &im, const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY);

    /**
    * @brief Constructor from PoLAR::VideoPlayer
    * @param camera the VideoPlayer which this Image will display the video flow
    */
    inline Image(VideoPlayer *camera);

    /**
      *destructor
      */
    inline ~Image();

    /** clone an object of the same type as the node.*/
    virtual osg::Object* cloneType() const { return new Image(); }

    /** return a clone of an image, with Object* return type.*/
    virtual osg::Object* clone(const osg::CopyOp& copyop) const { return new Image(*this, copyop); }

    /** return the name of the class.*/
    virtual const char* className() const { return "PoLAR::Image"; }

    /** manage the image sequence, go to next image in the sequence
          \param wrap if true, goto the first image after the last image has been reached. Else, does not go beyond the last image.
          \note emits indexSignal(int)
     */
    virtual inline void nextImageSlot(bool wrap=true);

    /** manage the image sequence, go to previous image in the sequence
          \param wrap if true, goto the last image after the firts image has been reached. Else, does not go before the first image.
          \note emits indexSignal(int)
     */
    virtual inline void previousImageSlot(bool wrap=true);

    /** manage the image sequence, go to a specific image in the sequence
        \note emits indexSignal(int)
     */
    virtual inline void gotoImageSlot(unsigned int i=0);

    /** get the values of the pixel at position \param x and \param y
     * \note the size of the returning vector depends on the depth of the image (grayscale: 1 / RGB: 3 / RGBA: 4)
     */
  virtual inline std::vector<unsigned char> getPixelData(double x, double y) const;

    /** get the current image in grey levels
    */
    virtual inline unsigned char* getMonoImage(unsigned char *out) const;

    /** given a displayed value 'level', give the actual grey value (aka invert the various LUTs)
        \sa getDisplayedPixelValue()
     */
    virtual inline float getRealPixelValue(unsigned char level) const
    {
        return (level-_offsetIntensity)/_scaleIntensity;
    }

    /** get the displayed value (between 0 and 255), given an actual pixel value (aka applies all the various LUTs)
        \sa getRealPixelValue()
    */
    virtual inline int getDisplayedPixelValue(float value) const
    {
        return range(_scaleIntensity*value+_offsetIntensity);
    }

    /** get the index of the image currently displayed
        \return the index of the image currently displayed
    */
    virtual inline int imageIndex() const {return _currentImageIndex;}

    /** \return the number of images in the sequence
     */
    virtual inline int getNbImages() const {return _numImages;}

    /** return the image data corresponding to a number of image in the sequence
      \param index the number of the image
    */
    virtual inline unsigned char * getDataPtr(unsigned int index) const
    {
        if(_numImages == 0 || !_imageSequence) return NULL;
        if (index >= _numImages) index = _numImages-1;
        return _imageSequence + index * _imageDimension;
    }

    /** return the current image data */
    virtual inline unsigned char * getDataPtr() const {return (unsigned char*)this->data();}

    /**
     * display the next frame in case of dynamic image (from a camera)
     */
    virtual inline void displayNextFrame(unsigned char* data);

    /**
     * display the next frame in case of dynamic image (from a camera)
     */
    virtual inline void displayNextFrame(unsigned char* data, int width, int height, int depth=3);

    /** @} */

protected:
    /** \name Protected members */
    /** @{ */

    /** call this if the image data have changed so that the pixel values min and max have changed.
        A normalization between 0 and 255 is made over the whole sequence for visualization purpose.
        If the min and max values of the sequence have changed so that min is lower OR the max is higher,
        you MUST call this function before calling update(). If they have changed but the min is higher AND
        the max is lower, you may call this function only if you want a renormalization of the visualized pixel
        values
    */
    virtual inline void updateMinMax();


    /** update the current image, manage the current image data to be displayed
      \note emits newImageSignal()
    */
    virtual inline void update();
    using osg::Image::update;


    /** the whole image sequence
  */
    unsigned char *_imageSequence;

    /** the current image data
  */
    unsigned char *_currentData;

    /** number of images in the case of a multi-image sequence
  */
    unsigned int _numImages;

    /** the max over the whole sequence
  */
    unsigned char _maxImage;

    /** the min over the whole sequence
  */
    unsigned char _minImage;

    /** necessary for the image normalization to 0..255
  */
    float _scaleIntensity;

    /** necessary for the image normalization to 0..255
  */
    float _offsetIntensity;

    /** the index of the current image displayed
  */
    unsigned int _currentImageIndex;

    /** If true, this object will take ownership of the sequence given in constructor and thus deallocate it
     */
    bool _manageSequence;

    /** @} */

};


}

#include "Image.tpp"
#endif // POLARIMAGE_H
