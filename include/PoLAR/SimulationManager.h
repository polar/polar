#ifndef SIMULATIONMANAGER_H
#define SIMULATIONMANAGER_H


#include <QThread>
#include <osg/Referenced>
#include <osg/Node>
#include <osgText/Text>
#include <string>
#include "SimulationWorker.h"
#include "SceneGraph.h"
#include "Viewer.h"
#include "export.h"

namespace PoLAR
{

/**
 * @brief Class for the management of a physics simulation.
 * Physics engines like Vega or Bullet are run into another thread thanks to their implementation in a derived class of Simulationworker.
 * This class aims to manage this thread.
 */
class PoLAR_EXPORT SimulationManager: public QObject, public osg::Referenced
{
    Q_OBJECT

public:
    /**
     * Constructor
     * @param worker the SimulationWorker to set in the thread
     * @param timestep the time step used for setting the simulation
     */
    SimulationManager(SimulationWorker *worker, double timestep=defaultTimeStep, bool launch=true);

    /**
      * destructor
      */
    ~SimulationManager();

    /**
     * launch the thread
     */
    void launch();

    /**
     * reset the thread
     */
    //void reset();

    /**
     * pause the simulation worker
     */
    void pause();

    /**
     * unpause the simulation worker
     */
    void play();

    /**
     * stop the simulation worker and the thread
     */
    void stop();

    /**
     * returns a pointer to the simulation worker if one exists, NULL otherwise
     */
    SimulationWorker* getWorker() const
    {
        if(_simWorker)
            return _simWorker;
        return NULL;
    }

    /**
     * returns true if the worker is runnung, false otherwise
     */
    bool isRunning() const
    {
        return _simWorker->isWorking();
    }

    /**
     * returns the current FPS of the thread
     */
    double getFPS() const
    {
        return _simWorker->currentFramerate();
    }

    /**
     * set to true to display FPS on the given viewer
     */
    void displayFramerate(bool b, Viewer* viewer);

    /**
     * returns true is the FPS are displayed
     */
    bool isFramerateDisplayed() const;


public slots:

    void updateFramerate(double fps);

protected:

    /**
     * initialization of the thread and the signals
     */
    void init();

    /**
     * the thread in which the simulation is launched
     */
    QThread *_qthread;

    /**
     * pointer to the simulation worker to be set in the thread
     */
    osg::ref_ptr<SimulationWorker> _simWorker;

    /**
     * time step to set in the simulation loop
     */
    double _timestep;

    /** pointer to the scene graph to which the HUD will be atttached */
    osg::ref_ptr<SceneGraph> _sceneGraph;


    /** pointer to the node containing the hud */
    osg::ref_ptr<osg::Node> _hudNode;


    /** pointer to the text for displaying FPS */
    osg::ref_ptr<osgText::Text> _hudText;


    /**
     * default time step (33.0 -> 30 FPS)
     */
    const static double defaultTimeStep;

    /**
     * if true, framerate is displayed on screen
     */
    bool _isFramerateDisplayed;

    /**
     * static strings for HUD
     */
    const static std::string threadPaused;
    const static std::string threadStopped;
    const static std::string threadWorking;
};

}

#endif // SIMULATIONMANAGER_H
