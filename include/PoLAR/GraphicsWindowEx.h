/* -*-c++-*- OpenSceneGraph - Copyright (C) 1998-2006 Robert Osfield
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/

/** @{ */
/** \file GraphicsWindowEx.h
\brief Patch on some OSG classes to allow the use of FBO with Qt > 5.4 (and QOpenGLWidget)

\author Pierre-Jean Petitprez
\date 2015
\note
Corresponding code: Viewer/GraphicsWindowEx.cpp
*/
/** @} */


#ifndef GRAPHICSWINDOWEX_H
#define GRAPHICSWINDOWEX_H

#ifdef OSG_USE_FBO

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif

#include "export.h"

#include <osg/State>
#include <osgViewer/GraphicsWindow>
#include <osgUtil/RenderStage>
#include <osgUtil/RenderLeaf>
#include <osgUtil/CullVisitor>
#include <osg/Version>


class PoLAR_EXPORT StateEx : public osg::State
{
public:
    StateEx() :
        defaultFbo(0)
    {}

    inline void setDefaultFbo(GLuint fbo)
    {
        defaultFbo = fbo;
    }

    inline GLuint getDefaultFbo() const
    {
        return defaultFbo;
    }

protected:
    GLuint defaultFbo;
};




class PoLAR_EXPORT GraphicsWindowEx : public osgViewer::GraphicsWindow
{
public:
    GraphicsWindowEx(osg::GraphicsContext::Traits* traits);
    GraphicsWindowEx(int x, int y, int width, int height);

    void init();

    virtual bool isSameKindAs(const osg::Object* object) const { return dynamic_cast<const GraphicsWindowEx *>(object) != 0; }
    virtual const char* libraryName() const { return ""; }
    virtual const char* className() const { return "GraphicsWindowEx"; }

    // dummy implementations, assume that graphics context is *always* current and valid.
    virtual bool valid() const { return true; }
    virtual bool realizeImplementation() { return true; }
    virtual bool isRealizedImplementation() const  { return true; }
    virtual void closeImplementation() {}
    virtual bool makeCurrentImplementation() { return true; }
    virtual bool releaseContextImplementation() { return true; }
    virtual void swapBuffersImplementation() {}
    virtual void grabFocus() {}
    virtual void grabFocusIfPointerInWindow() {}
    virtual void raiseWindow() {}
};


class PoLAR_EXPORT RenderStageEx: public osgUtil::RenderStage
{
public:
    RenderStageEx():
        RenderStage()
    {}

    virtual const char* className() const
    {
        return "RenderStageEx";
    }

    virtual void drawInner(osg::RenderInfo& renderInfo, osgUtil::RenderLeaf*& previous, bool& doCopyTexture);
};


class PoLAR_EXPORT CullVisitorEx: public osgUtil::CullVisitor
{
public:
    CullVisitorEx():
    #if OSG_MIN_VERSION_REQUIRED(3,3,7)
        osg::Object(),
    #else
        osg::Referenced(),
    #endif
        osgUtil::CullVisitor()
    {
        setRenderStage(new RenderStageEx());
    }

    CullVisitorEx(const CullVisitorEx& cv):
    #if OSG_MIN_VERSION_REQUIRED(3,3,7)
        osg::Object(),
    #else
        osg::Referenced(),
    #endif
        osgUtil::CullVisitor(cv)
    {
        setRenderStage(new RenderStageEx());
    }

    CullVisitorEx* clone() const { return new CullVisitorEx(*this); }
    
    using osgUtil::CullVisitor::apply;
    void apply(osg::Camera &camera);

    virtual const char* className() const
    {
        return "CullVisitorEx";
    }
};

#endif

#endif // GRAPHICSWINDOWEX_H

