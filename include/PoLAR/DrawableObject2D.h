/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file DrawableObject2D.h
\brief Management of the display of \ref Object2D

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2014
\note
Corresponding code: Object2D/DrawableObject2D.cpp
*/
/** @} */

#ifndef _POLAR_DRAWABLEOBJECT2D_H_
#define _POLAR_DRAWABLEOBJECT2D_H_

#include <QMouseEvent>
#include <QObject>
#include <QGraphicsItem>
#include <QRectF>
#include <QDebug>
#include <osg/ref_ptr>

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include "Object2D.h"
#include "export.h"

#define REGISTER_TYPE(className) \
    class className##Factory : public PoLAR::DrawableObject2DFactory { \
    public: \
    className##Factory() { \
    PoLAR::DrawableObject2D::registerType(#className, this); \
    } \
    virtual PoLAR::DrawableObject2D *create(std::list<Marker2D *> list, PoLAR::Viewer2D *Parent) { \
    return new className(list, Parent); \
    } \
    }; \
    static className##Factory global_##className##Factory;


namespace PoLAR
{

class Viewer2D;
class DrawableObject2D;


class PoLAR_EXPORT DrawableObject2DFactory
{
public:
    virtual DrawableObject2D *create(std::list<Marker2D *>, Viewer2D *) = 0;
};


/** \brief Management of the display of \ref Object2D
*/
class PoLAR_EXPORT DrawableObject2D : public QObject, public QGraphicsItem
{

    /** Overload of the output operator for this class
    \n All DrawableObject2D can be written in a stream thanks to this method.
    \li Exemple of use : save a DrawableObject2D (object) in a file (savefile.txt)
    \n std::ofstream out("savefile.txt", std::ios::out);
    \n out << object2D; \n
    \li Exemple of use : print out a DrawableObject2D (drawableObject2D) on the standard output
    \n std::cout << drawableObject2D; \n \n
    \sa source code of void ListDrawableObject2D::save(char *fname)
  */
    friend PoLAR_EXPORT std::ostream& operator<<(std::ostream& out, const DrawableObject2D& object2D);

    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    Q_ENUMS( MarkerShape )
    Q_PROPERTY( QColor SelectColor READ getSelectColor WRITE setSelectColor )
    Q_PROPERTY( QColor SelectMarkerColor READ getSelectMarkerColor WRITE setSelectMarkerColor )
    Q_PROPERTY( QColor UnselectColor READ getUnselectColor WRITE setUnselectColor )
    Q_PROPERTY( MarkerShape SelectShape READ getSelectShape WRITE setSelectShape )
    Q_PROPERTY( MarkerShape SelectMarkerShape READ getSelectMarkerShape WRITE setSelectMarkerShape )
    Q_PROPERTY( MarkerShape UnselectShape READ getUnselectShape WRITE setUnselectShape )
    Q_PROPERTY( int MarkerSize READ getMarkerSize WRITE setMarkerSize )
    Q_PROPERTY( int PointingPrecision READ getPointingPrecision WRITE setPointingPrecision )
    Q_PROPERTY( int LineWidth READ getLineWidth WRITE setLineWidth )
    Q_PROPERTY( bool Displayed READ isDisplayed WRITE setDisplay )
    Q_PROPERTY( bool Editable READ isEditable WRITE setEdit )
    Q_PROPERTY( bool Selected READ isSelected WRITE setSelect )

public:
    /** Exception thrown in case of I/O error (e.g. load failed)
   */
    class PoLAR_EXPORT IOError
    {
    public:
        IOError() {}
        IOError(std::string m) : msg(m) {}
        friend std::ostream &operator<<(std::ostream &s, const IOError &e)
        {
            if (!e.msg.empty()) s << e.msg << std::endl;
            return s;
        }
        friend QDebug operator<<(QDebug d, const IOError &e)
        {
            if (!e.msg.empty())
            {
                QString str = QString::fromStdString(e.msg);
                d << str;
            }
            return d;
        }

    private:
        std::string msg;
    };

    /** display properties for a 2D Object */
    enum MarkerShape {NONE=0, DEFAULT=1, CROSS=1, SQUARE, POINT, CIRCLE};
    enum MarkerManipType {ADD, REMOVE, SELECT, PRINT};
    const static int NbMarkerShape;// = 5;
    const static int defaultPrecision;// = 3;
    const static int defaultSize;// = 3;
    const static int defaultLineWidth;// = 1;
    const static MarkerShape defaultSelShape;// = SQUARE;
    const static MarkerShape defaultSelmarkShape;// = SQUARE;
    const static MarkerShape defaultUnselShape;// = CROSS;
    const static QColor defaultSelCol; // yellow
    const static QColor defaultUnselCol; // blue
    const static QColor defaultSelmarkCol; // red

    static const char *MarkerShapeToString(const MarkerShape shape)
    {
        switch(shape)
        {
        case NONE: return "NONE";
        case CROSS: return "CROSS";
        case SQUARE: return "SQUARE";
        case POINT: return "POINT";
        case CIRCLE: return "CIRCLE";
        }
        return "UNKNOWN";
    }

    static MarkerShape StringToMarkerShape(const QString s)
    {
        if (s == "NONE") return NONE;
        else if (s == "CROSS") return CROSS;
        else if (s == "SQUARE") return SQUARE;
        else if (s == "POINT") return POINT;
        else if (s == "CIRCLE") return CIRCLE;
        return NONE;
    }

    /** types of DrawableObject2D

  should be edited in case a new object is defined */
    enum ObjectType {
        Markers='M',
        Arrows='A',
        Blob='B',
        Spline='S',
        Polygon='P',
        PolyLine='L',
        Text='X'
    };

    /**
     * types of QPen available
     */
    enum PenType : unsigned int {
        Selected = 0,
        Unselected = 1,
        MarkerSelected = 2
    };

    /**
     * @brief checks if the object type read in the loaded file is valid or not
     */
    static bool isTypeValid(const std::string &s);

    /** to read a #DrawableObject2D from stream
      \param in the stream to read from
      \param w,h the width and height of the related image (see \ref ListDrawableObject2D)
      \param Parent the parent viewer
      \note the loading/saving format is described in function \ref ListDrawableObject2D::save
  */
    static DrawableObject2D *load(std::istream &in, Viewer2D *Parent);


    /**
     * @brief creates a Drawable[...] type using the factories map to find the right type
     */
    static DrawableObject2D *create(const std::string &name, std::list<Marker2D *> list, Viewer2D *Parent)
    {
        return _factories[name]->create(list, Parent);
    }


    /** \name Public Member Functions */
    /** @{ */


    /** constructor
    \param Parent parent viewer
    \param unselCol colour of an unselected segment, default is blue
    \param selCol colour of a selected segment, default is yellow
    \param selmarkCol colour of a selected marker, default is yellow
    \param unselShape shape of an unselected marker
    \param selShape shape of a marker when object is selected
    \param selmarkShape shape of a selected marker
    \param Size size of a marker
    \param Precision pointing precision for selection
    \param LineWidth line width\n
    if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
  */
    DrawableObject2D(Viewer2D *Parent = NULL,
                     QColor unselCol=defaultUnselCol,
                     QColor selCol=defaultSelCol,
                     QColor selmarkCol=defaultSelmarkCol,
                     MarkerShape unselShape=defaultUnselShape,
                     MarkerShape selShape=defaultSelShape,
                     MarkerShape selmarkShape=defaultSelmarkShape,
                     int Size=defaultSize,  int Precision=defaultPrecision,
                     int LineWidth = defaultLineWidth);



    /**
     * constructor
     * @param Parent parent viewer
     * @param unselectPen QPen for an unselected segment
     * @param selectPen QPen for a selected segment
     * @param selectMarkerPen QPen for a selected marker
     * @param unselShape shape of an unselected marker
     * @param selShape shape of a marker when object is selected
     * @param selmarkShape shape of a selected marker
     * @param Size size of a marker
     * @param Precision pointing precision for selection \n
     * if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
     */
    DrawableObject2D(Viewer2D *Parent,
                     QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                     MarkerShape unselShape=defaultUnselShape,
                     MarkerShape selShape=defaultSelShape,
                     MarkerShape selmarkShape=defaultSelmarkShape,
                     int Size=defaultSize,  int Precision=defaultPrecision);


    /**
     * @brief Constructor by deep copy
     * @param obj the DrawableObject2D to copy
     */
    DrawableObject2D(DrawableObject2D &obj, bool deepCopy=true);

    /** destructor
    \note emits modifyObjectSignal(DrawableObject2D *obj)
  */
    virtual ~DrawableObject2D();

    /**
     * @brief swap implementation of the copy-and-swap idiom
     */
    void swap(DrawableObject2D& first, DrawableObject2D& second);


    /**
     * @brief returns the class name
     * @note used for serializing the class (for saving/loading)
     * @note this method must be overriden in subclasses
     */
    virtual const char* className() const
    {
        return "DrawableObject2D";
    }

    // properties management

    void setViewer(Viewer2D *viewer)
    {
        _viewer = viewer;
    }

    /**
     * @brief set QPens for drawing
     * @param select the QPen for when the object2D is selected
     * @param unselect the QPen for when the object2D is not selected
     * @param selectMarker the QPen for when a marker is selected
     */
    void setPens(QPen& select, QPen& unselect, QPen& selectMarker);

    /**
     * @brief setPen set a QPen for drawing
     * @param pen the QPen to set
     * @param type between Selected, Unselected and MarkerSelected (see setPens)
     */
    void setPen(QPen& pen, PenType type);

    /**
     * @brief get the QPen corresponding to the given type
     */
    QPen& getPen(PenType type);


    /** set selection color
  */
    void setSelectColor(QColor color=defaultSelCol);
    /** get selection color
  */
    QColor getSelectColor() const
    {return _selcol;}

    /** set selected marker color
  */
    void setSelectMarkerColor(QColor color=defaultSelmarkCol);
    /** get selected marker color
  */
    QColor getSelectMarkerColor() const
    {return _selmarkcol;}

    /** set unselected color
  */
    void setUnselectColor(QColor color=defaultUnselCol);
    /** get unselected color
  */
    QColor getUnselectColor() const
    {return _unselcol;}

    /** set marker shape when object is selected
  */
    void setSelectShape(MarkerShape shape=defaultSelShape);
    MarkerShape getSelectShape() const {return selshape;}

    /** set selected marker shape
  */
    void setSelectMarkerShape(MarkerShape shape=defaultSelmarkShape);
    MarkerShape getSelectMarkerShape() const {return selmarkshape;}

    /** set unselected marker shape
  */
    void setUnselectShape(MarkerShape shape=defaultUnselShape);
    MarkerShape getUnselectShape() const {return unselshape;}

    /** set marker size
  */
    void setMarkerSize(int Size=defaultSize);
    /** get marker size
  */
    int getMarkerSize() const {return _size;}

    /** set pointing precision
  */
    void setPointingPrecision(int Precision=defaultPrecision) ;
    /** get pointing precision
  */
    int getPointingPrecision() const {return precision;}

    /** set line width
   */
    void setLineWidth(int LineWidth = defaultLineWidth);
    /** get line width
   */
    int getLineWidth() const {return _linewidth;}

    /** generic function to set properties
  */
    void setProperties(QColor unselectColor=defaultUnselCol,
                       QColor selectColor=defaultSelCol,
                       QColor selectMarkerColor=defaultSelmarkCol,
                       MarkerShape unselectShape=defaultUnselShape,
                       MarkerShape selectShape=defaultSelShape,
                       MarkerShape selectMarkerShape=defaultSelmarkShape,
                       int Size = defaultSize,
                       int Precision = defaultPrecision,
                       int LineWidth = defaultLineWidth);

    /** get the 2D object
    \return the 2D object
  */
    Object2D *getObject() { return _object.get(); }

    // status management
    void setDisplay(bool t) {_display = t;}

    /** turn on displaying of the 2D object
  */
    void displayOn() { setDisplay(true); }

    /** turn on displaying of the 2D object
  */
    void displayOff() { setDisplay(false); }


    void setEdit(bool t) {_edit=t;}
    /** turn on edition of the 2D object
  */
    void editOn() { setEdit(true); }

    /** turn on edition of the 2D object
  */
    void editOff() { setEdit(false); }


    void setSelect(bool t) {_select=t;}
    /** turn on selection of the 2D object
  */
    void selectOn() { setSelect(true); }

    /** turn on selection of the 2D object
  */
    void selectOff() { setSelect(false); }

    /** get displaying status of the 2D object
    \return displaying status of the 2D object
  */
    bool isDisplayed() const { return _display; }

    /** get edition status of the 2D object
    \return edition status of the 2D object
  */
    bool isEditable() const { return _edit; }

    /** get emptiness status of the 2D object
    \return 0 if 2D object does not contain markers else 1
  */
    bool isEmpty() const { return _object->nbMarkers() != 0 ? false : true; }

    /** get selection status of the 2D object
    \return selection status of the 2D object
  */
    bool isSelected() const { return _select; }

    /** basically reset colors and shape and disconnect slots
    \note emits unselectMarkerSignal()
  */
    void deactivate();

    /** basically connect slots
  */
    void activate();

    /**
     * add a control point at position (x,y)
     * \nif control point at (x,y) already exists, print its label and coordinates
     */
    void addMarker(double x, double y);

    /**
     * remove control point at position (x,y) if existing
     */
    void removeMarker(double x, double y);

    /**
     * remove control point which is currently selected
     */
    void removeCurrentMarker();

    /**
     * select control point at (x,y) if existing
     */
    void selectMarker(double x, double y);

    /**
     * print control point label and coordinates if control point at (x,y) exists
     */
    void printMarker(double x, double y);

    /**
     * unselect the selected control point
     */
    void unselectMarker();

    /** set the name of this object */
    inline void setName(const char* name)
    {
        setObjectName(QString(name));
    }


    /** get the name of this object */
    inline const char* getName() const
    {
        return objectName().toLatin1().constData();
    }


    /** set the type of this object
    \note the type must be chosen among these possibilities : \n
                'M' for a DrawableMarkers2D \n
                'A' for a DrawableArrows2D \n
                'B' for a DrawableBlob2D \n
                'P' for a DrawablePolygon2D \n
                'L' for a DrawablePolyLine2D \n
                'S' for a DrawableSpline2D \n
                'T' for a DrawableText2D\n
  */
    inline void setType(char type)
    {
        _type = type;
    }

    /** get the type of this object */
    inline char getType() const
    {
        return _type;
    }

    /**
     * get a pointer to the object2D
     * @note const pointer to forbid deallocation by the user
     */
    inline const Object2D * getObject() const
    {
        return _object.get();
    }

    /** to save this object in a stream, redefined in subclasses
      \param out the stream to write in
      \note the loading/saving format is described in function \ref ListDrawableObject2D::save
  */
    virtual void save(std::ostream &out) const;

    /**
     * save this object into a SVG file
     * @param fname the file in which to save the object
     */
    virtual void saveAsSvg(QString& fname);


    virtual void saveAsSvg(const char *fname)
    {
        QString str(fname);
        saveAsSvg(str);
    }


    /**
     * Redefinition of the virtual function from QGraphicsItem - needs to be redefined in the subclasses
     * This virtual method only draws the markers (the only common shape to all 2D items).
     * Each subclass must reimplement this method to draw the other shapes of the item.
     */
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    /**
     * Redefinition of the virtual function from QGraphicsItem.
     * This method should not be used.
     */
    virtual QRectF boundingRect() const;

    /**
     * Redefinition of the function from QGraphicsItem. Used to check an item's shape
     * @return the item's shape in QPainterPath format
     */
    virtual QPainterPath shape() const;

    /**
     * @brief returns the number of markers (control points) of the object
     */
    unsigned int getNbMarkers() const
    {
        if(_object.valid()) return _object->nbMarkers();
        return 0;
    }

    /**
     * @brief get the list of markers
     */
    std::list<Marker2D*>& getMarkers()
    {
        return _object->getMarkers();
    }

    /**
     * @brief returns a pointer to the marker at position i
     */
    Marker2D* getMarker(unsigned int i)
    {
        if(_object.valid()) return _object->getMarker(i);
        return NULL;
    }

    /**
     * @brief returns a pointer to the currently selected marker
     */
    Marker2D* getCurrentMarker()
    {
        if(_object.valid()) return _object->getCurrentMarker();
        return NULL;
    }

    /**
     * @brief Returns a copy of this object
     * @param deepCopy if true, the link object2D will be copied. If false, the object2D pointer will point to the copied object's object2D
     * @return a pointer to a new DrawableObject2D with the same specs than this object
     * @note this method must be implemented in subclasses
     */
    virtual DrawableObject2D * clone(bool=true)=0;


    /**
     * @brief register function to register custom object types
     */
    static void registerType(const std::string& name, DrawableObject2DFactory *factory)
    {
        _factories[name] = factory;
    }
    /** @} */


public slots:

    /** \name Public Slots */
    /** @{ */

    /** manage the mouse press event
    \note can emit several signals : selectMarkerSignal(Marker2D *), modifyObjectSignal(DrawableObject2D *obj), unselectMarkerSignal(), selectMarkerSignal(Marker2D *)
    \note emits updateSignal()
  */
    virtual void markerManipulationSlot(double x, double y, int type);

    /** manage the mouse move event
    \note emits modifyObjectSignal(DrawableObject2D *obj) and updateSignal()
  */
    void markerMoveSlot(double x, double y);

    /** manage the mouse release event
  */
    void markerReleaseSlot();

    /** change the current marker label
    \note emits modifyObjectSignal(DrawableObject2D *obj) in normal behaviour
  */
    void changeMarkerLabel(float l);

    /** @} */


signals:

    /** \name Signals */
    /** @{ */

    /** this signal is emitted when the 2D object needs to be (re)painted
  */
    void updateSignal();

    /** manage the marker selection
  */
    void selectMarkerSignal(Marker2D *);

    /** manage the marker unselection
  */
    void unselectMarkerSignal();

    /** manage the 2D object modification
  */
    void modifyObjectSignal(DrawableObject2D *obj);

    /** @} */


protected:
    /** \name Protected members */
    /** @{ */

    /**
     * @brief method used to load custom parameters that are different for each type of Drawable[...]
     * @param in the entry stream
     */
    virtual void load(std::istream&){}


    /** translate mouse coordinates from the image frame to the Qt frame
   */
    void transformCoord(double xima, double yima, double &xqt, double &yqt) const;

    /**
     * initialize the QPens used for drawing
     */
    void initPens();

    /**
     * reinitialize the color of a QPen
     * @param ind the index of the QPen in the pen list (0: selected 1: unselected 2: selected marker
     */
    void reinitPenColor(PenType ind);

    /**
     * reinitiliaze the width of the QPens
     * @param width the new width
     */
    void reinitPensWidth(int _width);

    /**
     * update the color and linewidth settings for selected object, unselected object and selected makers from the ones currently set in the corresponding QPens
     */
    void updateSettingsFromPens();

    /**
     * update the path used for selection detection
     */
    void updateShapePath(QPainterPath& path);


    void updateShapePath();

    /**
     * @brief add a marker shape to the drawing
     * @param painter the QPainter used
     * @param selectedShape the marker shape to add
     * @param x the position in x
     * @param y the position in y
     */
    void drawMarkerShape(QPainter *painter, MarkerShape selectedShape, int x, int y);

    void drawMarkers(QPainter *painter);

    /**
     * @brief update the object with a new isntance
     * @note internal use only
     */
    void setObject(Object2D* obj)
    {
        _object = obj;
    }

    /** @} */


protected:
    /**
     * the Viewer2D on which the drawable object will be drawn
     */
    Viewer2D *_viewer;

    /** colour of an unselected segment, default is blue
  */
    QColor _unselcol;

    /** colour of a selected segment, default is yellow
  */
    QColor _selcol;

    /** colour of a selected marker, default is yellow
  */
    QColor _selmarkcol;

    /** the 2D object
  */
    osg::ref_ptr<Object2D> _object;

    /** marker size
  */
    int _size;

    /** distance between the current marker position and mouse position
      in x (horizontally) and y (vertically)
  */
    double ex, ey;

    /** linewidth
  */
    int _linewidth;

    /**
     * the QPainterPath containting the current shape of the object. Used for checking if an object is under the mouse when clicked
     */
    QPainterPath *_currentShapePath;

    /**
     * vector containing the QPen's for drawing ([0]: selected object, [1]: unselected object, [2]: marker selected)
     */
    std::vector<QPen> _penList;


private:
    /** \name Private members */
    /** @{ */

    /** helper for load operation: reading the header for an object
      This header has the following format
      Type Name Color NumberOfPoints
      where
      - Type is one character, corresponding to a valid #ObjectType
      - Name is a string containing the object name
      - Color is color code as accepted by the QColor constructor (either the color name or \#RRGGBB)
      - NumberOfPoints is a positive integer giving the number of control points to read for this object
   */
    static void readHeader(std::istream &s, std::string &type, std::string &name, QColor &c, unsigned int &np);

    /** helper for load operation: reading a control point as a #Marker2D
      the format follows casual #Marker2D reading as in ListMarker2DRead(), that is
      label x y (on a single line if possible but this is not required
  */
    static void readMarker2D(std::istream &s, Marker2D &m)
    {
        double x, y;
        float label;
        s >> label;
        s >> x;
        s >> y;
        if (!s.good())
            throw DrawableObject2D::IOError("DrawableObject2D::load failed: could not read marker");
        m.setX(x);
        m.setY(y);
        m.setLabel(label);
    }

    /** @} */

private:

    /** pointing precision
  */
    int precision;

    /** shape of a marker when object is selected
  */
    MarkerShape selshape;

    /** shape of an unselected marker
  */
    MarkerShape unselshape;

    /** shape of a selected marker
  */
    MarkerShape selmarkshape;

    /** displaying variable
  */
    bool _display;

    /** edition variable
  */
    bool _edit;

    /** selection variable
  */
    bool _select;

    /** type of this object
  */
    char _type;

    /** map for registering the DrawableObject2D subclasses (for object serializing)
     */
    static std::map<std::string, DrawableObject2DFactory*> _factories;
};

/** Overload of the output operator for the DrawableObject2D class
\n All DrawableObject2D can be written in a stream thanks to this method.
\li Exemple of use : save a DrawableObject2D (object) in a file (savefile.txt)
\n std::ofstream out("savefile.txt", std::ios::out);
\n out << object2D; \n
\li Exemple of use : print out a DrawableObject2D (drawableObject2D) on the standard output
\n std::cout << drawableObject2D; \n \n
\sa source code of void ListDrawableObject2D::save(char *fname)
*/
PoLAR_EXPORT std::ostream& operator<<(std::ostream& out, const DrawableObject2D& object2D);

}


#endif // _POLAR_DRAWABLEOBJECT2D_H_
