#ifndef BULLETSOFTOBJECT3D_H
#define BULLETSOFTOBJECT3D_H

#include"BulletObject3D.h"
#include "Util.h"
#include <BulletSoftBody/btSoftBody.h>
#include <osgUtil/SmoothingVisitor>
#include <list>

namespace PoLAR
{

/**
 * Specialization of Object3D to represent soft deformable objects with collisions using the Bullet physics engine
 */
class PoLAR_EXPORT BulletSoftObject3D: public BulletObject3D
{

public:
    /**
     * @brief Constructor (for a soft body)
     * @param node pointer to the osg::Node containing the visual object
     * @param mass the mass to set to the physical representation of the object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    BulletSoftObject3D(osg::Node *node, float mass = 1.0f, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief Constructor (for a soft body)
     * @param filename source file of the object to load
     * @param mass the mass to set to the physical representation of the object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    BulletSoftObject3D(const std::string& filename, float mass = 1.0f, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief Constructor (for a soft body)
     * @param node pointer to the osg::Node containing the visual object
     * @param softBody the soft body to attach to this object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    BulletSoftObject3D(osg::Node *node, btSoftBody *softBody, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief Constructor (for a soft body)
     * @param softBody the soft body to attach to this object
     * @param material boolean to specify if this object must have a default material or not \n(if a material is already assigned to this object, it will be used and not overriden)
     * @param editable boolean to specify if this object can be dynamically modified or not
     * @param pickable boolean to specify if this object can be picked or not
     * @note object appearence (color, shininess, transparency) is not involved by the editable state of this object, if editable is set to false this object's appearence and its display state keep modifiable, if editable is set to true the position, orientation and size of this object can also be modified
     */
    BulletSoftObject3D(btSoftBody *softBody, bool material = false, bool editable = false, bool pickable = false);

    /**
     * @brief get the btSoftBody representing this object in thze physics Bullet world
     * @return the Bullet soft body associated with this object
     */
    btSoftBody *getBtSoftBody();


    /**
     * @brief create the btSoftBody associated with this Object3D with a default collision shape
     */
    void createSoftBody();

    /**
     * @brief set a custom soft body instead of creating a default one
     * @param softbody the soft body to set to this object
     */
    void setBtSoftBody(btSoftBody *softbody);


    /**
     * @brief set the list of vertices which will be static
     * @param vertList the list of static vertices
     */
    void setStaticVertices(std::list<int>& vertList);

    /**
     * @brief reset all the simulation, that means the object will be replaced to its original place.
     */
    void resetSimulation();

    /**
     * @brief overwriting of the scale method from PoLAR::Object3D due to the limitations of the soft bodies
     * @param s the scaling to apply to the 3 directions
     */
    void scale(double s);

    /**
     * @brief overwriting of the scale method from PoLAR::Object3D due to the limitations of the soft bodies
     * @param sx the scale in x
     * @param sy the scale in y
     * @param sz the scale in z
     */
    void scale(double sx, double sy, double sz);

    /**
     * @brief overwriting of the translate method from PoLAR::Object3D due to the limitations of the soft bodies
     * @param tx the translation in x
     * @param ty the translation in y
     * @param tz the translation in z
     */
    void translate(double tx, double ty, double tz);

    /**
     * @brief overwriting of the rotate method from PoLAR::Object3D due to the limitations of the soft bodies
     @param angle angle of the rotation
     @param x x component of the vector used for rotation axis
     @param y y component of the vector used for rotation axis
     @param z z component of the vector used for rotation axis
     */
    void rotate(double angle, double x, double y, double z);

    /**
     * @brief overwriting of the rotate method from PoLAR::Object3D due to the limitations of the soft bodies
     * @param q quaternion representing the rotation
     */
    void rotate(osg::Quat q);

    /**
     * @brief create a BulletSoftObject3D from the soft body given in parameter
     * @param softBody the soft body from which to extract the vertices and faces
     * @param generateTexCoord is true, default texture coordinates will be computed (works well for planes)
     * @return the object newly created
     */
    static BulletSoftObject3D *fromBtSoftBody(btSoftBody *softBody, bool generateTexCoord=false);

    /**
     * @brief create a osg::Node from the soft body given in parameter
     * @param softBody the soft body from which to extract the vertices and faces
     * @return the node newly created
     */
    static osg::Node *nodeFromBtSoftBody(btSoftBody *softBody);

protected:

    /**
      * Destructor
      */
    ~BulletSoftObject3D();

    /**
     * @brief find the closest vertex of the given point in the node array
     * @param pos the given point
     * @param nodes the node array in which to search
     * @return the index of the closest vertex in the node array
     */
    int getClosestVertex(btVector3 pos, btSoftBody::tNodeArray& nodes) const;

    /**
     * @brief computes a matching table between OSG vertices and Bullet vertices
     */
    void osgToBulletIndices();

    /**
     * @brief applies the current transformation matrix to the soft body
     */
    void applyTransform();

    /**
     * @brief This callback updates the OSG mesh everytime the Bullet representation of the object has changed
     */
    struct MeshUpdater;


    /**
     * the tranformation matrix to apply to the soft body
     * @note replaces the transformation matrix applied to the transform node in rigid bodies
     */
    btTransform _transform;

    /**
     *  the scale vector to apply to the soft body
     * @note replaces the scaling set to the transformation matrix applied to the transform node in rigid bodies
     */
    btVector3 _scale;

    /**
     * array of equivalency between OSG vertex indices and Bullet indices
     */
    unsigned int *_indexInBullet;

    /**
     * array of equivalency between Bullet vertex indices and OSG indices
     */
    unsigned int *_indexInOsg;

};

}

#endif // BULLETSOFTOBJECT3D_H
