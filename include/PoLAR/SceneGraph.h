/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file SceneGraph.h
\brief Encapsulation of OpenSceneGraph scene graph

\author Pierre-Jean Petitprez
\date 2015
\note 
Corresponding code: SceneGraph/SceneGraph.cpp
*/
/** @} */

#ifndef _POLAR_SCENEGRAPH_H_
#define _POLAR_SCENEGRAPH_H_

#include "export.h"
#include "Light.h"

#include <osg/Node>
#include <osg/Group>
#include <osg/StateSet>
#include <osg/BlendEquation>
#include <osg/BlendFunc>
#include <osg/BlendColor>
#include <osg/Material>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/ColorMask>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/LineWidth>
#include <osgManipulator/Selection>
#include <osg/TexEnvCombine>
#include <osg/TexEnv>
#include <osg/TexGenNode>
#include <osg/PolygonOffset>
#include <osg/CullFace>
#include <osgShadow/ShadowTechnique>
#include <osgShadow/ShadowedScene>
#include <list>


namespace PoLAR
{

class Object3D;

/** \brief Encapsulation of OpenSceneGraph scene graph

At instantiation a base structure is created which permits nodes and light sources addition.
Constructor should not be called explicitly, it is already called in Viewer2D3D constructor.

*/


class PoLAR_EXPORT SceneGraph : public QObject, public osg::Referenced
{

    Q_OBJECT

public :

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param withShadows boolean to specify if this scene graph has to manage shadowing
    \note creates an empty scene and a lights group \n
       both are children of the root node of this scene graph
  */
    SceneGraph( bool withShadows = false);

    /**
     * set a scene to this scenegraph, for example for sharing scenes between different scenegraphs
     * @param scene the scene to set
     * @return true if success, false otherwise
     */
    bool setScene(osg::Node* scene);

    /**
     * set a group of objects to this scenegraph, for example for sharing objects between different scenegraphs
     * @param node the group of objects to set
     * @return true if success, false otherwise
     */
    bool setShadowedObjectsNode(osg::Node* node);

    /**
     * set a group of phantom objects to this scenegraph, for example for sharing objects between different scenegraphs
     * @param node the group of phantom objects to set
     * @return true if success, false otherwise
     */
    bool setPhantomObjectsNode(osg::Node* node);

    /**
     * set a group of lights to this scenegraph, for example for sharing lights between different scenegraphs
     * @param node the group of lights to set
     * @return true if success, false otherwise
     */
    bool setLightGroup(osg::Node* node);

    /**
     * activate shadows on this scenegraph
     */
    void setShadowsOn();

    /**
     * deactivate shadows on this scenegraph
     */
    void setShadowsOff();

    /** get the root node of this scene graph
      \return a pointer to the root node of this scene graph
  */
    inline osg::Group* getSceneGraph() const {return _root.get();}

    /** get the scene node of this scene graph
      \return a pointer to the scene node of this scene graph
  */
    inline osg::Group* getScene() const {return _scene.get();}

    /** get the phantomshader node of this scene graph
      \return a pointer to the phantomshader node of this scene graph
  */
    inline osg::Group* getPhantomObjectsNode() const {return _phantomObjectsGroup.get();}

    /** get the shadowed scene of this scene graph
      \return a pointer to the shadowed scene of this scene graph
  */
#ifdef USE_OSG_SHADOWS
    inline osgShadow::ShadowedScene* getShadowedScene() const {return _shadowedScene.get();}
#else
    inline osg::Group* getShadowedScene() const {return _shadowedScene.get();}
#endif

    /** get the group of shadowed objects of this scene graph
      \return a pointer to the group of shadowed objects of this scene graph
  */
    inline osg::Group* getShadowedObjectsNode() const {return _shadowedObjectsGroup.get();}

    /** get the global stateset attached to the scene
      \return a pointer to the global stateset attached to the scene
  */
    inline osg::StateSet* getGraphStateSet() const {return _graphStateSet.get();}


#ifdef USE_OSG_SHADOWS
    /**
     * @brief set a custom shadow technique to replace the default soft shadow map
     * @param technique the technique to set
     */
    void setShadowTechnique(osgShadow::ShadowTechnique* technique);
#endif

    /** add a node to this scene graph
      \param object node to add to this scene graph, it will be added as a child of the scene node
      \return true if the node given in parameter was added successfully to this scene graph else false
  */
    bool addObject(osg::Node* object);

    /** remove a node of this scene graph
      \param object node to remove of this scene graph
      \return true if the node given in parameter was removed successfully from this scene graph else false
  */
    bool removeObject(osg::Node* object);

    /** replace a node in this scene graph
      \param origObject node to remove from this scene graph, it will be replaced by newObject
      \param newObject node to add to this scene graph, it will replace origObject
      \return true if the replacement was successfull else false
  */
    bool replaceObject(osg::Node *origObject, osg::Node *newObject);

    /**
     * switch an object from the normal objects group to the phantom objects group
     * @param object the object to change
     * @return true is success, false otherwise
     */
    bool switchToPhantom(Object3D* object);

    /**
     * switch an object from the phantom objects group to the normal objects group
     * @param object the object to change
     * @return true is success, false otherwise
     */
    bool switchFromPhantom(Object3D* object);

    /** get a node
    \param index of the node in the scene's children (aka position of addition in the scene graph, goes from 0 to number of scene's children -1)
    \return the searched node if the index given in parameter is correct, NULL otherwise
  */
    osg::Node* getObject(int index) const;

    /** get the number of nodes in the scene, ONLY direct children are counted (to count all the nodes in the graph would be irrelevant for our use)
    \return the number of direct children of the group of 3D objects
    \note phantom objects are not included
  */
    unsigned int getNumObjects() const;

    /** get the index number of a node in the scene, ONLY usable (with expected result) with direct children
    \param object to get the index
    \return the index (goes from 0 to the number of direct children of the scene group) of a direct child of the scene group if found, else -1
  */
    int getObjectIndex(osg::Node* object) const;

    /** get a phantom object
    \param index of the node in the scene's children (aka position of addition in the scene graph, goes from 0 to number of scene's children -1)
    \return the search phantom object if the index given in parameter is correct, NULL otherwise
  */
    osg::Node* getPhantomObject(int index) const;

    /** get the number of phantom objects in the scene, ONLY direct children are counted (to count all the nodes in the graph would be irrelevant for our use)
    \return the number of direct children of the group of 3D phantom objects
    \note only phantom objects are included
  */
    unsigned int getNumPhantomObjects() const;

    /** get the index number of a phantom object in the scene, ONLY usable (with expected result) with direct children
    \param object to get the index
    \return the index (goes from 0 to the number of direct children of the scene group) of a direct child of the scene group if found, else -1
  */
    int getPhantomObjectIndex(osg::Node* object) const;

    /** get the number of lights attached to this scene graph
       \return the number of lights attached to this scene graph
  */
    inline int getNumLights() const {return _numLights;}

    /** get the root node of the lights group
       \return a pointer to the root node of the lights group
  */
    inline osg::Group* getLightGroup() const {return _lightGroup.get();}


    /** add a light source to this scene graph
      \param lightSource light source to add
      \return the OpenGL number of the light source
  */
    int addLight(Light *lightSource);


    /** get a light
      \param lightNum the number of the light to get
      \return the light to get
  */

    Light* getLight(int lightNum);

    /** get a light number
      \param lightSource the light source to get the number
      \return the light source number to get
  */
    int getLightNumber(Light *lightSource);


    /** remove a light source
      \param lightNum the number of the light source to remove
  */
    bool removeLight(int lightNum);

    /** update position of a viewable light source
      \param lightNum the number of the light source whose 3D representation's position has to be updated
  */
    void updateViewableLight(int lightNum);


    /** check if this scene graph manages shadowing
  */
    inline bool withShadows() { return _withShadows; }


    /** set an Object3D as no shadow receiver
      \param obj the Object3D to set
  */
    bool setIsNotAShadowReceiver(PoLAR::Object3D* obj);

    /** set an Object3D as shadow receiver
      \param obj the Object3D to set
  */
    bool setIsAShadowReceiver(PoLAR::Object3D* obj);

    /** set an Object3D as no shadow caster
      \param obj the Object3D to set
  */
    bool setIsNotAShadowCaster(PoLAR::Object3D* obj);

    /** set an Object3D as shadow caster
      \param obj the Object3D to set
  */
    bool setIsAShadowCaster(PoLAR::Object3D* obj);

    /**
     * this method creates uniform variables to be used in GLSL shaders for light #id
     */
    void addLightUniforms(unsigned int id);

    /**
     * this method removes uniform variables for light #id
     */
    void removeLightUniforms(unsigned int id);

    /**
     * this method updates uniform variables relative to light #id
     */
    void updateLightUniforms(unsigned int id);

    /**
     * this method set uniform variables relative to light #id to 0.0 to simulate a Off state
     */
    void setOffLightUniforms(unsigned int id);

    /** @} */



protected :

    /** destructor
   */
    virtual ~SceneGraph();

#ifdef USE_OSG_SHADOWS
    /**
     * create the default shadow technique (soft shadow map)
     */
    void createShadowTechnique();
#endif

    /**
     * create the group managing the phantom objects
     */
    osg::Group * createPhantomGroup();

    /** create and add the 3D representation of a light source to this scene graph
      \param lightNum the number of the light source
      \return true if the 3D representation was added successfully to this scene graph else false
  */
    bool addViewableLight(int lightNum);

    /**
     * @brief set the light passed in parameter as the light casting shadows (see osgShadow::ShadowMap)
     */
    void setShadowLight(Light* lightSource);

    /**
     * this method creates uniform variables to be used in GLSL shaders (lights specifications)
     */
    void addLightUniforms(osg::StateSet* stateSet);

    /**
     * this method updates uniform variables to be used in GLSL shaders (lights specifications)
     */
    void updateLightUniforms(osg::StateSet* stateSet);

    /**
     * this method update uniform variables relative to light #id for the given stateset
     */
    void updateLightUniforms(unsigned int id, osg::StateSet *stateSet);

    /**
     * this method creates uniform variables to be used in GLSL shaders for light #id on the given stateset
     */
    void addLightUniforms(unsigned int id, osg::StateSet* stateSet);

    /**
     * this method removes uniform variables for light #id on the given stateset
     */
    void removeLightUniforms(unsigned int id, osg::StateSet* stateSet);

    /**
     * this method updates the uniform variable counting the number of lights in the scene
     */
    void updateNumLightsUniform();

    /**
     * this method set uniform variables relative to light #id to 0.0 to simulate a Off state
     */
    void setOffLightUniforms(unsigned int id, osg::StateSet* stateSet);

    /**
     * returns a string corresponding to the name of light @param id in the GLSL shaders
     */
    std::string getLightUniformName(unsigned int id);

    /**
     * @brief find the first light number that is not used (between 0 and 7)
     * @param num the free light number will be stored in this reference
     * @return the iterator to the directly following position in the list of lights
     */
    std::list< osg::ref_ptr<Light> >::iterator findFirstFreeLightNum(int &num);

    /**
     * @brief find at which position in the lights list a new light must be inserted
     * @param lightNum light number of the light
     * @param exists if returned as true, the light number is already taken
     * @return iterator to the directly following position in the list of lights
     */
    std::list< osg::ref_ptr<Light> >::iterator findPositionInList(int lightNum, bool& exists);

    /**
     * @brief find the light corresponding to the provided light number
     * @param num the light number needed
     * @return the iterator to the light corresponding to the provided light number
     */
    std::list< osg::ref_ptr<Light> >::iterator findLight(int lightNum);

    /**
     * @brief find the first light which creates shadows in the list of lights
     * @return the iterator to the position of the light in the list of lights
     */
    std::list< osg::ref_ptr<Light> >::iterator findFirstShadowerLight();


private :

    /** pointer to the root node of this scene graph
     */
    osg::ref_ptr<osg::Group> _root;

    /** pointer to the scene node of this scene graph
     */
    osg::ref_ptr<osg::Group> _scene;

#ifdef USE_OSG_SHADOWS
    /** pointer to the shadow scene node
      */
    osg::ref_ptr<osgShadow::ShadowedScene> _shadowedScene;

    /** pointer to the shadow technique applied on the shadow scene
     */
    osg::ref_ptr<osgShadow::ShadowTechnique> _shadowTechnique;
#else

    /** pointer to the shadow scene node
      */
    osg::ref_ptr<osg::Group> _shadowedScene;
#endif

    /** pointer to the light group of this scene graph
      */
    osg::ref_ptr<osg::Group> _lightGroup;

    /** pointer to the shadowed objects node
     */
    osg::ref_ptr<osg::Group> _shadowedObjectsGroup;

    /** pointer to the phantom objects node
      */
    osg::ref_ptr<osg::Group> _phantomObjectsGroup;

    /** pointer to the global stateset attached to the scene
     */
    osg::ref_ptr<osg::StateSet> _graphStateSet;

    /** pointer to the phantomshader node stateset
      */
    osg::ref_ptr<osg::StateSet> _phantomGroupStateSet;

    /** number of lights
      */
    int _numLights;

    /** list of light sources added to this scenegraph
     */
    std::list< osg::ref_ptr<Light> > _lights;

    /** boolean to store shadowing management state of this scene graph
     */
    bool _withShadows;

    /**
     * pointer to the light that creates shadows
     */
    Light* _shadowerLight;

};

}

#endif // _POLAR_SCENEGRAPH_H_
