/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file DrawableMarkers2D.h
\brief Management of the display of \ref Markers2D

\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/DrawableMarkers2D.cpp
*/
/** @} */

#ifndef _POLAR_DRAWABLEMARKERS2D_H_
#define _POLAR_DRAWABLEMARKERS2D_H_

#include "Markers2D.h"
#include "DrawableObject2D.h"
#include <QtWidgets/QGraphicsItemGroup>


namespace PoLAR
{

/** \brief Management of the display of \ref Markers2D

*/


class PoLAR_EXPORT DrawableMarkers2D : public DrawableObject2D
{
    Q_OBJECT
public:
    
    
    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param list markers list
    \param Parent parent viewer
    \param unselCol colour of an unselected segment, default is blue
    \param selCol colour of a selected segment, default is yellow
    \param selmarkCol colour of a selected marker, default is yellow
    \param unselShape shape of an unselected marker
    \param selShape shape of a marker when object is selected
    \param selmarkShape shape of a selected marker
    \param Size size of a marker
    \param Precision pointing precision for selection
    \param LineWidth line width
  */
    DrawableMarkers2D(std::list<Marker2D *> list, Viewer2D *Parent,
                      QColor unselCol=defaultUnselCol,
                      QColor selCol=defaultSelCol,
                      QColor selmarkCol=defaultSelmarkCol,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize,  int Precision=defaultPrecision,
                      int LineWidth=defaultLineWidth);

    /** default constructor
    \param Parent parent viewer
    \param unselCol colour of an unselected segment, default is blue
    \param selCol colour of a selected segment, default is yellow
    \param selmarkCol colour of a selected marker, default is yellow
    \param unselShape shape of an unselected marker
    \param selShape shape of a marker when object is selected
    \param selmarkShape shape of a selected marker
    \param Size size of a marker
    \param Precision pointing precision for selection
    \param LineWidth line width
  */
    DrawableMarkers2D(Viewer2D *Parent=NULL,
                      QColor unselCol=defaultUnselCol,
                      QColor selCol=defaultSelCol,
                      QColor selmarkCol=defaultSelmarkCol,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision,
                      int LineWidth=defaultLineWidth);

    /** defautl constructor
    \param Parent parent viewer
    \param object the Object2D to bind to this graphics representation
    \param unselCol colour of an unselected segment, default is blue
    \param selCol colour of a selected segment, default is yellow
    \param selmarkCol colour of a selected marker, default is yellow
    \param unselShape shape of an unselected marker
    \param selShape shape of a marker when object is selected
    \param selmarkShape shape of a selected marker
    \param Size size of a marker
    \param Precision pointing precision for selection
    \param LineWidth line width
  */
    DrawableMarkers2D(Viewer2D *Parent, Markers2D *object,
                      QColor unselCol=defaultUnselCol,
                      QColor selCol=defaultSelCol,
                      QColor selmarkCol=defaultSelmarkCol,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision,
                      int LineWidth = defaultLineWidth);

    /**
     * constructor
     * @param list markers list contained by the 2D object
     * @param Parent parent viewer
     * @param unselectPen QPen for an unselected segment
     * @param selectPen QPen for a selected segment
     * @param selectMarkerPen QPen for a selected marker
     * @param unselShape shape of an unselected marker
     * @param selShape shape of a marker when object is selected
     * @param selmarkShape shape of a selected marker
     * @param Size size of a marker
     * @param Precision pointing precision for selection \n
     * if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
     */
    DrawableMarkers2D(std::list<Marker2D*> list, Viewer2D *Parent,
                      QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision);

    /**
     * constructor
     * @param Parent parent viewer
     * @param unselectPen QPen for an unselected segment
     * @param selectPen QPen for a selected segment
     * @param selectMarkerPen QPen for a selected marker
     * @param unselShape shape of an unselected marker
     * @param selShape shape of a marker when object is selected
     * @param selmarkShape shape of a selected marker
     * @param Size size of a marker
     * @param Precision pointing precision for selection \n
     * if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
     */
    DrawableMarkers2D(Viewer2D *Parent,
                      QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision);


    /**
     * constructor
     * @param Parent parent viewer
     * @param object the Object2D to bind to this graphics representation
     * @param unselectPen QPen for an unselected segment
     * @param selectPen QPen for a selected segment
     * @param selectMarkerPen QPen for a selected marker
     * @param unselShape shape of an unselected marker
     * @param selShape shape of a marker when object is selected
     * @param selmarkShape shape of a selected marker
     * @param Size size of a marker
     * @param Precision pointing precision for selection \n
     * if MarkerShape = NONE -> no marker will be displayed (useful for polygons)
     */
    DrawableMarkers2D(Viewer2D *Parent, Markers2D *object,
                      QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                      MarkerShape unselShape=defaultUnselShape,
                      MarkerShape selShape=defaultSelShape,
                      MarkerShape selmarkShape=defaultSelmarkShape,
                      int Size=defaultSize, int Precision=defaultPrecision);

    /**
     * constructor by deep copy
     * @param obj the object to copy
     * @param deepCopy if true, the link object2D will be copied. If false, the object2D pointer will point to the copied object's object2D
     */
    DrawableMarkers2D(DrawableMarkers2D &obj, bool deepCopy=true);

    /**
     * @brief swap implementation of the copy-and-swap idiom
     */
    void swap(DrawableMarkers2D &first, DrawableMarkers2D &second);

    /**
     * @brief returns the class name
     * @note used for serializing the class (for saving/loading)
     * @note this method must be overriden in subclasses
     */
    virtual const char* className() const
    {
        return "DrawableMarkers2D";
    }

    /**
   * Redefinition of the virtual function to draw the object
   */
    virtual void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);

    /**
     * @brief Returns a copy of this object
     * @param deepCopy if true, the link object2D will be copied. If false, the object2D pointer will point to the copied object's object2D
     * @return a pointer to a new DrawableObject2D with the same specs than this object
     */
    DrawableMarkers2D *clone(bool deepCopy=true)
    {
        return new DrawableMarkers2D(*this, deepCopy);
    }

    /**
     * @brief Redefinition of the assignment operator
     */
    DrawableMarkers2D& operator=(DrawableMarkers2D obj);

    /** @} */

};

}

#endif // _POLAR_DRAWABLEMARKERS2D_H_
