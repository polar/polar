/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file ListDrawableObject2D.h
\brief Management of lists of \ref DrawableObject2D

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2015
\note 
Corresponding code: Object2D/ListDrawableObject2D.cpp
*/
/** @} */

#ifndef _POLAR_LISTDRAWABLEOBJECT2D_H_
#define _POLAR_LISTDRAWABLEOBJECT2D_H_

#include "DrawableObject2D.h"
#include "GraphicsScene.h"

namespace PoLAR
{

class Viewer2D;
class GraphicsScene;

/** \brief Management of lists of \ref DrawableObject2D
 *
 */
class PoLAR_EXPORT ListDrawableObject2D : public QObject
{
    Q_OBJECT
    
public:

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param Parent parent widget
    */
    ListDrawableObject2D(Viewer2D *Parent);

    /** Destructor
    */
    ~ListDrawableObject2D();

    /** activate the closest object to point (x,y) if distance is lower than precision
    \param x x coordinate of the point
    \param y y coordinate of the point
    \note deprecated function. See Viewer2D::viewportEvent.
    \note emits updateSignal()
    */
    void selectObject(float x, float y);

    /**
     * activate the object found from mouse click (see Viewer2D::viewportEvent)
     * @param object the object to activate
     * @note emits updateSignal()
     */
    void selectObject(DrawableObject2D *object);

    /**
      general function to add an object
    */
    void addObject(DrawableObject2D *obj);

    /** specific function with Marker2D list
    */
    DrawableObject2D *addMarkers(std::list<Marker2D*> listmarker);

    /**
     * specific function
     */
    DrawableObject2D* addMarkers();

    /** specific function with Marker2D list
    */
    DrawableObject2D *addPolygon(std::list<Marker2D*> listmarker);

    /**
     * specific function
     */
    DrawableObject2D* addPolygon();


    /** specific function with Marker2D list
    */
    DrawableObject2D* addPolyLine(std::list<Marker2D*> listmarker);

    /**
     * specific function
     */
    DrawableObject2D* addPolyLine();

    /** specific function with Marker2D list
    */
    DrawableObject2D* addSpline(std::list<Marker2D*> listmarker);

    /** specific function
    */
    DrawableObject2D* addSpline();

    /** specific function with Marker2D list
    */
    DrawableObject2D* addBlob(std::list<Marker2D*> listmarker);

    /**
     * specific function
     */
    DrawableObject2D* addBlob();

    /** specific function with Marker2D list
     */
    DrawableObject2D* addArrows(std::list<Marker2D *> listmarker);

    /**
     * specific function
     */
    DrawableObject2D* addArrows();

    /** specific function with Marker2D list
     */
    DrawableObject2D* addText(std::list<Marker2D *> listmarker, QString& text);

    /**
     * specific function
     */
    DrawableObject2D* addText(QString& text);

    /** count the number of objects in the list of 2D objects
    */
    int countObjects() { return _list.size();}

    /**
     * save the 2D objects of this list into a SVG file
     * @param fname
     */
    void saveAsSvg(QString& fname);

    void saveAsSvg(const char* fname)
    {
        QString str(fname);
        saveAsSvg(str);
    }

    /** save markers for current object (if any) in a file
    \param fname name of the file to write in
    */
    void save(const char *fname) const;

    /** load a list of \ref DrawableObject2D from a stream
      \param in the stream to read from
      \return true if everything went right, else false.
      \note the format of the loaded file must be the following : \n \n
      \b Type_of_the_object1 Space \b Name_of_the_object1 Space \b Color_of_the_object1 Space \b Number_of_control_points Return \n
      \b Label_of_the_point1 Space \b Abscissa_of_the_point1 Space \b Ordinate_of_the_point1 Return \n
      \b Label_of_the_point2 Space \b Abscissa_of_the_point2 Space \b Ordinate_of_the_point2 Return \n
      .. etc \n
      \b Type_of_the_object2 Space \b Name_of_the_object2 Space \b Color_of_the_object2 Return \n
      \b Label_of_the_point1 Space \b Abscissa_of_the_point1 Space \b Ordinate_of_the_point1 Return \n
      \b Label_of_the_point2 Space \b Abscissa_of_the_point2 Space \b Ordinate_of_the_point2 Return \n
      .. etc
      
      \n \b Type_of_the_object is a character and must be chosen among these possibilities : \n
      'M' for a DrawableMarkers2D \n
      'A' for a DrawableArrows2D \n
      'B' for a DrawableBlob2D \n
      'P' for a DrawablePolygon2D \n
      'L' for a DrawablePolyLine2D \n
      'S' for a DrawableSpline2D \n
      'T' for a DrawableText2D \n
      \n \b Name_of_the_object is a string (CAUTION : the string must not contain any separator)
      \n \b Color_of_the_object is a string that Qt accepts as valid for a color (see Qt doc on QColor class for more info): either a color name or a string beginning with a '#' character and where each composant (r,g,b) of the color is represented as a hexadecimal number (check here for correspondance : http://www.easycalculation.com/rgb-coder.php )
      \n \b Label_of_the_point , \b Abscissa_of_the_point and \b Ordinate_of_the_point are floats (CAUTION : a label has to be set for every point, if you don't use any, you can set it to 0)
      \note In case of error, a DrawableObject2D::IOError exception is raised and false is return
      \n Exemple : you want to load a blue DrawableSpline2D with 5 points, the loadfile has to be written as following :\n  \n
      S DrawableSpline2D #0000FF 5\n
      0 176 235 \n
      0 313 216 \n
      0 323 318 \n
      0 256 382 \n
      0 185 313 \n
      Or, for a less absolute blue, you may use\n
      S DrawableSpline2D blue 5\n
      0 176 235 \n
      0 313 216 \n
      0 323 318 \n
      0 256 382 \n
      0 185 313 \n
    */
    bool load(std::istream &in);

    /** load a list of \ref DrawableObject2D from a file
      \param fname the name of the file to read from
      \note opens a stream and calls load() with this stream
    */
    bool load(const char *fname);

    /** sets currently selected marker (if any) to l
    \param l the label to set
    */
    void changeMarkerLabel(float l);

    /** select the current object (connects objects slots...)
    \note can emit selectObjectSignal(DrawableObject2D *obj)
    */
    void activateCurrentObject();

    /** remove current object from the list
    \note emits updateSignal()
    */
    void removeCurrentObject();

    /** display/undisplay current object (true or false)
    \note can emit updateSignal()
    */
    void setCurrentObjectDisplay(bool b);

    /** sets current object editable/uneditable (true or false)
    */
    void setCurrentObjectEdition(bool b);

    /** start marker edition mouse mode
    \note emits updateSignal()
    */
    void startEditMarkers();

    /** stop marker edition mouse mode
    \note emits updateSignal()
    */
    void stopEditMarkers();

    /**
     * returns true if markers edition is active, false otherwise
     */
    bool markersAreActive()
    {
        return _editMarkers;
    }

    /** get the current DrawableObject2D (the last added or the selected one)
    \return the current drawable object2D
    */
    DrawableObject2D * getCurrentObject() {return _currentObject;}

    /** return a list of DrawableObject2D* selected by name
    \param name the name of the DrawableObject2D to get
    \return the list computed, if no DrawableObject2D are found the list returned is empty
    */
    std::list<DrawableObject2D*> getObjectsByName(const char* name) const;

    /** return a list of DrawableObject2D* selected by type
    \param type the type of the DrawableObject2D to get
    \return the list computed, if no DrawableObject2D are found the list returned is empty
    */
    std::list<DrawableObject2D*> getObjectsByType(char type) const;

    std::list<DrawableObject2D*> getObjectsByType(const char* type) const;

    /** return a DrawableObject2D* thanks to his index in the \ref list
    \param index of the DrawableObject2D to get
    \return the founded DrawableObject2D, if the index is not pertinent return a NULL DrawableObject2D
    */
    DrawableObject2D* getObjectByIndex(unsigned int index) const;

    /** return the list of all DrawableObject2D*
    */
    std::list<DrawableObject2D*> getObjects() const {return _list;}


    /** get the pointing precision
     */
    int getPrecision() const { return _precision;}

    /**
     * set this list as the current drawn one if true, set it as not currently drawn if false
     */
    void setCurrent(bool current=true)
    {
        _isCurrent = current;
    }


    /** @} */

signals:

    /** \name Signals */
    /** @{ */

    /** this signal is emitted when the object needs to be (re)painted
  */
    void updateSignal();

    /** signal to manage 2D objects selection
  */
    void selectObjectSignal(DrawableObject2D *obj);

    /** signal to manage 2D objects unselection
  */
    void unselectObjectSignal();

    /** @} */


public slots:

    /** \name Public Slots */
    /** @{ */

    /** unselect current object
  */
    void deactivateCurrentObject();


    /** remove an obsolete object
  */
    void removeDeadObject(QObject *obj);

    /** remove all objects from the list
   */
    void clear();

    /** set the pointing precision
   */
    void setPrecision(int p) { if (p > 0) _precision = p;}

    /**
     * called when the index of the image sequence has changed.
     * Useful for updating the 2D objects drawn on screen
     */
    void imageIndexChanged(int index);

    /** @} */

protected:


    /** the list of drawable 2D objects
  */
    std::list<DrawableObject2D*> _list;

    /** the current drawable 2D object
  */
    DrawableObject2D *_currentObject;

    /** pointing precision for object selection
   */
    int _precision;

    /**
     * Boolean to know if we are editing the markers or not
     */
    bool _editMarkers;

    /**
     * the parent Viewer
     */
    Viewer2D *_viewer;

    /**
     * the QGraphicsScene on which the 2D items are drawn
     */
    GraphicsScene *_scene;

    /**
     * if true, this list is the current list drawn on screen. If false, this list is not drawn
     */
    bool _isCurrent;

private:

    /** get the number of editable elements in the \ref list
    \return the number of editable elements in the \ref list
  */
    int _countEditable() const
    {
        int countEdit=0;
        std::list<DrawableObject2D*>::const_iterator itrL;
        for (itrL=_list.begin(); itrL != _list.end(); ++itrL)
            if ((*itrL)->isEditable()) countEdit++;
        return countEdit;
    }

    /** get the number of displayed elements in the \ref list
    \return the number of displayed elements in the \ref list
  */
    int _countDisplayed() const
    {
        int countDisp=0;
        std::list<DrawableObject2D*>::const_iterator itrL;
        for (itrL=_list.begin(); itrL != _list.end(); ++itrL)
            if ((*itrL)->isDisplayed()) countDisp++;
        return countDisp;
    }

    /** unselect a 2D object
    \note emits unselectObjectSignal()
  */
    void unselectObject();


    /**
     * Update the z values of the list of objects when an object is deleted or a new one is created.
     * This ensures the last created object is always drawn on top of the others
     */
    void updateZValues();

};

}

#endif //_POLAR_LISTDRAWABLEOBJECT2D_H_
