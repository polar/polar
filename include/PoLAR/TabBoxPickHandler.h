/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file TabBoxPickHandler.h
\brief TabBox typed pick handler of an Object3D

\author Frédéric Speisser
\date year 2008
\note 
Corresponding code: SceneGraph/TabBoxPickHandler.cpp
*/
/** @} */

#ifndef _POLAR_TABBOXPICKHANDLER_H_
#define _POLAR_TABBOXPICKHANDLER_H_

#include "Object3DPickHandler.h"


namespace PoLAR
{

class Viewer2D3D;

/** \brief TabBox typed pick handler of an Object3D

For general information of use take a look at \ref Object3DPickHandler class

The tabbox pick handler provides a dragger with which you can scale/stretch/translate the picked object thanks to a bounding box

*/


class PoLAR_EXPORT TabBoxPickHandler : public PoLAR::Object3DPickHandler
{

public:

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param displayInfo true to display informations about the objets pointed by the mouse else false
  */
    TabBoxPickHandler(bool displayInfo=true) : Object3DPickHandler(displayInfo) {}


protected:

    /** create the dragger */
    virtual osgManipulator::Dragger* createDragger();

    /** @} */

};

}

#endif // _POLAR_TABBOXPICKHANDLER_H_
