/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file PanZoom.h
\brief Management of the display of the background image or image sequence

\author Erwan Kerrien, Pierre-Jean Petitprez
\date 2007, 2014
\note 
Corresponding code: Image/PanZoom.cpp
*/
/** @} */


#ifndef _POLAR_PANZOOM_H_
#define _POLAR_PANZOOM_H_

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif
#include <QtGui/QMouseEvent>
#include <QtCore/QObject>
#include <osg/Camera>
#include "export.h"
#include "Util.h"

namespace PoLAR
{

class BaseViewer;


/** \brief Management of the display of the background image or image sequence

*/
class PoLAR_EXPORT PanZoom: public QObject, public osg::Referenced
{
    Q_OBJECT
public:

    /**
     * choice of zoom mode : by dragging the mouse when a button is pressed, by scrolling with the mouse wheel, or using finger pinch (mobile platforms)
     */
    enum ZoomMode {DRAG, SCROLL, PINCH};

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param v viewer in which background data has to be displayed
  */
    PanZoom(BaseViewer &v);

    /** get the camera which is computing the background data projection
    \return the camera which is computing the background data projection
  */
    osg::observer_ptr<osg::Camera> getCamera() {recomputeCamera(); return _camera.get();}

    /** get the computed projection
    \return the computed projection
  */
    osg::Matrixd getProjection() {return _camera->getProjectionMatrix();}

    /** get the image horizontal pan
    \return the image horizontal pan
  */
    float getPanX() const {return _dx;}

    /** get the image vertical pan
    \return the image vertical pan
  */
    float getPanY() const {return _dy;}

    /** get the image zoom
    \return the image zoom
  */
    float getZoom() const {return _zoom;}

    /** set the image zoom
    \param Zoom zoom to set
  */
    void setZoom(float zoom) {_zoom=zoom; update();}

    /** set the image pan
    \param Dx pan along x axis
    \param Dy pan along y axis
  */
    void setPan(float Dx, float Dy) {_dx=Dx, _dy=Dy; update();}

    /** resize image projection function of \param Width and \param Height
    \note only if the image size (in pixels), not the window size, has changed
  */
    void resize(int Width, int Height) {_width=Width, _height=Height; update();}

    /** recompute camera projection */
    void update();

    /** set intrinsic parameters (issued from 3D projection) of the projection
    \param im the intrinsic parameters
  */
    void setIntrinsics(osg::Matrixd im) {_intrinsicMat.set(im); update();}

    /**
    \return the intrinsic parameters of the projection
  */
    osg::Matrixd getIntrinsics() const {return _intrinsicMat;}

    /** translate mouse coordinates from the Qt frame to the image frame
  */
    void QtToImage(double xqt, double yqt, double &xima, double &yima);

    /** translate mouse coordinates from the image frame to the Qt frame
  */
    void ImageToQt(double xima, double yima, double &xqt, double &yqt);

    /**
     * @brief set the zoom mode
     * @param mode the mode to set between DRAG (drag the mouse with a mouse button clicked) and SCROLL (use the mouse wheel)
     * @param precision the precision of the zoom. Used only with the SCROLL mode.
     */
    void setZoomMode(ZoomMode mode, float precision = 1.0f)
    {
        _zoomMode = mode;
        _zoomPrecision = precision;
    }

    void setMousePos(double x, double y)
    {
        _mouseX = x;
        _mouseY = y;
    }

    /** @} */


public slots:

    /** \name Public Slots */
    /** @{ */

    /** manage the image zoom
    \note emits updateSignal()
     */
    void setZoomSlot(float Zoom) {setZoom(Zoom);}

    /** manage the image pan
    \note emits updateSignal()
     */
    void setPanSlot(float Dx, float Dy) {setPan(Dx,Dy);}

    /** manage the image resize
    \note emits updateSignal()
     */
    void resizeSlot(int Width, int Height) {resize(Width, Height);}

    /** manage the mouse press event
     * @note deprecated
     */
    void mousePressSlot( QMouseEvent* );

    /** manage the mouse release event
     * @note deprecated
     */
    void mouseReleaseSlot(QMouseEvent*);

    /** manage the mouse wheel event
     * @note deprecated
     */
    void mouseWheelSlot(QWheelEvent *event);

    /** manage the image zoom event
    */
    void mouseZoomDragSlot( QMouseEvent* event );

    /** manage the image zoom event
     */
    void mouseZoomScrollSlot(QWheelEvent* event);

    /** manage the image pan event
     */
    void mousePanSlot( QMouseEvent* event );

    /** manage the image pan event from already extracted coordinates
     */
    void mousePanSlot(double x, double y);

    /** manage the image zoom event from already extracted coordinates
     */
    void mouseZoomSlot(double, double y);

    /** @} */


signals:

    /** \name Signals */
    /** @{ */

    /** signal is emitted when repainting is necessary
  */
    void updateSignal();

    /** @} */


protected:

    /** destructor */
    virtual ~PanZoom();

    /** recompute camera projection */
    virtual void recomputeCamera();

private:

    /** pointer to the projection camera */
    osg::ref_ptr<osg::Camera> _camera;

    /** pointer to the master camera of the corresponding viewer */
    osg::Camera *_pcam;

    /** image zoom */
    float _zoom;

    /** image pan along x axis */
    double _dx;

    /** image pan along y axis */
    double _dy;

    /** width of the background image */
    int _width;

    /** height of the background image */
    int _height;

    /** last position of the mouse along x axis */
    int _mouseX;

    /** last position of the mouse along y axis */
    int _mouseY;

    /** intrinsic parameters */
    osg::Matrixd _intrinsicMat;

    /**
     * the zoom mode, either DRAG (drag the mouse with a mouse button clicked) or SCROLL (use the mouse wheel)
     */
    ZoomMode _zoomMode;

    /**
     * precision of the zoom (used only in SCROLL mode)
     */
    float _zoomPrecision;
};

}

#endif // _POLAR_PANZOOM_H_
