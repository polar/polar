/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file EpipolarPickHandler.h
\brief Epipolar typed pick handler of a geometry

\author Frédéric Speisser, Pierre-Jean Petitprez
\date year 2007
\note 
Corresponding code: SceneGraph/EpipolarPickHandler.cpp
*/
/** @} */

#ifndef _POLAR_EPIPOLARPICKHANDLER_H_
#define _POLAR_EPIPOLARPICKHANDLER_H_


#include "GeometryPickHandler.h"
#include "VertexDragger.h"

namespace PoLAR
{

class Viewer2D3D;

/** \brief Epipolar typed pick handler of a geometry


For general information of use take a look at \ref GeometryPickHandler class

The epipolar pick handler provides a dragger with which you can edit the picked vertex by translating it along the x,y axis of the video frame but, for z component, along the line going from this vertex to the projection of a chosen camera focal point (to be set with the method \ref setFocalPoint ). \n
To add several focal points to this epipolar handler use the method \ref addFocalPoint and to use one of them for z axis edition use the method \ref setFocalPoint with his corresponding index.
If you don't set a specific focal point to be used, the z edition is made along the line going from the picked vertex to the focal point of the camera used for the view in which the vertex was picked (by using this default behaviour with stereoviews this results in a translation along the correspondant epipolar line in all the other views). You can retrieve this default mode by using method \ref setFocalPoint with index 0 (the camera position of the view in which this epipolar handler is activ is stored at this place in the \ref _focalPointsArray )\n

*/


class PoLAR_EXPORT EpipolarPickHandler : public PoLAR::GeometryPickHandler
{
    Q_OBJECT

public:

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param displayInfo true to display informations about the objets pointed by the mouse else false
  */
    EpipolarPickHandler(bool displayInfo=true):
        GeometryPickHandler(displayInfo),
        _initialized(false)
    {
        _focalPointsArray = new osg::Vec3dArray;
        addFocalPoint(osg::Vec3d()); // this point will be replaced at first call of handle
    }

    /** handle events */
    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, osg::Object*, osg::NodeVisitor*)
    {
        return handle(ea, aa);
    }

    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

    /** add a focal point which will be able to be used for vertex edition along specific correspondant epipolar line,
      the focal point is stored in the \ref _focalPointsArray
      \param center the focal point to add (focal point coordinates must be expressed in WORLD FRAME)
      \return the index of the added focal point in the \ref _focalPointsArray
  */
    int addFocalPoint(osg::Vec3d center);

    /** get the focal point thanks to his index in the \ref _focalPointsArray */
    bool getFocalPoint(int index, osg::Vec3d &focalPoint);

    /** set the focal point, thanks to his index in the \ref _focalPointsArray, to be used for vertex edition
      \note the index of a focal point
  */
    void setFocalPoint(int index);

    /**
     * called when a new projection matrix is set to the viewer
     */
    void reinitializeCamera() { _initialized = false; }

    /** @} */


protected:

    /** create the dragger */
    virtual PoLAR::VertexDragger* createDragger(PoLAR::Object3D* object3D, osg::Vec3f &vertex, const osg::Matrixd &viewMatrix);


private:

    /** array of the coordinates of camera centers (in world frame) */
    osg::Vec3dArray* _focalPointsArray;

    /** current used focal point for edition */
    osg::Vec3d _currentFocalPoint;

    /**
     * boolean to know if the event handler has already been initialized of not
     */
    bool _initialized;
};


/** Class defining one component epipolar translation vertex dragger */
class PoLAR_EXPORT EpipolarTranslate1DVertexDragger : public PoLAR::Translate1DVertexDragger
{
public:

    EpipolarTranslate1DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix) : Translate1DVertexDragger(vertexArray, vertexIndex, startMotionMatrix) {}

    EpipolarTranslate1DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix, const osg::Vec3& s, const osg::Vec3& e) : Translate1DVertexDragger(vertexArray, vertexIndex, startMotionMatrix, s,e) {}

    /** Handle pick events on dragger and generate TranslateInLine commands. */
    virtual bool handle(const osgManipulator::PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

    // the two vertices must be given in the camera frame
    void updateLineProjector(osg::Vec3f camCenter, osg::Vec3f vertex) {_projector->setLine(camCenter, vertex);}

protected:

    virtual ~EpipolarTranslate1DVertexDragger() {}
    void translate(osgManipulator::TranslateInLineCommand *cmd, osg::Vec3d& projectedPoint);

};


/** Class defining epipolar vertex dragger */
class PoLAR_EXPORT EpipolarVertexDragger : public PoLAR::VertexDragger
{

public:

    EpipolarVertexDragger(PoLAR::Object3D* object3D, osg::Vec3f &vertex, const osg::Matrixd &viewMatrix, osg::Vec3d &focalPoint);
    virtual bool handle(const osgManipulator::PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us)
    {
        return handle(pi, ea, us, false);
    }

    virtual bool handle(const osgManipulator::PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us, bool shift);


protected:
    Translate1DVertexDragger* create1DVertexDragger(float scale, const osg::Matrixd& viewMatrix);

private:
    osg::Vec3f _currentFocalPoint;
};

}

#endif // _POLAR_EPIPOLARPICKHANDLER_H_
