/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef _POLAR_VERTEXDRAGGER_H
#define _POLAR_VERTEXDRAGGER_H

#include <osgManipulator/Translate1DDragger>
#include <osgManipulator/Translate2DDragger>

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif
#include "export.h"

#include "Object3D.h"

namespace PoLAR
{

/** Base class defining one component translation vertex dragger */
class PoLAR_EXPORT Translate1DVertexDragger : public osgManipulator::Translate1DDragger
{
public:

    Translate1DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix):
        Translate1DDragger(), _startMotionMatrix(startMotionMatrix), _vertexArray(vertexArray), _vertexIndex(vertexIndex)
    {}

    Translate1DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix, const osg::Vec3& s, const osg::Vec3& e):
        Translate1DDragger(s,e), _startMotionMatrix(startMotionMatrix), _vertexArray(vertexArray), _vertexIndex(vertexIndex)
    {}

    /** Handle pick events on dragger and generate TranslateInLine commands. */
    virtual bool handle(const osgManipulator::PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

protected:

    virtual ~Translate1DVertexDragger() {}

    virtual void translate(osgManipulator::TranslateInLineCommand *cmd, osg::Vec3d& projectedPoint) = 0;
    osg::Matrixd _localToWorld, _worldToLocal, _startMotionMatrix;
    osg::Vec3f _vertex;
    osg::Vec3Array* _vertexArray;
    int _vertexIndex;

};


/** Base class defining two components translation vertex dragger */
class PoLAR_EXPORT Translate2DVertexDragger : public osgManipulator::Translate2DDragger
{
public:

    Translate2DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix):
        Translate2DDragger(), _startMotionMatrix(startMotionMatrix), _vertexArray(vertexArray), _vertexIndex(vertexIndex)
    {}

    Translate2DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix, const osg::Plane& plane):
        Translate2DDragger(plane), _startMotionMatrix(startMotionMatrix), _vertexArray(vertexArray), _vertexIndex(vertexIndex)
    {}

    /** Handle pick events on dragger and generate TranslateInPlane commands. */
    virtual bool handle(const osgManipulator::PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);

protected:

    virtual ~Translate2DVertexDragger() {}

    osg::Matrixd _localToWorld, _worldToLocal, _startMotionMatrix;
    osg::Vec3f _vertex;
    osg::Vec3Array* _vertexArray;
    int _vertexIndex;
};



/** Base class defining vertex dragger */
class PoLAR_EXPORT VertexDragger : public osgManipulator::CompositeDragger
{

public:

    VertexDragger(PoLAR::Object3D* object3D, osg::Vec3f &vertex);

    virtual bool handle(const osgManipulator::PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, bool shift) = 0;

    virtual bool handle(const osgManipulator::PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
    {
        return handle(pi, ea, aa, false);
    }

    void setupDefaultGeometry();

    inline void setColor(const osg::Vec4& color) { if (_translate2DVertexDragger.valid()) _translate2DVertexDragger->setColor(color); }

    inline Translate1DVertexDragger* getTranslate1DVertexDragger() { return _translate1DVertexDragger.get(); }
    inline Translate2DVertexDragger* getTranslate2DVertexDragger() { return _translate2DVertexDragger.get(); }
    inline PoLAR::Object3D* getObjectEdited() {return _object3D;}
    inline bool stopEdition() {return _stopEdition;}
    osg::Vec3f getVertexEdited();

    void setUsingTranslate1DVertexDraggerMouseButton(int button)
    {
        _usingTranslate1DVertexDraggerMouseButton = button;
    }

    void setUsingTranslate2DVertexDraggerMouseButton(int button)
    {
        _usingTranslate2DVertexDraggerMouseButton = button;
    }

    void setVertexDeletionMouseButton(int button)
    {
        _vertexDeletionMouseButton = button;
    }


protected:

    virtual ~VertexDragger();
    void setupDraggers(const osg::Matrixd &viewMatrix);
    virtual Translate1DVertexDragger* create1DVertexDragger(float, const osg::Matrixd&) = 0;

    osg::ref_ptr<Translate1DVertexDragger> _translate1DVertexDragger;
    osg::ref_ptr<Translate2DVertexDragger>_translate2DVertexDragger;
    bool _usingTranslate1DVertexDragger;
    bool _usingTranslate2DVertexDragger;
    PoLAR::Object3D* _object3D;
    osg::Vec3f _vertex;
    std::pair<unsigned int, unsigned int> _vertexIndex; // first part : drawable index - second part : vertex index in the drawable's vertices list
    bool _displayListState, _stopEdition;
    int _usingTranslate1DVertexDraggerMouseButton;
    int _usingTranslate2DVertexDraggerMouseButton;
    int _vertexDeletionMouseButton;
};


/** Class defining one component default translation vertex dragger */
class DefaultTranslate1DVertexDragger : public PoLAR::Translate1DVertexDragger
{
public:

    DefaultTranslate1DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix) : Translate1DVertexDragger(vertexArray, vertexIndex, startMotionMatrix) {}

    DefaultTranslate1DVertexDragger(osg::Vec3Array* vertexArray, int vertexIndex, osg::Matrixd startMotionMatrix, const osg::Vec3& s, const osg::Vec3& e) : Translate1DVertexDragger(vertexArray, vertexIndex, startMotionMatrix, s,e) {}

    /** Handle pick events on dragger and generate TranslateInLine commands. */
    virtual bool handle(const osgManipulator::PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

protected:

    virtual ~DefaultTranslate1DVertexDragger() {}
    void translate(osgManipulator::TranslateInLineCommand *cmd, osg::Vec3d& projectedPoint);

};


/** Class defining default vertex dragger */
class DefaultVertexDragger : public PoLAR::VertexDragger
{

public:

    DefaultVertexDragger(PoLAR::Object3D* object3D, osg::Vec3f &vertex, const osg::Matrixd &viewMatrix);
    virtual bool handle(const osgManipulator::PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us, bool shift);
    virtual bool handle(const osgManipulator::PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us)
    {
        return handle(pi, ea, us, false);
    }

protected:
    Translate1DVertexDragger* create1DVertexDragger(float scale, const osg::Matrixd& viewMatrix);

};

}

#endif // _POLAR_VERTEXDRAGGER_H
