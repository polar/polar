﻿/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file ViewerCamera.h
\brief Management of video sequences

\author Pierre-Jean Petitprez
\date year 2014
\note 
Corresponding code: Camera/ViewerCamera.cpp
*/
/** @} */


#ifndef _POLAR_VIEWERCAMERA_H_
#define _POLAR_VIEWERCAMERA_H_

#include "Viewer.h"
#include "VideoPlayer.h"



namespace PoLAR
{

/**
 * \brief Management of video sequences
*/
class ViewerCamera : public Viewer
{
    Q_OBJECT
public:
    /** \name Public Member Functions */
    /** @{ */

    /**
     * @brief Constructor
     * @param parent parent Qwidget
     * @param name assigned to the widget
     * @param f flags to specify various window-system properties for the widget
     * @param shadows boolean to specify if scene graph to display with this viewer must include shadows management
     * @param distortion boolean to specify if distortion management must be included with this viewer
     */
    ViewerCamera(QWidget *parent=0, const char *name=0, Qt::WindowFlags f=0, bool shadows=false, bool distortion=false):
        Viewer(parent, name, f, shadows, distortion), _camera((VideoPlayer*)NULL), _isEditable(false)
    {
        QObject::connect(this, SIGNAL(newFrame()), this, SLOT(update()));
        _nbima = 1;
    }

    /**
     * @brief set the video from a webcam
     * @param camNb the device number in the devices list (eg. for /dev/video0 : camNumber=0)
     * @param w the width of the widget
     * @param h the height of the widget
     * @param d the depth of the images (grey leveled(depth=1) or coloured(depth=3))
     * @param ResizeParent boolean to specify if the parent must be resized or not
     * @param ShareObjects boolean to specify if the images must share the same list of 2D objects
     */
    void setBgVideo(int camNb, int w, int h, int d, bool ResizeParent=true, bool ShareObjects = false);

    /**
     * @brief set the video from a video file
     * @param name the path to the video file
     * @param w the width of the widget
     * @param h the height of the widget
     * @param d the depth of the images (grey leveled(depth=1) or coloured(depth=3))
     * @param ResizeParent boolean to specify if the parent must be resized or not
     * @param ShareObjects boolean to specify if the images must share the same list of 2D objects
     */
    void setBgVideo(const char *name, int w, int h, int d, bool ResizeParent=true, bool ShareObjects = false);


    const VideoPlayer *getCamera()
    {
        return _camera;
    }

    /** @} */


public slots:
    /** \name Public slots */
    /** @{ */

    /**
     * manage a new emitted frame from the camera
     */
    void nextFrameSlot();

    /**
     * pause the camera acquisition
     */
    void pauseCameraSlot();

    /**
     * stop the camera acquisition
     */
    void stopCameraSlot();

    /**
     * Start the image edition - orverwrites \ref Viewer::startEditImageSlot()
     */
    void startEditImageSlot();

    /** @} */


signals:
    /** \name Signals */
    /** @{ */

    /** emitted when a new image is read */
    void newFrame();

    /** @} */


protected:
    /** \name Protected members */
    /** @{ */

    /**
     * start the camera acquisition
     */
    void startCamera();

    /**
     * @brief the camera from which the frames are get
     */
    VideoPlayer *_camera;

    /**
     * @brief boolean to keep the edition mode set by \ref startEditImageSlot()
     */
    bool _isEditable;

    /** @} */
};

}

#endif // _POLAR_VIEWERCAMERA_H_
