/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Camera.h
\brief Read the video flow using QT5 Multimedia module

\author Pierre-Jean Petitprez
\date year 2014
\note
Corresponding code: Camera/Camera.cpp
*/
/** @} */

#ifndef _POLAR_VIDEOPLAYER_H
#define _POLAR_VIDEOPLAYER_H

#define USE_PROBE 0

#include "export.h"

#include <QtCore/QObject>
#include <QtMultimedia/QCamera>
#include <QtMultimedia/QMediaPlayer>
#include <QtMultimedia/QAbstractVideoSurface>
#include <QtMultimedia/QVideoFrame>
#include <QList>
#include <QVideoProbe>
#include <osg/Referenced>

#include<iostream>

namespace PoLAR
{

/**
 * @brief Implementation of a camera for reading video streams
 */
class PoLAR_EXPORT VideoPlayer : public QAbstractVideoSurface, public osg::Referenced
{    
    Q_OBJECT
public:
    /** \name Public Member Functions */
    /** @{ */


    enum Mode {
        CameraMode = 0,
        MediaPlayerMode = 1,
        QmlMode = 2
    };

    /**
     * @brief Constructor for webcam steaming
     * @param device the webcam number (/dev/videoX)
     * @param width the width of the image which will show the video stream
     * @param height the height of the image which will show the video stream
     * @param depth the depth of the images (1: grayscale, 3: RGB)
     * @param parent the parent of the object. NULL by default
     * \note Qt seems to have difficulties managing grayscale images.
     */
    VideoPlayer(int device, int width=0, int height=0, int depth=3, QObject *parent=NULL);

    /**
     * @brief Constructor for reading from a recorded file
     * @param name the video file name
     * @param width the width of the image which will show the viedo stream
     * @param height the height of the image which will show the viedo stream
     * @param depth the depth of the images (1: grayscale, 3: RGB)
     * @param parent the parent of the object. NULL by default
     * \note Qt seems to have difficulties managing grayscale images.
     */
    VideoPlayer(const char *name, int width=0, int height=0, int depth=3, QObject *parent=NULL);


    VideoPlayer(QObject *parent=NULL);

    /**
      * Destructor
      */
    ~VideoPlayer();

    /**
     * @brief returs true if the VideoPlayer is ready to play from a video stream or from a file. Returns false if no camera/media player is ready
     */
    bool valid()
    {
        return (_qcamera != NULL || _qplayer != NULL);
    }

    /**
     * @brief returns the width of the video frame
     * @return the width of the video frame
     */
    int getRealWidth() const { return _width; }

    /**
     * @brief returns the height of the video frame
     * @return the height of the video frame
     */
    int getRealHeight() const { return _height; }

    /**
     * @brief returns the requested display width of the video frame
     * @return the requested display width of the video frame
     */
    int getRequestedWidth() const { return _requestedWidth; }

    /**
     * @brief returns the requested display height of the video frame
     * @return the requested display height of the video frame
     */
    int getRequestedHeight() const { return _requestedHeight; }

    /**
     * @brief Returns a list of all resolutions available
     * @note in case of a media file, the list contains one single QSize element corresponding to the file resolution. Width and height of -1 mean the meta data of the file are not accessible.
     * @note needs Qt > 5.5.0
     * @return a list of the available resolutions, from the smallest to the largest
     */
    QList<QSize> getAvailableResolutions();

    /**
     * @brief Returns the depth of the video frame
     * @return the depth of the video frame
     */
    int getDepth() const { return _depth; }

    /**
     * @brief get access to the QCamera encapsulated in this class. Returns null if not existing
     */
    QCamera* getQCamera()
    {
        return _qcamera;
    }

    /**
     * @brief returns true if in camera mode and a camera is active, else return false.
     */
    bool isCameraAvailable() const { return _mode == CameraMode && _qcamera;}
    /**
     * @brief get access to the QMediaPlayer encapsulated in this class. Returns null if not existing
     */
    QMediaPlayer* getQMediaPlayer()
    {
        return _qplayer;
    }

    /**
     * @brief returns true if in MediaPlayer mode and a MediaPlayer is active, else return false.
     */
    bool isPlayerAvailable() const { return _mode == MediaPlayerMode && _qplayer;}

    /**
     * @brief Returns the data buffer of the current frame
     * @return the data buffer of the current frame
     */
    unsigned char* display() { return _data; }

    /**
     * @brief Sets a frame vertical mirror to true or false
     * @param b if true, the frames will be vetically mirrored
     */
    void mirrored(bool b=true)
    {
        _mirrorFrame = b;
    }

    /**
     * @brief Sets the media player to loop through its media list of not
     * @param b if true, the media player loops though its media plist. If false, the list is played only once
     */
    void loop(bool b=true);

    /**
     * @brief load the previous track in the playlist
     */
    void previous();

    /**
     * load the next track in the playlist
     */
    void next();

    /**
     * @brief add media to the playlist
     * @param name name of the media
     * @return true on success, false otherwise
     */
    bool addMedia(const char *name);

    /**
     * @brief remove media from the playlist
     * @param index index of the media in the playlist
     * @return true on success, false otherwise
     */
    bool removeMedia(int index);

    /**
     * @brief Redefining the virtual method 'supportedPixelFormats' - called by Qt callbacks
     * @param type the handle type
     * @return the list of supported formats
     */
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(QAbstractVideoBuffer::HandleType type) const;

    /**
     * @brief Redefines the virtual method 'present' (called by Qt callbacks)
     * @param frame the current video frame
     * @return true if the frame is valid, false otherwise
     */
    bool present(const QVideoFrame& frame);

    /** @} */


public slots:

    /** \name Public Slots */
    /** @{ */

    /**
     * @brief Initialization and start of the camera.
     * @return 1 in case of success, 0 otherwise
     */
    int play();

    /**
     * @brief Stops the camera acquisition
     */
    void stop();

    /**
     * @brief Pauses the display of the frames
     */
    void pause();

    /**
     * @brief Rewind the play (in the case of a recorded file. Not available in the case of stream camera)
     * @param speed the speed of rewind. Default is 1 (rewind equal to the normal play speed)
     */
    void rewind(qreal speed=1.0);

    /**
     * @brief Forward the play (in the case of a recorded file. Not available in the case of stream camera)
     * @param speed the speed of play. Default is 1 (normal speed)
     */
    void forward(qreal speed=1.0);


    /**
     * @brief Set the playback position (in the case of a recorded file. Not available in the case of stream camera)
     * @param pos the playback position to set, expressed in milliseconds since the beginning of the media
     */
    void setPosition(qint64 pos);

    /**
     * @brief Set the sound volume of the media (in the case of a recorded file. Not available in the case of stream camera)
     * @param volume the volume, between 0 (muted) and 100 (max volume). Values outside the range are clamped
     */
    void setVolume(int volume);


    /** @} */

signals:
    /** \name Signals */
    /** @{ */

    /**
     * @brief Emitted when a new frame is available for reading
     */
    void newFrame(unsigned char*, int, int, int);
    // void newFrame();

    /** @} */


private slots:
    /** \name Protected Members */
    /** @{ */

    /**
     * @brief process the frame set in parameter (decoding / scale / send to connected clients)
     */
    void processFrame(const QVideoFrame &frame);


    /** @} */

protected:
    /** \name Protected Members */
    /** @{ */

    /**
     * set the best resolution according to the parameters given when constructing the class
     * @note when no target width and height are given, the max available resolution is chosen
     */
    void setFittingResolution();

    /**
     * YUV420P / YV12 to RGB24 conversion
     */
    void yuv2rgb(int w, int h, unsigned char* in, unsigned char* out, bool is420);

    /**
     * the QCamera used to get the video stream
     */
    QCamera * _qcamera;

    /**
     * the media player used to play video files
     */
    QMediaPlayer * _qplayer;

    /**
     * the current mode: 0 (camera) or 1 (media player)
     */
    Mode _mode;

    /**
     * the data buffer of the current frame
     */
    unsigned char* _data;

    /**
     * boolean to enable/disable the display of the incoming frames.
     * Used to pause the display of the frames without stopping the camera
     */
    bool _displayFrames;

    /**
     * requested display width
     */
    int _requestedWidth;

    /**
     * requested display height
     */
    int _requestedHeight;

    /**
     * the width of the current frame
     * @note it might be different to the requested width
     */
    int _width;

    /**
     * the height of the current frame
     * @note it might be different to the requested heigth
     */
    int _height;

    /**
     * the depth of the current frame
     */
    int _depth;

    /**
     * the size of the current frame data buffer
     */
    int _bufferSize;

    /**
     * temporary QImage used for rescaling when needed
     */
    QImage *_rescaleImg;

    /**
     * if true, the frames will be vertically mirrored
     */
    bool _mirrorFrame;

#if USE_PROBE
    QVideoProbe* _probe;
#endif

    /** @} */

};

}

#endif // _POLAR_VIDEOPLAYER_H
