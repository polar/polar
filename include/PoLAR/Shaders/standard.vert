attribute vec4 osg_Vertex;
attribute vec4 osg_MultiTexCoord0;
uniform mat4 osg_ModelViewProjectionMatrix;
varying vec4 texCoord0;


void main(void)
{
    gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex;
    texCoord0 = osg_MultiTexCoord0;
}