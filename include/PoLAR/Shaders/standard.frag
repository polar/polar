varying vec4 texCoord0;
uniform sampler2D tex;

void main(void)
{
    gl_FragColor = texture2D(tex, texCoord0.st);
}