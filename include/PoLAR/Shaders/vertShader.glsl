attribute vec4 osg_Vertex;
attribute vec3 osg_Normal;
attribute vec4 osg_MultiTexCoord0;
uniform mat4 lightMVP;
uniform mat4 osg_ModelViewProjectionMatrix;
uniform mat4 osg_ModelViewMatrix;
uniform mat3 osg_NormalMatrix;
uniform mat4 osg_ViewMatrixInverse;


varying vec4 texCoord0;
varying vec4 shadowCoord;
varying vec3 normal;
varying vec3 vertexPos;


void main(void)
{
    vec3 eNormal = vec3(osg_NormalMatrix * osg_Normal);
    normal = normalize(eNormal);
    texCoord0 = osg_MultiTexCoord0;
    
    vec4 vPosition = osg_ModelViewMatrix * osg_Vertex;
    vertexPos = vPosition.xyz;
    vec4 posWorld = osg_ViewMatrixInverse * vPosition;
    shadowCoord = lightMVP * posWorld;
    
    gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex;
}