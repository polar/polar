//#define USE_DEPTH 1

uniform sampler2D shadowTexture;
uniform bool withShadows;
uniform float ambientBias;
varying vec4 shadowCoord;


float unpack(vec4 packedVal)
{
    vec4 unpackFcts = vec4(1.0/(256.0*256.0*256.0), 1.0/(256.0*256.0), 1.0/256.0, 1.0);
    return dot(packedVal, unpackFcts);
}

vec4 unpackShadowMap()
{
    vec4 shadow = vec4(1.0);
    vec4 shadowPos = shadowCoord / shadowCoord.w;
    vec4 shadowTex = texture2D(shadowTexture, shadowPos.st);
    shadowPos = (shadowPos + 1.0) / 2.0;
  //  shadowPos.z -=  0.0005;
    float depth = unpack(shadowTex);
    if(depth<shadowPos.z)
        shadow *= 0.;
    shadow += vec4(ambientBias);
    return shadow;
}

vec4 applyShadowMap()
{
    vec4 shadow = vec4(1.0);
    if(shadowCoord.q > 0.0)
    {
        shadow = texture2DProj(shadowTexture, shadowCoord);
    }
    return (shadow + vec4(ambientBias));
}

void main(void)
{
    vec4 base = vec4(1.0);
    if(withShadows)
    {
    #ifdef USE_DEPTH
      vec4 shadow = applyShadowMap();
    #else
      vec4 shadow = unpackShadowMap();
    #endif
      base *= shadow;
    }
    gl_FragColor = base;
}