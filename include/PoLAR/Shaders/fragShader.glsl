#define MAX_LIGHTS 8

//#define USE_DEPTH 

// vertex-related uniforms
varying vec3 vertexPos;
varying vec3 normal;
varying vec4 texCoord0;
varying vec4 shadowCoord;
uniform mat4 osg_ViewMatrix;
uniform sampler2D baseTexture;
uniform sampler2D shadowTexture;
uniform int hasTexture;
uniform bool withShadows;
uniform float ambientBias;

// light-related uniforms
uniform int nbLights;

struct LightSourceParameters 
{   
   vec4 ambient;              // Aclarri   
   vec4 diffuse;              // Dcli   
   vec4 specular;             // Scli   
   vec4 position;             // Ppli   
   vec4 halfVector;           // Derived: Hi   
   vec3 spotDirection;        // Sdli   
   float spotExponent;        // Srli   
   float spotCutoff;          // Crli                              
                              // (range: [0.0,90.0], 180.0)   
   float spotCosCutoff;       // Derived: cos(Crli)                 
                              // (range: [1.0,0.0],-1.0)   
   float constantAttenuation; // K0   
   float linearAttenuation;   // K1   
   float quadraticAttenuation;// K2  
   int isInEyeSpace;
   int isShadowCaster;
};    

struct MaterialParameters  
{   
   vec4 emission;    // Ecm   
   vec4 ambient;     // Acm   
   vec4 diffuse;     // Dcm   
   vec4 specular;    // Scm   
   float shininess;  // Srm  
};  

uniform LightSourceParameters LightSource[MAX_LIGHTS];
uniform MaterialParameters material;

float ambientScene = 0.0;
float specularCoeff = 0.5;


float unpack(vec4 packedVal)
{
     vec4 unpackFcts = vec4(1.0/(256.0*256.0*256.0), 1.0/(256.0*256.0), 1.0/256.0, 1.0);
     return dot(packedVal, unpackFcts);
}


vec4 unpackShadowMap()
{
    vec4 shadow = vec4(1.0);
    vec4 shadowPos = shadowCoord / shadowCoord.w;
    vec4 shadowTex = texture2D(shadowTexture, shadowPos.st);
    shadowPos = (shadowPos + 1.0) / 2.0;
    shadowPos.z -=  0.001;
    float depth = unpack(shadowTex);
    //shadow *= float(depth+bias>=shadowPos.z);
    if(depth<shadowPos.z)
        shadow *= 0.;
    shadow += vec4(ambientBias);
    return shadow;
}


vec4 applyShadowMap()
{
    vec4 shadow = vec4(1.0);
    if(shadowCoord.q > 0.0)
    {
        shadow = texture2DProj(shadowTexture, shadowCoord);
    }
    return (shadow + vec4(ambientBias));
}



vec4 computeAmbient(vec4 matAmb, vec4 lightAmb)
{
   vec4 ambColor = matAmb * lightAmb;
   return ambColor;
}

vec4 computeDiffuse(vec4 matDiff, vec4 lightDiff, float attenuation, vec3 D, vec3 N)
{
    float lambert = dot(N, D);
    vec4 diffColor = clamp(matDiff * lightDiff * max(lambert, 0.0), 0.0, 1.0) * attenuation; 
    return diffColor;
}

vec4 computeSpecular(vec4 matSpec, vec4 lightSpec, float attenuation, float shininess, vec3 D, vec3 N)
{
    vec3 E = normalize(-vertexPos);
    vec3 R = normalize(reflect(-D, N));
    vec4 specColor = clamp(attenuation * matSpec * lightSpec * pow(max(dot(R, E), 0.0), specularCoeff*shininess), 0.0, 1.0);
    return specColor;
}

float computePointLightAttenuation(LightSourceParameters light, float dist)
{
    return (1.0 / (light.constantAttenuation + light.linearAttenuation*dist + light.quadraticAttenuation*dist*dist));
}

float computeSpotLightAttenuation(vec3 lightDir, LightSourceParameters light)
{
    float attenuation = 0.0;
    vec4 spotDir4 = osg_ViewMatrix*vec4(light.spotDirection, 1.0);
    vec3 spotDir = normalize(spotDir4.xyz);
    float clampedCosine = dot(spotDir, -lightDir);
    if (clampedCosine > light.spotCosCutoff) // inside of spotlight cone?
    {
       attenuation = pow(clampedCosine, light.spotExponent);  
    }
    return attenuation;
}


void main(void)
{
    vec4 diffuse = vec4(0.0);
    vec4 specular = vec4(0.0);
    vec4 ambient = vec4(0.0);
    float shininess = 0.0;
    vec3 N = normalize(normal);
    vec4 color = vec4(ambientScene);
    float dist = 0.0;
    vec4 tmp = vec4(0.);
    
    if(hasTexture == 1) // textured object: we use the texture as diffuse and don't set any ambient and specular for the moment
    {
         diffuse = texture2D(baseTexture, texCoord0.st);
    }
    else
    {
         diffuse  = material.diffuse;
         specular = material.specular;
         ambient = material.ambient;
         shininess = material.shininess;
    }
    
    for(int i=0; i<MAX_LIGHTS; i++)
    {
        if(i==nbLights) break;
        vec4 lightPos;
        vec3 lightDir;
        float attenuation = 0.0;
        if(LightSource[i].isInEyeSpace == 1)
        {
            lightPos = LightSource[i].position;
        }
        else
        {
            lightPos = osg_ViewMatrix*LightSource[i].position;
        }
        
        if(LightSource[i].position.w == 0.0) // directional light
        {
            lightDir = normalize(lightPos.xyz);
            attenuation = 1.0;
        }
        else // point/spot light
        {
            lightDir = lightPos.xyz - vertexPos;
            dist = length(lightDir);
            lightDir = normalize(lightDir);
            attenuation = computePointLightAttenuation(LightSource[i], dist);
            if (LightSource[i].spotCutoff <= 90.0) // spotlight?
            {
                attenuation *= computeSpotLightAttenuation(lightDir, LightSource[i]);
            }
        }
        
      /*  if(LightSource[i].isShadowCaster == 1)
        {
            //vec3 l = normalize(lightPos.xyz);
           // float cosTheta = clamp(dot(N, -lightPos), 0.0, 1.0);
           // bias =  tan(acos(cosTheta)) * 0.005;
            bias = clamp(bias, 0.0, 0.01);
           // bias = max( (1.0 - dot(N, lightDir)), 0.0001);
        }*/
        
        // Ambient term
        color += computeAmbient(ambient, LightSource[i].ambient);
    
        // Lambert term (diffuse)
        color += computeDiffuse(diffuse, LightSource[i].diffuse, attenuation, lightDir, N);
    
        // Specular term
        color += computeSpecular(specular, LightSource[i].specular, attenuation, shininess, lightDir, N);;
    }
    
    if(withShadows)
    {
    #ifdef USE_DEPTH
       vec4 shadow = applyShadowMap();
    #else
       vec4 shadow = unpackShadowMap();
    #endif
        gl_FragColor = color * shadow;
    }
    else
        gl_FragColor =  color;
}