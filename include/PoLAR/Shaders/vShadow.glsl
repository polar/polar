attribute vec4 osg_Vertex;
uniform mat4 osg_ModelViewProjectionMatrix;

varying vec4 vPosition;

void main(void)
{
    vPosition = osg_ModelViewProjectionMatrix * osg_Vertex;
    gl_Position = vPosition;
}
