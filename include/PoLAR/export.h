/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifndef PoLAR_EXPORT_
#define PoLAR_EXPORT_ 1

#ifdef _MSC_VER
    #if defined (PoLAR_STATIC)
        #define PoLAR_EXPORT
    #elif defined (PoLAR_DYNAMIC)
        #define PoLAR_EXPORT __declspec(dllexport)
    #else
        #define PoLAR_EXPORT __declspec(dllimport)
    #endif
#else
    #define PoLAR_EXPORT
#endif

#endif