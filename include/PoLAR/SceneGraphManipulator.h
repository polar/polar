/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file SceneGraphManipulator.h
\brief Custom manipulator of a scenegraph

\author Frédéric Speisser, Pierre-Jean Petitprez
\date 2007, 2015
\note 
Corresponding code: SceneGraph/SceneGraphManipulator.cpp
*/
/** @} */

#ifndef _POLAR_SCENEGRAPHMANIPULATOR_H_
#define _POLAR_SCENEGRAPHMANIPULATOR_H_

#include "Viewer2D3D.h"

#include <osgUtil/SceneView>
#include <osg/Quat>
#include <osg/Version>
#include <osgGA/CameraManipulator>
#include <osgGA/GUIEventAdapter>

namespace PoLAR
{

class Viewer2D3D;

/** \brief Custom manipulator of a scenegraph

This manipulator was developped for specific needs, thanks to it we can : \n
- do a 3D translation parallel to the image plane, do a 3D translation orthogonal to the image plane and rotate around a center which can be defined (thanks to the method setTrackNode(osg::Node* node)); \n
- have an adaptative interaction, depending on the image zoom, depending on the type of the projection used (Viewer2D3D::VISION, Viewer2D3D::ANGIO and Viewer2D3D::ANGIOHINV) to visualise the 3D items and depending on the size of the 3D items

It can be used as a native OpenSceneGraph manipulator since it inherits from the global OpenSceneGraph matrix manipulator class (osgGA::MatrixManipulator)

*/

class PoLAR_EXPORT SceneGraphManipulator : public osgGA::CameraManipulator
{

public:

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
  */
    SceneGraphManipulator();

    /** set the node to be tracked, actually set the center of the rotation as the center of the node's bounding box. */
    void setTrackNode(osg::Node* node);

    /** get the node tracked. */
    osg::Node* getTrackNode() { return _trackNodePath.empty() ? 0 : _trackNodePath.back().get(); }

    /** get the node tracked (const).  */
    const osg::Node* getTrackNode() const { return _trackNodePath.empty() ? 0 : _trackNodePath.back().get(); }

    virtual const char* className() const { return "SceneGraphManipulator"; }

    /** set the position of the matrix manipulator using a 4x4 Matrix.*/
    virtual void setByMatrix(const osg::Matrixd& matrix);

    /** set the position of the matrix manipulator using a 4x4 Matrix.*/
    virtual void setByInverseMatrix(const osg::Matrixd& matrix) { setByMatrix(osg::Matrixd::inverse(matrix)); }

    /** get the position of the manipulator as 4x4 Matrix.*/
    virtual osg::Matrixd getMatrix() const;

    /** get the position of the manipulator as a inverse matrix of the manipulator, typically used as a model view matrix.*/
    virtual osg::Matrixd getInverseMatrix() const;

    /** move the camera to the default position. \n
      may be ignored by manipulators if home functionality is not appropriate.*/
    virtual void home(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& us);

    /** move the camera to the default position. \n
      may be ignored by manipulators if home functionality is not appropriate.*/
    virtual void home(double);

    /** start/restart the manipulator.*/
    virtual void init(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& us);

    /** handle events, return true if handled, false otherwise.*/
    virtual bool handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& us);

    /** get the keyboard and mouse usage of this manipulator.*/
    virtual void getUsage(osg::ApplicationUsage& usage) const;

    /** set the rotation of the trackball. */
    void setRotation(const osg::Matrixd& rotation) { _rotation = rotation; }

    /** get the rotation of the trackball. */
    const osg::Matrixd& getRotation() const { return _rotation; }

    /** set the initial rotation of the trackball. */
    void setRotationInit(const osg::Matrixd& rotation) { _rotationInit = rotation; }

    /** get the initial rotation of the trackball. */
    const osg::Matrixd& getRotationInit() const { return _rotationInit; }

    /** set the initial position of the trackball. */
    void setPositionInit(const osg::Vec3d& position) { _positionInit = position; }

    /** get the initial position of the trackball. */
    const osg::Vec3d& getPositionInit() const { return _positionInit; }

    /** set the distance of the trackball to the center of the tracked node. */
    void setDistanceX(double distance) { _distanceX = distance; }
    void setDistanceY(double distance) { _distanceY = distance; }
    void setDistanceZ(double distance) { _distanceZ = distance; }

    /** get the distance of the trackball to the center of the tracked node. */
    double getDistanceX() const { return _distanceY; }
    double getDistanceY() const { return _distanceX; }
    double getDistanceZ() const { return _distanceZ; }

    /**
     * @brief customize the mouse button and key modifier to rotate the camera
     * @note the modifier must be a osgGA::GUIEventAdapter::KeySymbol type. Only left keys are taken into account (Qt does not differ between left and right modifier keys)\n
     * Possibilities are: osgGA::GUIEventAdapter::KEY_Control_L, osgGA::GUIEventAdapter::KEY_Shift_L, osgGA::GUIEventAdapter::KEY_Alt_L\n
     * @note No modifier set = only mouse button is used
     */
    void setRotateCameraMouseButton(osgGA::GUIEventAdapter::MouseButtonMask button, int modifier=0)
    {
        _rotateCameraMouseButton = button;
        _rotateCameraModifier = modifier;
       // if(modifier) _useModifiers = true;
    }

    /**
     * @brief customize the mouse button and key modifier to rotate the camera around the axis orthogonal to the image
     * @note the modifier must be a osgGA::GUIEventAdapter::KeySymbol type. Only left keys are taken into account (Qt does not differ between left and right modifier keys)\n
     * Possibilities are: osgGA::GUIEventAdapter::KEY_Control_L, osgGA::GUIEventAdapter::KEY_Shift_L, osgGA::GUIEventAdapter::KEY_Alt_L\n
     * @note No modifier set = only mouse button is used
     */
    void setRotateCameraOrthogonalMouseButton(osgGA::GUIEventAdapter::MouseButtonMask button, int modifier=0)
    {
        _rotateCamera2MouseButton = button;
        _rotateCamera2Modifier = modifier;
       // if(modifier) _useModifiers = true;
    }

    /**
     * @brief customize the mouse button and key modifier to translate the camera parallely to the image plane
     * @note the modifier must be a osgGA::GUIEventAdapter::KeySymbol type. Only left keys are taken into account (Qt does not differ between left and right modifier keys)\n
     * Possibilities are: osgGA::GUIEventAdapter::KEY_Control_L, osgGA::GUIEventAdapter::KEY_Shift_L, osgGA::GUIEventAdapter::KEY_Alt_L\n
     * @note No modifier set = only mouse button is used
     */
    void setTranslateXYMouseButton(osgGA::GUIEventAdapter::MouseButtonMask button,  int modifier=0)
    {
        _panXYMouseButton = button;
        _panXYModifier = modifier;
       // if(modifier) _useModifiers = true;
    }

    /**
     * @brief customize the mouse button and key modifier to translate the camera orthogonally to the image plane
     * @note the modifier must be a osgGA::GUIEventAdapter::KeySymbol type. Only left keys are taken into account (Qt does not differ between left and right modifier keys)\n
     * Possibilities are: osgGA::GUIEventAdapter::KEY_Control_L, osgGA::GUIEventAdapter::KEY_Shift_L, osgGA::GUIEventAdapter::KEY_Alt_L\n
     * @note No modifier set = only mouse button is used
     */
    void setTranslateZMouseButton(osgGA::GUIEventAdapter::MouseButtonMask button, int modifier=0)
    {
        _panZMouseButton = button;
        _panZModifier = modifier;
      //  if(modifier) _useModifiers = true;
    }


    /**
     * @brief customize the key shortcut to reset the camera view to the initial extrinsics
     */
    void setResetCameraShortcut(osgGA::GUIEventAdapter::KeySymbol key)
    {
        _resetCameraKey = key;
    }



    /** @} */


protected:

    /**  destructor
  */
    virtual ~SceneGraphManipulator();

    /** storage for the node path in the graph of the tracked node. */
    typedef std::vector< osg::observer_ptr<osg::Node> >   ObserveredNodePath;

    /** set the tracked node's path. */
    void setTrackNodePath(const osg::NodePath& nodePath)
    {
        _trackNodePath.clear();
        _trackNodePath.reserve(nodePath.size());
        std::copy(nodePath.begin(), nodePath.end(), std::back_inserter(_trackNodePath));
    }

    /** set the tracked node's path. */
    void setTrackNodePath(const ObserveredNodePath& nodePath) { _trackNodePath = nodePath; }

    /** get the tracked node's path. */
    ObserveredNodePath& getTrackNodePath() { return _trackNodePath; }

    /** get the tracked node's path. */
    osg::NodePath getNodePath() const;

    /** check if the tracked node's path is valid. */
    bool validateNodePath() const;

    /** compute world to local matrix. */
    void computeNodeWorldToLocal(osg::Matrixd& worldToLocal) const;

    /** compute local to world matrix. */
    void computeNodeLocalToWorld(osg::Matrixd& localToWorld) const;

    /** compute center and orientation of the tracked node. */
    void computeNodeCenterAndRotation(osg::Vec3d& center, osg::Quat& rotation) const;

    /** Reset the internal GUIEvent stack.*/
    void flushMouseEventStack();

    /** Add the current mouse GUIEvent to internal stack.*/
    void addMouseEvent(const osgGA::GUIEventAdapter& ea);

    /** compute camera position/orientation thanks to the openGL lookAt standard
    \param eye the position of the camera
    \param up and \param center the orientation of the camera
  */
    void computePosition(const osg::Vec3d& eye,const osg::Vec3d& center,const osg::Vec3d& up);

    /** set the interaction mode of the manipulator to fit with projection used for the 3D display, the image zoom and 3D scene's size
    \param mode type of the projection used for 3D display
    \param center center around which the trackball's rotation is made
    \param imageZoom image zoom
  */
    void setInteractionMode(int mode, osg::Vec3f center, float imageZoom);

    /** For the give mouse movement calculate the movement of the camera.
      Return true is camera has moved and a redraw is required.*/
    bool calcMovement();

    /** Check the speed at which the mouse is moving.
      If speed is below a threshold then return false, otherwise return true.*/
    bool isMouseMoving();

    /**
     * init the mouse buttons and key shortcuts to default values
     */
    void initDefaultShortcuts();


    /** Internal event stack comprising before last mouse event. */
    osg::ref_ptr<const osgGA::GUIEventAdapter> _ga_t1;

    /** Internal event stack comprising last mouse event. */
    osg::ref_ptr<const osgGA::GUIEventAdapter> _ga_t0;

    /** the tracked node's path. */
    ObserveredNodePath _trackNodePath;

    /** boolean to specify manipulation's inertia. */
    bool _thrown;

    /** projection type used for the 3D display necessary for setInteractionMode(Viewer2D3D::ProjectionType mode, osg::Vec3f center, float imageZoom). */
    int _trackerMode;

    /** pan rating used for the interaction, calculated thanks to setInteractionMode(Viewer2D3D::ProjectionType mode, osg::Vec3f center, float imageZoom). */
    float _panRating;

    /** initial orientation of the camera. */
    osg::Matrixd _rotationInit;

    /** initial position of the camera. */
    osg::Vec3d _positionInit;

    /** rotation computed by the manipulator, to apply to the camera. */
    osg::Matrixd _rotation;

    /** position along X axis computed by the manipulator, to apply to the camera. */
    double _distanceX;

    /** position along Y axis computed by the manipulator, to apply to the camera. */
    double _distanceY;

    /** position along Z axis computed by the manipulator, to apply to the camera. */
    double _distanceZ;

    /** pointer to the manipulated scene graph */
    osg::ref_ptr<PoLAR::SceneGraph> _sceneGraph;

    /** boolean to store if the initialisation of the scene graph manipulator was already made or not */
    bool _init;

    /**
     * mouse button for performing pan in XY plane (parallel to screen)
     */
    int _panXYMouseButton;

    /**
     * mouse button for performing pan in Z plane (orthogonal to screen)
     */
    int _panZMouseButton;

    /**
     * mouse button for camera rotation
     */
    int _rotateCameraMouseButton;

    /**
     * mouse button for camera rotation around an axis orthogonal of the image
     */
    int _rotateCamera2MouseButton;

    /**
     * modifier (CTRL/ Shift /ALT) for XY pan. Default to none
     */
    int _panXYModifier;

    /**
     * modifier (CTRL/ Shift /ALT) for Z pan. Default to none
     */
    int _panZModifier;

    /**
     * modifier (CTRL/ Shift /ALT) for camera rotation. Default to none
     */
    int _rotateCameraModifier;

    /**
     * modifier (CTRL/ Shift /ALT) for camera rotation. Default to CTRL
     */
    int _rotateCamera2Modifier;

    /**
     * key for reseting camera to original extrinsics
     */
    int _resetCameraKey;

    /**
     * modifier key currently being hit
     */
    int _currentModifier;

    /**
     * Is set to true when modifiers are being used for pan and camera manipulation
     */
    bool _useModifiers;

};

}

#endif //_POLAR_SCENEGRAPHMANIPULATOR_H_
