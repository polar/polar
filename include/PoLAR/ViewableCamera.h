/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file ViewableCamera.h
\brief Class for a dynamic representation of a camera 

\author Frédéric Speisser
\date year 2007
\note 
Corresponding code: Object3D/ViewableCamera.cpp
*/
/** @} */

#ifndef _POLAR_VIEWABLECAMERA_H_
#define _POLAR_VIEWABLECAMERA_H_

#include "export.h"

#include <osg/Node>
#include <osg/Group>
#include <osg/StateSet>
#include <osg/BlendColor>
#include <osg/ColorMask>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/Matrixd>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/LineWidth>
#include <osg/PolygonMode>

#include <QtCore/QObject>


namespace PoLAR
{

/** \brief Class for a dynamic representation of a camera 

The aim of this class is to provide, in an easy way, the possibility to have a 3D camera representation whose position and orientation are dynamically modified according to the behaviour of a chosen camera.

Typical use is to see the movement of the camera of a viewer in another viewer (run the example testvisucolor3D2 to see how it works)
*/


class PoLAR_EXPORT ViewableCamera : public QObject, public osg::Group
{
    Q_OBJECT

public :

    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param camera the camera of which we want a 3D representation
    \param viewablePath boolean to specify if the path of the camera has to be visible
    \note to construct as follows : ViewableCamera *viewCam = new ViewableCamera(viewer1->getCamera(), true); \n
          viewer can be a ViewerStatic or a ViewerDynamic
    \note ViewableCamera is a nativ osg::Node, thereby you can make a PoLAR::Object3D out of it and add it to the scenegraph in a standard way :
    \note viewer2->addObject3D(new PoLAR::Object3D(viewCam));
  */
    ViewableCamera(osg::Camera *camera, bool viewablePath = false);

    /** get path length
    \return the path length if camera's path is set to visible
  */
    inline int getPathLength() {return _viewablePath ? _pathVertices->size() : 0;}

    /** @} */


public slots :

    /** \name Public Slots */
    /** @{ */

    /** update the position and the orientation of this viewable camera \n
      update the path if camera's path is set to visible
  */
    void update();

    /** delete the path if camera's path is set to visible
  */
    void resetPath();

    /** @} */



protected :

    /** create base structure to store path
  */
    osg::Geode* initPath();

    /** create 3D camera representation
  */
    osg::Group* createViewableCamera();

    /** update position and orientation of this viewable camera
  */
    void updatePositionAttitude();

    /** update path of this viewable camera
  */
    void updatePath();


private :

    /** pointer to the original camera
  */
    osg::ref_ptr<osg::Camera> _camera;

    /** pointer to the transformation matrix which stores position and orientation of this viewable camera
  */
    osg::MatrixTransform *_cameraMatrixTransform;

    /** pointer to the array of vertices corresponding to the path of this viewable camera
  */
    osg::Vec3Array* _pathVertices;

    /** pointer to unitary element of the geometry (here a line) corresponding to the path of this viewable camera
  */
    osg::DrawElementsUInt* _pathDrawElements;

    /** pointer to the geometry corresponding to the path of this viewable camera
  */
    osg::Geometry* _pathGeometry;

    /** number of vertices in path (variable used to avoid several calls to getPathLength())
  */
    int _numPoints;

    /** boolean to store the state of the path (visible or not)
  */
    bool _viewablePath;

    /** storage corresponding to the last position of the camera
  */
    osg::Vec3f _posPrec;
};

}

#endif // _POLAR_VIEWABLECAMERA_H_
