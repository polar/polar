/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file TaggedPolygon2D.h
\brief Management of 2D tagged polygons

    Indexed list of markers having the shape of a closed polygon in 2D \n
    The index is stored in the integral part of the label of the markers \n
    Beware then that this integral part will be modified when adding a marker \n
    See setCurrentIndex member function that modifies the integral part of the
    currently selected marker (if any) to a given integer


\author Erwan Kerrien, Pierre-Jean Petitprez
\date unknown
\note 
Corresponding code: Object2D/TaggedPolygon2D.cpp
*/
/** @} */

#ifndef _POLAR_TAGGEDPOLYGON2D_H_
#define _POLAR_TAGGEDPOLYGON2D_H_

#include "Polygon2D.h"

namespace PoLAR
{

/** \brief Management of 2D tagged polygons

*/



class TaggedPolygon2D : public Polygon2D
{
public:

    /** \name Public Member Functions */
    /** @{ */

    /** constructor
    \param list markers list which makes the 2D spline
  */
    TaggedPolygon2D(std::list<Marker2D*> &list);

    /**
     * default constructor
     */
    TaggedPolygon2D();

    /**
     * copy constructor
     */
    TaggedPolygon2D(TaggedPolygon2D &obj);

    /** add a marker and set it as the currently selected marker
    \param marker the marker to add
  */
    void addMarker(Marker2D *marker);

    /** set an index as the current one
    \param i the index to set \n
      modifies the integral part of the label so that it is the same as the marker immediately before the new marker in the list (same index) \n
      if the list was empty before adding this marker, the default index is 0
  */
    void setCurrentIndex(int i);

    /**
     * @brief returns a copy of this object
     */
    virtual TaggedPolygon2D* clone()
    {
        return new TaggedPolygon2D(*this);
    }

    /** @} */
};

}

#endif // _POLAR_TAGGEDPOLYGON2D_H_
