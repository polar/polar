/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Text2D.h
\brief Management of 2D text

\author Pierre-Jean Petitprez
\date 2015
\note
Corresponding code: Object2D/Text2D.cpp
*/
/** @} */

#ifndef POLAR_TEXT2D_H
#define POLAR_TEXT2D_H

#include "Object2D.h"
#include <list>
#include <QString>

namespace PoLAR
{

/**
 * @brief Management of 2D text
 */
class PoLAR_EXPORT Text2D : public Object2D
{

public:

    /**  constructor
    */
    Text2D(std::list<Marker2D *> list);

    /** default constructor
     */
    Text2D();

    /**
     * copy constructor
     */
    Text2D(Text2D &obj);

    /**  general object selection (using points)
    */
    float select(double x, double y, double precision) {return selectMarker(x, y, precision);}

    /**
     * @brief set the text to be displayed
     */
    void setText(QString& text)
    {
        _text = text;
    }

    /**
     * @brief get the text displayed
     */
    QString& getText()
    {
        return _text;
    }

    /**
     * @brief returns a copy of this object
     */
    virtual Text2D* clone()
    {
        return new Text2D(*this);
    }

private:

    /**
     * the text linked to these markers (same text for all the markers)
     */
    QString _text;

};

}

#endif // POLAR_TEXT2D_H
