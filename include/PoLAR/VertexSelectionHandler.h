/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/


#ifndef VERTEXSELECTIONDRAGGER_H
#define VERTEXSELECTIONDRAGGER_H

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif
#include "export.h"

#include "EventHandlerInterface.h"
#include "PhysicsObject3D.h"
#include <osgGA/GUIEventHandler>
#include <utility>

namespace PoLAR
{

class Viewer2D3D;
class VertexDragger;

/**
 * @brief Handler to manae vertex selection in a mesh
 */
class PoLAR_EXPORT VertexSelectionHandler : public QObject, public osgGA::GUIEventHandler, public EventHandlerInterface
{
    Q_OBJECT

public:


    /** \name Public Member Functions */
    /** @{ */

    /**  constructor
    \param displayInfo true to display informations about the objets pointed by the mouse else false
  */
    VertexSelectionHandler(bool displayInfo=false);

    /** destructor
  */
    virtual ~VertexSelectionHandler()
    {
        cleanUp();
    }

    /** handle events */
    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, osg::Object*, osg::NodeVisitor*)
    {
        return handle(ea, aa);
    }

    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);


    /** get the current manipulated vertex */
    inline std::pair<unsigned int, osg::Vec3f>& getVertexManipulated() { return _vertexManipulated; }


    /** @} */


public slots :

    /** \name Public Slots */
    /** @{ */



    /** @} */


signals:

    /** \name Signals */
    /** @{ */
    void push(float mouseX, float mouseY, osg::Vec3f vertex);

    void drag(float deltaX, float deltaY);

    void force(float x, float y, float z);

    void release();

    /** @} */

protected:


private:

    bool findNearestVertex(float x, float y, osgViewer::View *viewer);

    /** pointer to the structure used for storage of the intersections for one pick event */
    osgManipulator::PointerInfo _pointer;


    /** vertex in current edition
  */
    std::pair<unsigned int, osg::Vec3f> _vertexManipulated;

    /** PoLAR::Object3D currently pointed
  */
    osg::ref_ptr<PoLAR::Object3D> _currentObservedObject;


    float _mouseX;
    float _mouseY;

    float _dragStartX;
    float _dragStartY;

    int _mouseButton;
    bool _isClicked;

};

}

#include "Viewer2D3D.h"
#endif // VERTEXSELECTIONDRAGGER_H
