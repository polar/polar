/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "BaseViewer.h"

#include <osg/Camera>
#include <osgGA/EventQueue>

#ifdef OSG_USE_FBO
#include <osgViewer/Renderer>
#endif

#include <QKeyEvent>
#include <QPainter>
#include <QWheelEvent>
#include <QOpenGLContext>
#include <QWindow>
#include <iostream>
#include <QDebug>

#include "GraphicsViewer.h"

using namespace PoLAR;



BaseViewer::BaseViewer(GraphicsViewer *parent, WindowFlags f):
    QOpenGLWidget(0, f), osgViewer::Viewer(),
    _parentViewer(parent)
{
    osg::ref_ptr<osg::Camera> camera = getCamera();
    camera->setViewport(0, 0, width(), height());

#ifdef OSG_USE_FBO
    /* OSG patch for FBO usage
     * Despite having set a new CullVisitor prototype (see GraphicsViewer class), we need to force
     * OSG to use the same custom RenderStage than the custom CullVisitor.
     * Indeed, OSG instanciates a regular RenderStage in the SceneView class, which is not impacted by the CullVisitor config.
     * */
    osgViewer::Renderer *renderer = static_cast<osgViewer::Renderer *>(camera->getRenderer());
    renderer->getSceneView(0)->setRenderStage(osgUtil::CullVisitor::prototype()->getRenderStage());
    renderer->getSceneView(0)->getCullVisitor()->setRenderStage(osgUtil::CullVisitor::prototype()->getRenderStage());
    renderer->getSceneView(1)->setRenderStage(osgUtil::CullVisitor::prototype()->getRenderStage());
    renderer->getSceneView(1)->getCullVisitor()->setRenderStage(osgUtil::CullVisitor::prototype()->getRenderStage());

    // custom GraphicsWindow to overwrite the use of the regular osg::State and allow manual FBO swapping
    _gw = new GraphicsWindowEx(0, 0, width(), height());
#else
    _gw = new osgViewer::GraphicsWindowEmbedded(0, 0, width(), height());
#endif

    camera->setGraphicsContext(_gw);

    setThreadingModel(osgViewer::Viewer::SingleThreaded);
    setRunFrameScheme(osgViewer::Viewer::ON_DEMAND);

    setMouseTracking(true);
    setAutoFillBackground(false);

    setAttribute(Qt::WA_AcceptTouchEvents);
    connectKeyEventSlots();

}

BaseViewer::~BaseViewer()
{
    _gw->close();
}

#ifdef OSG_USE_FBO
void BaseViewer::addEventHandler(osgGA::GUIEventHandler *eventHandler)
{
    osgViewer::Viewer::addEventHandler(eventHandler);

    /* OSG patch for FBO usage
     * if the event handler is a StatsHandler, we need to add its camera (used for the post rendering of the GUI)
     * into our scenegraph so that this camera will be considered as one of ours.
     * Otherwise, the stats will be drawn under the main camera in the default FBO.
     */
    osgViewer::StatsHandler *stats = dynamic_cast<osgViewer::StatsHandler*>(eventHandler);
    if(stats)
    {
        osg::Camera* camera = stats->getCamera();
        camera->setName("StatsHandler");
        getSceneData()->asGroup()->addChild(camera);
    }
}
#endif

void BaseViewer::initializeGL()
{
    this->realize();
    initializeOpenGLFunctions();
    // Ask the viewer to supply replacement variables for GL native ones in shaders
    osg::State* state = getCamera()->getGraphicsContext()->getState();
    state->setUseModelViewAndProjectionUniforms(true);
#ifndef USE_FIXED_PIPELINE
    state->setUseVertexAttributeAliasing(true);
#endif

#ifdef PoLAR_DEBUG
    char* string  = (char*)glGetString(GL_EXTENSIONS);
    qDebug("%s\n", string);
#endif
}


void BaseViewer::paintEvent(QPaintEvent *e)
{
    /* Note 21/04/2016 (Pierre-Jean):
     * the default behaviour of this function is to call paintGL().
     * For some reason, calling an update of this widget from the GraphicsViewer leads to 2 calls to paintGL.
     * I have disactivated the default paintEvent so that paintGL is called only once.
     */
    (void)e;
    //QOpenGLWidget::paintEvent(e);
}

void BaseViewer::paintGL()
{
    if(this->checkNeedToDoFrame())
    {
        // make frame
        frame();
    }
}

void BaseViewer::resizeGL(int width, int height)
{
    this->getEventQueue()->windowResize(0, 0, width, height);
    _gw->resized(0, 0, width, height);
    glViewport(0,0, width, height);
    //osg::ref_ptr<osg::Camera> camera = getCamera();
    //camera->setViewport(0, 0, width, height);
}


void BaseViewer::frame(double simulationTime)
{
#ifdef OSG_USE_FBO
    /* OSG patch for FBO usage
     * Swap the FBO ID back to the default one (used by Qt)
     */
    osg::State& state = *(_gw->getState());
    static_cast<StateEx *>(&state)->setDefaultFbo(defaultFramebufferObject());
#endif
    osgViewer::Viewer::frame(simulationTime);
}


void BaseViewer::requestRedraw()
{
    osgViewer::Viewer::requestRedraw();
}


void BaseViewer::keyPressEvent(QKeyEvent* event)
{
    emit keyPressSignal(event);
}

void BaseViewer::keyReleaseEvent(QKeyEvent* event)
{
    emit keyReleaseSignal(event);
}

void BaseViewer::mouseMoveEvent(QMouseEvent* event)
{
    emit mouseMoveSignal(event);
}

void BaseViewer::mousePressEvent(QMouseEvent* event)
{
    emit mousePressSignal(event);
}

void BaseViewer::mouseReleaseEvent(QMouseEvent* event)
{
    emit mouseReleaseSignal(event);
}

void BaseViewer::wheelEvent(QWheelEvent* event)
{
    event->accept();
    int delta = event->delta();
    osgGA::GUIEventAdapter::ScrollingMotion motion = delta > 0 ?  osgGA::GUIEventAdapter::SCROLL_UP
                                                                : osgGA::GUIEventAdapter::SCROLL_DOWN;
    getEventQueue()->mouseScroll(motion);
}


void BaseViewer::mouseDoubleClickEvent(QMouseEvent *event)
{
    unsigned int button = this->button(event);
    getEventQueue()->mouseDoubleButtonPress(static_cast<float>(event->x()), static_cast<float>(event->y()), button);
}

bool BaseViewer::event(QEvent* event)
{

    switch(event->type())
    {
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
    case QEvent::TouchCancel:
    {
        emit touchEventSignal(event);
        update();
        break;
    }
    case QEvent::KeyPress:
    case QEvent::KeyRelease:
    case QEvent::MouseButtonDblClick:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove:
    case QEvent::Wheel:
        //requestRedraw();
        break;

    default:
        break;
    }

    return QOpenGLWidget::event(event);
}


void BaseViewer::keyPressSlot(QKeyEvent *e)
{
    if(e->text().isEmpty()) // modifier key
    {
        osgGA::GUIEventAdapter* ea = new osgGA::GUIEventAdapter;
        ea->setEventType(osgGA::GUIEventAdapter::KEYDOWN);
        ea->setKey(modifierKey(e));
        getEventQueue()->addEvent(ea);
    }
    else
    {
        getEventQueue()->keyPress(*(e->text().toLatin1().data()));
    }

}


void BaseViewer::keyReleaseSlot(QKeyEvent *e)
{
    if(e->text().isEmpty()) // modifier key
    {
        osgGA::GUIEventAdapter* ea = new osgGA::GUIEventAdapter;
        ea->setEventType(osgGA::GUIEventAdapter::KEYUP);
        ea->setKey(modifierKey(e));
        getEventQueue()->addEvent(ea);
    }
    else
    {
        getEventQueue()->keyRelease(*(e->text().toLatin1().data()));
    }
}

void BaseViewer::mousePressSlot(QMouseEvent *e)
{
    unsigned int button = this->button(e);
    getEventQueue()->mouseButtonPress(static_cast<float>(e->x()),
                                      static_cast<float>(e->y()),
                                      button);
}

void BaseViewer::mouseReleaseSlot(QMouseEvent *e)
{
    unsigned int button = this->button(e);
    getEventQueue()->mouseButtonRelease(static_cast<float>(e->x()),
                                        static_cast<float>(e->y()),
                                        button);
}


void BaseViewer::mouseMoveSlot(QMouseEvent *e)
{
    getEventQueue()->mouseMotion(static_cast<float>(e->x()),
                                 static_cast<float>(e->y()));
}

void BaseViewer::touchEventSlot(QEvent *event)
{
    switch(event->type())
    {
#if 0
    case QEvent::TouchBegin:
    {
        QTouchEvent* e = static_cast<QTouchEvent*>(event);
        float x = e->touchPoints()[0].pos().x();
        float y = e->touchPoints()[0].pos().y();
        getEventQueue()->touchBegan(0, osgGA::GUIEventAdapter::TOUCH_BEGAN, x, y);
        break;
    }
    case QEvent::TouchUpdate:
    {
        QTouchEvent* e = static_cast<QTouchEvent*>(event);
        float x = e->touchPoints()[0].pos().x();
        float y = e->touchPoints()[0].pos().y();
        getEventQueue()->touchBegan(0, osgGA::GUIEventAdapter::TOUCH_MOVED, x, y);
        break;
    }
    case QEvent::TouchEnd:
    case QEvent::TouchCancel:
    {
        QTouchEvent* e = static_cast<QTouchEvent*>(event);
        float x = e->touchPoints()[0].pos().x();
        float y = e->touchPoints()[0].pos().y();
        getEventQueue()->touchBegan(0, osgGA::GUIEventAdapter::TOUCH_ENDED, x, y);
        break;
    }
#else
    case QEvent::TouchBegin:
    {
        QTouchEvent* e = static_cast<QTouchEvent*>(event);
        unsigned int button = 1;
        float x = e->touchPoints()[0].pos().x();
        float y = e->touchPoints()[0].pos().y();
        getEventQueue()->mouseButtonPress(x, y, button);
        break;
    }
    case QEvent::TouchUpdate:
    {
        QTouchEvent* e = static_cast<QTouchEvent*>(event);
        unsigned int button = 1;
        float x = e->touchPoints()[0].pos().x();
        float y = e->touchPoints()[0].pos().y();
        getEventQueue()->mouseMotion(x, y, button);
        break;
    }
    case QEvent::TouchEnd:
    case QEvent::TouchCancel:
    {
        QTouchEvent* e = static_cast<QTouchEvent*>(event);
        unsigned int button = 1;
        float x = e->touchPoints()[0].pos().x();
        float y = e->touchPoints()[0].pos().y();
        getEventQueue()->mouseButtonRelease(x, y, button);
        break;
    }
    default:
        break;
    }
#endif
}


unsigned int BaseViewer::button(QMouseEvent* event)
{
    // 1 = left mouse button
    // 2 = middle mouse button
    // 3 = right mouse button

    unsigned int button = 0;

    switch(event->button())
    {
    case Qt::LeftButton:
        button = 1;
        break;

    case Qt::MiddleButton:
        button = 2;
        break;

    case Qt::RightButton:
        button = 3;
        break;

    default:
        break;
    }

    return button;
}


unsigned int BaseViewer::modifierKey(QKeyEvent *event)
{
    unsigned int key = 0;
    switch(event->key())
    {
    case Qt::Key_Shift:
        key = osgGA::GUIEventAdapter::KEY_Shift_L; //left key is chosen in an arbitrary way since Qt doesn't differ left or right
        break;
    case Qt::Key_Control :
        key = osgGA::GUIEventAdapter::KEY_Control_L; //left key is chosen in an arbitrary way since Qt doesn't differ left or right
        break;
    case Qt::Key_Alt :
        key = osgGA::GUIEventAdapter::KEY_Alt_L; //left key is chosen in an arbitrary way since Qt doesn't differ left or right
        break;
    }

    return key;
}


osgGA::EventQueue* BaseViewer::getEventQueue() const
{
    osgGA::EventQueue* eventQueue = _gw->getEventQueue();

    if(eventQueue)
        return eventQueue;
    else
    {
        qWarning() << "Unable to obtain valid event queue";
        return new osgGA::EventQueue;
    }
}


void BaseViewer::connectKeyEventSlots()
{
    QObject::connect(this, SIGNAL(keyPressSignal(QKeyEvent*)), this, SLOT(keyPressSlot(QKeyEvent*)));
    QObject::connect(this, SIGNAL(keyReleaseSignal(QKeyEvent*)), this, SLOT(keyReleaseSlot(QKeyEvent*)));

}

void BaseViewer::disconnectKeyEventSlots()
{
    QObject::disconnect(this, SIGNAL(keyPressSignal(QKeyEvent*)), this, SLOT(keyPressSlot(QKeyEvent*)));
    QObject::disconnect(this, SIGNAL(keyReleaseSignal(QKeyEvent*)), this, SLOT(keyReleaseSlot(QKeyEvent*)));

}
