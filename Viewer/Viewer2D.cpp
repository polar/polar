/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Viewer2D.h"
#include "DrawableMarkers2D.h"
#include "DrawablePolygon2D.h"
#include "DrawablePolyLine2D.h"
#include "DrawableSpline2D.h"
#include "DrawableBlob2D.h"
#include "DrawableArrows2D.h"
#include "DrawableText2D.h"
#include "Image.h"

using Qt::ShiftModifier;
using namespace PoLAR;


Viewer2D::Viewer2D(QWidget *parent, const char *name, WindowFlags f, bool distortion):
    GraphicsViewer(parent, name, f, distortion),
    _drawableObjectsList(NULL),
    _shareObjects(false),
    _saveFile(NULL),
    _loadFile(NULL)
{
}


Viewer2D::~Viewer2D()
{
    if (_saveFile) delete []_saveFile;
    if (_loadFile) delete []_loadFile;

    if (!_listDrawableObjectsArray.empty())
    {
        if (!_shareObjects)
            for(unsigned int i=0; i<_listDrawableObjectsArray.size(); i++)
            {
                if (_listDrawableObjectsArray[i]) delete _listDrawableObjectsArray[i];
            }
        else
            if (_listDrawableObjectsArray[0]) delete _listDrawableObjectsArray[0];
        _listDrawableObjectsArray.clear();
        _drawableObjectsList = NULL;
    }
}



void Viewer2D::setBgImage(PoLAR::BaseImage* img, bool resizeParent, bool shareObjects, bool resetObjects)
{
    GraphicsViewer::setBgImage(img, resizeParent);
    if (_image.valid() && resetObjects)
    {
        initShareObjects(shareObjects);
    }

    gotoImageSlot(0);
}


void Viewer2D::initShareObjects(bool shareObjects)
{
    if (!_listDrawableObjectsArray.empty())
    {
        if (_shareObjects)
        {
            if (_listDrawableObjectsArray[0]) delete _listDrawableObjectsArray[0];
        }
        else for (unsigned int i=0; i<_listDrawableObjectsArray.size(); i++)
            if (_listDrawableObjectsArray[i]) delete _listDrawableObjectsArray[i];

        _listDrawableObjectsArray.clear();
    }

    _shareObjects = shareObjects;

    if (!_shareObjects)
        for (int i=0; i<_nbima; i++)
            _listDrawableObjectsArray.push_back(new ListDrawableObject2D(this));
    else
    {
        _listDrawableObjectsArray.push_back(new ListDrawableObject2D(this));
        for (int i=1; i<_nbima; i++)
            _listDrawableObjectsArray.push_back(_listDrawableObjectsArray[0]);
    }

    _drawableObjectsList = _listDrawableObjectsArray[0];
    _drawableObjectsList->setCurrent();
}



void Viewer2D::selectObject2D(double x, double y)
{
    std::list<DrawableObject2D*> list = _drawableObjectsList->getObjects();
    std::list<DrawableObject2D*>::iterator it;

    // get the top most 2D object (if any) under the mouse cursor
    for(it = list.begin(); it != list.end(); ++it)
    {
        if((*it) && (*it)->contains(QPointF(x,y)))
        {
            _drawableObjectsList->selectObject(*it);
            break;
        }
    }
}


void Viewer2D::removeAllObjects2D()
{
    if(!_listDrawableObjectsArray.empty())
    {
        if (_shareObjects)
            _listDrawableObjectsArray[0]->clear();
        else
            for(unsigned int i=0; i<_listDrawableObjectsArray.size(); i++)
            {
                _listDrawableObjectsArray[i]->clear();
            }
    }
}


void Viewer2D::removeObject2D(double x, double y)
{
    std::list<DrawableObject2D*> list = _drawableObjectsList->getObjects();
    std::list<DrawableObject2D*>::iterator it;

    // get the top most drawable object (if any) under the mouse cursor
    for(it = list.begin(); it != list.end(); ++it)
    {
        if((*it) && (*it)->contains(QPointF(x,y)))
        {
            if((*it)->isSelected()) _drawableObjectsList->removeCurrentObject();
            break;
        }
    }
}


void Viewer2D::removeCurrentObject2D()
{
    _drawableObjectsList->removeCurrentObject();
}


void Viewer2D::startEditMarkersSlot()
{
    //stopEditImageSlot();
    if (_drawableObjectsList)
    {
        _drawableObjectsList->startEditMarkers();
    }
}

void Viewer2D::stopEditMarkersSlot()
{
    if (_drawableObjectsList) _drawableObjectsList->stopEditMarkers();
}

void Viewer2D::changeMarkerLabelSlot(float l)
{
    if (_drawableObjectsList) _drawableObjectsList->changeMarkerLabel(l);
}

void Viewer2D::changeMarkerLabelSlot(int l)
{
    float f(l);
    if (_drawableObjectsList) _drawableObjectsList->changeMarkerLabel(f);
}

void Viewer2D::selectMarkerSlot(Marker2D *m)
{
    emit selectMarkerSignal(m);
}

void Viewer2D::unselectMarkerSlot()
{
    emit unselectMarkerSignal();
}


int Viewer2D::getNumObjects2D() const
{
    return  _drawableObjectsList ? _drawableObjectsList->countObjects() : 0;
}


void Viewer2D::addObject2D(DrawableObject2D *obj)
{
    if (_drawableObjectsList)
        _drawableObjectsList->addObject(obj);
}


DrawableObject2D * Viewer2D::addMarkers(std::list<Marker2D *> list)
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addMarkers(list);
    return NULL;
}

DrawableObject2D * Viewer2D::addMarkers()
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addMarkers();
    return NULL;
}


DrawableObject2D * Viewer2D::addPolygon(std::list<Marker2D *> list)
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addPolygon(list);
    return NULL;
}


DrawableObject2D * Viewer2D::addPolygon()
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addPolygon();
    return NULL;
}


DrawableObject2D * Viewer2D::addPolyLine(std::list<Marker2D *> list)
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addPolyLine(list);
    return NULL;
}

DrawableObject2D * Viewer2D::addPolyLine()
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addPolyLine();
    return NULL;
}

DrawableObject2D * Viewer2D::addSpline(std::list<Marker2D *> list)
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addSpline(list);
    return NULL;
}

DrawableObject2D * Viewer2D::addSpline()
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addSpline();
    return NULL;
}

DrawableObject2D * Viewer2D::addBlob(std::list<Marker2D *> list)
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addBlob(list);
    return NULL;
}

DrawableObject2D * Viewer2D::addBlob()
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addBlob();
    return NULL;
}

DrawableObject2D * Viewer2D::addArrows(std::list<Marker2D *> list)
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addArrows(list);
    return NULL;
}

DrawableObject2D * Viewer2D::addArrows()
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addArrows();
    return NULL;
}

DrawableObject2D * Viewer2D::addText(std::list<Marker2D *> list, QString& text)
{
    if (_drawableObjectsList)
        return _drawableObjectsList->addText(list, text);
    return NULL;
}

DrawableObject2D * Viewer2D::addText(QString text)
{
    if(_drawableObjectsList)
        return _drawableObjectsList->addText(text);
    return NULL;
}


// add objects given a list of 2D markers on the image with index idxima
// do nothing if idxima is out of range
void Viewer2D::addObject2D(DrawableObject2D *obj, int idxima)
{
    if (idxima <-1 || idxima>=_nbima) return;
    if (_drawableObjectsList)
    {
        ListDrawableObject2D *tmp = _drawableObjectsList;
        _drawableObjectsList = _listDrawableObjectsArray[idxima==-1? getImageIndex(): idxima];
        _drawableObjectsList->addObject(obj);
        _drawableObjectsList = tmp;
    }
}

DrawableObject2D* Viewer2D::addMarkers(std::list<Marker2D *> list, int idxima)
{
    if (idxima <-1 || idxima>=_nbima) return NULL;
    if (_drawableObjectsList)
    {
        ListDrawableObject2D *tmp = _drawableObjectsList;
        _drawableObjectsList = _listDrawableObjectsArray[idxima==-1? getImageIndex(): idxima];
        DrawableObject2D* obj = _drawableObjectsList->addMarkers(list);
        _drawableObjectsList = tmp;
        return obj;
    }
    return NULL;
}

DrawableObject2D* Viewer2D::addPolygon(std::list<Marker2D *> list, int idxima)
{
    if (idxima <-1 || idxima>=_nbima) return NULL;
    if (_drawableObjectsList)
    {
        ListDrawableObject2D *tmp = _drawableObjectsList;
        _drawableObjectsList = _listDrawableObjectsArray[idxima==-1? getImageIndex(): idxima];
        DrawableObject2D* obj = _drawableObjectsList->addPolygon(list);
        _drawableObjectsList = tmp;
        return obj;
    }
    return NULL;
}

DrawableObject2D* Viewer2D::addPolyLine(std::list<Marker2D *> list, int idxima)
{
    if (idxima <-1 || idxima>=_nbima) return NULL;
    if (_drawableObjectsList)
    {
        ListDrawableObject2D *tmp = _drawableObjectsList;
        _drawableObjectsList = _listDrawableObjectsArray[idxima==-1? getImageIndex(): idxima];
        DrawableObject2D* obj = _drawableObjectsList->addPolyLine(list);
        _drawableObjectsList = tmp;
        return obj;
    }
    return NULL;
}

DrawableObject2D* Viewer2D::addSpline(std::list<Marker2D *> list, int idxima)
{
    if (idxima <-1 || idxima>=_nbima) return NULL;
    if (_drawableObjectsList)
    {
        ListDrawableObject2D *tmp = _drawableObjectsList;
        _drawableObjectsList = _listDrawableObjectsArray[idxima==-1? getImageIndex(): idxima];
        DrawableObject2D* obj = _drawableObjectsList->addSpline(list);
        _drawableObjectsList = tmp;
        return obj;
    }
    return NULL;
}

DrawableObject2D* Viewer2D::addBlob(std::list<Marker2D *> list, int idxima)
{
    if (idxima <-1 || idxima>=_nbima) return NULL;
    if (_drawableObjectsList)
    {
        ListDrawableObject2D *tmp = _drawableObjectsList;
        _drawableObjectsList = _listDrawableObjectsArray[idxima==-1? getImageIndex(): idxima];
        DrawableObject2D* obj = _drawableObjectsList->addBlob(list);
        _drawableObjectsList = tmp;
        return obj;
    }
    return NULL;
}


void Viewer2D::copyObjects2D(int srcImg, int destImg, bool deepCopy, bool clearDestList)
{
    if(!_listDrawableObjectsArray.empty() && srcImg>=0 && srcImg<_nbima && destImg>=0 && destImg<_nbima /*&& srcImg!=destImg*/)
    {
        if(_listDrawableObjectsArray[srcImg])
        {
            if(_listDrawableObjectsArray[destImg] && _listDrawableObjectsArray[destImg] != _listDrawableObjectsArray[srcImg])
            {
                if(clearDestList) _listDrawableObjectsArray[destImg]->clear();
            }
            else
                _listDrawableObjectsArray[destImg] = new ListDrawableObject2D(this);

            for(int i=0; i<_listDrawableObjectsArray[srcImg]->countObjects(); i++)
            {
                DrawableObject2D *srcObj = _listDrawableObjectsArray[srcImg]->getObjectByIndex(i);
                DrawableObject2D *obj = srcObj->clone(deepCopy);
                if(obj)
                {
                    _listDrawableObjectsArray[destImg]->addObject(obj);
                    _listDrawableObjectsArray[destImg]->deactivateCurrentObject();
                }
            }
        }
    }
}


void Viewer2D::copyObjects2D(int srcImg, bool deepCopy, bool clearDestList)
{
    if(!_shareObjects)
    {
        for(int i=0; i<_nbima; i++)
            if(i != srcImg)
                copyObjects2D(srcImg, i, deepCopy, clearDestList);
    }
}



void Viewer2D::copyCurrentObject2D(int destImg, bool deepCopy)
{
    if(!_listDrawableObjectsArray.empty() && destImg<_nbima)
    {
        DrawableObject2D *srcObj = _drawableObjectsList->getCurrentObject();
        if(srcObj)
        {
            DrawableObject2D *newObj = srcObj->clone(deepCopy);
            if(newObj)
            {
                _listDrawableObjectsArray[destImg]->addObject(newObj);
                _listDrawableObjectsArray[destImg]->deactivateCurrentObject();
            }
        }
    }
}


void Viewer2D::copyCurrentObject2D(bool deepCopy)
{
    if(!_shareObjects)
    {
        for(int i=0; i<_nbima; i++)
            if(i != getImageIndex())
                copyCurrentObject2D(i, deepCopy);
    }
}


void Viewer2D::saveMarkersSlot() const
{
    if (!_listDrawableObjectsArray.empty() && _saveFile)
    {
        char buf[512];
        // check if _saveFile corresponds to a format
        // in such case, markers are saved for all images
        // else we consider that the markers should be saved for the current image alone
        sprintf(buf, _saveFile, 0);
        if (strcmp(buf, _saveFile))
        {
            for (int i=0; i<_nbima; i++)
            {
                sprintf(buf, _saveFile, i);
                if (_listDrawableObjectsArray[i]) _listDrawableObjectsArray[i]->save(buf);
            }
        }
        else if (_drawableObjectsList) _drawableObjectsList->save(_saveFile);
    }
}



void Viewer2D::saveMarkersSlot(int imgId) const
{
    if (!_listDrawableObjectsArray.empty() && _saveFile)
        if (imgId < _nbima)
        {
            char buf[512];
            sprintf(buf, _saveFile, imgId);
            if (_listDrawableObjectsArray[imgId]) _listDrawableObjectsArray[imgId]->save(buf);
        }
}


void Viewer2D::saveObject2DAsSvg(QString& fname, int imgId) const
{
    if(!_listDrawableObjectsArray.empty() && imgId == -1) getDrawableObject2DList(getImageIndex())->saveAsSvg(fname);
    else if (!_listDrawableObjectsArray.empty() && imgId < _nbima)
    {
        getDrawableObject2DList(imgId)->saveAsSvg(fname);
    }
}


void Viewer2D::loadMarkersSlot()
{
    if (!_listDrawableObjectsArray.empty() && _loadFile)
    {
        char buf[512];
        // check if _loadFile corresponds to a format
        // in such case, markers are read onto all images
        // else we consider that the markers should be read for the current image alone
        sprintf(buf, _loadFile, 0);
        if (strcmp(buf, _loadFile))
        {
            for (int i=0; i<_nbima; i++)
                if (_listDrawableObjectsArray[i])
                {
                    sprintf(buf, _loadFile, i);
                    _listDrawableObjectsArray[i]->load(buf);
                }
        }
        else
            if (_drawableObjectsList) _drawableObjectsList->load(_loadFile);
    }
}

void Viewer2D::loadMarkersSlot(int imgId)
{
    if (!_listDrawableObjectsArray.empty() && _loadFile)
        if (imgId < _nbima && _listDrawableObjectsArray[imgId])
        {
            char buf[512];
            sprintf(buf, _loadFile, imgId);
            _listDrawableObjectsArray[imgId]->load(buf);
        }
}


void Viewer2D::readGrx(char *fname)
{
    if (_drawableObjectsList)
    {
        if (!_drawableObjectsList) qDebug("Viewer2D::readGrx() disconnect");
        QObject::disconnect(_drawableObjectsList, SIGNAL(updateSignal()), this, SLOT(update()));
    }

    enum PrimType {POINT=0, SEGMENT=1};
    const int NPrimType=2;
    enum PrimColors {W=0,R=1,G=2,B=3,C=4,Y=5,M=6};
    const int NPrimColors=7;

    std::list<Marker2D*> prims[NPrimType][NPrimColors];

    QColor col[NPrimColors]={ QColor("white"),
                              QColor("red"),
                              QColor("green"),
                              QColor("blue"),
                              QColor("cyan"),
                              QColor("yellow"),
                              QColor("magenta")
                            };

	std::ifstream ifs (fname);
    DrawableObject2D *o;
    std::list<Marker2D*> l;

    if (ifs.good())
    {
        int n;
        ifs >> n;
        char t, c;
        double x, y;
        PrimColors pc;
        while (!ifs.eof() && ifs.good() && n-- > 0)
        {
            ifs >> t; // primitive type
            ifs >> c; // color
            switch (c)
            {
            case 'r': pc=R; break;
            case 'g': pc=G; break;
            case 'b': pc=B; break;
            case 'c': pc=C; break;
            case 'y': pc=Y; break;
            case 'm': pc=M; break;
            case 'w': pc=W; break;
            default: pc=W;
            }
            switch(t)
            {
            case 's':
                ifs >> x >> y;
                prims[SEGMENT][pc].push_front(new Marker2D(x,y,0));
                ifs >> x >> y;
                prims[SEGMENT][pc].push_front(new Marker2D(x,y,0));
                break;
            case 'p':
                ifs >> x >> y;
                prims[POINT][pc].push_front(new Marker2D(x,y,0));
                break;
            case 'P':
                int nbpts;
                l.clear();
                ifs >> nbpts;
                while (nbpts-->0)
                {
                    ifs >> x >> y;
                    l.push_front(new Marker2D(x,y,0));
                }
                o = new DrawablePolyLine2D(l,this);
                o->editOff();
                o->setUnselectColor(col[pc]);
                addObject2D(o);
                break;
            }
        }
    }
    ifs.close();

    int j;
    for (j=0; j<NPrimColors; j++)
    {
        l=prims[SEGMENT][j];
        if(l.size() > 0)
        {
            o=new DrawableArrows2D(l,this);
            o->editOff();
            o->setUnselectColor(col[j]);
            addObject2D(o);
        }
    }
    for (j=0; j<NPrimColors; j++)
    {
        l=prims[POINT][j];
        if(l.size() > 0)
        {
            o=new DrawableMarkers2D(l,this);
            o->editOff();
            o->setUnselectColor(col[j]);
            addObject2D(o);
        }
    }
    if (_drawableObjectsList) QObject::connect(_drawableObjectsList, SIGNAL(updateSignal()), this, SLOT(update()));
}


void Viewer2D::saveAsSvg(QString &fname)
{
    if(!fname.isEmpty())
    {
        QSvgGenerator gen;
        gen.setFileName(fname);
        gen.setDescription(QString("Generated from PoLAR"));
        gen.setTitle(objectName());
        gen.setSize(QSize(width(), height()));
        gen.setViewBox(QRect(0, 0, width(), height()));
        QPainter painter;
        painter.begin(&gen);
        requestRedraw();
        render(&painter);
        painter.end();
    }
}

// display/undisplay current object (true or false)
void Viewer2D::setCurrentObject2DDisplay(bool b)
{
    if (_drawableObjectsList) _drawableObjectsList->setCurrentObjectDisplay(b);
}

// sets current object editable/uneditable (true or false)
void Viewer2D::setCurrentObject2DEdition(bool b)
{
    if (_drawableObjectsList) _drawableObjectsList->setCurrentObjectEdition(b);
}

void Viewer2D::gotoImageSlot(int n)
{
    if (_drawableObjectsList && !_listDrawableObjectsArray.empty())
    {
      bool maa=_drawableObjectsList->markersAreActive();
      if (!_shareObjects)
	{
	  stopEditMarkersSlot();
	  _drawableObjectsList->setCurrent(false);
	}
      GraphicsViewer::gotoImageSlot(n);
      if (!_shareObjects)
	{
	  _drawableObjectsList = _listDrawableObjectsArray[getImageIndex()];
	  if (maa) startEditMarkersSlot();
	  _drawableObjectsList->setCurrent();
	}
    }
    update();
    emit imageIndexSignal(getImageIndex());
}

void Viewer2D::nextImageSlot(bool wrap)
{
    if (_drawableObjectsList && !_listDrawableObjectsArray.empty())
    {
      bool maa=_drawableObjectsList->markersAreActive();
      if (!_shareObjects)
	{
	  stopEditMarkersSlot();
	  _drawableObjectsList->setCurrent(false);
	}
        GraphicsViewer::nextImageSlot(wrap);
      if (!_shareObjects)
	{
	  _drawableObjectsList = _listDrawableObjectsArray[getImageIndex()];
	  if (maa) startEditMarkersSlot();
	  _drawableObjectsList->setCurrent();
	}
    }
    update();
    emit imageIndexSignal(getImageIndex());
}

void Viewer2D::previousImageSlot(bool wrap)
{
    if (_drawableObjectsList && !_listDrawableObjectsArray.empty())
    {
      bool maa=_drawableObjectsList->markersAreActive();
      if (!_shareObjects)
	{
	  stopEditMarkersSlot();
	  _drawableObjectsList->setCurrent(false);
	}
      GraphicsViewer::previousImageSlot(wrap);
      if (!_shareObjects)
	{
	  _drawableObjectsList = _listDrawableObjectsArray[getImageIndex()];
	  if (maa) startEditMarkersSlot();
	  _drawableObjectsList->setCurrent();
	}
    }
    update();
    emit imageIndexSignal(getImageIndex());
}

