/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Viewer.h"
#include "Image.h"
#include <QScreen>
#include <QGuiApplication>

using namespace PoLAR;


const std::string Viewer::_selectObj2D = "selectObject2D";
const std::string Viewer::_deleteObj2D = "deleteObject2D";
const std::string Viewer::_addMarker = "addMarker";
const std::string Viewer::_deleteMarker = "deleteMarker";
const std::string Viewer::_selectMarker = "selectMarker";
const std::string Viewer::_moveMarker = "moveMarker";
const std::string Viewer::_releaseMarker = "releaseMarker";
const std::string Viewer::_panMove = "movePan";
const std::string Viewer::_panPress = "pressPan";
const std::string Viewer::_zoomMove = "moveZoom";
const std::string Viewer::_zoomPress = "pressZoom";
const std::string Viewer::_wlMove = "moveWL";
const std::string Viewer::_wlPress = "pressWL";


Viewer::Viewer(QWidget *parent, const char *name, WindowFlags f, bool shadows, bool distortion):
    Viewer2D3D(parent, name, f, shadows, distortion),
    _currentMode(NOMODE)
{
    createEventsMap();
    updateTypesMap();
#if(MOBILE_PLATFORM)
    grabGesture(Qt::SwipeGesture);
    grabGesture(Qt::PanGesture);
    grabGesture(Qt::PinchGesture);
    grabGesture(Qt::TapAndHoldGesture);
    grabGesture(Qt::TapGesture);
    setAttribute(Qt::WA_AcceptTouchEvents);
    viewport()->setAttribute(Qt::WA_AcceptTouchEvents);
    qApp->setAttribute(Qt::AA_SynthesizeMouseForUnhandledTouchEvents, false);
    qApp->setAttribute(Qt::AA_SynthesizeTouchForUnhandledMouseEvents, true);

    QScreen * screen = QGuiApplication::primaryScreen();
    connect(screen, SIGNAL(orientationChanged(Qt::ScreenOrientation)), this, SLOT(onRotate(Qt::ScreenOrientation)));
    screen->setOrientationUpdateMask(Qt::PortraitOrientation| Qt::LandscapeOrientation| Qt::InvertedPortraitOrientation| Qt::InvertedLandscapeOrientation);
#endif
}


Viewer::~Viewer()
{
    clearEventsMap();
}


void Viewer::keyPressEvent(QKeyEvent *event)
{
    Viewer2D3D::keyPressEvent(event);
    filterEvent(event);
}


void Viewer::keyReleaseEvent(QKeyEvent *event)
{
    Viewer2D3D::keyReleaseEvent(event);
    filterEvent(event);
}


bool Viewer::viewportEvent(QEvent *event)
{
    if(event->type() == QEvent::UpdateLater || event->type() == QEvent::Paint || event->type() == QEvent::UpdateRequest || event->type() == QEvent::Resize)
        return Viewer2D3D::viewportEvent(event);

    filterEvent(event);
    return Viewer2D3D::viewportEvent(event);
}


bool Viewer::event(QEvent *event)
{
    bool ret = filterEvent(event);
    if(ret) return ret;
    return Viewer2D3D::event(event);
}

bool Viewer::filterEvent(QEvent *event)
{
    bool ret = false;
    auto itfind = _typesMap.find(event->type());
    if(itfind != _typesMap.end())
    {
        std::list<PolarEvent*> evList = itfind->second;

        for(auto itr : evList)
        {
            if(_currentMode == itr->mode)
            {
                double x=0.0, y=0.0;
                int modifiers = Qt::NoModifier;
                int buttons = Qt::NoButton;

                switch(event->type())
                {
                // Touchscreen gestures
                // Gestures don't seem do be very efficient. By default we use them for zooming, which works OK. Pan and swipe are not very robust
                case QEvent::Gesture:
                {
                    QGestureEvent* castedEvent = static_cast<QGestureEvent*>(event);
                    QGesture* gesture = castedEvent->gestures()[0];
                    buttons = gesture->gestureType();
                    switch(buttons)
                    {
                    case Qt::PinchGesture:
                    {
                        QPinchGesture* pinch = static_cast<QPinchGesture*>(gesture);
                        x = pinch->scaleFactor();
                        y = pinch->scaleFactor();
                        break;
                    }
                    case Qt::TapAndHoldGesture:
                    {
                        QTapAndHoldGesture* tap = static_cast<QTapAndHoldGesture*>(gesture);
                        x = tap->position().x();
                        y = tap->position().y();
                        break;
                    }
                    case Qt::TapGesture:
                    {
                        QTapGesture* tap = static_cast<QTapGesture*>(gesture);
                        x = tap->position().x();
                        y = tap->position().y();
                        break;
                    }
                    case Qt::PanGesture:
                    {
                        QPanGesture* pan = static_cast<QPanGesture*>(gesture);
                        x = pan->delta().x();
                        y = pan->delta().y();
                        break;
                    }
                    case Qt::SwipeGesture:
                    {
                        QSwipeGesture* swipe = static_cast<QSwipeGesture*>(gesture);
                        switch(swipe->horizontalDirection())
                        {
                        case QSwipeGesture::Left:
                            x = -1.0;
                            break;
                        case QSwipeGesture::Right:
                            x = 1.0;
                            break;
                        default:
                            break;
                        }
                        switch(swipe->verticalDirection())
                        {
                        case QSwipeGesture::Down:
                            y = -1.0;
                            break;
                        case QSwipeGesture::Up:
                            y = 1.0;
                            break;
                        default:
                            break;
                        }
                        break;
                    }
                    default:
                        break;
                    }

                    break;
                }

                    // classic mouse events
                case QEvent::MouseButtonPress:
                case QEvent::MouseButtonDblClick:
                case QEvent::MouseButtonRelease:
                case QEvent::MouseMove:
                {
                    QMouseEvent* castedEvent = static_cast<QMouseEvent*>(event);
                    x = castedEvent->x();
                    y = castedEvent->y();
                    modifiers = castedEvent->modifiers();
                    buttons = (castedEvent->type() == QEvent::MouseMove) ? castedEvent->buttons() : castedEvent->button();
                    break;
                }
                case QEvent::Wheel:
                {
                    QWheelEvent* castedEvent = static_cast<QWheelEvent*>(event);
                    x = castedEvent->angleDelta().x();
                    y = castedEvent->angleDelta().y();
                    modifiers = castedEvent->modifiers();
                    buttons = castedEvent->buttons();
		    break;
                }

                    // Classic key events
                case QEvent::KeyPress:
                case QEvent::KeyRelease:
                {
                    QKeyEvent* castedEvent = static_cast<QKeyEvent*>(event);
                    modifiers = castedEvent->modifiers();
                    buttons = castedEvent->key();
                    break;
                }

                    // Touchscreen events
                case QEvent::TouchBegin:
                case QEvent::TouchUpdate:
                case QEvent::TouchEnd:
                {
                    QTouchEvent* castedEvent = static_cast<QTouchEvent*>(event);
                    castedEvent->accept();
                    // For now we limit the touch events to one-finger touches only
                    if(castedEvent->touchPoints().size() > 1) break;
                    modifiers = castedEvent->modifiers();
                    buttons = castedEvent->touchPointStates();
                    x = castedEvent->touchPoints()[0].pos().x();
                    y = castedEvent->touchPoints()[0].pos().y();
                    ret = true;
                    break;
                }
                case QEvent::TouchCancel:
                default:
                    break;
                }

                if((modifiers == itr->modifier) && (buttons == itr->keyOrButton))
                {
                    (this->*itr->fct)(x, y);
                    //return;
                }
            }
        }
    }
    return ret;
}


void Viewer::showEvent(QShowEvent *event)
{
    Viewer2D3D::showEvent(event);
    int w, h;
#if MOBILE_PLATFORM
    QRect rect = qApp->desktop()->screenGeometry();
    w = rect.width();
    h = rect.height();
#else
    w = width();
    h = height();
#endif
    if(!getBgImage()) setBgImage(new Image<unsigned char>(NULL, w, h, 1));
    if(getExtrinsics().isIdentity())
    {
        osg::Matrixd P = createVisionProjection(width(), height());
        setProjection(P);
    }
}


void Viewer::onRotate(Qt::ScreenOrientation)
{
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect r = screen->availableGeometry();
    resize(r.width(), r.height());
}


void Viewer::setBgImage(BaseImage *img, bool resizeParent, bool shareObjects, bool resetObjects)
{
  Viewer2D3D::setBgImage(img, resizeParent, shareObjects, resetObjects);
}


template<typename T> void Viewer::setBgImage(T *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects)
{
  setBgImage(new Image<T>(sequence, w, h, d, nima), resizeParent, shareObjects, resetObjects);
}
template void PoLAR_EXPORT Viewer::setBgImage<float>(float *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);
template void PoLAR_EXPORT Viewer::setBgImage<double>(double *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);
template void PoLAR_EXPORT Viewer::setBgImage<char>(char *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);
template void PoLAR_EXPORT Viewer::setBgImage<unsigned char>(unsigned char *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);
template void PoLAR_EXPORT Viewer::setBgImage<short>(short *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);
template void PoLAR_EXPORT Viewer::setBgImage<unsigned short>(unsigned short *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);
template void PoLAR_EXPORT Viewer::setBgImage<int>(int *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);
template void PoLAR_EXPORT Viewer::setBgImage<unsigned int>(unsigned int *sequence, int w, int h, int d, int nima, bool resizeParent, bool shareObjects, bool resetObjects);


void Viewer::startWindowLevelSlot(BaseImage *image)
{
    if(image->valid())
    {
        QObject::connect(this, SIGNAL(mousePressSignal(QMouseEvent*)), image, SLOT(mousePressSlot(QMouseEvent*)));
        QObject::connect(this, SIGNAL(mouseReleaseSignal(QMouseEvent*)), image, SLOT(mouseReleaseSlot(QMouseEvent*)));
    }
}


void Viewer::stopWindowLevelSlot(BaseImage *image)
{
    if(image->valid())
    {
        QObject::disconnect(this, SIGNAL(mousePressSignal(QMouseEvent*)), image, SLOT(mousePressSlot(QMouseEvent*)));
        QObject::disconnect(this, SIGNAL(mouseReleaseSignal(QMouseEvent*)), image, SLOT(mouseReleaseSlot(QMouseEvent*)));
    }
}


void Viewer::startEditImageSlot()
{
    Viewer2D3D::stopManipulateSceneSlot();
    Viewer2D3D::stopEditMarkersSlot();
    /*if (_image.valid() && _image->valid())
    {
        startWindowLevelSlot(_image.get());
    }*/
    _currentMode = IMAGE;
    emit editionModeChanged();
}

void Viewer::stopEditImageSlot()
{
    /* if (_image.valid() && _image->valid())
    {
        stopWindowLevelSlot(_image.get());
    }*/
}


void Viewer::startEditMarkersSlot()
{
    Viewer2D3D::stopManipulateSceneSlot();
    stopEditImageSlot();
    Viewer2D3D::startEditMarkersSlot();
    _currentMode = OBJECTS2D;
    emit editionModeChanged();
}


void Viewer::startManipulateSceneSlot()
{
    stopEditImageSlot();
    Viewer2D3D::stopEditMarkersSlot();
    Viewer2D3D::startManipulateSceneSlot();
    _currentMode = SCENE3D;
    emit editionModeChanged();
}


void Viewer::stopEditionSlot()
{
    stopEditImageSlot();
    stopEditMarkersSlot();
    stopManipulateSceneSlot();
    _currentMode = NOMODE;
    emit editionModeChanged();

}

void Viewer::startEditionModeSlot(EditionMode mode)
{
    switch(mode)
    {
    case IMAGE:
        startEditImageSlot();
        break;
    case OBJECTS2D:
        startEditMarkersSlot();
        break;
    case SCENE3D:
        startManipulateSceneSlot();
        break;
    default:
        break;
    }
}


void Viewer::updateEventManipulation(std::string key, Qt::KeyboardModifier m, int b)
{
    _eventsMap[key]->modifier = m;
    _eventsMap[key]->keyOrButton = b;
}

void Viewer::setPanManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_panMove, m, b);
    updateEventManipulation(_panPress, m, b);
}

void Viewer::setZoomManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_zoomMove, m, b);
    updateEventManipulation(_zoomPress, m, b);

}

void Viewer::setZoomMode(PanZoom::ZoomMode mode, float precision)
{
    Viewer2D3D::setZoomMode(mode, precision);
    if(mode == PanZoom::DRAG)
    {
        _eventsMap[_zoomMove]->type = QEvent::MouseMove;
        _eventsMap[_zoomMove]->modifier = Qt::NoModifier;
        _eventsMap[_zoomMove]->keyOrButton = Qt::RightButton;
        _eventsMap[_zoomPress]->type = QEvent::MouseButtonPress;
        _eventsMap[_zoomPress]->modifier = Qt::NoModifier;
        _eventsMap[_zoomPress]->keyOrButton = Qt::RightButton;
    }
    else if(mode == PanZoom::SCROLL)
    {
        _eventsMap[_zoomMove]->type = QEvent::Wheel;
        _eventsMap[_zoomMove]->modifier = Qt::NoModifier;
        _eventsMap[_zoomMove]->keyOrButton = Qt::NoButton;
    }
#if(MOBILE_PLATFORM)
    else if(mode == PanZoom::PINCH)
    {
        _eventsMap[_zoomMove]->type = QEvent::Gesture;
        _eventsMap[_zoomMove]->modifier = Qt::NoModifier;
        _eventsMap[_zoomMove]->keyOrButton = Qt::PinchGesture;
        _eventsMap[_zoomPress]->type = QEvent::TouchBegin;
        _eventsMap[_zoomPress]->modifier = Qt::NoModifier;
        _eventsMap[_zoomPress]->keyOrButton = Qt::NoButton;
    }
#endif
    updateTypesMap();
}


void Viewer::clearEventsMap()
{
    for(auto it : _eventsMap)
        delete it.second;
    _eventsMap.clear();
    _typesMap.clear();
}

void Viewer::resetInteractionMethodsToDefault()
{
    clearEventsMap();
    createEventsMap();
    updateTypesMap();
}

void Viewer::setWindowLevelManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_wlMove, m, b);
    updateEventManipulation(_wlPress, m, b);
}

void Viewer::setSelectObject2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_selectObj2D, m, b);
}

void Viewer::setDeleteObject2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_deleteObj2D, m, b);
}

void Viewer::setAddMarker2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_addMarker, m, b);
}

void Viewer::setDeleteMarker2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_deleteMarker, m, b);
}

void Viewer::setSelectMarker2DManipulation(Qt::KeyboardModifier m, Qt::MouseButton b)
{
    updateEventManipulation(_selectMarker, m, b);
    updateEventManipulation(_moveMarker, m, b);
    updateEventManipulation(_releaseMarker, m, b);
}


void Viewer::createEventsMap()
{
#if(MOBILE_PLATFORM)
    _eventsMap[_selectObj2D] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::ShiftModifier, Qt::LeftButton, &Viewer::selectObject2D);
    _eventsMap[_deleteObj2D] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::ShiftModifier, Qt::RightButton, &Viewer::removeObject2D);
    _eventsMap[_selectMarker] = new PolarEvent(OBJECTS2D, QEvent::TouchBegin, Qt::NoModifier, Qt::TouchPointPressed, &Viewer::selectMarker2D);
    //_eventsMap[_addMarker] = new PolarEvent(OBJECTS2D, QEvent::Gesture, Qt::NoModifier, Qt::TapAndHoldGesture, &Viewer::addMarker2D);
    _eventsMap[_addMarker] = new PolarEvent(OBJECTS2D, QEvent::TouchEnd, Qt::NoModifier, Qt::TouchPointReleased, &Viewer::addMarker2D);
    _eventsMap[_deleteMarker] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::NoModifier, Qt::RightButton, &Viewer::deleteMarker2D);
    _eventsMap[_moveMarker] = new PolarEvent(OBJECTS2D, QEvent::TouchUpdate, Qt::NoModifier, Qt::TouchPointMoved, &Viewer::moveMarker2D);
    _eventsMap[_releaseMarker] = new PolarEvent(OBJECTS2D, QEvent::TouchEnd, Qt::NoModifier, Qt::TouchPointReleased, &Viewer::releaseMarker2D);
    _eventsMap[_panMove] = new PolarEvent(IMAGE, QEvent::TouchUpdate, Qt::NoModifier, Qt::TouchPointMoved, &Viewer::setPanMove);
    _eventsMap[_panPress] = new PolarEvent(IMAGE, QEvent::TouchBegin, Qt::NoModifier, Qt::TouchPointPressed, &Viewer::setPanPress);
    _eventsMap[_zoomMove] = new PolarEvent(IMAGE, QEvent::Gesture, Qt::NoModifier, Qt::PinchGesture, &Viewer::setZoomMove);
    _eventsMap[_zoomPress] = new PolarEvent(IMAGE, QEvent::TouchBegin, Qt::NoModifier, Qt::NoButton, &Viewer::setZoomPress);
    _eventsMap[_wlMove] = new PolarEvent(IMAGE, QEvent::MouseMove, Qt::NoModifier, Qt::MiddleButton, &Viewer::setWindowLevelMove);
    _eventsMap[_wlPress] = new PolarEvent(IMAGE, QEvent::MouseButtonPress, Qt::NoModifier, Qt::MiddleButton, &Viewer::setWindowLevelPress);
#else
    _eventsMap[_selectObj2D] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::ShiftModifier, Qt::LeftButton, &Viewer::selectObject2D);
    _eventsMap[_deleteObj2D] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::ShiftModifier, Qt::RightButton, &Viewer::removeObject2D);
    _eventsMap[_addMarker] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::NoModifier, Qt::MiddleButton, &Viewer::addMarker2D);
    _eventsMap[_deleteMarker] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::NoModifier, Qt::RightButton, &Viewer::deleteMarker2D);
    _eventsMap[_selectMarker] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonPress, Qt::NoModifier, Qt::LeftButton, &Viewer::selectMarker2D);
    _eventsMap[_moveMarker] = new PolarEvent(OBJECTS2D, QEvent::MouseMove, Qt::NoModifier, Qt::LeftButton, &Viewer::moveMarker2D);
    _eventsMap[_releaseMarker] = new PolarEvent(OBJECTS2D, QEvent::MouseButtonRelease, Qt::NoModifier, Qt::LeftButton, &Viewer::releaseMarker2D);
    _eventsMap[_panMove] = new PolarEvent(IMAGE, QEvent::MouseMove, Qt::NoModifier, Qt::LeftButton, &Viewer::setPanMove);
    _eventsMap[_panPress] = new PolarEvent(IMAGE, QEvent::MouseButtonPress, Qt::NoModifier, Qt::LeftButton, &Viewer::setPanPress);
    _eventsMap[_zoomMove] = new PolarEvent(IMAGE, QEvent::MouseMove, Qt::NoModifier, Qt::RightButton, &Viewer::setZoomMove);
    _eventsMap[_zoomPress] = new PolarEvent(IMAGE, QEvent::MouseButtonPress, Qt::NoModifier, Qt::RightButton, &Viewer::setZoomPress);
    _eventsMap[_wlMove] = new PolarEvent(IMAGE, QEvent::MouseMove, Qt::NoModifier, Qt::MiddleButton, &Viewer::setWindowLevelMove);
    _eventsMap[_wlPress] = new PolarEvent(IMAGE, QEvent::MouseButtonPress, Qt::NoModifier, Qt::MiddleButton, &Viewer::setWindowLevelPress);
#endif
}

void Viewer::updateTypesMap()
{
    _typesMap.clear();
    for(auto const it : _eventsMap)
    {
        _typesMap[it.second->type].push_back(it.second);
    }
}
