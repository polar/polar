/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Viewer2D3D.h"
#include "SceneGraphManipulator.h"
#include "EpipolarPickHandler.h"
#include "config.h"
#include <osg/Matrixd>

using namespace PoLAR;


Viewer2D3D::Viewer2D3D(QWidget *parent, const char *name, WindowFlags f, bool shadows, bool distortion):
    Viewer2D(parent, name, f, distortion),
    _sceneGraph(new SceneGraph(shadows)),
    _extrinsicMat(osg::Matrixd::identity()),
    _projectionType(UNKNOWN),
    _currentPickedObject(NULL)
{
    setFocusPolicy(Qt::StrongFocus);

    // set well known masks to be able to avoid some parts of the scene graph during specific traversals (especially set for the pick handler)
    _panZoom->getCamera()->setNodeMask(PICKHANDLER_MASK);
    _sceneGraph->getLightGroup()->setNodeMask(PICKHANDLER_MASK);

    // add the scenegraph to the OSG scene data
    _sceneGraph->getSceneGraph()->addChild(_panZoom->getCamera().get());
    _osgQtViewer->setSceneData(_sceneGraph->getSceneGraph());

    // add the default headlight to the scenegraph, link it to the camera so that it always lightens the scene from any point of view
    osg::ref_ptr<osg::LightSource> lightSource = new osg::LightSource;
    lightSource->setLight(getOsgViewer()->getLight());
    osg::ref_ptr<Light> light = new Light(lightSource.get(), false);
    light->setAsDirectional();
    light->setReferenceFrameVideo();
    _sceneGraph->addLight(light.get());
}


Viewer2D3D::~Viewer2D3D()
{
}


osg::Matrixd Viewer2D3D::createVisionProjection(int imgWidth, int imgHeight)
{
    osg::Vec3 center = _sceneGraph->getScene()->getBound().center();
    float radius = _sceneGraph->getScene()->getBound().radius();
    double K = 3;
    double alpha = (imgWidth < imgHeight ? imgWidth : imgHeight) * K/2.0;
    osg::Matrixd p = Util::projectionSetParameters(imgWidth/2., imgHeight/2., alpha, alpha, 0, 0, 0,
                                                   -center[0], -center[1], -center[2]+K*radius);
    return p;
}



osg::Matrixd Viewer2D3D::createVisionProjection(int imgWidth, int imgHeight, osg::Vec3f center, float radius)
{
    double K = 3;
    double alpha = (imgWidth < imgHeight ? imgWidth : imgHeight) * K/2.0;
    osg::Matrixd p = Util::projectionSetParameters(imgWidth/2., imgHeight/2., alpha, alpha, 0, 0, 0,
                                                   -center[0], -center[1], -center[2]+K*radius);
    return p;
}


osg::Matrixd Viewer2D3D::createAngioProjection(int imgWidth, int imgHeight)
{
    osg::Vec3 center = _sceneGraph->getScene()->getBound().center();
    float radius = _sceneGraph->getScene()->getBound().radius();
    double K = 3;
    double alpha = (imgWidth < imgHeight ? imgWidth : imgHeight) * K/2.0;
    osg::Matrixd p = Util::projectionSetParameters(imgWidth/2., imgHeight/2., alpha, alpha, 0, 0, 0,
                                                   -center[0], -center[1], -center[2] - K*radius);
    return p;
}



osg::Matrixd Viewer2D3D::createAngioProjection(int imgWidth, int imgHeight, osg::Vec3f center, float radius)
{
    double K = 3;
    double alpha = (imgWidth < imgHeight ? imgWidth : imgHeight) * K/2.0;
    osg::Matrixd p = Util::projectionSetParameters(imgWidth/2., imgHeight/2., alpha, alpha, 0, 0, 0,
                                                   -center[0], -center[1], -center[2] - K*radius);
    return p;
}


void Viewer2D3D::setDefaultProjection(ProjectionType pt)
{
  osg::Matrixd M;
  switch(pt)
    {
    case ANGIO:
      {
	M = createAngioProjection(width(), height());
	break;
      }
    case ANGIOHINV:
      {
	M = createAngioProjection(width(), height());
	osg::Matrixd A(-1, 0, 0, 0,
		       0, 1, 0, 0,
		       width(), 0, 1, 0,
		       0, 0, 0, 1);
	M = M * A;
	break;
      }
    default:
      M = createVisionProjection(width(), height());
    }
  setProjection(M,pt);
}


void Viewer2D3D::setProjection(osg::Matrixd M, ProjectionType pt)
{
    _projectionType = pt;

    osg::Matrixd projMat(M);
    // swap the image to get physically correct extrinsics. According to Toscani's formulae, the effect of such a swap
    // is to change the sign of the last two lines of the extrinsic matrix. It does not depend on the value of the image
    // width, as long as it is not null
    if (pt == ANGIOHINV)
    {
        osg::Matrixd swap(-1,0,0,0,
                          0,1,0,0,
                          1,0,1,0,
                          0,0,0,1);
        projMat *= swap;
    }

    // make sure 3rd line of the 3x3 left part is a unit vector and that the determinant of this 3x3 left part is positive (useful ???)
    Util::normalizeProjectionMatrix(projMat);

    // extract the external parameters
    osg::Matrixd wc2vc;
    Util::projectionWc2vcToscani(projMat, wc2vc);

    switch (pt)
    {
    case ANGIO: case ANGIOHINV: break;
    default: // VISION. Remember UNKNOWN is treated as VISION
        // invert last two lines: last line necessary since the SceneView->CullVisitor assumes that near and far clipping planes
        // lie at negative Z. Will dig that further later but the principle seems to be there.
        // second line is there to keep a direct reference frame
        osg::Matrixd invMat(1,0,0,0,
                            0,-1,0,0,
                            0,0,-1,0,
                            0,0,0,1);
        wc2vc *= invMat;
        break;
    }

    // invert extrinsics [R,T]->[R^t,-R^tT]
    osg::Matrixd vc2wc;
    vc2wc.invert(wc2vc);

    // right multiply the projection matrix by vc2wc (inverse of the extrinsics) : it gives the intrinsics
    projMat = vc2wc * projMat;

    if (pt == ANGIOHINV)
    {
        osg::Matrixd swap(-1, 0, 0, 0,
                          0, 1, 0, 0,
                          1, 0, 1, 0,
                          0, 0, 0, 1);
        projMat *= swap;
    }
    if (pt == ANGIO || pt == ANGIOHINV)
    {
        // multiply by -1
        projMat *= osg::Matrix::scale(-1, -1, -1);
    }

    // put the matrices in their places to be
    int fact = 1;
    if (pt == ANGIO || pt == ANGIOHINV) fact = -1;

    // store the intrinsic mat with special care to invert Z for angio and angiohinv cases
    // we also need to swap the last 2 columns
    osg::Matrixd intrinsicMat;
    intrinsicMat.set(projMat(0,0), projMat(0,1), 0, projMat(0,2),
                     projMat(1,0), projMat(1,1), 0, projMat(1,2),
                     projMat(2,0), projMat(2,1), 0, projMat(2,2),
                     projMat(3,0), projMat(3,1), fact, projMat(3,2));

    _panZoom->setIntrinsics(intrinsicMat);
    _extrinsicMat.set(wc2vc);

    setupManipulator(getOsgViewer()->getCameraManipulator());

    if (pt == ANGIO || pt == ANGIOHINV) getOsgViewer()->getLight()->setPosition(osg::Vec4f(0,0,-1,0));
    else getOsgViewer()->getLight()->setPosition(osg::Vec4f(0,0,1,0));

    // to update camera position needed for epipolar edition computation
    osgViewer::View::EventHandlers::iterator itr;
    osgViewer::View::EventHandlers eventHandlers = getOsgViewer()->getEventHandlers();
    EpipolarPickHandler* epipolPick = NULL;
    for (itr=eventHandlers.begin(); itr != eventHandlers.end(); ++itr)
    {
        epipolPick = dynamic_cast<PoLAR::EpipolarPickHandler*>((*itr).get());
        if (epipolPick)
            epipolPick->reinitializeCamera();
    }

    update();
}



osg::Matrixd Viewer2D3D::getProjection() const
{
    osg::Matrixd p = getOsgViewer()->getCamera()->getViewMatrix() * _panZoom->getIntrinsics();
    // Multiplication is inverted due to matrix transposition
    // invert the last 2 lines
    osg::Matrixd result(p(0,0), p(0,1), p(0,3), p(0,2),
                        p(1,0), p(1,1), p(1,3), p(1,2),
                        p(2,0), p(2,1), p(2,3), p(2,2),
                        p(3,0), p(3,1), p(3,3), p(3,2));

    return result;
}


osg::Matrixd Viewer2D3D::getPose() const
{
    osg::Matrixd wc2vc;
    osg::Matrixd M = getProjection();
    Util::projectionWc2vcToscani(M, wc2vc);

    osg::Matrixd extrinsicMat;
    extrinsicMat.set(wc2vc);
    return extrinsicMat;
}


void Viewer2D3D::setPose(osg::Matrixd P)
{
    _extrinsicMat.set(P);
    osg::Matrixd invMat(1,0,0,0,
                        0,-1,0,0,
                        0,0,-1,0,
                        0,0,0,1);
    _extrinsicMat *= invMat;
    //_osgQtViewer->getCamera()->setViewMatrix(_extrinsicMat);
    osg::Vec3d eye, center, up;
    _extrinsicMat.getLookAt(eye, center, up);
    getCameraManipulator()->setHomePosition(eye, center, up);
    getCameraManipulator()->home(0);
}

void Viewer2D3D::setIntrinsics(osg::Matrixd P)
{
    _panZoom->setIntrinsics(P);
}

void Viewer2D3D::stopManipulateSceneSlot()
{
    osgViewer::View::EventHandlers eventHandlers = getOsgViewer()->getEventHandlers();
    osgViewer::View::EventHandlers::iterator itrL;
    for (itrL=eventHandlers.begin(); itrL != eventHandlers.end(); ++itrL)
    {
        Object3DPickHandler* sgPick = dynamic_cast<Object3DPickHandler*>((*itrL).get());
        GeometryPickHandler* geomPick = dynamic_cast<GeometryPickHandler*>((*itrL).get());

        if (sgPick) {sgPick->setInteractMode(Object3DPickHandler::VIEW); sgPick->cleanUp(this);}
        if (geomPick) {geomPick->setInteractMode(GeometryPickHandler::VIEW); geomPick->cleanUp();}
    }
    QObject::disconnect(_osgQtViewer, SIGNAL(mousePressSignal(QMouseEvent*)), _osgQtViewer, SLOT(mousePressSlot(QMouseEvent*)));
    QObject::disconnect(_osgQtViewer, SIGNAL(mouseReleaseSignal(QMouseEvent*)), _osgQtViewer, SLOT(mouseReleaseSlot(QMouseEvent*)));
    QObject::disconnect(_osgQtViewer, SIGNAL(mouseMoveSignal(QMouseEvent*)), _osgQtViewer, SLOT(mouseMoveSlot(QMouseEvent*)));
    QObject::disconnect(_osgQtViewer, SIGNAL(touchEventSignal(QEvent*)), _osgQtViewer, SLOT(touchEventSlot(QEvent*)));
}


void Viewer2D3D::startManipulateSceneSlot()
{
    if (getOsgViewer()->getCameraManipulator())
    {
        QObject::connect(_osgQtViewer, SIGNAL(mousePressSignal(QMouseEvent*)), _osgQtViewer, SLOT(mousePressSlot(QMouseEvent*)), Qt::UniqueConnection);
        QObject::connect(_osgQtViewer, SIGNAL(mouseReleaseSignal(QMouseEvent*)), _osgQtViewer, SLOT(mouseReleaseSlot(QMouseEvent*)), Qt::UniqueConnection);
        QObject::connect(_osgQtViewer, SIGNAL(mouseMoveSignal(QMouseEvent*)), _osgQtViewer, SLOT(mouseMoveSlot(QMouseEvent*)), Qt::UniqueConnection);
    }
}


void Viewer2D3D::createOrSetupCameraManipulator()
{
    if(getCameraManipulator())
        setupManipulator(getCameraManipulator());
    else
    {
        osg::ref_ptr<PoLAR::SceneGraphManipulator> m = new PoLAR::SceneGraphManipulator;
        m->setName("CameraManipulator");
        setCameraManipulator(m.get());
    }
}


void Viewer2D3D::setupManipulator(osgGA::CameraManipulator *m)
{
    if (m)
    {
        SceneGraphManipulator* customManip = dynamic_cast<SceneGraphManipulator*>(m);
        if (customManip && _sceneGraph.valid())
        {
            if (customManip->getTrackNode()==0) customManip->setTrackNode(_sceneGraph->getScene());
            customManip->osg::Object::setUserData(_sceneGraph.get());
        }

        osg::Vec3d eye, center, up;
        _extrinsicMat.getLookAt(eye, center, up); // = _osgQtViewer->getCamera()->getViewMatrix().getLookAt
        m->setHomePosition(eye, center, up);
        m->home(0);
    }
}


void Viewer2D3D::setCameraManipulator(osgGA::CameraManipulator *m)
{
    setupManipulator(m);
    if (m) Viewer2D::setCameraManipulator(m);
}


void Viewer2D3D::setTrackNode(osg::Node *node)
{
    if(node)
    {
        SceneGraphManipulator *m = dynamic_cast<SceneGraphManipulator*>(getCameraManipulator());
        if(m) m->setTrackNode(node);
    }
}


void Viewer2D3D::addEventHandler(osgGA::GUIEventHandler *eventHandler)
{
    EventHandlerInterface* event = dynamic_cast<EventHandlerInterface*>(eventHandler);
    if (event)
        event->init(_sceneGraph.get());

    Viewer2D::addEventHandler(eventHandler);
}


int Viewer2D3D::addObject3D(Object3D *object3D)
{
    if(_sceneGraph->addObject(object3D))
    {
        if (!getCameraManipulator())
        {
            osg::ref_ptr<PoLAR::SceneGraphManipulator> m = new PoLAR::SceneGraphManipulator;
            m->setName("CameraManipulator");
            setCameraManipulator(m.get());
        }
        QObject::connect(object3D, SIGNAL(updateSignal()), this, SLOT(requestRedrawOrUpdate()));
        return (_sceneGraph->getNumObjects()-1);
    }
    else return -1;
}

int Viewer2D3D::removeObject3D(Object3D *object3D)
{
    int index = _sceneGraph->getObjectIndex(object3D);
    if (_sceneGraph->removeObject(object3D))
    {
        QObject::disconnect(object3D, SIGNAL(updateSignal()), this, SLOT(update()));
        return index;
    }
    else return -1;
}

int Viewer2D3D::replaceObject3D(Object3D *origObject3D, Object3D *newObject3D)
{
    if (_sceneGraph->replaceObject(origObject3D, newObject3D))
    {
        QObject::disconnect(origObject3D, SIGNAL(updateSignal()), this, SLOT(update()));
        QObject::connect(newObject3D, SIGNAL(updateSignal()), this, SLOT(update()));
        return _sceneGraph->getObjectIndex(newObject3D);
    }
    else return -1;
}

Object3D* Viewer2D3D::getObject3D(int index)
{
    osg::Node* nd = _sceneGraph->getObject(index);
    if (nd != NULL)
    {
        Object3D* object3D = dynamic_cast<Object3D*>(nd->asGroup());
        if(object3D)
            return object3D;
    }

#ifdef PoLAR_DEBUG
    qDebug() <<"Index number does not correspond to any 3D object of the scene";
#endif
    return NULL;
}

Object3D* Viewer2D3D::getPhantomObject3D(int index)
{
    osg::Node* nd = _sceneGraph->getPhantomObject(index);
    if (nd != NULL)
    {
        Object3D* object3D = dynamic_cast<Object3D*>(nd->asGroup());
        if(object3D)
            return object3D;
    }

#ifdef PoLAR_DEBUG
    qDebug()<<"Index number does not correspond to any 3D phantom object of the scene";
#endif
    return NULL;
}

std::list< osg::ref_ptr<Object3D> > Viewer2D3D::getObjects3D()
{
    std::list< osg::ref_ptr<Object3D> > computedList;
    unsigned int nbObj = _sceneGraph->getNumObjects();
    while (nbObj > 0)
    {
        if (Object3D* obj=getObject3D(nbObj-1)) computedList.push_front(obj);
        nbObj--;
    }
    return computedList;
}


std::list< osg::ref_ptr<Object3D> > Viewer2D3D::getPhantomObjects3D()
{
    std::list< osg::ref_ptr<Object3D> > computedList;
    unsigned int nbObj = _sceneGraph->getNumPhantomObjects();
    while (nbObj > 0)
    {
        if (Object3D* obj=getPhantomObject3D(nbObj-1)) computedList.push_front(obj);
        nbObj--;
    }
    return computedList;
}

Light* Viewer2D3D::addLightSource(float posX, float posY, float posZ, bool shadower, bool viewable,
                                  float ambientR, float ambientG, float ambientB,
                                  float diffuseR, float diffuseG, float diffuseB,
                                  float specularR, float specularG, float specularB,
                                  bool videoFrame)
{
    if(_sceneGraph.valid())
    {
        osg::ref_ptr<Light> lightSource = new Light(osg::Vec4(posX, posY, posZ, 1.0f),
                                                    osg::Vec4(ambientR, ambientG, ambientB, 1.0f),
                                                    osg::Vec4(diffuseR, diffuseG, diffuseB, 1.0f),
                                                    osg::Vec4(specularR, specularG, specularB, 1.0f),
                                                    shadower, viewable, videoFrame);
        if (!lightSource.valid()) return NULL;
        else
        {
            _sceneGraph->addLight(lightSource.get());
            QObject::connect(lightSource.get(), SIGNAL(updateSignal()), this, SLOT(update()));
            return lightSource.get();
        }
    }
    else return NULL;
}

bool Viewer2D3D::addLightSource(Light *lightSource)
{
    if(_sceneGraph.valid())
    {
        int res = _sceneGraph->addLight(lightSource);
        if (res>=0)
        {
            QObject::connect(lightSource, SIGNAL(updateSignal()), this, SLOT(update()));
            return true;
        }
    }
    return false;
}

Light* Viewer2D3D::getLightSource(int lightNum) const
{
    if (_sceneGraph.valid())
    {
        osg::ref_ptr<Light> lightSource = _sceneGraph->getLight(lightNum);
        return lightSource.get();
    }
    else return NULL;
}

bool Viewer2D3D::removeLightSource(Light *lightSource)
{
    bool res = false;
    if(lightSource)
    {
        QObject::disconnect(lightSource, SIGNAL(updateSignal()), this, SLOT(update()));
        if(_sceneGraph.valid())
            res = _sceneGraph->removeLight(lightSource->getLightNum());
    }
    return res;
}

int Viewer2D3D::getLightNumber(Light *lightSource)
{
    if(_sceneGraph.valid())
        return _sceneGraph->getLightNumber(lightSource);
    else return -1;
}

Object3D* Viewer2D3D::getCurrentPickedObject()
{
    if (_currentPickedObject.valid()) return _currentPickedObject.get();
    else
    {
#ifdef PoLAR_DEBUG
        qDebug() << "No object is currently picked";
#endif
        return NULL;
    }
}

GeometryPickHandler* Viewer2D3D::getGeometryPickHandler() const
{
    GeometryPickHandler* geomPick = NULL;

    osgViewer::View::EventHandlers::iterator itrL;
    osgViewer::View::EventHandlers eventHandlers = getOsgViewer()->getEventHandlers();
    for (itrL=eventHandlers.begin(); itrL != eventHandlers.end(); ++itrL)
        geomPick = dynamic_cast<PoLAR::GeometryPickHandler*>((*itrL).get());

    return geomPick;
}

Object3DPickHandler* Viewer2D3D::getObject3DPickHandler() const
{
    Object3DPickHandler* objPick = NULL;

    osgViewer::View::EventHandlers::iterator itrL;
    osgViewer::View::EventHandlers eventHandlers = getOsgViewer()->getEventHandlers();
    for (itrL=eventHandlers.begin(); itrL != eventHandlers.end(); ++itrL)
        objPick = dynamic_cast<PoLAR::Object3DPickHandler*>((*itrL).get());

    return objPick;
}


bool Viewer2D3D::setScene3D(osg::Node *scene)
{
    bool res = false;
    if(_sceneGraph.valid())
    {
        res = _sceneGraph->setScene(scene);
        if(res)
        {
            createOrSetupCameraManipulator();
        }
    }
    return res;
}


bool Viewer2D3D::setObjects3DGroup(osg::Node *node)
{
    bool res = false;
    if(_sceneGraph.valid())
    {
        res = _sceneGraph->setShadowedObjectsNode(node);
        if(res)
        {
            createOrSetupCameraManipulator();
        }
    }
    return res;
}

bool Viewer2D3D::setPhantomObjectsGroup(osg::Node *node)
{
    bool res = false;
    if(_sceneGraph.valid())
    {
        res = _sceneGraph->setPhantomObjectsNode(node);
        if(res)
        {
            createOrSetupCameraManipulator();
        }
    }
    return res;
}

bool Viewer2D3D::setLightGroup(osg::Node *node)
{
    bool res = false;
    if(_sceneGraph.valid())
    {
        res = _sceneGraph->setLightGroup(node);
        if(res)
        {
            createOrSetupCameraManipulator();
        }
    }
    return res;
}
