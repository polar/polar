/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "GraphicsViewer.h"
#include "ShaderManager.h"
#include "Image.h"
#include <QGraphicsProxyWidget>
#include <QScrollBar>
#include <QApplication>
#include <QStyle>
#include <QDesktopWidget>
#include <QScreen>
#include <QDebug>

using namespace PoLAR;


GraphicsViewer::GraphicsViewer(QWidget *parent, const char *name, WindowFlags f, bool distortion):
    QGraphicsView(parent),
    _nbima(0),
    _withDistortion(distortion),
    _resizeOnResolutionChanged(false),
    _timerId(0),
    _idle(false),
    _currentFramerateTarget(0)
{
    setObjectName(name);

#ifdef OSG_USE_FBO
    /* OSG patch for FBO usage
     * If not already done (case of multiple viewers in a single application)
     * we replace the prototype of the regular CullVisitor by our custom one
     * This way, everytime OSG needs to call a CullVisitor, it will call our custom one
     */
    CullVisitorEx *cve = dynamic_cast<CullVisitorEx*>(osgUtil::CullVisitor::prototype().get());
    if(!cve)
    {
        cve = new CullVisitorEx();
        osgUtil::CullVisitor::prototype() = cve;
    }
#endif

    _osgQtViewer = new BaseViewer(this, f);
    _scene2d = new GraphicsScene;
    initViewer();

    _panZoom = new PanZoom(*(_osgQtViewer.get()));
    QObject::connect(_panZoom.get(), SIGNAL(updateSignal()), this, SLOT(requestRedraw()));

    _swImage = new osg::Switch;
    _panZoom->getCamera()->addChild(_swImage.get());

    _bgGeode = new osg::Geode;
    _swImage->addChild(_bgGeode.get());

    setFocusPolicy(Qt::StrongFocus);

    this->setBgImage((BaseImage*)NULL);
    
#ifdef SET_LOCALE_NUMERIC_C
    setlocale(LC_NUMERIC,"C");
#endif

    _timerId = this->startTimer(0);
    _lastFrameStartTime.setStartTick(0);
}


GraphicsViewer::~GraphicsViewer()
{
    if (_timerId != 0)
    {
        this->killTimer(_timerId);
    }
    delete _scene2d;
}


void GraphicsViewer::initViewer()
{
    setScene(_scene2d);
    _scene2d->setSceneRect(0, 0, width(), height());

    // add the QOpenGLWidget (managing the OSG view) in the QGraphicsScene
    _scene2d->addWidget(_osgQtViewer.get());
    _osgQtViewer->graphicsProxyWidget()->setZValue(-1); // be sure that the widget will always be under all the other GraphicsItems
    setMouseTracking(_osgQtViewer->hasMouseTracking());

    // deactivate the scroll bars
    horizontalScrollBar()->blockSignals(true);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    verticalScrollBar()->blockSignals(true);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // deactivate the frame drawn by default around the viewer (because of QFrame inheritance)
    setFrameShape(QFrame::NoFrame);
}


void GraphicsViewer::timerEvent(QTimerEvent *)
{
    if(isVisible())
    {
        // limit the frame rate
        if(_osgQtViewer->getRunMaxFrameRate() > 0.0)
        {
            double dt = _lastFrameStartTime.time_s();
            double minFrameTime = 1.0 / _osgQtViewer->getRunMaxFrameRate();
            if(dt < minFrameTime)
            {
                OpenThreads::Thread::microSleep(static_cast<unsigned int>(1000000.0*(minFrameTime-dt)));
            }
        }
        else
        {
            // avoid excessive CPU loading when no frame is required in ON_DEMAND mode
            if(_osgQtViewer->getRunFrameScheme() == osgViewer::ViewerBase::ON_DEMAND)
            {
                double dt = _lastFrameStartTime.time_s();
                if(dt < 0.01)
                {
                    OpenThreads::Thread::microSleep(static_cast<unsigned int>(1000000.0*(0.01-dt)));
                }
            }

            // record start frame time
            _lastFrameStartTime.setStartTick();
            if(_osgQtViewer->getRunFrameScheme() == osgViewer::ViewerBase::ON_DEMAND)
            {
                if(_osgQtViewer->checkNeedToDoFrame())
                {
                    update();
                }
            }
            else
            {
                update();
            }
        }
    }
}


void GraphicsViewer::update()
{
    viewport()->update();
    emit repainted();
}


void GraphicsViewer::requestRedraw()
{
    _osgQtViewer->requestRedraw();
}


void GraphicsViewer::requestRedrawOrUpdate()
{
    if(_idle)
        update();
    else
        requestRedraw();
}


void GraphicsViewer::setFramerate(unsigned int n)
{
    if(n!=0)
    {
        double ms = (1.0/(double)n)*1000.0;
        this->killTimer(_timerId);
        _currentFramerateTarget = (int)ms;
        _timerId = this->startTimer((int)ms);
    }
    else
    {
        this->killTimer(_timerId);
        _timerId = this->startTimer(0);
    }
}


void GraphicsViewer::setIdle(bool b)
{
    if(_idle == b) return;
    _idle = b;
    if(_idle && _timerId != 0)
    {
        this->killTimer(_timerId);
        _timerId = 0;
    }
    else
    {
        _timerId = this->startTimer(_currentFramerateTarget);
    }
}



void GraphicsViewer::addEventHandler(osgGA::GUIEventHandler *eventHandler)
{
    if(_osgQtViewer.get()) _osgQtViewer->addEventHandler(eventHandler);
}


void GraphicsViewer::setCameraManipulator(osgGA::CameraManipulator *m)
{
    if(_osgQtViewer.get()) _osgQtViewer->setCameraManipulator(m);
}


void GraphicsViewer::setOverlay(bool overlay)
{
    _scene2d->setOverlay(overlay);
    if(overlay)
        QObject::connect(_scene2d, SIGNAL(overlayPaint(QPainter*, const QRectF&)), this, SLOT(overpainting2D(QPainter*, const QRectF&)), Qt::UniqueConnection);
    else
        QObject::disconnect(_scene2d, SIGNAL(overlayPaint(QPainter*, const QRectF&)), this, SLOT(overpainting2D(QPainter*, const QRectF&)));
}


bool GraphicsViewer::notify(QEvent *event)
{
    return qApp->notify(this, event);
}


void GraphicsViewer::resizeEvent(QResizeEvent* event)
{
    int w = event->size().width();
    int h = event->size().height();
    _osgQtViewer->resize(w, h);
    _scene2d->setSceneRect(0, 0, w, h);
    _scene2d->invalidate();
    _panZoom->update();
}


bool GraphicsViewer::viewportEvent(QEvent* event)
{
    qApp->notify(_osgQtViewer.get(), event);
    return QGraphicsView::viewportEvent(event);
}


void GraphicsViewer::wheelEvent(QWheelEvent* event)
{
    emit mouseWheelSignal(event);
    qApp->notify(_osgQtViewer.get(), event);
    requestRedrawOrUpdate();
}


void GraphicsViewer::keyPressEvent(QKeyEvent *event)
{
    qApp->notify(_osgQtViewer.get(), event);
    requestRedrawOrUpdate();
}

void GraphicsViewer::keyReleaseEvent(QKeyEvent *event)
{
    qApp->notify(_osgQtViewer.get(), event);
    requestRedrawOrUpdate();
}


void GraphicsViewer::mouseMoveEvent(QMouseEvent *event)
{
    double x,y;
    emit mouseMoveSignal(event);
    QtToImage(event->x(), event->y(), x, y);
    emit mouseMoveSignal(x,y);
    //qApp->notify(_osgQtViewer.get(), event);
    requestRedrawOrUpdate();
}

void GraphicsViewer::mousePressEvent(QMouseEvent *event)
{
    double x,y;
    emit mousePressSignal(event);
    QtToImage(event->x(), event->y(), x, y);
    emit mousePressSignal(x,y, event->button(), event->modifiers());
    //qApp->notify(_osgQtViewer.get(), event);
    requestRedrawOrUpdate();
}

void GraphicsViewer::mouseReleaseEvent(QMouseEvent *event)
{
    emit mouseReleaseSignal(event);
    //qApp->notify(_osgQtViewer.get(), event);
    requestRedrawOrUpdate();
}

void GraphicsViewer::mouseDoubleClickEvent(QMouseEvent *event)
{
    QGraphicsView::mouseDoubleClickEvent(event);
    //qApp->notify(_osgQtViewer.get(), event);
    requestRedrawOrUpdate();
}

void GraphicsViewer::setBgImage(BaseImage *image, bool resizeParent)
{
    if (_image.valid() && _image->valid())
    {
        if (!_image.get()) qDebug("Viewer::setBgImage() disconnect");
        _image.get()->disconnect(this);
    }
    _image=image;
    _resizeWithParent = resizeParent;

    if (_image.valid())
    {
        _nbima = _image->getNbImages();
        int w=_image->getWidth(), h=_image->getHeight();
        _panZoom->resize(w,h);

        if(_image->valid())
        {
            float wT = 1.0, hT = 1.0;

            if (_bgGeode->getNumDrawables() == 0)
                _bgGeode->addDrawable(osg::createTexturedQuadGeometry( osg::Vec3(0,0,0),
                                                                       osg::Vec3(w,0,0),
                                                                       osg::Vec3(0,h,0),
                                                                       wT, hT
                                                                       )
                                      );
            else
                _bgGeode->setDrawable(0,osg::createTexturedQuadGeometry( osg::Vec3(0,0,0),
                                                                         osg::Vec3(w,0,0),
                                                                         osg::Vec3(0,h,0),
                                                                         wT, hT
                                                                         )
                                      );
            _bgGeode->getDrawable(0)->setUseVertexBufferObjects(true);
            osg::Texture2D* t = _image->makeTexture();
            t->setResizeNonPowerOfTwoHint(false);

            osg::ref_ptr<osg::StateSet> stateSet = _bgGeode->getOrCreateStateSet();
            stateSet->setTextureAttributeAndModes(0, t ,osg::StateAttribute::ON);
            stateSet->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
            stateSet->setMode(GL_DEPTH_TEST,osg::StateAttribute::OFF);

            QObject::connect(_image.get(), SIGNAL(updateSignal()), this, SLOT(requestRedraw()));
            _image->gotoImageSlot(0);

            // Adding a shader system for window level and distortion
            ShaderManager::addWindowLevel(_bgGeode.get(), _image.get());

            if(_withDistortion)
            {
                _lambda1 = float(0);
                _lambda2 = float(0);
                _cx = float(0);
                _cy = float(0);

                // Adding the uniforms for distortion management in the shader
                stateSet->addUniform(new osg::Uniform("withDistortion", 1));
                stateSet->addUniform(new osg::Uniform("texSize", osg::Vec2f(_image->getWidth(), _image->getHeight())));
                stateSet->addUniform(new osg::Uniform("lambda1", _lambda1));
                stateSet->addUniform(new osg::Uniform("lambda2", _lambda2));
                stateSet->addUniform(new osg::Uniform("centre", osg::Vec2f(_cx, _cy)));
            }
        }
        if (resizeParent)
            resizeWithParent(w,h);

        QObject::connect(_image.get(), SIGNAL(resolutionChanged()), this, SLOT(bgImgResolutionChanged()), Qt::UniqueConnection);
        QObject::connect(_image.get(), SIGNAL(newImageSignal()), this, SLOT(requestRedraw()), Qt::UniqueConnection);
    }
    else
    {
        _nbima = 1;
        int w, h;
#ifdef MOBILE_PLATFORM
        QRect rect = qApp->desktop()->screenGeometry();
        w = rect.width();
        h = rect.height();
        QScreen *screen = QGuiApplication::primaryScreen();
        QRect r = screen->availableGeometry();
        resize(r.width(), r.height());
#else
        w = width();
        h = height();
#endif
        _panZoom->resize(w, h);
        if (_bgGeode->getNumDrawables() != 0) _bgGeode->removeDrawable(_bgGeode->getDrawable(0));
    }
}


void GraphicsViewer::setDistortion(GLfloat lambda1, GLfloat lambda2, GLfloat cx, GLfloat cy)
{
    if (_withDistortion)
    {
        _lambda1 = lambda1;
        _lambda2 = lambda2;
        _cx = cx; _cy = cy;

        if (_image.valid() && _image->valid())
        {
            _bgGeode->getOrCreateStateSet()->getUniform("lambda1")->set(_lambda1);
            _bgGeode->getOrCreateStateSet()->getUniform("lambda2")->set(_lambda2);
            _bgGeode->getOrCreateStateSet()->getUniform("centre")->set(osg::Vec2f(_cx, _cy));
        }
    }
}



void GraphicsViewer::bgImageOff()
{
    _swImage->setAllChildrenOff();
    update();
}

void GraphicsViewer::bgImageOn()
{
    _swImage->setAllChildrenOn();
    update();
}

void GraphicsViewer::bgImageToggle()
{
    if (isBgImageOn()) bgImageOff();
    else bgImageOn();
}


void GraphicsViewer::gotoImageSlot(int n)
{
    if (_image.valid())
    {
        (_image.get())->gotoImageSlot(n);
        update();
    }
}

void GraphicsViewer::nextImageSlot(bool wrap)
{
    if (_image.valid())
    {
        (_image.get())->nextImageSlot(wrap);
        update();
    }
}

void GraphicsViewer::previousImageSlot(bool wrap)
{
    if (_image.valid())
    {
        (_image.get())->previousImageSlot(wrap);
        update();
    }
}

void GraphicsViewer::overpainting2D(QPainter *, const QRectF &)
{
    qWarning() << "overpainting2D : error\nThis method must be overwritten in a subclass";
}

void GraphicsViewer::setBackgroundColor(float r, float g, float b, float a)
{
    if(_osgQtViewer.get()) _osgQtViewer->getCamera()->setClearColor(osg::Vec4(r, g, b, a));
}

void GraphicsViewer::setBackgroundColor(osg::Vec4 color)
{
    if(_osgQtViewer.get()) _osgQtViewer->getCamera()->setClearColor(color);
}


void GraphicsViewer::setPanPress(double x, double y)
{
    if(_panZoom.get())
    {
        _panZoom->setMousePos(x, y);
    }
}

void GraphicsViewer::setPanMove(double x, double y)
{
    if(_panZoom.get())
    {
        _panZoom->mousePanSlot(x, y);
    }
}

void GraphicsViewer::setZoomPress(double x, double y)
{
    if(_panZoom.get())
    {
        _panZoom->setMousePos(x, y);
    }
}

void GraphicsViewer::setZoomMove(double x, double y)
{
    if(_panZoom.get())
    {
        _panZoom->mouseZoomSlot(x, y);
    }
}


void GraphicsViewer::setWindowLevelPress(double x, double y)
{
    if(_image.get())
    {
        _image->setMousePos(x, y);
    }
}

void GraphicsViewer::setWindowLevelMove(double x, double y)
{
    if(_image.get())
    {
        _image->windowLevelSlot(x, y);
    }
}


QPixmap GraphicsViewer::grab()
{
    QPixmap pixmap(size());
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing);
    _scene2d->render(&painter);
    painter.end();
    return pixmap;
}

void GraphicsViewer::resizeWithParent(int w, int h)
{
    if (parentWidget())
    {
        int dw = parentWidget()->width()-width();
        int dh = parentWidget()->height()-height();
        parentWidget()->resize(int(dw+w*getZoom()), int(dh+h*getZoom()));
    }
}


void GraphicsViewer::center()
{
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));
}



void GraphicsViewer::bgImgResolutionChanged()
{
    setBgImage(_image.get());
    if(_resizeOnResolutionChanged)
    {
        int w = _image->getWidth();
        int h = _image->getHeight();
        resize(w,h);
        if(_resizeWithParent) resizeWithParent(w, h);
    }
}


void GraphicsViewer::setOptimalZoom()
{
    int w, h;
#if MOBILE_PLATFORM
    QRect rect = qApp->desktop()->screenGeometry();
    w = rect.width();
    h = rect.height();
#else
    w = width();
    h = height();
#endif
    if(_image.valid() && _image->valid())
    {
        double zoom = (std::min)(double(h)/double(_image->getHeight()), double(w)/double(_image->getWidth()));
        setZoom(zoom);
    }
}
