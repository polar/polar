/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "GraphicsScene.h"
#include <QGraphicsWidget>


using namespace PoLAR;

GraphicsScene::GraphicsScene():
    _overlayPaint(false),
    _imageIndex(0)
{
}


GraphicsScene::~GraphicsScene()
{
    // remove all the items so that the GraphicsScene has no longer ownership of them, and thus will not delete them.
    // We want to delete them manually.
    removeAllItems(true);
}


void GraphicsScene::removeAllItems(bool withWidget)
{
    QList<QGraphicsItem *> list = this->items();
    QList<QGraphicsItem *>::iterator itr;
    for(itr=list.begin(); itr!=list.end(); ++itr)
    {
        if(!withWidget)
        {
            QGraphicsWidget *widget = dynamic_cast<QGraphicsWidget*>(*itr);
            if(!widget)
                this->removeItem((*itr));
        }
        else
            this->removeItem((*itr));
    }
}

void GraphicsScene::drawForeground(QPainter *painter, const QRectF &rect)
{
    if(_overlayPaint) emit overlayPaint(painter, rect);
    else return;
}


void GraphicsScene::update()
{
    QGraphicsScene::update();
}

/*
void GraphicsScene::drawBackground(QPainter* painter, const QRectF& rect)
{
}
*/
