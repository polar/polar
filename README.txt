PoLAR (Portable Library for Augmented Reality)
Copyright (C) 2015 Inria, Université de Lorraine, CNRS  


Main steps to follow to use PoLAR:

1. Dependencies

PoLAR needs OpenSceneGraph and Qt.

a. Linux packages
- OpenSceneGraph: openscenegraph libopenscenegraph-dev
- Qt: qt5-default qttools5-dev-tools libqt5opengl5-dev qtmultimedia5-dev libqt5multimedia5-plugins qtdeclarative5-dev libqt5svg5-dev
- for Multimedia (webcam) options, we recommend using gstreamer packages: libgstreamer0.10-0 libgstreamer0.10-dev gstreamer-tools gstreamer0.10-plugins-good
- for doxygen building (optional): doxygen graphviz

b. Mac OS

OpenSceneGraph and Qt are available through port or brew. For a complete guide with brew, follow the steps explained in the PoLAR user manual (see /Manual directory).

c. Windows

We recommend using Visual Studio. You will need to install OpenSceneGraph from sources or prebuilt Visual Studio binaries and download the Qt binaries from the Qt website.
For a complete installation guide, follow the steps explained in the PoLAR user manual (see /Manual directory).

2. Build PoLAR

PoLAR uses CMake for the building process.
Some CMake variables can be modified:
- BUILD_PoLAR_EXAMPLE: if set to ON, the examples will be built.
- DYNAMIC_PoLAR: set to ON, PoLAR is built as a shared library. If OFF, PoLAR is built as a static library.
- USE_OFF_PLUGIN: if ON, the plugin for loading OFF 3D models will be built (same output as the PoLAR library)
- QT_CUSTOM_DIR, OSG_CUSTOM_DIR, OSG_CUSTOM_INCLUDE_DIRS: require a path in string format to a Qt or OSG installation folder. Needed if you don't want or can't to use Qt and OSG as packages (e.g. on other platforms than Linux)
- DOXYGEN_GENERATION: when set to ON, the doxygen documentation is generated and available from the /Doc folder.


3. Using PoLAR

Now you have installed all this stuff (without errors I hope), you can use the PoLAR platform.
Run the examples in the Examples directory to see the main possibilities of the library 
(check the README file in the Examples directory to see how to launch and how to use them).
If you want to use the OFF loader, type ". polar_config" from the PoLAR root dir in order to correctly set the required environment variables.

The doxygen documentation is available here : http://polar.gforge.inria.fr/Doc/


If you notice a bug using the platform you can use the bug tracker (https://gforge.inria.fr/projects/polar/).
