/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "VideoPlayer.h"
#include <QWidget>
#include <QCameraInfo>
#include <QtMultimedia/QVideoEncoderSettingsControl>
#include <QCameraImageCapture>
#include <QtMultimedia/QMediaPlaylist>
#include <QVideoSurfaceFormat>
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include <QtGlobal>
#include <QMediaMetaData>
#include <iostream>

using namespace PoLAR;


VideoPlayer::VideoPlayer(int deviceNumber, int width, int height, int depth, QObject *parent):
    QAbstractVideoSurface(parent),
    _qplayer(NULL), _mode(CameraMode), _data(NULL),
    _displayFrames(false), _requestedWidth(width), _requestedHeight(height),
    _width(1), _height(1), _depth(depth),
    _rescaleImg(NULL),
    _mirrorFrame(false)
  #if USE_PROBE
  ,  _probe(NULL)
  #endif
{
    int numberofDevices  = QCameraInfo::availableCameras().size();
    if(numberofDevices > deviceNumber)
    {
        _qcamera = new QCamera(QCameraInfo::availableCameras()[deviceNumber], this);
        _qcamera->setViewfinder(this);
        _qcamera->load();
        setFittingResolution();

#if USE_PROBE
        _probe = new QVideoProbe;
        if (_probe->setSource(_qcamera))
	  QObject::connect(_probe, SIGNAL(videoFrameProbed(QVideoFrame)), this, SLOT(processFrame(QVideoFrame)));
#endif
        // avoid creating array with size 0
        if(depth > 0)
        {
            _bufferSize = _width * _height * _depth;
            _data = new unsigned char[_bufferSize];
            memset(_data, 0, _bufferSize);
        }
        else
        {
            qCritical() << "Invalid depth";
            _data = NULL;
        }
    }
    else
    {
        _qcamera = NULL;
        qCritical() << "No camera plugged";
    }
}


VideoPlayer::VideoPlayer(const char *name, int width, int height, int depth, QObject *parent):
    QAbstractVideoSurface(parent),
    _qcamera(NULL), _mode(MediaPlayerMode), _data(NULL),
    _displayFrames(false), _requestedWidth(width), _requestedHeight(height),
    _width(1), _height(1), _depth(depth),
    _rescaleImg(NULL),
    _mirrorFrame(false)
  #if USE_PROBE
  , _probe(NULL)
  #endif
{
    QFile file(name);
    if(file.open(QIODevice::ReadOnly))
    {
        _qplayer = new QMediaPlayer(this);
        QMediaPlaylist *playlist = new QMediaPlaylist;
        QMediaContent media(QUrl::fromLocalFile(QFileInfo(file).absoluteFilePath())); //QMediaPlayer only accepts absolute paths
        playlist->addMedia(media);
        playlist->setPlaybackMode(QMediaPlaylist::Loop);
        playlist->setCurrentIndex(1);
        _qplayer->setPlaylist(playlist);
        _qplayer->setVideoOutput(this);
        _qplayer->setVolume(0);

	setFittingResolution();
#if USE_PROBE
        _probe = new QVideoProbe;
        if (_probe->setSource(_qplayer))
	  QObject::connect(_probe, SIGNAL(videoFrameProbed(QVideoFrame)), this, SLOT(processFrame(QVideoFrame)));
#endif
        if(depth > 0)
        {
            _bufferSize = _width * _height * _depth;
            _data = new unsigned char[_bufferSize];
            memset(_data, 0, _bufferSize);
        }
        else
        {
            qCritical() << "Invalid depth";
            _data = NULL;
        }
    }
    else
    {
        _qplayer = (QMediaPlayer*)NULL;
        qCritical() << "Could not open file" << name;
    }
}


VideoPlayer::VideoPlayer(QObject *parent):
    QAbstractVideoSurface(parent),
    _qcamera(NULL), _qplayer(NULL), _mode(QmlMode), _data(NULL),
    _displayFrames(false), _requestedWidth(0), _requestedHeight(0),
    _width(1), _height(1), _depth(3),
    _rescaleImg(NULL),
    _mirrorFrame(false)
{
    // avoid creating array with size 0
    if(_depth > 0)
    {
        _bufferSize = _width * _height * _depth;
        _data = new unsigned char[_bufferSize];
        memset(_data, 0, _bufferSize);
    }
    else
    {
        qCritical() << "Invalid depth";
        _data = NULL;
    }
}


VideoPlayer::~VideoPlayer()
{
  if(isCameraAvailable())
    {
      _qcamera->stop();
      _qcamera->unlock();
      _qcamera->unload();
      _qcamera->deleteLater();
#if USE_PROBE
      if (_probe) delete _probe;
#endif
    }
  else if(isPlayerAvailable())
    {
      delete _qplayer;
#if USE_PROBE
      if (_probe) delete _probe;
#endif
    }

    if(_data)
      delete[] _data;
    
    if(_rescaleImg)
      delete _rescaleImg;
}


void VideoPlayer::setFittingResolution()
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
  if (isCameraAvailable())
    {
        QList<QSize> resList = _qcamera->supportedViewfinderResolutions();
        QSize requestedSize(_requestedWidth, _requestedHeight);
        if (resList.size())
        {
            if (requestedSize.isNull())
            {
                // set the max resolution available
                requestedSize = resList[resList.size() - 1];
                _requestedWidth = resList[resList.size() - 1].width();
                _requestedHeight = resList[resList.size() - 1].height();
            }
            else
            {
                // try to set the most appproaching resolution
                int idx = resList.indexOf(requestedSize);
                if (idx == -1)
                {
                    // we skip the QCameraViewfinderSettings creation
                    return;
                }
            }
            QCameraViewfinderSettings viewfinderSettings;
            viewfinderSettings.setResolution(requestedSize);
            _qcamera->setViewfinderSettings(viewfinderSettings);
        }
    }
  else if (isPlayerAvailable())
    {
      _width = _requestedWidth = _qplayer->metaData(QMediaMetaData::Resolution).toSize().width();
      _height = _requestedHeight = _qplayer->metaData(QMediaMetaData::Resolution).toSize().height();
      QSize par = _qplayer->metaData(QMediaMetaData::PixelAspectRatio).toSize();
      if (par.width() > par.height()) _requestedWidth = int((_requestedWidth*par.width())/par.height());
      if (par.width() < par.height()) _requestedHeight = int((_requestedHeight*par.height())/par.width());
      if (_width <= 0 || _height <= 0)
	{
	  _width = 1;
	  _height = 1;
	  _requestedWidth = _requestedHeight = 1;
	}
      qDebug() << "Width/height: " << _width << " " <<_height<< " " <<_requestedWidth << " " << _requestedHeight;
    }
#endif
}


QList<QSize> VideoPlayer::getAvailableResolutions()
{
    QList<QSize> resList;
    if (isCameraAvailable())
    {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
        resList = _qcamera->supportedViewfinderResolutions();
#else
        const char* qtVersion = qVersion();
        qCritical() << "Cannot access to camera resolutions in Qt" << qtVersion;
#endif
    }
    else if (isPlayerAvailable())
    {
      //QSize res = _qplayer->playlist()->media(0).canonicalResource().resolution();
      QSize res = _qplayer->metaData(QMediaMetaData::Resolution).toSize();
      resList.push_back(res);
    }

    return resList;
}


int VideoPlayer::play()
{
    switch(_mode)
    {
    case CameraMode:
        if(_qcamera)
        {
            _qcamera->start();
            if(_qcamera->state() != QCamera::ActiveState) return 0;
            else
            {
                _displayFrames = true;
                return 1;
            }
        }
	break;
    case MediaPlayerMode:
        if(_qplayer)
        {
            _qplayer->setPlaybackRate(1);
            _qplayer->play();
            if(_qplayer->state() != QMediaPlayer::PlayingState) return 0;
            else
            {
	      setFittingResolution();
                _displayFrames = true;
                return 1;
            }
        }
	break;
    case QmlMode:
      _displayFrames = true;
      return 1;
      break;
    default:
      return 0;
    }
    return 0; 
}


void VideoPlayer::pause()
{
  if(isCameraAvailable() && _qcamera->state() == QCamera::ActiveState && _displayFrames)
    {
        _displayFrames = false;
    }
  else if(isPlayerAvailable() && _qplayer->state() == QMediaPlayer::PlayingState)
    {
        _qplayer->setPlaybackRate(1);
        _qplayer->pause();
        _displayFrames = false;
    }
}

void VideoPlayer::stop()
{
  if(isCameraAvailable())
    {
        _qcamera->stop();
    }
  else if(isPlayerAvailable())
    {
        _qplayer->stop();
    }
}


void VideoPlayer::rewind(qreal speed)
{
    forward(-speed);
}

void VideoPlayer::forward(qreal speed)
{
  if(isPlayerAvailable())
    {
        _qplayer->setPlaybackRate(speed);

    }
    else
        qCritical() << "No MediaPlayer available";
}

void VideoPlayer::setPosition(qint64 pos)
{
  if(isPlayerAvailable())
    {
        _qplayer->setPosition(pos);
    }
    else
        qCritical() << "No MediaPlayer available";
}


void VideoPlayer::setVolume(int volume)
{
  if(isPlayerAvailable())
    {
        _qplayer->setVolume(volume);
    }
    else
        qCritical() << "No MediaPlayer available";
}


void VideoPlayer::loop(bool b)
{
  if(isPlayerAvailable())
    {
        if(b)
            _qplayer->playlist()->setPlaybackMode(QMediaPlaylist::Loop);
        else
            _qplayer->playlist()->setPlaybackMode(QMediaPlaylist::Sequential);
    }
}


void VideoPlayer::previous()
{
  if(isPlayerAvailable())
        _qplayer->playlist()->previous();
}

void VideoPlayer::next()
{
  if(isPlayerAvailable())
        _qplayer->playlist()->next();
}

bool VideoPlayer::addMedia(const char *name)
{
  if(isPlayerAvailable())
    {
      QFile file(name);
      if(file.open(QIODevice::ReadOnly))
	return _qplayer->playlist()->addMedia(QUrl::fromLocalFile(QFileInfo(file).absoluteFilePath()));
    }
  return false;
}

bool VideoPlayer::removeMedia(int index)
{
  if(isPlayerAvailable())
    {
      return _qplayer->playlist()->removeMedia(index);
    }
    return false;
}


void VideoPlayer::yuv2rgb(int w, int h, unsigned char *in, unsigned char *out, bool is420)
{
    int totalSize = w * h;
    unsigned char *Ybuf = in;
    unsigned char *Ubuf;
    unsigned char *Vbuf;
    if(is420)
    {
        Ubuf = Ybuf + totalSize;
        Vbuf = Ubuf + totalSize/4;
    }
    else
    {
        Vbuf = Ybuf + totalSize;
        Ubuf = Vbuf + totalSize/4;
    }
    int rgbIndex = 0;
    int Y, U, V, r, g, b;

    for(int y=0; y<_height; y++)
    {
        for(int x=0; x<_width; x++)
        {
            Y = Ybuf[y*_width+x] - 16;
            U = Ubuf[(y/2) * (_width/2) + x/2] - 128;
            V = Vbuf[(y/2) * (_width/2) + x/2] - 128;
            r = qBound(0, (298*Y + 409*V + 128) >> 8, 255);
            g = qBound(0, (298*Y - 100*U - 208*V + 128) >> 8, 255);
            b = qBound(0, (298*Y + 516*U + 128) >> 8, 255);
            out[rgbIndex++] = r;
            out[rgbIndex++] = g;
            out[rgbIndex++] = b;
        }
    }
}


QList<QVideoFrame::PixelFormat> VideoPlayer::supportedPixelFormats(QAbstractVideoBuffer::HandleType type) const
{
    // if no handle: the data can only be accessed by mapping the buffer.
    if(type == QAbstractVideoBuffer::NoHandle)
    {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_ARGB32
                << QVideoFrame::Format_ARGB32_Premultiplied
                << QVideoFrame::Format_RGB32
                << QVideoFrame::Format_RGB24
                << QVideoFrame::Format_RGB565
                << QVideoFrame::Format_RGB555
                << QVideoFrame::Format_ARGB8565_Premultiplied
                << QVideoFrame::Format_BGRA32
                << QVideoFrame::Format_BGRA32_Premultiplied
                << QVideoFrame::Format_BGR32
                << QVideoFrame::Format_BGR24
                << QVideoFrame::Format_BGR565
                << QVideoFrame::Format_BGR555
                << QVideoFrame::Format_BGRA5658_Premultiplied
                << QVideoFrame::Format_AYUV444
                << QVideoFrame::Format_AYUV444_Premultiplied
                << QVideoFrame::Format_YUV444
                << QVideoFrame::Format_YUV420P
                << QVideoFrame::Format_YV12
                << QVideoFrame::Format_UYVY
                << QVideoFrame::Format_YUYV
                << QVideoFrame::Format_NV12
                << QVideoFrame::Format_NV21
                << QVideoFrame::Format_IMC1
                << QVideoFrame::Format_IMC2
                << QVideoFrame::Format_IMC3
                << QVideoFrame::Format_IMC4
                << QVideoFrame::Format_Y8
                << QVideoFrame::Format_Y16
                << QVideoFrame::Format_Jpeg
                << QVideoFrame::Format_CameraRaw
                << QVideoFrame::Format_AdobeDng;
    }
    else
    {
        return QList<QVideoFrame::PixelFormat>();
    }
}


bool VideoPlayer::present(const QVideoFrame& frame)
{
    if (frame.isValid())
    {
#if !USE_PROBE
        processFrame(frame);
#endif
        return true;
    }
    return false;
}


void VideoPlayer::processFrame(const QVideoFrame &frame)
{
    if(frame.isValid() && _displayFrames)
    {
        QVideoFrame cloneFrame(frame);
        cloneFrame.map(QAbstractVideoBuffer::ReadOnly);
	// get the current frame size
        _width = cloneFrame.width();
        _height = cloneFrame.height();

        int rgbSize = _width * _height * _depth;
        if(_bufferSize != rgbSize)
        {
            // resize the data buffer to the right size
            _bufferSize = rgbSize;
            delete[] _data;
            _data = new unsigned char[_bufferSize];
        }

        switch(cloneFrame.pixelFormat())
        {
        case QVideoFrame::Format_Y8: // grayscale
        {
            if(_mirrorFrame)
            {
                QImage tmp(cloneFrame.bits(), _width, _height, QImage::Format_Indexed8);
                memcpy(_data, tmp.mirrored().bits(), _bufferSize);
            }
            else
                memcpy(_data, cloneFrame.bits(), _bufferSize);
            break;
        }
        case QVideoFrame::Format_RGB24: // RGB 888
        {
            if(_mirrorFrame)
            {
                QImage tmp(cloneFrame.bits(), _width, _height, QImage::Format_RGB888);
                memcpy(_data, tmp.mirrored().bits(), _bufferSize);
            }
            else
                memcpy(_data, cloneFrame.bits(), _bufferSize);
            break;
        }

        case QVideoFrame::Format_BGR32:
        {
            unsigned char* dataptr;
            dataptr = _data;
            unsigned char* in = cloneFrame.bits();
            int j = 0;
            int i = 0;
            for(i=0; i<_width*_height*4; i++)
            {
                unsigned char b = in[i++];
                unsigned char g = in[i++];
                unsigned char r = in[i++];
                dataptr[j++] = r;
                dataptr[j++] = g;
                dataptr[j++] = b;
            }
            break;
        }
        case QVideoFrame::Format_YUV420P:
        {
            unsigned char *dataptr;
            if(_mirrorFrame)
                dataptr = new unsigned char[_bufferSize];
            else
                dataptr = _data;

            yuv2rgb(_width, _height, cloneFrame.bits(), dataptr, true);

            if(_mirrorFrame)
            {
                QImage tmp(dataptr, _width, _height, QImage::Format_RGB888);
                memcpy(_data, tmp.mirrored().bits(), _bufferSize);
                delete [] dataptr;
            }
            break;
        }

        case QVideoFrame::Format_YV12:
        {
            unsigned char *dataptr;
            if(_mirrorFrame)
                dataptr = new unsigned char[_bufferSize];
            else
                dataptr = _data;

            yuv2rgb(_width, _height, cloneFrame.bits(), dataptr, false);

            if(_mirrorFrame)
            {
                QImage tmp(dataptr, _width, _height, QImage::Format_RGB888);
                memcpy(_data, tmp.mirrored().bits(), _bufferSize);
                delete [] dataptr;
            }
            break;
        }

        default: // conversion using Qt QImage
        {
            QImage::Format format = QVideoFrame::imageFormatFromPixelFormat(cloneFrame.pixelFormat());
            if (format != QImage::Format_Invalid)
            {
                QImage tmp(cloneFrame.bits(), _width, _height, format);
                QImage converted;
                if(_mirrorFrame)
                    converted = tmp.convertToFormat(QImage::Format_RGB888).mirrored();
                else
                    converted = tmp.convertToFormat(QImage::Format_RGB888);

                memcpy(_data, converted.bits(), _bufferSize);
            }
            else
                qCritical() << "Format " << cloneFrame.pixelFormat() << " is unmanaged";
            break;
        }
        }

        if((_width != _requestedWidth || _height != _requestedHeight) && (_requestedWidth != 0 && _requestedHeight != 0))
	  {
            // rescale the frame to the requested size only if requested sizes are not 0
            if(!_rescaleImg)
	      _rescaleImg = new QImage(_data, _width, _height, QImage::Format_RGB888);
            else
	      memcpy(_rescaleImg->bits(), _data, _bufferSize);
	    
            emit newFrame(_rescaleImg->scaled(_requestedWidth, _requestedHeight, Qt::IgnoreAspectRatio).bits(),
                          _requestedWidth, _requestedHeight, _depth);
	  }
        else
	  {
            emit newFrame(_data, _width, _height, _depth);
	  }
	
        cloneFrame.unmap();
    }
}

