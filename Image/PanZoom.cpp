/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "PanZoom.h"
#include "BaseViewer.h"
#include <iostream>

using namespace PoLAR;


PanZoom::PanZoom(BaseViewer &v): 
    QObject(),
    _camera(new osg::Camera),
    _pcam(v.getCamera()),
    _zoom(1), _dx(0), _dy(0),
    _width(v.width()), _height(v.height()),
    _mouseX(0), _mouseY(0)
{
    _pcam->setClearMask(GL_DEPTH_BUFFER_BIT);
    _pcam->setReferenceFrame(osg::Transform::ABSOLUTE_RF);

    _camera->setViewport(_pcam->getViewport());
    _camera->setRenderOrder(osg::Camera::PRE_RENDER,0);
    _camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    _camera->setClearMask(GL_COLOR_BUFFER_BIT);
    _camera->setName("bgImgNode");
    recomputeCamera();

    // default zom options
#if(MOBILE_PLATFORM)
    _zoomMode = PINCH;
#else
    _zoomMode = DRAG;
#endif
    _zoomPrecision = 1.0f;
}

PanZoom::~PanZoom()
{
}


void PanZoom::update()
{
    recomputeCamera();
    emit updateSignal();
}

void PanZoom::mousePressSlot(QMouseEvent*)
{
/*
    if(event->button() == _panMouseButton.button && event->modifiers() == _panMouseButton.modifier)
    {
        QObject::connect(sender(), SIGNAL(mouseMoveSignal(QMouseEvent*)), this, SLOT(mousePanSlot(QMouseEvent*)));
    }
    else if(_zoomMode == DRAG && event->button() == _zoomMouseButton.button && event->modifiers() == _zoomMouseButton.modifier)
    {
        QObject::connect(sender(), SIGNAL(mouseMoveSignal(QMouseEvent*)), this, SLOT(mouseZoomDragSlot(QMouseEvent*)));
    }
    _mouseX=event->x(), _mouseY=event->y();
*/
}

void PanZoom::mouseWheelSlot(QWheelEvent *event)
{
    if(_zoomMode == SCROLL)
    {
        mouseZoomScrollSlot(event);
    }
}


void PanZoom::mousePanSlot(QMouseEvent* event)
{
  mousePanSlot(event->x(), event->y());
}


void PanZoom::mousePanSlot(double x, double y)
{
  // if the values are too high (meaning wrong interpretation from Qt), we reject the event
  float dx = (_mouseX-x)/_zoom;
  float dy = (_mouseY-y)/_zoom;
  if(abs(dx) > 100 || abs(dy) > 100) return;
  _dx += dx, _dy += dy;
  _mouseX=x, _mouseY=y;
  update();
}


void PanZoom::mouseZoomDragSlot(QMouseEvent* event)
{
    int y = event->y();
    mouseZoomSlot(0, y);
}

void PanZoom::mouseZoomScrollSlot(QWheelEvent* event)
{
    QPoint angle = event->angleDelta();
    float y(angle.y());
    mouseZoomSlot(0, y);
}


void PanZoom::mouseZoomSlot(double , double y)
{
    float mul = 0.0f;
    if(_zoomMode == DRAG)
        mul = (float)(y-_mouseY)/(float)_height;
    else if(_zoomMode == SCROLL)
        mul = (y/_zoomPrecision)/_height;
    else if(_zoomMode == PINCH)
    {
        mul = y/_zoomPrecision;
        _zoom *= mul;
        _mouseY = y;
        update();
        return;
    }
    else return;

    if (_mouseY < 0)
        _zoom /= (1-mul);
    else
        _zoom *= (1+mul);
    _mouseY = y;
    update();
}

void PanZoom::mouseReleaseSlot( QMouseEvent* )
{
    if (!sender()) qDebug("PanZoom::mouseReleaseSlot() disconnect");
    QObject::disconnect(sender(), SIGNAL(mouseMoveSignal(QMouseEvent*)), this, 0);
}

void PanZoom::QtToImage(double xqt, double yqt, double &xima, double &yima)
{
    double W=_camera->getViewport()->width(), H=_camera->getViewport()->height();
    xima = (xqt - W/2.0)/_zoom + _dx + _width/2.0;
    yima = (yqt - H/2.0)/_zoom + _dy + _height/2.0;
}

void PanZoom::ImageToQt(double xima, double yima, double &xqt, double &yqt)
{
    double W=_camera->getViewport()->width(), H=_camera->getViewport()->height();
    xqt = (xima - _width/2.0 - _dx) * _zoom + W/2.0;
    yqt = (yima - _height/2.0 - _dy) * _zoom + H/2.0;
}

void PanZoom::recomputeCamera()
{
    double W=_camera->getViewport()->width()/_zoom;
    double H=_camera->getViewport()->height()/_zoom;
    double l=(_width-W)/2.0+_dx;
    double r=(_width+W)/2.0+_dx;
    double b=(_height+H)/2.0+_dy;
    double t=(_height-H)/2.0+_dy;
    _camera->setProjectionMatrixAsOrtho2D(l,r,b,t);
    _pcam->setProjectionMatrix(_intrinsicMat*_camera->getProjectionMatrix());
}
