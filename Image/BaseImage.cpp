/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "BaseImage.h"
#include "Util.h"
#include <osgDB/ReadFile>
#include <iostream>
#include <QDebug>

using namespace PoLAR;

const int BaseImage::_wDef = 256;
const int BaseImage::_hDef = 128;


/* some function to wrap the osgDB loader */
osg::Image* loadImageFromFile(const std::string& fileName)
{
    osg::Image* ptr = osgDB::readImageFile(fileName);
    if(!ptr)
    {
        ptr = new osg::Image;
    }
    return ptr;
}


BaseImage::BaseImage():
    QObject(),
    _width(0), _height(0), _depth(1), _imageDimension(0),
    _GLformat(GL_LUMINANCE)
{
    _colorWidth = 2 * _wDef;
    _colorHeight = 4 * _hDef;
    initMouseShortcuts();
}

/* Delegate constructor that takes ownership of the source image and deletes it */
BaseImage::BaseImage(const osg::Image& image, const osg::CopyOp& copyop):
    QObject(),
    osg::Image(image, copyop),
    _colorWidth(0), _colorHeight(0)
{
    // quick hack to avoid memory leaks
    // TODO: find a better way
    osg::ref_ptr<osg::Image> ptr = const_cast<osg::Image*>(&image);
    ptr = NULL;
}


BaseImage::BaseImage(const std::string& fileName, bool flipVertical, int nbImages):
    BaseImage(*(loadImageFromFile(fileName)))
{
    if(!valid())
    {
        qCritical() << "Image not valid";
    }
    else
    {
        if(flipVertical) this->flipVertical(); // flip the image vertically if opened vertically mirrored
        _width = this->s();
        _height = this->t();
        int depth = this->getTotalDataSize() / (_width*_height*nbImages);
        setGLFormat(depth);
        if(isDepthSupported(depth)) _depth = depth;
        else _depth = 1;
        _imageDimension = _width * _height * _depth;
        _colorWidth = 2 * _wDef;
        _colorHeight = 4 * _hDef;
    }
    initMouseShortcuts();
}


BaseImage::BaseImage(int Width, int Height, int Depth):
    QObject(),
    _width(0), _height(0), _depth(1),
    _GLformat(GL_LUMINANCE),
    _colorWidth(0), _colorHeight(0)
{
    if (Width > 0 && Height > 0 && isDepthSupported(Depth))
    {
        _width = Width, _height = Height;
        _depth = Depth;
        setGLFormat(_depth);
        _imageDimension = _width * _height * _depth;
        _colorWidth = 2 * _wDef;
        _colorHeight = 4 * _hDef;
    }
    initMouseShortcuts();
}

/** Copy constructor using CopyOp to manage deep vs shallow copy.*/
BaseImage::BaseImage(const BaseImage &im, const osg::CopyOp& copyop):
    QObject(),
    osg::Image(im, copyop),
    _width(im._width), _height(im._height), _depth(im._depth)
{
    _imageDimension = _width * _height * _depth;
    setGLFormat(_depth);
    _colorWidth = 2 * _wDef;
    _colorHeight = 4 * _hDef;
    _windowLevelMouseButton = im._windowLevelMouseButton;
}


BaseImage::~BaseImage()
{
}


void BaseImage::initMouseShortcuts()
{
    _windowLevelMouseButton.button = Qt::MiddleButton; // default mouse button for window level edition
    _windowLevelMouseButton.modifier = Qt::NoModifier;
}


void BaseImage::setMousePos(double x, double y)
{
    _mouseX = x;
    _mouseY = y;
}


void BaseImage::mousePressSlot(QMouseEvent *e)
{
    if(e->button() ==_windowLevelMouseButton.button && e->modifiers() == _windowLevelMouseButton.modifier)
    {
        QObject::connect(sender(), SIGNAL(mouseMoveSignal(QMouseEvent*)),this, SLOT(windowLevelSlot(QMouseEvent*)));
        setMousePos(e->x(), e->y());
    }
}

void BaseImage::mouseReleaseSlot(QMouseEvent*)
{
    if (!sender()) qDebug("BaseImage::mouseReleaseSlot() disconnect");
    QObject::disconnect(sender(),SIGNAL(mouseMoveSignal(QMouseEvent*)),this,0);
}

void BaseImage::windowLevelSlot(QMouseEvent *e)
{
    if (valid())
    {
        int x = e->x(), y=e->y();
        windowLevelSlot(x, y);
    }
}

void BaseImage::windowLevelSlot(double x, double y)
{
    if(valid())
    {
        _colorWidth += (x - _mouseX);
        _colorHeight += (y - _mouseY);
        emit updateSignal();
        _mouseX = x, _mouseY = y;
    }
}


void BaseImage::setWindowLevelMouseButton(Qt::MouseButton button, Qt::KeyboardModifiers modifier)
{
    _windowLevelMouseButton.button = button;
    _windowLevelMouseButton.modifier = modifier;
}

void BaseImage::setGLFormat(int depth)
{
    switch(depth)
    {
    case 1: // greyscale image
        _GLformat = GL_LUMINANCE;
        break;
    case 3: // RGB
        _GLformat = GL_RGB;
        break;
    case 4: // RGBA (e.g. PNG)
        _GLformat = GL_RGBA;
        break;
    default:
        _GLformat = GL_LUMINANCE;
        qCritical("This image depth (%d) is not managed", depth);
        break;
    }

}

