/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "BlendDialog.h"
#include <QTextStream>

using namespace PoLAR;


BlendDialog::BlendDialog(Viewer2D3D *viewer, Object3D *object, QWidget *parent, bool modal, WindowFlags f)
    : QDialog(parent, f)
{
    setModal(modal);

    // internal stuff
    _viewer = viewer;
    _object = object;

    _blendValueMax = 100;

    // Menu
    menu = new QMenuBar(this);
    file = menu->addMenu( "&File");
    file->addAction( "&Load a configuration",  this, SLOT(loadSlot()), Qt::CTRL+Qt::Key_C );
    file->addAction( "&Save a configuration",  this, SLOT(saveSlot()), Qt::CTRL+Qt::Key_S );
    file->addSeparator();
    file->addAction( "&Quit",  this, SLOT(hide()), Qt::CTRL+Qt::Key_Q );

    mainLayout = new QVBoxLayout(this);
    QLabel *blankLabel = new QLabel(this);
    blankLabel->setFixedHeight(20);
    mainLayout->addWidget(blankLabel);// blank line
    blendLayout = new QVBoxLayout();
    blendCheck = new QCheckBox(QString("Transparency"), this);
    blendCheck->setFixedSize(blendCheck->sizeHint());
    blendLayout->addWidget(blendCheck);

    blendCheck->setChecked(false);

    blendLabel = new QLabel(QString("Degree of opacity"), this);
    blendLabel->setFixedHeight(20);
    blendLayout->addWidget(blendLabel);

    blendSliderLayout = new QHBoxLayout();
    blendSlider = new QSlider(Qt::Horizontal, this);

    blendSlider->setFixedHeight(20);
    blendSlider->setMinimum(0);
    blendSlider->setMaximum(_blendValueMax);

    blendSpinBox = new QSpinBox(this);
    blendSpinBox->setRange(0,_blendValueMax);
    blendSpinBox->setSingleStep(1);
    blendSliderLayout->addWidget(blendSlider);
    blendSliderLayout->addWidget(blendSpinBox);

    blendLayout->addLayout(blendSliderLayout);

    if (_object)
    {
        _blendInit = (int)(_blendValueMax*_object->getTransparencyValue());
        blendSlider->setValue(_blendInit);
        blendSpinBox->setValue(_blendInit);

        QObject::connect(blendCheck, SIGNAL(stateChanged(int)), _object, SLOT(toggleTransparencySlot(int)));
        QObject::connect(blendSlider, SIGNAL(valueChanged(int)),  this, SLOT(valueSetBlendSlot(int)));
        QObject::connect(blendSlider, SIGNAL(valueChanged(int)), blendSpinBox, SLOT(setValue(int)));
        QObject::connect(blendSpinBox, SIGNAL(valueChanged(int)),  this, SLOT(valueSetBlendSlot(int)));
        QObject::connect(blendSpinBox, SIGNAL(valueChanged(int)), blendSlider, SLOT(setValue(int)));
        QObject::connect(_object, SIGNAL(updateSignal()), (BaseViewer*)(_viewer), SLOT(update()));
    }

    mainLayout->addLayout(blendLayout);

    buttonsLayout = new QHBoxLayout();
    cancelButton = new QPushButton("Quit", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(hide()));
    //   cancelButton->setFixedWidth(200);

    defaultSettingsButton = new QPushButton("Default parameters", this);
    connect(defaultSettingsButton, SIGNAL(clicked()), this, SLOT(defaultSettingsSlot()));
    //   defaultSettingsButton->setFixedWidth(200);

    buttonsLayout->addWidget(defaultSettingsButton);
    buttonsLayout->addWidget(cancelButton);
    mainLayout->addLayout(buttonsLayout);
    mainLayout->activate();
}

void BlendDialog::loadSlot()
{
    QString f;
    f=QFileDialog::getOpenFileName( this,
                                    "Load a config file",
                                    QString::null,
                                    "XML files (*.xml)");
    if (f != QString::null) loadFileDialog(f);
}


void BlendDialog::saveSlot()
{
    QString f;
    f=QFileDialog::getSaveFileName( this,
                                    QString::null,
                                    "Save a config file"
                                    "XML files (*.xml)");
    if (f != QString::null)
    {
        if (f.endsWith(".xml")) saveFileDialog(f);
        else saveFileDialog(f + QString(".xml"));
    }
}


void BlendDialog::setObject(Object3D *object)
{
    _object=object;

    _blendInit = (int)(_blendValueMax*_object->getTransparencyValue());
    blendSlider->setValue(_blendInit);
    blendSpinBox->setValue(_blendInit);

    QObject::connect(blendCheck, SIGNAL(stateChanged(int)), _object, SLOT(toggleTransparencySlot(int)));
    QObject::connect(blendSlider, SIGNAL(valueChanged(int)),  this, SLOT(valueSetBlendSlot(int)));
    QObject::connect(blendSlider, SIGNAL(valueChanged(int)), blendSpinBox, SLOT(setValue(int)));
    QObject::connect(blendSpinBox, SIGNAL(valueChanged(int)),  this, SLOT(valueSetBlendSlot(int)));
    QObject::connect(blendSpinBox, SIGNAL(valueChanged(int)), blendSlider, SLOT(setValue(int)));
    QObject::connect(_object, SIGNAL(updateSignal()), (BaseViewer*)(_viewer), SLOT(update()));
} 


void BlendDialog::defaultSettingsSlot()
{
    blendCheck->setChecked(false);
    blendSlider->setValue(_blendInit);
    blendSpinBox->setValue(_blendInit);
}


void BlendDialog::loadFileDialog(const QString& filename)
{
    QDomDocument doc("config");
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) return;

    if (!doc.setContent(&file)) {file.close(); return;}
    file.close();
    if (!(doc.elementsByTagName("BlendDialog")).count()) return;
    if ((doc.elementsByTagName("blendStatus")).count()) {doc.elementsByTagName("blendStatus").item(0).toElement().text().toInt() ? blendCheck->setChecked(true): blendCheck->setChecked(false);}
    if ((doc.elementsByTagName("blendValue")).count()) blendSlider->setValue(doc.elementsByTagName("blendValue").item(0).toElement().text().toInt());
}


void BlendDialog::saveFileDialog(const QString& filename)
{
    QDomDocument doc("config");
    QDomElement root = doc.createElement("BlendDialog");
    doc.appendChild(root);

    QDomElement blending = doc.createElement("blending");
    root.appendChild(blending);

    QDomElement blendStatus = doc.createElement("blendStatus");
    blending.appendChild(blendStatus);
    int bstatus;
    blendCheck->isChecked() ? bstatus=1:bstatus=0;
    QString str;
    QDomText value1 = doc.createTextNode(str.sprintf("%d",bstatus));
    blendStatus.appendChild(value1);

    QDomElement blendValue = doc.createElement("blendValue");
    blending.appendChild(blendValue);
    QDomText value2 = doc.createTextNode(str.sprintf("%d",blendSlider->value()));
    blendValue.appendChild(value2);

    QDomElement lighting = doc.createElement("lighting");
    root.appendChild(lighting);

    QString xml = doc.toString();

    QFile file(filename);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        stream << xml << "\n";
        file.close();
    }
}
