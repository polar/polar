/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "BasicLightDialog.h"
#include <QTextStream>

using namespace PoLAR;


BasicLightDialog::BasicLightDialog(Viewer2D3D *viewer, PoLAR::Light *light, QWidget *parent, bool modal, WindowFlags f)
    : QDialog(parent, f), _viewer(viewer), _light(light), _lightValuesMax(255)
{
    setModal(modal);

    // Menu
    menu = new QMenuBar(this);
    file = menu->addMenu( "&File");
    file->addAction( "&Load a configuration",  this, SLOT(loadSlot()), Qt::CTRL+Qt::Key_C );
    file->addAction( "&Save a configuration",  this, SLOT(saveSlot()), Qt::CTRL+Qt::Key_S );
    file->addSeparator();
    file->addAction( "&Quit",  this, SLOT(hide()), Qt::CTRL+Qt::Key_Q );

    mainLayout = new QVBoxLayout(this);
    QLabel *blankLabel = new QLabel(this);
    blankLabel->setFixedHeight(20);
    mainLayout->addWidget(blankLabel);// blank line

    lightLayout = new QVBoxLayout();
    QLabel *numLightLabel;

    if (_light->isReferenceFrameVideo()) numLightLabel = new QLabel(QString("Light source (video frame)"), this);
    else numLightLabel = new QLabel(QString("Light source (world frame)"), this);

    numLightLabel->setFixedHeight(20);
    lightLayout->addWidget(numLightLabel);

    lightCheck = new QCheckBox(QString("On/Off"), this);
    lightCheck->setFixedSize(lightCheck->sizeHint());
    lightLayout->addWidget(lightCheck);

    //      _light->setOn();
    lightCheck->setChecked(true);

    lightLabel = new QLabel(QString("Advanced parameters"), this);
    lightLabel->setFixedHeight(20);
    lightLayout->addWidget(lightLabel);

    lightSlidersLayout = new QHBoxLayout();
    lightAmbientSlidersLayout = new QVBoxLayout();
    ambientLabel = new QLabel(QString("Ambient component"), this);
    ambientLabel->setFixedHeight(20);

    lightAmbientRedLayout = new QHBoxLayout();
    RaLabel = new QLabel(QString("R"), this);
    RaLabel->setFixedHeight(20);
    lightAmbientRedSlider = new QSlider(Qt::Horizontal, this);
    lightAmbientRedSlider->setMinimum(0);
    lightAmbientRedSlider->setMaximum(_lightValuesMax);
    lightAmbientRedSlider->setFixedWidth(100);
    lightAmbientRedSlider->setMinimumSize(lightAmbientRedSlider->sizeHint());
    lightAmbientRedSpinBox = new QSpinBox(this);
    lightAmbientRedSpinBox->setRange(0,_lightValuesMax);
    lightAmbientRedSpinBox->setSingleStep(1);
    lightAmbientRedLayout->addWidget(RaLabel);
    lightAmbientRedLayout->addWidget(lightAmbientRedSlider);
    lightAmbientRedLayout->addWidget(lightAmbientRedSpinBox);

    lightAmbientGreenLayout = new QHBoxLayout();
    GaLabel = new QLabel(QString("G"), this);
    GaLabel->setFixedHeight(20);
    lightAmbientGreenSlider = new QSlider(Qt::Horizontal, this);
    lightAmbientGreenSlider->setMinimum(0);
    lightAmbientGreenSlider->setMaximum(_lightValuesMax);
    lightAmbientGreenSlider->setFixedWidth(100);
    lightAmbientGreenSlider->setMinimumSize(lightAmbientGreenSlider->sizeHint());
    lightAmbientGreenSpinBox = new QSpinBox(this);
    lightAmbientGreenSpinBox->setRange(0,_lightValuesMax);
    lightAmbientGreenSpinBox->setSingleStep(1);
    lightAmbientGreenLayout->addWidget(GaLabel);
    lightAmbientGreenLayout->addWidget(lightAmbientGreenSlider);
    lightAmbientGreenLayout->addWidget(lightAmbientGreenSpinBox);

    lightAmbientBlueLayout = new QHBoxLayout();
    BaLabel = new QLabel(QString("B"), this);
    BaLabel->setFixedHeight(20);
    lightAmbientBlueSlider = new QSlider(Qt::Horizontal, this);
    lightAmbientBlueSlider->setMinimum(0);
    lightAmbientBlueSlider->setMaximum(_lightValuesMax);
    lightAmbientBlueSlider->setFixedWidth(100);
    lightAmbientBlueSlider->setMinimumSize(lightAmbientBlueSlider->sizeHint());
    lightAmbientBlueSpinBox = new QSpinBox(this);
    lightAmbientBlueSpinBox->setRange(0,_lightValuesMax);
    lightAmbientBlueSpinBox->setSingleStep(1);
    lightAmbientBlueLayout->addWidget(BaLabel);
    lightAmbientBlueLayout->addWidget(lightAmbientBlueSlider);
    lightAmbientBlueLayout->addWidget(lightAmbientBlueSpinBox);

    lightAmbientSlidersLayout->addWidget(ambientLabel);
    lightAmbientSlidersLayout->addLayout(lightAmbientRedLayout);
    lightAmbientSlidersLayout->addLayout(lightAmbientGreenLayout);
    lightAmbientSlidersLayout->addLayout(lightAmbientBlueLayout);

    lightDiffuseSlidersLayout = new QVBoxLayout();
    diffuseLabel = new QLabel(QString("Diffuse component"), this);
    diffuseLabel->setFixedHeight(20);

    lightDiffuseRedLayout = new QHBoxLayout();
    RdLabel = new QLabel(QString("R"), this);
    RdLabel->setFixedHeight(20);
    lightDiffuseRedSlider = new QSlider(Qt::Horizontal, this);
    lightDiffuseRedSlider->setMinimum(0);
    lightDiffuseRedSlider->setMaximum(_lightValuesMax);
    lightDiffuseRedSlider->setFixedWidth(100);
    lightDiffuseRedSlider->setMinimumSize(lightDiffuseRedSlider->sizeHint());
    lightDiffuseRedSpinBox = new QSpinBox(this);
    lightDiffuseRedSpinBox->setRange(0,_lightValuesMax);
    lightDiffuseRedSpinBox->setSingleStep(1);
    lightDiffuseRedLayout->addWidget(RdLabel);
    lightDiffuseRedLayout->addWidget(lightDiffuseRedSlider);
    lightDiffuseRedLayout->addWidget(lightDiffuseRedSpinBox);

    lightDiffuseGreenLayout = new QHBoxLayout();
    GdLabel = new QLabel(QString("G"), this);
    GdLabel->setFixedHeight(20);
    lightDiffuseGreenSlider = new QSlider(Qt::Horizontal, this);
    lightDiffuseGreenSlider->setMinimum(0);
    lightDiffuseGreenSlider->setMaximum(_lightValuesMax);
    lightDiffuseGreenSlider->setFixedWidth(100);
    lightDiffuseGreenSlider->setMinimumSize(lightDiffuseGreenSlider->sizeHint());
    lightDiffuseGreenSpinBox = new QSpinBox(this);
    lightDiffuseGreenSpinBox->setRange(0,_lightValuesMax);
    lightDiffuseGreenSpinBox->setSingleStep(1);
    lightDiffuseGreenLayout->addWidget(GdLabel);
    lightDiffuseGreenLayout->addWidget(lightDiffuseGreenSlider);
    lightDiffuseGreenLayout->addWidget(lightDiffuseGreenSpinBox);

    lightDiffuseBlueLayout = new QHBoxLayout();
    BdLabel = new QLabel(QString("B"), this);
    BdLabel->setFixedHeight(20);
    lightDiffuseBlueSlider = new QSlider(Qt::Horizontal, this);
    lightDiffuseBlueSlider->setMinimum(0);
    lightDiffuseBlueSlider->setMaximum(_lightValuesMax);
    lightDiffuseBlueSlider->setFixedWidth(100);
    lightDiffuseBlueSlider->setMinimumSize(lightDiffuseBlueSlider->sizeHint());
    lightDiffuseBlueSpinBox = new QSpinBox(this);
    lightDiffuseBlueSpinBox->setRange(0,_lightValuesMax);
    lightDiffuseBlueSpinBox->setSingleStep(1);
    lightDiffuseBlueLayout->addWidget(BdLabel);
    lightDiffuseBlueLayout->addWidget(lightDiffuseBlueSlider);
    lightDiffuseBlueLayout->addWidget(lightDiffuseBlueSpinBox);

    lightDiffuseSlidersLayout->addWidget(diffuseLabel);
    lightDiffuseSlidersLayout->addLayout(lightDiffuseRedLayout);
    lightDiffuseSlidersLayout->addLayout(lightDiffuseGreenLayout);
    lightDiffuseSlidersLayout->addLayout(lightDiffuseBlueLayout);

    lightSpecularSlidersLayout = new QVBoxLayout();
    specularLabel = new QLabel(QString("Specular component"), this);
    specularLabel->setFixedHeight(20);

    lightSpecularRedLayout = new QHBoxLayout();
    RsLabel = new QLabel(QString("R"), this);
    RsLabel->setFixedHeight(20);
    lightSpecularRedSlider = new QSlider(Qt::Horizontal, this);
    lightSpecularRedSlider->setMinimum(0);
    lightSpecularRedSlider->setMaximum(_lightValuesMax);
    lightSpecularRedSlider->setFixedWidth(100);
    lightSpecularRedSlider->setMinimumSize(lightSpecularRedSlider->sizeHint());
    lightSpecularRedSpinBox = new QSpinBox(this);
    lightSpecularRedSpinBox->setRange(0,_lightValuesMax);
    lightSpecularRedSpinBox->setSingleStep(1);
    lightSpecularRedLayout->addWidget(RsLabel);
    lightSpecularRedLayout->addWidget(lightSpecularRedSlider);
    lightSpecularRedLayout->addWidget(lightSpecularRedSpinBox);

    lightSpecularGreenLayout = new QHBoxLayout();
    GsLabel = new QLabel(QString("G"), this);
    GsLabel->setFixedHeight(20);
    lightSpecularGreenSlider = new QSlider(Qt::Horizontal, this);
    lightSpecularGreenSlider->setMinimum(0);
    lightSpecularGreenSlider->setMaximum(_lightValuesMax);
    lightSpecularGreenSlider->setFixedWidth(100);
    lightSpecularGreenSlider->setMinimumSize(lightSpecularGreenSlider->sizeHint());
    lightSpecularGreenSpinBox = new QSpinBox(this);
    lightSpecularGreenSpinBox->setRange(0,_lightValuesMax);
    lightSpecularGreenSpinBox->setSingleStep(1);
    lightSpecularGreenLayout->addWidget(GsLabel);
    lightSpecularGreenLayout->addWidget(lightSpecularGreenSlider);
    lightSpecularGreenLayout->addWidget(lightSpecularGreenSpinBox);

    lightSpecularBlueLayout = new QHBoxLayout();
    BsLabel = new QLabel(QString("B"), this);
    BsLabel->setFixedHeight(20);
    lightSpecularBlueSlider = new QSlider(Qt::Horizontal, this);
    lightSpecularBlueSlider->setMinimum(0);
    lightSpecularBlueSlider->setMaximum(_lightValuesMax);
    lightSpecularBlueSlider->setFixedWidth(100);
    lightSpecularBlueSlider->setMinimumSize(lightSpecularBlueSlider->sizeHint());
    lightSpecularBlueSpinBox = new QSpinBox(this);
    lightSpecularBlueSpinBox->setRange(0,_lightValuesMax);
    lightSpecularBlueSpinBox->setSingleStep(1);
    lightSpecularBlueLayout->addWidget(BsLabel);
    lightSpecularBlueLayout->addWidget(lightSpecularBlueSlider);
    lightSpecularBlueLayout->addWidget(lightSpecularBlueSpinBox);

    lightSpecularSlidersLayout->addWidget(specularLabel);
    lightSpecularSlidersLayout->addLayout(lightSpecularRedLayout);
    lightSpecularSlidersLayout->addLayout(lightSpecularGreenLayout);
    lightSpecularSlidersLayout->addLayout(lightSpecularBlueLayout);

    lightSlidersLayout->addLayout(lightAmbientSlidersLayout);
    lightSlidersLayout->addLayout(lightDiffuseSlidersLayout);
    lightSlidersLayout->addLayout(lightSpecularSlidersLayout);

    lightLayout->addLayout(lightSlidersLayout);

    setLight(light);
    buttonsLayout = new QHBoxLayout();
    cancelButton = new QPushButton("Quit", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(hide()));
    //   cancelButton->setFixedWidth(200);

    defaultSettingsButton = new QPushButton("Default parameters", this);
    connect(defaultSettingsButton, SIGNAL(clicked()), this, SLOT(defaultSettingsSlot()));
    //   defaultSettingsButton->setFixedWidth(200);

    buttonsLayout->addWidget(defaultSettingsButton);
    buttonsLayout->addWidget(cancelButton);
    mainLayout->addLayout(lightLayout);
    mainLayout->addLayout(buttonsLayout);
    mainLayout->activate();
}


void BasicLightDialog::loadSlot()
{
    QString f;
    f=QFileDialog::getOpenFileName( this,
                                    "Load a config file",
                                    QString::null,
                                    "XML files (*.xml)");
    if (f != QString::null) loadFileDialog(f);
}


void BasicLightDialog::saveSlot()
{
    QString f;
    f=QFileDialog::getSaveFileName( this,
                                    QString::null,
                                    "Save a config file"
                                    "XML files (*.xml)");
    if (f != QString::null)
    {
        if (f.endsWith(".xml")) saveFileDialog(f);
        else saveFileDialog(f + QString(".xml"));
    }
}




void BasicLightDialog::setLight(Light *light)
{
    _light=light;

    if (_light)
    {
        _light->setOn();
        lightCheck->setChecked(true);
        osg::Vec4 ambient = _light->getAmbient();
        _ambientRedInit = (int)(_lightValuesMax*ambient.r());
        _ambientGreenInit = (int)(_lightValuesMax*ambient.g());
        _ambientBlueInit = (int)(_lightValuesMax*ambient.b());
        lightAmbientRedSlider->setValue(_ambientRedInit);
        lightAmbientGreenSlider->setValue(_ambientGreenInit);
        lightAmbientBlueSlider->setValue(_ambientBlueInit);
        lightAmbientRedSpinBox->setValue(_ambientRedInit);
        lightAmbientGreenSpinBox->setValue(_ambientGreenInit);
        lightAmbientBlueSpinBox->setValue(_ambientBlueInit);

        osg::Vec4 diffuse = _light->getDiffuse();
        _diffuseRedInit = (int)(_lightValuesMax*diffuse.r());
        _diffuseGreenInit = (int)(_lightValuesMax*diffuse.g());
        _diffuseBlueInit = (int)(_lightValuesMax*diffuse.b());
        lightDiffuseRedSlider->setValue(_diffuseRedInit);
        lightDiffuseGreenSlider->setValue(_diffuseGreenInit);
        lightDiffuseBlueSlider->setValue(_diffuseBlueInit);
        lightDiffuseRedSpinBox->setValue(_diffuseRedInit);
        lightDiffuseGreenSpinBox->setValue(_diffuseGreenInit);
        lightDiffuseBlueSpinBox->setValue(_diffuseBlueInit);

        osg::Vec4 spec = _light->getSpecular();
        _specularRedInit = (int)(_lightValuesMax*spec.r());
        _specularGreenInit = (int)(_lightValuesMax*spec.g());
        _specularBlueInit = (int)(_lightValuesMax*spec.b());
        lightSpecularRedSlider->setValue(_specularRedInit);
        lightSpecularGreenSlider->setValue(_specularGreenInit);
        lightSpecularBlueSlider->setValue(_specularBlueInit);
        lightSpecularRedSpinBox->setValue(_specularRedInit);
        lightSpecularGreenSpinBox->setValue(_specularGreenInit);
        lightSpecularBlueSpinBox->setValue(_specularBlueInit);

        QObject::connect(lightCheck, SIGNAL(stateChanged(int)), _light, SLOT(onOffSlot(int)));

        QObject::connect(lightAmbientRedSlider, SIGNAL(valueChanged(int)), this, SLOT(ambientRedSlot(int)));
        QObject::connect(lightAmbientRedSlider, SIGNAL(valueChanged(int)), lightAmbientRedSpinBox, SLOT(setValue(int)));
        QObject::connect(lightAmbientRedSpinBox, SIGNAL(valueChanged(int)), this, SLOT(ambientRedSlot(int)));
        QObject::connect(lightAmbientRedSpinBox, SIGNAL(valueChanged(int)), lightAmbientRedSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(ambientRedValueChanged(float)), _light, SLOT(ambientRedSlot(float)));

        QObject::connect(lightAmbientGreenSlider, SIGNAL(valueChanged(int)), this, SLOT(ambientGreenSlot(int)));
        QObject::connect(lightAmbientGreenSlider, SIGNAL(valueChanged(int)), lightAmbientGreenSpinBox, SLOT(setValue(int)));
        QObject::connect(lightAmbientGreenSpinBox, SIGNAL(valueChanged(int)), this, SLOT(ambientGreenSlot(int)));
        QObject::connect(lightAmbientGreenSpinBox, SIGNAL(valueChanged(int)), lightAmbientGreenSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(ambientGreenValueChanged(float)), _light, SLOT(ambientGreenSlot(float)));

        QObject::connect(lightAmbientBlueSlider, SIGNAL(valueChanged(int)), this, SLOT(ambientBlueSlot(int)));
        QObject::connect(lightAmbientBlueSlider, SIGNAL(valueChanged(int)), lightAmbientBlueSpinBox, SLOT(setValue(int)));
        QObject::connect(lightAmbientBlueSpinBox, SIGNAL(valueChanged(int)), this, SLOT(ambientBlueSlot(int)));
        QObject::connect(lightAmbientBlueSpinBox, SIGNAL(valueChanged(int)), lightAmbientBlueSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(ambientBlueValueChanged(float)), _light, SLOT(ambientBlueSlot(float)));

        QObject::connect(lightDiffuseRedSlider, SIGNAL(valueChanged(int)), this, SLOT(diffuseRedSlot(int)));
        QObject::connect(lightDiffuseRedSlider, SIGNAL(valueChanged(int)), lightDiffuseRedSpinBox, SLOT(setValue(int)));
        QObject::connect(lightDiffuseRedSpinBox, SIGNAL(valueChanged(int)), this, SLOT(diffuseRedSlot(int)));
        QObject::connect(lightDiffuseRedSpinBox, SIGNAL(valueChanged(int)), lightDiffuseRedSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(diffuseRedValueChanged(float)), _light, SLOT(diffuseRedSlot(float)));

        QObject::connect(lightDiffuseGreenSlider, SIGNAL(valueChanged(int)), this, SLOT(diffuseGreenSlot(int)));
        QObject::connect(lightDiffuseGreenSlider, SIGNAL(valueChanged(int)), lightDiffuseGreenSpinBox, SLOT(setValue(int)));
        QObject::connect(lightDiffuseGreenSpinBox, SIGNAL(valueChanged(int)), this, SLOT(diffuseGreenSlot(int)));
        QObject::connect(lightDiffuseGreenSpinBox, SIGNAL(valueChanged(int)), lightDiffuseGreenSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(diffuseGreenValueChanged(float)), _light, SLOT(diffuseGreenSlot(float)));

        QObject::connect(lightDiffuseBlueSlider, SIGNAL(valueChanged(int)), this, SLOT(diffuseBlueSlot(int)));
        QObject::connect(lightDiffuseBlueSlider, SIGNAL(valueChanged(int)), lightDiffuseBlueSpinBox, SLOT(setValue(int)));
        QObject::connect(lightDiffuseBlueSpinBox, SIGNAL(valueChanged(int)), this, SLOT(diffuseBlueSlot(int)));
        QObject::connect(lightDiffuseBlueSpinBox, SIGNAL(valueChanged(int)), lightDiffuseBlueSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(diffuseBlueValueChanged(float)), _light, SLOT(diffuseBlueSlot(float)));

        QObject::connect(lightSpecularRedSlider, SIGNAL(valueChanged(int)), this, SLOT(specularRedSlot(int)));
        QObject::connect(lightSpecularRedSlider, SIGNAL(valueChanged(int)), lightSpecularRedSpinBox, SLOT(setValue(int)));
        QObject::connect(lightSpecularRedSpinBox, SIGNAL(valueChanged(int)), this, SLOT(specularRedSlot(int)));
        QObject::connect(lightSpecularRedSpinBox, SIGNAL(valueChanged(int)), lightSpecularRedSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(specularRedValueChanged(float)), _light, SLOT(specularRedSlot(float)));

        QObject::connect(lightSpecularGreenSlider, SIGNAL(valueChanged(int)), this, SLOT(specularGreenSlot(int)));
        QObject::connect(lightSpecularGreenSlider, SIGNAL(valueChanged(int)), lightSpecularGreenSpinBox, SLOT(setValue(int)));
        QObject::connect(lightSpecularGreenSpinBox, SIGNAL(valueChanged(int)), this, SLOT(specularGreenSlot(int)));
        QObject::connect(lightSpecularGreenSpinBox, SIGNAL(valueChanged(int)), lightSpecularGreenSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(specularGreenValueChanged(float)), _light, SLOT(specularGreenSlot(float)));

        QObject::connect(lightSpecularBlueSlider, SIGNAL(valueChanged(int)), this, SLOT(specularBlueSlot(int)));
        QObject::connect(lightSpecularBlueSlider, SIGNAL(valueChanged(int)), lightSpecularBlueSpinBox, SLOT(setValue(int)));
        QObject::connect(lightSpecularBlueSpinBox, SIGNAL(valueChanged(int)), this, SLOT(specularBlueSlot(int)));
        QObject::connect(lightSpecularBlueSpinBox, SIGNAL(valueChanged(int)), lightSpecularBlueSlider, SLOT(setValue(int)));
        QObject::connect(this, SIGNAL(specularBlueValueChanged(float)), _light, SLOT(specularBlueSlot(float)));

        QObject::connect(_light, SIGNAL(updateSignal()), (BaseViewer*)_viewer, SLOT(update()));
    }
} 

void BasicLightDialog::defaultSettingsSlot()
{
    lightCheck->setChecked(true);

    lightAmbientRedSlider->setValue(_ambientRedInit);
    lightAmbientGreenSlider->setValue(_ambientBlueInit);
    lightAmbientBlueSlider->setValue(_ambientGreenInit);
    lightAmbientRedSpinBox->setValue(_ambientRedInit);
    lightAmbientGreenSpinBox->setValue(_ambientBlueInit);
    lightAmbientBlueSpinBox->setValue(_ambientGreenInit);

    lightDiffuseRedSlider->setValue(_diffuseRedInit);
    lightDiffuseGreenSlider->setValue(_diffuseBlueInit);
    lightDiffuseBlueSlider->setValue(_diffuseGreenInit);
    lightDiffuseRedSpinBox->setValue(_diffuseRedInit);
    lightDiffuseGreenSpinBox->setValue(_diffuseBlueInit);
    lightDiffuseBlueSpinBox->setValue(_diffuseGreenInit);

    lightSpecularRedSlider->setValue(_specularRedInit);
    lightSpecularGreenSlider->setValue(_specularBlueInit);
    lightSpecularBlueSlider->setValue(_specularGreenInit);
    lightSpecularRedSpinBox->setValue(_specularRedInit);
    lightSpecularGreenSpinBox->setValue(_specularBlueInit);
    lightSpecularBlueSpinBox->setValue(_specularGreenInit);
}


void BasicLightDialog::loadFileDialog(const QString& filename)
{
    QDomDocument doc("config");
    QFile file(filename);

    if (!file.open(QIODevice::ReadOnly)) return;

    if (!doc.setContent(&file)) {file.close(); return;}
    file.close();
    if (!(doc.elementsByTagName("BasicLightDialog")).count()) return;
    if ((doc.elementsByTagName("lightStatus")).count()) {doc.elementsByTagName("lightStatus").item(0).toElement().text().toInt() ? lightCheck->setChecked(true): lightCheck->setChecked(false);}
    if ((doc.elementsByTagName("ambientRedValue")).count()) lightAmbientRedSlider->setValue(doc.elementsByTagName("ambientRedValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("ambientGreenValue")).count()) lightAmbientGreenSlider->setValue(doc.elementsByTagName("ambientGreenValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("ambientBlueValue")).count()) lightAmbientBlueSlider->setValue(doc.elementsByTagName("ambientBlueValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("diffuseRedValue")).count()) lightDiffuseRedSlider->setValue(doc.elementsByTagName("diffuseRedValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("diffuseGreenValue")).count()) lightDiffuseGreenSlider->setValue(doc.elementsByTagName("diffuseGreenValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("diffuseBlueValue")).count()) lightDiffuseBlueSlider->setValue(doc.elementsByTagName("diffuseBlueValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("specularRedValue")).count()) lightSpecularRedSlider->setValue(doc.elementsByTagName("specularRedValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("specularGreenValue")).count()) lightSpecularGreenSlider->setValue(doc.elementsByTagName("specularGreenValue").item(0).toElement().text().toInt());
    if ((doc.elementsByTagName("specularBlueValue")).count()) lightSpecularBlueSlider->setValue(doc.elementsByTagName("specularBlueValue").item(0).toElement().text().toInt());
}


void BasicLightDialog::saveFileDialog(const QString& filename)
{
    QDomDocument doc("config");
    QDomElement root = doc.createElement("BasicLightDialog");
    doc.appendChild(root);

    QDomElement lighting = doc.createElement("lighting");
    root.appendChild(lighting);

    QDomElement lightStatus = doc.createElement("lightStatus");
    lighting.appendChild(lightStatus);
    int lstatus;
    QString str;
    lightCheck->isChecked() ? lstatus=1:lstatus=0;
    QDomText value3 = doc.createTextNode(str.sprintf("%d",lstatus));
    lightStatus.appendChild(value3);

    QDomElement ambientRedValue = doc.createElement("ambientRedValue");
    lighting.appendChild(ambientRedValue);
    QDomText value4 = doc.createTextNode(str.sprintf("%d",lightAmbientRedSlider->value()));
    ambientRedValue.appendChild(value4);

    QDomElement ambientGreenValue = doc.createElement("ambientGreenValue");
    lighting.appendChild(ambientGreenValue);
    QDomText value5 = doc.createTextNode(str.sprintf("%d",lightAmbientGreenSlider->value()));
    ambientGreenValue.appendChild(value5);

    QDomElement ambientBlueValue = doc.createElement("ambientBlueValue");
    lighting.appendChild(ambientBlueValue);
    QDomText value6 = doc.createTextNode(str.sprintf("%d",lightAmbientBlueSlider->value()));
    ambientBlueValue.appendChild(value6);

    QDomElement diffuseRedValue = doc.createElement("diffuseRedValue");
    lighting.appendChild(diffuseRedValue);
    QDomText value7 = doc.createTextNode(str.sprintf("%d",lightDiffuseRedSlider->value()));
    diffuseRedValue.appendChild(value7);

    QDomElement diffuseGreenValue = doc.createElement("diffuseGreenValue");
    lighting.appendChild(diffuseGreenValue);
    QDomText value8 = doc.createTextNode(str.sprintf("%d",lightDiffuseGreenSlider->value()));
    diffuseGreenValue.appendChild(value8);

    QDomElement diffuseBlueValue = doc.createElement("diffuseBlueValue");
    lighting.appendChild(diffuseBlueValue);
    QDomText value9 = doc.createTextNode(str.sprintf("%d",lightDiffuseBlueSlider->value()));
    diffuseBlueValue.appendChild(value9);

    QDomElement specularRedValue = doc.createElement("specularRedValue");
    lighting.appendChild(specularRedValue);
    QDomText value10 = doc.createTextNode(str.sprintf("%d",lightSpecularRedSlider->value()));
    specularRedValue.appendChild(value10);

    QDomElement specularGreenValue = doc.createElement("specularGreenValue");
    lighting.appendChild(specularGreenValue);
    QDomText value11 = doc.createTextNode(str.sprintf("%d",lightSpecularGreenSlider->value()));
    specularGreenValue.appendChild(value11);

    QDomElement specularBlueValue = doc.createElement("specularBlueValue");
    lighting.appendChild(specularBlueValue);
    QDomText value12 = doc.createTextNode(str.sprintf("%d",lightSpecularBlueSlider->value()));
    specularBlueValue.appendChild(value12);

    QString xml = doc.toString();

    QFile file(filename);

    if (file.open(QIODevice::WriteOnly))

    {
        QTextStream stream(&file);
        stream << xml << "\n";
        file.close();
    }
}
