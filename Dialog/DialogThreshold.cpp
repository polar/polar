/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>

#include <QLayout>
#include <QRadioButton>
#include <QPushButton>
#include <QSplitter>
#include <QGroupBox>
#include <QButtonGroup>
#include <QColorDialog>

#include "DialogThreshold.h"

using namespace std;
using namespace PoLAR;


DialogThreshold::DialogThreshold()
{
    frames.clear();
    color = qRgb(0,255,0);

    QVBoxLayout *vbl = new QVBoxLayout(this);

    QWidget *vbox = new QWidget();
    QVBoxLayout *vlayout = new QVBoxLayout();


    // first slider

    QGroupBox *btgroup1=new QGroupBox(this);
    btgroup1->setTitle(QString("Threshold min"));
    vlayout->addWidget(btgroup1);
    QVBoxLayout *slider1Layout = new QVBoxLayout();
    minLabel  = new QLabel(btgroup1);
    minLabel->setFixedHeight(20);
    minLabel->setNum(0);
    minSlider = new QSlider(Qt::Horizontal, btgroup1);
    minSlider->setMinimum(0);
    minSlider->setMaximum(254);
    minSlider->setPageStep(1);
    minSlider->setValue(0);
    minSlider->setTracking(true);
    minLabel->adjustSize();
    minSlider->adjustSize();
    slider1Layout->addWidget(minLabel);
    slider1Layout->addWidget(minSlider);
    btgroup1->setLayout(slider1Layout);

    connect( minSlider, SIGNAL(valueChanged(int)), this, SLOT(setMinValue(int)) );
    // must then send a signal to glimage

    // second slider

    QGroupBox *btgroup2=new QGroupBox(this);
    btgroup2->setTitle(QString("Threshold max"));
    vlayout->addWidget(btgroup2);
    QVBoxLayout *slider2Layout = new QVBoxLayout();
    maxLabel  = new QLabel(QString("Threshold max"), btgroup2);
    maxLabel->setFixedHeight(20);
    maxLabel->setNum(1);
    maxSlider = new QSlider(Qt::Horizontal, btgroup2);
    maxSlider->setMinimum(0);
    maxSlider->setMaximum(255);
    maxSlider->setPageStep(1);
    maxSlider->setValue(255);
    maxSlider->setTracking(true);
    maxLabel->adjustSize();
    maxSlider->adjustSize();
    slider2Layout->addWidget(maxLabel);
    slider2Layout->addWidget(maxSlider);
    btgroup2->setLayout(slider2Layout);

    connect( maxSlider, SIGNAL(valueChanged(int)), this, SLOT(setMaxValue(int)) );
    connect( maxSlider, SIGNAL(valueChanged(int)), this, SLOT(preview()));

    connect( minSlider, SIGNAL(valueChanged(int)), this, SLOT(updateMaxValue(int)) );
    connect( maxSlider, SIGNAL(valueChanged(int)), this, SLOT(updateMinValue(int)) );

    // third slider
    QGroupBox *btgroup3=new QGroupBox(this);
    btgroup3->setTitle(QString("Transparency"));
    vlayout->addWidget(btgroup3);
    QVBoxLayout *slider3Layout = new QVBoxLayout();
    alphaLabel  = new QLabel(QString("Transparency"), btgroup3);
    alphaLabel->setFixedHeight(20);
    alphaLabel->setNum(0.5);
    alphaSlider = new QSlider(Qt::Horizontal, btgroup3);
    alphaSlider->setMinimum(0);
    alphaSlider->setMaximum(100);
    alphaSlider->setPageStep(1);
    alphaSlider->setValue(50);
    alphaSlider->setTracking(true);
    alphaLabel->adjustSize();
    alphaSlider->adjustSize();
    slider3Layout->addWidget(alphaLabel);
    slider3Layout->addWidget(alphaSlider);
    btgroup3->setLayout(slider3Layout);

    vbox->adjustSize();
    vbox->setLayout(vlayout);
    vbl->addWidget(vbox);

    connect( alphaSlider, SIGNAL(valueChanged(int)), this, SLOT(preview()) );
    connect( alphaSlider, SIGNAL(valueChanged(int)), this, SLOT(setAlpha(int)) );


    // Buttons
    QWidget *hbox = new QWidget();
    QHBoxLayout *hlayout = new QHBoxLayout();
    QPushButton *colorb = new QPushButton(QString("Color"), this);
    hlayout->addWidget(colorb);
    QPushButton *applyb = new QPushButton(QString("Apply"), this);
    hlayout->addWidget(applyb);
    QPushButton *okb = new QPushButton(QString("OK"), this);
    hlayout->addWidget(okb);
    QPushButton *cancelButton = new QPushButton(QString("Cancel"), this);
    hlayout->addWidget(cancelButton);

    hbox->adjustSize();
    hbox->setLayout(hlayout);
    vbl->addWidget(hbox);

    QObject::connect(colorb, SIGNAL(clicked()), this, SLOT(changeBgColor()));
    QObject::connect(applyb, SIGNAL(clicked()), this, SLOT(sendThresholds()));
    QObject::connect(okb, SIGNAL(clicked()), this, SLOT(sendThresholds()));
    QObject::connect(okb, SIGNAL(clicked()), this, SLOT(hide()));
    QObject::connect(cancelButton, SIGNAL(clicked()), this, SLOT(hide()));
}

DialogThreshold::~DialogThreshold()
{
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        ThresholdFrame *tf = (*it);
        delete tf;
        ++it;
    }
    frames.clear();
}

void DialogThreshold::showEvent(QShowEvent *e)
{
    QDialog::showEvent(e);
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        ThresholdFrame *f = *it;
        f->updateImage(minSlider->value(), maxSlider->value());
        connect(f->getViewer()->getBgImage(), SIGNAL(newImageSignal()), this, SLOT(updateImage()));
        ++it;
    }
    emit showSignal();
}

void DialogThreshold::hideEvent(QHideEvent *e)
{
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        ThresholdFrame *f = *it;
        f->showOverlay(false);
        disconnect(f->getViewer()->getBgImage(), SIGNAL(newImageSignal()), this, SLOT(updateImage()));
        ++it;
    }
    QDialog::hideEvent(e);
    emit hideSignal();
}

void DialogThreshold::closeEvent(QCloseEvent *e)
{
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        ThresholdFrame *f = *it;
        f->showOverlay(false);
        disconnect(f->getViewer()->getBgImage(), SIGNAL(newImageSignal()), this, SLOT(updateImage()));
        ++it;
    }
    QDialog::closeEvent(e);
}

void DialogThreshold::addFrame(Viewer2D3D *viewer)
{
    if (viewer)
    {
        ThresholdFrame *f = new ThresholdFrame(viewer,color, alphaSlider->value()/100.0);
        frames.push_front(f);
        if (isVisible())
        {
            f->updateImage(minSlider->value(), maxSlider->value());
            connect(viewer->getBgImage(), SIGNAL(newImageSignal()), this, SLOT(updateImage()));
        }
    }
}


void DialogThreshold::sendThresholds()
{
    emit getThresholds(minSlider->value(), maxSlider->value());
}


void DialogThreshold::preview()
{
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        (*it)->getViewer()->update();
        ++it;
    }

}

void DialogThreshold::updateMinValue(int i)
{
    minSlider->setMaximum(i > 1 ? i-1 : 0);
}

void DialogThreshold::updateMaxValue(int i)
{
    maxSlider->setMinimum(i < 255 ? i+1 : 255);
}


void DialogThreshold::setAlpha(int i)
{
    alphaLabel->setNum(i/100.0);
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        (*it)->setAlpha(alphaSlider->value()/100.0);
        ++it;
    }
}

void DialogThreshold::setMinValue(int i)
{
    minLabel->setNum(i/255.0);
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        (*it)->updateImage(minSlider->value(), maxSlider->value());
        ++it;
    }
}

void DialogThreshold::setMaxValue(int i)
{
    maxLabel->setNum(i/255.0);
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        (*it)->updateImage(minSlider->value(), maxSlider->value());
        ++it;
    }
}

void DialogThreshold::changeBgColor()
{
    QColor newcol = QColorDialog::getColor(QColor(color));
    if(newcol.isValid())
    {
        color = newcol.rgb();
        std::list<ThresholdFrame*>::iterator it = frames.begin();
        while(it != frames.end())
        {
            (*it)->setColor(color);
            ++it;
        }
    }
}


void DialogThreshold::updateImage()
{
    std::list<ThresholdFrame*>::iterator it = frames.begin();
    while(it != frames.end())
    {
        if ((*it)->getViewer()->getBgImage() == sender())
            (*it)->updateImage(minSlider->value(), maxSlider->value());
        ++it;
    }
    
}
void ThresholdFrame::allocate()
{
    if(_viewer->getBgImage()->valid() && !_image->valid())
    {
        _width = _viewer->getBgImage()->getWidth();
        _height = _viewer->getBgImage()->getHeight();

        _image->allocateImage(_width, _height, 3, GL_RGBA, GL_UNSIGNED_BYTE);
        float wT = 1.0, hT = 1.0;

        osg::ref_ptr<osg::Geode> g=new osg::Geode;
        g->addDrawable(osg::createTexturedQuadGeometry( osg::Vec3(0,0,0),
                                                        osg::Vec3(_width,0,0),
                                                        osg::Vec3(0,_height,0),
                                                        wT,hT
                                                        )
                       );
        osg::ref_ptr<osg::Texture2D> t = new osg::Texture2D(_image.get());
        t->setResizeNonPowerOfTwoHint(false);
        _stateSet = g->getOrCreateStateSet();
        g->getOrCreateStateSet()->setTextureAttributeAndModes(0,t.get(),osg::StateAttribute::ON);
        g->getOrCreateStateSet()->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
        g->getOrCreateStateSet()->setMode(GL_DEPTH_TEST,osg::StateAttribute::OFF);
        g->getOrCreateStateSet()->setAttribute(new osg::BlendColor(osg::Vec4f(0,0,0,_alpha)), osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
        g->getOrCreateStateSet()->setAttribute(new osg::BlendFunc(osg::BlendFunc::CONSTANT_ALPHA, osg::BlendFunc::ONE_MINUS_CONSTANT_ALPHA), osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
        g->getOrCreateStateSet()->setMode(GL_BLEND,osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
        g->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN|osg::StateAttribute::OVERRIDE);
        _switch->addChild(g.get());
        _viewer->getOrthoCamera()->addChild(_switch.get());
    }
}

void ThresholdFrame::setColor(QRgb c)
{
    if (_image->valid())
    {
        _color = RGBA(c);
        unsigned int *ti;
        int k,l;
        for (k=0; k<_height; k++)
        {
            ti = (unsigned int *)(_image->data())+k*_width;
            for (l=0; l<_width; l++, ti++)
                if (*ti) *ti = _color;
        }
        _image->dirty();
        _viewer->update();
    }
}
void ThresholdFrame::setAlpha(float alpha)
{
    if (_stateSet)
    {
        _alpha = alpha;
        _stateSet->setAttribute(new osg::BlendColor(osg::Vec4f(0,0,0,_alpha)), osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
        _viewer->update();
    }
}

void ThresholdFrame::showOverlay(bool b)
{
    if (b) _switch->setAllChildrenOn();
    else  _switch->setAllChildrenOff();
    _viewer->update();
}

void ThresholdFrame::updateImage(int tmin, int tmax)
{
    allocate();
    if (_viewer->getBgImage() && _viewer->getBgImage()->valid() && _image->valid())
    {
        unsigned char *td = _viewer->getBgImage()->getMonoImage(_data);
        unsigned int *ti;
        int k,l;
        for (k=0; k<_height; k++)
        {
            ti = (unsigned int *)(_image->data())+k*_width;
            for (l=0; l<_width; l++, td++, ti++)
            {
                if (*td >= tmin && *td <= tmax) *ti = _color;
                else *ti = 0;
            }
        }
        _image->dirty();
        _switch->setAllChildrenOn();
        _viewer->update();
    }
}
