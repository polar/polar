#ifndef INTERFACE_H
#define INTERFACE_H

#include <QtCore/QStringList>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QInputDialog>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QComboBox>

#include <PoLAR/VegaObject3D.h>
#include <PoLAR/SimulationManager.h>
#include "VegaViewer.h"
#include "VegaSimulation.h"


class Interface : public QMainWindow
{
    Q_OBJECT

public:
    Interface(QString &configFile);

    ~Interface();


signals:
    void startEditImageSignal();
    void startEditMarkersSignal();


private slots:

    void exit();
    void readConfigFile();
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);

private:

    void loadObject();

    void createSimulation();

    osg::ref_ptr<VegaViewer> _viewer;
    osg::ref_ptr<PoLAR::VegaObject3D> _object3D;
    osg::ref_ptr<PoLAR::VegaObject3D> _volumetricObj;
    osg::ref_ptr<VegaSimulation> _vegaSim;
    osg::ref_ptr<PoLAR::SimulationManager> _simMan;
    QString _configFileName;
    QString _vegaFile;
    QString _objFile;
    QString _volumetricFile;
    QString _fixedVertices;
    bool _paused;
    QToolBar *_toolbar;
    QLabel *_statuslabel;
    QMessageBox *_mb;
};



#endif // INTERFACE_H
