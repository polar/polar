#include "Interface.h"
#include "../../Examples/props.h"
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/io_utils>
#include <osgManipulator/TabBoxDragger>
#include <osgViewer/ViewerEventHandlers>
#include <osg/PolygonMode>
#include <QColorDialog>
#include <QFileDialog>
#include <QDebug>

#include <PoLAR/DraggerManipulationHandler.h>
#include <PoLAR/VertexSelectionHandler.h>
#include <PoLAR/SceneGraphManipulator.h>
#include <PoLAR/Util.h>


using namespace PoLAR;

Interface::Interface(QString& configFile):
    QMainWindow(),
    _configFileName(configFile),
    _paused(true)
{
    //showMaximized();
    resize(1200, 900);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));
    QString dir = dataPath() + "icons/";

    // Create a Viewer with shadows management
    _viewer = new VegaViewer(this, "viewer", 0);
    setCentralWidget(_viewer.get());
    _viewer->addEventHandler(new osgViewer::StatsHandler);
    PoLAR::SceneGraphManipulator* manip = new PoLAR::SceneGraphManipulator;
    manip->setRotateCameraMouseButton(osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON);
    manip->setTranslateZMouseButton(osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON);
    manip->setTranslateXYMouseButton(osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON, osgGA::GUIEventAdapter::KEY_Control_L);
    _viewer->setCameraManipulator(manip);

    PoLAR::VertexSelectionHandler* vertexHandler = new PoLAR::VertexSelectionHandler(true);
    vertexHandler->setCharacterSize(10);
    _viewer->addEventHandler(vertexHandler);


    QObject::connect(_viewer.get(), SIGNAL(shown()), this, SLOT(readConfigFile()), Qt::QueuedConnection);

    _toolbar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea, _toolbar);
    _toolbar->setOrientation(Qt::Vertical);

    _statuslabel = new QLabel(_toolbar);
    _statuslabel->setText(*(new QString("Tools")));

    _toolbar->addAction(QIcon(dir+"exit.png"), QString("Quit"), this, SLOT(exit()));


    // add focus to this interface in order to get the keyboard events
    this->setFocusPolicy(Qt::StrongFocus);
    this->setFocus();
    // remove the focus from the Viewer, otherwise it will bypass the interface's events
    _viewer->setFocusPolicy(Qt::NoFocus);

    _mb = new QMessageBox(
                QMessageBox::Information,
                QString("Help: shortkeys"),
                QString("Shortkeys:\n"),
                QMessageBox::NoButton,
                this);
    _mb->setWindowModality(Qt::NonModal);
}

Interface::~Interface()
{
}


void Interface::readConfigFile()
{
    //read JSON config file
    QFile configFile(_configFileName);
    if (!configFile.open(QIODevice::ReadOnly))
    {
        qWarning() << "Could not open config file" << _configFileName;
        return;
    }

    QByteArray saveData = configFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonObject jsonObj(loadDoc.object());
    _vegaFile = jsonObj["veg"].toString();
    _objFile = jsonObj["obj"].toString();
    _volumetricFile = jsonObj["volumetric"].toString();
    _fixedVertices = jsonObj["fixedVerts"].toString();

    bool fileOK = true;
    if(!Util::fileExists(_vegaFile.toStdString()))
    {
        qWarning() << "Vega file" << _vegaFile << ": file not found";
        fileOK = false;
    }
    if(!Util::fileExists(_objFile.toStdString()))
    {
        qWarning() << "obj file" << _objFile << ": file not found";
        fileOK = false;
    }
    if(fileOK) loadObject();

    createSimulation();
}



void Interface::loadObject()
{
    if(!_vegaFile.toStdString().empty() && !_objFile.toStdString().empty())
    {
        // Load the model given in parameter
        osg::ref_ptr<osg::Node> loadedModel;
        osgDB::Options* options = new osgDB::Options;
        options->setOptionString("noRotation");
        loadedModel = osgDB::readNodeFile(_objFile.toStdString(), options);
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;
        if(!loadedModel)
        {
            std::cerr << "Unable to load model" << std::endl;
        }
        else
        {
            optimizer.optimize(loadedModel.get());
            loadedModel->accept(smooth);
            loadedModel->setName("DeformationOBJ");
            _object3D = new PoLAR::VegaObject3D(loadedModel.get(), _vegaFile.toStdString().c_str());
            _object3D->setEditOn();
            //_object3D->setPickOn();
            //_object3D->rotateXSlot(-M_PI/2);
            _viewer->addObject3D(_object3D.get());

            osg::Matrixd P = _viewer->createVisionProjection(_viewer->width(), _viewer->height());   // Create a default projection
            _viewer->setProjection(P);

            osg::Vec3 center = _viewer->getSceneGraph()->getScene()->getBound().center();
            osg::Matrixd M(1, 0, 0, 0,
                           0, -1, 0, 0,
                           0, 0, -1, 0,
                           center[0], center[1], P(3,2), 1);
            _viewer->setPose(M);

            _viewer->setTrackNode(loadedModel.get());
            _viewer->startManipulateSceneSlot();


            if(!_volumetricFile.toStdString().empty())
            {
                osg::ref_ptr<osg::Node> voluModel;
                voluModel = osgDB::readNodeFile(_volumetricFile.toStdString(), options);
                if(!loadedModel)
                {
                    std::cerr << "Unable to load model" << std::endl;
                }
                else
                {
                    voluModel->setName("VolumetricOBJ");
                    _volumetricObj = new PoLAR::VegaObject3D(voluModel.get(), _vegaFile.toStdString().c_str());
                    _volumetricObj->getOrCreateStateSet()->setAttributeAndModes(new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE),  osg::StateAttribute::OVERRIDE);
                    _volumetricObj->setPickOn();
                    _viewer->addObject3D(_volumetricObj.get());
                }
            }
            if(!_fixedVertices.toStdString().empty())
            {

            }
        }
    }
    else
        qWarning() << "Unable to find files";
}


void Interface::createSimulation()
{
    if(_object3D.valid() && _volumetricObj.valid())
    {
        _vegaSim = new VegaSimulation(_object3D.get(), _volumetricObj.get());
        _simMan = new PoLAR::SimulationManager(_vegaSim.get(), 10.0, true);
        _simMan->launch();
        _paused = false;
    }
}


void Interface::exit()
{
    close();
}

// Add key bindings
void Interface::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Q:
    {
        this->exit();
        break;
    }
    case Qt::Key_H:
        // Show the help message box
        _mb->show();
        break;
    case Qt::Key_D:
    {
        // Hide/display background image
        _viewer->bgImageToggle();
        break;
    }
    case Qt::Key_I:
    {
        // Set the image interaction to be active
        _viewer->startEditImageSlot();
        break;
    }
    case Qt::Key_W:
    {
        // Set the scene graph interaction to be active
        _viewer->startManipulateSceneSlot();
        break;
    }

    case Qt::Key_P:
    {
        osg::Matrixd p = _viewer->getPose();
        std::cout << p << std::endl;
        break;
    }

    case Qt::Key_G:
    {
        QString fname=QFileDialog::getSaveFileName( this,
                                                    "Choose a filename to save the snapshot image",
                                                    QString::null,
                                                    "PNG images (*.png)");
        if (!fname.isEmpty()) _viewer->grab().save(fname, "PNG");

        break;
    }
    case Qt::Key_F:
    {
        if(_simMan)
        {
            if(!_paused)
            {
                _simMan->pause();
                _paused = true;
            }
            else
            {
                _simMan->play();
                _paused = false;
            }
        }
        break;
    }


    default:
        _viewer->notify(event);
    }
}

void Interface::keyReleaseEvent(QKeyEvent *event)
{
    //simply forwarding to the viewer
    _viewer->notify(event);
}
