#ifndef VEGAVIEWER_H
#define VEGAVIEWER_H

#include <PoLAR/Viewer.h>


class VegaViewer: public PoLAR::Viewer
{

    Q_OBJECT
public:
    VegaViewer(QWidget *parent=0, const char *name=0, Qt::WindowFlags f=0);

signals:
  void shown();

public slots:

    void showEvent(QShowEvent *event);
};

#endif // VEGAVIEWER_H
