#include "VegaSimulation.h"
#include <iostream>

VegaSimulation::VegaSimulation(PoLAR::VegaObject3D *object, PoLAR::VegaObject3D *volumetricObj):
    _object(object), _volumetricObj(volumetricObj)
{
}


void VegaSimulation::initialize()
{
    _volumetricObj->initForces();
    _object->initForces();
}


void VegaSimulation::run()
{

    std::cout << "run\n";
    _volumetricObj->applyForces();
    _object->applyForces();


    int code = _volumetricObj->doTimestep();
    if (code != 0)
    {
        std::cerr << "The integrator went unstable. Reduce the timestep, or increase the number of substeps per timestep.\n";
    }
    //_object->updateOsgMesh();

    _volumetricObj->requestMeshUpdate();
    _object->requestMeshUpdate();

}
