#ifndef VEGASIMULATION_H
#define VEGASIMULATION_H

#include <PoLAR/SimulationWorker.h>
#include <PoLAR/VegaObject3D.h>
#include <list>

class DraggedVertexForce: public QObject, PoLAR::VegaInteraction
{
    Q_OBJECT

public:

    DraggedVertexForce():
        QObject()
    {}

    void initializeForce(PoLAR::VegaObject3D *object)
    {

    }

    void applyForce(PoLAR::VegaObject3D *object)
    {

    }

    void resetForce(PoLAR::VegaObject3D *object)
    {

    }

public slots:
    void updateData(float mouseX, float mouseY, osg::Vec3f vertex)
    {
        _mouseX = mouseX;
        _mouseY = mouseY;
        _pulledVertex = vertex;
    }



protected:

    float _mouseX;
    float _mouseY;
    osg::Vec3f _pulledVertex;
};


class VegaSimulation: public PoLAR::SimulationWorker
{
public:
    VegaSimulation(PoLAR::VegaObject3D* object, PoLAR::VegaObject3D* volumetricObj);

    ~VegaSimulation()
    {}

protected:
    void initialize();

    void run();

private:
    osg::ref_ptr<PoLAR::VegaObject3D> _object;
    osg::ref_ptr<PoLAR::VegaObject3D> _volumetricObj;
};

#endif // VEGASIMULATION_H
