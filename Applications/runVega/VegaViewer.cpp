#include "VegaViewer.h"

VegaViewer::VegaViewer(QWidget * parent, const char * name, WindowFlags f):
    Viewer(parent, name, f, false)
{
    setBackgroundColor(0.6,0.6,0.6);
}


void VegaViewer::showEvent(QShowEvent *event)
{
    PoLAR::Viewer::showEvent(event);
    emit shown();
}
