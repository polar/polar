#ifndef BASICINTERFACEDSA3D_H
#define BASICINTERFACEDSA3D_H

#include <QtCore/QStringList>
#include <QMessageBox> 
#include <QMainWindow>
#include <QToolBar>
#include <QToolButton>
#include <QSlider>
#include <QScrollBar>
#include <QInputDialog>
#include <QSpinBox>
#include <QLabel>
#include <QtOpenGL/QGLWidget>

using Qt::WindowFlags;

using QGL::StencilBuffer;
using QGL::AlphaChannel;
using QGL::AccumBuffer;

#include <PoLAR/Viewer.h>
#include "DialogCurves.h"
using PoLAR::Marker2D;

class Interface : public QMainWindow  
{
  Q_OBJECT
  
public:
  Interface();
  ~Interface();

  void addSequence(float* sequence, int w, int h, int d, int nima)
  {
    viewer->setBgImage<float>(sequence, w, h, d, nima);
    _origWidth = w, _origHeight = h;
    scrollbar->setMaximum(nima-1);
    viewer->bgImageOn();
    viewer->startEditImageSlot();
  }
  
  void loadMarkers(const char *s)
  {
    viewer->setLoadFilename(s);
    viewer->loadMarkersSlot();
    viewer->setSaveFilename(s);
    std::list<PoLAR::DrawableObject2D*> l;
    std::list<PoLAR::DrawableObject2D*>::iterator lit;
    for (int i=0; i<viewer->getNbImages(); i++)
      {
	l=viewer->getDrawableObject2DList(i)->getObjects();
	for (lit=l.begin(); lit != l.end(); ++lit)
	  {
	    (*lit)->setSelectColor(dialogCurves.selectColor());
	    (*lit)->setSelectMarkerColor(dialogCurves.selectMarkerColor());
	    (*lit)->setUnselectShape(dialogCurves.unselectShape());
	    (*lit)->setSelectShape(dialogCurves.selectShape());
	    (*lit)->setSelectMarkerShape(dialogCurves.selectMarkerShape());
	    (*lit)->setMarkerSize(dialogCurves.markerSize());
	    (*lit)->setPointingPrecision(dialogCurves.pointingPrecision());
	    (*lit)->setLineWidth(dialogCurves.lineWidth());
	  }
      }
  }

  void setProperties(QColor usCol=PoLAR::DrawableObject2D::defaultUnselCol,
		     QColor sCol=PoLAR::DrawableObject2D::defaultSelCol,
		     QColor smCol=PoLAR::DrawableObject2D::defaultSelmarkCol,
		     PoLAR::DrawableObject2D::MarkerShape usShape=PoLAR::DrawableObject2D::defaultUnselShape,
		     PoLAR::DrawableObject2D::MarkerShape sShape=PoLAR::DrawableObject2D::defaultSelShape,
		     PoLAR::DrawableObject2D::MarkerShape smShape=PoLAR::DrawableObject2D::defaultSelmarkShape,
		     int S=PoLAR::DrawableObject2D::defaultSize,  
		     int P = PoLAR::DrawableObject2D::defaultPrecision,
		     int LW = PoLAR::DrawableObject2D::defaultLineWidth)
  {
    dialogCurves.setProperties(usCol,sCol,smCol,usShape,sShape,smShape,S,P,LW);
  }

  inline PoLAR::Viewer* getViewer() {return viewer;}

signals:
  void startEditImageSignal();
  void startEditMarkersSignal();

public slots:
  void exit();
  void startEditImage() {emit startEditImageSignal();}
  void startEditMarkers() {emit startEditMarkersSignal();}
  void activateLabelBox(Marker2D *m);
  void deactivateLabelBox();
  void changeTitle(int n);
  virtual void keyPressEvent(QKeyEvent *event);
  void updateInfosLabel(QMouseEvent *e);
  void setMarkerSize(int size);
  void setLineWidth(int linewidth);
  void setPointingPrecision(int pointingprecision);
  void setSelectShape(PoLAR::DrawableObject2D::MarkerShape shape);
  void setSelectMarkerShape(PoLAR::DrawableObject2D::MarkerShape shape);
  void setUnselectShape(PoLAR::DrawableObject2D::MarkerShape shape);


private:
  // la taille de l'image originale
  int _origWidth, _origHeight;

  QToolBar *toolbar, *scrollbarTB;
  QPixmap *quitPixmap, *imagePixmap, *markerPixmap, *savePixmap, *propertiesPixmap;
  QToolButton *quitButton, *imageButton, *markerButton, *saveButton, *propertiesButton;
  QSpinBox *labelBox;
  QScrollBar *scrollbar;
  PoLAR::Viewer *viewer;
  QLabel *statuslabel, *infoslabel;
  QMessageBox *mb;
  DialogCurves dialogCurves;
};

#endif // BASICINTERFACE_H
