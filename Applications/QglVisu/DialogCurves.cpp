using namespace std;
#include "DialogCurves.h"
#include <QFrame>
#include <QGroupBox>
#include <QPushButton>
#include <QSlider>
#include <QButtonGroup>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <iostream>

using namespace PoLAR;

DialogCurves::DialogCurves(int size, 
			   int lineWidth, 
			   int pointingPrecision, 
			   DrawableObject2D::MarkerShape selectMarkerShape,
			   DrawableObject2D::MarkerShape selectShape, 
			   DrawableObject2D::MarkerShape unselectShape)
{
  QVBoxLayout *vbl = new QVBoxLayout(this);

  QFrame *frame = new QFrame( this );
  frame->setLayout(new QVBoxLayout(frame));

  QPushButton *okb = new QPushButton( "OK", this );
  QObject::connect(okb, SIGNAL(clicked()), this, SLOT(hide()));

  QGroupBox *boxsz = new QGroupBox("Taille des marqueurs");
  boxsz->setAlignment(Qt::Vertical );
  boxsz->setLayout(new QVBoxLayout(boxsz));
  boxsz->layout()->setMargin( 11 );

  szSlider = new QSlider(Qt::Horizontal, boxsz);
  szSlider->setMinimum(1);
  szSlider->setMaximum(10);
  szSlider->setPageStep(1);
  szSlider->setTickPosition(QSlider::TicksBelow);
  szSlider->setTracking(true);
  szSlider->setValue(size);
  boxsz->layout()->addWidget(szSlider);
  QObject::connect(szSlider, SIGNAL(valueChanged(int)), this, SIGNAL(markerSizeSignal(int)));

  QGroupBox *boxlw = new QGroupBox("Epaisseur du trait");
  boxlw->setAlignment(Qt::Vertical );
  boxlw->setLayout(new QVBoxLayout(boxlw));
  boxlw->layout()->setMargin( 11 );

  lwSlider = new QSlider(Qt::Horizontal, boxlw);
  lwSlider->setMinimum(1);
  lwSlider->setMaximum(10);
  lwSlider->setPageStep(1);
  lwSlider->setTickPosition(QSlider::TicksBelow);
  lwSlider->setTracking(true);
  lwSlider->setValue(lineWidth);
  boxlw->layout()->addWidget(lwSlider);
  QObject::connect(lwSlider, SIGNAL(valueChanged(int)), this, SIGNAL(lineWidthSignal(int)));

  QGroupBox *boxpp = new QGroupBox("Précision de pointage");
  boxpp->setAlignment(Qt::Vertical );
  boxpp->setLayout(new QVBoxLayout(boxpp));
  boxpp->layout()->setMargin( 11 );
  boxpp->setTitle( QString("Précision de pointage") );

  ppSlider = new QSlider(Qt::Horizontal, boxlw);
  ppSlider->setMinimum(1);
  ppSlider->setMaximum(10);
  ppSlider->setPageStep(1);
  ppSlider->setTickPosition(QSlider::TicksBelow);
  ppSlider->setTracking(true);
  ppSlider->setValue(pointingPrecision);
  boxpp->layout()->addWidget(ppSlider);
  QObject::connect(ppSlider, SIGNAL(valueChanged(int)), this, SIGNAL(pointingPrecisionSignal(int)));

  QGroupBox *box = new QGroupBox("Forme des marqueurs");
  box->setAlignment(Qt::Horizontal );
  box->setLayout(new QHBoxLayout(box));
  box->layout()->setSpacing( 6 );
  box->layout()->setMargin( 11 );

  QGroupBox *selMgb=new QGroupBox("Marqueur sélectionné");
  selMgb->setAlignment(Qt::Vertical );
  selMgb->setLayout(new QVBoxLayout(selMgb));
  selMgb->setFlat(true);

  selMShapebtg=new QButtonGroup(selMgb);
  for (int i=0; i<DrawableObject2D::NbMarkerShape; i++)
    {
      QRadioButton *rbt=new QRadioButton(DrawableObject2D::MarkerShapeToString(DrawableObject2D::MarkerShape(i)),selMgb);
      selMShapebtg->addButton(rbt,i);
      selMShapebtg->button(i)->setChecked(false);
      selMgb->layout()->addWidget(rbt);
    }
  selMShapebtg->button(int(selectMarkerShape))->setChecked(true);
  QObject::connect(selMShapebtg, SIGNAL(buttonClicked(int)), this, SLOT(selectMarkerShapeSlot(int)));


  QGroupBox *selgb=new QGroupBox("Objet sélectionné");
  selgb->setAlignment(Qt::Vertical);
  selgb->setLayout(new QVBoxLayout(selgb));
  selgb->setFlat(true);

  selShapebtg=new QButtonGroup();
  for (int i=0; i<DrawableObject2D::NbMarkerShape; i++)
    {
      QRadioButton *rbt=new QRadioButton(DrawableObject2D::MarkerShapeToString(DrawableObject2D::MarkerShape(i)),selgb);
      selShapebtg->addButton(rbt,i);
      selShapebtg->button(i)->setChecked(false);
      selgb->layout()->addWidget(rbt);
    }    
  selShapebtg->button(int(selectShape))->setChecked(true);
  QObject::connect(selShapebtg, SIGNAL(buttonClicked(int)), this, SLOT(selectShapeSlot(int)));
  
  QGroupBox *unselgb=new QGroupBox("Objets non-sélectionnés");
  unselgb->setAlignment(Qt::Vertical );
  unselgb->setLayout(new QVBoxLayout(unselgb));
  unselgb->setFlat(true);

  unselShapebtg=new QButtonGroup();
  for (int i=0; i<DrawableObject2D::NbMarkerShape; i++)
    {
      QRadioButton *rbt=new QRadioButton(DrawableObject2D::MarkerShapeToString(DrawableObject2D::MarkerShape(i)),unselgb);
      unselShapebtg->addButton(rbt,i);
      unselShapebtg->button(i)->setChecked(false);
      unselgb->layout()->addWidget(rbt);
    }    
  unselShapebtg->button(int(unselectShape))->setChecked(true);
  QObject::connect(unselShapebtg, SIGNAL(buttonClicked(int)), this, SLOT(unselectShapeSlot(int)));

  // laying things out
  box->layout()->addWidget(selMgb);
  box->layout()->addWidget(selgb);
  box->layout()->addWidget(unselgb);
  
  frame->layout()->addWidget(boxsz);
  frame->layout()->addWidget(boxlw);
  frame->layout()->addWidget(boxpp);
  frame->layout()->addWidget(box);

  vbl->addWidget( frame );
  vbl->addWidget( okb );

  setLayout(vbl);
}

DialogCurves::~DialogCurves()
{
}
