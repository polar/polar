// main.cpp: implementation of the main class.
//
//////////////////////////////////////////////////////////////////////
using namespace std;
#include <QApplication>
#include "basicInterface.h"
#include "ImageSequence.h"

using namespace PoLAR;

int main(int argc,char ** argv)
{
  QApplication * app;
  Interface * inter;
  QColor unselCol = QColor("red"), selCol = QColor("yellow"), selmarkCol = QColor("red");

  if (argc < 4 || argc > 8)
    {
      std::cerr << "Syntaxe: qglvisu <data> <dimx> <dimy> [<dimz>] [<offset>] "
	"[<DataType>] [<markers>]\n"
	"Where: \n"
	"<offset> is defaults to 0\n"
	"<DataType> tells the data pixel type and is one in\n"
	"\t - f for float (DEFAULT)\n"
	"\t - d for double\n"
	"\t - ul for unsigned long\n"
	"\t - l for long\n"
	"\t - ui for int\n"
	"\t - i for int\n"
	"\t - us for unsigned short\n"
	"\t - s for short\n"
	"\t - uc for unsigned char\n"
	"\t - c for char\n"
	"\n"
	"<markers> is the name format for the object files. This format will be implemented\n"
	"\t For example if the format is toto.%06d.p2d, the files will formed as \n"
	"\t toto.<n>.p2d, with <n> the image rank coded on 6 digits (e.g. \n"
	"\t toto.000045.p2d if <markers>=toto and image rank is 45)\n";
	      
      exit(0);
    }

  /* mandatory params */
  char *fname = argv[1];
  int width=atoi(argv[2]);
  int height=atoi(argv[3]);
  int nima=1;
  if (argc >= 5)
    nima=atoi(argv[4]);
  int offset=0;
  if (argc >= 6)
    offset=atoi(argv[5]);
  string t("uc");
  if (argc >= 7)
    t=string(argv[6]);
  float* sequence = readData(fname,width,height,nima,offset,t.c_str());
  if (!sequence) std::cerr << "Could not read image data" << std::endl;

  app = new QApplication(argc,argv);
  inter = new Interface();

  inter->show();
  inter->addSequence(sequence, width, height, 1, nima);
  inter->setProperties(unselCol, selCol, selmarkCol, DrawableObject2D::CROSS, DrawableObject2D::CROSS, DrawableObject2D::CROSS, 3);
  if (argc >= 8)
    inter->loadMarkers(argv[7]);

  return(app->exec());
}
