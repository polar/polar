#include "basicInterface.h"

#include <QColorDialog>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <PoLAR/Image.h>

using namespace PoLAR;
using PoLAR::Marker2D;

Interface::Interface()
{
  QString dir="/home/kerrien/git/polar/Examples/data/icons/";
  viewer = new PoLAR::Viewer(this, "viewer", 0, 0/*, true*/);
  viewer->addEventHandler(new osgViewer::StatsHandler);// add stats handler
  setCentralWidget(viewer);

  toolbar = new QToolBar(this);
  addToolBar(Qt::LeftToolBarArea, toolbar);
  toolbar->setOrientation(Qt::Vertical);

  statuslabel = new QLabel(toolbar);
  statuslabel->setText(*(new QString("Tools")));
  toolbar->addWidget(statuslabel);

  infoslabel = new QLabel(toolbar);
  infoslabel->setText(*(new QString("       NaN,       NaN\n       NaN")));
  infoslabel->setFixedWidth(150);
  toolbar->addWidget(infoslabel);

  viewer->setMouseTracking(true);
  QObject::connect(viewer, SIGNAL(mouseMoveSignal(QMouseEvent*)), this, SLOT(updateInfosLabel(QMouseEvent*)));

  toolbar->addAction(QIcon(dir+"exit.png"), QString("Quitter"), this, SLOT(exit()));

  QAction *editImgAct = new QAction(QIcon(dir+"lum.png"), tr("edit image"), this);
  editImgAct->setStatusTip(tr("Edit Image"));
  QObject::connect(editImgAct, SIGNAL(triggered()), viewer, SLOT(startEditImageSlot()));
  QObject::connect(editImgAct, SIGNAL(triggered()), this, SLOT(deactivateLabelBox()));
  toolbar->addAction(editImgAct);

  toolbar->addAction(QIcon(dir+"edit.png"), QString("Edit Markers"), viewer, SLOT(startEditMarkersSlot()));



  toolbar->addAction(QIcon(dir+"props.png"), QString("2D object properties"), &dialogCurves, SLOT(show()));

  toolbar->addAction(QIcon(dir+"props.png"), QString("Save objects"), viewer, SLOT(saveMarkersSlot()));

  labelBox = new QSpinBox(/*0,_blendValueMax, 1, */this/*, "blendSpinBox"*/);
  labelBox->setRange(-1,999);
  labelBox->setSingleStep(1);
  toolbar->addWidget(labelBox);

  deactivateLabelBox();
  QObject::connect(labelBox, SIGNAL(valueChanged(int)), viewer, SLOT(changeMarkerLabelSlot(int)));
  QObject::connect(viewer, SIGNAL(selectMarkerSignal(Marker2D*)), this, SLOT(activateLabelBox(Marker2D*)));
  QObject::connect(viewer, SIGNAL(unselectMarkerSignal()), this, SLOT(deactivateLabelBox()));

  scrollbarTB = new QToolBar(this);
  scrollbarTB->setOrientation(Qt::Vertical);

  scrollbar = new QScrollBar(this);
  scrollbar->setMinimum(0);
  scrollbar->setMaximum(0);
  scrollbar->setPageStep(10);
  scrollbar->setValue(0);
  scrollbar->setFixedHeight(height());
  scrollbar->setOrientation(Qt::Vertical);
  scrollbarTB->addWidget(scrollbar);
  scrollbarTB->adjustSize();

  QObject::connect(scrollbar, SIGNAL(valueChanged(int)), viewer, SLOT(gotoImageSlot(int)));
  QObject::connect(scrollbar, SIGNAL(valueChanged(int)), this, SLOT(changeTitle(int)));

  addToolBar(Qt::RightToolBarArea, scrollbarTB);

  move(0,0);

  QObject::connect(this, SIGNAL(startEditImageSignal()), viewer, SLOT(startEditImageSlot()));
  QObject::connect(this, SIGNAL(startEditMarkersSignal()), viewer, SLOT(startEditMarkersSlot()));

  // add focus to this interface in order to get the keyboard events
  this->setFocusPolicy(Qt::StrongFocus);
  // remove the focus from the Viewer, otherwise it will bypass the interface's events
  viewer->setFocusPolicy(Qt::NoFocus);
  this->setFocus();
  
  mb= new QMessageBox(
		      QMessageBox::Information,
		      QString("Help: shortkeys"),
		      QString("Shortkeys:\n")+
		      QString("H: this help\n")+
		      QString("Q: quit the application\n")+
		      QString("I: edit image mode (pan/zoom)\n")+
		      QString("E: add/edit markers mode (blobs)\n")+
		      QString("D: toggle background image display\n")+
		      QString("G: grab a window snapshot\n")+
		      QString("Up: goto previous image\n")+
		      QString("Down: goto next image\n")+
		      QString("Right: goto next image\n")+
		      QString("Left: goto previous image\n")+
		      QString("M: add Markers\n")+
		      QString("P: add a Polygon\n")+
		      QString("L: add a polyLine\n")+
		      QString("S: add a open Spline\n")+
		      QString("B: add a Blob (closed spline)\n")+
		      QString("else default openscenegraph keys\n"),
		      QMessageBox::NoButton,
		      this);
  mb->setWindowModality(Qt::NonModal);
  
  // on connecte les widgets de dialogCurves
  QObject::connect(&dialogCurves, SIGNAL(markerSizeSignal(int)), this, SLOT(setMarkerSize(int)));
  QObject::connect(&dialogCurves, SIGNAL(lineWidthSignal(int)), this, SLOT(setLineWidth(int)));
  QObject::connect(&dialogCurves, SIGNAL(pointingPrecisionSignal(int)), this, SLOT(setPointingPrecision(int)));
  QObject::connect(&dialogCurves, SIGNAL(selectShapeSignal(PoLAR::DrawableObject2D::MarkerShape)), this, SLOT(setSelectShape(PoLAR::DrawableObject2D::MarkerShape)));
  QObject::connect(&dialogCurves, SIGNAL(selectMarkerShapeSignal(PoLAR::DrawableObject2D::MarkerShape)), this, SLOT(setSelectMarkerShape(PoLAR::DrawableObject2D::MarkerShape)));
  QObject::connect(&dialogCurves, SIGNAL(unselectShapeSignal(PoLAR::DrawableObject2D::MarkerShape)), this, SLOT(setUnselectShape(PoLAR::DrawableObject2D::MarkerShape)));
}

Interface::~Interface()
{
}

void Interface::exit()
{
  ::exit(0);
}

void Interface::activateLabelBox(Marker2D *m)
{
  labelBox->setEnabled(true);
  if (m) labelBox->setValue(int(m->label()));
}

void Interface::deactivateLabelBox()
{
  labelBox->setValue(-1);
  labelBox->setEnabled(false);
}

void Interface::updateInfosLabel(QMouseEvent *e)
{
  double x,y;
  QString buf;
  PoLAR::Image<float> *ima=dynamic_cast< PoLAR::Image<float>* >(viewer->getBgImage());
  if (ima->valid())
     {
         viewer->QtToImage(e->x(), e->y(),x,y);
	 std::vector<float> val= ima->getPixelData(x,y);
	 buf=QString("%1, %2\n").arg(x,13).arg(y,13);
	 for (unsigned int i=0; i<val.size(); buf.append(QString("%1 ").arg(val[i++],13)));
    }
  else 
    buf=QString("       Nan,       Nan\n       Nan");
  infoslabel->setText(buf);
}

void Interface::changeTitle(int n)
{
  statuslabel->setText(QString("Image %1").arg(n,3));
}

// Add key bindings
void Interface::keyPressEvent(QKeyEvent *event)
{
  switch(event->key())
    {
    case Qt::Key_H:
      mb->show();
      break;
    case Qt::Key_Q: {exit(); break;}
    case Qt::Key_Up: 
      {
	int i=scrollbar->value()-1;
	if (i < scrollbar->minimum()) i=scrollbar->maximum();
	scrollbar->setValue(i);
	break;
      }
    case Qt::Key_Down: 
      {
	int i=scrollbar->value()+1;
	if (i > scrollbar->maximum()) i=scrollbar->minimum();
	scrollbar->setValue(i);
	break;
      }
    case Qt::Key_Right: 
      {
	int i=scrollbar->value()+1;
	if (i > scrollbar->maximum()) i=scrollbar->minimum();
	scrollbar->setValue(i);
	break;
      }
    case Qt::Key_Left: 
      {
	int i=scrollbar->value()-1;
	if (i < scrollbar->minimum()) i=scrollbar->maximum();
	scrollbar->setValue(i);
	break;
      }
    case Qt::Key_D: 
      {
	viewer->bgImageToggle();
	break;
      }
    case Qt::Key_I:
      {
	viewer->startEditImageSlot();
	break;
      }
    case Qt::Key_E:
      {
	viewer->startEditMarkersSlot();
	break;
      }
    case Qt::Key_M:
      {
	viewer->addMarkers();
	viewer->startEditMarkersSlot();
	break;
      }
    case Qt::Key_P:
      {
	viewer->addPolygon();
	viewer->startEditMarkersSlot();
	break;
      }
    case Qt::Key_L:
      {
	viewer->addPolyLine();
	viewer->startEditMarkersSlot();
	break;
      }
    case Qt::Key_S:
      {
	viewer->addSpline();
	viewer->startEditMarkersSlot();
	break;
      }
    case Qt::Key_B:
      {
	viewer->addBlob();
	viewer->startEditMarkersSlot();
	break;
      }
    case Qt::Key_G:
      {
        viewer->setIdle(true);
	QString fname=QFileDialog::getSaveFileName( this, 
						    "Choose a filename to save the snapshot image",
						    QString::null,
						    "PNG images (*.png)");
        viewer->setIdle(false);
	if (!fname.isEmpty()) viewer->grab().save(fname, "PNG");
	break;
      }
    default: viewer->notify(event);
    }
}

void Interface::setMarkerSize(int size)
{
  if (viewer)
    {
      std::list<PoLAR::DrawableObject2D*> objL=viewer->getDrawableObject2DList()->getObjects();
      std::list<PoLAR::DrawableObject2D*>::iterator itr;
      for (itr=objL.begin(); itr != objL.end(); ++itr)
	(*itr)->setMarkerSize(size);
    }
}

void Interface::setLineWidth(int linewidth)
{
  if (viewer)
    {
      std::list<PoLAR::DrawableObject2D*> objL=viewer->getDrawableObject2DList()->getObjects();
      std::list<PoLAR::DrawableObject2D*>::iterator itr;
      for (itr=objL.begin(); itr != objL.end(); ++itr)
	(*itr)->setLineWidth(linewidth);
    }
}

void Interface::setPointingPrecision(int pointingprecision)
{
  if (viewer)
    {
      std::list<PoLAR::DrawableObject2D*> objL=viewer->getDrawableObject2DList()->getObjects();
      std::list<PoLAR::DrawableObject2D*>::iterator itr;
      for (itr=objL.begin(); itr != objL.end(); ++itr)
	(*itr)->setPointingPrecision(pointingprecision);
    }
}

void Interface::setSelectShape(PoLAR::DrawableObject2D::MarkerShape shape)
{
  if (viewer)
    {
      std::list<PoLAR::DrawableObject2D*> objL=viewer->getDrawableObject2DList()->getObjects();
      std::list<PoLAR::DrawableObject2D*>::iterator itr;
      for (itr=objL.begin(); itr != objL.end(); ++itr)
	(*itr)->setSelectShape(shape);
    }
}

void Interface::setSelectMarkerShape(PoLAR::DrawableObject2D::MarkerShape shape)
{
  if (viewer)
    {
      std::list<PoLAR::DrawableObject2D*> objL=viewer->getDrawableObject2DList()->getObjects();
      std::list<PoLAR::DrawableObject2D*>::iterator itr;
      for (itr=objL.begin(); itr != objL.end(); ++itr)
	(*itr)->setSelectMarkerShape(shape);
    }
}

void Interface::setUnselectShape(PoLAR::DrawableObject2D::MarkerShape shape)
{
  if (viewer)
    {
      std::list<PoLAR::DrawableObject2D*> objL=viewer->getDrawableObject2DList()->getObjects();
      std::list<PoLAR::DrawableObject2D*>::iterator itr;
      for (itr=objL.begin(); itr != objL.end(); ++itr)
	(*itr)->setUnselectShape(shape);
    }
}

