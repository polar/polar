#ifndef _DIALOGCURVES_H
#define _DIALOGCURVES_H

#include <QDialog>
#include <QSlider>
#include <QButtonGroup>
#include <QRadioButton>
#include <PoLAR/DrawableObject2D.h>

class DialogCurves : public QDialog
{
  Q_OBJECT
    
public:
  DialogCurves(int size=PoLAR::DrawableObject2D::defaultSize,  
	       int lineWidth=PoLAR::DrawableObject2D::defaultLineWidth, 
	       int pointingPrecision=PoLAR::DrawableObject2D::defaultPrecision, 
	       PoLAR::DrawableObject2D::MarkerShape selectMarkerShape=PoLAR::DrawableObject2D::defaultSelmarkShape,
	       PoLAR::DrawableObject2D::MarkerShape selectShape=PoLAR::DrawableObject2D::defaultSelShape, 
	       PoLAR::DrawableObject2D::MarkerShape unselectShape=PoLAR::DrawableObject2D::defaultUnselShape);
  ~DialogCurves();
  
  QColor unselectColor() const {return UnselCol;}
  QColor selectColor() const {return SelCol;}
  QColor selectMarkerColor() const {return SelMarkCol;}
  PoLAR::DrawableObject2D::MarkerShape selectShape() const 
    {
      return PoLAR::DrawableObject2D::StringToMarkerShape(selShapebtg->checkedButton()->text());
    }
  PoLAR::DrawableObject2D::MarkerShape selectMarkerShape() const 
    {
      return PoLAR::DrawableObject2D::StringToMarkerShape(selMShapebtg->checkedButton()->text());
    }
  PoLAR::DrawableObject2D::MarkerShape unselectShape() const 
    {
      return PoLAR::DrawableObject2D::StringToMarkerShape(unselShapebtg->checkedButton()->text());
    }
  int markerSize() const {return szSlider->value();}
  int pointingPrecision() const {return ppSlider->value();}
  int lineWidth() const {return lwSlider->value();}

  void setUnselectColor(QColor usCol=PoLAR::DrawableObject2D::defaultUnselCol)
  { UnselCol=usCol; }
  void setSelectColor(QColor sCol=PoLAR::DrawableObject2D::defaultSelCol)
  { SelCol=sCol; }
  void setSelectMarkerColor(QColor smCol=PoLAR::DrawableObject2D::defaultSelmarkCol)
  { SelMarkCol=smCol; }
  void setProperties(QColor usCol=PoLAR::DrawableObject2D::defaultUnselCol,
		     QColor sCol=PoLAR::DrawableObject2D::defaultSelCol,
		     QColor smCol=PoLAR::DrawableObject2D::defaultSelmarkCol,
		     PoLAR::DrawableObject2D::MarkerShape usShape=PoLAR::DrawableObject2D::defaultUnselShape,
		     PoLAR::DrawableObject2D::MarkerShape sShape=PoLAR::DrawableObject2D::defaultSelShape,
		     PoLAR::DrawableObject2D::MarkerShape smShape=PoLAR::DrawableObject2D::defaultSelmarkShape,
		     int S=PoLAR::DrawableObject2D::defaultSize,  
		     int P = PoLAR::DrawableObject2D::defaultPrecision,
		     int LW = PoLAR::DrawableObject2D::defaultLineWidth)
    {
      setUnselectColor(usCol);
      setSelectColor(sCol);
      setSelectMarkerColor(smCol);
      setUnselectShape(usShape);
      setSelectShape(sShape);
      setSelectMarkerShape(smShape);
      setMarkerSize(S);
      setPointingPrecision(P);
      setLineWidth(LW);
    }

signals:
  void selectShapeSignal(PoLAR::DrawableObject2D::MarkerShape shape);
  void selectMarkerShapeSignal(PoLAR::DrawableObject2D::MarkerShape shape);
  void unselectShapeSignal(PoLAR::DrawableObject2D::MarkerShape shape);
  void markerSizeSignal(int size);
  void lineWidthSignal(int width);
  void pointingPrecisionSignal(int precision);

public slots:
  void setMarkerSize(int S=PoLAR::DrawableObject2D::defaultSize)
  {
    if (S>=szSlider->minimum() && S <= szSlider->maximum()) 
      szSlider->setValue(S);
  }

  void setPointingPrecision(int P=PoLAR::DrawableObject2D::defaultPrecision)
  {
    if (P>=ppSlider->minimum() && P <= ppSlider->maximum()) 
      ppSlider->setValue(P);
  }

  void setLineWidth(int LW = PoLAR::DrawableObject2D::defaultLineWidth)
  {
    if (LW >= lwSlider->minimum() && LW <= lwSlider->maximum()) 
      lwSlider->setValue(LW);
  }

  void setUnselectShape(PoLAR::DrawableObject2D::MarkerShape usShape=PoLAR::DrawableObject2D::defaultUnselShape)
  {
    if (int(usShape) >= 0 && int(usShape) < PoLAR::DrawableObject2D::NbMarkerShape)
      unselShapebtg->button(int(usShape))->setChecked(true);
  }    

  void setSelectShape(PoLAR::DrawableObject2D::MarkerShape sShape=PoLAR::DrawableObject2D::defaultSelShape)
  {
    if (int(sShape) >= 0 && int(sShape) < PoLAR::DrawableObject2D::NbMarkerShape)
      selShapebtg->button(int(sShape))->setChecked(true);
  }

  void setSelectMarkerShape(PoLAR::DrawableObject2D::MarkerShape smShape=PoLAR::DrawableObject2D::defaultSelmarkShape)
  {
    if (int(smShape) >= 0 && int(smShape) < PoLAR::DrawableObject2D::NbMarkerShape)
      selMShapebtg->button(int(smShape))->setChecked(true);
  }
  
private slots:
  void selectShapeSlot(int shape) {emit selectShapeSignal(PoLAR::DrawableObject2D::MarkerShape(shape));}
  void selectMarkerShapeSlot(int shape) {emit selectMarkerShapeSignal(PoLAR::DrawableObject2D::MarkerShape(shape));}
  void unselectShapeSlot(int shape) {emit unselectShapeSignal(PoLAR::DrawableObject2D::MarkerShape(shape));}
  
private:
  QColor UnselCol;
  QColor SelCol;
  QColor SelMarkCol;
  QSlider *szSlider, *lwSlider, *ppSlider;
  QButtonGroup *selShapebtg, *selMShapebtg, *unselShapebtg;
};

#endif // _DIALOGCURVES_H
