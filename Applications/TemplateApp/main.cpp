/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include <QFileInfo>
#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/io_utils>

#include "MyCustomViewer.h"
#include <PoLAR/Image.h>
#include <PoLAR/Util.h>

int main(int argc,char ** argv)
{
    //osg::setNotifyLevel(osg::DEBUG_FP);
    QApplication app(argc, argv);
    int width;
    int height;
    osg::ref_ptr<PoLAR::Image_uc> myImage;

    // Create the viewer
    MyCustomViewer viewer;

    // Read the image given in parameter
    if (argc >1)
    {
        QFileInfo imgFile(argv[1]);
        if(imgFile.exists() && imgFile.isFile())
        {
            myImage = new PoLAR::Image_uc(argv[1], true);
        }
        else
        {
            std::cerr << argv[1] << ": file not found" << std::endl;
            exit(0);
        }
        width = myImage->getWidth();
        height = myImage->getHeight();

        viewer.resize(width,height);

        // Add the image read as background image
        viewer.setBgImage(myImage);

        // Show it
        viewer.bgImageOn();

        // Have the image interaction activ
        viewer.startEditImageSlot();

        // Load the model given in parameter
        if (argc > 3)
        {
            osg::ref_ptr<osg::Node> loadedModel;
            loadedModel = osgDB::readNodeFile(argv[3]);
            osgUtil::SmoothingVisitor smooth;
            osgUtil::Optimizer optimizer;

            if(!loadedModel)
            {
                std::cerr << "Unable to load model" << std::endl;
                exit(0);
            }

            optimizer.optimize(loadedModel.get());
            loadedModel->accept(smooth);
            osg::Matrixd m = osg::Matrixd::translate(0.5,0.0,0.005); // initialisation of matrix m with a custom tranformation
            // Add the object3D created from loaded model to the viewer
            osg::ref_ptr<PoLAR::Object3D> obj = new PoLAR::Object3D(loadedModel.get(), true, true);
            obj->setTransformationMatrix(m);
            viewer.addObject3D(obj.get());
            viewer.setTrackNode(obj.get());
        }


        // Load the projection given in parameter
        if (argc > 2)
        {
            osg::Matrixd P = PoLAR::Util::readProjectionMatrix(argv[2]);
            viewer.setProjection(P);
        }

        // create a ground
        {
            viewer.addLightSource(5,2,5, true);
            osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
            osg::ref_ptr<osg::ShapeDrawable> shape;
            shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 5.0f, 5.0f, 0.00001f));
            floorGeode->addDrawable(shape.get());
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get());
            object3D->setName("Floor");
            object3D->setEditOn();
            object3D->setPhantomOn();
            viewer.addObject3D(object3D.get());
        }

        // Show the widget
        viewer.center();
        viewer.show();

    }
    else
    {
        std::cerr << "Syntax: " << argv[0] << " <image> <projection> <3D model>" <<std::endl;
        exit(0);
    }

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
