# Modify the two variables here to the correct paths to PoLAR install dir and OpenSceneGraph install dir #

PoLAR_DIR = $$PWD/../../install-android
OSG_DIR = "/home/pierrejean/Documents/Android/OpenSceneGraph-3.5.0/install"

#  Modify the two variables here to the used OpenSceneGraph version and the target ABI (used for osgPlugins) #

OSG_VERSION = 3.5.0
ANDROID_ABI = armeabi-v7a


# You should not have to change anything between this line and the next comment #

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets svg multimedia opengl xml

QMAKE_CXXFLAGS += -std=gnu++11 -fpie -fPIC

TARGET = runCalib
TEMPLATE = app


SOURCES += main.cpp \
        interface.cpp \
	ProjectionComputation.cpp
	
HEADERS  += interface.h \
	ProjectionComputation.h


CONFIG += mobility
MOBILITY = 


INCLUDEPATH +=$${PoLAR_DIR}/include
DEPENDPATH += $${PoLAR_DIR}/include


unix: PRE_TARGETDEPS += $${PoLAR_DIR}/lib/libPoLAR.a

unix: LIBS += -L$${PoLAR_DIR}/lib -lPoLAR

unix: PRE_TARGETDEPS += $${PoLAR_DIR}/lib/libosgdb_off.a
unix: LIBS += -L$${PoLAR_DIR}/lib -lPoLAR -losgdb_off

INCLUDEPATH += $${OSG_DIR}/include
DEPENDPATH += $${OSG_DIR}/include

# Add links to plugins for loading models and textures #
# First line: plugin list. Add/remove the linked plugins according to the ones you need
unix: LIBS += -L$${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION} -losgdb_pnm -losgdb_jpeg -losgdb_obj -losgdb_ive -losgdb_osg -losgdb_freetype
# Next line: with some extensions (e.g. osgt objects) and some particular encapsulated objects (e.g. animated objects) it is needed to add some serialization dependencies
unix: LIBS += -L$${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION} -losgdb_serializers_osg -losgdb_serializers_osganimation
# Next line: plugin dependencies. For example libjpeg for the jpeg plugin
unix: LIBS += -L$${OSG_DIR}/obj/local/$${ANDROID_ABI} -ljpeg -lft2

# Next lines: path to the plugin libraries. One line = one plugin
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_pnm.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_jpeg.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_obj.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_ive.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_osg.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_freetype.a
# Next lines: with some extensions (e.g. osgt objects) and some particular encapsulated objects (e.g. animated objects) it is needed to add some serialization dependencies
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_serializers_osg.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/osgPlugins-$${OSG_VERSION}/libosgdb_serializers_osganimation.a
# Next lines: path to the plugin dependencies libraries. One line = one dependency lib
unix: PRE_TARGETDEPS += $${OSG_DIR}/obj/local/$${ANDROID_ABI}/libjpeg.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/obj/local/$${ANDROID_ABI}/libft2.a

# You should not have to change anything under this line #

unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosg.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgViewer.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgGA.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgFX.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgDB.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgPresentation.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgParticle.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgText.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgUI.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgUtil.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgManipulator.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgShadow.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgAnimation.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgSim.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgTerrain.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgVolume.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libosgWidget.a
unix: PRE_TARGETDEPS += $${OSG_DIR}/lib/libOpenThreads.a
unix: LIBS += -L$${OSG_DIR}/lib -losgViewer -losgGA -losgFX -losgParticle -losgDB -losgPresentation -losgText -losgUI -losgManipulator -losgShadow -losgAnimation -losgSim -losgTerrain -losgVolume -losgWidget -losgUtil -losg -lOpenThreads


