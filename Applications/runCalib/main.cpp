/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <iostream>
#include "interface.h"
#include <PoLAR/Image.h>
#include <PoLAR/AnimatedObject3D.h>
#include <PoLAR/Util.h>
#include <PoLAR/Notify.h>
#include <PoLAR/FrameAxis.h>
#include <QDebug>


#if ANDROID
#include <osgDB/Registry>
// register the plugins necessary to load models and textures
USE_OSGPLUGIN(pnm)
USE_OSGPLUGIN(jpeg)
USE_OSGPLUGIN(off)
USE_OSGPLUGIN(obj)
USE_OSGPLUGIN(osg2)
USE_OSGPLUGIN(freetype)
// register the serialization libraries needed for loading some extensions (e.g. animated osgt models)
USE_SERIALIZER_WRAPPER_LIBRARY(osg)
USE_SERIALIZER_WRAPPER_LIBRARY(osgAnimation)

#endif



#define ROTATION_STEP 10
#define WALK_LENGTH 2

struct PathCallback : public PoLAR::Object3DCallback
{
    PathCallback():
        _pathArea(0), _x(0.0), _y(-0.01), _rotInc(0.0), _rotateEnded(false)
    {
        _timer1.setStartTick();
    }


    void update(PoLAR::Object3D *object)
    {
        double dt = _timer1.time_s();
        int seconds(floor(dt));
        int length = (_pathArea == 0 || _pathArea == 2) ? WALK_LENGTH*2 :WALK_LENGTH;
        if(seconds >= length)
        {
            if(_rotInc == -1.0)
            {
                _timer2.setStartTick();
                _rotInc = 0.0;
            }
            double ms  = _timer2.time_m();
            if(ms >= 20.0)
            {
                double rot = M_PI/(2*ROTATION_STEP);
                _rotInc += rot;
                object->rotate(rot, 0, 0, 1);
                if(PoLAR::Util::Null((_rotInc - M_PI/2)))
                {
                    _rotInc = -1.0;
                    _rotateEnded = true;
                }
            }

            if(_rotateEnded)
            {
                _pathArea++;
                if(_pathArea > 3) _pathArea = 0;
                switch(_pathArea)
                {
                case 0:
                    _x = 0.0;
                    _y = -0.01;
                    break;
                case 1:
                    _x = 0.01;
                    _y = 0.0;
                    break;
                case 2:
                    _x = 0.0;
                    _y = 0.01;
                    break;
                case 3:
                    _x = -0.01;
                    _y = 0.0;
                    break;
                default:
                    break;
                }
                _rotateEnded = false;
                _timer1.setStartTick();
            }
        }
        object->translate(osg::Vec3d(_x,_y,0.0));
    }

    osg::Timer _timer1;
    osg::Timer _timer2;
    int _pathArea;
    double _x, _y;
    double _rotInc;
    bool _rotateEnded;
};


int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    Interface inter;
    inter.resize(800, 600);
    // Show the widget
    inter.show();

    //osg::setNotifyLevel(osg::DEBUG_FP);
    osg::setNotifyHandler(new PoLAR::QDebugNotifyHandler);

    osg::ref_ptr<PoLAR::Image_uc> myImage;

#if ANDROID
    QString dataPath("/sdcard/polar/data/");
#endif

#if 0
    //QString img(dataPath  + "example1.ppm");
    QString img(dataPath  + "example2.jpg");
#else
    QString img;
    if(argc>1)
    {
        img = QString(argv[1]);
    }
    else
    {
#if ANDROID
        QString dir("/sdcard/DCIM/Camera/");
#else
        QString dir = QString::null;
#endif
        img = QFileDialog::getOpenFileName(&inter,
                                           "Choose an image",
                                           dir);
    }
    if(!img.size())
    {
        qDebug("Image is empty");
        exit(0);
    }

    else
    {
#endif
        if(PoLAR::Util::fileExists(img.toStdString()))
            myImage = new PoLAR::Image<unsigned char>(img.toStdString(), true);
        else
        {
            qDebug() << "Unable to load image" << img;
            exit(0);
        }

        // Add the image read as background image
        if(myImage.valid()) inter.getViewer()->setBgImage(myImage.get());

        // Show it
        inter.getViewer()->bgImageOn();
        //inter.getViewer()->setOptimalZoom();

        {
#if ANDROID
            //QString model(dataPath  + "armchair.obj");
            QString model(dataPath  + "anim_tris.osgt");
#else
            QString model;
            if(argc>2)
            {
                model = QString(argv[2]);
            }
            else
            {
                model = QFileDialog::getOpenFileName(&inter,
                                                     "Choose a model",
                                                     QString::null);
            }
#endif
            if(PoLAR::Util::fileExists(model.toStdString()))
            {
                //osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(model.toStdString(), true, true);
                osg::ref_ptr<PoLAR::AnimatedObject3D> object3D = new PoLAR::AnimatedObject3D(model.toStdString(), true, true);
                object3D->optimize();
                object3D->scale(0.2);
                object3D->translate(2.0, 2.5, -5.6);
                if(object3D->hasAnimations())
                    object3D->setUpdateCallback(new PathCallback);
                inter.addNode(object3D.get());
            }
            else
                qDebug() << "Unable to find model" << model;
        }


        {
            osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(new PoLAR::FrameAxis, true, true);
            inter.addFrame(object3D.get());
        }


        {
            // add ground
            osg::ref_ptr<PoLAR::Object3D> ground = PoLAR::Object3D::createGround(20.0f, 20.0f);
            ground->setName("Table");
            ground->translate(10.0, 10.0, 0.0);
            ground->setPhantomOn();
            inter.getViewer()->addObject3D(ground.get());
        }


        inter.getViewer()->setOptimalZoom();

        return app.exec();
#if 1
    }
#endif
}
