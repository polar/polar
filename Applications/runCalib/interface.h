#ifndef INTERFACE_H
#define INTERFACE_H

#include <QStringList>
#include <QMessageBox>
#include <QMainWindow>
#include <QToolBar>
#include <QToolButton>
#include <QSlider>
#include <QScrollBar>
#include <QInputDialog>
#include <QSpinBox>
#include <QLabel>
#include <QComboBox>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <osgGA/TrackballManipulator>

#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>
#include <PoLAR/AnimatedObject3D.h>
#include <PoLAR/LightDialog.h>

using PoLAR::Marker2D;

class Interface : public QMainWindow
{
    Q_OBJECT

public:
    Interface();

    ~Interface();

    void addImage(PoLAR::BaseImage* image, int nima)
    {
        viewer->setBgImage(image);
        scrollbar->setMaximum(nima-1);
        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }


    void addSequence(float* sequence, int w, int h, int d, int nima)
    {
        viewer->setBgImage(sequence, w, h, d, nima);
        scrollbar->setMaximum(nima-1);

        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }

    void addNode(PoLAR::AnimatedObject3D *object)
    {
        _object = object;
    }

    void addFrame(PoLAR::Object3D *object)
    {
        _frame = object;
        getViewer()->addObject3D(object);
    }

    void setProjection(osg::Matrixd& P, PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION) {viewer->setProjection (P, pt);}
    void setProjection() {viewer->setProjection();} // default projection

    inline PoLAR::Viewer* getViewer() {return viewer;}


signals:
    void startEditImageSignal();
    void startEditMarkersSignal();


protected:

    virtual void keyPressEvent(QKeyEvent *event);


private slots:


    void hideFrameAxis();

    void newMarkers();

    void markersInfo()
    {
        PoLAR::DrawableObject2D* obj = viewer->getCurrentObject2D();
        if(obj)
            qDebug("%d", obj->getObject()->nbMarkers());
    }

    void deleteMarker();

    void debugInfo();

    void computeMatrix();

private:
    QToolBar *toolbar;
    QToolButton *quitButton, *imageButton, *markerButton, *saveButton, *editSceneButton;
    QSpinBox *labelBox;
    QScrollBar *scrollbar;
    osg::ref_ptr<PoLAR::Viewer> viewer;
    osg::ref_ptr<PoLAR::AnimatedObject3D> _object;
    osg::ref_ptr<PoLAR::Object3D> _frame;
    PoLAR::LightDialog *lightDialog;
};

#endif // INTERFACE_H
