#include "ProjectionComputation.h"
#include <QDebug>
#include <PoLAR/Util.h>
#include <cmath>
#include <osg/Uniform>
#include <osg/io_utils>
#include <osg/Matrixd>
#include <iostream>

// matrix inversion
// the result is put in Y
void ProjectionComputation::matrixInversion(double **A, int order, double **Y)
{
    // get the determinant of a
    double det = 1.0/calcDeterminant(A,order);

    // memory allocation
    double *temp = new double[(order-1)*(order-1)];
    double **minor = new double*[order-1];
    for(int i=0;i<order-1;i++)
        minor[i] = temp+(i*(order-1));

    for(int j=0;j<order;j++)
    {
        for(int i=0;i<order;i++)
        {
            // get the co-factor (matrix) of A(j,i)
            getMinor(A,minor,j,i,order);
            Y[i][j] = det*calcDeterminant(minor,order-1);
            if( (i+j)%2 == 1)
                Y[i][j] = -Y[i][j];
        }
    }

    // release memory
    //delete [] minor[0];
    delete [] temp;
    delete [] minor;
}

// calculate the cofactor of element (row,col)
int ProjectionComputation::getMinor(double **src, double **dest, int row, int col, int order)
{
    // indicate which col and row is being copied to dest
    int colCount=0,rowCount=0;

    for(int i = 0; i < order; i++ )
    {
        if( i != row )
        {
            colCount = 0;
            for(int j = 0; j < order; j++ )
            {
                // when j is not the element
                if( j != col )
                {
                    dest[rowCount][colCount] = src[i][j];
                    colCount++;
                }
            }
            rowCount++;
        }
    }

    return 1;
}

// Calculate the determinant recursively
double ProjectionComputation::calcDeterminant(double **mat, int order)
{
    // order must be >= 0
    // stop the recursion when matrix is a single element
    if( order == 1 )
        return mat[0][0];

    // the determinant value
    double det = 0.0;

    // allocate the cofactor matrix
    double **minor;
    minor = new double*[order-1];
    for(int i=0;i<order-1;i++)
        minor[i] = new double[order-1];

    for(int i = 0; i < order; i++ )
    {
        // get minor of element (0,i)
        getMinor( mat, minor, 0, i , order);
        // the recusion is here!

        det += (i%2==1?-1.0:1.0) * mat[0][i] * calcDeterminant(minor,order-1);
        //det += pow( -1.0, i ) * mat[0][i] * calcDeterminant( minor,order-1 );
    }

    // release memory
    for(int i=0;i<order-1;i++)
        delete [] minor[i];
    delete [] minor;

    return det;
}


osg::Matrix ProjectionComputation::calculate(osg::Vec2 p1, osg::Vec2 p2, osg::Vec2 p3, osg::Vec2 p4, float w, float h, float s)
{
    osg::Vec3 q1p(p1[0]-w/2.0, p1[1]-h/2.0, 1.0);
    osg::Vec3 q2p(p2[0]-w/2.0, p2[1]-h/2.0, 1.0);
    osg::Vec3 q3p(p3[0]-w/2.0, p3[1]-h/2.0, 1.0);
    osg::Vec3 q4p(p4[0]-w/2.0, p4[1]-h/2.0, 1.0);

    osg::Vec3 l1, l2, l3, l4;

    // vanishing lines computation
    vanishingLines(q1p, q2p, q3p, q4p, l1, l2, l3, l4);

    // intersections V and W
    osg::Vec3 vh = l1 ^ l2;
    osg::Vec3 wh = l3 ^ l4;
    osg::Vec3 V(vh[0]/vh[2], vh[1]/vh[2], 1.0);
    osg::Vec3 W(wh[0]/wh[2], wh[1]/wh[2], 1.0);

    // focal distance f
    if((V[0]*W[0]+V[1]*W[1])>0)
    {
        qDebug() << "Physical impossibility: redo marker positioning";
    }

    double f = sqrt(-(V[0]*W[0]+V[1]*W[1]));
    // intrinsics
    osg::Matrix3d K(f, 0.0, w/2.0,
                    0.0, f, h/2.0,
                    0.0, 0.0, 1.0);

    double qip_ptr[8] = {  q1p[0],
                           q1p[1],
                           q2p[0],
                           q2p[1],
                           q3p[0],
                           q3p[1],
                           q4p[0],
                           q4p[1]};
    osg::MatrixTemplate<double, 8, 1> qip;
    qip.set(qip_ptr);


    double A_ptr[8][8] =
    {
        {0,0,1,0,0,0,0,0},
        {0,0,0,0,0,1,0,0},
        {1,0,1,0,0,0,-q2p[0],0},
        {0,0,0,1,0,1,-q2p[1],0},
        {1,1,1,0,0,0,-q3p[0],-q3p[0]},
        {0,0,0,1,1,1,-q3p[1],-q3p[1]},
        {0,1,1,0,0,0,0,-q4p[0]},
        {0,0,0,0,1,1,0,-q4p[1]}
    };


    double* Ap[8];
    for (int i = 0; i < 8; ++i)
        Ap[i] = A_ptr[i];
    double** App = Ap;

    double A1[8][8];
    double* A1p[8];
    for (int i = 0; i < 8; ++i)
        A1p[i] = A1[i];
    double** A1pp = A1p;

    matrixInversion(App, 8, A1pp);

    // homography
    float AdotQ[8];
    for(int i=0; i<8; i++)
    {
        float val = 0;
        for(int j=0; j<8; j++)
        {
            val += A1[i][j] * qip[j];
        }
        AdotQ[i] = val;
    }

    osg::Matrix3d H(AdotQ[0], AdotQ[1], AdotQ[2],
            AdotQ[3], AdotQ[4], AdotQ[5],
            AdotQ[6], AdotQ[7], 1.0
            );


    // Projection matrix
    osg::Matrix3d temp(1.0/f, 0.0, 0.0,
                       0.0, 1.0/f, 0.0,
                       0.0, 0.0, 1.0);

    osg::Matrix3d B = PoLAR::Util::multiply33(temp, H);


    osg::Vec3 B1(B(0,0), B(1,0), B(2,0));
    osg::Vec3 B2(B(0,1), B(1,1), B(2,1));
    osg::Vec3 B3(B(0,2), B(1,2), B(2,2));

    // some intermediate values
    osg::Vec3 R1 = (B1/B1.length());
    osg::Vec3 R3_inter = R1 ^ B2;
    osg::Vec3 R3 = (R3_inter / R3_inter.length());
    osg::Vec3 R2 = R3 ^ R1;

    double l = B1.length() / s;
    // double t_calc = B2.length() / l;

    osg::Vec3 T(B3/l);

    osg::Matrix3x4 E(R1[0], R2[0], R3[0], T[0],
            R1[1], R2[1], R3[1], T[1],
            R1[2], R2[2], R3[2], T[2]);

    //Projection = intrinsics * extrinsics
    osg::Matrix3x4 M;
    const unsigned int dim = 3;
    for(unsigned int i=0; i<dim; i++)
        for(unsigned int j=0; j<dim+1; j++)
            for(unsigned int k=0; k<dim; k++)
            {
                M(i, j) += K(i, k) * E(k, j);
            }

    // conversion to Matrix 4x4
    osg::Matrixd proj;
    for(unsigned int i=0; i<dim; i++)
    {
        for(unsigned int j=0; j<dim+1; j++)
        {
            proj(i, j) = M(i,j);
        }
    }

    // we transpose the final projection. The computation has been made with transposed matrices
    osg::Matrixd p = PoLAR::Util::transpose(proj);
    return p;
}


void ProjectionComputation::vanishingLines(osg::Vec3 q1p, osg::Vec3 q2p, osg::Vec3 q3p, osg::Vec3 q4p, osg::Vec3& l1, osg::Vec3& l2, osg::Vec3& l3, osg::Vec3& l4)
{
    l1 = q1p ^ q2p;
    l2 = q4p ^ q3p;
    l3 = q2p ^ q3p;
    l4 = q1p ^ q4p;
}
