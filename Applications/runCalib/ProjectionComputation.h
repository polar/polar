#ifndef PROJECTIONCOMPUTATION2_H
#define PROJECTIONCOMPUTATION2_H

#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Matrix>

namespace ProjectionComputation
{

osg::Matrix calculate(osg::Vec2 p1, osg::Vec2 p2, osg::Vec2 p3, osg::Vec2 p4, float w, float h, float s);

void vanishingLines(osg::Vec3 q1p, osg::Vec3 q2p, osg::Vec3 q3p, osg::Vec3 q4p, osg::Vec3& l1, osg::Vec3& l2, osg::Vec3& l3, osg::Vec3& l4);

 void matrixInversion(double **A, int order, double **Y);
 int getMinor(double **src, double **dest, int row, int col, int order);
 double calcDeterminant( double **mat, int order);

}

#endif // PROJECTIONCOMPUTATION2_H
