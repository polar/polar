/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QColorDialog>
#include <QTouchEvent>
#include "interface.h"
#include "ProjectionComputation.h"
#include "../../Examples/props.h"
using namespace PoLAR;

Interface::Interface(): QMainWindow()
{
    setAttribute(Qt::WA_AcceptTouchEvents);

#if ANDROID
    QString dataPath("/sdcard/polar/data/");
    QString dir = dataPath + "icons/";
#else
    QString dir = dataPath() + "icons/";
#endif
    // Create a Viewer with shadows management
    viewer = new PoLAR::Viewer(this, "viewer", 0, true);
    //viewer->setShadowsOff();

    setCentralWidget(viewer);
    toolbar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea, toolbar);
    toolbar->setOrientation(Qt::Vertical);
    toolbar->setAttribute(Qt::WA_AcceptTouchEvents);
#if ANDROID
    toolbar->setIconSize(QSize(128,128));
#endif
    qApp->setAttribute(Qt::AA_SynthesizeMouseForUnhandledTouchEvents, true);

    QAction *editImgAct = new QAction(QIcon(dir+"img.png"), tr("Edit image"), this);
    editImgAct->setStatusTip(tr("Edit Image"));
    QObject::connect(editImgAct, SIGNAL(triggered()), viewer, SLOT(startEditImageSlot()));
    //QObject::connect(editImgAct, SIGNAL(triggered()), this, SLOT(editImg()));
    toolbar->addAction(editImgAct);

    QAction *sceneManipAct = new QAction(QIcon(dir+"scene.png"), tr("Manipulate Scene"), this);
    sceneManipAct->setStatusTip(tr("Manipulate Scene"));
    QObject::connect(sceneManipAct, SIGNAL(triggered()), viewer, SLOT(startManipulateSceneSlot()));
    //QObject::connect(sceneManipAct, SIGNAL(triggered()), this, SLOT(sceneManip()));
    toolbar->addAction(sceneManipAct);

    toolbar->addAction(QIcon(dir+"edit.png"), QString("Edit Markers"), this, SLOT(newMarkers()));
    toolbar->addAction(QIcon(dir+"onoff.png"), QString("Hide FrameAxis"), this, SLOT(hideFrameAxis()));
    toolbar->addAction(QIcon(dir+"exit.png"), QString("Remove Markers"), this, SLOT(deleteMarker()));
    toolbar->addAction(QIcon(dir+"valid.png"), QString("Calculate matrix"), this, SLOT(computeMatrix()));
    toolbar->addAction(QIcon(dir+"log.png"), QString("Print info"), this, SLOT(debugInfo()));

    QLabel* label = new QLabel;
    label->setText("Rectangle size");
    toolbar->addWidget(label);
    labelBox = new QSpinBox;
    labelBox->setValue(7.0);
    toolbar->addWidget(labelBox);

    PoLAR::Light* light = getViewer()->addLightSource(2,-2, 5.5, true);
    lightDialog = new PoLAR::LightDialog(viewer, light, this);
    toolbar->addAction(QIcon(dir+"light.png"), QString("Light parameters"), lightDialog, SLOT(show()));

    for(auto it : toolbar->children())
    {
        if(QWidget* w = dynamic_cast<QWidget*>(it))
        {
            w->setAttribute(Qt::WA_AcceptTouchEvents);
        }
    }

    // add focus to this interface in order to get the keyboard events
    this->setFocusPolicy(Qt::StrongFocus);
    // remove the focus from the Viewer, otherwise it will bypass the interface's events
    viewer->setFocusPolicy(Qt::NoFocus);

    viewer->startEditImageSlot();
}

Interface::~Interface()
{
    delete lightDialog;
}


void Interface::hideFrameAxis()
{
    if(_frame.valid())
    {
        _frame->setDisplayOff();
    }
}

void Interface::newMarkers()
{
    DrawableObject2D* obj = viewer->getObject2DByIndex(0);
    if(!obj)
    {
        viewer->newPolygon();
        DrawableObject2D* obj = viewer->getCurrentObject2D();
        obj->setLineWidth(3);
        obj->setMarkerSize(50);
        obj->setPointingPrecision(500);
    }
    else
    {
        obj->selectOn();
        viewer->startEditMarkersSlot();
    }
}

void Interface::deleteMarker()
{
    PoLAR::DrawableObject2D* obj = viewer->getCurrentObject2D();
    if(obj)
    {
        if(obj->getCurrentMarker())
            //     obj->getObject()->removeMarker(obj->getObject()->getCurrentMarker());
            obj->removeCurrentMarker();
        else viewer->removeCurrentObject2D();
    }
}


void Interface::debugInfo()
{
    //qDebug("%d %d", viewer->width(), viewer->height());
    osg::Matrix M = getViewer()->getProjection();

    QString file = QFileDialog::getSaveFileName(this,
                                                "Choose destination file",
                                                QString::null);
    if(file.size())
    {
        PoLAR::Util::saveProjectionMatrix(M, file);
    }
}


void Interface::computeMatrix()
{
    PoLAR::DrawableObject2D* currentObj = getViewer()->getObject2DByIndex(0);
    if(currentObj)
    {
        std::list<PoLAR::Marker2D*>::iterator it = currentObj->getMarkers().begin();
        std::vector<osg::Vec2> vecList;
        while(it != currentObj->getMarkers().end())
        {
            PoLAR::Marker2D* m = (*it);
            osg::Vec2 pt(m->x(), m->y());
            vecList.push_back(pt);
            ++it;
        }
        if(vecList.size() >= 4 && getViewer()->getBgImage())
        {
            float s = labelBox->value();
            osg::Matrix proj = ProjectionComputation::calculate(vecList[0],vecList[1], vecList[2], vecList[3], getViewer()->getBgImage()->getWidth(), getViewer()->getBgImage()->getHeight(), s);
            if(_object.valid())
            {
                getViewer()->addObject3D(_object.get());
                getViewer()->setTrackNode(_object.get());
                if(_object->hasAnimations())
                {
                   _object->play();
                }
            }
            getViewer()->setProjection(proj);
        }
    }
}


void Interface::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Q:
        close();
        break;
    default:
        break;
    }
}
