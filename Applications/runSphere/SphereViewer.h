#ifndef SPHEREVIEWER_H
#define SPHEREVIEWER_H

#include <PoLAR/Viewer.h>
#include <PoLAR/VideoPlayer.h>

class SphereViewer : public PoLAR::Viewer
{
public:
    SphereViewer(PoLAR::BaseImage* img, PoLAR::VideoPlayer* camera);

    void setImage(PoLAR::BaseImage* img)
    {
        _img = img;
    }

protected:

    void keyPressEvent(QKeyEvent *event);

    PoLAR::BaseImage* _img;
    PoLAR::VideoPlayer* _camera;
};


#endif // SPHEREVIEWER_H
