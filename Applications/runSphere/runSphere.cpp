
#include <iostream>
#include <QApplication>
#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>

#include <PoLAR/Image.h>
#include <PoLAR/Object3D.h>
#include <PoLAR/ShaderManager.h>
#include "SphereViewer.h"

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);

    osg::ref_ptr<PoLAR::Image<unsigned char> > myImage;
    osg::ref_ptr<PoLAR::VideoPlayer> camera;
    int width = 1024;
    int height = 768;
    int cameraNumber = 0; // the device number: /dev/videoX

    if(argc < 2)
    {
        std::cerr << "Syntax: " << argv[0] << " <3D model> [<texture>]" << std::endl;
        exit(0);
    }

    if(argc > 2)
    {
        // load static image. First check if the file exists
        if(PoLAR::Util::fileExists(argv[2]))
            myImage = new PoLAR::Image<unsigned char>(argv[2], true);
        else
        {
            std::cerr << "File " << argv[2] << " not found" << std::endl;
            exit(0);
        }
    }
    else
    {
        // create webcam manager
        camera = new PoLAR::VideoPlayer(cameraNumber);
        if(!camera.get())
        {
            std::cerr << "Camera initialisation failed" << std::endl;
            exit(0);
        }
        // create texture from webcam stream
        myImage = new PoLAR::Image<unsigned char>(camera.get());
    }

    // create viewer
    SphereViewer viewer(myImage.get(), camera.get());
    viewer.resize(width, height);

    // load 3D model
    osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFile(argv[1]);
    if(!loadedModel)
    {
        std::cerr << "Unable to load model" << std::endl;
        exit(0);
    }
    osgUtil::SmoothingVisitor smooth;
    osgUtil::Optimizer optimizer;
    optimizer.optimize(loadedModel.get());
    loadedModel->accept(smooth);
    osg::ref_ptr<PoLAR::Object3D> object = new PoLAR::Object3D(loadedModel.get());
    object->setEditOn();
    // add texture to 3D model
	bool flipTexture;
#if defined(WIN32) || defined(_WIN32)
	flipTexture = false;
#else
	flipTexture = true;
#endif
    object->setObjectTextureState(myImage.get(), flipTexture);

    // add window/level support to texture
    osg::ref_ptr<osg::Program> program = new osg::Program;
    PoLAR::ShaderManager::instanciateWLShaderSources(program.get());
    PoLAR::ShaderManager::instanciateWLShaderUniforms(object->getOriginalNode()->getOrCreateStateSet(), myImage.get());
    object->getOriginalNode()->getOrCreateStateSet()->setAttributeAndModes(program, osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);

    // add 3D model to viewer
    viewer.addObject3D(object.get());

    // set default projection
    osg::Matrixd proj = viewer.createVisionProjection(width, height);
    viewer.setProjection(proj);
    // create custom projection
    osg::Matrixd extrinsics(1, 0, 0, 0,
                            0, 0, 1, 0,
                            0, -1, 0, 0,
                            0, 0, proj(3,2), 1);
    viewer.setPose(extrinsics);

    // show the iewer
    viewer.center();
    viewer.show();

    // launch webcam stream
    if(camera.valid()) camera->play();

    // start image edition mode
    viewer.startEditImageSlot();
    viewer.startWindowLevelSlot(myImage.get());

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );
    return app.exec();
}
