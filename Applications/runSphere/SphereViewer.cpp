#include "SphereViewer.h"

SphereViewer::SphereViewer(PoLAR::BaseImage* img, PoLAR::VideoPlayer* camera):
    PoLAR::Viewer(),
    _img(img), _camera(camera)
{
}


void SphereViewer::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Q:
    {
        close();
        break;
    }
    case Qt::Key_W:
    {
        if(_img)
            stopWindowLevelSlot(_img);
        startManipulateSceneSlot();
        break;
    }
    case Qt::Key_I:
    {
        if(_img)
            startWindowLevelSlot(_img);
        startEditImageSlot();
        break;
    }
    case Qt::Key_R:
    {
        if(_img)
            _img->resetWindowLevel();
        break;
    }
    case Qt::Key_P:
    {
        std::cout << getPose() << std::endl;
        break;
    }
    case Qt::Key_Up:
    {
        if(_camera) _camera->play();
        break;
    }
    case Qt::Key_Down:
    {
        if(_camera) _camera->stop();
        break;
    }

    default:
        PoLAR::Viewer::keyPressEvent(event);
        break;
    }
}
