#ifndef INTERFACE_H
#define INTERFACE_H

#include <QStringList>
#include <QMessageBox>
#include <QMainWindow>
#include <QToolBar>
#include <QToolButton>
#include <QSlider>
#include <QScrollBar>
#include <QInputDialog>
#include <QSpinBox>
#include <QLabel>
#include <QComboBox>

#include <osgViewer/ViewerEventHandlers> // for the StatsHandler (frame rate...)
#include <osgGA/TrackballManipulator>

#include <PoLAR/Viewer.h>
#include <PoLAR/DrawableObject2D.h>

using PoLAR::Marker2D;

class Interface : public QMainWindow
{
    Q_OBJECT

public:
    Interface();

    ~Interface();

    void addImage(PoLAR::BaseImage* image, int nima)
    {
        viewer->setBgImage(image);
        scrollbar->setMaximum(nima-1);
        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }


    void addSequence(float* sequence, int w, int h, int d, int nima)
    {
        viewer->setBgImage(sequence, w, h, d, nima);
        scrollbar->setMaximum(nima-1);

        viewer->bgImageOn();
        viewer->startEditImageSlot();
    }

    void addNode(PoLAR::Object3D *object)
    {
        viewer->addObject3D(object);
    }

    void setProjection(osg::Matrixd& P, PoLAR::Viewer2D3D::ProjectionType pt = PoLAR::Viewer2D3D::VISION) {viewer->setProjection (P, pt);}
    void setProjection() {viewer->setProjection();} // default projection

    inline PoLAR::Viewer* getViewer() {return viewer;}


signals:
    void startEditImageSignal();
    void startEditMarkersSignal();


protected:


    //virtual bool event(QEvent* event);


private slots:

    void editImg()
    {
        qDebug("edit img\n");
    }

    void sceneManip()
    {
        qDebug("scene manip\n");
    }


    void newSpline();

    void splineInfo()
    {
        PoLAR::DrawableObject2D* obj = viewer->getCurrentObject2D();
        if(obj)
            qDebug("%d", obj->getObject()->nbMarkers());
    }

    void deleteMarker();

    void debugInfo();

    void manipulateObject();

    void removeDragger();

private:
    QToolBar *toolbar;
    QToolButton *quitButton, *imageButton, *markerButton, *saveButton, *editSceneButton;
    QSpinBox *labelBox;
    QScrollBar *scrollbar;
    PoLAR::Viewer *viewer;
};

#endif // INTERFACE_H
