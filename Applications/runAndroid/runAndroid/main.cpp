/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <iostream>
#include "interface.h"
#include <PoLAR/Image.h>
#include <PoLAR/Object3D.h>
#include <PoLAR/Notify.h>

#include "mainwindow.h"

// register the plugins necessary to load models and textures
#include <osgDB/Registry>
USE_OSGPLUGIN(pnm)
USE_OSGPLUGIN(jpeg)
USE_OSGPLUGIN(off)
USE_OSGPLUGIN(obj)
USE_OSGPLUGIN(osg2)
USE_OSGPLUGIN(freetype)
// register the serialization libraries needed for loading some extensions (e.g. animated osgt models)
USE_SERIALIZER_WRAPPER_LIBRARY(osg)
USE_SERIALIZER_WRAPPER_LIBRARY(osgAnimation)


int main(int argc,char ** argv)
{
    QApplication app(argc, argv);

    // Redirect the OSG notify stream to QDebug
    //osg::setNotifyHandler(new PoLAR::QDebugNotifyHandler);

    Interface inter;
    QString dataPath("/sdcard/polar/data/");

    osg::ref_ptr<PoLAR::Image_uc> myImage;
    // Create the viewer, here the widget used to display the image read is a native PoLAR viewer

    QString img(dataPath  + "example1.ppm");
    myImage = new PoLAR::Image_uc(img.toStdString(), true);

    // Add the image read as background image
    inter.getViewer()->setBgImage(myImage.get());

    // Show it
    inter.getViewer()->bgImageOn();

    inter.getViewer()->addLightSource(5.0,2.0,5.0, true);

    {
        QString model(dataPath  + "armchair.obj");
        //QString model(dataPath  + "horseNorm.off");
        osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(model.toStdString(), false, true, true);
        object3D->optimize();
        inter.addNode(object3D.get());
        inter.getViewer()->setTrackNode(object3D.get());
    }

    { // create a ground for receiving shadows
        osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
        osg::ref_ptr<osg::ShapeDrawable> shape;
        shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, -0.00001f), 5.0f, 5.0f, 0.00005f));
        floorGeode->addDrawable(shape.get());
        osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get());
        object3D->setName("Floor");
        object3D->setEditOn();
        inter.addNode(object3D.get());
        object3D->setPhantomOn();
    }


    { // create the wardrobe
        osg::ref_ptr<osg::Geode> boxGeode = new osg::Geode;
        osg::ref_ptr<osg::ShapeDrawable> shape;
        shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 1.0f, 1.0f, 1.0f));
        boxGeode->addDrawable(shape.get());
        osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(boxGeode.get(), false, true, true);
        object3D->setEditOn();
        object3D->setName("wardrobe");
        osg::Matrixd m =  osg::Matrixd::rotate(osg::Quat(0,0,0,0.94))
                * osg::Matrixd::scale(0.41,0.56,1.56) * osg::Matrixd::translate(-0.57,0.33,0.81);
        object3D->setTransformationMatrix(m);
        object3D->setPhantomOn();
        inter.addNode(object3D.get());
    }

    QString proj(dataPath + "example1.proj");
    osg::Matrixd P = PoLAR::Util::readProjectionMatrix(proj);
    inter.setProjection(P);

    inter.getViewer()->setOptimalZoom();

    // Show the widget
    inter.show();

    return app.exec();
}
