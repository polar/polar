/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QColorDialog>
#include <QTouchEvent>
#include "interface.h"

using namespace PoLAR;

Interface::Interface(): QMainWindow()
{
    setAttribute(Qt::WA_AcceptTouchEvents);

    QString dataPath("/sdcard/polar/data/");
    QString dir = dataPath + "icons/";

    // Create a Viewer with shadows management
    viewer = new PoLAR::Viewer(this, "viewer", 0, true);
    //viewer->setShadowsOn();

    setCentralWidget(viewer);
    toolbar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea, toolbar);
    toolbar->setOrientation(Qt::Vertical);
    toolbar->setAttribute(Qt::WA_AcceptTouchEvents);
    toolbar->setIconSize(QSize(128,128));
    qApp->setAttribute(Qt::AA_SynthesizeMouseForUnhandledTouchEvents, true);

    QAction *editImgAct = new QAction(QIcon(dir+"img.png"), tr("Edit image"), this);
    editImgAct->setStatusTip(tr("Edit Image"));
    QObject::connect(editImgAct, SIGNAL(triggered()), viewer, SLOT(startEditImageSlot()));
    //QObject::connect(editImgAct, SIGNAL(triggered()), this, SLOT(editImg()));
    toolbar->addAction(editImgAct);

    QAction *sceneManipAct = new QAction(QIcon(dir+"scene.png"), tr("Manipulate Scene"), this);
    sceneManipAct->setStatusTip(tr("Manipulate Scene"));
    QObject::connect(sceneManipAct, SIGNAL(triggered()), this, SLOT(manipulateObject()));
    //QObject::connect(sceneManipAct, SIGNAL(triggered()), this, SLOT(sceneManip()));
    toolbar->addAction(sceneManipAct);

    toolbar->addAction(QIcon(dir+"edit.png"), QString("Edit Markers"), this, SLOT(newSpline()));
    toolbar->addAction(QIcon(dir+"exit.png"), QString("Remove Markers"), this, SLOT(deleteMarker()));

    toolbar->addAction(QIcon(dir+"log.png"), QString("Print info"), this, SLOT(debugInfo()));

    for(auto it : toolbar->children())
    {
        if(QWidget* w = dynamic_cast<QWidget*>(it))
        {
            w->setAttribute(Qt::WA_AcceptTouchEvents);
        }
    }

    // add focus to this interface in order to get the keyboard events
    this->setFocusPolicy(Qt::StrongFocus);
    // remove the focus from the Viewer, otherwise it will bypass the interface's events
    viewer->setFocusPolicy(Qt::NoFocus);

    QObject::connect(viewer, SIGNAL(editionModeChanged()), this, SLOT(removeDragger()));

    viewer->startEditImageSlot();
}

Interface::~Interface()
{
}


void Interface::newSpline()
{
    viewer->newSpline();
    DrawableObject2D* obj = viewer->getCurrentObject2D();
    obj->setLineWidth(3);
    obj->setMarkerSize(8);
    obj->setPointingPrecision(30);
}

void Interface::deleteMarker()
{
    PoLAR::DrawableObject2D* obj = viewer->getCurrentObject2D();
    if(obj)
    {
        if(obj->getCurrentMarker())
            obj->removeCurrentMarker();
        else viewer->removeCurrentObject2D();
    }
}


void Interface::debugInfo()
{
    qDebug("%d %d", viewer->width(), viewer->height());
}


void Interface::manipulateObject()
{
    viewer->startManipulateSceneSlot();
    PoLAR::Object3D* object = viewer->getObject3D(0);
    if(object)
    {
        object->addDragger(new osgManipulator::TabBoxDragger);
    }
}

void Interface::removeDragger()
{
    PoLAR::Object3D* object = viewer->getObject3D(0);
    if(object)
    {
        object->removeDragger();
    }
}
