/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include <QFileInfo>
#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/io_utils>

#include <PoLAR/Image.h>
#include <PoLAR/Util.h>
#include <PoLAR/Viewer.h> // PoLAR::Viewer header


int main(int argc,char ** argv)
{
    //osg::setNotifyLevel(osg::DEBUG_FP);
    QApplication app(argc, argv);
    int width;
    int height;
    osg::ref_ptr<PoLAR::Image<unsigned char> > myImage;

    PoLAR::Viewer viewer(0, "Display Thorax Anatomy");

    // Read the X-ray image
    QFileInfo imgFile("../data/thorax.png");
    if(imgFile.exists() && imgFile.isFile())
        myImage = new PoLAR::Image<unsigned char>("../data/thorax.png", true, 1);
    else
    {
        std::cerr << "Thorax image file not found" << std::endl;
        exit(0);
    }
    width = myImage->getWidth();
    height = myImage->getHeight();

    viewer.resize(width,height);

    // Add the image read as background image
    viewer.setBgImage(myImage);

    // Show it
    viewer.bgImageOn();

    // Have the image interaction activ
    viewer.startEditImageSlot();
 
    // Load the organs
    osgUtil::SmoothingVisitor smooth;
    osgUtil::Optimizer optimizer;


    // Lung
    osg::ref_ptr<osg::Node> loadedLung;
    loadedLung = osgDB::readNodeFile("../data/lung.obj");
    if(!loadedLung)
    {
        std::cerr << "Unable to load lung" << std::endl;
        exit(0);
    }
    optimizer.optimize(loadedLung.get());
    loadedLung->accept(smooth);
    osg::ref_ptr<PoLAR::Object3D> lung = new PoLAR::Object3D(loadedLung.get(), true, true);
    viewer.addObject3D(lung.get());
    viewer.setTrackNode(lung.get());

    // liver
    osg::ref_ptr<osg::Node> loadedliver;
    loadedliver = osgDB::readNodeFile("../data/liver.obj");
    if(!loadedliver)
    {
        std::cerr << "Unable to load liver" << std::endl;
        exit(0);
    }
    optimizer.optimize(loadedliver.get());
    loadedliver->accept(smooth);
    osg::ref_ptr<PoLAR::Object3D> liver = new PoLAR::Object3D(loadedliver.get(), true, true);
    viewer.addObject3D(liver.get());
    viewer.setTrackNode(liver.get());

    // diaph1Layer
    osg::ref_ptr<osg::Node> loadeddiaph1Layer;
    loadeddiaph1Layer = osgDB::readNodeFile("../data/diaph1Layer.obj");
    if(!loadeddiaph1Layer)
    {
        std::cerr << "Unable to load diaph1Layer" << std::endl;
        exit(0);
    }
    optimizer.optimize(loadeddiaph1Layer.get());
    loadeddiaph1Layer->accept(smooth);
    osg::ref_ptr<PoLAR::Object3D> diaph1Layer = new PoLAR::Object3D(loadeddiaph1Layer.get(), true, true);
    viewer.addObject3D(diaph1Layer.get());
    viewer.setTrackNode(diaph1Layer.get());

    // spine
    osg::ref_ptr<osg::Node> loadedspine;
    loadedspine = osgDB::readNodeFile("../data/spine.obj");
    if(!loadedspine)
    {
        std::cerr << "Unable to load spine" << std::endl;
        exit(0);
    }
    optimizer.optimize(loadedspine.get());
    loadedspine->accept(smooth);
    osg::ref_ptr<PoLAR::Object3D> spine = new PoLAR::Object3D(loadedspine.get(), true, true);
    viewer.addObject3D(spine.get());
    viewer.setTrackNode(spine.get());

    // ribs
    for (int cptSide(0);cptSide<2;cptSide++){
        for (int cptRibs(3);cptRibs<11;cptRibs++){
            osg::ref_ptr<osg::Node> loadedrib;
            std::ostringstream ribFileStream;
            if (cptSide==0)
                ribFileStream << "../data/ribs/rib" << cptRibs << "L.obj"; 
            else
                ribFileStream << "../data/ribs/rib" << cptRibs << "R.obj"; 
            std::string ribFile = ribFileStream.str();
            loadedrib = osgDB::readNodeFile(ribFile);
            if(!loadedrib)
            {
                std::cerr << "Unable to load rib" << std::endl;
                exit(0);
            }
            optimizer.optimize(loadedrib.get());
            loadedrib->accept(smooth);
            osg::ref_ptr<PoLAR::Object3D> rib = new PoLAR::Object3D(loadedrib.get(), true, true);
            viewer.addObject3D(rib.get());
            viewer.setTrackNode(rib.get());
        }
    }



    // Show the widget
    viewer.center();
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
