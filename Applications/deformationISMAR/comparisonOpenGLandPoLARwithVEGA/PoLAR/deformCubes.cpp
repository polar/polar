#include <QtWidgets/QApplication> // for Qt functions
#include <osgDB/ReadFile>
#include "Viewer.h" // PoLAR::Viewer header
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include "SceneGraph.h"
#include "Image.h" 
#include "Object3D.h"
#include <osg/Geometry>
// To display the fps
#include <osgViewer/ViewerEventHandlers> 

#include <forceModel.h>
#include <corotationalLinearFEM.h>
#include <corotationalLinearFEMForceModel.h>
#include <generateMassMatrix.h>
#include <implicitNewmarkSparse.h>

#include "SceneGraphManipulator.h"
#include "Util.h"
#include "SimulationVega.h"
#include "SimulationWorker.h"
#include "VegaObject3D.h"
#include "SimulationManager.h"

#include <iostream>
#include <vector>
#include <list>

using namespace std;


class PressCube: public PoLAR::VegaInteraction
{
public:
    void initializeForce(PoLAR::VegaObject3D *object)
    {
        numVertices = object->getVolumetricMesh()->getNumVertices();
        int r = 3 * numVertices;
        u = (double*) calloc (r, sizeof(double));
        f_extBase  = (double*) calloc (r, sizeof(double));
        f_ext = (double*) calloc (r, sizeof(double));
        initialPressure(object);
    }
    void applyForce(PoLAR::VegaObject3D *object){}
PressCube(int forceIntensity):
        surfaceForce(forceIntensity)
{
    std::cout << "Creating BCs\n" ;
}

~PressCube()
{
    free(u);
    free(f_ext);
    free(f_extBase);
    std::cout << "Destroying BCs\n" ;
}
void resetForce(PoLAR::VegaObject3D *object)
{
    std::cout << "Reseting BCs\n" ;
    free(u);
    free(f_ext);
    free(f_extBase);
    int r = 3 * numVertices;
    u = (double*) calloc (r, sizeof(double));
    f_extBase  = (double*) calloc (r, sizeof(double));
    f_ext = (double*) calloc (r, sizeof(double));
    object->setExternalForces(f_ext);
}
protected:
    int surfaceForce, x, y, z, numVertices;
    double *u, *f_extBase, *f_ext;
    void initialPressure(PoLAR::VegaObject3D *object)
    {
        numVertices = object->getVolumetricMesh()->getNumVertices();
        double epsilonDOF=1;
        std::vector<int> pushedDOFs;
        for (int cptIndex=0;cptIndex<numVertices;cptIndex++){
            if (object->getVolumetricMesh()->getVertex(cptIndex)[0][1]<-25+epsilonDOF){
                pushedDOFs.push_back(cptIndex);
            }
        }   
        int r = 3 * numVertices;
        memcpy(u, object->getIntegrator()->Getq(), sizeof(double) * r);
        memcpy(f_ext, f_extBase, sizeof(double) * r);
        double forceY=(surfaceForce*9.81*1e4)/pushedDOFs.size();
        double externalForce[3] = {0,forceY,0};
        for(std::vector<int>::iterator it = pushedDOFs.begin(); it != pushedDOFs.end(); ++it) {
            int pulledVertex(*it);
            f_ext[3*pulledVertex+0] += externalForce[0];
            f_ext[3*pulledVertex+1] += externalForce[1];
            f_ext[3*pulledVertex+2] += externalForce[2];
        }
        object->setExternalForces(f_ext);
    } 
};



class MySimulationVega: public PoLAR::SimulationWorker
{
public:
    //MySimulationVega(std::list<osg::ref_ptr<PoLAR::VegaObject3D> > objectList):
    MySimulationVega(osg::ref_ptr<PoLAR::VegaObject3D> object):
        _object(object)    
    {}
    ~MySimulationVega()    
    {}

protected:
    void initialize()
    {
        _object->initForces();
    }

    void run()
    {
        _object->doTimestep();
        _object->requestMeshUpdate();
    }
    osg::ref_ptr<PoLAR::VegaObject3D> _object;
};


void initPhysics(PoLAR::VegaObject3D *obj)
{

    TetMesh *tetraMesh = (TetMesh*)obj->getVolumetricMesh();
    CorotationalLinearFEM * deformableModel = new CorotationalLinearFEM(tetraMesh);
    ForceModel * forceModel = new CorotationalLinearFEMForceModel(deformableModel);
    int numTetraVertices = 3 * tetraMesh->getNumVertices(); // total number of DOFs
    double epsilonDOF=5;
    std::vector<int> constrainedDOFsVector;
    osg::Vec3Array *listVerticesOBJ= obj->getVertexList(0);
    int cptIndex(0);
    for(osg::Vec3Array::iterator it = listVerticesOBJ->begin(); it != listVerticesOBJ->end(); ++it, cptIndex++) {
         if ( (*it).y()>25-epsilonDOF && ( (*it).x()>25-epsilonDOF ||  (*it).x()<-25+epsilonDOF) ) {           
            constrainedDOFsVector.push_back(cptIndex*3+1);
            constrainedDOFsVector.push_back(cptIndex*3+2);
        }
    } 
    SparseMatrix *massMatrix;
    GenerateMassMatrix::computeMassMatrix(tetraMesh, &massMatrix, true);
    int positiveDefiniteSolver = 0;
    int numConstrainedDOFs =constrainedDOFsVector.size();
    int *constrainedDOFs = &constrainedDOFsVector[0];
    double dampingMassCoef = 1.0; // "underwater"-like damping (here turned off)
    double dampingStiffnessCoef = 0.01; // (primarily) high-frequency damping
    double timeStep = 0.2; // the timestep, in seconds
    float newmarkBeta = 0.25;
    float newmarkGamma = 0.5;
    int maxIterations = 1;
    double epsilon = 1E-6;
    int numSolverThreads = 1;
    ImplicitNewmarkSparse *integrator = new ImplicitNewmarkSparse(numTetraVertices, timeStep, massMatrix, forceModel,
                                                                  positiveDefiniteSolver, numConstrainedDOFs, constrainedDOFs,
                                                                  dampingMassCoef, dampingStiffnessCoef, maxIterations, epsilon, newmarkBeta, newmarkGamma, numSolverThreads);
    obj->setIntegrator(integrator);
}

int main(int argc,char ** argv)
{
    int surfaceForce(250);
    QApplication app(argc, argv);
    PoLAR::Viewer viewer(0, "Cube deformation");
    int width = 700;
    int height = 500;
    viewer.resize(width, height);
    viewer.show();
    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );
    viewer.addEventHandler(new osgViewer::StatsHandler);
    viewer.startEditImageSlot();
    osg::ref_ptr<PoLAR::Image<unsigned char> > myImage;
    myImage = new PoLAR::Image<unsigned char> ("../../../data/images/cubeX/X250.png", true); 
    viewer.setBgImage(myImage);   
    viewer.bgImageOn();// Show it
    osg::ref_ptr<PoLAR::Light> light = new PoLAR::Light();
    viewer.addLightSource(light.get());
    osg::Matrixd P = PoLAR::Util::
    readProjectionMatrix("../../../data/projection.txt");
    viewer.setProjection(P);
    osg::ref_ptr<MySimulationVega> vegaSim;
    osg::ref_ptr<PoLAR::SimulationManager> simMan;
    osg::ref_ptr<osg::Node> loadedModel;
    osgDB::Options* options = new osgDB::Options;
    options->setOptionString("noRotation");
    const char* objGeometry="../../../data/volumes/cubeX/cubeX.obj";
    loadedModel = osgDB::readNodeFile(objGeometry,options);
    const char* vegasGeometry = "../../../data/volumes/cubeX/cubeX.veg";
    osg::ref_ptr<PoLAR::VegaObject3D> obj3D = 
             new PoLAR::VegaObject3D(loadedModel.get(),vegasGeometry,false);
    viewer.addObject3D(obj3D.get());   
    osgUtil::SmoothingVisitor smooth;
    osgUtil::Optimizer optimizer;
    optimizer.optimize(obj3D.get());
    obj3D->accept(smooth);   
    obj3D->setEditOn();  
    obj3D->rotateXSlot(-M_PI/2);  
    obj3D->translate(25, 25, 0);    
    initPhysics(obj3D.get());
    PressCube *myForce = new PressCube(surfaceForce);
    obj3D->addForce(myForce);
    vegaSim = new MySimulationVega(obj3D);
    simMan = new PoLAR::SimulationManager(vegaSim, 0.1, true);
    simMan->launch();
    simMan->play();
    
    viewer.show();
    viewer.setFramerate(30);
    // Run the app
    return app.exec();
    
}
