# - Find VegaFEM
# Find the native VegaFEM includes and library
# This module defines
#  VegaFEM_INCLUDE_DIR, where to find corotationalLinearFEM.h, etc.
#  VegaFEM_INCLUDE_DIRS, which is the same as VegaFEM_INCLUDE_DIR.
#  VegaFEM_LIBRARY_DIR, where to find the libraries (e.g. sceneObject, integratorSparse, etc.).
#  VegaFEM_LIBRARY_DIRS, which is the same as VegaFEM_LIBRARY_DIR.
#  VegaFEM_LIBRARIES, the libraries needed to use VegaFEM.
#  VEGAFEM_FOUND, If false, do not try to use VegaFEM.

FIND_PATH(VegaFEM_VegaFEM_INCLUDE_DIR
    NAMES corotationalLinearFEM.h
    PATHS /usr/include /usr/local/include $ENV{VegaFEM_BASE_DIR}/libraries/include ${VegaFEM_DIR}/libraries/include
)

SET(VegaFEM_NAMES ${VegaFEM_NAMES} libsceneObject.a libsceneObject.so libsceneObject.dylib sceneObject.lib)
FIND_PATH(VegaFEM_VegaFEM_LIBRARY_DIR
    NAMES ${VegaFEM_NAMES}
    PATHS /usr/lib64 /usr/lib /usr/local/lib64 /usr/local/lib $ENV{VegaFEM_BASE_DIR}/lib64 $ENV{VegaFEM_BASE_DIR}/libraries/lib $ENV{VegaFEM_BASE_DIR}/libraries/lib ${VegaFEM_DIR}/libraries/lib64 ${VegaFEM_DIR}/libraries/lib)

IF (VegaFEM_VegaFEM_LIBRARY_DIR AND VegaFEM_VegaFEM_INCLUDE_DIR)
      set(VegaFEM_INCLUDE_DIRS ${VegaFEM_VegaFEM_INCLUDE_DIR})
      set(VegaFEM_INCLUDE_DIR  ${VegaFEM_INCLUDE_DIRS}) # for backward compatiblity
      set(VegaFEM_LIBRARY_DIRS ${VegaFEM_VegaFEM_LIBRARY_DIR})
      set(VegaFEM_LIBRARY_DIR  ${VegaFEM_LIBRARY_DIRS})
      set(VegaFEM_LIBRARIES sceneObject integratorSparse integrator elasticForceModel forceModel loadList insertRows lighting configFile volumetricMesh graph sparseMatrix getopts camera sparseSolver massSpringSystem isotropicHyperelasticFEM stvk corotationalLinearFEM polarDecomposition minivector matrixIO objMesh imageIO)
ENDIF ()

# handle the QUIETLY and REQUIRED arguments and set VEGAFEM_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(VegaFEM
                                  DEFAULT_MSG VegaFEM_INCLUDE_DIRS VegaFEM_LIBRARY_DIRS VegaFEM_LIBRARIES)

mark_as_advanced(VegaFEM_INCLUDE_DIRS VegaFEM_INCLUDE_DIR VegaFEM_LIBRARY_DIRS VegaFEM_LIBRARY_DIR VegaFEM_LIBRARY_DIR VegaFEM_LIBRARIES)

