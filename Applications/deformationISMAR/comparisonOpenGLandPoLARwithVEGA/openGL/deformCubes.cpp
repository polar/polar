#ifndef __APPLE__
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif

#include "sceneObjectDeformable.h"
#include "implicitBackwardEulerSparse.h"
#include "corotationalLinearFEMForceModel.h"
#include "volumetricMeshLoader.h"
#include "generateMeshGraph.h"
#include "generateMassMatrix.h"


ImplicitNewmarkSparse *implicitNewmarkSparse;
int r;
SceneObjectDeformable * deformableObjectRenderingMesh = NULL;
int renderDeformableObject=1;
int windowID;
int i=0;
Graph * meshGraph = NULL;
TetMesh * tetMesh;

// called periodically by GLUT:
void idleFunction(void)
{
    glutSetWindow(windowID);
    glutSetWindowTitle("Deforming a cube with an Open GL display");
    glutPostRedisplay();
}

// graphics loop function.
void displayFunction(void)
{
    int code = implicitNewmarkSparse->DoTimestep();
    if (code != 0)
    {
        printf("The integrator went unstable. Reduce the timestep, or increase the number of substeps per timestep.\n");
    }
    double * u  = (double*) calloc (r, sizeof(double));
    memcpy(u, implicitNewmarkSparse->Getq(), sizeof(double) * r);
    deformableObjectRenderingMesh->SetVertexDeformations(u);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    // render the main deformable object (surface of volumetric mesh)
    if (renderDeformableObject)
    {
        deformableObjectRenderingMesh->Render();
    }

  glutSwapBuffers();
}

// initialize GLUT
void initGLUT(int argc, char* argv[], char * windowTitle, int windowWidth, int windowHeight, int * windowID)
{
  glutInit(&argc, argv);// Initialize GLUT.
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
  glutInitWindowSize(windowWidth, windowHeight);
  *windowID = glutCreateWindow(windowTitle);
}

// Initialize the force field
void initialPressure()
{
    double * f_extBase  = (double*) calloc (r, sizeof(double));
    double * f_ext = (double*) calloc (r, sizeof(double));
    int surfaceForce=500;
    // reset external forces (usually to zero)
    memcpy(f_ext, f_extBase, sizeof(double) * r);

    int numVertices = tetMesh->getNumVertices();
    double epsilonDOF=1;
    std::vector<int> pushedDOFs;
    for (int cptIndex=0;cptIndex<numVertices;cptIndex++){
        if (tetMesh->getVertex(cptIndex)[0][1]<-25+epsilonDOF){
            pushedDOFs.push_back(cptIndex);
        }
    }
    memcpy(f_ext, f_extBase, sizeof(double) * r);
    double forceY=(surfaceForce*9.81*1e4)/pushedDOFs.size();
    double externalForce[3] = {0,forceY,0};
    for(std::vector<int>::iterator it = pushedDOFs.begin(); it != pushedDOFs.end(); ++it) {
        int pulledVertex(*it);
        f_ext[3*pulledVertex+0] += externalForce[0];
        f_ext[3*pulledVertex+1] += externalForce[1];
        f_ext[3*pulledVertex+2] += externalForce[2];
    }
    implicitNewmarkSparse->SetExternalForces(f_ext);
}

// program initialization
void initSimulation(char * veg_filename, char * obj_filename)
{
    //Load a 3D mesh
    printf( "\nLoading... \n\n,%s", veg_filename );
    char* inputFilename = veg_filename;
    VolumetricMesh * volumetricMesh = VolumetricMeshLoader::load(inputFilename);
    if (volumetricMesh == NULL)
        printf("Error: failed to load mesh.\n");
    else
        printf("Success. Number of vertices: %d . Number of elements: %d .\n",
               volumetricMesh->getNumVertices(), volumetricMesh->getNumElements());
    //Check and assign a 3D deformable model
    if (volumetricMesh->getElementType() == VolumetricMesh::TET)
        tetMesh = (TetMesh*) volumetricMesh; // such down-casting is safe in Vega
    else
    {
        printf("Error: not a tet mesh.\n");
        exit(1);
    }
    CorotationalLinearFEM * deformableModel = new CorotationalLinearFEM(tetMesh);
    //Apply force, generate matrices
    // create the class to connect the deformable model to the integrator
    ForceModel * forceModel = new CorotationalLinearFEMForceModel(deformableModel);
    r = 3 * tetMesh->getNumVertices(); // total number of DOFs
    // Create mesh graph for fast Neighbors finding
    meshGraph = GenerateMeshGraph::Generate(volumetricMesh);
    double timeStep = 0.05; // the timestep, in seconds
    SparseMatrix * massMatrix;
    // create consistent (non-lumped) mass matrix
    GenerateMassMatrix::computeMassMatrix(tetMesh, &massMatrix, true);
    // This option only affects PARDISO and SPOOLES solvers, where it is best
    // to keep it at 0, which implies a symmetric, non-PD solve.
    // With CG, this option is ignored.
    int positiveDefiniteSolver = 0;
    // constraining vertices 4, 10, 14 (constrained DOFs are specified 0-indexed):
    std::vector<int> constrainedDOFsVector;
    double epsilonDOF=5;
    for (int cpt=0;cpt<tetMesh->getNumVertices();cpt++){
        if ( tetMesh->getVertex(cpt)[0][1]>25-epsilonDOF &&
            ( tetMesh->getVertex(cpt)[0][0]>25-epsilonDOF ||
              tetMesh->getVertex(cpt)[0][0]<-25+epsilonDOF) ) {
                 constrainedDOFsVector.push_back(cpt*3+1);
                 constrainedDOFsVector.push_back(cpt*3+2);
        }
    }
    int numConstrainedDOFs =constrainedDOFsVector.size();
    int *constrainedDOFs = &constrainedDOFsVector[0];
    // (tangential) Rayleigh damping
    double dampingMassCoef = 1.0; // "underwater"-like damping (here turned off)
    double dampingStiffnessCoef = 0.01; // (primarily) high-frequency damping
    // initialize the integrator
    float newmarkBeta = 0.25;
    float newmarkGamma = 0.5;
    int maxIterations = 1;
    double epsilon= 1E-6;
    int numSolverThreads= 1;
    implicitNewmarkSparse = new ImplicitNewmarkSparse(r, timeStep, massMatrix, forceModel,
                                                      positiveDefiniteSolver, numConstrainedDOFs, constrainedDOFs,
                                                      dampingMassCoef, dampingStiffnessCoef, maxIterations, epsilon, newmarkBeta, newmarkGamma, numSolverThreads);
    //init the rendering
    deformableObjectRenderingMesh = new SceneObjectDeformable(obj_filename);
    deformableObjectRenderingMesh->ResetDeformationToRest();
    deformableObjectRenderingMesh->BuildNeighboringStructure();
    deformableObjectRenderingMesh->BuildNormals();
    deformableObjectRenderingMesh->SetMaterialAlpha(0);
    // init the forces as an initial presure
    initialPressure();
}

//-----------
void InitGl()
//-----------
{
  /* Enable a single OpenGL light. */
  GLfloat light_diffuse[] = {1.0, 0.0, 0.0, 1.0};  /* Red diffuse light. */
  GLfloat light_position[] = {0, 100, 25.0, 0.0};  /* Infinite light location. */
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);
  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);//Use depth buffering for hidden surface elimination.
  glMatrixMode(GL_PROJECTION);// Setup the view of the cube.
  glOrtho(-40, 60, -40, 60, 90, -15);
  glMatrixMode(GL_MODELVIEW);
  gluLookAt(30, 50, 90,  // eye is at (10,0.5,0.5)
              00, 0, 0,      // center is at (0,0,0)
              0.0, -1.0, 00.);      // up is in positive Y direction
}

int main( int argc, char* argv[] )
{
    char objGeometry[] = "../../../data/volumes/cubeX/cubeX.obj";
    char vegasGeometry[] = "../../../data/volumes/cubeX/cubeX.veg";
    //char objGeometry[]="/Users/villardp/GIT/polar/Applications/deformationISMAR/data/volumes/cubeX/cubeX.obj";
    //char vegasGeometry[]="/Users/villardp/GIT/polar/Applications/deformationISMAR/data/volumes/cubeX/cubeX.veg";
    initSimulation( vegasGeometry, objGeometry);
    glutInit(&argc, argv);// Initialize GLUT.
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
    glutInitWindowSize(800, 600);
    windowID = glutCreateWindow("Real-time sim");
    glutDisplayFunc(displayFunction);
    glutIdleFunc(idleFunction);
    InitGl();
    glutMainLoop(); // you have reached the point of no return..
    return 0;
}
