Portable Library for Augmented Reality - collection of volume assets

Volumes are defined by:
- elements ".ele" to define a list of tetrahedrons
- nodes ".node" to define a list of vertices
- Vega file ".veg" to define a vega volume
- Wavefront file ".obj" to define a triangle mesh

These assets are intended to be used as example assets combined with the example programs provided in the parent directory.
All of them are published under the GNU General Public License.

Volume asset folders:
* cubeX a cube with a X-shape structure
* cubeW a cube with a W-shape structure