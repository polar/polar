/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
/** @{ */
/** \file Image.h
\brief Class to apply boundary conditions on a cube.

\author Pierre-Frederic Villard
\date year 2016
*/
/** @} */

#ifndef PRESSCUBE_H
#define PRESSCUBE_H


#include <forceModel.h>
#include <corotationalLinearFEM.h>
#include <corotationalLinearFEMForceModel.h>
#include <generateMassMatrix.h>
#include <implicitNewmarkSparse.h>

#include <PoLAR/VegaObject3D.h>

#include <iostream>
#include <vector>
#include <list>

/**
 * @brief Class to apply boundary conditions on a cube.
 */
class PressCube: public PoLAR::VegaInteraction
{
public:
   /** \name Public members */
    /** @{ */

    /**
     * Default constructor
     */
     PressCube(int forceIntensity);

    /** Constructor
    @param Width width of the image
    @param Height height of the image
    @param Depth depth of the image (grey leveled(depth=1) or coloured(depth=3))
  */

    ~PressCube();

	void initializeForce(PoLAR::VegaObject3D *object);

	void initialPressure(PoLAR::VegaObject3D *object);

    double* applyForce(PoLAR::VegaObject3D *){ return NULL; }

	void resetForce(PoLAR::VegaObject3D *object);

protected:
    int surfaceForce;
    int x, y, z;
    unsigned int numVertices;
    double *u;
    double *f_extBase;
    double *f_ext;
};

#endif // PRESSCUBE_H
