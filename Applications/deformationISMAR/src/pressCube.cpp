/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/
#include "pressCube.h"

using namespace std;

PressCube::PressCube(int forceIntensity):
        surfaceForce(forceIntensity)
{}

PressCube::~PressCube()
{
    free(u);
    free(f_ext);
    free(f_extBase);
    std::cout << "Destroying BCs\n" ;
}

void PressCube::initializeForce(PoLAR::VegaObject3D *object)
{
    std::cout << "Initializing BCs\n" ;
    numVertices = object->getVolumetricMesh()->getNumVertices();
    int r = 3 * numVertices;
    u = (double*) calloc (r, sizeof(double));
    f_extBase  = (double*) calloc (r, sizeof(double));
    f_ext = (double*) calloc (r, sizeof(double));
    initialPressure(object);
}

void PressCube::initialPressure(PoLAR::VegaObject3D *object)
{   
    // the top part need to be pushed
    numVertices = object->getVolumetricMesh()->getNumVertices();
    double epsilonDOF=1;
    std::vector<int> pushedDOFs;
    for (unsigned int cptIndex=0;cptIndex<numVertices;cptIndex++){
        if (object->getVolumetricMesh()->getVertex(cptIndex)[1]<-25+epsilonDOF){
            pushedDOFs.push_back(cptIndex);
        }
    }   
    std::cout << "pushed points= "<< pushedDOFs.size() <<"\n" ;
    int r = 3 * numVertices;
    memcpy(u, object->getIntegrator()->Getq(), sizeof(double) * r);
    // reset external forces (usually to zero)
    memcpy(f_ext, f_extBase, sizeof(double) * r);
    // 3D force
     double forceY=(surfaceForce*9.81*1e4)/pushedDOFs.size();
    double externalForce[3] = {0,forceY,0};
    for(std::vector<int>::iterator it = pushedDOFs.begin(); it != pushedDOFs.end(); ++it) {
        int pulledVertex(*it);
        f_ext[3*pulledVertex+0] += externalForce[0];
        f_ext[3*pulledVertex+1] += externalForce[1];
        f_ext[3*pulledVertex+2] += externalForce[2];
    }
    object->getIntegrator()->SetExternalForces(f_ext);
}

void PressCube::resetForce(PoLAR::VegaObject3D *object)
{
    std::cout << "Reseting BCs\n" ;
    free(u);
    free(f_ext);
    free(f_extBase);
    int r = 3 * numVertices;
    u = (double*) calloc (r, sizeof(double));
    f_extBase  = (double*) calloc (r, sizeof(double));
    f_ext = (double*) calloc (r, sizeof(double));
    object->getIntegrator()->SetExternalForces(f_ext);
    initialPressure(object);
}
