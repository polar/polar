#include <QtWidgets/QApplication> // for Qt functions
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/Geometry>
#include <osg/Shader>

// For wireframe display
#include <osg/PolygonMode>

// To display the fps
#include <osgViewer/ViewerEventHandlers> 

#include <forceModel.h>
#include <corotationalLinearFEM.h>
#include <corotationalLinearFEMForceModel.h>
#include <generateMassMatrix.h>
#include <implicitNewmarkSparse.h>

#include <PoLAR/SceneGraph.h>
#include <PoLAR/Image.h>
#include <PoLAR/Object3D.h>
#include <PoLAR/FrameAxis.h>
#include <PoLAR/SceneGraphManipulator.h>
#include <PoLAR/Util.h>
#include <PoLAR/VegaWorker.h>
#include <PoLAR/VegaObject3D.h>

#include <iostream>
#include <vector>
#include <list>
#include <string>

#include "pressCube.h"
#include "DeformationViewer.h"

using namespace std;



class MySimulationVega: public PoLAR::VegaWorker
{
public:
    //MySimulationVega(std::list<osg::ref_ptr<PoLAR::VegaObject3D> > objectList):
    MySimulationVega(osg::ref_ptr<PoLAR::VegaObject3D> object, IntegratorBase* integrator):
        VegaWorker(integrator),
        _object(object)
    {}

    ~MySimulationVega()
    {}

protected:
    void initialize()
    {
        _object->initForces();
    }

    void run()
    {
        //_object->applyForces();
        int code = doTimestep();
        if (code != 0)
        {
            std::cerr << "The integrator went unstable. Reduce the timestep, or increase the number of substeps per timestep.\n";
        }

        _object->requestMeshUpdate();
    }

    osg::ref_ptr<PoLAR::VegaObject3D> _object;
};


IntegratorBase* initPhysics(PoLAR::VegaObject3D *obj, osg::ref_ptr<osg::Vec4Array> colors)
{

    TetMesh *tetraMesh = (TetMesh*)obj->getVolumetricMesh();
    CorotationalLinearFEM * deformableModel = new CorotationalLinearFEM(tetraMesh);

    //Apply force, generate matrices
    // create the class to connect the deformable model to the integrator
    ForceModel * forceModel = new CorotationalLinearFEMForceModel(deformableModel);
    int numTetraVertices = 3 * tetraMesh->getNumVertices(); // total number of DOFs

    // the bottom part needs to be bloqued
    double epsilonDOF=5;
    std::vector<int> constrainedDOFsVector;
    osg::Vec3Array *listVerticesOBJ= obj->getVertexList(0);
    int cptIndex(0);
    for(osg::Vec3Array::iterator it = listVerticesOBJ->begin(); it != listVerticesOBJ->end(); ++it, cptIndex++) {
        // Check if Y is touching the ground and both extremities on X
        if ( (*it).y()>25-epsilonDOF && ( (*it).x()>25-epsilonDOF ||  (*it).x()<-25+epsilonDOF) ) {
            //constrainedDOFsVector.push_back(cptIndex*3); // No need to fix X
            constrainedDOFsVector.push_back(cptIndex*3+1);
            constrainedDOFsVector.push_back(cptIndex*3+2);
            colors->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 0.25f) );
        }
        else{
            colors->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 0.25f) );
        }
    }


    SparseMatrix *massMatrix;
    // create consistent (non-lumped) mass matrix
    GenerateMassMatrix::computeMassMatrix(tetraMesh, &massMatrix, true);
    // This option only affects PARDISO and SPOOLES solvers, where it is best
    // to keep it at 0, which implies a symmetric, non-PD solve.
    // With CG, this option is ignored.
    int positiveDefiniteSolver = 0;
    // constraining vertices 4, 10, 14 (constrained DOFs are specified 0-indexed):
    int numConstrainedDOFs =constrainedDOFsVector.size();
    int *constrainedDOFs = &constrainedDOFsVector[0];
    // (tangential) Rayleigh damping
    double dampingMassCoef = 1.0; // "underwater"-like damping (here turned off)
    double dampingStiffnessCoef = 0.02; // (primarily) high-frequency damping

    // initialize the integrator
    double timeStep = 0.02;  // the timestep, in seconds
    float newmarkBeta = 0.25;
    float newmarkGamma = 0.5;
    int maxIterations = 1;
    double epsilon = 1E-6;
    int numSolverThreads = 1;
    ImplicitNewmarkSparse *integrator = new ImplicitNewmarkSparse(numTetraVertices, timeStep, massMatrix, forceModel,
                                                                   numConstrainedDOFs, constrainedDOFs,
                                                                  dampingMassCoef, dampingStiffnessCoef, maxIterations, epsilon,
                                                                  newmarkBeta, newmarkGamma, numSolverThreads);
    cout << "Assigning initial parameters\n" ;
    return integrator;
}

int main(int argc,char ** argv)
{
    //osg::setNotifyLevel(osg::DEBUG_FP);
    // Handle the input parameters
    if (argc<2)
    {
        cout << "usage: ./deformCubes simuConfig" << endl;
        cout << "   with :" << endl;
        cout << "     1 - load an empty image with 3D axis" << endl;
        cout << "     2 - load a X-shaped cube with no force" << endl;
        cout << "     3 - load a X-shaped cube with a 2.45 N force (250 g)" << endl;
        cout << "     4 - load a X-shaped cube with a 4.90 N force (500 g)" << endl;
        cout << "     5 - load a X-shaped cube with a 9.81 N force (1 Kg)" << endl;
        cout << "     6 - load a W-shaped cube with no force" << endl;
        cout << "     7 - load a W-shaped cube with a 2.45 N force (250 g)" << endl;
        cout << "     8 - load a W-shaped cube with a 4.90 N force (500 g)" << endl;
        cout << "     9 - load a W-shaped cube with a 9.81 N force (1 Kg)" << endl;
        cout << "    10 - load a bunny" << endl;

        exit(0);
    }
    int simuConfig(atoi(argv[1]));
    int surfaceForce(0);

    // Create the Qt application
    QApplication app(argc, argv);
    // Create the Qt widget, with no parent and a name
    DeformationViewer viewer(0, "Cube deformation");

    //ViewerForDeformation viewer(0, "Cube deformation");
    // Resize the widget
    int width = 700;
    int height = 500;
    viewer.resize(width, height);
    // Show the widget
    viewer.show();
    // This line ensures the application will properly close when the widget has been closed
    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );
    // Display the fps when 's' is pressed
    viewer.addEventHandler(new osgViewer::StatsHandler);

    /* Load the image */
    // 1 - load an empty image with 3D axis
    // 2 - load a X-shaped cube with no force
    // 3 - load a X-shaped cube with a 2.45 N force (250 g)
    // 4 - load a X-shaped cube with a 4.90 N force (500 g)
    // 5 - load a X-shaped cube with a 9.81 N force (1 Kg)
    // 6 - load a W-shaped cube with no force
    // 7 - load a W-shaped cube with a 2.45 N force (250 g)
    // 8 - load a W-shaped cube with a 4.90 N force (500 g)
    // 9 - load a W-shaped cube with a 9.81 N force (1 Kg)
    //Using OSG smart pointers
    osg::ref_ptr<PoLAR::Image<unsigned char> > myImage;
    // Open the image

    switch (simuConfig){
    case 1: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/empty.png", true);
        break;
    }
    case 2: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeX/X.png", true);
        break;
    }
    case 3: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeX/X250.png", true);
        surfaceForce=250;
        break;
    }
    case 4: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeX/X500.png", true);
        surfaceForce=500;
        break;
    }
    case 5: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeX/X1000.png", true);
        surfaceForce=1000;
        break;
    }
    case 6: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeW/W.png", true);
        break;
    }
    case 7: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeW/W250.png", true);
        surfaceForce=250;
        break;
    }
    case 8: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeW/W500.png", true);
        surfaceForce=500;
        break;
    }
    case 9: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/cubeW/W1000.png", true);
        surfaceForce=1000;
        break;
    }
    case 10: {
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/bunny/bunny.png", true);
        surfaceForce=1000;
        break;
    }
    default: {
        cout << "Applying default image\n" << endl;
        myImage = new PoLAR::Image<unsigned char>
                ("../data/images/empty.png", true);
        break;
    }
    }

    // Resize the widget according to the size of the image read
    // width = myImage->getWidth();
    // height = myImage->getHeight();
    // viewer.resize(width, height);
    // Add the image read as background image
    viewer.setBgImage(myImage);
    // Show it
    viewer.bgImageOn();
    // Have the image interaction active
    viewer.startEditImageSlot();

    osg::ref_ptr<PoLAR::Light> light = new PoLAR::Light();
    viewer.addLightSource(light.get());

    /* Load the 3D object */
    // Load the model given in parameter
    // Load a projection matrix

    osg::Matrixd P = PoLAR::Util::
            readProjectionMatrix("../Resources/projection.txt");
    viewer.setProjection(P);

    // Add a frame axis to the scene
    PoLAR::FrameAxis *polarAxis= new PoLAR::FrameAxis;
    polarAxis->setLightingOn();
    PoLAR::Object3D *axis = new PoLAR::Object3D(polarAxis,true);
    axis->setName("FrameAxis");
    axis->setEditOn();
    axis->scale(100.0f);
    viewer.addObject3D(axis);


    osg::ref_ptr<MySimulationVega> vegaSim;
    osg::ref_ptr<PoLAR::SimulationManager> simMan;

    osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
    bool doDeformation =  (simuConfig>1 && simuConfig<10) ;

    if (doDeformation)
    {

        osg::ref_ptr<osg::Geode> hiddenBox = new osg::Geode;
        osg::ref_ptr<osg::ShapeDrawable> shape;
        double boxDimension[3]={34,52,51};
        shape = new osg::ShapeDrawable(new osg::Box(
                                           osg::Vec3(-10+boxDimension[0]/2.f,
                                           0+boxDimension[1]/2.f,
                                       0+boxDimension[2]/2.f),
                boxDimension[0],
                boxDimension[1],
                boxDimension[2]));
        hiddenBox->addDrawable(shape.get());
        PoLAR::Object3D *hideCube = new PoLAR::
                Object3D(hiddenBox.get());
        hideCube->setName("hide cube");
        hideCube->setEditOn();
        hideCube->setPhantomOn();
        viewer.addObject3D(hideCube);


        /* Deformation model */
        // Load the model given in parameter
        osg::ref_ptr<osg::Node> loadedModel;
        osgDB::Options* options = new osgDB::Options;
        options->setOptionString("noRotation");
        // Take the X-shaped by default
        const char* objGeometry="../Resources/cubeX.obj";
        if(simuConfig>5){
            // Otherwise the w-shaped
            objGeometry="../Resources/cubeW.obj";
        }

        loadedModel = osgDB::readNodeFile(objGeometry,options);
        if(!loadedModel.valid())
        {
            std::cout << "Error while loading model " << objGeometry << std::endl;
            exit(0);
        }
        loadedModel->setName("DeformationOBJ");
        
        const char *vegaGeometry;
        if(simuConfig>5)
        {
            // Otherwise the w-shaped
            vegaGeometry = "../Resources/cubeW.veg";
        }
        else
        {
            vegaGeometry = "../Resources/cubeX.veg";
        }
        osg::ref_ptr<PoLAR::VegaObject3D> obj3D =
                new PoLAR::VegaObject3D(loadedModel.get(),vegaGeometry,false,true);
        obj3D->setName("Vega obj");
        
        viewer.addObject3D(obj3D.get());
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;
        optimizer.optimize(obj3D.get());
        obj3D->accept(smooth);
        obj3D->setEditOn();
        obj3D->translate(25, 25, 0);
        obj3D->rotateXSlot(-M_PI/2);
        obj3D->getOrCreateStateSet()->setAttributeAndModes(
                    new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK,
                                         osg::PolygonMode::LINE),  osg::StateAttribute::OVERRIDE);
        //osg::PolygonMode::LINE peut etre remplacé par
        //       osg::PolygonMode::POINT (~point cloud) ou
        //       osg::PolygonMode::FILL (faces pleines : affichage classique)
        obj3D->setMaterial(0.4, 0., 0., 1., 0., 0., 0., 0., 0.,0.);

        // Uncomment below to have transparency
        obj3D->setTransparencyOn();
        obj3D->updateTransparency(0.5);
        
        /*  osg::ref_ptr<PoLAR::Object3D> cube2 = new PoLAR::Object3D(objGeometry, true, true);
        viewer.addObject3D(cube2.get());
        cube2->setEditOn();
        cube2->translate(25,25,0);*/


        std::cout << "============================\n" ;

        
        IntegratorBase* integrator = initPhysics(obj3D.get(),colors);
        obj3D->setIntegrator(integrator);
        obj3D->getDrawable(0)->asGeometry()->setColorArray(colors.get(), osg::Array::BIND_PER_VERTEX);

        PressCube *myForce = new PressCube(surfaceForce);
        obj3D->addForce(myForce);

        vegaSim = new MySimulationVega(obj3D, integrator);
        simMan = new PoLAR::SimulationManager(vegaSim, 0.1, true);
        viewer.setSimulation(simMan.get());
        simMan->launch();
        simMan->play();
    }

    if (simuConfig==10)
    {
        cout << "loading a bunny" << endl;
        // Set the rigid part
        //Load the model given in parameter
        osg::ref_ptr<osg::Node> loadedModel;
        osgDB::Options* options = new osgDB::Options;
        options->setOptionString("noRotation");
        // Take the X-shaped by default
        //const char objGeometry[] = "../data/volumes/bunny/bunny_rigid.stl";
        const char objGeometry[] = "../data/volumes/bunny/bunny.off";
        loadedModel = osgDB::readNodeFile(objGeometry,options);
        if(!loadedModel.valid())
        {
            std::cout << "Error while loading model " << objGeometry << std::endl;
            exit(0);
        }
        osg::ref_ptr<PoLAR::Object3D> obj3D = new PoLAR::Object3D(loadedModel.get(), true, true);
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;
        optimizer.optimize(loadedModel.get());
        loadedModel->accept(smooth);
        //  viewer.addObject3D(obj3D.get());
        obj3D->setEditOn();
        //        obj3D->getOrCreateStateSet()->setAttributeAndModes(
        //            new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK,
        //                osg::PolygonMode::LINE),  osg::StateAttribute::OVERRIDE);
        obj3D->setMaterial(0.0, 0.4, 0., 0., 1., 0., 0., 0., 0.,0.);
        obj3D->setTransparencyOn();
        obj3D->updateTransparency(0.8);

        double bunnyTranslation[3]={-287,48,-1.25};
        double bunnyRotation=-M_PI/2;

        obj3D->rotateZSlot(bunnyRotation);
        obj3D->translate(bunnyTranslation[0], bunnyTranslation[1], bunnyTranslation[2]);

        // Deformable part
        // Load the model given in parameter
        const char objGeometryDef[] = "../data/volumes/bunny/bunny.obj";
        osg::ref_ptr<osg::Node> loadedModelDef;
        loadedModelDef = osgDB::readNodeFile(objGeometryDef,options);
        if(!loadedModelDef.valid())
        {
            std::cout << "Error while loading model " << objGeometryDef << std::endl;
            exit(0);
        }

/*
        // Adding  shader
        const char vertShaderfile[] = "../data/shaders/bunny.vert";
        const char fragShaderfile[] = "../data/shaders/bunny.frag";
        osg::ref_ptr<osg::Shader> vertShader = new osg::Shader(osg::Shader::VERTEX);
        osg::ref_ptr<osg::Shader> fragShader = new osg::Shader(osg::Shader::FRAGMENT);
        vertShader->loadShaderSourceFromFile(vertShaderfile);
        fragShader->loadShaderSourceFromFile(fragShaderfile);
        osg::ref_ptr<osg::Program> program = new osg::Program;
        program->addShader(vertShader.get());
        program->addShader(fragShader.get());
        loadedModelDef->getOrCreateStateSet()->setAttributeAndModes(program.get(), osg::StateAttribute::ON);
        osg::Uniform* nbLights = new osg::Uniform("nbLights", viewer.getSceneGraph()->getNumLights());
        loadedModelDef->getOrCreateStateSet()->addUniform(nbLights);
*/

        const char vegasGeometryDef[] = "../data/volumes/bunny/bunny.veg";
        osg::ref_ptr<PoLAR::VegaObject3D> obj3DDef =
                new PoLAR::VegaObject3D(loadedModelDef.get(),vegasGeometryDef,false);
        viewer.addObject3D(obj3DDef.get());
        optimizer.optimize(obj3DDef.get());
        obj3DDef->accept(smooth);
        obj3DDef->setEditOn();
        obj3DDef->rotateZSlot(bunnyRotation);
        obj3DDef->translate(bunnyTranslation[0], bunnyTranslation[1], bunnyTranslation[2]);
         obj3DDef->getOrCreateStateSet()->setAttributeAndModes(
                    new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK,
                                         osg::PolygonMode::LINE),  osg::StateAttribute::OVERRIDE);

        obj3DDef->setMaterial(0.4, 0., 0., 1., 0., 0., 0., 0., 0.,0.);

        std::cout << "============================\n" ;
        initPhysics(obj3DDef.get(),colors);

        viewer.setTrackNode(obj3DDef.get());
    }


    viewer.show();
    // Run the app
    return app.exec();
    
}
