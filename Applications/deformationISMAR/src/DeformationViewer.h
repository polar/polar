#ifndef DEFORMATIONVIEWER_H
#define DEFORMATIONVIEWER_H

#include <PoLAR/Viewer.h> // PoLAR::Viewer header
#include <PoLAR/SimulationManager.h>

class DeformationViewer: public PoLAR::Viewer
{
public:
    DeformationViewer(QWidget *parent=0, const char *name=0, WindowFlags f=0, bool shadows=false);

    void setSimulation(PoLAR::SimulationManager *simMan)
    {
        if(simMan)
        {
            _simMan = simMan;
        }
    }

   bool givePhantomStatus() {
       return isPhantomActivated;
   }

   void togglePhantomActivation(){
       isPhantomActivated=!isPhantomActivated;
   }

protected:

    virtual void keyPressEvent(QKeyEvent *event);
    bool isPhantomActivated=true;
    osg::ref_ptr<PoLAR::SimulationManager> _simMan;
};

#endif // DEFORMATIONVIEWER_H
