#include "DeformationViewer.h"
#include <PoLAR/PhysicsObject3D.h>

DeformationViewer::DeformationViewer(QWidget *parent, const char *name, WindowFlags f, bool shadows):
    PoLAR::Viewer(parent, name, f, shadows)
{
    setFramerate(30);
}


void DeformationViewer::keyPressEvent(QKeyEvent* event)
{
    switch(event->key())
    {
    case Qt::Key_Q://quit the application
    {
        close();
        break;
    }
    case Qt::Key_X://show the fps
    {
        if(_simMan.get())
        {
            if(_simMan->isFramerateDisplayed())
                _simMan->displayFramerate(false, this);
            else
                _simMan->displayFramerate(true, this);
        }
        break;
    }
    case Qt::Key_R://reset the application
    {
        std::list<osg::ref_ptr<PoLAR::Object3D> > list = getObjects3D();
        std::list<osg::ref_ptr<PoLAR::Object3D> >::iterator it;

        for(it=list.begin(); it!=list.end(); ++it)
        {
            PoLAR::PhysicsObject3D *phobj = dynamic_cast<PoLAR::PhysicsObject3D*>((it)->get());
            if(phobj)
            {
                phobj->resetSimulation();
            }
        }
        break;
    }
    case Qt::Key_T://toggle phantom object
    {
        osg::ref_ptr<PoLAR::Object3D> phantomObject=getPhantomObject3D(0);
        if (phantomObject.valid())
        { // toggle the display of the Phantom object3D
            if (givePhantomStatus())
            {
                phantomObject->setDisplayOff();
            }
            else
            {
                phantomObject->setDisplayOn();
            }
            togglePhantomActivation();
        }
        break;
    }
    case Qt::Key_D:
    {
        bgImageToggle();
        break;
    }
    case Qt::Key_I:
    {
        startEditImageSlot();
        break;
    }
    case Qt::Key_W:
    {
        startManipulateSceneSlot();
        break;
    }
    case Qt::Key_F:
    {
        std::list<osg::ref_ptr<PoLAR::Object3D> > list = getObjects3D();
        std::list<osg::ref_ptr<PoLAR::Object3D> >::iterator it;

        for(it=list.begin(); it!=list.end(); ++it)
        {
            if(!it->get()->getName().compare("FrameAxis"))
            {
                if(it->get()->isDisplayed())
                    it->get()->setDisplayOff();
                else
                    it->get()->setDisplayOn();
            }
        }
        break;
    }
    default:
        PoLAR::Viewer::keyPressEvent(event);
    }
}

