QT += core \
    gui \
    dbus \
    multimedia \
    network \
    opengl \
    printsupport \
    qml \
    svg \
    widgets \
    xml

LIBS += -L/usr/local/lib -lPoLAR -lPoLAR_Physics -lPoLAR_Vega\
    -losg -losgAnimation -losgDB -losgGA -losgManipulator -losgShadow -losgText -losgUtil -losgViewer \
    -lOpenThreads -L$$(VegaFEM_BASE_DIR)/libraries/lib -lintegrator -lmassSpringSystem -lcorotationalLinearFEM\
    -lintegratorSparse -linsertRows -lmassSpringSystem -lsparseSolver -lelasticForceModel  -lforceModel\
    -lsparseMatrix -lpolarDecomposition -lisotropicHyperelasticFEM -lstvk -lminivector -lvolumetricMesh

INCLUDEPATH = /usr/local/include $$(VegaFEM_BASE_DIR)/libraries/include

CONFIG += c++11 qt
QMAKE_CXXFLAGS_RELEASE += -O3

TARGET = Deform
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt
.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


APP_FILES.files = data/projection.txt\
         data/volumes/cubeX/cubeX.obj   data/volumes/cubeX/cubeX.veg\
         data/volumes/cubeX/cubeX.node   data/volumes/cubeX/cubeX.ele\
         data/volumes/cubeW/cubeW.obj   data/volumes/cubeW/cubeW.veg\
         data/volumes/cubeW/cubeW.node   data/volumes/cubeW/cubeW.ele
APP_FILES.path = Contents/Resources
QMAKE_BUNDLE_DATA += APP_FILES

SOURCES += \
        src/DeformationViewer.cpp src/deformCubes.cpp src/pressCube.cpp

HEADERS += \
        src/DeformationViewer.h  src/pressCube.h 

