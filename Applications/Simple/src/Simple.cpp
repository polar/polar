/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <iostream>
#include <QApplication>
#include <osg/io_utils>

#include <PoLAR/Image.h>
#include <PoLAR/Util.h>
#include <PoLAR/Object3D.h>

#include <PoLAR/FrameAxis.h>

#include "SimpleViewer3D.h"




int main(int argc,char ** argv)
{
    QApplication app(argc, argv);

    int width;
    int height;
    osg::ref_ptr<PoLAR::Image_uc> myImage;

    // Create the viewer
    SimpleViewer3D viewer;

    // Read the image
    myImage = new PoLAR::Image_uc("data/image.jpg", true, 1);

    width = myImage->getWidth();
    height = myImage->getHeight();

    viewer.resize(width,height);

    // Add the image read as background image
    viewer.setBgImage(myImage);

    // Show it
    viewer.bgImageOn();

    // Have the image interaction activ
    viewer.startEditImageSlot();

    // Define the type of projection used
    PoLAR::Viewer::ProjectionType pt=PoLAR::Viewer2D3D::VISION;
    pt=PoLAR::Viewer::VISION;

    osg::Matrixd P = PoLAR::Util::readProjectionMatrix("data/projection.txt");
    viewer.setProjection(P, pt);

    // Add light source
    osg::ref_ptr<PoLAR::Light> light = new PoLAR::Light();
    viewer.addLightSource(light.get());

    // Add a frame axis to the scene
    PoLAR::FrameAxis *polarAxis= new PoLAR::FrameAxis;
    polarAxis->setLightingOn();
    PoLAR::Object3D *axis = new PoLAR::Object3D(polarAxis,true);
    axis->setName("FrameAxis");
    axis->setEditOn();
    axis->scale(10.0f);
    viewer.addObject3D(axis);

    // Load the model #1
    osg::ref_ptr<PoLAR::Object3D> obj = new PoLAR::Object3D("data/box.obj", true, true);
    obj->setName("object");
    obj->optimize();
    obj->setMaterial(0.4, 0., 0., 1., 0., 0., 0., 0., 0.,0.);
    // Uncomment below to have transparency
    obj->setTransparencyOn();
    obj->updateTransparency(0.5);
    // Add the object3D created from loaded model to the viewer
    viewer.addObject3D(obj.get());
    viewer.addBlendDialog(obj.get());
    viewer.setTrackNode(obj.get());

    // Load the model #2
    osg::ref_ptr<PoLAR::Object3D> obj2 = new PoLAR::Object3D("data/bunny.obj", true, true);
    obj2->setName("bunny");
    obj2->scale(150.0f);
    obj2->rotate(1.5,0,0,1);
    obj2->translate(-150,0,0);
    obj2->getOrCreateStateSet()->setAttributeAndModes(
               new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK,
                                    osg::PolygonMode::LINE),  osg::StateAttribute::OVERRIDE);

    obj2->setMaterial(0.4, 0., 0., 0., 1., 0., 0., 0., 0.,0.);
    obj2->optimize();
    //osg::Matrixd m =  osg::Matrixd::translate(0.5,0.0,0.005); // initialisation of matrix m with a custom tranformation
    //obj->setTransformationMatrix(m);
    // Add the object3D created from loaded model to the viewer
    viewer.addObject3D(obj2.get());
    viewer.addBlendDialog(obj2.get());
    viewer.setTrackNode(obj2.get());

    // create a phantom ground to receive shadows
    {
        viewer.addLightSource(5,2,5, true);
        osg::ref_ptr<PoLAR::Object3D> ground = PoLAR::Object3D::createGround();
        ground->setPhantomOn();
        viewer.addObject3D(ground.get());
    }

    // Show the widget
    viewer.center();
    viewer.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
