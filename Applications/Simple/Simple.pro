QT += core \
    gui \
    dbus \
    multimedia \
    network \
    opengl \
    printsupport \
    qml \
    svg \
    widgets \
    xml

LIBS += -L/usr/local/lib -lPoLAR \
    -losg -losgAnimation -losgDB -losgGA -losgManipulator -losgShadow -losgText -losgUtil -losgViewer \
    -lOpenThreads

INCLUDEPATH = /usr/local/include

CONFIG += c++11 qt
QMAKE_CXXFLAGS_RELEASE += -O3

TARGET = Simple
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt
.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        src/Simple.cpp

HEADERS += \
        src/SimpleViewer3D.h

