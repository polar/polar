/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include <QApplication>
#include <iostream>
#include "Image.h"
#include "interface.h"
#include "Object3D.h"
#include <VideoPlayer.h>
#include <QDebug>
#include <QCameraInfo>
#include <osgDB/Registry>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osgGA/TrackballManipulator>
#include <osgGA/MultiTouchTrackballManipulator>

#include "mainwindow.h"
//#include "ImageProcessor.h"
#include "MyFilter.h"

// register the plugins necessary to load models and textures
USE_OSGPLUGIN(pnm)
USE_OSGPLUGIN(jpeg)
USE_OSGPLUGIN(off)
USE_OSGPLUGIN(obj)

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);

    //MainWindow viewer;
    Interface inter;
    QString dataPath("/sdcard/polar/data/");

    osg::ref_ptr<PoLAR::Image<unsigned char> > myImage;
    // Create the viewer, here the widget used to display the image read is a native PoLAR viewer

    QString img(dataPath  + "example1.ppm");
    myImage = new PoLAR::Image<unsigned char>(img.toStdString(), true, 1);


    // Add the image read as background image
    inter.getViewer()->setBgImage(myImage.get());

    // Show it
    inter.getViewer()->bgImageOn();

    // declare shaders
    char vertSource[] =
            "attribute vec4 osg_Vertex;\n"
            "attribute vec4 osg_MultiTexCoord0;\n"
            "uniform mat4 osg_ModelViewProjectionMatrix;\n"
            "varying vec4 texCoord0;\n"

            "void main(void)\n"
            "{\n"
            "gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex;\n"
            "texCoord0 = osg_MultiTexCoord0;\n"
            "}\n";

    char fragSource[] =
            "precision mediump float;\n"
            "varying vec4 texCoord0;"
            "uniform sampler2D tex;\n"

            "void main(void)\n"
            "{\n"
            "gl_FragColor = texture2D(tex, texCoord0.st);\n"
            "}\n";


    char vertSourcePhantom[] =
            "attribute vec4 osg_Vertex;\n"
            "attribute vec4 osg_MultiTexCoord0;\n"
            "attribute vec4 osg_MultiTexCoord1;\n"
            "uniform mat4 osg_ModelViewProjectionMatrix;\n"
            "varying vec4 texCoord0;\n"
            "varying vec4 texCoord1;\n"

            "void main(void)\n"
            "{\n"
            "gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex;\n"
            "texCoord0 = osg_MultiTexCoord0;\n"
            "texCoord1 = osg_MultiTexCoord1;\n"
            "}\n";

    char fragSourcePhantom[] =
            "precision mediump float;\n"
            "varying vec4 texCoord0;"
            "varying vec4 texCoord1;"
            "//uniform sampler2DShadow osgShadow_shadowTexture;\n"

            "void main(void)\n"
            "{\n"
            "//vec4 shadow = shadow2DProj( osgShadow_shadowTexture, texCoord1 );\n"
            "gl_FragColor = vec4(1.0,1.0,1.0,0.0);\n"
            "//gl_FragColor *= shadow;\n"
            "}\n";


    {
        QString model(dataPath  + "armchair.obj");
        osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFile(model.toStdString());
        osgUtil::SmoothingVisitor smooth;
        osgUtil::Optimizer optimizer;
        optimizer.optimize(loadedModel.get());
        loadedModel->accept(smooth);
        osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(loadedModel.get(), true, true);
        osg::Matrixd m = osg::Matrixd::translate(0.5,-2.0, -2.0);
        object3D->setTransformationMatrix(m);
        object3D->rotate(M_PI, 0, 0, 1);
        object3D->scale(1.8);

        osg::ref_ptr<osg::Program> program = new osg::Program;
        program->setName( "simple shader" );
        program->addShader( new osg::Shader( osg::Shader::VERTEX, vertSource ) );
        program->addShader( new osg::Shader( osg::Shader::FRAGMENT, fragSource ) );
        loadedModel->getOrCreateStateSet()->setAttributeAndModes( program.get(), osg::StateAttribute::ON );
        inter.addNode(object3D.get());
        inter.getViewer()->setTrackNode(object3D.get());
    }
/*
    { // create a ground for receiving shadows
        osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
        osg::ref_ptr<osg::ShapeDrawable> shape;
        shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, -0.00001f), 5.0f, 5.0f, 0.00005f));
        floorGeode->addDrawable(shape.get());
        osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(floorGeode.get());
        object3D->setName("Floor");
        object3D->setEditOn();
        osg::ref_ptr<osg::Program> program = new osg::Program;
        program->setName( "phantom shader" );
        program->addShader( new osg::Shader( osg::Shader::VERTEX, vertSourcePhantom ) );
        program->addShader( new osg::Shader( osg::Shader::FRAGMENT, fragSourcePhantom ) );
        floorGeode->getOrCreateStateSet()->setAttributeAndModes( program.get(), osg::StateAttribute::ON );
        inter.getViewer()->addObject3D(object3D.get());
        object3D->setPhantomOn();
    }


    { // create the wardrobe
        osg::ref_ptr<osg::Geode> boxGeode = new osg::Geode;
        osg::ref_ptr<osg::ShapeDrawable> shape;
        shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 1.0f, 1.0f, 1.0f));
        boxGeode->addDrawable(shape.get());
        osg::ref_ptr<PoLAR::Object3D> object3D = new PoLAR::Object3D(boxGeode.get(), false, true, true);
        object3D->setEditOn();
        object3D->setName("wardrobe");
        osg::ref_ptr<osg::Program> program = new osg::Program;
        program->setName( "phantom shader" );
        program->addShader( new osg::Shader( osg::Shader::VERTEX, vertSourcePhantom ) );
        program->addShader( new osg::Shader( osg::Shader::FRAGMENT, fragSourcePhantom ) );
        boxGeode->getOrCreateStateSet()->setAttributeAndModes( program.get(), osg::StateAttribute::ON );
        osg::Matrixd m =  osg::Matrixd::rotate(osg::Quat(0,0,0,0.94))
                * osg::Matrixd::scale(0.41,0.56,1.56) * osg::Matrixd::translate(-0.57,0.33,0.81);
        object3D->setTransformationMatrix(m);
        object3D->setPhantomOn();
        inter.getViewer()->addObject3D(object3D.get());
    }
    */

    QString proj(dataPath + "example1.proj");
    osg::Matrixd P = PoLAR::Util::readProjectionMatrix(proj);
    inter.setProjection(P);

    inter.getViewer()->setOptimalZoom();

    // Show the widget
    inter.show();


    /*P = inter.getViewer()->getProjection();
    qDebug("%f %f %f %f", P(0,0),P(0,1),P(0,2), P(0,3));
    qDebug("%f %f %f %f", P(1,0),P(1,1),P(1,2), P(1,3));
    qDebug("%f %f %f %f", P(2,0),P(2,1),P(2,2), P(2,3));
    qDebug("%f %f %f %f", P(3,0),P(3,1),P(3,2), P(3,3));
*/

    return app.exec();
}
