#-------------------------------------------------
#
# Project created by QtCreator 2016-01-22T17:23:17
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets svg multimedia multimediawidgets opengl xml

QMAKE_CXXFLAGS += -std=gnu++11 -fpie -fPIC

TARGET = calibAndroid
TEMPLATE = app


SOURCES += main.cpp \
        mainwindow.cpp \
        interface.cpp \
        ProjectionComputation.cpp


HEADERS  += mainwindow.h \
        interface.h \
        ProjectionComputation.h


CONFIG += mobility
MOBILITY = 


INCLUDEPATH += $$PWD/../../../include
DEPENDPATH += $$PWD/../../../include


unix: PRE_TARGETDEPS += $$PWD/../../../build-android/lib/libPoLAR.a

unix: LIBS += -L$$PWD/../../../build-android/lib -lPoLAR

unix: PRE_TARGETDEPS += $$PWD/../../../build-android/lib/libosgdb_off.a
unix: LIBS += -L$$PWD/../../../build-android/lib -lPoLAR -losgdb_off

INCLUDEPATH += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/include
DEPENDPATH += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/include

INCLUDEPATH += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/3rdparty/libjpeg
DEPENDPATH += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/3rdparty/libjpeg
DEPENDPATH += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/3rdparty/freetype

unix: LIBS += -L$$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/osgPlugins-3.5.0 -losgdb_pnm -losgdb_jpeg -losgdb_obj
unix: LIBS += -L$$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/obj/local/armeabi-v7a -ljpeg
unix: LIBS += -L$$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/obj/local/armeabi-v7a -lft2

unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosg.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosgViewer.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosgGA.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosgDB.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosgText.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosgUtil.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosgManipulator.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libosgShadow.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/libOpenThreads.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/osgPlugins-3.5.0/libosgdb_pnm.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/osgPlugins-3.5.0/libosgdb_jpeg.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib/osgPlugins-3.5.0/libosgdb_obj.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/obj/local/armeabi-v7a/libjpeg.a
unix: PRE_TARGETDEPS += $$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/obj/local/armeabi-v7a/libft2.a
unix: LIBS += -L$$PWD/../../../../../Android/OpenSceneGraph-3.5.0/install/lib -losgViewer -losgGA -losgDB -losgText -losgUtil -losgManipulator -losgShadow -losg -lOpenThreads

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../Android/Sdk/ndk-bundle/platforms/android-19/arch-arm/usr/lib/release/ -lGLESv1_CM
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../Android/Sdk/ndk-bundle/platforms/android-19/arch-arm/usr/lib/debug/ -lGLESv1_CM
#else:unix: LIBS += -L$$PWD/../../../../../../Android/Sdk/ndk-bundle/platforms/android-19/arch-arm/usr/lib/ -lGLESv1_CM

#INCLUDEPATH += $$PWD/../../../../../../Android/Sdk/ndk-bundle/platforms/android-19/arch-arm/usr/include
#DEPENDPATH += $$PWD/../../../../../../Android/Sdk/ndk-bundle/platforms/android-19/arch-arm/usr/include

DISTFILES +=

RESOURCES +=
