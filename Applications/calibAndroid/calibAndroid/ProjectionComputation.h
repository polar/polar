#ifndef PROJECTIONCOMPUTATION_H
#define PROJECTIONCOMPUTATION_H

#include <osg/Vec2>
#include <osg/Matrix>

class ProjectionComputation
{
public:
    static osg::Matrix calculate(osg::Vec2 p1, osg::Vec2 p2, osg::Vec2 p3, osg::Vec2 p4, float w, float h, float s);
    static void MatrixInversion(float **A, int order, float **Y);
    static int GetMinor(float **src, float **dest, int row, int col, int order);
    static double CalcDeterminant( float **mat, int order);
};

#endif // PROJECTIONCOMPUTATION_H
