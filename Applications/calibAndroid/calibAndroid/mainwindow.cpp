#include "mainwindow.h"
#include <QEvent>
#include <QApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    grabGesture(Qt::SwipeGesture);
    grabGesture(Qt::PanGesture);
    grabGesture(Qt::PinchGesture);
    this->setAttribute(Qt::WA_AcceptTouchEvents);
    qApp->setAttribute(Qt::AA_SynthesizeMouseForUnhandledTouchEvents, false);
}

MainWindow::~MainWindow()
{

}


bool MainWindow::event(QEvent *event)
{
    switch(event->type())
    {
    case QEvent::Gesture:
    {
        qDebug("gesture\n");
        break;
    }
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonDblClick:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove:
    {
        qDebug("mouse\n");

        break;
    }
    case QEvent::Wheel:
    {
        qDebug("wheel\n");

        break;
    }
    case QEvent::KeyPress:
    case QEvent::KeyRelease:
    {
        qDebug("key\n");

        break;
    }
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
    {
        qDebug("touch\n");
        break;
    }
    default:
        break;
    }
    return true;
}
