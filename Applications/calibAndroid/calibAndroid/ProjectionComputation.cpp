#include "ProjectionComputation.h"
#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Matrix>
#include <QDebug>
#include <cmath>
#include <osg/io_utils>
#include <iostream>


// matrix inversion
// the result is put in Y
void ProjectionComputation::MatrixInversion(float **A, int order, float **Y)
{
    // get the determinant of a
    double det = 1.0/CalcDeterminant(A,order);

    // memory allocation
    float *temp = new float[(order-1)*(order-1)];
    float **minor = new float*[order-1];
    for(int i=0;i<order-1;i++)
        minor[i] = temp+(i*(order-1));

    for(int j=0;j<order;j++)
    {
        for(int i=0;i<order;i++)
        {
            // get the co-factor (matrix) of A(j,i)
            GetMinor(A,minor,j,i,order);
            Y[i][j] = det*CalcDeterminant(minor,order-1);
            if( (i+j)%2 == 1)
                Y[i][j] = -Y[i][j];
        }
    }

    // release memory
    //delete [] minor[0];
    delete [] temp;
    delete [] minor;
}

// calculate the cofactor of element (row,col)
int ProjectionComputation::GetMinor(float **src, float **dest, int row, int col, int order)
{
    // indicate which col and row is being copied to dest
    int colCount=0,rowCount=0;

    for(int i = 0; i < order; i++ )
    {
        if( i != row )
        {
            colCount = 0;
            for(int j = 0; j < order; j++ )
            {
                // when j is not the element
                if( j != col )
                {
                    dest[rowCount][colCount] = src[i][j];
                    colCount++;
                }
            }
            rowCount++;
        }
    }

    return 1;
}

// Calculate the determinant recursively.
double ProjectionComputation::CalcDeterminant( float **mat, int order)
{
    // order must be >= 0
    // stop the recursion when matrix is a single element
    if( order == 1 )
        return mat[0][0];

    // the determinant value
    float det = 0;

    // allocate the cofactor matrix
    float **minor;
    minor = new float*[order-1];
    for(int i=0;i<order-1;i++)
        minor[i] = new float[order-1];

    for(int i = 0; i < order; i++ )
    {
        // get minor of element (0,i)
        GetMinor( mat, minor, 0, i , order);
        // the recusion is here!

        det += (i%2==1?-1.0:1.0) * mat[0][i] * CalcDeterminant(minor,order-1);
        //det += pow( -1.0, i ) * mat[0][i] * CalcDeterminant( minor,order-1 );
    }

    // release memory
    for(int i=0;i<order-1;i++)
        delete [] minor[i];
    delete [] minor;

    return det;
}

osg::Matrix ProjectionComputation::calculate(osg::Vec2 p1, osg::Vec2 p2, osg::Vec2 p3, osg::Vec2 p4, float w, float h, float s)
{
    osg::Vec2 q1 = p1, q2 = p2, q3 = p3, q4 = p4;
    //float h, w;
    //float s;

    osg::Vec3 q1p(q1[0] - w/2.0, q1[1] - h/2.0, 1);
    osg::Vec3 q2p(q2[0] - w/2.0, q2[1] - h/2.0, 1);
    osg::Vec3 q3p(q3[0] - w/2.0, q3[1] - h/2.0, 1);
    osg::Vec3 q4p(q4[0] - w/2.0, q4[1] - h/2.0, 1);

    osg::Vec3 l1 = q1p ^ q2p;
    osg::Vec3 l2 = q4p ^ q3p;
    osg::Vec3 l3 = q2p ^ q3p;
    osg::Vec3 l4 = q1p ^ q4p;

    osg::Vec3 vh = l1 ^ l2;
    osg::Vec3 wh = l3 ^ l4;
    osg::Vec3 V(vh[0]/vh[2], vh[1]/vh[2], 1);
    osg::Vec3 W(wh[0]/wh[2], wh[1]/wh[2], 1);

    if(V[0]*W[0] + V[1]*W[1] > 0.0)
    {
        qDebug()<< "Wrong point values";
        return osg::Matrix::identity();
    }

    float f = sqrt(-(V[0]*W[0] + V[1]*W[1]));

    // intrinsics
    osg::Matrix intrinsics(f, 0, w/2.0, 0,
                           0, f, h/2.0, 0,
                           0, 0, 1, 0,
                           0, 0, 0, 1);


    //QiP
    float qip[] = {q1p[0], q1p[1], q2p[0], q2p[1], q3p[0], q3p[1], q4p[0], q4p[1]};
    float A[][8] = {{0,0,1,0,0,0,0,0},
                   {0,0,0,0,0,1,0,0},
                   {1,0,1,0,0,0,-q2p[0],0},
                   {0,0,0,1,0,1,-q2p[1],0},
                   {1,1,1,0,0,0,-q3p[0],-q3p[0]},
                   {0,0,0,1,1,1,-q3p[1],-q3p[1]},
                   {0,1,1,0,0,0,0,-q4p[0]},
                   {0,0,0,0,1,1,0,-q4p[1]}};

    float* Ap[8];
    for (int i = 0; i < 8; ++i)
        Ap[i] = A[i];
    float** App = Ap;

    float A1[8][8];
    float* A1p[8];
    for (int i = 0; i < 8; ++i)
        A1p[i] = A1[i];
    float** A1pp = A1p;
    
    MatrixInversion(App, 8, A1pp);

    float AdotQ[8];
    for(int i=0; i<8; i++)
    {
        float val = 0;
        for(int j=0; j<8; j++)
        {
            val += A1[i][j] * qip[j];
        }
        AdotQ[i] = val;
    }

    osg::Matrix H(AdotQ[0], AdotQ[1], AdotQ[2], 0,
            AdotQ[3], AdotQ[4], AdotQ[5], 0,
            AdotQ[6], AdotQ[7], 1, 0,
            0, 0, 0, 1);

    // Projection matrix
    osg::Matrix temp(1.0/f, 0, 0, 0,
                     0, 1.0/f, 0, 0,
                     0, 0, 1, 0,
                     0,0,0,1);

    osg::Matrix B(temp * H);
    osg::Vec3 B1(B(0,0), B(1,0), B(2,0));
    osg::Vec3 B2(B(0,1), B(1,1), B(2,1));
    osg::Vec3 B3(B(0,2), B(1,2), B(2,2));

    osg::Vec3 R1(B1 / B1.length());
    osg::Vec3 R1cB2(R1 ^ B2);
    osg::Vec3 R3((R1cB2/R1cB2.length()));
    osg::Vec3 R2 = R3 ^ R1;

    float l = B1.length() / s;
    //float t_calc = B2.length() / l;
    osg::Vec3 T = B3/l;

    osg::Matrix E(R1[0], R2[0], T[0], 0,
            R1[1], R2[1], T[1], 0,
            R1[2], R2[2], T[2], 0,
            0, 0, 0, 1);

    osg::Matrix Proj = intrinsics*E;
    std::cout << Proj << std::endl;
    return Proj;
}
