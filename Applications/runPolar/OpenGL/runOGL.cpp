#include <cv.h>
#include <highgui.h>

#include <osg/Matrixd>
#include <osg/io_utils>

// Standard includes
#include <stdio.h>
#include <string.h>
#include <assert.h>

// OpenGL/Glut includes
#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

// Timing includes
#include <sys/time.h>

#include "../Tracker.h"



#define KEY_ESCAPE 27


double focal = 750.0;
double imgwidth = 640.0;
double imgheight = 480.0;
double znear = 0.1;
double zfar = 1000.0;

osg::Matrixd _intrinsics = osg::Matrixd(focal,0,imgwidth/2.0,0,
                                        0,focal,imgheight/2.0,0,
                                        0,0,1,0,
                                        0,0,0,1);

osg::Matrixd extrinsics = osg::Matrix::identity();

CvCapture* g_Capture;
GLint g_hWindow;

Tracker *tracker;


// Frame size
int frame_width  = 640;
int frame_height = 480;

// current frames per second, slightly smoothed over time
double fps;

// Return current time in seconds
double current_time_in_seconds();
// Initialize glut window
GLvoid init_glut();
// Glut display callback, draws a single rectangle using video buffer as
// texture
GLvoid display();
// Glut reshape callback
GLvoid reshape(GLint w, GLint h);
// Glut keyboard callback
GLvoid key_press (unsigned char key, GLint x, GLint y);
// Glut idle callback, fetches next video frame
GLvoid idle();

float ver[8][3] =
{
    {-5.0,-5.0,5.0},
    {-5.0,5.0,5.0},
    {5.0,5.0,5.0},
    {5.0,-5.0,5.0},
    {-5.0,-5.0,-5.0},
    {-5.0,5.0,-5.0},
    {5.0,5.0,-5.0},
    {5.0,-5.0,-5.0},
};

GLfloat color[8][3] =
{
    {0.0,0.0,0.0},
    {1.0,0.0,0.0},
    {1.0,1.0,0.0},
    {0.0,1.0,0.0},
    {0.0,0.0,1.0},
    {1.0,0.0,1.0},
    {1.0,1.0,1.0},
    {0.0,1.0,1.0},
};

void quad(int a,int b,int c,int d)
{
    glBegin(GL_QUADS);
    glColor3fv(color[a]);
    glVertex3fv(ver[a]);

    glColor3fv(color[b]);
    glVertex3fv(ver[b]);

    glColor3fv(color[c]);
    glVertex3fv(ver[c]);

    glColor3fv(color[d]);
    glVertex3fv(ver[d]);
    glEnd();
}

void colorcube()
{
    quad(0,3,2,1);
    quad(2,3,7,6);
    quad(0,4,7,3);
    quad(1,2,6,5);
    quad(4,5,6,7);
    quad(0,1,5,4);
}



osg::Matrixd cvMat332osgMatrix(const cv::Matx33d &cvmat, const cv::Vec3d &t=cv::Vec3d(0,0,0))
{
    osg::Matrixd ret=osg::Matrixd::identity();
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<3; j++)
            ret(j,i) = cvmat(i,j); // apply transpose here
        ret(3,i) = t(i);
    }


    return ret;
}


void updateProj()
{

    // no need to transpose: the functions does it
    osg::Matrixd H = cvMat332osgMatrix(tracker->homography());
    // left multiply by the inverse of the intrinsics
#if(VERBOSE)
    std::cout << "TOTO\n";
    std::cout << H << std::endl;
#endif
    H.postMult(osg::Matrixd::inverse(_intrinsics));
#if(VERBOSE)
    std::cout << "INTR\n";
    std::cout << _intrinsics << std::endl;
    std::cout << "INVERSE\n";
    std::cout << osg::Matrixd::inverse(_intrinsics) << std::endl;
#endif
    // decompose the Homography into extrinsics
    osg::Vec3d R1(H(0,0),H(0,1),H(0,2));
    osg::Vec3d R2(H(1,0),H(1,1),H(1,2));
    osg::Vec3d T(H(2,0),H(2,1),H(2,2));

    T /= R1.length();
#if(VERBOSE)
    std::cout << "Lengths\n";
    std::cout << R1.length() << std::endl;
    std::cout << R2.length() << std::endl;
#endif
    R1.normalize();
    R2.normalize();
#if(VERBOSE)
    std::cout << "pscal\n";
    std::cout << R1*R2 << std::endl;
    std::cout << "R1\n";
    std::cout << R1 << std::endl;
    std::cout << "R2\n";
    std::cout << R2 << std::endl;
#endif
    osg::Vec3d R3=R1^R2;
    R3.normalize();
    R2=R3^R1;
#if(VERBOSE)
    std::cout << "R22\n";
    std::cout << R2 << std::endl;
#endif
    osg::Matrixd A(0,1,0,0,
                   1,0,0,0,
                   0,0,-1,0,
                   0,0,0,1);

    osg::Matrixd osgExtr=osg::Matrixd::identity();
    for(unsigned int i=0; i<3; i++)
    {
        osgExtr(0,i) = R1[i];
        osgExtr(1,i) = R2[i];
        osgExtr(2,i) = R3[i];
        osgExtr(3,i) = T[i];
    }

    // premultiply by A to have the correct orientation for the world coordinate frame
    //setPose(A*osgExtr);
    //setProjection(A*osgExtr*_intrinsics);
    //osg::Matrixd proj = A*osgExtr*_intrinsics;
    extrinsics = A*osgExtr;
    osg::Matrixd invMat(1,0,0,0,
                        0,-1,0,0,
                        0,0,-1,0,
                        0,0,0,1);
    extrinsics *= invMat;
}



double current_time_in_seconds()
{
    timeval timer;
    gettimeofday(&timer,NULL);
    double seconds = 1e-6 * timer.tv_usec + timer.tv_sec;
    return seconds;
}


GLvoid init_glut()
{

    glClearColor (0.0, 0.0, 0.0, 0.0);

    // Set up callbacks
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(key_press);
    glutIdleFunc(idle);
}


GLvoid display(void)
{
    //glClearColor( 0, 0, 0, 1 );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    // These are necessary if using glTexImage2D instead of gluBuild2DMipmaps
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    // Set Projection Matrix
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, frame_width, frame_height, 0);

    // Switch to Model View Matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Draw a textured quad
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
    glTexCoord2f(1.0f, 0.0f); glVertex2f(frame_width, 0.0f);
    glTexCoord2f(1.0f, 1.0f); glVertex2f(frame_width, frame_height);
    glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, frame_height);
    glEnd();

    glDisable(GL_TEXTURE_2D);

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
 //   int w = glutGet( GLUT_WINDOW_WIDTH );
 //   int h = glutGet( GLUT_WINDOW_HEIGHT );
 //   gluPerspective( 60, w / h, 0.1, 100 );

    GLdouble perspMatrix[16]={2*focal/imgwidth,0,0,0,
                              0,2*focal/imgheight,0,0,
                              0,0,(zfar+znear)/(zfar-znear),1,
                              0,0,2*zfar*znear/(znear-zfar),0};
    glMultMatrixd(perspMatrix);

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
  /*  gluLookAt(5, 5, 5,
              0, 0, 0,
              0, 0, 1
              );*/

    osg::Vec3d eye, center, up;
    extrinsics.getLookAt(eye, center, up);
    gluLookAt(eye[0], eye[1], eye[2],
              center[0], center[1], center[2],
              up[0], up[1], up[2]);

    colorcube();
    glFlush();

    glutSwapBuffers();
}


GLvoid reshape(GLint w, GLint h)
{
    glViewport(0, 0, w, h);
}

GLvoid key_press(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'f':
        printf("fps: %g\n",fps);
        break;

    case KEY_ESCAPE:
        cvReleaseCapture(&g_Capture);
        glutDestroyWindow(g_hWindow);
        delete tracker;
        exit(0);
        break;
    }
    glutPostRedisplay();
}


GLvoid idle()
{
    // start timer
    double start_seconds = current_time_in_seconds();

    // Capture next frame, this will almost always be the limiting factor in the
    // framerate, my webcam only gets ~15 fps
    IplImage * image = cvQueryFrame(g_Capture);

    cv::Mat img = cv::cvarrToMat(image);
    if(tracker->updateH(img))
    {
        updateProj();
        //cv::Matx33d homo = tracker->homography();
        // std::cout << "\n" <<homo << "\n" << std::endl;
    }


    // Image is memory aligned which means we there may be extra space at the end
    // of each row. gluBuild2DMipmaps needs contiguous data, so we buffer it here
    char * buffer = new char[image->width*image->height*image->nChannels];
    int step     = image->widthStep;
    int height   = image->height;
    int width    = image->width;
    int channels = image->nChannels;
    char * data  = (char *)image->imageData;
    // memcpy version below seems slightly faster
    //for(int i=0;i&lt;height;i++)
    //for(int j=0;j&lt;width;j++)
    //for(int k=0;k&lt;channels;k++)
    //{
    //  buffer[i*width*channels+j*channels+k] = data[i*step+j*channels+k];
    //}
    for(int i=0;i<height;i++)
    {
        memcpy(&buffer[i*width*channels],&(data[i*step]),width*channels);
    }

    // Create Texture
    glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RGB,
                image->width,
                image->height,
                0,
                GL_BGR,
                GL_UNSIGNED_BYTE,
                buffer);


    // Clean up buffer
    delete[] buffer;

    // Update display
    glutPostRedisplay();

    double stop_seconds = current_time_in_seconds();
    fps = 0.9*fps + 0.1*1.0/(stop_seconds-start_seconds);
}




int main(int argc, char* argv[])
{
    char ref_img[] = "/home/pierrejean/Documents/PoLAR/polar/Examples/data/metaio_A4.png";
    double wmilli = 210.0;
    double hmilli = 297.0;
    cv::Mat image = cv::imread(ref_img, CV_LOAD_IMAGE_COLOR);

    tracker = new Tracker(image, wmilli, hmilli);
    // Create OpenCV camera capture
    // If multiple cameras are installed, this takes "first" one
    g_Capture = cvCaptureFromCAM(0);
    assert(g_Capture);
    // capture properties
    frame_height = (int)cvGetCaptureProperty(g_Capture, CV_CAP_PROP_FRAME_HEIGHT);
    frame_width  = (int)cvGetCaptureProperty(g_Capture, CV_CAP_PROP_FRAME_WIDTH);

    // Create GLUT Window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(frame_width, frame_height);

    g_hWindow = glutCreateWindow("Video Texture");

    // Initialize OpenGL
    init_glut();

    glutMainLoop();

    return 0;
}
