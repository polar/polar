#ifndef POSEVIEWER_H
#define POSEVIEWER_H

#include <PoLAR/Viewer.h>

class PoseViewer: public PoLAR::Viewer
{
  Q_OBJECT
  
public:
  PoseViewer(QWidget *parent=0, const char *name=0, Qt::WindowFlags f=0);
  
  void setImage(PoLAR::BaseImage *ima, double w_milli, double h_milli);

  PoLAR::Object3D* getImageNode() { return _obj.get(); }
  
signals:
  void shown();

public slots:
  
protected:
  void showEvent(QShowEvent *event);

  osg::ref_ptr<PoLAR::Object3D> _obj;
  double _w,_h;
};

#endif // POSEVIEWER_H
