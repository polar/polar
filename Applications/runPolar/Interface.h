#ifndef INTERFACE_H
#define INTERFACE_H

#include <QtCore/QStringList>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QInputDialog>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QComboBox>

#include <osgGA/TrackballManipulator>
#include <osgManipulator/Dragger>

#include "PoseViewer.h"
#include "VideoViewer.h"
#include "LoadImageDialog.h"

#include <PoLAR/SceneGraph.h>
#include <PoLAR/SceneGraphManipulator.h>
#include <PoLAR/TabBoxPickHandler.h>
#include <PoLAR/TrackBallPickHandler.h>
#include <PoLAR/TranslateAxisPickHandler.h>
#include <PoLAR/ScaleAxisPickHandler.h>
#include <PoLAR/RotateSpherePickHandler.h>
#include <PoLAR/DefaultPickHandler.h>
#include <PoLAR/VertexDragger.h>
#include <PoLAR/Object3D.h>
#include <PoLAR/Light.h>
#include <PoLAR/Image.h>

#define APPLY_CULL_MASK 0

class Interface : public QMainWindow
{
    Q_OBJECT

public:
    Interface(QString &configFile);

    ~Interface();

    inline PoseViewer* getViewer() {return _poseViewer.get();}

    static bool fileExists(QString path)
    {
        QFileInfo checkFile(path);
        if (checkFile.exists() && checkFile.isFile())
        {
            return true;
        } else
        {
            return false;
        }
    }


signals:
    void startEditImageSignal();
    void startEditMarkersSignal();


private slots:

    void exit();
    void readConfigFile();
    void getImageData(double w_milli, double h_milli);
    void loadObject();
    void manipulateObject();
    void draggerComboSlot(int);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    void stopManipulateObject();
    void resetObjectManipulation();

    void scaleChanged(double scale);

    void takeScreenshot();

    inline void removeCurrentObject3DPickHandler()
    {
        osgViewer::View::EventHandlers::iterator it = std::find(_poseViewer->getEventHandlers().begin(), _poseViewer->getEventHandlers().end(), _pick);
        if (it != _poseViewer->getEventHandlers().end()) _poseViewer->getEventHandlers().erase(it);
    }
    // choose tabbox dragger for object3D edition
    inline void tabboxDragger()
    {
        _dragger = new osgManipulator::TabBoxDragger;
    }

    // choose trackball dragger for object3D edition
    inline void trackballDragger()
    {
        _dragger = new osgManipulator::TrackballDragger;
    }

    // choose translateaxis dragger for object3D edition
    inline void translateaxisDragger()
    {
        _dragger = new osgManipulator::TranslateAxisDragger;
    }

    // choose scaleaxis dragger for object3D edition
    inline void scaleaxisDragger()
    {
        _dragger = new osgManipulator::ScaleAxisDragger;
    }

    // choose rotatesphere dragger for object3D edition
    inline void rotatesphereDragger()
    {
        osgManipulator::RotateSphereDragger* d = new osgManipulator::RotateSphereDragger();
        d->setColor(osg::Vec4f(0.0, 1.0, 0.0, 0.2));
        d->setPickColor(osg::Vec4f(1.0, 1.0, 0.0, 0.2));
        d->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
        d->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        _dragger = d;
    }

    inline void vertexDragger()
    {
        //_dragger = new PoLAR::DefaultVertexDragger;
    }

    void launchVideoViewer();

    void webcamNumChanged(int v)
    {
        _camNum = v;
    }

    void focalChanged(int f)
    {
        _focal = f;
    }

    void videoViewerClosed();

private:

    void createPhantomGround();

    void loadObject(QString file);

    void loadImage(QString file);

    double _h_milli, _w_milli;
    float _focal;
    int _camNum;
    osg::Matrixd _projMat;
    bool _isImageLoaded;
    QString _configFileName;
    osg::ref_ptr<PoLAR::Image<unsigned char> > _refImg;
    osg::ref_ptr<PoseViewer> _poseViewer;
    osg::ref_ptr<PoLAR::Object3D> _object3D;
    osg::Vec3d _originalScale;
    osg::ref_ptr<osgManipulator::Dragger> _dragger;
    osg::ref_ptr<VideoViewer> _videoViewer;
    LoadImageDialog *_imageFileDialog;
    PoLAR::Object3DPickHandler *_pick;
    QToolBar *_toolbar;
    QLabel *_statuslabel;
    QComboBox *_draggerCombo;
    QSpinBox* _camNumBox;
    QSpinBox* _focalBox;
    QDoubleSpinBox* _scaleBox;
    QMessageBox *_mb;
    osg::Matrixd _K;
};



#endif // INTERFACE_H
