#include "VideoViewer.h"
#include <PoLAR/Util.h>
#include <PoLAR/FrameAxis.h>
#include <PoLAR/DrawableArrows2D.h>
#include <QString>
#include <osg/Matrixd>

const cv::Mat image2OpenCVMatrix(PoLAR::Image<unsigned char> *img)
{
    int type=0;
    if(img->getDepth() == 1) type = CV_8UC1;
    else if(img->getDepth() == 3) type = CV_8UC3;
    else if(img->getDepth() == 4) type = CV_8UC4;

    return cv::Mat(img->getHeight(), img->getWidth(), type, img->data());
}

osg::Matrixd cvMat332osgMatrix(const cv::Matx33d &cvmat, const cv::Vec3d &t=cv::Vec3d(0,0,0))
{
    osg::Matrixd ret=osg::Matrixd::identity();
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<3; j++)
            ret(j,i) = cvmat(i,j); // apply transpose here
        ret(3,i) = t(i);
    }


    return ret;
}

cv::Matx33d osgMatrix2cvMat33(const osg::Matrixd &mat)
{
    cv::Matx33d ret(cv::Mat(cv::Mat::eye(3,3,CV_32F)));
    for(unsigned int i=0; i<3; i++)
        for(unsigned int j=0; j<3; j++)
            ret(j,i) = mat(i,j); // apply transpose here

    return ret;
}

VideoViewer::VideoViewer(PoLAR::Image<unsigned char> *ref_img,
                         const double wmilli,
                         const double hmilli,
                         const osg::Matrixd &K,
                         int camNum,
                         float focal):
    PoLAR::Viewer(0, 0, 0, true),
    _tracker(image2OpenCVMatrix(ref_img), wmilli, hmilli),
    _intrinsics(K),
    _markersOn(false),
    _firstFrame(true)
{
    _videoPlayer = new PoLAR::VideoPlayer(camNum);
#if defined(WIN32)
    _videoPlayer->mirrored();
#endif

    osg::ref_ptr<PoLAR::Image<unsigned char> > img = new PoLAR::Image<unsigned char>(_videoPlayer.get());
    setBgImage(img.get());

    //    _markers = new DrawableMarkers2D(this);
    //    addObject2D(_markers);

    //double f=750; // estimated at 753.6
    _intrinsics = osg::Matrixd(focal,0,img->getWidth()/2.0,0,
                               0,focal,img->getHeight()/2.0,0,
                               0,0,1,0,
                               0,0,0,1);
    _intrinsics = PoLAR::Util::transpose(_intrinsics);

    center();
    setResizeOnResolutionChanged();
    addEventHandler(new osgViewer::StatsHandler);
    if(_markersOn) createMarkers();

#if(USE_HOMOGRAPHY)
    QObject::connect(_videoPlayer, SIGNAL(newFrame(unsigned char*,int,int,int)), this, SLOT(updateMatrixH(unsigned char*,int,int,int)));
#else
    QObject::connect(_videoPlayer, SIGNAL(newFrame(unsigned char*,int,int,int)), this, SLOT(updateMatrixExtr(unsigned char*,int,int,int)));
#endif
}



void VideoViewer::updateMatrixH(unsigned char *data, int w, int h, int)
{

    if(_tracker.updateH(cv::Mat(h,w,CV_8UC3,data)))
    {
        if(_firstFrame)
        {
            _intrinsics(2, 0) = w/2.0;
            _intrinsics(2, 1) = h/2.0;
        }

        // no need to transpose: the functions does it
        osg::Matrixd H = cvMat332osgMatrix(_tracker.homography());
        // left multiply by the inverse of the intrinsics
#if(VERBOSE)
        std::cout << "TOTO\n";
        std::cout << H << std::endl;
#endif
        H.postMult(osg::Matrixd::inverse(_intrinsics));
#if(VERBOSE)
        std::cout << "INTR\n";
        std::cout << _intrinsics << std::endl;
        std::cout << "INVERSE\n";
        std::cout << osg::Matrixd::inverse(_intrinsics) << std::endl;
#endif
        // decompose the Homography into extrinsics
        osg::Vec3d R1(H(0,0),H(0,1),H(0,2));
        osg::Vec3d R2(H(1,0),H(1,1),H(1,2));
        osg::Vec3d T(H(2,0),H(2,1),H(2,2));

        T /= R1.length();
#if(VERBOSE)
        std::cout << "Lengths\n";
        std::cout << R1.length() << std::endl;
        std::cout << R2.length() << std::endl;
#endif
        R1.normalize();
        R2.normalize();
#if(VERBOSE)
        std::cout << "pscal\n";
        std::cout << R1*R2 << std::endl;
        std::cout << "R1\n";
        std::cout << R1 << std::endl;
        std::cout << "R2\n";
        std::cout << R2 << std::endl;
#endif
        osg::Vec3d R3=R1^R2;
        R3.normalize();
        R2=R3^R1;
#if(VERBOSE)
        std::cout << "R22\n";
        std::cout << R2 << std::endl;
#endif
        osg::Matrixd A(0,1,0,0,
                       1,0,0,0,
                       0,0,-1,0,
                       0,0,0,1);

        osg::Matrixd osgExtr=osg::Matrixd::identity();
        for(unsigned int i=0; i<3; i++)
        {
            osgExtr(0,i) = R1[i];
            osgExtr(1,i) = R2[i];
            osgExtr(2,i) = R3[i];
            osgExtr(3,i) = T[i];
        }

        if(_firstFrame)
        {
            // premultiply by A to have the correct orientation for the world coordinate frame
            setProjection(A*osgExtr*_intrinsics);
            _firstFrame = false;
        }
        else
        {
            setPose(A*osgExtr);
        }

        if(_markersOn)
        {
            PoLAR::DrawableArrows2D *co = (PoLAR::DrawableArrows2D *)this->getObject2DByIndex(0);
            co->getObject()->setMarkers(_tracker.getMarkers());
        }
    }
}

void VideoViewer::updateMatrixExtr(unsigned char *data, int w, int h, int)
{
    if(_tracker.updateExtr(cv::Mat(h,w,CV_8UC3,data)))
    {
        // no need to transpose: the functions does it
        cv::Matx33d R;
        cv::Vec3d T;
        _tracker.getExtr(R,T);

        osg::Matrixd A(0,1,0,0,
                       1,0,0,0,
                       0,0,-1,0,
                       0,0,0,1);

        osg::Matrixd osgExtr=cvMat332osgMatrix(R,T);
        std::cout << osgExtr << std::endl;

        // premultiply by A to have the correct orientation for the world coordinate frame
        setProjection(A*osgExtr*_intrinsics);
        //setPose(osgExtr);

        if(_markersOn)
        {
            PoLAR::DrawableArrows2D *co = (PoLAR::DrawableArrows2D *)this->getObject2DByIndex(0);
            co->getObject()->setMarkers(_tracker.getMarkers());
        }
    }
}


void VideoViewer::createMarkers()
{
    if(getNumObjects2D() != 0)
    {
        this->removeAllObjects2D();
    }
    PoLAR::DrawableArrows2D *co = new PoLAR::DrawableArrows2D(_tracker.getMarkers(),this);
    co->setUnselectColor(QColor("green"));
    co->setUnselectShape(PoLAR::DrawableObject2D::POINT);
    co->setArrowShape(true);
    co->setArrowSize(3);
    addObject2D(co);
}

void VideoViewer::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Q:
    {
        close();
        //hide();
        break;
    }
    case Qt::Key_W:
    {
        startManipulateSceneSlot();
        break;
    }
    case Qt::Key_I:
    {
        startEditImageSlot();
        break;
    }
    case Qt::Key_B:
    {
#if(USE_HOMOGRAPHY)
        updateMatrixH(NULL,0,0,0);
#else
        updateMatrixExtr(NULL,0,0,0);
#endif
        break;
    }
    case Qt::Key_P:
    {
        osg::Matrixd P = getProjection();
        std::cout << P << std::endl;
        break;
    }
    case Qt::Key_M:
    {
        toggleMarkers();
        break;
    }
    case Qt::Key_G:
    {
        /* QString fname=QFileDialog::getSaveFileName( this,
                                                    "Choose a filename to save the snapshot image",
                                                    QString::null,
                                                    "SVG images (*.svg)");
        if (!fname.isEmpty())*/
        QString fname("screenshot.svg");
        saveAsSvg(fname);

        break;
    }

    default:
        PoLAR::Viewer::keyPressEvent(event);
        break;
    }
}

void VideoViewer::toggleMarkers()
{
    if(_markersOn)
        activateMarkers(false);
    else
        activateMarkers(true);
}

void VideoViewer::activateMarkers(bool b)
{
    _markersOn = b;
    _tracker.activateMarkers(b);
    if(_markersOn) createMarkers();
    else removeAllObjects2D();

}

void VideoViewer::closeEvent(QCloseEvent *event)
{
    if(_videoPlayer.valid())
    {
        _videoPlayer->getQCamera()->unlock();
        _videoPlayer->getQCamera()->unload();
        _videoPlayer->stop();
    }
    PoLAR::Viewer::closeEvent(event);
    emit closed();
}
