#include <QtWidgets/QApplication>
#include "Interface.h"

int main(int argc,char ** argv)
{
    QApplication app(argc, argv);
    QString configFile("config.json");
    if(argc > 1)
        configFile = argv[1];
    Interface inter(configFile);
    inter.show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );

    return app.exec();
}
