#ifndef VIDEOVIEWER_H
#define VIDEOVIEWER_H

#include "Tracker.h"
#include <PoLAR/Viewer.h>
#include <PoLAR/VideoPlayer.h>
#include <PoLAR/Image.h>
#include <PoLAR/DrawableMarkers2D.h>


class VideoViewer: public PoLAR::Viewer
{
    Q_OBJECT

public:
    VideoViewer(PoLAR::Image<unsigned char> *ref_img,
                const double wmilli,
                const double hmilli,
                const osg::Matrixd &K,
                int camNum, float focal);


    void play()
    {
        if(_videoPlayer.valid())
            _videoPlayer->play();
    }

	QCamera* getQCamera()
	{
		if (_videoPlayer.valid())
			return _videoPlayer->getQCamera();
		return NULL;
	}

    void activateMarkers(bool b=true);

    void toggleMarkers();

public slots:
    virtual void keyPressEvent(QKeyEvent *event);
    void updateMatrixH(unsigned char* data, int w, int h, int);
    void updateMatrixExtr(unsigned char* data, int w, int h, int);

signals:
    void closed();

protected:
    void closeEvent(QCloseEvent *event);
    void createMarkers();

private:
    osg::ref_ptr<PoLAR::VideoPlayer> _videoPlayer;
    Tracker _tracker;
    osg::Matrixd _intrinsics;
    PoLAR::DrawableMarkers2D* _markers;
    bool _markersOn;
    bool _firstFrame;
};


#endif // VIDEOVIEWER_H
