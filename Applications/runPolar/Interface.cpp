#include "Interface.h"
#include <PoLAR/Util.h>
#include "../../Examples/props.h"
#include <PoLAR/DraggerManipulationHandler.h>
#include <osgDB/ReadFile>
#include <osgUtil/SmoothingVisitor>
#include <osgUtil/Optimizer>
#include <osg/io_utils>
#include <osgManipulator/TabBoxDragger>
#include <osgViewer/ViewerEventHandlers>
#include <QColorDialog>
#include <QDebug>


using namespace PoLAR;

Interface::Interface(QString& configFile):
    QMainWindow(),
    _h_milli(210.0),
    _w_milli(297.0),
    _focal(750.0),
    _camNum(0),
    _isImageLoaded(false),
    _configFileName(configFile)
{
    //showMaximized();
    resize(1200, 900);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));
    QString dir = dataPath() + "icons/";

    // Create a Viewer with shadows management
    _poseViewer = new PoseViewer(this, "viewer", 0);
    setCentralWidget(_poseViewer.get());
#if APPLY_CULL_MASK
    _poseViewer->getOsgViewer()->getCamera()->setCullMask(0x8);
#endif
    _poseViewer->addEventHandler(new osgViewer::StatsHandler);

    _pick = new PoLAR::TabBoxPickHandler(false); // create pick handler
    _poseViewer->addEventHandler(new DraggerManipulationHandler());

    QObject::connect(_poseViewer.get(), SIGNAL(shown()), this, SLOT(readConfigFile()), Qt::QueuedConnection);

    _imageFileDialog = new LoadImageDialog;
    _imageFileDialog->setOption(QFileDialog::DontUseNativeDialog);
    QObject::connect(_imageFileDialog, SIGNAL(closed(double, double)), this, SLOT(getImageData(double, double)));

    _toolbar = new QToolBar(this);
    addToolBar(Qt::LeftToolBarArea, _toolbar);
    _toolbar->setOrientation(Qt::Vertical);

    _statuslabel = new QLabel(_toolbar);
    _statuslabel->setText(*(new QString("Tools")));

    _toolbar->addAction(QIcon(dir+"exit.png"), QString("Quit"), this, SLOT(exit()));

    QAction *loadImgAct = new QAction(QIcon(dir+"img.png"), tr("Load reference image"), this);
    loadImgAct->setStatusTip(tr("Load reference image"));
    QObject::connect(loadImgAct, SIGNAL(triggered()), _imageFileDialog, SLOT(show()));
    _toolbar->addAction(loadImgAct);

    _toolbar->addAction(QIcon(dir+"object.png"), QString("Add 3D object"), this, SLOT(loadObject()));

    _toolbar->addAction(QIcon(dir+"scene.png"), QString("Manipulate 3D object"), this, SLOT(manipulateObject()));
    // Combo Box to permit to choose the type of dragger to use for object edition
    _draggerCombo = new QComboBox(this);
    _draggerCombo->addItem("TabBox dragger");
    _draggerCombo->addItem("Trackball dragger");
    _draggerCombo->addItem("TranslateAxis dragger");
    _draggerCombo->addItem("ScaleAxis dragger");
    _draggerCombo->addItem("RotateSphere dragger");
    //_draggerCombo->addItem("Vertex dragger");
    _toolbar->addWidget(_draggerCombo);

    QObject::connect(_draggerCombo, SIGNAL(activated(int)), this, SLOT(draggerComboSlot(int)));

    _dragger = new osgManipulator::TabBoxDragger;

    QPushButton *stopButton = new QPushButton(QString("Stop manipulate object"), this);
    _toolbar->addWidget(stopButton);
    QObject::connect(stopButton, SIGNAL(pressed()), this, SLOT(stopManipulateObject()));
    QPushButton *resetButton = new QPushButton(QString("Reset object manipulation"), this);
    _toolbar->addWidget(resetButton);
    QObject::connect(resetButton, SIGNAL(pressed()), this, SLOT(resetObjectManipulation()));

    QLabel* labelW = new QLabel(_toolbar);
    labelW->setText(*(new QString("Webcam #:")));
    _toolbar->addWidget(labelW);
    _camNumBox = new QSpinBox(_toolbar);
    _camNumBox->setRange(0,19);
    _camNumBox->setSingleStep(1);
    _camNumBox->setValue(_camNum);
    _toolbar->addWidget(_camNumBox);
    QObject::connect(_camNumBox, SIGNAL(valueChanged(int)), this, SLOT(webcamNumChanged(int)));

    QLabel* labelF = new QLabel(_toolbar);
    labelF->setText(*(new QString("Focal:")));
    _toolbar->addWidget(labelF);
    _focalBox = new QSpinBox(_toolbar);
    _focalBox->setRange(0,5000);
    _focalBox->setSingleStep(1);
    _focalBox->setValue(_focal);
    _toolbar->addWidget(_focalBox);
    QObject::connect(_focalBox, SIGNAL(valueChanged(int)), this, SLOT(focalChanged(int)));

    QLabel* labelScale = new QLabel(_toolbar);
    labelScale->setText(QString("Model scale:"));
    _toolbar->addWidget(labelScale);
    _scaleBox = new QDoubleSpinBox(_toolbar);
    _scaleBox->setRange(0.001, 1000.0);
    _scaleBox->setSingleStep(0.001);
    _scaleBox->setValue(1.0);
    _toolbar->addWidget(_scaleBox);
    QObject::connect(_scaleBox, SIGNAL(valueChanged(double)), this, SLOT(scaleChanged(double)));

    QPushButton *goButton = new QPushButton(QString("Launch video"), this);
    _toolbar->addWidget(goButton);
    QObject::connect(goButton, SIGNAL(pressed()), this, SLOT(launchVideoViewer()));

    QPushButton *screenshotButton = new QPushButton(QString("Take screenshot"), this);
    _toolbar->addWidget(screenshotButton);
    QObject::connect(screenshotButton, SIGNAL(pressed()), this, SLOT(takeScreenshot()));

    // add focus to this interface in order to get the keyboard events
    this->setFocusPolicy(Qt::StrongFocus);
    this->setFocus();
    // remove the focus from the Viewer, otherwise it will bypass the interface's events
    _poseViewer->setFocusPolicy(Qt::NoFocus);

    _mb = new QMessageBox(
                QMessageBox::Information,
                QString("Help: shortkeys"),
                QString("Shortkeys:\n"),
                QMessageBox::NoButton,
                this);
    _mb->setWindowModality(Qt::NonModal);
}

Interface::~Interface()
{
    delete _imageFileDialog;
    delete _pick;
}


void Interface::readConfigFile()
{
    //read JSON config file
    QFile configFile(_configFileName);
    if (!configFile.open(QIODevice::ReadOnly))
    {
        qWarning() << "Could not open config file" << _configFileName;
        return;
    }

    QByteArray saveData = configFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonObject jsonObj(loadDoc.object());
    double focal = jsonObj["focal"].toDouble();
    QString image = jsonObj["pattern"].toString();
    double h_milli = jsonObj["height_mm"].toDouble();
    double w_milli = jsonObj["width_mm"].toDouble();
    QString model = jsonObj["model"].toString();
    double camNum = jsonObj["webcam"].toDouble();

    _camNum = camNum;
    _camNumBox->setValue(_camNum);

    if(focal > 0.0)
    {
        _focal = focal;
        _focalBox->setValue(_focal);
    }
    if(fileExists(image))
    {
        if(h_milli > 0.0)
            _h_milli = h_milli;
        if(w_milli > 0.0)
            _w_milli = w_milli;
        loadImage(image);
    }
    else
        if(image.isEmpty())
            qWarning() << "config.json does not contain pattern info";
        else
            qWarning() << image << ": file not found";
    if(fileExists(model))
        loadObject(model);
    else
        if(model.isEmpty())
            qWarning() << "config.json does not contain 3D model info";
        else
            qWarning() << model << ": file not found";
}


void Interface::getImageData(double w_milli, double h_milli)
{
    _w_milli = w_milli;
    _h_milli = h_milli;
    QString fname = _imageFileDialog->selectedFiles().at(0);
    if(!fname.isEmpty())
    {
        loadImage(fname);
    }
}


void Interface::loadImage(QString file)
{
    _refImg = new PoLAR::Image<unsigned char>(file.toStdString(), true);
    _poseViewer->setImage(_refImg.get(), _w_milli, _h_milli);
    _isImageLoaded = true;
    osg::Matrixd projMat = _poseViewer->createVisionProjection(_poseViewer->width(), _poseViewer->height());
    _poseViewer->setProjection(projMat);

    osg::Matrixd P(1, 0, 0, 0,
                   0, -1, 0, 0,
                   0, 0, -1, 0,
                   -_h_milli/2.0, _w_milli/2.0, projMat(3,2), 1);
    _poseViewer->setPose(P);
    _poseViewer->startManipulateSceneSlot();
}


void Interface::loadObject()
{
    QString fname = QFileDialog::getOpenFileName( this,
                                                  "Load a 3D model",
                                                  QString::null);
    if(!fname.isEmpty())
    {
        loadObject(fname);
    }
}


void Interface::loadObject(QString file)
{
    osg::ref_ptr<osg::Node> loadedModel;
    loadedModel = osgDB::readNodeFile(file.toStdString());
    if(loadedModel)
    {
        osgUtil::Optimizer optimizer;
        osgUtil::SmoothingVisitor smooth;
        // Texture optimization prevents textures to be shared between different viewers
        optimizer.optimize(loadedModel.get(), osgUtil::Optimizer::ALL_OPTIMIZATIONS&~osgUtil::Optimizer::OPTIMIZE_TEXTURE_SETTINGS);
        loadedModel->accept(smooth);

        // create a pickable and editable PoLAR::Object3D
        _object3D = new PoLAR::Object3D(loadedModel.get(), true, true, true);
        osg::Matrixd mat = osg::Matrixd::scale(100.,100.,100.);
        _object3D->setTransformationMatrix(mat);
        _object3D->setName("loaded Model");
        if(_refImg.valid() && _refImg->valid())
        {
            double gamma = _h_milli/_refImg->getHeight();
            _object3D->translate(gamma * _refImg->getHeight()/2.0, gamma * _refImg->getWidth()/2.0, 0);
        }
        _object3D->setFrameMatrix(osg::Matrix::identity());
        _poseViewer->addObject3D(_object3D.get());
        _originalScale = _object3D->getScale();
        // createPhantomGround();
    }
    else
        qWarning() << "Failed loading model";
}


void Interface::createPhantomGround()
{
    osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode;
    osg::ref_ptr<osg::ShapeDrawable> shape;
    shape = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0.5f, 0.5f, 0.0f), 5.0f, 5.0f, 0.001f));
    floorGeode->addDrawable(shape.get());
    PoLAR::Object3D *object3D = new PoLAR::Object3D(floorGeode.get());
    object3D->setName("Phantom");
    if(_videoViewer.get()) _videoViewer->addObject3D(object3D);
    //if(_poseViewer.get()) _poseViewer->addObject3D(object3D);
    object3D->setEditOn();
    object3D->setPhantomOn();
#if APPLY_CULL_MASK
    object3D->setNodeMask(object3D->getNodeMask() & ~0x8);
#endif
    osg::Matrixd mat = _object3D->getTransformationMatrix();
    object3D->setTransformationMatrix(mat);
}


void Interface::manipulateObject()
{
    if(_object3D.valid() && _dragger.valid())
    {
        _object3D->removeDragger();
        _dragger->setupDefaultGeometry();
        _object3D->addDragger(_dragger.get());
    }
}

void Interface::stopManipulateObject()
{
    if(_object3D.valid() && _dragger.valid())
    {
        _object3D->removeDragger();
    }
}

void Interface::resetObjectManipulation()
{
    if(_object3D.valid())
    {
        _object3D->resetAllManipulations();
        double scale = _scaleBox->value();
        scaleChanged(scale);
    }
}


void Interface::scaleChanged(double scale)
{
    if(_object3D.get() && scale > 0.0)
    {
        osg::Vec3d s = _object3D->getScale();
        osg::Vec3d diff = osg::Vec3d(_originalScale.x()/s.x(), _originalScale.y()/s.y(), _originalScale.z()/s.z());
        _object3D->scale(diff);
        _object3D->scale(scale);
    }
}

void Interface::draggerComboSlot(int index)
{
    switch (index)
    {
    case 0 : tabboxDragger(); break;
    case 1 : trackballDragger(); break;
    case 2 : translateaxisDragger(); break;
    case 3 : scaleaxisDragger(); break;
    case 4 : rotatesphereDragger(); break;
        //case 5 : vertexDragger(); break;
    default : tabboxDragger(); break;
    }
    if(_object3D.valid())
        manipulateObject();
}


void Interface::launchVideoViewer()
{
    // if(!_videoViewer.valid())
    {
        _videoViewer = new VideoViewer(_refImg.get(), _w_milli, _h_milli, _K, _camNum, _focal);
#if APPLY_CULL_MASK
        _videoViewer->getOsgViewer()->getCamera()->setCullMask(0x4);
#endif
    }
    //osg::ref_ptr<osg::Group> scene = _poseViewer->getScene3D();
    //scene->setNodeMask(scene->getNodeMask() | 0x4);
    //_videoViewer->setScene3D(scene.get());
    _videoViewer->addObject3D(_object3D.get());
    _videoViewer->addLightSource(_poseViewer->getLightSource(1));
    createPhantomGround();
    //osg::ref_ptr<osg::Group> objs = _poseViewer->getObjects3DGroup();
    //_videoViewer->setObjects3DGroup(objs.get());
    //_videoViewer->setLightGroup(_poseViewer->getLightGroup());

    _videoViewer->play();
    _videoViewer->show();
    _poseViewer->setIdle();
    QObject::connect(_videoViewer.get(), SIGNAL(closed()), this, SLOT(videoViewerClosed()));
}


void Interface::videoViewerClosed()
{
    if(_poseViewer.valid())
        _poseViewer->setIdle(false);
}

void Interface::exit()
{
    if(_videoViewer.valid() && _videoViewer->isVisible())
        _videoViewer->close();
    close();
}


void Interface::takeScreenshot()
{
    if(_videoViewer.get())
    {
        QString fname("screenshot.svg");
        _videoViewer->saveAsSvg(fname);
    }
}

// Add key bindings
void Interface::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Q:
    {
        this->exit();
        break;
    }
    case Qt::Key_H:
        // Show the help message box
        _mb->show();
        break;
    case Qt::Key_D:
    {
        // Hide/display background image
        _poseViewer->bgImageToggle();
        break;
    }
    case Qt::Key_I:
    {
        // Set the image interaction to be active
        _poseViewer->startEditImageSlot();
        break;
    }
    case Qt::Key_W:
    {
        // Set the scene graph interaction to be active
        _poseViewer->startManipulateSceneSlot();
        break;
    }

    case Qt::Key_P:
    {
        osg::Matrixd p = _poseViewer->getPose();
        std::cout << p << std::endl;
        break;
    }

    case Qt::Key_M:
    {
        osg::Matrixd p = _poseViewer->getPose();
        _poseViewer->setPose(p);
        break;
    }

    case Qt::Key_F1:
    {
        tabboxDragger();
        break;
    }
    case Qt::Key_F2:
    {
        trackballDragger();
        break;
    }
    case Qt::Key_F3:
    {
        translateaxisDragger();
        break;
    }
    case Qt::Key_F4:
    {
        scaleaxisDragger();
        break;
    }
    case Qt::Key_F5:
    {
        rotatesphereDragger();
        break;
    }

    case Qt::Key_G:
    {
        if(_videoViewer.get())
        {
            QString fname=QFileDialog::getSaveFileName( this,
                                                        "Choose a filename to save the snapshot image",
                                                        QString::null,
                                                        "PNG images (*.png)");
            if (!fname.isEmpty()) _videoViewer->grab().save(fname, "PNG");
        }
        break;
    }

    default:
        _poseViewer->notify(event);
    }
}

void Interface::keyReleaseEvent(QKeyEvent *event)
{
    //simply forwarding to the viewer
    _poseViewer->notify(event);
}
