#ifndef TRACKER_H
#define TRACKER_H

#define USE_HOMOGRAPHY 1
#define VERBOSE 0

#include <iostream>
#include <stdio.h>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/xfeatures2d.hpp"

#if(USE_PoLAR)
#include <PoLAR/Markers2D.h>
#endif

struct SURFDetector
{
    cv::Ptr<cv::Feature2D> surf;
    SURFDetector(double hessian = 3000.0) { surf = cv::xfeatures2d::SURF::create(hessian); }
    template<class T>
    void operator()(const T& in,
                    const T& mask,
                    std::vector<cv::KeyPoint>& pts,
                    T& descriptors,
                    bool useProvided = false)
    { surf->detectAndCompute(in, mask, pts, descriptors, useProvided); }
};

template<class KPMatcher>
struct SURFMatcher
{
    KPMatcher matcher;
    template<class T>
    void match(const T& in1,
               const T& in2,
               std::vector<cv::DMatch>& matches)
    { matcher.match(in1, in2, matches); }
};

class Tracker
{

public:
    // ima can be set by: cv::Mat(nrows,ncols,CV_8UC3,imdata);
    Tracker(const cv::Mat& ima,
            const double wmilli,
            const double hmilli,
            const cv::Mat& mask=cv::Mat());

    const cv::Matx33d &homography() const {return H;}
    void getExtr(cv::Matx33d &_R, cv::Vec3d &_T) const {_R=R; _T=T;}
    const cv::Matx33d &getIntr() const {return K;}

    // ima can be set by: cv::Mat(nrows,ncols,CV_8UC3,imdata);
    bool updateH(const cv::Mat& ima, const cv::Mat& mask=cv::Mat());
    bool updateExtr(const cv::Mat& ima, const cv::Mat& mask=cv::Mat());

#if(USE_PoLAR)
    std::list<PoLAR::Marker2D*>& getMarkers() { return _markers;}
    void activateMarkers(bool b=true) { _markersOn = b; if(!_markersOn) _markers.clear(); }
#endif
    void setIntrinsics(const cv::Matx33d &_K) { K=_K; }

private:
    static const int GOOD_PTS_MAX;
    static const float GOOD_PORTION;
    SURFDetector surf;
    SURFMatcher<cv::BFMatcher> matcher;
    std::vector<cv::KeyPoint> keypoints1;
    cv::UMat _descriptors1;
    cv::Mat descriptors1 = _descriptors1.getMat(cv::ACCESS_RW);
    cv::Matx33d H, K;
    cv::Matx33d R;
    cv::Vec3d T;

#if(USE_PoLAR)
    bool _markersOn;
    std::list<PoLAR::Marker2D*> _markers;
#endif
};


#endif // TRACKER_H
