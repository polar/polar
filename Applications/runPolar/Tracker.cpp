#include "Tracker.h"
#include <osg/Vec3d>
#include <osg/io_utils>

const int Tracker::GOOD_PTS_MAX = 500;
const float Tracker::GOOD_PORTION = 1.0f;
const float homogThres=1.0f;
const float extrThres=1.0f;

Tracker::Tracker(const cv::Mat& ima,
                 const double wmilli,
                 const double hmilli,
                 const cv::Mat& mask)
#if(USE_PoLAR)
    :_markersOn(false)
#endif
{
    surf(ima, mask, keypoints1, descriptors1);
    // normalize the keypoint coordinates between 0 and 1
    double wratio = wmilli/ima.cols;
    double hratio = hmilli/ima.rows;
    for( size_t i = 0; i < keypoints1.size(); i++ )
    {
        keypoints1[i].pt.x*=wratio;
        keypoints1[i].pt.y*=hratio;
    }
#if(VERBOSE)
    for( size_t i = 0; i < keypoints1.size(); i++ )
    {
        cv::Point2f p=keypoints1[i].pt;
        std::cout << p.x << " " << p.y << std::endl;
    }
#endif
}


// ima can be set by: cv::Mat(nrows,ncols,CV_8UC3,imdata);
bool Tracker::updateH(const cv::Mat& ima, const cv::Mat& mask)
{
    std::vector<cv::KeyPoint> keypoints2;
    cv::UMat _descriptors2;
    cv::Mat descriptors2 = _descriptors2.getMat(cv::ACCESS_RW);
    surf(ima, mask, keypoints2, descriptors2);
    std::vector<cv::DMatch> matches;
    if(!descriptors1.empty() && !descriptors2.empty())
    {
        matcher.match(descriptors2, descriptors1, matches);
        //-- Sort matches and preserve top 10% matches
        std::sort(matches.begin(), matches.end());
        std::vector< cv::DMatch > good_matches;

        const int ptsPairs = std::min(this->GOOD_PTS_MAX, (int)(matches.size() * this->GOOD_PORTION));
        for( int i = 0; i < ptsPairs; i++ )
            good_matches.push_back( matches[i] );

        if (good_matches.size() >= 4)
        {

            //-- Localize the object
            std::vector<cv::Point2f> obj;
            std::vector<cv::Point2f> scene;
            for(size_t i = 0; i < good_matches.size(); i++ )
            {
                //-- Get the keypoints from the good matches
                obj.push_back(keypoints2[ good_matches[i].queryIdx ].pt );
                scene.push_back(keypoints1[ good_matches[i].trainIdx ].pt);
            }
            //	    this->H = cv::findHomography( scene, obj, cv::RANSAC, homogThres);
            cv::Mat ret = cv::findHomography( scene, obj, cv::LMEDS);
            if(ret.cols == H.cols && ret.rows == H.rows)
            {
                H = ret;
            }
            else return false;

#if(VERBOSE)
            std::cout << good_matches.size() << std::endl;
            std::cout << "Homography\n";
            std::cout << H(0,0) << ' ' << H(0,1) << ' ' << H(0,2) << std::endl;
            std::cout << H(1,0) << ' ' << H(1,1) << ' ' << H(1,2) << std::endl;
            std::cout << H(2,0) << ' ' << H(2,1) << ' ' << H(2,2) << std::endl;
#endif

#if(USE_PoLAR)
            if(_markersOn)
            {
                _markers.clear();
                for(size_t i = 0; i < good_matches.size(); i++ )
                {
                    cv::Point2f p2=keypoints2[ good_matches[i].queryIdx ].pt;
                    cv::Point2f p1=keypoints1[ good_matches[i].trainIdx ].pt;
                    double x=H(0,0)*p1.x+H(0,1)*p1.y+H(0,2);
                    double y=H(1,0)*p1.x+H(1,1)*p1.y+H(1,2);
                    double z=H(2,0)*p1.x+H(2,1)*p1.y+H(2,2);
                    if (fabs(z) > 1e-6)
                    {
                        p1.x=x/z, p1.y=y/z;
                        if (cv::norm(p1-p2) < homogThres)
                        {
                            PoLAR::Marker2D* marker = new PoLAR::Marker2D(p2.x, p2.y);
                            PoLAR::Marker2D* markerRef = new PoLAR::Marker2D(p1.x, p1.y);
                            _markers.push_back(marker);
                            _markers.push_back(markerRef);
                        }
                    }
                }
            }
#endif
            return true;
        }
        else return false;
    }
    return false;
}


// ima can be set by: cv::Mat(nrows,ncols,CV_8UC3,imdata);
bool Tracker::updateExtr(const cv::Mat& ima, const cv::Mat& mask)
{
    std::vector<cv::KeyPoint> keypoints2;
    cv::UMat _descriptors2;
    cv::Mat descriptors2 = _descriptors2.getMat(cv::ACCESS_RW);
    surf(ima, mask, keypoints2, descriptors2);
    std::vector<cv::DMatch> matches;
    if(!descriptors1.empty() && !descriptors2.empty())
    {
        matcher.match(descriptors2, descriptors1, matches);
        //-- Sort matches and preserve top 10% matches
        std::sort(matches.begin(), matches.end());
        std::vector< cv::DMatch > good_matches;

        const int ptsPairs = std::min(this->GOOD_PTS_MAX, (int)(matches.size() * this->GOOD_PORTION));
        for( int i = 0; i < ptsPairs; i++ )
            good_matches.push_back( matches[i] );

        if (good_matches.size() >= 4)
        {

            //-- Localize the object
            std::vector<cv::Point2f> obj;
            std::vector<cv::Point3f> scene;
            cv::Point2f p;
            for(size_t i = 0; i < good_matches.size(); i++ )
            {
                //-- Get the keypoints from the good matches
                obj.push_back(keypoints2[ good_matches[i].queryIdx ].pt );
                p=keypoints1[ good_matches[i].trainIdx ].pt;
                scene.push_back(cv::Point3f(p.x,p.y,0));
            }
            cv::Mat rvec, tvec;
#if 1
            cv::solvePnPRansac(scene, obj, K, cv::noArray(), rvec, tvec, false,
                               500, extrThres, 0.7, cv::noArray(), CV_EPNP);
#else
            cv::gpu::solvePnPRansac(scene, obj, K, cv::noArray(), rvec, tvec, false,
                               500, extrThres, 0.7, cv::noArray(), CV_EPNP);
#endif
            cv::Rodrigues(rvec, R);
            T=tvec;

#if(VERBOSE)
            std::cout << good_matches.size() << std::endl;
            std::cout << "Transfo\n";
            std::cout << R(0,0) << ' ' << R(0,1) << ' ' << R(0,2) << ' ' << T(0) << std::endl;
            std::cout << R(1,0) << ' ' << R(1,1) << ' ' << R(1,2) << ' ' << T(1) << std::endl;
            std::cout << R(2,0) << ' ' << R(2,1) << ' ' << R(2,2) << ' ' << T(2) << std::endl;

            std::cout << "Intrinsics\n";
            std::cout << K(0,0) << ' ' << K(0,1) << ' ' << K(0,2) << std::endl;
            std::cout << K(1,0) << ' ' << K(1,1) << ' ' << K(1,2) << std::endl;
            std::cout << K(2,0) << ' ' << K(2,1) << ' ' << K(2,2) << std::endl;

            H(0,0)=R(0,0), H(0,1)=R(0,1),H(0,2)=T(0);
            H(1,0)=R(1,0), H(1,1)=R(1,1),H(1,2)=T(1);
            H(2,0)=R(2,0), H(2,1)=R(2,1),H(2,2)=T(2);
            H = K*H;

            std::cout << "Homography\n";
            std::cout << H(0,0) << ' ' << H(0,1) << ' ' << H(0,2) << std::endl;
            std::cout << H(1,0) << ' ' << H(1,1) << ' ' << H(1,2) << std::endl;
            std::cout << H(2,0) << ' ' << H(2,1) << ' ' << H(2,2) << std::endl;
#endif
#if(USE_PoLAR)
            if(_markersOn)
            {
                _markers.clear();
                for(size_t i = 0; i < good_matches.size(); i++ )
                {
                    cv::Point2f p2=keypoints2[ good_matches[i].queryIdx ].pt;
                    cv::Point2f p1=keypoints1[ good_matches[i].trainIdx ].pt;
                    double x=H(0,0)*p1.x+H(0,1)*p1.y+H(0,2);
                    double y=H(1,0)*p1.x+H(1,1)*p1.y+H(1,2);
                    double z=H(2,0)*p1.x+H(2,1)*p1.y+H(2,2);
                    if (fabs(z) > 1e-6)
                    {
                        p1.x=x/z, p1.y=y/z;
                        if (cv::norm(p1-p2) < homogThres)
                        {
                            PoLAR::Marker2D* marker = new PoLAR::Marker2D(p2.x, p2.y);
                            PoLAR::Marker2D* markerRef = new PoLAR::Marker2D(p1.x, p1.y);
                            _markers.push_back(marker);
                            _markers.push_back(markerRef);
                        }
                    }
                }
            }
#endif
            return true;

        }
        else return false;
    }
    return false;
}
