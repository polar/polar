#include "PoseViewer.h"
#include "Interface.h"
#include <osg/Plane>
#include <PoLAR/ShaderManager.h>
#include <PoLAR/SceneGraphManipulator.h>
#include <PoLAR/Light.h>

PoseViewer::PoseViewer(QWidget * parent, const char * name, WindowFlags f):
    Viewer(parent, name, f, true), _w(0.0), _h(0.0)
{
    setBackgroundColor(0.6,0.6,0.6);
    PoLAR::Light* light = addLightSource(0.0, 0.0, 5.0, true);
    light->setLightNum(1);
    light->setPosition(0.0,0.0,5.0,0.0);
}

void PoseViewer::setImage(PoLAR::BaseImage *ima, double w_milli, double h_milli)
{
    if (ima)
    {
        if(_obj.get())
        {
            removeObject3D(_obj.get());
        }

        osg::ref_ptr<osg::Geode> geode = new osg::Geode;
        int w = ima->getWidth();
        int h = ima->getHeight();

        float wT = 1.0, hT = 1.0;
        if (geode->getNumDrawables() == 0)
            geode->addDrawable(osg::createTexturedQuadGeometry( osg::Vec3(0,0,0),
                                                                osg::Vec3(0,w_milli,0),
                                                                osg::Vec3(h_milli,0,0),
                                                                wT,hT
                                                                )
                               );
        else
            geode->setDrawable(0,osg::createTexturedQuadGeometry( osg::Vec3(0,0,0),
                                                                  osg::Vec3(0,w_milli,0),
                                                                  osg::Vec3(h_milli,0,0),
                                                                  wT,hT
                                                                  )
                               );
        osg::ref_ptr<osg::Texture2D> t = ima->makeTexture();
        t->setResizeNonPowerOfTwoHint(false);

        osg::ref_ptr<osg::StateSet> stateSet = geode->getOrCreateStateSet();
        t->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR_MIPMAP_LINEAR);
        t->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
        stateSet->setTextureAttributeAndModes(0, t.get(), osg::StateAttribute::ON);
        stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::OVERRIDE);

        _obj = new PoLAR::Object3D(geode.get());
        addObject3D(_obj);
        _obj->setEditOn();
        _obj->setShadowCasterOff();
#if APPLY_CULL_MASK
        _obj->setNodeMask(_obj->getNodeMask() & ~0x4);
#endif
        setTrackNode(geode.get());
        _w = w;
        _h = h;

    }
}

void PoseViewer::showEvent(QShowEvent *event)
{
    PoLAR::Viewer::showEvent(event);
    emit shown();
}
