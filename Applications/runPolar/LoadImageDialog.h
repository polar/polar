#ifndef LOADIMAGEDIALOG_H
#define LOADIMAGEDIALOG_H

#include <QFileDialog>
#include <QDoubleSpinBox>

class LoadImageDialog: public QFileDialog
{
    Q_OBJECT
public:
    LoadImageDialog();
    ~LoadImageDialog();

public slots:
    void accept();

signals:
    void closed(double, double);

private:
    QDoubleSpinBox *_wSpinBox;
    QDoubleSpinBox *_hSpinBox;
};


#endif // LOADIMAGEDIALOG_H
