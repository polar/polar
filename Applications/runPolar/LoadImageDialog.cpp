#include "LoadImageDialog.h"

#include <QDialogButtonBox>
#include <QBoxLayout>
#include <QToolBar>
#include <QLabel>
#include <QHBoxLayout>
#include <iostream>

LoadImageDialog::LoadImageDialog()
{
    QGridLayout* mainLayout = dynamic_cast<QGridLayout*>(layout());
    if(mainLayout)
    {
        QHBoxLayout *hbl = new QHBoxLayout();
        int numRows = mainLayout->rowCount();
        mainLayout->addLayout(hbl, numRows, 1, Qt::AlignLeft);

        QLabel *wLabel = new QLabel("Image width (mm): ");
        _wSpinBox = new QDoubleSpinBox();
        _wSpinBox->setRange(1.0, 1000.0);
        _wSpinBox->setValue(297.0);
        hbl->addWidget(wLabel);
        hbl->addWidget(_wSpinBox);

        QLabel *hLabel = new QLabel("Image height (mm): ");
        _hSpinBox = new QDoubleSpinBox();
        _hSpinBox->setRange(1.0, 1000.0);
        _hSpinBox->setValue(210.0);
        hbl->addWidget(hLabel);
        hbl->addWidget(_hSpinBox);
        hbl->insertSpacing(hbl->count()-2, 100);
    }
}


LoadImageDialog::~LoadImageDialog()
{
    delete _hSpinBox;
}

void LoadImageDialog::accept()
{
    QFileDialog::accept();
    double w_milli = _wSpinBox->value();
    double h_milli = _hSpinBox->value();
    emit closed(w_milli, h_milli);
}
