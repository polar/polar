# - Find a Sofa installation 

# It requires SOFA_ROOT environment variable to be set

# When Sofa is found, the following variables are defined
# - SOFA_INCLUDE_DIR
# - SOFA_LIBRARY_DIR
# - SOFA_LIBRARIES
# - SOFA_FOUND

IF ( EXISTS $ENV{SOFA_ROOT} )
  set( SOFA_ROOT $ENV{SOFA_ROOT} )
ENDIF ( EXISTS $ENV{SOFA_ROOT} )

IF ( NOT SOFA_ROOT )
  SET( SOFA_ROOT "SOFA_ROOT-NOTFOUND" CACHE PATH "Root of Sofa installation or build directory")
  MESSAGE(FATAL_ERROR "SOFA_ROOT not found, please set it")
#  MARK_AS_ADVANCED( SOFA_ROOT )
ENDIF ( NOT SOFA_ROOT )

# INCLUDES
# SOFA MODULES includes
FIND_PATH(SOFA_MODULES_INCLUDE_PATH sofa/simulation/tree/tree.h ${SOFA_ROOT}/modules)
MARK_AS_ADVANCED(SOFA_MODULES_INCLUDE_PATH)
# SOFA FRAMEWORK includes
FIND_PATH(SOFA_FRAMEWORK_INCLUDE_PATH sofa/core/CollisionModel.h ${SOFA_ROOT}/framework)
MARK_AS_ADVANCED(SOFA_FRAMEWORK_INCLUDE_PATH)
# SOFA APPLICATIONS includes
FIND_PATH(SOFA_APPLICATIONS_INCLUDE_PATH  sofa/gui/SofaGUI.h ${SOFA_ROOT}/applications)
MARK_AS_ADVANCED(SOFA_APPLICATIONS_INCLUDE_PATH)

# External libs ###########################################################
# Boost includes
FIND_PATH(BOOST_INCLUDE_PATH boost/array.hpp ${SOFA_ROOT}/extlibs/miniBoost/)
MARK_AS_ADVANCED(BOOST_INCLUDE_PATH)

# tinyxml includes
FIND_PATH(TINYXML_INCLUDE_PATH tinyxml.h ${SOFA_ROOT}/extlibs/tinyxml/)
MARK_AS_ADVANCED(TINYXML_INCLUDE_PATH)

# newmat includes
FIND_PATH(NEWMAT_INCLUDE_PATH newmat/newmat.h ${SOFA_ROOT}/extlibs/newmat/)
MARK_AS_ADVANCED(NEWMAT_INCLUDE_PATH)

# miniFlowVR includes
FIND_PATH(FLOWVR_INCLUDE_PATH flowvr/render/bbox.h ${SOFA_ROOT}/extlibs/miniFlowVR/include)
MARK_AS_ADVANCED(FLOWVR_INCLUDE_PATH)



# LIBRARIES
IF (APPLE)
  FIND_PATH(SOFA_MODULES_LIBRARY_PATH libsofasimulation.dylib ${SOFA_ROOT}/lib/macx)
  FIND_PATH(SOFA_FRAMEWORK_LIBRARY_PATH libsofacore.dylib ${SOFA_ROOT}/lib/macx)
  FIND_PATH(SOFA_APPLICATIONS_LIBRARY_PATH libsofagui.dylib ${SOFA_ROOT}/lib/macx)
ENDIF(APPLE)

IF (WIN32)
  FIND_PATH(SOFA_MODULES_LIBRARY_PATH sofasimulation.lib ${SOFA_ROOT}/lib/win32/ReleaseVC7)
  FIND_PATH(SOFA_FRAMEWORK_LIBRARY_PATH sofacore.lib ${SOFA_ROOT}/lib/win32/ReleaseVC7)
  FIND_PATH(SOFA_APPLICATIONS_LIBRARY_PATH sofagui.lib ${SOFA_ROOT}/lib/win32/ReleaseVC7)
ENDIF(WIN32)

IF (UNIX)
  FIND_PATH(SOFA_MODULES_LIBRARY_PATH libsofasimulation.so ${SOFA_ROOT}/lib/linux)
  FIND_PATH(SOFA_FRAMEWORK_LIBRARY_PATH libsofacore.so ${SOFA_ROOT}/lib/linux)
  FIND_PATH(SOFA_APPLICATIONS_LIBRARY_PATH libsofagui.so ${SOFA_ROOT}/lib/linux)
ENDIF(UNIX)
MARK_AS_ADVANCED(SOFA_MODULES_LIBRARY_PATH)
MARK_AS_ADVANCED(SOFA_FRAMEWORK_LIBRARY_PATH)
MARK_AS_ADVANCED(SOFA_APPLICATIONS_LIBRARY_PATH)


IF ( SOFA_MODULES_INCLUDE_PATH 
    AND SOFA_FRAMEWORK_INCLUDE_PATH
    AND SOFA_APPLICATIONS_INCLUDE_PATH
    AND BOOST_INCLUDE_PATH
    AND TINYXML_INCLUDE_PATH
    AND NEWMAT_INCLUDE_PATH
    AND FLOWVR_INCLUDE_PATH
    AND SOFA_MODULES_LIBRARY_PATH
    AND SOFA_FRAMEWORK_LIBRARY_PATH
    AND SOFA_APPLICATIONS_LIBRARY_PATH )
  SET( SOFA_FOUND "YES" )
  SET( SOFA_INCLUDE_DIR
    ${SOFA_MODULES_INCLUDE_PATH}
    ${SOFA_FRAMEWORK_INCLUDE_PATH}
    ${SOFA_APPLICATIONS_INCLUDE_PATH}
    ${BOOST_INCLUDE_PATH}
    ${TINYXML_INCLUDE_PATH}
    ${NEWMAT_INCLUDE_PATH}
    ${FLOWVR_INCLUDE_PATH} )
  SET(SOFA_LIBRARY_DIR ${SOFA_MODULES_LIBRARY_PATH} ${SOFA_FRAMEWORK_LIBRARY_PATH} ${SOFA_APPLICATIONS_LIBRARY_PATH})
  SET(SOFA_LIBRARIES 
    sofaguiglut sofagui sofaguimain sofacomponentmastersolver
    sofacomponentfem sofacomponentinteractionforcefield
    sofacomponentcontextobject sofacomponentbehaviormodel sofacomponentengine
    sofacomponentlinearsolver sofacomponentodesolver sofacomponentbase
    sofacomponentcontroller sofacomponentvisualmodel sofacomponentmass
    sofacomponentforcefield sofacomponentmapping sofacomponentconstraint
    sofacomponentcollision sofacomponentmisc sofacomponent sofasimulation
    sofatree sofacomponentmastersolver sofacomponentfem
    sofacomponentinteractionforcefield sofacomponentcontextobject
    sofacomponentbehaviormodel sofacomponentengine sofacomponentlinearsolver
    sofacomponentodesolver sofacomponentbase sofacomponentcontroller
    sofacomponentvisualmodel sofacomponentmass sofacomponentforcefield
    sofacomponentmapping sofacomponentconstraint sofacomponentcollision
    sofacomponentmisc sofacomponent sofasimulation sofatree sofahelper
    sofadefaulttype sofacore sofahelper sofadefaulttype sofacore
    )
ENDIF ( SOFA_MODULES_INCLUDE_PATH 
    AND SOFA_FRAMEWORK_INCLUDE_PATH
    AND SOFA_APPLICATIONS_INCLUDE_PATH
    AND BOOST_INCLUDE_PATH
    AND TINYXML_INCLUDE_PATH
    AND NEWMAT_INCLUDE_PATH
    AND FLOWVR_INCLUDE_PATH
    AND SOFA_MODULES_LIBRARY_PATH
    AND SOFA_FRAMEWORK_LIBRARY_PATH
    AND SOFA_APPLICATIONS_LIBRARY_PATH )



