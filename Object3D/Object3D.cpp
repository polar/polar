/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "config.h"
#include "Object3D.h"
#include "GeodeFindVisitor.h"
#include "Util.h"
#include <iostream>
#include <osg/io_utils>
#include <osg/Stencil>
#include <osg/Depth>
#include <osg/TexMat>
#include <osg/Texture2D>
#include <osgDB/ReadFile>
#include <osgUtil/Optimizer>
#include <osgUtil/SmoothingVisitor>
#include <osgManipulator/CommandManager>
#include <osgManipulator/RotateSphereDragger>
#include <QDebug>

using namespace PoLAR;
using namespace osg;



void Object3DCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
{
    if(PoLAR::Object3D* object = dynamic_cast<PoLAR::Object3D*>(node))
    {
        update(object);
    }
    traverse(node, nv);
}


Object3D::Object3D(const std::string &filename, bool material, bool editable, bool pickable):
    Object3D(Util::loadFromFile(filename), material, editable, pickable)
{
}


Object3D::Object3D(Node *node, bool material, bool editable, bool pickable):
    _originalNode(node),
    _hasMaterial(material), _isEditable(editable), _isPickable(pickable),
    _isAPhantom(false), _isAShadowCaster(true), _isAShadowReceiver(true),
    _transparencyValue(0.5)
{
    if(_originalNode.get())
    {
        setObjectParameters();
    }
    else
    {
        qCritical() << "Loaded object is null";
        _originalNode = new Node;
        _objectStateSet = new StateSet;
    }
}


Object3D::Object3D(const Object3D &object, const CopyOp &copyop):
    QObject(), Switch(object, copyop),
    _frameMatrix(object._frameMatrix), _transformationMatrix(object._transformationMatrix),
    _origin(object._origin), _originalCenter(object._originalCenter),
    _hasMaterial(object._hasMaterial), _isEditable(object._isEditable), _isPickable(object._isPickable),
    _isAPhantom(object._isAPhantom), _isAShadowCaster(object._isAShadowCaster), _isAShadowReceiver(object._isAShadowReceiver),
    _transparencyValue(object._transparencyValue), _sgList(object._sgList)
{
    _selNode = dynamic_cast<MatrixTransform*>(this->getChild(0));
    _originalNode = _selNode->getChild(0);
    updateNodeMask();
    _objectStateSet = _selNode->getOrCreateStateSet();
    setName(object.getName());
#ifdef USE_PHONG_SHADING
    createMaterialUniforms();
#endif
}


Object3D::~Object3D()
{
}


Object* Object3D::clone(const CopyOp &copyop) const
{
    return new Object3D(*this, copyop);
}


void Object3D::setObjectParameters()
{
    _originalNode->getOrCreateStateSet()->setMode(GL_NORMALIZE, StateAttribute::ON);
    _originalNode->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, StateAttribute::ON);

    _selNode = new osgManipulator::Selection;
    _selNode->addChild(_originalNode);

    this->addChild(_selNode.get());
    updateNodeMask();

    _objectStateSet = _selNode->getOrCreateStateSet();

    // create a new blend color used by the blendfunc
    ref_ptr<BlendColor> bc = new BlendColor(Vec4f(1.0,1.0,1.0,1.0));
    _objectStateSet->setAttribute(bc.get());

    // create a new blend function using GL_CONSTANT_ALPHA and GL_ONE_MINUS_CONSTANT_ALPHA
    ref_ptr<BlendFunc> bf = new BlendFunc(GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
    _objectStateSet->setAttribute(bf.get());

    _originalCenter = this->getBound().center();

    // if (_isEditable)
    // {
    //setTransformationMatrix(Matrix::identity());
    //setFrameMatrix(Matrix::identity());
    _selNode->setMatrix(Matrix::identity());
    _frameMatrix = Matrix::identity();
    _transformationMatrix = getTransformationMatrix();
    computeCurrentOrigin();
    // }
    //_frameMatrix = Matrixd::identity();
    //_transformationMatrix = Matrixd::identity();
    //computeCurrentOrigin();

    if (_hasMaterial) setMaterial();
#ifdef USE_PHONG_SHADING
    else // creating some default material that mimics the fixed pipeline default material
    {
        createDefaultMaterial();
    }
    createMaterialUniforms();
#endif

    if (_originalNode->getName().empty()) setName("Object3D");
    else setName(_originalNode->getName());
}



void Object3D::optimize()
{
    osgUtil::SmoothingVisitor smooth;
    osgUtil::Optimizer optimizer;
    optimizer.optimize(this);
    this->accept(smooth);
}



void Object3D::updateNodeMask()
{
    int mask = this->getNodeMask();
    if(_isAShadowCaster)
        mask = mask | SHADOW_CASTER_MASK;
    else
        mask = mask & ~SHADOW_CASTER_MASK;

    if(_isAShadowReceiver)
        mask = mask | SHADOW_RECEIVER_MASK;
    else
        mask = mask & ~SHADOW_RECEIVER_MASK;

    this->setNodeMask(mask);
}


bool Object3D::hasTexture()
{
    if(_objectStateSet->getTextureAttribute(0, StateAttribute::TEXTURE)) return true;
    for(unsigned int i=0; i<getNumDrawables(); i++)
    {
        StateAttribute* textAttrib = getDrawable(i)->getOrCreateStateSet()->getTextureAttribute(0, StateAttribute::TEXTURE);
        if(textAttrib)
            return true;
    }
    return false;
}


void Object3D::setMaterial(float ambientX, float ambientY, float ambientZ,
                           float diffuseX, float diffuseY, float diffuseZ,
                           float specularX, float specularY, float specularZ,
                           float shininess)
{
    ref_ptr<StateAttribute> attribute = _originalNode->getOrCreateStateSet()->getAttribute(StateAttribute::MATERIAL);
    if (attribute.get())
    {
        _objectStateSet->setAttributeAndModes(attribute.get());
        _originalNode->getOrCreateStateSet()->removeAttribute(attribute.get());
    }

    ref_ptr<Material> material = dynamic_cast< Material* > (_objectStateSet->getAttribute(StateAttribute::MATERIAL));
    if (!material.valid()) material = new Material;

    material->setAmbient(Material::FRONT_AND_BACK, Vec4(ambientX, ambientY, ambientZ, 1.0));
    material->setDiffuse(Material::FRONT_AND_BACK, Vec4(diffuseX, diffuseY, diffuseZ, 1.0));
    material->setSpecular(Material::FRONT_AND_BACK, Vec4(specularX, specularY, specularZ, 1.0));
    material->setShininess(Material::FRONT_AND_BACK, shininess);
    material->setAlpha(Material::FRONT_AND_BACK, 1.0);
    _objectStateSet->setAttributeAndModes(material.get());
    if(!_hasMaterial)
    {
#ifdef USE_PHONG_SHADING
        createMaterialUniforms();
#endif
        _hasMaterial = true;
    }
}


void Object3D::createMaterialUniforms()
{
    Material* material = dynamic_cast< Material* > (_objectStateSet->getAttribute(StateAttribute::MATERIAL));
    if(material)
    {
        _objectStateSet->addUniform(new Uniform("material.emission", material->getEmission(Material::FRONT)));
        _objectStateSet->addUniform(new Uniform("material.ambient", material->getAmbient(Material::FRONT)));
        _objectStateSet->addUniform(new Uniform("material.diffuse", material->getDiffuse(Material::FRONT)));
        _objectStateSet->addUniform(new Uniform("material.specular", material->getSpecular(Material::FRONT)));
        _objectStateSet->addUniform(new Uniform("material.shininess", material->getShininess(Material::FRONT)));
    }
    int texture;
    if(hasTexture())
        texture = 1;
    else
        texture = 0;
    _objectStateSet->addUniform(new Uniform("hasTexture", texture));

}

void Object3D::updateMaterialUniforms()
{
    Material* material = dynamic_cast< Material* > (_objectStateSet->getAttribute(StateAttribute::MATERIAL));
    if(material)
    {
        if(!_objectStateSet->getUniform("material.emission"))
            createMaterialUniforms();
        _objectStateSet->getUniform("material.emission")->set(material->getEmission(Material::FRONT));
        _objectStateSet->getUniform("material.ambient")->set(material->getAmbient(Material::FRONT));
        _objectStateSet->getUniform("material.diffuse")->set(material->getDiffuse(Material::FRONT));
        _objectStateSet->getUniform("material.specular")->set(material->getSpecular(Material::FRONT));
        _objectStateSet->getUniform("material.shininess")->set(material->getShininess(Material::FRONT));
    }
    int texture;
    if(hasTexture())
        texture = 1;
    else
        texture = 0;
    if(!_objectStateSet->getUniform("hasTexture"))
        createMaterialUniforms();
    _objectStateSet->getUniform("hasTexture")->set(texture);
}



void Object3D::setAmbientColor(float ambientX, float ambientY, float ambientZ)
{
    if (_hasMaterial)
    {
        ref_ptr<Material> mat = dynamic_cast< Material* > (_objectStateSet->getAttribute(StateAttribute::MATERIAL));
        mat->setAmbient(Material::FRONT_AND_BACK, Vec4(ambientX, ambientY, ambientZ, 1.0));
#ifdef USE_PHONG_SHADING
        updateMaterialUniforms();
#endif
    }
    else printMaterialNotSet();
}


void Object3D::setDiffuseColor(float diffuseX, float diffuseY, float diffuseZ)
{
    if (_hasMaterial)
    {
        ref_ptr<Material> mat = dynamic_cast< Material* > (_objectStateSet->getAttribute(StateAttribute::MATERIAL));
        mat->setDiffuse(Material::FRONT_AND_BACK, Vec4(diffuseX, diffuseY, diffuseZ, 1.0));
#ifdef USE_PHONG_SHADING
        updateMaterialUniforms();
#endif
    }
    else printMaterialNotSet();
}


void Object3D::setSpecularColor(float specularX, float specularY, float specularZ)
{
    if (_hasMaterial)
    {
        ref_ptr<Material> mat = dynamic_cast< Material* > (_objectStateSet->getAttribute(StateAttribute::MATERIAL));
        mat->setSpecular(Material::FRONT_AND_BACK, Vec4(specularX, specularY, specularZ, 1.0));
#ifdef USE_PHONG_SHADING
        updateMaterialUniforms();
#endif
    }
    else printMaterialNotSet();
}


void Object3D::setShininess(float shininess)
{
    if (_hasMaterial)
    {
        ref_ptr<Material> mat = dynamic_cast< Material* > (_objectStateSet->getAttribute(StateAttribute::MATERIAL));
        mat->setShininess(Material::FRONT_AND_BACK, shininess);
#ifdef USE_PHONG_SHADING
        updateMaterialUniforms();
#endif
    }
    else printMaterialNotSet();
}


void Object3D::setTransparencyOn()
{
    ref_ptr<BlendColor> bc = dynamic_cast< BlendColor* > (_objectStateSet->getAttribute(StateAttribute::BLENDCOLOR));
    Vec4 consCol = bc->getConstantColor();
    bc->setConstantColor(Vec4f(consCol.r(),consCol.g(),consCol.b(),_transparencyValue));
    _objectStateSet->setMode(GL_BLEND,StateAttribute::ON|StateAttribute::OVERRIDE);
    _objectStateSet->setRenderingHint(StateSet::TRANSPARENT_BIN|StateAttribute::OVERRIDE);
}


void Object3D::setTransparencyOn(float value)
{
    _transparencyValue = value;
    setTransparencyOn();
}


void Object3D::setTransparencyOff()
{
    _objectStateSet->removeAttribute(StateAttribute::CULLFACE);
    _objectStateSet->setMode(GL_BLEND, StateAttribute::OFF|StateAttribute::OVERRIDE);
    _objectStateSet->setRenderingHint(StateSet::DEFAULT_BIN|StateAttribute::OVERRIDE);
}


void Object3D::updateTransparency(float value)
{
    if (isTransparent())
    {
        _transparencyValue = value;
        ref_ptr<BlendColor> bc = dynamic_cast< BlendColor* > (_objectStateSet->getAttribute(StateAttribute::BLENDCOLOR));
        Vec4 consCol = bc->getConstantColor();
        bc->setConstantColor(Vec4f(consCol.r(),consCol.g(),consCol.b(),_transparencyValue));
    }
}


bool Object3D::isTransparent()
{
    if (((_objectStateSet->getMode(GL_BLEND))%2)==0) return false;
    else return true;
}


float Object3D::getTransparencyValue()
{
    return _transparencyValue;
}



void Object3D::setFrameMatrix(Matrixd matrix)
{
    if (_isEditable)
    {
        _frameMatrix = matrix;
        _transformationMatrix = getTransformationMatrix();
        resetAllManipulations();
    }
    else printNonEditable();
}

Matrixd Object3D::getFrameMatrix()
{
    if (_isEditable) return _frameMatrix;
    else return Matrixd::identity();

}

void Object3D::resetAllManipulations()
{
    if (_isEditable)
    {
        setTransformationMatrix(_transformationMatrix);
    }
    else printNonEditable();
}

void Object3D::setTransformationMatrix(Matrixd matrix)
{
    if (_isEditable)
    {
        _selNode->setMatrix(matrix);
        computeCurrentOrigin();
    }
    else printNonEditable();
}

const Matrixd& Object3D::getTransformationMatrix()
{
    return _selNode->getMatrix();
}

Vec3d Object3D::getPosition() const
{
    Vec3d trans;
    trans = _selNode->getMatrix().getTrans();
    return trans;
}

void Object3D::getRotation(double *angle, double *x, double *y, double *z) const
{
    _selNode->getMatrix().getRotate().getRotate(*angle, *x, *y, *z);
}

Quat Object3D::getRotation() const
{
    return _selNode->getMatrix().getRotate();
}

Vec3d Object3D::getScale() const
{
    Vec3d scale;
    scale = _selNode->getMatrix().getScale();
    return scale;
}

void Object3D::translate(double tx, double ty, double tz)
{
    if (_isEditable)
    {
        _origin[0] += tx, _origin[1] += ty, _origin[2] += tz;
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(tx, ty, tz) * _frameMatrix);
    }
    else printNonEditable();

}

void Object3D::translate(Vec3d trans)
{
    translate(trans.x(), trans.y(), trans.z());
}


void Object3D::scale(double sx, double sy, double sz)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::scale(sx, sy, sz)* Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}


void Object3D::scale(Vec3d s)
{
    scale(s.x(), s.y(), s.z());
}


void Object3D::scale(double s)
{
    scale(s, s, s);
}

void Object3D::rotate(double angle, double x, double y, double z)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::rotate(Quat(angle, Vec3f(x, y, z))) * Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}


void Object3D::rotate(Quat q)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::rotate(q) * Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}


void Object3D::toggleTransparencySlot(int state)
{
    switch(state)
    {
    case 0:
        setTransparencyOff();
        refresh();
        break;
    case 1: break;
    case 2:
        setTransparencyOn();
        //updateTransparency(_transparencyValue);
        refresh();
        break;
    }
}


void Object3D::setTransparencyValueSlot(float value)
{
    updateTransparency(value);
    refresh();
}


void Object3D::setDisplayOn()
{
    this->setAllChildrenOn();
    refresh();
}


void Object3D::setDisplayOff()
{
    this->setAllChildrenOff();
    refresh();
}


void Object3D::displayOnOffSlot(int state)
{
    switch(state)
    {
    case 0:
        setDisplayOff();
        break;
    case 1: break;
    case 2:
        setDisplayOn();
        break;
    }
}



void Object3D::translateXSlot(double tx)
{
    if (_isEditable)
    {
        _origin[0] += tx;
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(tx, 0, 0) * _frameMatrix);
    }
    else printNonEditable();
}

void Object3D::translateYSlot(double ty)
{
    if (_isEditable)
    {
        _origin[1] += ty;
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(0, ty, 0) * _frameMatrix);
    }
    else printNonEditable();
}

void Object3D::translateZSlot(double tz)
{
    if (_isEditable)
    {
        _origin[2] += tz;
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(0, 0, tz) * _frameMatrix);
    }
    else printNonEditable();
}


void Object3D::scaleXSlot(double sx)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::scale(sx, 1.0, 1.0) * Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}

void Object3D::scaleYSlot(double sy)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::scale(1.0, sy, 1.0) * Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}

void Object3D::scaleZSlot(double sz)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::scale(1.0, 1.0, sz) * Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}


void Object3D::rotateXSlot(double ax)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::rotate(Quat(ax, Vec3f(1.0, 0.0, 0.0))) *  Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}

void Object3D::rotateYSlot(double ay)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::rotate(Quat(ay, Vec3f(0.0, 1.0, 0.0))) * Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}

void Object3D::rotateZSlot(double az)
{
    if (_isEditable)
    {
        _selNode->postMult(Matrix::inverse(_frameMatrix) * Matrixd::translate(-_origin) * Matrixd::rotate(Quat(az, Vec3f(0.0, 0.0, 1.0))) * Matrixd::translate(_origin) * _frameMatrix);
    }
    else printNonEditable();
}


void Object3D::computeCurrentOrigin()
{
    //_origin = Vec3d(0.0,0.0,0.0);
    _origin = _originalCenter;
    osg::Vec3d trans = getTransformationMatrix().getTrans();
    _origin += trans;
}


void Object3D::refresh()
{
    emit updateSignal();
}


void Object3D::setPhantomOn()
{
    if(!_isAPhantom)
    {
        _isAPhantom = true;
        for(std::list<SceneGraph*>::iterator it=_sgList.begin(); it!=_sgList.end(); ++it)
        {
            (*it)->switchToPhantom(this);
        }
        updateNodeMask();
    }
}


void Object3D::setPhantomOn(SceneGraph *sceneGraph)
{
    if(!_isAPhantom)
    {
        _isAPhantom = true;
        if(sceneGraph!=NULL)
        {
            sceneGraph->switchToPhantom(this);
        }
        updateNodeMask();
    }
}


void Object3D::setPhantomOff()
{
    if(_isAPhantom)
    {
        _isAPhantom = false;
        for(std::list<SceneGraph*>::iterator it=_sgList.begin(); it!=_sgList.end(); ++it)
        {
            (*it)->switchFromPhantom(this);
        }
        updateNodeMask();
    }
}


void Object3D::setPhantomOff(SceneGraph* sceneGraph)
{
    if(_isAPhantom)
    {
        _isAPhantom = false;
        if(sceneGraph!=NULL)
        {
            sceneGraph->switchFromPhantom(this);
        }
        updateNodeMask();
    }
}


void Object3D::setShadowReceiverOn()
{
    if (!_isAShadowReceiver)
    {
        _isAShadowReceiver = true;
        updateNodeMask();
    }
    else printShadowReceiver(true);
}


void Object3D::setShadowReceiverOff()
{
    if (_isAShadowReceiver)
    {
        _isAShadowReceiver = false;
        updateNodeMask();
    }
    else printShadowReceiver(false);
}


void Object3D::setShadowCasterOn()
{
    if (!_isAShadowCaster)
    {
        _isAShadowCaster = true;
        updateNodeMask();
    }
    else printShadowCaster(true);
}



void Object3D::setShadowCasterOff()
{
    if (_isAShadowCaster)
    {
        _isAShadowCaster = false;
        updateNodeMask();
    }
    else printShadowCaster(false);
}


void Object3D::addSceneGraph(SceneGraph *sg)
{
    if(sg)
    {
        std::list<SceneGraph*>::iterator findIt = std::find(_sgList.begin(), _sgList.end(), sg);
        if(findIt == _sgList.end())
            _sgList.push_back(sg);
    }
}

void Object3D::removeSceneGraph(SceneGraph *sg)
{
    if(sg)
    {
        std::list<SceneGraph*>::iterator findIt = std::find(_sgList.begin(), _sgList.end(), sg);
        if(findIt != _sgList.end())
            _sgList.erase(findIt);
    }
}

bool Object3D::findNearestVertex(Vec3f point, std::pair<unsigned int, Vec3f>& foundVertex)
{
    bool found = false;
    unsigned int nbDrawables = getNumDrawables();
    Vec3Array *vertexArray = NULL;
    double distMin = 100.0;
    double dist;

    for(unsigned int i=0; i<nbDrawables; i++)
    {
        vertexArray = getVertexList(i);
        if(vertexArray)
        {
            std::vector<Vec3>::iterator itr;
            for (itr=vertexArray->begin(); itr != vertexArray->end(); ++itr)
            {
                dist = Util::computeDist(*itr, point);
                if(dist < distMin)
                {
                    distMin = dist;
                    foundVertex = std::make_pair(i, *itr);
                    found = true;
                }
            }
        }
    }

    return found;
}


Vec3Array *Object3D::getVertexList(Node *node, unsigned int index)
{
    Vec3Array *vertices = NULL;
    Drawable *drawable = getDrawable(node, index);
    Geometry *geom = drawable->asGeometry();
    if(geom)
        vertices = dynamic_cast<Vec3Array*>(geom->getVertexArray());
    return vertices;
}


Drawable *Object3D::getDrawable(Node *node, unsigned int index)
{
    GeodeFindVisitor visitor;
    node->accept(visitor);
    Geode *geode = visitor.getFirst();
    if(geode)
    {
        Drawable *drawable = geode->getDrawable(index);
        return drawable;
    }
    return NULL;
}


unsigned int Object3D::getNumDrawables() const
{
    GeodeFindVisitor visitor;
    _originalNode->accept(visitor);
    Geode *geode = visitor.getFirst();
    if(geode)
    {
        return geode->getNumDrawables();
    }
    return 0;
}


void Object3D::dirty()
{
    GeodeFindVisitor visitor;
    _originalNode->accept(visitor);
    std::vector<Geode*> geodeList = visitor.getGeodeList();
    std::vector<Geode*>::iterator it;
    for(it=geodeList.begin(); it!=geodeList.end(); ++it)
    {
        Geode *geode = *it;
        if(geode)
        {
            for(unsigned int i=0; i<geode->getNumDrawables(); i++)
            {
                Drawable *drawable = geode->getDrawable(i);
                drawable->dirtyBound();
                Geometry* geom(drawable->asGeometry());
                osgUtil::SmoothingVisitor sv;
                sv.smooth(*geom);
                geom->getNormalArray()->dirty();
            }
        }
    }
}


void Object3D::setObjectTextureState(const std::string& name, bool flipVertically)
{
    Image* imgTexture = osgDB::readImageFile(name);
    setObjectTextureState(imgTexture, flipVertically);
}


void Object3D::setObjectTextureState(Image *image, bool flipVertically)
{
    // retrieve or create a StateSet
    StateSet* stateTexture = _objectStateSet;
    if(stateTexture && image)
    {
        // create a new two-dimensional texture object
        Texture2D *texCube = new Texture2D;
        texCube->setDataVariance(Object::DYNAMIC);
        // set the texture to the loaded image
        texCube->setImage(image);
        texCube->setResizeNonPowerOfTwoHint(false);
        texCube->setWrap(Texture2D::WRAP_S, Texture2D::REPEAT);
        texCube->setWrap(Texture2D::WRAP_T, Texture2D::REPEAT);

        // set the texture to the state
        stateTexture->setTextureAttributeAndModes(0, texCube, StateAttribute::ON|StateAttribute::OVERRIDE);

        if(flipVertically)
        {
            // apply a vertical (Y component) flip on the texture coordinates
            TexMat *texMat = dynamic_cast<TexMat*>(stateTexture->getAttribute(StateAttribute::TEXMAT,0));
            if(!texMat)
                texMat = new TexMat;
            Matrix M = Matrix::scale(1, -1, 1);
            texMat->setMatrix(M);
            stateTexture->setTextureAttributeAndModes(0, texMat, StateAttribute::ON|StateAttribute::OVERRIDE);
        }
    }
#ifdef USE_PHONG_SHADING
    updateMaterialUniforms();
#endif
}



void Object3D::addDragger(osgManipulator::Dragger *dragger)
{
    if(_isPickable)
    {
        removeDragger();

        this->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
        dragger->setupDefaultGeometry();

        ref_ptr<osgManipulator::Selection> selection = _selNode;
#if 0
        int mask = dragger->getNodeMask();
        mask = mask & ~SHADOW_CASTER_MASK;
        mask = mask & ~SHADOW_RECEIVER_MASK;
        dragger->setNodeMask(mask);
        SceneGraph* sg = *(_sgList.begin());
        sg->getSceneGraph()->addChild(dragger);
#else
        ref_ptr<Group> parent = new Group;
        // the dragger is added to the scenegraph of the viewer in which the add is made, and only this one
        Group* group = this->getParent(0)->getParent(0)->getParent(0)
        #if USE_SHADOWS
                ->getParent(0)
        #endif
                ;
        parent->addChild(dragger);
        group->addChild(parent);
#endif

        float scale = this->getOriginalNode()->getBound().radius() * 1.6;
        dragger->setMatrix(this->getFrameMatrix() *
                           Matrix::inverse(Matrix::translate(this->getFrameMatrix().getTrans())) *
                           Matrix::inverse(Matrix::rotate(this->getTransformationMatrix().getRotate())) *
                           Matrix::scale(scale, scale, scale) *
                           Matrix::translate(this->getOriginalNode()->getBound().center()));

        dragger->postMult(selection->getMatrix());

        ref_ptr<osgManipulator::CommandManager> commandManager = new osgManipulator::CommandManager();
        commandManager->connect(*dragger, *selection);
        _dragger = dragger;
        int mask = _dragger->getNodeMask();
        mask = mask & ~SHADOW_CASTER_MASK;
        mask = mask & ~SHADOW_RECEIVER_MASK;
        _dragger->setNodeMask(mask);
    }
    else printNonPickable();
}


void Object3D::removeDragger()
{
    if(_dragger.valid())
    {
#if 0
        SceneGraph* sg = *(_sgList.begin());
        sg->getSceneGraph()->removeChild(_dragger.get());
#else
        osg::Group* group = this->getParent(0)->getParent(0)->getParent(0)
        #if USE_SHADOWS
                ->getParent(0)
        #endif
                ;
        group->removeChild(_dragger->getParent(0));
#endif
        _dragger = NULL;
    }
}


void Object3D::generateTextureCoordinates()
{
    // generate default texture coordinates (projection from orthographic view from front)
    for(unsigned int i=0; i<getNumDrawables(); i++)
    {
        Geometry* geom = getDrawable(i)->asGeometry();
        if(geom)
        {
            Vec3Array *vertices(static_cast<Vec3Array*>(geom->getVertexArray()));
            Vec2Array *texCoordArray(static_cast<Vec2Array*>(geom->getTexCoordArray(0)));
            if(!texCoordArray)
            {
                texCoordArray = new Vec2Array;
                geom->setTexCoordArray(0, texCoordArray);
            }
            if(vertices && texCoordArray)
            {
                Vec3 mostX(*vertices->begin());
                Vec3 mostY(*vertices->begin());
                Vec3 lessX(*vertices->begin());
                Vec3 lessY(*vertices->begin());
                Vec3Array::iterator it;
                for(it=vertices->begin(); it!=vertices->end(); ++it)
                {
                    if((*it).x() > mostX.x()) mostX = (*it);
                    if((*it).z() > mostY.z()) mostY = (*it);
                    if((*it).x() < lessX.x()) lessX = (*it);
                    if((*it).z() < lessY.z()) lessY = (*it);
                }

                Vec2 translate(0.,0.);
                translate.x() = lessX.x();
                translate.y() = lessY.z();
                Vec2 ratio(0.,0.);
                ratio.x() = mostX.x() - lessX.x();
                ratio.y() = mostY.z() - lessY.z();

                for(it=vertices->begin(); it!=vertices->end(); ++it)
                {
                    Vec3 point = *it;
                    Vec2 texCoord(point.x(), point.z());
                    texCoord -= translate;
                    texCoord.x() = texCoord.x() / ratio.x();
                    texCoord.y() = texCoord.y() / ratio.y();
                    texCoordArray->push_back(texCoord);
                }
            }
        }
    }
}


void Object3D::printNonEditable()
{
    qWarning() << this->getNameAsQString() <<" is not editable.";
}


void Object3D::printMaterialNotSet()
{
    qWarning() << "The color of " << this->getNameAsQString() << " cannot be changed (a material for this object must have been set or chosen before).";
}


void Object3D::printNonPickable()
{
    qWarning() << "Object " << getNameAsQString() << " is not pickable";
}

void Object3D::printShadowCaster(bool b)
{
    if(b)
        qWarning() << this->getNameAsQString() << " is already set as a shadow caster.";
    else
        qWarning() << this->getNameAsQString() << " is not set as a shadow caster.";
}

void Object3D::printShadowReceiver(bool b)
{
    if(b)
        qWarning() << this->getNameAsQString() << " is already set as a shadow receiver.";
    else
        qWarning() << this->getNameAsQString() << " is not set as a shadow receiver.";
}


void Object3D::createDefaultMaterial()
{
    setMaterial(0.3,0.3,0.3, 0.3,0.3,0.3, 1.0,1.0,1.0, 15.0);
}


Object3D* Object3D::createGround(float sizeX, float sizeY, float posZ)
{
    ref_ptr<Geode> floorGeode = new Geode;
    ref_ptr<ShapeDrawable> shape;
    // Creating a box instead of a plane is insteresting to keep occultations even when looking from under the ground
    shape = new ShapeDrawable(new Box(osg::Vec3(0.f, 0.f, posZ), sizeX, sizeY, 0.00001f));
    floorGeode->addDrawable(shape.get());
    Object3D* object3D = new Object3D(floorGeode.get());
    object3D->setName("Ground");
    object3D->setEditOn();
    return object3D;
}
