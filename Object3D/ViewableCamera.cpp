/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifdef WIN32
#define _USE_MATH_DEFINES
#endif

#include <cmath>
#include "ViewableCamera.h"
#include <iostream>
#include <osg/io_utils>

using namespace osg;
using namespace PoLAR;

ViewableCamera::ViewableCamera(Camera *camera, bool viewablePath)
{
    _camera = camera;
    _viewablePath = viewablePath;
    _cameraMatrixTransform = new MatrixTransform;
    _cameraMatrixTransform->addChild(createViewableCamera());
    this->addChild(_cameraMatrixTransform);
    updatePositionAttitude();
    _posPrec = Vec3(0,0,0);

    if (viewablePath)
    {
        this->addChild(initPath());
        updatePath();
    }
    this->getOrCreateStateSet()->setAttributeAndModes(new PolygonMode(PolygonMode::FRONT_AND_BACK, PolygonMode::LINE));
}

Group* ViewableCamera::createViewableCamera()
{
    // Camera
    Geode *cameraGeode = new Geode();
    {
        Cone *cameraCone = new Cone(Vec3(0.0f,0.0f,0.0f),0.1f, 0.2f);
        ShapeDrawable *cameraShape = new ShapeDrawable(cameraCone);
        cameraShape->setColor(Vec4(1.0f,0.0f,0.0f,1.0f));
        cameraGeode->addDrawable(cameraShape);
        cameraGeode->setName("Camera");
        cameraGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, StateAttribute::ON);
        cameraGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
    }

    // Camera's Up Axis
    Geode* conegeode = new Geode;
    {
        Cone* cone = new Cone (Vec3(0.0,0.25,0.0), 0.025f, 0.05f);
        cone->setRotation(Quat(-M_PI/2,Vec3f(1.0,0.0,0.0)));
        ShapeDrawable *coneShape = new ShapeDrawable(cone);
        coneShape->setColor(Vec4(1.0f,1.0f,1.0f,1.0f));
        conegeode->addDrawable(coneShape);
        conegeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, StateAttribute::ON);
        conegeode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
    }

    Geode* lineGeode = new Geode;
    {
        Geometry* geometry = new Geometry();
        Vec3Array* vertices = new Vec3Array(2);
        (*vertices)[0] = Vec3(0.0,0.0,0.0);
        (*vertices)[1] = Vec3(0.0,0.25,0.0);
        geometry->setVertexArray(vertices);
        geometry->addPrimitiveSet(new DrawArrays(PrimitiveSet::LINES,0,2));

        Vec4Array* colors = new Vec4Array;
        colors->push_back(Vec4(1.0f, 1.0f, 1.0f, 1.0f));
        geometry->setColorArray(colors);
        geometry->setColorBinding(Geometry::BIND_OVERALL);

        lineGeode->addDrawable(geometry);
        LineWidth* linewidth = new LineWidth();
        linewidth->setWidth(1.0f);
        lineGeode->getOrCreateStateSet()->setAttributeAndModes(linewidth, StateAttribute::ON);
        lineGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, StateAttribute::ON);
        lineGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
    }

    conegeode->setName("Camera's Up axis");
    lineGeode->setName("Camera's Up axis");

    Group* viewableCameraGroup = new Group();
    viewableCameraGroup->addChild(cameraGeode);
    viewableCameraGroup->addChild(conegeode);
    viewableCameraGroup->addChild(lineGeode);

    return viewableCameraGroup;
}


void ViewableCamera::updatePositionAttitude()
{
    Matrixd matRot, matTransInv, matRotInv, viewMatrix = _camera->getViewMatrix();

    matRotInv.invert(Matrixd::rotate(viewMatrix.getRotate()));
    matTransInv.makeTranslate((Matrixd::translate(-viewMatrix.getTrans()) * matRotInv).getTrans());

    _cameraMatrixTransform->setMatrix(matRotInv * matTransInv);
}


Geode* ViewableCamera::initPath()
{
    StateSet* pathStateSet = new StateSet;
    {
        LineWidth *lineWidth = new LineWidth;
        lineWidth->setWidth(2.0f);
        pathStateSet->setAttributeAndModes(lineWidth, StateAttribute::ON);
        pathStateSet->setMode(GL_LIGHTING, StateAttribute::OFF);
        pathStateSet->setMode(GL_NORMALIZE, StateAttribute::ON);
    }

    _numPoints = 0;
    Geode *pathGeode = new Geode;
    pathGeode->setStateSet(pathStateSet);
    pathGeode->setName("Camera's path");

    _pathGeometry = new Geometry;
    _pathGeometry->setUseDisplayList(false);

    _pathVertices = new Vec3Array();
    _pathGeometry->setVertexArray(_pathVertices);

    _pathDrawElements = new DrawElementsUInt(PrimitiveSet::LINE_STRIP);
    _pathGeometry->addPrimitiveSet(_pathDrawElements);

    Vec4Array* pathColor = new Vec4Array;
    pathColor->push_back(Vec4(1.0f, 1.0f, 1.0f, 1.0f));
    _pathGeometry->setColorArray(pathColor);
    _pathGeometry->setColorBinding(Geometry::BIND_OVERALL);

    pathGeode->addDrawable(_pathGeometry);
    return pathGeode;
}


void ViewableCamera::updatePath()
{
    Matrixd m(_cameraMatrixTransform->getMatrix());
    if (m.valid())
    {
        if (! (_posPrec == Vec3f(m(3,0), m(3,1), m(3,2))))
        {
            _posPrec = Vec3(m(3,0), m(3,1), m(3,2));
            _pathVertices->push_back(_posPrec);
            _pathDrawElements->push_back(_numPoints);
            _numPoints++;
        }
    }
    _pathGeometry->dirtyBound();
}


void ViewableCamera::resetPath()
{
    if (getPathLength())
    {
        std::vector<Vec3>::iterator startItr1=_pathVertices->begin(), endItr1=_pathVertices->end();
        _pathVertices->erase(startItr1, endItr1);

        std::vector<GLuint>::iterator startItr2=_pathDrawElements->begin(), endItr2=_pathDrawElements->end();
        _pathDrawElements->erase(startItr2, endItr2);
        _numPoints = 0;
    }
}


void ViewableCamera::update()
{
    updatePositionAttitude();
    if (_viewablePath) updatePath();
}
