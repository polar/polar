/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "AnimatedObject3D.h"
#include "Util.h"
#include <QDebug>

using namespace PoLAR;
using namespace osg;


AnimatedObject3D::AnimatedObject3D(const std::string &filename, bool material, bool editable, bool pickable):
    AnimatedObject3D(Util::loadFromFile(filename), material, editable, pickable)
{
}


AnimatedObject3D::AnimatedObject3D(Node *node, bool material, bool editable, bool pickable):
    Object3D(node, material, editable, pickable),
    _currentAnimation(0)
{
    if(_originalNode.get())
    {
        AnimationManagerFinder finder;
        _originalNode->accept(finder);
        if(finder._am.valid())
        {
            _manager = finder._am.get();
            osgAnimation::Animation::PlayMode playMode = osgAnimation::Animation::LOOP;
            for (osgAnimation::AnimationList::const_iterator animIter = finder._am->getAnimationList().begin();
                 animIter != finder._am->getAnimationList().end(); ++animIter)
            {
                (*animIter)->setPlayMode(playMode);
                (*animIter)->computeDuration();
            }
            for (osgAnimation::AnimationList::const_iterator it = _manager->getAnimationList().begin(); it != _manager->getAnimationList().end(); it++)
                _map[(*it)->getName()] = *it;

            for(osgAnimation::AnimationMap::iterator it = _map.begin(); it != _map.end(); it++)
                _amv.push_back(it->first);

            _originalNode->setUpdateCallback(finder._am.get());
        }

    }
}


AnimatedObject3D::AnimatedObject3D(const AnimatedObject3D &object, const CopyOp &copyop):
    Object3D(object, copyop)
{
    _manager = new osgAnimation::BasicAnimationManager(*(object._manager.get()), copyop);
    for (osgAnimation::AnimationList::const_iterator it = _manager->getAnimationList().begin(); it != _manager->getAnimationList().end(); it++)
        _map[(*it)->getName()] = *it;

    for(osgAnimation::AnimationMap::iterator it = _map.begin(); it != _map.end(); it++)
        _amv.push_back(it->first);
    _currentAnimation = object._currentAnimation;
}



AnimatedObject3D::~AnimatedObject3D()
{
}


Object* AnimatedObject3D::clone(const CopyOp &copyop) const
{
    return new AnimatedObject3D(*this, copyop);
}


bool AnimatedObject3D::hasAnimations()
{
    if(_manager.valid())
    {
        if(_manager->getAnimationList().size()>0)
            return true;
    }
    return false;
}


unsigned int AnimatedObject3D::getNumAnimations()
{
    return _amv.size();
}


void AnimatedObject3D::printAnimationList()
{
    qDebug() << "Animation List: ";
    for(osgAnimation::AnimationMap::iterator it = _map.begin(); it != _map.end(); it++)
        qDebug() << QString::fromStdString(it->first);
}

bool AnimatedObject3D::play()
{
    if(_currentAnimation < _amv.size())
    {
        _manager->playAnimation(_map[_amv[_currentAnimation]].get());
        return true;
    }

    return false;
}

bool AnimatedObject3D::stop()
{
    if(_currentAnimation < _amv.size())
    {
        _manager->stopAnimation(_map[_amv[_currentAnimation]].get());
        return true;
    }
    return false;
}

bool AnimatedObject3D::isPlaying() const
{
    if(_manager.valid())
    {
        osgAnimation::Animation* anim = _manager->getAnimationList()[_currentAnimation];
        if(anim)
        {
            bool b = _manager->isPlaying(anim);
            return b;
        }
    }
    return false;
}


bool AnimatedObject3D::isPlaying(const std::string& name)
{
    unsigned int prev = _currentAnimation;
    bool playing = false;
    if(selectByName(name))
    {
        if(_manager.valid())
        {
            osgAnimation::Animation* anim = _manager->getAnimationList()[_currentAnimation];
            if(anim)
            {
                playing = _manager->isPlaying(anim);
            }
        }
    }
    _currentAnimation = prev;
    return playing;
}

void AnimatedObject3D::setAnimationSpeed(float speed)
{
    if(speed > 0.0 && _manager.valid())
    {
        osgAnimation::Animation* anim = _manager->getAnimationList()[_currentAnimation];
        if(anim) anim->setDuration((anim->getDuration() / speed));
    }
}


void AnimatedObject3D::selectNextAnimation()
{
    _currentAnimation = (_currentAnimation + 1) % _map.size();
}

void AnimatedObject3D::selectPreviousAnimation()
{
    _currentAnimation = (_map.size() + _currentAnimation - 1) % _map.size();
}


bool AnimatedObject3D::selectByName(const std::string& name)
{
    bool found = false;
    for(unsigned int i = 0; i < _amv.size(); i++)
    {
        if(_amv[i] == name)
        {
            found = true;
            _currentAnimation = i;
        }
        if(found) break;
    }
    return found;
}


bool AnimatedObject3D::playByName(const std::string& name)
{
    bool found = selectByName(name);
    if(_manager.valid() && found)
    {
        _manager->playAnimation(_map[name].get());
        return true;
    }
    return false;
}


void AnimatedObject3D::setAnimationPlayMode(osgAnimation::Animation::PlayMode mode)
{
    if(_manager.valid())
    {
        osgAnimation::Animation* anim = _manager->getAnimationList()[_currentAnimation];
        if(anim)
            anim->setPlayMode(mode);
        else
            qDebug() << "animation" <<  QString::fromStdString((_amv[_currentAnimation])) << "unavailable";
    }
}


osgAnimation::Animation::PlayMode AnimatedObject3D::getAnimationPlayMode() const
{
    if(_manager.valid())
    {
        osgAnimation::Animation* anim = _manager->getAnimationList()[_currentAnimation];
        if(anim)
            return anim->getPlayMode();
        else
        {
            qDebug() << "animation" <<  QString::fromStdString((_amv[_currentAnimation])) << "unavailable";
        }
    }
    return osgAnimation::Animation::LOOP;
}



const std::string& AnimatedObject3D::getCurrentAnimationName() const
{
    return _amv[_currentAnimation];
}

const std::vector<std::string>& AnimatedObject3D::getAnimationsList() const
{
    return _amv;
}
