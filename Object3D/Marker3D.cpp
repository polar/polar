/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Marker3D.h"
#include <osg/ShapeDrawable>

using namespace PoLAR;
using namespace osg;

Marker3D::Marker3D():
    _center(0.0f,0.0f,0.0f),
    _size(0.1f),
    _type(CUBE)
{
    createShape();
}

Marker3D::Marker3D(Vec3 &center, float size, TYPE type):
    _center(center),
    _size(size),
    _type(type)
{
    createShape();
}


Marker3D::Marker3D(const Marker3D &marker, const CopyOp &copyop):
    Geode(marker, copyop),
    _center(marker._center),
    _size(marker._size),
    _type(marker._type)
{

}


void Marker3D::createShape()
{
    switch(_type)
    {
    case CUBE:
    {
        this->addDrawable(createCube());
        break;
    }
    case SPHERE:
    {
        this->addDrawable(createSphere());
        break;
    }
    default:
        break;
    }
}

Drawable* Marker3D::createCube()
{
    return new ShapeDrawable(new Box(_center, _size));
}

Drawable* Marker3D::createSphere()
{
    return new ShapeDrawable(new Sphere(_center, _size));
}
