/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Light.h"
#include "Object3D.h"
#include "SceneGraph.h"
#include <osg/PositionAttitudeTransform>
#include <iostream>

using namespace PoLAR;

Light::Light():

    _viewableLight(NULL), _isDirectional(true), _isASpotLight(false), _lightIsOn(true), _shadower(false)
{
    _lightSource = new osg::LightSource;
    checkLightForm();
    setViewable(false);
    _lightSource->getLight()->setLightNum(-1);
}

Light::Light(osg::Vec4 position, osg::Vec4 ambient, osg::Vec4 diffuse, osg::Vec4 specular, bool shadowCaster, bool viewable, bool videoFrame):
    _viewableLight(NULL), _isDirectional(true), _isASpotLight(false), _lightIsOn(true), _shadower(shadowCaster)
{
    _lightSource = new osg::LightSource;
    setAmbient(ambient);
    setDiffuse(diffuse);
    setSpecular(specular);
    setPosition(position);
    if(videoFrame)
        setReferenceFrameVideo();
    else setReferenceFrameWorld();

    checkLightForm();
    setViewable(viewable);
    _lightSource->getLight()->setLightNum(-1);
}

Light::Light(osg::LightSource* source, bool shadowCaster, bool viewable):
    _viewableLight(NULL), _lightIsOn(true), _shadower(shadowCaster)
{
    _lightSource = source;
    checkLightForm();
    setViewable(viewable);
}


Light::Light(const Light &light, const osg::CopyOp &copyop):
    QObject(), osg::Referenced(),
    _sgList(light._sgList), _isViewable(light._isViewable), _isDirectional(light._isDirectional), _isASpotLight(light._isASpotLight),
    _lightIsOn(light._lightIsOn), _shadower(light._shadower)
{
    _lightSource = dynamic_cast<osg::LightSource*>(light._lightSource->clone(copyop));
    if(_isViewable) _viewableLight = dynamic_cast<Object3D*>(light._viewableLight->clone(copyop));
}


Light::~Light()
{
}


Light* Light::clone(const osg::CopyOp &copyop) const
{
    return new Light(*this, copyop);
}


void Light::checkLightForm()
{
    if(_lightSource->getLight()->getPosition().w() == 0.0f)
    {
        _isDirectional = true;
        _isASpotLight = false;
    }
    else
    {
        _isDirectional = false;
        if(_lightSource->getLight()->getSpotCutoff() != 180.0f)
            _isASpotLight = true;
        else _isASpotLight = false;
    }
}


void Light::addSceneGraph(SceneGraph *sg)
{
    if(sg)
    {
        std::list<SceneGraph*>::iterator findIt = std::find(_sgList.begin(), _sgList.end(), sg);
        if(findIt == _sgList.end())
            _sgList.push_back(sg);
    }
}

void Light::removeSceneGraph(SceneGraph *sg)
{
    if(sg)
    {
        std::list<SceneGraph*>::iterator findIt = std::find(_sgList.begin(), _sgList.end(), sg);
        if(findIt != _sgList.end())
            _sgList.erase(findIt);
    }
}

void Light::setReferenceFrameVideo()
{
    _lightSource->setReferenceFrame(osg::LightSource::ABSOLUTE_RF);
    updateViewableObjectReference();
}


void Light::setReferenceFrameWorld()
{
    _lightSource->setReferenceFrame(osg::LightSource::RELATIVE_RF);
    updateViewableObjectReference();
}

bool Light::isReferenceFrameVideo()
{
    return (_lightSource->getReferenceFrame() == osg::LightSource::ABSOLUTE_RF);
}


bool Light::isReferenceFrame_World()
{
    return (_lightSource->getReferenceFrame() == osg::LightSource::RELATIVE_RF);
}


void Light::setShadowCaster(bool b)
{
    _shadower = b;

}

void Light::setViewable(bool b)
{
    _isViewable = b;
    if(b)
    {
        addViewableObject();
    }
    else
    {
        removeViewableObject();
    }
}


bool Light::addViewableObject()
{
    generateViewableObject();
    updateViewableObjectPosition();
    updateViewableObjectReference();
    if(_sgList.size() == 0) return false;
    bool ret = true;
    for(std::list<SceneGraph*>::iterator it=_sgList.begin(); it!=_sgList.end(); ++it)
    {
        if((*it)->getObjectIndex(_viewableLight.get()) == -1)
            ret = ret && (*it)->addObject(_viewableLight.get());
    }
    refresh();
    return ret;
}

bool Light::addViewableObject(SceneGraph* scenegraph)
{
    generateViewableObject();
    updateViewableObjectPosition();
    updateViewableObjectReference();
    bool ret = false;
    if(scenegraph->getObjectIndex(_viewableLight.get()) == -1)
        ret = scenegraph->addObject(_viewableLight.get());
    refresh();
    return ret;
}


bool Light::removeViewableObject()
{
    if(_sgList.size() == 0) return false;
    bool ret = true;
    for(std::list<SceneGraph*>::iterator it=_sgList.begin(); it!=_sgList.end(); ++it)
    {
        ret = ret && (*it)->removeObject(_viewableLight.get());
    }
    refresh();
    return ret;
}


bool Light::removeViewableObject(SceneGraph* scenegraph)
{
    bool ret = false;
    ret = scenegraph->removeObject(_viewableLight.get());
    refresh();
    return ret;
}



void Light::setPosition(const osg::Vec4 &position)
{
    _lightSource->getLight()->setPosition(position);
    if(position.w() == 0.0f)
        _isDirectional = true;
    else
        _isDirectional = false;
    updateViewableObjectPosition();
    refresh();
}

void Light::setPosition(float x, float y, float z, float w)
{
    setPosition(osg::Vec4(x, y, z, w));
    refresh();
}


void Light::setOn()
{
    _lightIsOn = true;
    for(std::list<SceneGraph*>::iterator it=_sgList.begin(); it!=_sgList.end(); ++it)
    {
        (*it)->getGraphStateSet()->setAssociatedModes(_lightSource->getLight(), osg::StateAttribute::ON);
#ifdef USE_PHONG_SHADING
        (*it)->updateLightUniforms(this->getLightNum());
#endif
    }
}


void Light::setOff()
{
    _lightIsOn = false;
    for(std::list<SceneGraph*>::iterator it=_sgList.begin(); it!=_sgList.end(); ++it)
    {
        (*it)->getGraphStateSet()->setAssociatedModes(_lightSource->getLight(), osg::StateAttribute::OFF);
#ifdef USE_PHONG_SHADING
        (*it)->setOffLightUniforms(this->getLightNum());
#endif
    }
}


void Light::setAsDirectional()
{
    osg::Vec4 pos = getPosition();
    setPosition(pos.x(), pos.y(), pos.z(), 0.0f);
    refresh();
}


void Light::setAsPointLight()
{
    osg::Vec4 pos = getPosition();
    setPosition(pos.x(), pos.y(), pos.z(), 1.0f);
    refresh();
}

void Light::onOffSlot(int state)
{
    switch(state)
    {
    case 0:
        setOff();
        break;
    case 1: break;
    case 2:
        setOn();
        break;
    }
}


void Light::ambientRedSlot(float value)
{
    osg::Vec4 ambient = getAmbient();
    setAmbient(value, ambient.g(), ambient.b());
    refresh();
}

void Light::ambientGreenSlot(float value)
{
    osg::Vec4 ambient = getAmbient();
    setAmbient(ambient.r(), value, ambient.b());
    refresh();
}

void Light::ambientBlueSlot(float value)
{
    osg::Vec4 ambient = getAmbient();
    setAmbient(ambient.r(), ambient.g(), value);
    refresh();
}

void Light::diffuseRedSlot(float value)
{
    osg::Vec4 diffuse = getDiffuse();
    setDiffuse(value, diffuse.g(), diffuse.b());
    refresh();
}

void Light::diffuseGreenSlot(float value)
{
    osg::Vec4 diffuse = getDiffuse();
    setDiffuse(diffuse.r(), value, diffuse.b());
    refresh();
}

void Light::diffuseBlueSlot(float value)
{
    osg::Vec4 diffuse = getDiffuse();
    setDiffuse(diffuse.r(), diffuse.g(), value);
    refresh();
}

void Light::specularRedSlot(float value)
{
    osg:: Vec4 specular = getSpecular();
    setSpecular(value, specular.g(), specular.b());
    refresh();
}

void Light::specularGreenSlot(float value)
{
    osg::Vec4 specular = getSpecular();
    setSpecular(specular.r(), value, specular.b());
    refresh();
}

void Light::specularBlueSlot(float value)
{
    osg::Vec4 specular = getSpecular();
    setSpecular(specular.r(), specular.g(), value);
    refresh();
}

void Light::positionXSlot(float value)
{
    osg::Vec4 pos = getPosition();
    setPosition(value, pos.y(), pos.z(), pos.w());
    refresh();
}

void Light::positionYSlot(float value)
{
    osg::Vec4 pos = getPosition();
    setPosition(pos.x(), value, pos.z(), pos.w());
    refresh();
}

void Light::positionZSlot(float value)
{
    osg::Vec4 pos = getPosition();
    setPosition(pos.x(), pos.y(), value, pos.w());
    refresh();
}

void Light::directionXSlot(float value)
{
    osg::Vec3 dir = getDirection();
    setDirection(value, dir.y(), dir.z());
    refresh();
}

void Light::directionYSlot(float value)
{
    osg::Vec3 dir = getDirection();
    setDirection(dir.x(), value, dir.z());
    refresh();
}

void Light::directionZSlot(float value)
{
    osg::Vec3 dir = getDirection();
    setDirection(dir.x(), dir.y(), value);
    refresh();
}

void Light::constantAttenuationSlot(float value)
{
    setConstantAttenuation(value);
    refresh();
}

void Light::linearAttenuationSlot(float value)
{
    setLinearAttenuation(value);
    refresh();
}

void Light::quadraticAttenuationSlot(float value)
{
    setQuadraticAttenuation(value);
    refresh();
}

void Light::spotExponentSlot(float value)
{
    setSpotExponent(value);
    refresh();
}

void Light::spotCutoffSlot(float value)
{
    setSpotCutoff(value);
    refresh();
}

void Light::refresh()
{
    emit updateSignal();
#ifdef USE_PHONG_SHADING
    updateUniforms();
#endif
}


void Light::updateUniforms()
{
    if(getLightNum() != -1)
    {
        for(std::list<SceneGraph*>::iterator it=_sgList.begin(); it!=_sgList.end(); ++it)
        {
            (*it)->updateLightUniforms(this->getLightNum());
        }
    }
}


void Light::generateViewableObject()
{
    osg::ref_ptr<osg::Sphere> bSphere = new osg::Sphere(osg::Vec3(0.0f, 0.0f, 0.0f), 0.1f);
    osg::ref_ptr<osg::ShapeDrawable> sdSphere = new osg::ShapeDrawable(bSphere);
    sdSphere->setColor(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));
    osg::ref_ptr<osg::Geode> geodeSphere = new osg::Geode;
    geodeSphere->addDrawable(sdSphere.get());
    osg::PositionAttitudeTransform* pat = new osg::PositionAttitudeTransform();
    pat->addChild(geodeSphere.get());
    char name[256];
    sprintf(name, "Light_%d", getLightNum());
    geodeSphere->setName(name);
    _viewableLight = new Object3D(pat, false, true, false);
    _viewableLight->setShadowCasterOff();
    _viewableLight->setShadowReceiverOff();
}

void Light::updateViewableObjectPosition()
{
    osg::Vec3 pos(getPosition().x(), getPosition().y(), getPosition().z());
    if(pos.valid() && _viewableLight.valid())
    {
        osg::PositionAttitudeTransform* pat = dynamic_cast<osg::PositionAttitudeTransform*>(_viewableLight->getOriginalNode());
        if(pat)
        {
            pat->setPosition(pos);
        }
    }
}

void Light::updateViewableObjectReference()
{
    if(_viewableLight.valid())
    {
        osg::PositionAttitudeTransform* pat = dynamic_cast<osg::PositionAttitudeTransform*>(_viewableLight->getOriginalNode());
        if(pat)
        {
            osg::PositionAttitudeTransform::ReferenceFrame ref = isReferenceFrameVideo() ? osg::PositionAttitudeTransform::ABSOLUTE_RF : osg::PositionAttitudeTransform::RELATIVE_RF;
            pat->setReferenceFrame(ref);
        }
    }
}
