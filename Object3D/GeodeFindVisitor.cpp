/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "GeodeFindVisitor.h"

using namespace PoLAR;

GeodeFindVisitor::GeodeFindVisitor()
    : osg::NodeVisitor (osg::NodeVisitor::TRAVERSE_ALL_CHILDREN)
{}

void GeodeFindVisitor::apply(osg::Node &searchNode) {
    if (!strcmp (searchNode.className(), "Geode"))
    {
        _foundGeodes.push_back((osg::Geode*) &searchNode);
    }
    traverse(searchNode);
}

osg::Geode *GeodeFindVisitor::getFirst() const
{
    if (_foundGeodes.size() > 0)
        return _foundGeodes.at(0);
    else
        return NULL;
}

std::vector<osg::Geode*>& GeodeFindVisitor::getGeodeList()
{
    return _foundGeodes;
}
