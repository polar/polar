/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

/** @{ */
/** \file ReaderWriterOFF.cpp
\brief Class extending an OpenSceneGraph ReaderWriter to load OFF models

\author Pierre-Jean Petitprez
\date year 2014
\note For OSG to be able to find this plugin, it is necessary to give OSG the path to the .so file (>export OSG_LIBRARY_PATH=/path/to/osgdb_off.so)
*/
/** @} */

#if defined(_WIN32) || defined(WIN32)
#include <windows.h>
#endif

#include <osg/Notify>
#include <osg/Geode>
#include <osg/Geometry>

#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/fstream>
#include <osgDB/Registry>
#include <osgUtil/SmoothingVisitor>

#include <string>
#include <map>
#include <vector>


class ReaderWriterOFF: public osgDB::ReaderWriter
{
public:

    ReaderWriterOFF()
    {
        supportsExtension("off","Object File Format");
    }

    virtual const char* className() const { return "Object File Format Reader"; }

    osgDB::ReaderWriter::ReadResult readNode(const std::string& file, const osgDB::ReaderWriter::Options* options) const
    {
        std::string ext = osgDB::getLowerCaseFileExtension(file);
        if(!acceptsExtension(ext)) return ReadResult::FILE_NOT_HANDLED;

        std::string fileName = osgDB::findDataFile(file, options);
        if(fileName.empty()) return ReadResult::FILE_NOT_FOUND;

        osgDB::ifstream stream(fileName.c_str(), std::ios::in);
        if(!stream) return ReadResult::ERROR_IN_READING_FILE;
        return readNode(stream, options);

    }

    osgDB::ReaderWriter::ReadResult readNode(std::istream& file, const osgDB::ReaderWriter::Options*) const
    {
        unsigned int verticesNumber = 0;
        unsigned int facesNumber = 0;
        unsigned int edgesNumber = 0;
        std::string format;

        file >> format;
        if(format.compare("OFF") != 0 && format.compare("COFF") != 0 && format.compare("NOFF") != 0)
        {
            return corruptedOFFFile();
        }

        file >> verticesNumber;
        file >> facesNumber;
        file >> edgesNumber; // not used but needs to be present anyway (OFF standard)
        if(verticesNumber <= 0 || facesNumber <= 0)
        {
            return corruptedOFFFile();
        }

        // starting with getting the vertices (each vertex is represented as 3 floats "x y z")
        std::map<int, osg::Vec3> verticesMap; // map to associate the vertices and their index
        osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array;
        osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;

        unsigned int index = 0;
        while(index < verticesNumber && !file.eof() && !(file.rdstate() & std::istream::failbit))
        {
            osg::Vec3 point;
            file >> point.x() >> point.y() >> point.z();
            vertices->push_back(point);
            verticesMap[index] = point;

            if(format.compare("COFF") == 0) // colored OFF : colors are represented as "r g b a" floats (0.0 .. 1.0) after the vertex coordinates
            {
                osg::Vec4 color;
                file >> color.r() >> color.g() >> color.b() >> color.a();
                colors->push_back(color);
            }
            index++;
        }
        if(index != verticesNumber) return corruptedOFFFile();

        osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
        geom->setVertexArray(vertices.get());
        if(format.compare("COFF") == 0) geom->setColorArray(colors.get(), osg::Array::BIND_PER_VERTEX);

        // Now get the faces
        unsigned int verticesNumberByFace;
        osg::ref_ptr<osg::Vec3Array> normals = new osg::Vec3Array;
        index = 0;
        while(index < facesNumber && !file.eof() && !(file.rdstate() & std::istream::failbit))
        {
            file >> verticesNumberByFace;
			std::vector<unsigned int> vertexIndices;
            for(unsigned int i = 0; i < verticesNumberByFace; i++)
            {
				unsigned int tmp;
				file >> tmp;
				vertexIndices.push_back(tmp);
            }

            // Adding the vertices of the face by index
            // Maybe should add support for faces with 5+ vertices (osg::PrimitiveSet::POLYGON ?)
            osg::PrimitiveSet::Mode mode = (verticesNumberByFace == 3) ? osg::PrimitiveSet::TRIANGLES : osg::PrimitiveSet::QUADS;
            osg::DrawElementsUInt *osgFace = new osg::DrawElementsUInt(mode, 0);
            for(unsigned int i = 0; i < verticesNumberByFace; i++)
            {
                osgFace->push_back(vertexIndices[i]);
            }
            geom->addPrimitiveSet(osgFace);

            // Compute the normals
            osg::Vec3 associatedNormal = calculateNormal(verticesMap[vertexIndices[0]], verticesMap[vertexIndices[1]], verticesMap[vertexIndices[2]]);
            normals->push_back(associatedNormal);
            index++;
        }
        if(index != facesNumber) return corruptedOFFFile();

        geom->setNormalArray(normals.get(), osg::Array::BIND_PER_PRIMITIVE_SET);

        osg::ref_ptr<osg::Geode> geode = new osg::Geode;
        geode->addDrawable(geom.get());
        return geode.release();
    }

private:
    static osg::Vec3 calculateNormal(osg::Vec3 v1, osg::Vec3 v2, osg::Vec3 v3)
    {
        osg::Vec3 edge1 = v2 - v1, edge2 = v2 - v3;

        // cross product following the right hand rule. Note that OSG redefines the ^ operator for this usage
        return edge2 ^ edge1;
    }

    static osgDB::ReaderWriter::ReadResult corruptedOFFFile()
    {
        OSG_INFO << "Unrecognized OFF file" << std::endl;
        return ReadResult::ERROR_IN_READING_FILE;
    }

};


REGISTER_OSGPLUGIN(off, ReaderWriterOFF)
