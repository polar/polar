/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "ShaderManager.h"
#include "config.h"
#include <QDebug>
#include <osg/StateAttribute>

#define USE_EMBEDDED_SHADERS 1

using namespace PoLAR;

#ifndef USE_FIXED_PIPELINE
const char wlVertSource[] =
        "uniform mat4 osg_ModelViewProjectionMatrix;\n"
        "attribute vec4 osg_Vertex;\n"
        "attribute vec4 osg_MultiTexCoord0;\n"
        "varying vec4 texCoord0;\n"

        "void main(void)\n"
        "{\n"
        "gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex;\n"
        "texCoord0 = osg_MultiTexCoord0;\n"
        "}\n";
#endif


/**
 * window/level fragment shader
 */
const char wlFragSource[] =
        #ifdef MOBILE_PLATFORM
        "precision mediump float;\n"
        #endif
        "uniform int colorHeight;\n"
        "uniform int colorWidth;\n"
        "uniform float wDef;\n"
        "uniform float hDef;\n"

        "/* distortion uniforms */\n"
        "uniform int withDistortion;\n"
        "uniform vec2 texSize;\n"
        "uniform float lambda1, lambda2;\n"
        "uniform vec2 centre;\n"

        "uniform sampler2D tex;\n"
        #ifndef USE_FIXED_PIPELINE
        "varying vec4 texCoord0;\n"
        #endif

        "/* some functions for window level computation */\n"
        "float range(float val)\n"
        "{\n"
        "   return val < 0.0 ? 0.0 : (val > 255.0 ? 255.0 : val);\n"
        "}\n"

        "float wl(float color, float a, float b)\n"
        "{\n"
        "    return range(color * 255.0 * a + b) / 255.0;\n"
        "}\n"

        "/** main **/\n"
        "void main(void)\n"
        "{\n"
        #if defined(USE_FIXED_PIPELINE)
        "   vec4 texCoord0 = gl_TexCoord[0];\n"
        #endif
        "    vec4 baseColor = vec4(0.0);\n"
        "    bool outOfTexture = false;\n"

        "    // distortion computation\n"
        "    if(withDistortion == 1)\n"
        "    {\n"
        "        vec2 centreGL = centre / texSize;\n"
        "        vec2 delta = texCoord0.st - centreGL;\n"
        "        float r2 = texSize.x * texSize.x * delta.x * delta.x + texSize.y * texSize.y * delta.y * delta.y;\n"
        "        float cRd = 1.0 + (lambda1 + (lambda2 * r2)) * r2;\n"
        "        vec2 texPos = cRd * delta + centreGL;\n"

        "        if (any(lessThan(texPos, vec2(0.0,0.0))) || any(greaterThan(texPos, vec2(1.0,1.0))))\n"
        "           outOfTexture = true;\n"
        "        else\n"
        "           baseColor = texture2D(tex, texPos);\n"
        "    }\n"
        "    else // withDistortion = 0\n"
        "    {\n"
        "        baseColor = texture2D(tex, texCoord0.st);\n"
        "    }\n"


        "    // window level computation\n"
        "    if(!outOfTexture)\n"
        "    {\n"
        "        float a = wDef * 2.0 / float(colorWidth);\n"
        "        float b = a * float(colorHeight) / 4.0 - hDef;\n"

        "        float red = wl(baseColor.r, a, b);\n"
        "       float green = wl(baseColor.g, a, b);\n"
        "        float blue = wl(baseColor.b, a, b);\n"

        "        gl_FragColor = vec4(red, green, blue, 1.0);\n"
        "    }\n"
        "    else\n"
        "        gl_FragColor = baseColor;\n"
        "}\n";



char shadowVertSource[] =
        "attribute vec4 osg_Vertex;\n"
        "uniform mat4 osg_ModelViewProjectionMatrix;\n"
        "varying vec4 vPosition;\n"

        "void main(void)\n"
        "{\n"
        "  vPosition = osg_ModelViewProjectionMatrix * osg_Vertex;\n"
        "  gl_Position = vPosition;\n"
        "\n"
        "}\n";


/*
 * Some Android platforms do not have the GL extension to manage depth textures
 * We get rid of this limitation by saving the depth value in the RGBA texture
 */
char shadowFragSource[] =
        #if MOBILE_PLATFORM
        "precision highp float;\n"
        #endif
        "varying vec4 vPosition;\n"

        // from Fabien Sangalard's DEngine
        // https://github.com/fabiensanglard/dEngine
        "vec4 pack(float depth)\n"
        "{\n"
        "    const vec4 bitSh = vec4(256.0 * 256.0 * 256.0,  256.0 * 256.0, 256.0, 1.0);\n"
        "    const vec4 bitMsk = vec4(0.0, 1.0 / 256.0, 1.0 / 256.0, 1.0 / 256.0);\n"
        "    vec4 comp = fract(bitSh*depth);\n"
        "    comp -= comp.xxyz * bitMsk;\n"
        "    return comp;\n"
        "}\n"

        "void main()\n"
        "{\n"
        "    float normalizedDistance  = vPosition.z / vPosition.w;\n"
        "    normalizedDistance = (normalizedDistance + 1.0) / 2.0;\n"
        "    gl_FragColor = pack(normalizedDistance);\n"
        "}\n";



char phantomVertSource[] =
        "attribute vec4 osg_Vertex;\n"
        "uniform mat4 osg_ModelViewProjectionMatrix;\n"
        "uniform mat4 osg_ModelViewMatrix;\n"
        "uniform mat4 osg_ViewMatrixInverse;\n"
        #ifndef USE_OSG_SHADOWS
        "uniform mat4 lightMVP;\n"
        "varying vec4 shadowCoord;\n"
        #endif

        "void main(void)\n"
        "{\n"
        #ifndef USE_OSG_SHADOWS
        "  vec4 vPosition = osg_ModelViewMatrix * osg_Vertex;\n"
        "  vec4 posWorld = osg_ViewMatrixInverse * vPosition;\n"
        "  shadowCoord = lightMVP * posWorld;\n"
        #endif
        "  gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex;\n"
        "\n"
        "}\n";


char phantomFragSource[] =
        #if MOBILE_PLATFORM
        "precision highp float;\n"
        #endif
        #ifndef USE_OSG_SHADOWS
        "varying vec4 shadowCoord;\n"
        "uniform float ambientBias;\n"
        #endif
        "uniform sampler2D shadowTexture;\n"
        "uniform bool withShadows;\n"

        #ifndef USE_DEPTH_MAPS
        "float unpack(vec4 packedVal)\n"
        "{\n"
        "    vec4 unpackFcts = vec4(1.0/(256.0*256.0*256.0), 1.0/(256.0*256.0), 1.0/256.0, 1.0);\n"
        "    return dot(packedVal, unpackFcts);\n"
        "}\n"

        "vec4 unpackShadowMap()\n"
        "{\n"
        "    vec4 shadow = vec4(1.0);\n"
        "    vec4 shadowPos = shadowCoord / shadowCoord.w;\n"
        "    vec4 shadowTex = texture2D(shadowTexture, shadowPos.st);\n"
        "    float depth = unpack(shadowTex);\n"
        "    shadowPos = (shadowPos + 1.0) / 2.0;\n"
        "    //shadowPos.z -= 0.0005;\n"
        "    if(depth<shadowPos.z)\n"
        "        shadow *= 0.0;\n"
        "    shadow += vec4(ambientBias);\n"
        "    return shadow;\n"
        "}\n"
        #else
        "vec4 applyShadowMap()\n"
        "{\n"
        "    vec4 shadow = vec4(1.0);\n"
        "    if(shadowCoord.q > 0.0)\n"
        "    {\n"
        "        shadow = texture2DProj(shadowTexture, shadowCoord);\n"
        "    }\n"
        "    return (shadow + vec4(ambientBias));\n"
        "}\n"
        #endif

        "void main(void)\n"
        "{\n"
        "  vec4 color = vec4(1.0);\n"
        "  if(withShadows)\n"
        "  {\n"

        #ifdef USE_DEPTH_MAPS
        "      vec4 shadow = applyShadowMap();\n"
        #else
        "      vec4 shadow = unpackShadowMap();\n"
        #endif
        "      color *= shadow;\n"
        "  }\n"
        "  gl_FragColor = color;\n"
        "}\n";



char phongVertSource[] =
        "attribute vec4 osg_Vertex;\n"
        #ifndef USE_FIXED_PIPELINE
        "attribute vec3 osg_Normal;\n"
        "attribute vec4 osg_MultiTexCoord0;\n"
        #endif
        "uniform mat4 osg_ModelViewProjectionMatrix;\n"
        "uniform mat4 osg_ModelViewMatrix;\n"
        "uniform mat4 osg_ViewMatrixInverse;\n"
        "uniform mat3 osg_NormalMatrix;\n"

        "varying vec4 texCoord0;\n"
        "varying vec3 normal;\n"
        "varying vec3 vertexPos;\n"
        #ifndef USE_OSG_SHADOWS
        "varying vec4 shadowCoord;\n"
        "uniform mat4 lightMVP;\n"
        #endif

        "void main(void)\n"
        "{\n"
        "   vec4 vPosition = osg_ModelViewMatrix * osg_Vertex;\n"
        "   vertexPos = vPosition.xyz;\n"
        "   vec3 eNormal = vec3(osg_NormalMatrix *"
        #ifndef USE_FIXED_PIPELINE
        " osg_Normal"
        #else
        "gl_Normal"
        #endif
        ");\n"
        "   normal = normalize(eNormal);\n"
        #ifndef USE_FIXED_PIPELINE
        "   texCoord0 = osg_MultiTexCoord0;\n"
        #else
        "   texCoord0 = gl_MultiTexCoord0;\n"
        #endif
        #ifndef USE_OSG_SHADOWS
        "   vec4 posWorld = osg_ViewMatrixInverse * vPosition;\n"
        "   shadowCoord = lightMVP * posWorld;\n"
        #endif
        "   gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex;\n"
        "}\n";


char phongFragSource[] =
        #if MOBILE_PLATFORM
        "precision highp float;\n"
        #endif
        "#define MAX_LIGHTS 8\n"
        "\n"
        #ifndef USE_OSG_SHADOWS
        "varying vec4 shadowCoord;\n"
        "uniform float ambientBias;\n"
        #endif
        "varying vec4 texCoord0;\n"
        "varying vec3 normal;\n"
        "varying vec3 vertexPos;\n"
        "uniform mat4 osg_ModelViewMatrix;\n"
        "uniform mat4 osg_ViewMatrix;\n"
        "uniform sampler2D tex;\n"
        "uniform sampler2D shadowTexture;\n"
        "uniform int hasTexture;\n"
        "uniform bool withShadows;\n"
        "uniform int nbLights;\n"
        "\n"
        "struct LightSourceParameters \n"
        "{   \n"
        "   vec4 ambient;              // Aclarri   \n"
        "   vec4 diffuse;              // Dcli   \n"
        "   vec4 specular;             // Scli   \n"
        "   vec4 position;             // Ppli   \n"
        "   vec4 halfVector;           // Derived: Hi   \n"
        "   vec3 spotDirection;        // Sdli   \n"
        "   float spotExponent;        // Srli   \n"
        "   float spotCutoff;          // Crli                              \n"
        "                              // (range: [0.0,90.0], 180.0)   \n"
        "   float spotCosCutoff;       // Derived: cos(Crli)                 \n"
        "                              // (range: [1.0,0.0],-1.0)   \n"
        "   float constantAttenuation; // K0   \n"
        "   float linearAttenuation;   // K1   \n"
        "   float quadraticAttenuation;// K2  \n"
        "   int isInEyeSpace;\n"
        "   int isShadowCaster;\n"
        "};    \n"
        "\n"
        "struct MaterialParameters  \n"
        "{   \n"
        "   vec4 emission;    // Ecm   \n"
        "   vec4 ambient;     // Acm   \n"
        "   vec4 diffuse;     // Dcm   \n"
        "   vec4 specular;    // Scm   \n"
        "   float shininess;  // Srm  \n"
        "};  \n"
        "\n"
        "uniform LightSourceParameters LightSource[MAX_LIGHTS];\n"
        "uniform MaterialParameters material;\n"
        "\n"
        "float ambientScene = 0.0;\n"
        "float specularCoeff = 0.5;\n"
        "\n"

        #ifndef USE_DEPTH_MAPS
        "float unpack(vec4 packedVal)\n"
        "{\n"
        "    vec4 unpackFcts = vec4(1.0/(256.0*256.0*256.0), 1.0/(256.0*256.0), 1.0/256.0, 1.0);\n"
        "    return dot(packedVal, unpackFcts);\n"
        "}\n"

        "vec4 unpackShadowMap()\n"
        "{\n"
        "    vec4 shadow = vec4(1.0);\n"
        "    vec4 shadowPos = shadowCoord / shadowCoord.w;\n"
        "    vec4 shadowTex = texture2D(shadowTexture, shadowPos.st);\n"
        "    float depth = unpack(shadowTex);\n"
        "    shadowPos = (shadowPos + 1.0) / 2.0;\n"
        "    shadowPos.z -= 0.001;\n"
        "    if(depth<shadowPos.z)\n"
        "        shadow *= 0.0;\n"
        "    shadow += vec4(ambientBias);\n"
        "    return shadow;\n"
        "}\n"
        #else
        "vec4 applyShadowMap()\n"
        "{\n"
        "    vec4 shadow = vec4(1.0);\n"
        "    if(shadowCoord.q > 0.0)\n"
        "    {\n"
        "        shadow = texture2DProj(shadowTexture, shadowCoord);\n"
        "    }\n"
        "    return (shadow + vec4(ambientBias));\n"
        "}\n"
        #endif

        "\n"
        "vec4 computeAmbient(vec4 matAmb, vec4 lightAmb)\n"
        "{\n"
        "   vec4 ambColor = matAmb * lightAmb;\n"
        "   return ambColor;\n"
        "}\n"
        "\n"
        "vec4 computeDiffuse(vec4 matDiff, vec4 lightDiff, float attenuation, vec3 D, vec3 N)\n"
        "{\n"
        "    float lambert = dot(N, D);\n"
        "    vec4 diffColor = clamp(matDiff * lightDiff * max(lambert, 0.0), 0.0, 1.0) * attenuation; \n"
        "    return diffColor;\n"
        "}\n"
        "\n"
        "vec4 computeSpecular(vec4 matSpec, vec4 lightSpec, float attenuation, float shininess, vec3 D, vec3 N)\n"
        "{\n"
        "    vec3 E = normalize(-vertexPos);\n"
        "    vec3 R = normalize(reflect(-D, N));\n"
        "    vec4 specColor = clamp(attenuation * matSpec * lightSpec * pow(max(dot(R, E), 0.0), specularCoeff*shininess), 0.0, 1.0);\n"
        "    return specColor;\n"
        "}\n"
        "\n"
        "float computePointLightAttenuation(LightSourceParameters light, float dist)\n"
        "{\n"
        "    return (1.0 / (light.constantAttenuation + light.linearAttenuation*dist + light.quadraticAttenuation*dist*dist));\n"
        "}\n"
        "\n"
        "float computeSpotLightAttenuation(vec3 lightDir, LightSourceParameters light)\n"
        "{\n"
        "    float attenuation = 0.0;\n"
        "    vec4 spotDir4 = osg_ViewMatrix*vec4(light.spotDirection, 1.0);\n"
        "    vec3 spotDir = normalize(spotDir4.xyz);\n"
        "    float clampedCosine = dot(spotDir, -lightDir);\n"
        "    if (clampedCosine > light.spotCosCutoff) // inside of spotlight cone?\n"
        "    {\n"
        "       attenuation = pow(clampedCosine, light.spotExponent);  \n"
        "    }\n"
        "    return attenuation;\n"
        "}\n"
        "\n"
        "\n"
        "void main(void)\n"
        "{\n"
        "    vec4 diffuse = vec4(0.0);\n"
        "    vec4 specular = vec4(0.0);\n"
        "    vec4 ambient = vec4(0.0);\n"
        "    float shininess = 0.0;\n"
        "    vec3 N = normalize(normal);\n"
        "    vec4 color = vec4(ambientScene);\n"
        "    \n"
        "    if(hasTexture == 1) // textured object: we use the texture as diffuse and don't set any ambient and specular for the moment\n"
        "    {\n"
        "         diffuse = texture2D(tex, texCoord0.st);\n"
        "    }\n"
        "    else\n"
        "    {\n"
        "         diffuse  = material.diffuse;\n"
        "         specular = material.specular;\n"
        "         ambient = material.ambient;\n"
        "         shininess = material.shininess;\n"
        "    }\n"
        "    \n"
        "    for(int i=0; i<MAX_LIGHTS; i++)\n"
        "    {\n"
        "        if(i==nbLights) break;\n"
        "        vec4 lightPos;\n"
        "        vec3 lightDir;\n"
        "        float attenuation = 0.0;\n"
        "        if(LightSource[i].isInEyeSpace == 1)\n"
        "        {\n"
        "            lightPos = LightSource[i].position;\n"
        "        }\n"
        "        else\n"
        "        {\n"
        "            lightPos = osg_ViewMatrix*LightSource[i].position;\n"
        "        }\n"
        "        \n"
        "        if(LightSource[i].position.w == 0.0) // directional light\n"
        "        {\n"
        "            lightDir = normalize(lightPos.xyz);\n"
        "            attenuation = 1.0;\n"
        "        }\n"
        "        else // point/spot light\n"
        "        {\n"
        "            lightDir = lightPos.xyz - vertexPos;\n"
        "            float dist = length(lightDir);\n"
        "            lightDir = normalize(lightDir);\n"
        "            attenuation = computePointLightAttenuation(LightSource[i], dist);\n"
        "            if (LightSource[i].spotCutoff <= 90.0) // spotlight?\n"
        "            {\n"
        "                attenuation *= computeSpotLightAttenuation(lightDir, LightSource[i]);\n"
        "            }\n"
        "        }\n"
        "        \n"
        "        // Ambient term\n"
        "        color += computeAmbient(ambient, LightSource[i].ambient);\n"
        "    \n"
        "        // Lambert term (diffuse)\n"
        "        color += computeDiffuse(diffuse, LightSource[i].diffuse, attenuation, lightDir, N);\n"
        "    \n"
        "        // Specular term\n"
        "        color += computeSpecular(specular, LightSource[i].specular, attenuation, shininess, lightDir, N);\n"
        "    }\n"
        "    if(withShadows)\n"
        "    {\n"
        #ifdef USE_DEPTH_MAPS
        "      vec4 shadow = applyShadowMap();\n"
        #else
        "      vec4 shadow = unpackShadowMap();\n"
        #endif
        "      color *= shadow;\n"
        "    }\n"
        "    gl_FragColor = color;\n"
        "}\n";



bool ShaderManager::loadShaderSource(osg::Shader* obj, const std::string& fileName)
{
    std::string fqFileName = osgDB::findDataFile(fileName);
    if(fqFileName.length() == 0)
    {
        qCritical() << "File \"" << fileName.c_str() << "\" not found.";
        return false;
    }
    else if (!obj->loadShaderSourceFromFile(fqFileName))
    {
        qCritical() << "Could not load file: " << fileName.c_str();
        return false;
    }

    return true;
}


void ShaderManager::instanciateWLShaderSources(osg::Program* program)
{
    if(program)
    {
#if !defined(USE_FIXED_PIPELINE)
        osg::ref_ptr<osg::Shader> vertShader = new osg::Shader(osg::Shader::VERTEX, wlVertSource);
        program->addShader(vertShader.get());
#endif
        osg::ref_ptr<osg::Shader> fragShader = new osg::Shader(osg::Shader::FRAGMENT, wlFragSource);
        program->addShader(fragShader.get());
    }
}


void ShaderManager::instanciateWLShaderUniforms(osg::StateSet* stateSet, BaseImage* image)
{
    if(image && stateSet)
    {
        osg::Uniform* colorHeight = new osg::Uniform("colorHeight", image->getColorHeight());
        osg::Uniform* colorWidth = new osg::Uniform("colorWidth", image->getColorWidth());
#if !defined(MOBILE_PLATFORM)
        colorHeight->setUpdateCallback(new updateHeight(image));
        colorWidth->setUpdateCallback(new updateWidth(image));
#endif
        stateSet->addUniform(colorHeight);
        stateSet->addUniform(colorWidth);
        stateSet->addUniform(new osg::Uniform("wDef", (float)(image->getWDef())));
        stateSet->addUniform(new osg::Uniform("hDef", (float)(image->getHDef())));
    }
}

void ShaderManager::addWindowLevel(osg::Node *node, BaseImage *img)
{
    if(node)
    {
        osg::ref_ptr<osg::StateSet> shaderStateSet = node->getOrCreateStateSet();
        osg::ref_ptr<osg::Program> program = new osg::Program;
        program->setName("Window Level shader");
        ShaderManager::instanciateWLShaderSources(program.get());
        ShaderManager::instanciateWLShaderUniforms(shaderStateSet.get(), img);
        shaderStateSet->setAttributeAndModes(program, osg::StateAttribute::ON);
    }
}

void ShaderManager::instanciateShadowShaders(osg::Program *program)
{
    if(program)
    {

        osg::ref_ptr<osg::Shader> vertShader;
        osg::ref_ptr<osg::Shader> fragShader;

        /* Set to 0 to use shaders from a source file located in the include folder
         * Set to 1 to load the shaders from the const char[] located in this file
         */
#if USE_EMBEDDED_SHADERS
        vertShader = new osg::Shader(osg::Shader::VERTEX, shadowVertSource);
        fragShader = new osg::Shader(osg::Shader::FRAGMENT, shadowFragSource);
#else
        vertShader = new osg::Shader(osg::Shader::VERTEX);
        fragShader = new osg::Shader(osg::Shader::FRAGMENT);
        const std::string vertStr(getIncludePath() + "/Shaders/vShadow.glsl");
        const std::string fragStr(getIncludePath() + "/Shaders/fShadow.glsl");
        loadShaderSource(vertShader.get(), vertStr);
        loadShaderSource(fragShader.get(), fragStr);
#endif
        program->addShader(vertShader.get());
        program->addShader(fragShader.get());
    }
}

void ShaderManager::instanciateDefaultShaders(osg::Program *program)
{
    if(program)
    {

        osg::ref_ptr<osg::Shader> vertShader;
        osg::ref_ptr<osg::Shader> fragShader;

        /* Set to 0 to use shaders from a source file located in the include folder
         * Set to 1 to load the shaders from the const char[] located in this file
         */
#if USE_EMBEDDED_SHADERS
        vertShader = new osg::Shader(osg::Shader::VERTEX, phongVertSource);
        fragShader = new osg::Shader(osg::Shader::FRAGMENT, phongFragSource);
#else
        vertShader = new osg::Shader(osg::Shader::VERTEX);
        fragShader = new osg::Shader(osg::Shader::FRAGMENT);
        const std::string vertStr(getIncludePath() + "/Shaders/vertShader.glsl");
        const std::string fragStr(getIncludePath() + "/Shaders/fragShader.glsl");
        loadShaderSource(vertShader.get(), vertStr);
        loadShaderSource(fragShader.get(), fragStr);
#endif
        program->addShader(vertShader.get());
        program->addShader(fragShader.get());
    }
}


void ShaderManager::instanciatePhantomShaders(osg::Program *program)
{
    if(program)
    {
        osg::ref_ptr<osg::Shader> vertShader;
        osg::ref_ptr<osg::Shader> fragShader;

        /* Set to 0 to use shaders from a source file located in the include folder
         * Set to 1 to load the shaders from the const char[] located in this file
         */
#if USE_EMBEDDED_SHADERS
        vertShader = new osg::Shader(osg::Shader::VERTEX, phantomVertSource);
        fragShader = new osg::Shader(osg::Shader::FRAGMENT, phantomFragSource);
#else
        vertShader = new osg::Shader(osg::Shader::VERTEX);
        fragShader = new osg::Shader(osg::Shader::FRAGMENT);
        const std::string vertStr(getIncludePath() + "/Shaders/vertShader.glsl");
        const std::string fragStr(getIncludePath() + "/Shaders/fragPhantom.glsl");
        loadShaderSource(vertShader.get(), vertStr);
        loadShaderSource(fragShader.get(), fragStr);
#endif
        program->addShader(vertShader.get());
        program->addShader(fragShader.get());
    }
}


osg::Program* ShaderManager::addDefaultShaders(osg::Node *node)
{
    if(node)
    {
        osg::ref_ptr<osg::StateSet> shaderStateSet = node->getOrCreateStateSet();
        osg::ref_ptr<osg::Program> program = new osg::Program;
        program->setName("Default shader");
        ShaderManager::instanciateDefaultShaders(program.get());
        shaderStateSet->setAttributeAndModes(program, osg::StateAttribute::ON);
        return program.get();
    }
    return NULL;
}


osg::Program *ShaderManager::addPhantomShaders(osg::Node *node)
{
    if(node)
    {
        osg::ref_ptr<osg::StateSet> shaderStateSet = node->getOrCreateStateSet();
        osg::ref_ptr<osg::Program> program = new osg::Program;
        program->setName("Phantom shader");
        ShaderManager::instanciatePhantomShaders(program.get());
        shaderStateSet->setAttributeAndModes(program, osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
        return program.get();
    }
    return NULL;
}
