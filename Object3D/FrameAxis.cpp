/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifdef WIN32
#define _USE_MATH_DEFINES
#endif

#include "FrameAxis.h"
#include <cmath>
#include <osg/Node>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>
#include <osg/LineWidth>
#include <osg/StateAttribute>

using namespace osg;
using namespace PoLAR;


FrameAxis::FrameAxis():
    _lighting(false)
{
    // X axis
    this->addChild((createFrameAxis(1.0,0.0,0.0)));
    // Y axis
    this->addChild((createFrameAxis(0.0,1.0,0.0)));
    // Z axis
    this->addChild((createFrameAxis(0.0,0.0,1.0)));
    this->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
}

FrameAxis::~FrameAxis() {}


void FrameAxis::setLightingOn()
{
    _lighting = true;
    this->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::ON);
}


void FrameAxis::setLightingOff()
{
    _lighting = false;
    this->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
}


Group* FrameAxis::createFrameAxis(float x, float y, float z)
{
    Group *axisGroup = new Group;
    Geode* conegeode = new Geode;
    // Create a cone.
    {
        Cone* cone = new Cone (Vec3(x, y, z), 0.1f, 0.2f);
        cone->setRotation(Quat(M_PI/2,Vec3f(-y,x,0.0)));
        ShapeDrawable *coneShape = new ShapeDrawable(cone);
        coneShape->setColor(Vec4(x,y,z,1.0f));
        conegeode->addDrawable(coneShape);
    }
    conegeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, StateAttribute::ON);

    Geode* lineGeode = new Geode;
    // Create a line.
    {
        Geometry* geometry = new Geometry();

        Vec3Array* vertices = new Vec3Array(2);
        (*vertices)[0] = Vec3(0.0,0.0,0.0);
        (*vertices)[1] = Vec3(x,y,z);

        geometry->setVertexArray(vertices);
        geometry->addPrimitiveSet(new DrawArrays(PrimitiveSet::LINES,0,2));

        Vec4Array* colors = new osg::Vec4Array;
        colors->push_back(osg::Vec4(x, y, z, 1.0f));
        geometry->setColorArray(colors);

        lineGeode->addDrawable(geometry);
    }

    LineWidth* linewidth = new LineWidth();
    linewidth->setWidth(2.0f);
    lineGeode->getOrCreateStateSet()->setAttributeAndModes(linewidth, StateAttribute::ON);
    lineGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, StateAttribute::ON);
    lineGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);

    std::string name;
    if(x > 0.0) name = std::string("World X axis");
    else if (y > 0.0) name = std::string("World Y axis");
    else if (z > 0.0) name = std::string("World Z axis");
    conegeode->setName(name);
    lineGeode->setName(name);

    axisGroup->addChild(conegeode);
    axisGroup->addChild(lineGeode);
    createMaterial(axisGroup, x, y, z);
    return axisGroup;
}


void FrameAxis::createMaterial(Group *group, float x, float y, float z)
{
    ref_ptr<Material> material = dynamic_cast< Material* > (group->getOrCreateStateSet()->getAttribute(StateAttribute::MATERIAL));
    if (!material.valid()) material = new Material;
    material->setDiffuse(Material::FRONT_AND_BACK, Vec4(x, y, z, 1.0));
    group->getOrCreateStateSet()->setAttributeAndModes(material.get());
#ifdef USE_PHONG_SHADING
    createMaterialUniforms(group, material.get());
#endif
}


void FrameAxis::createMaterialUniforms(Group *group, Material* material)
{
    if(material)
    {
        group->getOrCreateStateSet()->addUniform(new Uniform("material.emission", material->getEmission(Material::FRONT)));
        group->getOrCreateStateSet()->addUniform(new Uniform("material.ambient", material->getAmbient(Material::FRONT)));
        group->getOrCreateStateSet()->addUniform(new Uniform("material.diffuse", material->getDiffuse(Material::FRONT)));
        group->getOrCreateStateSet()->addUniform(new Uniform("material.specular", material->getSpecular(Material::FRONT)));
        group->getOrCreateStateSet()->addUniform(new Uniform("material.shininess", material->getShininess(Material::FRONT)));
    }

    group->getOrCreateStateSet()->addUniform(new Uniform("hasTexture", false));

}
