/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#ifdef WIN32
#define _USE_MATH_DEFINES
#endif

#include "Util.h"
#include <cmath>
#include <cstdio>
#include <iostream>
#include <osg/io_utils>
#include <osg/Geode>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <QFileInfo>
#include <QDebug>

using namespace PoLAR;

osg::Node* Util::loadFromFile(const std::string& filename)
{
    osg::Node* node = osgDB::readNodeFile(filename);
    if(!node)
    {
        qCritical() << "Unable to load model" << QString::fromStdString(filename);
        return NULL;
    }
    return node;
}


bool Util::fileExists(const std::string& filename)
{
    QFileInfo file(QString::fromStdString(filename));
    if(file.exists() && file.isFile())
        return true;
    return false;
}

double Util::computeDist(osg::Vec3f point1, osg::Vec3f point2)
{
    double v1 = Util::sqr(point2.x()-point1.x());
    double v2 = Util::sqr(point2.y()-point1.y());
    double v3 = Util::sqr(point2.z()-point1.z());
    return (sqrt(v1+v2+v3));
}

void Util::initvec2(double *a, double x, double y)
{
    a[0] = x;
    a[1] = y;
}

double Util::sqrdist2(double *a, double *b)
{
    return (a[0]-b[0])*(a[0]-b[0])+(a[1]-b[1])*(a[1]-b[1]);
}


double Util::determinant3(osg::Matrixd& M, int ind1, int ind2, int ind3)
{
    double det = (M(0,ind1)*(M(1,ind2)*M(2,ind3)-M(2,ind2)*M(1,ind3))
                  - M(1,ind1)*(M(0,ind2)*M(2,ind3)-M(2,ind2)*M(0,ind3))
                  + M(2,ind1)*(M(0,ind2)*M(1,ind3)-M(1,ind2)*M(0,ind3)));
    return det;
}

double Util::pscal3(osg::Matrixd& M, int ind1, int ind2)
{
    double ret = M(0,ind1)*M(0,ind2) + M(1,ind1)*M(1,ind2) + M(2,ind1)*M(2,ind2);
    return ret;
}

double Util::pscal3(osg::Matrixd& M1, osg::Matrixd& M2, int ind1, int ind2)
{
    double ret = M1(0,ind1)*M2(0,ind2) + M1(1,ind1)*M2(1,ind2) + M1(2,ind1)*M2(2,ind2);
    return ret;
}


void Util::vecprod3(osg::Matrixd& M, int ind1, int ind2, int ind3)
{
    M(0,ind1) = M(1,ind2)*M(2,ind3) - M(1,ind3)*M(2,ind2);
    M(1,ind1) = M(0,ind3)*M(2,ind2) - M(0,ind2)*M(2,ind3);
    M(2,ind1) = M(0,ind2)*M(1,ind3) - M(0,ind3)*M(1,ind2);
}


osg::Matrix3d Util::multiply33(osg::Matrix3d& A, osg::Matrix3d& B)
{
    osg::Matrix3d M;
    const unsigned int dim = 3;
    for(unsigned int i=0; i<dim; i++)
        for(unsigned int j=0; j<dim; j++)
            for(unsigned int k=0; k<dim; k++)
    {
       M(i, j) += A(i, k) * B(k, j);
    }
    return M;
}

osg::Matrixd Util::transpose(const osg::Matrixd &M)
{
    osg::Matrixd R;
    for(unsigned int i=0; i<4; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            R(i, j) = M(j, i);
        }
    }
    return R;
}


osg::Matrixd Util::subtract(osg::Matrixd &M1, osg::Matrixd &M2)
{
    osg::Matrixd P;
    for(unsigned int i=0; i<4; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            P(i, j) = M1(i, j) - M2(i, j);
        }
    }
    return P;
}

void Util::normalizeProjectionMatrix(osg::Matrixd& M)
{
    float scale = (float)(sqrt(M(0,2)*M(0,2) + M(1,2)*M(1,2) + M(2,2)*M(2,2)));
    if(!Null(scale))
    {
        double det = Util::determinant3(M, 0, 1, 2);
        if(det < 0) scale = -scale;
        M = M * osg::Matrix::scale(1/scale, 1/scale, 1/scale);
    }
}

osg::Vec3 Util::calculateNormal(osg::Vec3 v1, osg::Vec3 v2, osg::Vec3 v3)
{
    osg::Vec3 edge1 = v2 - v1, edge2 = v2 - v3;

    // cross product following the right hand rule. Note that OSG redefines the ^ operator for this usage
    return edge2 ^ edge1;
}


/*
 * More exact computation of the extrinsic parameters by using Toscani's equations
 * when assuming the projection matrix has 11 parameters. Only one difference :
 * Toscani assesses that Tz is positive, meaning that its absolute coordinates (world)
 * are chosen in this way. This choice does not seem wise because when the camera moves
 * relative to the scene, the origin of the scene could be found behind the camera. In this case
 * Tz is negative. It seems more stable to assess det(A) > 0, with A the 3x3 part of the
 * projection matrix (proportional to KR). This is the normaliaztion chosen here.
 * */
void Util:: projectionWc2vcToscani(osg::Matrixd& M, osg::Matrixd& wc2vc)
{
    unsigned int i;
    double kk = Util::pscal3(M, 2, 2);
    double k = sqrt(kk);
    double dSURe, cSURb, alphaV, alphaU, g;

    if(Util::determinant3(M, 0, 1, 2) < 0) k = -k;

    for(i=0; i<=3; i++)
    {
        wc2vc(i,2) = M(i,2)/k;
    }

    g = Util::pscal3(M,0,2)/kk;
    dSURe = -Util::pscal3(M,1,2)/kk;
    alphaV = sqrt(Util::pscal3(M,1,1)/kk-dSURe*dSURe);

    /*
     * some simplifications are made on Toscani's equations by putting 1/alphaV as a factor
     *  to avoid additional computations
     */
    for(i=0; i<=3; i++)
    {
        wc2vc(i,1) = (M(i,1)+dSURe*M(i,2))/(k*alphaV);
    }

    /*
     * we are sure that the matrix will be orthogonal. We can thus calculate the last vector
     * by a simple vectorial product
     * */
    Util::vecprod3(wc2vc,0,1,2);

    cSURb = -(Util::pscal3(M,0,1)/kk + g*dSURe) / (alphaV*alphaV);
    alphaU = sqrt(Util::pscal3(M,0,0)/kk - cSURb*cSURb*alphaV*alphaV - g*g);

    /*
     * Only Tz is missing, we can take simpler equations
     */
    wc2vc(3,0) = (M(3,0) - Util::pscal3(M, wc2vc, 0, 1) * wc2vc(3,1) - Util::pscal3(M, wc2vc, 0, 2) * wc2vc(3,2)) / (k*alphaU);



}


osg::Matrixd Util::projectionVc2wcFromWc2vc(osg::Matrixd& wc2vc)
{
    osg::Matrixd result;
    osg::Vec3d tRT;
    osg::Vec3d T;

    osg::Matrixd R(wc2vc);
    T[0] = wc2vc(3,0);
    T[1] = wc2vc(3,1);
    T[2] = wc2vc(3,2);

    tRT = osg::Matrix::transform3x3(R,T);

    result.set(R(0,0), R(1,0), R(2,0), 0,
               R(0,1), R(1,1), R(2,1), 0,
               R(0,2), R(1,2), R(2,2), 0,
               -tRT[0], -tRT[1], -tRT[2], 1);

    return result;
}


/* combine a rigid motion with a projection matrix: the rotation (rot) and
    translation (trans) given as arguments are the rigid motion applied to the
    3D points IN THE WORLD FRAME */
/* the input projection is modified and returned. When you don't want the input
    projection matrix to be modified, just make a call to:
    Projection new_proj = ProjectionCombineRigidMotion(ProjectionCopy(P), rot, trans);
 */
osg::Matrixd Util::projectionCombineRigidMotion(osg::Matrixd& P, osg::Matrixd &rot, osg::Vec3d& trans)
{
    osg::Matrixd mat = rot * P;

    mat(3,0) = 0;
    mat(3,1) = 0;
    mat(3,2) = 0;

    osg::Vec3d vec = osg::Matrix::transform3x3(trans, P);

    for(int i=0;i<=2;i++)
        for(int j=0;j<=2;j++)
            P(i,j) = mat(i,j);

    P(3,0) += vec[0];
    P(3,1) += vec[1];
    P(3,2) += vec[2];

    return P;
}


void Util::saveProjectionMatrix(osg::Matrixd& M, const char *out)
{
#if !defined(__GNUC__) && ( defined(WIN32) || defined(_WIN32) )
    FILE *fp;
    errno_t err = fopen_s(&fp, out, "w");
    if (err)
#else
    FILE *fp = fopen(out, "w");
    if (fp==NULL)
#endif
    {
        qCritical() << "Error loading file: " << out;
        return;
    }

    for (unsigned int i = 0; i < 3; i++)
        fprintf(fp, "%.20f %.20f %.20f %.20f\n", M(0,i), M(1,i), M(2,i), M(3,i));
    fclose(fp);
}


void Util::saveProjectionMatrix(osg::Matrixd &M, QString s)
{
    saveProjectionMatrix(M, s.toStdString().c_str());
}


osg::Matrixd Util::readProjectionMatrix(const char* fileName)
{
    osg::Matrixd result;

#if !defined(__GNUC__) && ( defined(WIN32) || defined(_WIN32) )
    FILE *file;
    errno_t err = fopen_s(&file, fileName, "r");
    if (err)
#else
    FILE *file = fopen(fileName, "r");
    if (file == NULL)
#endif
    {
        qCritical() << fileName << ": file does not exist";
        // we should raise an exception or put a particular matrix to test if it has been loaded
        return result;
    }

    double val;

    for(unsigned int i=0; i<3; i++)
        for(unsigned int j=0; j<4; j++)
        {
#if !defined(__GNUC__) && ( defined(WIN32) || defined(_WIN32) )
            if (fscanf_s(file, "%lf", &(val)) != 1)
#else
            if(fscanf(file, "%lf", &(val)) != 1)
#endif
            {
                fclose(file);
                qCritical() << fileName << ": file corrupted";
                return result;
            }
            else result(j,i) = val;
        }
    fclose(file);
    return result;
}


osg::Matrixd Util::readProjectionMatrix(QString s)
{
    return readProjectionMatrix(s.toStdString().c_str());
}


osg::Matrixd Util::projectionSetParameters(double u0, double v0,
                                           double alphaU, double alphaV,
                                           double alpha, double beta, double gamma,
                                           double tx, double ty, double tz)
{
    osg::Matrixd result;
    osg::Matrixd A;
    osg::Matrixd wc2vc;
    osg::Matrixd Ralpha;
    osg::Matrixd Rbeta;
    osg::Matrixd Rgamma;
    osg::Matrixd t1;
    osg::Matrixd R;
    unsigned int i,j;

    A(0,0) = alphaU; A(1,0) = 0.;     A(2,0) = u0;
    A(0,1) = 0.;     A(1,1) = alphaV; A(2,1) = v0;
    A(0,2) = A(1,2) = 0.;             A(2,2) = 1.;


    Ralpha(0,0) = cos(alpha); Ralpha(1,0) = -sin(alpha); Ralpha(2,0) = 0.0;
    Ralpha(0,1) = sin(alpha); Ralpha(1,1) =  cos(alpha); Ralpha(2,1) = 0.0;
    Ralpha(0,2) = 0.0;        Ralpha(1,2) = 0.0;         Ralpha(2,2) = 1.0;

    Rbeta(0,0) = cos(beta);   Rbeta(1,0)  = 0.0;         Rbeta(2,0)  = -sin(beta);
    Rbeta(0,1) = 0.0;         Rbeta(1,1)  = 1.0;         Rbeta(2,1)  = 0.0;
    Rbeta(0,2) = sin(beta);   Rbeta(1,2)  = 0.0;         Rbeta(2,2)  = cos(beta);

    Rgamma(0,0) = 1.0;        Rgamma(1,0) = 0.0;         Rgamma(2,0) = 0.0;
    Rgamma(0,1) = 0.0;        Rgamma(1,1) = cos(gamma);  Rgamma(2,1) = -sin(gamma);
    Rgamma(0,2) = 0.0;        Rgamma(1,2) = sin(gamma);  Rgamma(2,2) = cos(gamma);

    t1 = Ralpha * Rbeta;
    R = t1 * Rgamma;

    for(i = 0; i < 3; i++)
        for(j = 0; j < 3; j++)
            wc2vc(i,j) = R(i,j);
    wc2vc(3,0) = tx;
    wc2vc(3,1) = ty;
    wc2vc(3,2) = tz;

    result = wc2vc * A;

    return result;
}


osg::Vec3d Util::multVec3(osg::Vec3d v1, osg::Vec3d v2)
{
    return osg::Vec3d(v1.x() * v2.x(), v1.y() * v2.y(), v1.z() * v2.z());
}


osg::Node* Util::generatePlaneFromNormal(const osg::Vec4 &plane)
{
    osg::Vec3 n(plane.x(),plane.y(),plane.z());
    n.normalize();
    float d (plane.w());
    osg::Vec3 v (1.f,0,0);

    //osg::Vec3 u1 = v -n*(v.x()*n.x() +v.y()*n.y() + v.z()*n.z());
    osg::Vec3 u1 = v ^ n;
    osg::Vec3 u2;
    if (u1.length()==0)
    {
        u1 = osg::Vec3(0.f,1.f,0.f);
        u2 = osg::Vec3(0.f,0.f,1.f);
    }
    else
    {
        u1.normalize();
        u2 = n^u1;
        u2.normalize();
    }

    osg::Vec3 p =  n * d;

    osg::ref_ptr<osg::Geode> groundPlane = new osg::Geode;
    osg::Geometry* groundGeom = new osg::Geometry;
    groundPlane->addDrawable(groundGeom);
    osg::ref_ptr<osg::Vec3Array> vertarray = new osg::Vec3Array;
    groundGeom->setVertexArray( vertarray.get() );

    int width(30);

    const int nVerts(4*width+2);
    for(int i = -width; i < width; i++)
    {
        for(int j = -width; j < width+1; j++)
        {
            vertarray->push_back(p + u1*i + u2*j);
            vertarray->push_back(p + u1*(i+1) + u2*j);
        }
        groundGeom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLE_STRIP, (i+width)*nVerts, nVerts));
    }

    osg::ref_ptr<osg::Vec3Array> norm = new osg::Vec3Array;
    groundGeom->setNormalArray(norm.get());
    norm->push_back(n);
    groundGeom->setNormalBinding(osg::Geometry::BIND_OVERALL);

    osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;
    groundGeom->setColorArray(c.get());
    c->push_back(osg::Vec4(1.f, 1.f, 1.f, 1.f));
    groundGeom->setColorBinding(osg::Geometry::BIND_OVERALL);

    return groundPlane.release();
}


bool Util::isFixedFunctionPipelineAvailable()
{
#ifdef USE_FIXED_PIPELINE
    return true;
#else
    return false;
#endif
}
