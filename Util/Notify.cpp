#include "Notify.h"
#include <QDebug>

using namespace PoLAR;

void QDebugNotifyHandler::notify(osg::NotifySeverity severity, const char *message)
{
    if (severity <= osg::WARN)
        qWarning() << QString::fromLatin1(message);
    else
        qDebug() << QString::fromLatin1(message);
}
