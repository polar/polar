#include "TaggedPolygon2D.h"
#include <cmath>

using namespace PoLAR;


TaggedPolygon2D::TaggedPolygon2D(std::list<Marker2D *> &list)
    : Polygon2D(list)
{
}

TaggedPolygon2D::TaggedPolygon2D()
    : Polygon2D()
{
}

TaggedPolygon2D::TaggedPolygon2D(TaggedPolygon2D &obj):
    Polygon2D(obj)
{
}

// The integer part of the marker label is modified
// to contain the index of the segment which it belongs to
void TaggedPolygon2D::addMarker(Marker2D *marker)
{
    Polygon2D::addMarker(marker);
    if (_listMarkers.size() < 2 || _currentMarker == _listMarkers.end())
    {
        setCurrentIndex(0);
    }
    else
    {
        int i = (_currentMarker != _listMarkers.end()) ? (int)floor((*_currentMarker)->label) : 0;
        setCurrentIndex(i);
    }
}

void TaggedPolygon2D::setCurrentIndex(int i)
{
    if (_currentMarker != _listMarkers.end())
    {
        float real = (*_currentMarker)->label - float(floor((*_currentMarker)->label));
        (*_currentMarker)->label = i+real;
    }
}
