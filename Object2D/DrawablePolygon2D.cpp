/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DrawablePolygon2D.h"
#include "Viewer2D.h"

using namespace PoLAR;

REGISTER_TYPE(DrawablePolygon2D)


DrawablePolygon2D::DrawablePolygon2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = new Polygon2D(list);
    setName("DrawablePolygon2D");
    setType(DrawableObject2D::Polygon);
}


DrawablePolygon2D::DrawablePolygon2D(Viewer2D *Parent,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = new Polygon2D();
    setName("DrawablePolygon2D");
    setType(DrawableObject2D::Polygon);
}


DrawablePolygon2D::DrawablePolygon2D(Viewer2D *Parent,  Polygon2D *object,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = object;
    setName("DrawablePolygon2D");
    setType(DrawableObject2D::Polygon);
}

DrawablePolygon2D::DrawablePolygon2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                     QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                                     MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Polygon2D(list);
    setName("DrawablePolygon2D");
    setType(DrawableObject2D::Polygon);
}

DrawablePolygon2D::DrawablePolygon2D(Viewer2D *Parent, QPen& unselectPen,
                                     QPen& selectPen, QPen& selectMarkerPen,
                                     MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Polygon2D();
    setName("DrawablePolygon2D");
    setType(DrawableObject2D::Polygon);
}

DrawablePolygon2D::DrawablePolygon2D(Viewer2D *Parent, Polygon2D *object, QPen& unselectPen,
                                     QPen& selectPen, QPen& selectMarkerPen,
                                     MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = object;
    setName("DrawablePolygon2D");
    setType(DrawableObject2D::Polygon);
}

DrawablePolygon2D::DrawablePolygon2D(DrawablePolygon2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy)
{
}


void DrawablePolygon2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!isDisplayed()) return;

    if (_object->nbMarkers() >= 2) // we must have at least 2 points
    {
        QPen pen = (isSelected() && isEditable() ? _penList[DrawableObject2D::Selected] : _penList[DrawableObject2D::Unselected]);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);

        std::list<Marker2D*> list = _object->getMarkers();
        std::list<Marker2D*>::iterator tmp = list.begin();
        Marker2D *origin = list.front();

        QPainterPath path;

        for(unsigned int index=0; index<list.size()-1; ++index)
        {
            double x1, y1, x2, y2;
            transformCoord((*tmp)->x(), (*tmp)->y(), x1, y1);
            ++tmp;
            transformCoord((*tmp)->x(), (*tmp)->y(), x2, y2);

            /*QPainterPath path;
            path.moveTo(x1, y1);
            path.lineTo(x2, y2);
            newPath.addPath(path);
            painter->drawPath(path);
            */
            path.moveTo(x1, y1);
            path.lineTo(x2, y2);
        }

        // close the polygon
        if (_object->nbMarkers() > 2)
        {
            double x1, y1, x2, y2;
            transformCoord((*tmp)->x(), (*tmp)->y(), x1, y1);
            transformCoord(origin->x(), origin->y(), x2, y2);

            /*QPainterPath path;
            path.moveTo(x1, y1);
            path.lineTo(x2, y2);
            newPath.addPath(path);
            painter->drawPath(path);
            */
            path.moveTo(x1, y1);
            path.lineTo(x2, y2);
        }
        painter->drawPath(path);
        updateShapePath(path);
    }
    DrawableObject2D::paint(painter, option, widget);
}


void DrawablePolygon2D::swap(DrawablePolygon2D &first, DrawablePolygon2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
}

DrawablePolygon2D& DrawablePolygon2D::operator=(DrawablePolygon2D obj)
{
    swap(*this, obj);
    return *this;
}

QPointF DrawablePolygon2D::getCoord(QPointF &point) const
{
    QPointF ret;
    _viewer->ImageToQt(point.x(), point.y(), ret.rx(), ret.ry());
    return ret;
}
