﻿/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Blob2D.h"
#include "Bezier.h"
#include "Util.h"
#include <cstring>
#include <algorithm>
#include <iterator>
#include <QDebug>

using namespace PoLAR;
using namespace Util;

Blob2D::Blob2D(std::list<Marker2D*> list)
    : Polygon2D(list),
      ctrlpts(NULL),
      lengths(NULL)
{
    _makeCtrlPoints();
}


Blob2D::Blob2D()
    : Polygon2D(),
      ctrlpts(NULL),
      lengths(NULL)
{
    _makeCtrlPoints();
}


Blob2D::Blob2D(Blob2D &obj):
    Polygon2D(obj),
    ctrlpts(NULL),
    lengths(NULL)
{
  _makeCtrlPoints();
}



Blob2D::~Blob2D()
{
    if (ctrlpts) delete []ctrlpts;
    if (lengths) delete []lengths;
}

void Blob2D::setMarkers(std::list<Marker2D *> &list, bool destroyMarkers)
{
    Polygon2D::setMarkers(list, destroyMarkers);
    _makeCtrlPoints();
}

void Blob2D::_makeCtrlPoints()
{
    if (ctrlpts) delete []ctrlpts;
    if (lengths) delete []lengths;

    if (_listMarkers.empty())
    {
        ctrlpts = NULL, lengths = NULL;
    }
    else
    {
        int i=0;
        ctrlpts = new double [(3*_nbMarkers+1)*3];
        lengths = new double [_nbMarkers];
        // copy user defined control points to ctrlpts (Bezier control points)
        _currentMarker = _listMarkers.begin();
        _currentMarkerIndex = 0;
        while (_currentMarker != _listMarkers.end())
        {
            ctrlpts[i] = (*_currentMarker)->x();
            ctrlpts[i+1] = (*_currentMarker)->y();
            ctrlpts[i+2] = 0;
            i += 9;
            ++_currentMarker;
            _currentMarkerIndex++;
        }
        // update intermediate control points:
        // we perform far too many calculations by calling _updateCtrlPoints
        // but the code is much easier to read
        _currentMarker = _listMarkers.begin();
        _currentMarkerIndex = 0;
        while (_currentMarker != _listMarkers.end())
        {
            _updateCtrlPoints();
            _currentMarker++;
            _currentMarkerIndex++;
            if (_currentMarker != _listMarkers.end())
            {
                ++_currentMarker;
                _currentMarkerIndex++;
            }
        }
    }
}


void Blob2D::addMarker(Marker2D *marker)
{
    Polygon2D::addMarker(marker);

    double *tmp = new double [(_nbMarkers*3+1)*3];
    double *tlengths = new double [_nbMarkers];

    if(_nbMarkers >= 2)
    {
        // get the rank of the current marker
        int currentIndex = _currentMarkerIndex;
        if(currentIndex)
        {
            memcpy(tmp, ctrlpts, (3*currentIndex-2)*3*sizeof(double));
            memcpy(tlengths, lengths, currentIndex * sizeof(double));
        }

        // copy control points after the one added (at least one since the last
        // control point is the first one
        memcpy(tmp+(3*currentIndex+3)*3, ctrlpts+3*currentIndex*3, (3*(_nbMarkers-currentIndex)-2)*3*sizeof(double));
        memcpy(tlengths+currentIndex+1, lengths+currentIndex, (_nbMarkers-currentIndex-1)*sizeof(double));
    }
    if (ctrlpts) delete []ctrlpts;
    if (lengths) delete []lengths;
    ctrlpts = tmp;
    lengths = tlengths;
    _updateCtrlPoints();
}

void Blob2D::removeMarker(Marker2D *marker)
{
    if (!marker) marker = getCurrentMarker();
    std::list<Marker2D*>::iterator tl = std::find(_listMarkers.begin(), _listMarkers.end(), marker);
    if (tl == _listMarkers.end()) return;

    // marker is in the list
    int currentIndex = std::distance(_listMarkers.begin(), tl);
    // when we delete the first marker in the list, the marker index stays 0 (marker 1 becomes marker 0)
    if(currentIndex>0) currentIndex--;
    Object2D::removeMarker(marker);

    if (_nbMarkers > 0)
    {
        double *tmp = new double [(_nbMarkers*3+1)*3];
        double *tlengths = new double [_nbMarkers];
        if (_nbMarkers > 1)
        {
            if (currentIndex != -1)
            {
                // copy the previous control points:
                // 3*currentIndex+1 points at the beginning
                memcpy(tmp, ctrlpts, (3*currentIndex+1)* 3*sizeof(double));
                memcpy(tlengths, lengths, (currentIndex+1)*sizeof(double));

                // then leave 5 control points (2 on tmp) and copy the rest
                memcpy(tmp+(3*currentIndex+3)*3, ctrlpts+(3*currentIndex+6)*3,
                       (3*(_nbMarkers-currentIndex)-2)*3*sizeof(double));
                memcpy(tlengths+currentIndex+1, lengths+currentIndex+2, (_nbMarkers-currentIndex-1)*sizeof(double));
            }
            else
            {
                // the first one was removed: pass 5 control points away (2 on tmp)
                //and copy
                memcpy(tmp, ctrlpts+9, (3*(_nbMarkers-2)+1)*3*sizeof(double));
                // copy first point to last point
                memcpy(tmp+_nbMarkers*3*3, tmp, 3*sizeof(double));
                // pass the first length away
                memcpy(tlengths, lengths+1, _nbMarkers*sizeof(double));
                currentIndex = _nbMarkers-1;
            }
        }
        if (ctrlpts) delete []ctrlpts;
        if (lengths) delete []lengths;
        ctrlpts = tmp;
        lengths = tlengths;
        _currentMarker = _listMarkers.begin();
        if(currentIndex != 0)
        {
            std::advance(_currentMarker, currentIndex);
            _currentMarkerIndex = currentIndex;
        }
        else _currentMarkerIndex = 0;
        _updateCtrlPoints();
    }
    else
    {
        if (ctrlpts) delete []ctrlpts;
        if (lengths) delete []lengths;
        ctrlpts = NULL;
        lengths = NULL;
    }
    _currentMarker = _listMarkers.end();
    _currentMarkerIndex = -1;
}


// we interpolate using cardinal splines
// in this function the control points for the equivalent Bezier curve
// are determined and stored in ctrlpts
void Blob2D::_updateCtrlPoints()
{
    // we assume _currentList is up to date:
    // this is our indicator to know which point was modified
    if (_currentMarker == _listMarkers.end())
        return;

    double m[8], *tm = m;
    int i, n;
    int currentIndex = _currentMarkerIndex;
    double a = 0.5;

    i = 9*currentIndex;
    ctrlpts[i] = (*_currentMarker)->x();
    ctrlpts[i+1] = (*_currentMarker)->y();
    ctrlpts[i+2] = 0;
    ctrlpts[i+5] = 0;
    ctrlpts[i+8] = 0;
    // copy first point to last point to close curve
    if (currentIndex==0) memcpy(ctrlpts+9*_nbMarkers, ctrlpts, 3*sizeof(double));

    if (_nbMarkers==1)
    {
        lengths[0] = 0;
        return;
    }

    // this is only valid if we have more than 1 point

    // update surrounding control points: we know we have at least 2 markers
    if (_nbMarkers==2) // only 2 markers: draw a straight line
    {
        // compute the two intermediate control points
        i=3;
        ctrlpts[i] = ((3-a)*ctrlpts[0]+a*ctrlpts[9])/3;
        ctrlpts[i+1] = ((3-a)*ctrlpts[1]+a*ctrlpts[10])/3;
        ctrlpts[i+2] = 0;
        i=6;
        ctrlpts[i] = ((3-a)*ctrlpts[9]+a*ctrlpts[0])/3;
        ctrlpts[i+1] = ((3-a)*ctrlpts[10]+a*ctrlpts[1])/3;
        ctrlpts[i+2] = 0;
        // copy these points to the other two intermediate control points
        memcpy(ctrlpts+12, ctrlpts+6, 3*sizeof(double));
        memcpy(ctrlpts+15, ctrlpts+3, 3*sizeof(double));
        lengths[0] = lengths[1] = PoLAR::Bezier::bezierArcLength(ctrlpts);
        return;
    }

    int jmin = currentIndex-3, jmax = currentIndex, j, k;
    if (jmin < 0) jmin += _nbMarkers, jmax += _nbMarkers;

    for (j=jmin; j<=jmax; j++)
    {
        tm = m;
        // get the control points from ctrlpts (copies of markers in list)
        for (i=j, n=0; n<4; i++, n++)
        {
            k = (i%_nbMarkers)*9;
            *tm++ = ctrlpts[k];
            *tm++ = ctrlpts[k+1];
        }

        // compute the two intermediate control points
        // between the (j+1)th and the (j+2)th control points
        // (jth and (j+3)th are the extremities of the cardinal spline
        //  and are never reached)
        i = (j+1)%_nbMarkers;
        i = i*9+3;
        ctrlpts[i] = (-a*m[0]+3*m[2]+a*m[4])/3;
        ctrlpts[i+1] = (-a*m[1]+3*m[3]+a*m[5])/3;
        ctrlpts[i+2] = 0;
        i+=3;
        ctrlpts[i] = (a*m[2]+3*m[4]-a*m[6])/3;
        ctrlpts[i+1] = (a*m[3]+3*m[5]-a*m[7])/3;
        ctrlpts[i+2] = 0;
        // update/compute the length of the bezier arc
        i = (j+1)%_nbMarkers;
        lengths[i] = PoLAR::Bezier::bezierArcLength(ctrlpts+9*i);
    }
}

void Blob2D::moveCurrentMarker(double nx, double ny)
{
    Object2D::moveCurrentMarker(nx,ny);
    _updateCtrlPoints();
}


double Blob2D::length() const
{
    double l=0;
    int i=0;
    for (i=0; i<_nbMarkers; i++) l+=lengths[i];
    return l;
}

// Blob2D::getPoint : returns a point on the curve with curved abscissa u
// u=0 mod length() -> return first point (ctrlpts[0] = _listMarker->marker)
Marker2D *Blob2D::getPoint(double u) const
{
    // first make u be in [0, length()]
    double l = length();
    while (u < 0) u += l;
    while (u > l) u -= l;

    // find the arc index where the point should belong
    int i=0;
    while (u > lengths[i]) u-=lengths[i++];

    return PoLAR::Bezier::bezierPoint(ctrlpts+9*i, (Null(lengths[i]) ? 0 : u/lengths[i]));
}

// returns a list of N markers that lie on the curve at equidistant curved abscissae
std::list<Marker2D *> Blob2D::resample(int N) const
{
    std::list<Marker2D *> ret;
    double l = length();
    int i = N;
    while (i--) ret.push_front(getPoint((i*l)/N));
    return ret;
}


// test function
void Blob2D::dumpCtrlPts(char *fname) const
{
#if !defined(__GNUC__) && (defined(WIN32) || defined(_WIN32))
	FILE *f;
	errno_t err = fopen_s(&f, fname, "w");
	if(err)
#else
	FILE *f = fopen(fname, "w");
	if (!f)
#endif
	{
        qCritical() << "Could not open file " << fname;
		return;
	}
    dumpCtrlPts(f);
    fclose(f);
}

void Blob2D::dumpCtrlPts(FILE *f) const
{
    int i;
    fprintf(f, "%d\n", 3*_nbMarkers+1);
    for (i=0; i<3*_nbMarkers+1; i++)
        fprintf(f, "%f %f\n", ctrlpts[3*i], ctrlpts[3*i+1]);
}
