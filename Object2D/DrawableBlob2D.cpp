/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DrawableBlob2D.h"
#include "Viewer2D.h"

using namespace PoLAR;

REGISTER_TYPE(DrawableBlob2D)


DrawableBlob2D::DrawableBlob2D(std::list<Marker2D*> list, Viewer2D *Parent,
                               QColor unselCol, QColor selCol,
                               QColor selmarkCol, MarkerShape unselShape,
                               MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = new Blob2D(list);
    setName("DrawableBlob2D");
    setType(DrawableObject2D::Blob);
}


DrawableBlob2D::DrawableBlob2D(Viewer2D *Parent,
                               QColor unselCol, QColor selCol,
                               QColor selmarkCol, MarkerShape unselShape,
                               MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = new Blob2D();
    setName("DrawableBlob2D");
    setType(DrawableObject2D::Blob);
}

DrawableBlob2D::DrawableBlob2D(Viewer2D *Parent,  Blob2D *object,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = object;
    setName("DrawableBlob2D");
    setType(DrawableObject2D::Blob);
}

DrawableBlob2D::DrawableBlob2D(std::list<Marker2D*> list, Viewer2D *Parent, QPen& unselectPen,
                               QPen& selectPen, QPen& selectMarkerPen,
                               MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Blob2D(list);
    setName("DrawableBlob2D");
    setType(DrawableObject2D::Blob);
}

DrawableBlob2D::DrawableBlob2D(Viewer2D *Parent, QPen& unselectPen,
                               QPen& selectPen, QPen& selectMarkerPen,
                               MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Blob2D();
    setName("DrawableBlob2D");
    setType(DrawableObject2D::Blob);
}

DrawableBlob2D::DrawableBlob2D(Viewer2D *Parent, Blob2D *object, QPen& unselectPen,
                               QPen& selectPen, QPen& selectMarkerPen,
                               MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = object;
    setName("DrawableBlob2D");
    setType(DrawableObject2D::Blob);
}

DrawableBlob2D::DrawableBlob2D(DrawableBlob2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy)
{
}


void DrawableBlob2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!isDisplayed()) return;

    if (_object->nbMarkers() >= 2)
    {
        QPen pen = (isSelected() && isEditable() ? _penList[DrawableObject2D::Selected] : _penList[DrawableObject2D::Unselected]);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);

        double *tcp = static_cast<Blob2D*>(_object.get())->getControlPoints();
        QPainterPath path;
        for (unsigned int i=0; i<_object->nbMarkers();i++, tcp+=9)
        {
            /*
             * Cubic Bezier curve:
             *  tcp[0] = x0, tcp[1] = y0 -> starting point
             *  tcp[3] = x1, tcp[4] = y1 -> control point 1
             *  tcp[6] = x2, tcp[7] = y2 -> control point 2
             *  tcp[9] = x3, tcp[10] = y3 -> ending point
             * */
            double x0, y0, x1, y1, x2, y2, x3, y3;
            transformCoord(tcp[0], tcp[1], x0, y0);
            transformCoord(tcp[3], tcp[4], x1, y1);
            transformCoord(tcp[6], tcp[7], x2, y2);
            transformCoord(tcp[9], tcp[10], x3, y3);

            path.moveTo(x0, y0);
            path.cubicTo(x1, y1, x2, y2, x3, y3);

        }
        updateShapePath(path);
        painter->drawPath(path);
    }

    // drawing the markers on top of the path
    DrawableObject2D::paint(painter, option, widget);
}


void DrawableBlob2D::swap(DrawableBlob2D &first, DrawableBlob2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
}

DrawableBlob2D& DrawableBlob2D::operator=(DrawableBlob2D obj)
{
    swap(*this, obj);
    return *this;
}
