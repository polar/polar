/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#if defined(WIN32) || defined(_WIN32)
#define _USE_MATH_DEFINES
#endif

#include <cmath>
#include "Bezier.h"
#include <Util.h>

using namespace PoLAR;
using namespace Util;

double Bezier::_bezierArcLengthPolygon(double p[])
{
    double d1 = sqrdist2(p, p+2);
    double d2 = sqrdist2(p+2, p+4);
    double d3 = sqrdist2(p+4, p+6);
    return sqrt(d1) + sqrt(d2) + sqrt(d3);
}

double Bezier::_bezierArcLengthRecursive(double p[], double prec)
{
    double d = Bezier::_bezierArcLengthPolygon(p);
    if (d < prec) return d;

    double pp[14]; // next iteration of the mid-point rule
    copyvec2<double, double>(pp, p);
    initvec2(pp+2, 0.5*(p[0]+p[2]), 0.5*(p[1]+p[3]));
    initvec2(pp+4, 0.5*(0.5*(p[0]+p[4])+p[2]), 0.5*(0.5*(p[1]+p[5])+p[3]));
    initvec2(pp+6, 0.125*(p[0]+3*(p[2]+p[4])+p[6]), 0.125*(p[1]+3*(p[3]+p[5])+p[7]));
    initvec2(pp+8, 0.5*(0.5*(p[2]+p[6])+p[4]), 0.5*(0.5*(p[3]+p[7])+p[5]));
    initvec2(pp+10, 0.5*(p[4]+p[6]), 0.5*(p[5]+p[7]));
    copyvec2<double, double>(pp+12, p+6);
    d = Bezier::_bezierArcLengthRecursive(pp, prec) + Bezier::_bezierArcLengthRecursive(pp+6, prec);
    return d;
}

double Bezier::bezierArcLength(double *pts)
{
    const double prec = 1; // one pixel in precision
    double p[8];
    copyvec2<double, double>(p, pts);
    copyvec2<double, double>(p+2, pts+3);
    copyvec2<double, double>(p+4, pts+6);
    copyvec2<double, double>(p+6, pts+9);
    return Bezier::_bezierArcLengthRecursive(p, prec);
}

// we use de Casteljau algorithm
Marker2D *Bezier::bezierPoint(double *pts, double u)
{
    double x[4], y[4], unmu=1-u;
    x[0] = pts[0], x[1] = pts[3], x[2] = pts[6], x[3] = pts[9];
    y[0] = pts[1], y[1] = pts[4], y[2] = pts[7], y[3] = pts[10];
    // casteljau
    // first pass
    x[0] = unmu*x[0] + u*x[1];
    y[0] = unmu*y[0] + u*y[1];
    x[1] = unmu*x[1] + u*x[2];
    y[1] = unmu*y[1] + u*y[2];
    x[2] = unmu*x[2] + u*x[3];
    y[2] = unmu*y[2] + u*y[3];
    // second pass
    x[0] = unmu*x[0] + u*x[1];
    y[0] = unmu*y[0] + u*y[1];
    x[1] = unmu*x[1] + u*x[2];
    y[1] = unmu*y[1] + u*y[2];
    // third pass
    x[0] = unmu*x[0] + u*x[1];
    y[0] = unmu*y[0] + u*y[1];

    return new Marker2D(x[0], y[0], 0);
}
