/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Spline2D.h"
#include "Bezier.h"
#include "Util.h"

using namespace PoLAR;
using namespace Util;

Spline2D::Spline2D(std::list<Marker2D *> &list)
    : PolyLine2D(list),
      ctrlpts(NULL),
      lengths(NULL)
{
    _makeCtrlPoints();
}

Spline2D::Spline2D()
    : PolyLine2D(),
      ctrlpts(NULL),
      lengths(NULL)
{
    _makeCtrlPoints();
}


Spline2D::Spline2D(Spline2D &obj):
    PolyLine2D(obj),
  ctrlpts(NULL),
  lengths(NULL)
{
_makeCtrlPoints();
}


Spline2D::~Spline2D()
{
    if (ctrlpts) delete []ctrlpts;
    if (lengths) delete []lengths;
}


void Spline2D::setMarkers(std::list<Marker2D *> &list, bool destroyMarkers)
{
    PolyLine2D::setMarkers(list,destroyMarkers);
    _makeCtrlPoints();
}

void Spline2D::_makeCtrlPoints()
{
    if (ctrlpts) delete []ctrlpts;
    if (lengths) delete []lengths;

    if (_listMarkers.size() == 0) ctrlpts = NULL, lengths = NULL;
    else
    {
        int i=0;
        ctrlpts = new double[((_nbMarkers-1)*3+1)*3];
        lengths = (_nbMarkers > 1) ? new double [_nbMarkers-1] : NULL;
        // copy user defined control points to ctrlpts (Bezier control points)
        _currentMarker = _listMarkers.begin();
        _currentMarkerIndex = 0;
        while (_currentMarker != _listMarkers.end())
        {
            ctrlpts[i] = (*_currentMarker)->x();
            ctrlpts[i+1] = (*_currentMarker)->y();
            ctrlpts[i+2] = 0;
            i += 9;
            ++_currentMarker;
            _currentMarkerIndex++;
        }
        // update intermediate control points:
        // we perform far too many calculations by calling _updateCtrlPoints
        // but the code is much easier to read
        _currentMarker = _listMarkers.begin();
        _currentMarkerIndex = 0;
        while (_currentMarker != _listMarkers.end())
        {
            _updateCtrlPoints();
            ++_currentMarker;
            _currentMarkerIndex++;
            if (_currentMarker != _listMarkers.end())
            {
                ++_currentMarker;
                ++_currentMarkerIndex;
            }
        }
    }
}

void Spline2D::addMarker(Marker2D *marker)
{
    PolyLine2D::addMarker(marker);
    double *tmp = new double[((_nbMarkers-1)*3+1)*3];

    tmp[0] = (*_currentMarker)->x();
    tmp[1] = (*_currentMarker)->y();
    tmp[2] = 0;
    if (ctrlpts) // need at least 2 points to update control points
    {
        double *tlengths = new double [_nbMarkers-1];
        // sets the z value to 0 for the other two added points
        tmp[5] = tmp[8] = 0;
        // copy the previous control points: leave place for 3 more points
        // at the beginning
        memcpy(tmp+9, ctrlpts, ((_nbMarkers-2)*3+1)*3*sizeof(double));
        if (_nbMarkers > 2)
        {
            memcpy(tlengths+1, lengths, (_nbMarkers-2)*sizeof(double));
            delete []lengths;
        }
        else tlengths[0] = 0;
        delete []ctrlpts;
        ctrlpts = tmp;
        lengths = tlengths;
        _updateCtrlPoints();
    }
    else ctrlpts = tmp;
}

void Spline2D::removeMarker(Marker2D *marker)
{
    int currentIndex = 0;
    if(!marker)
    {
        // delete the current marker
        _listMarkers.erase(_currentMarker);
        currentIndex = _currentMarkerIndex - 1;
        _currentMarker = _listMarkers.end();
        _currentMarkerIndex = -1;
    }
    else
    { //delete the marker passed in parameter
        std::list<Marker2D*>::iterator tl = std::find(_listMarkers.begin(), _listMarkers.end(), marker);
        if(tl == _listMarkers.end()) return;

        //marker is in the list
        currentIndex = std::distance(_listMarkers.begin(), tl) - 1;
        _currentMarker = tl;
        --_currentMarker;
        _currentMarkerIndex--;
        _listMarkers.erase(tl);
    }
    updateNbMarkers();

    if(_nbMarkers == 1) // only one point !
    {
        delete []ctrlpts;
        delete []lengths;
        ctrlpts = new double[3];
        lengths = NULL;
        ctrlpts[0] = _listMarkers.front()->x();
        ctrlpts[1] = _listMarkers.front()->y();
        ctrlpts[2] = 0;
    }
    else if (_nbMarkers > 1)
    {
        double *tmp = new double[((_nbMarkers-1)*3+1)*3];
        double *tlengths = new double [_nbMarkers-1];
        memset(tmp, 0, ((_nbMarkers-1)*3+1)*3*sizeof(double));
        _currentMarker = _listMarkers.begin();
        std::advance(_currentMarker, currentIndex);
        _currentMarkerIndex = currentIndex;

        if (currentIndex > 0) // the first one was removed: pass 3 points away and copy
        {
            // copy the first points
            memcpy(tmp, ctrlpts, (3*currentIndex+1)* 3*sizeof(double));
            memcpy(tlengths, lengths, currentIndex*sizeof(double));

            if (currentIndex != _nbMarkers-1) // the last point was not removed. copy what's remain
            {
                memcpy(tmp+(3*currentIndex+3)*3, ctrlpts+(3*currentIndex+6)*3,
                       (3*(_nbMarkers-currentIndex)-5)*3*sizeof(double));
                if (currentIndex < _nbMarkers-2)
                    memcpy(tlengths+currentIndex+1, lengths+currentIndex+2, (_nbMarkers-currentIndex-2)*sizeof(double));
            }
        }
        else
        {
            memcpy(tmp, ctrlpts+9, ((_nbMarkers-1)*3+1)*3*sizeof(double));
            memcpy(tlengths, lengths+1, (_nbMarkers-1)*sizeof(double));
        }
        delete []ctrlpts;
        delete []lengths;
        ctrlpts = tmp;
        lengths = tlengths;
        _updateCtrlPoints();
    }
    else
    {
        delete []ctrlpts;
        delete []lengths;
        ctrlpts = NULL, lengths = NULL;
    }
    _currentMarker = _listMarkers.end();
    _currentMarkerIndex = -1;
}



// we interpolate using cardinal splines
// in this function the control points for the equivalent Bezier curve
// are determined and stored in ctrlpts
void Spline2D::_updateCtrlPoints()
{
    double m[8], *tm=m;
    int i, n;

    const double a=0.5;

    int currentIndex = _currentMarkerIndex;
    if (_currentMarker != _listMarkers.end())
    {
        i=9*currentIndex;
        ctrlpts[i] = (*_currentMarker)->x();
        ctrlpts[i+1] = (*_currentMarker)->y();
    }

    if (_nbMarkers == 1) return; // enough's been done

    if (_nbMarkers == 2)
    {
        // extrapolate by a simple copy of extremal points
        *tm++ = ctrlpts[0];
        *tm++ = ctrlpts[1];
        for (i=0, n=1; n<3; i+=9, n++)
        {
            *tm++ = ctrlpts[i];
            *tm++ = ctrlpts[i+1];
        }
        i-=9;
        // extrapolate by a simple copy of extremal points
        *tm++ = ctrlpts[i];
        *tm++ = ctrlpts[i+1];

        // compute the two intermediate control points
        i = 3;
        ctrlpts[i] = (-a*m[0]+3*m[2]+a*m[4])/3;
        ctrlpts[i+1] = (-a*m[1]+3*m[3]+a*m[5])/3;
        i+=3;
        ctrlpts[i] = (a*m[2]+3*m[4]-a*m[6])/3;
        ctrlpts[i+1] = (a*m[3]+3*m[5]-a*m[7])/3;
        lengths[0] = PoLAR::Bezier::bezierArcLength(ctrlpts);
        return;
    }

    // update surrounding control points: we know we have at least 2 markers
    int jmin = currentIndex-3, jmax = currentIndex, j;
    if (jmin < -1) jmin = -1;
    if (jmax > _nbMarkers-3) jmax = _nbMarkers-3;
    if (jmax < -1) jmax = -1;
    for (j=jmin; j<=jmax; j++)
    {
        tm = m;
        // set of 4 markers to be used
        if (j<0)
        {
            // extrapolation to find one more control point for the
            // cardinal spline before the first user defined point:
            // formula based on the annulation of the second
            // derivative of the curve on the first user defined control point
            *tm++ = ((3-a)/(2*a)*ctrlpts[0] + (2*a-3)/(2*a)*ctrlpts[9] + 0.5*ctrlpts[18]);
            *tm++ = ((3-a)/(2*a)*ctrlpts[1] + (2*a-3)/(2*a)*ctrlpts[10] + 0.5*ctrlpts[19]);

            for (i=0, n=1; n<4; i+=9, n++)
            {
                *tm++ = ctrlpts[i];
                *tm++ = ctrlpts[i+1];
            }
        }
        else if (j>=_nbMarkers-3)
        {
            for (i=9*(_nbMarkers-3), n=0; n<3; i+=9, n++)
            {
                *tm++ = ctrlpts[i];
                *tm++ = ctrlpts[i+1];
            }
            i-=9;

            // extrapolation to find one more control point for the
            // cardinal spline at the end of the set: formula based on
            // the annulation of the second derivative of the curve on
            // the last user defined control point
            *tm++ = ((3-a)/(2*a)*ctrlpts[i] + (2*a-3)/(2*a)*ctrlpts[i-9] + 0.5*ctrlpts[i-18]);
            *tm++ = ((3-a)/(2*a)*ctrlpts[i+1] + (2*a-3)/(2*a)*ctrlpts[i-8] + 0.5*ctrlpts[i-17]);

        }
        else
            for (i=9*j, n=0; n<4; i+=9, n++)
            {
                *tm++ = ctrlpts[i];
                *tm++ = ctrlpts[i+1];
            }

        // compute the two intermediate control points
        i = 9*(j+1)+3;
        ctrlpts[i] = (-a*m[0]+3*m[2]+a*m[4])/3;
        ctrlpts[i+1] = (-a*m[1]+3*m[3]+a*m[5])/3;
        i+=3;
        ctrlpts[i] = (a*m[2]+3*m[4]-a*m[6])/3;
        ctrlpts[i+1] = (a*m[3]+3*m[5]-a*m[7])/3;

        lengths[j+1] = PoLAR::Bezier::bezierArcLength(ctrlpts+9*(j+1));
    }
}

void Spline2D::moveCurrentMarker(double nx, double ny)
{
    Object2D::moveCurrentMarker(nx,ny);
    _updateCtrlPoints();
}


double Spline2D::length() const
{
    double l=0;
    int i=0;
    for (i=0; i<_nbMarkers-1; i++) l+=lengths[i];
    return l;
}

// Spline2D::getPoint : returns a point on the curve with curved abscissa u 
// u=0 mod length() -> return first point (ctrlpts[0] = _listMarker->marker)
Marker2D *Spline2D::getPoint(double u)
{
    // first make u be in [0, length()]
    double l = length();
    if (u <= 0) return PoLAR::Bezier::bezierPoint(ctrlpts, 0);
    if (u >= l) return PoLAR::Bezier::bezierPoint(ctrlpts+9*(_nbMarkers-2), 1);

    // find the arc index where the point should belong
    int i=0;
    while (i < _nbMarkers-1 && u > lengths[i]) u-=lengths[i++];
    if (i == _nbMarkers-1) u=1, i=_nbMarkers-2;
    else if (Null(lengths[i])) u=0;
    else u /= lengths[i];
    return PoLAR::Bezier::bezierPoint(ctrlpts+9*i, u);
}

// returns a list of N markers that lie on the curve at equidistant curved abscissae
std::list<Marker2D*> Spline2D::resample(int N)
{
    std::list<Marker2D*> ret;
    double l=length();
    int i=N;
    if (N<=0) return ret;
    if (N==1)
    {
        if (_listMarkers.size() > 0)
        {
            ret.push_front(new Marker2D(_listMarkers.front()));
        }
        return ret;
    }
    // divide by (N-1) so as to keep the same extremities for the curves
    while(i--) ret.push_front(getPoint((i*l)/(N-1)));
    return ret;
}


