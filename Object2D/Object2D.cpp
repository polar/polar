/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Object2D.h"

using namespace PoLAR;


Object2D::Object2D(std::list<Marker2D *> list)
{
    setMarkers(list);
    _nbMarkers = _listMarkers.size();
}

Object2D::Object2D()
{
    _currentMarker = _listMarkers.end();
    _currentMarkerIndex = -1;
    _nbMarkers = _listMarkers.size();
}


Object2D::Object2D(Object2D &obj):
    osg::Referenced()
{
    std::list<Marker2D*> markerList = obj.getMarkers();
    std::list<Marker2D*>::iterator it;
    for(it=markerList.begin(); it!=markerList.end(); ++it)
    {
        Marker2D *mkr = new Marker2D((*it)->x(), (*it)->y(), (*it)->label());
        addMarker(mkr);
    }
    _currentMarker = _listMarkers.end();
    _currentMarkerIndex = -1;
    _nbMarkers = _listMarkers.size();
}


Object2D::~Object2D()
{
    resetPointers();
    destroyCurrentList();
    _listMarkers.clear();
}


void Object2D::destroyCurrentList()
{
    for(std::list<Marker2D*>::iterator tmp = _listMarkers.begin(); tmp!=_listMarkers.end(); ++tmp)
    {
        delete (*tmp);
    }
}

void Object2D::resetPointers()
{
    _currentMarker = _listMarkers.end();
    _currentMarkerIndex = -1;
}



void Object2D::setMarkers(std::list<Marker2D *> &list, bool destroyMarkers)
{
    if(destroyMarkers)
    {
        destroyCurrentList();
        _listMarkers.clear();
    }
    _listMarkers = list;
    _nbMarkers = list.size();
    resetPointers();
}



double Object2D::selectMarker(double x, double y, double precision)
{
    Marker2D point(x, y, 0);
    double dmin, d;

    // select closest marker
    std::list<Marker2D*>::iterator tmp = _listMarkers.begin();
    if(tmp == _listMarkers.end())
    {
        return 0;
    }
    int tmpIndex = 0;

    dmin = Marker2D::distance2((*tmp), &point);
    _currentMarker = tmp;
    _currentMarkerIndex = tmpIndex;

    ++tmp;
    ++tmpIndex;
    while (tmp != _listMarkers.end())
    {
        d = Marker2D::distance2((*tmp), &point);
        if (d < dmin)
        {
            _currentMarker = tmp;
            dmin = d;
            _currentMarkerIndex = tmpIndex;
        }
        ++tmp;
        ++tmpIndex;
    }

    // exclude the selected marker if the distance is too high
    if (precision >= 0 && dmin > precision*precision)
    {
        _currentMarker = _listMarkers.end();
        _currentMarkerIndex = -1;
    }

    // in any case, return the distance to the closest marker, even if it was not selected
    return dmin;
}

void Object2D::unselectMarker()
{
    _currentMarker = _listMarkers.end();
    _currentMarkerIndex = -1;
}


void Object2D::removeMarker(Marker2D *marker)
{
    if (!marker)
    {
        marker = getCurrentMarker();
    }
    std::list<Marker2D*>::iterator tmp;
    for(tmp = _listMarkers.begin(); tmp !=  _listMarkers.end(); ++tmp)
    {
        if((*tmp) == marker)
        {
            _listMarkers.erase(tmp);
            _currentMarker = _listMarkers.end();
            _currentMarkerIndex = -1;
            break;
        }
    }
    updateNbMarkers();
}


void Object2D::saveMarkers(char *fname)
{
    if(_listMarkers.size() == 0) return;
#if !defined(__GNUC__)  && (defined(WIN32) || defined(_WIN32))
    FILE *f;
    errno_t err = fopen_s(&f, fname, "w+");
    if(err)
#else
    FILE *f = fopen(fname, "w+");
    if (!f)
#endif
        return;

    std::list<Marker2D*>::iterator tmp;
    for(tmp = _listMarkers.begin(); tmp !=  _listMarkers.end(); ++tmp)
    {
        (*tmp)->fprintf(f);
    }
    fclose(f);
}


void Object2D::addMarker(Marker2D *marker)
{
    _listMarkers.push_front(marker);
    _currentMarker = _listMarkers.begin();
    _currentMarkerIndex = 0;
    updateNbMarkers();
}


Marker2D* Object2D::getMarker(unsigned int i)
{
    if(_listMarkers.size() > i)
    {
        std::list<Marker2D*>::iterator it = _listMarkers.begin();
        std::advance(it, i);
        return (*it);
    }
    return NULL;
}

// compute the bounding box as a 2D rectangle:
// - ll refers to the LowerLeft corner, a.k.a (xmin, ymin)
// - ur refers to the UpperRight corner, a.k.a (xmax, ymax)
void Object2D::boundingBox(double *xll, double *yll, double *xur, double *yur)
{
    if (_listMarkers.empty())
    {
        *xll = *xur = *yll = *yur = 0;
        return;
    }

    double x,y;
    *xll = *xur = (_listMarkers.front())->x();
    *yll = *yur = (_listMarkers.front())->y();
    std::list<Marker2D*>::iterator tmp;
    for(tmp = _listMarkers.begin(); tmp !=  _listMarkers.end(); ++tmp)
    {
        x = (*tmp)->x(), y=(*tmp)->y();
        if (x < *xll) *xll = x;
        else if (x > *xur) *xur = x;
        if (y < *yll) *yll = y;
        else if (y > *yur) *yur = y;
    }
}

