/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Marker2D.h"
#include "Util.h"
#include <cmath>
#include <cstdio>
#include <iostream>
#include <iomanip>

using namespace PoLAR;
using namespace Util;

Marker2D::Marker2D(double x, double y, float label):
    QPointF(x, y), labelp(label)
{
}

Marker2D::Marker2D(Marker2D *marker):
    QPointF(marker->x(), marker->y()), labelp(marker->labelp)
{
}

Marker2D::Marker2D(QPointF& p):
    QPointF(p.x(), p.y()), labelp(0.0f)
{
}

//Marker2D::~Marker2D() {}

void Marker2D::fprintf(FILE *out) const
{
    std::fprintf(out, "%f: (%f, %f)\n", labelp, x(), y());
}


void Marker2D::save(std::ostream &out) const
{
    out << std::fixed << std::setprecision(6) << labelp << " " << x() << " " << y() << std::endl;
}

double Marker2D::norm2() const
{
    return x()*x() + y()*y();
}

double Marker2D::distance2(Marker2D *m1, Marker2D *m2)
{
    double dx = m1->x() - m2->x(), dy = m1->y() - m2->y();
    return dx*dx + dy*dy;
}

double Marker2D::distance(Marker2D *m1, Marker2D *m2)
{
    return sqrt(Marker2D::distance2(m1, m2));
}

double Marker2D::determinant(Marker2D *p1, Marker2D *p2)
{
    return p1->x() * p2->y() - p2->x() * p1->y();
}

void Marker2D::difference(Marker2D *p1, Marker2D *p2, Marker2D *r)
{
    r->setX(p1->x() - p2->x());
    r->setY(p1->y() - p2->y());
}

double Marker2D::scalarProduct(Marker2D *p1, Marker2D *p2)
{
    return p1->x() * p2->x() + p1->y() * p2->y();
}

double Marker2D::distance2ToSegment(Marker2D *m, Marker2D *sdeb, Marker2D *sfin)
{
    Marker2D dm, ds;
    double lambda, norm;
    Marker2D::difference(m, sdeb, &dm);
    Marker2D::difference(sfin, sdeb, &ds);
    norm = ds.norm2();
    if (Null(norm))
        return dm.norm2();
    lambda = Marker2D::scalarProduct(&dm, &ds);
    if (lambda <= 0)
        return dm.norm2();
    if (lambda >= norm)
        return Marker2D::distance2(m,sfin);
    lambda = Marker2D::determinant(&dm, &ds);
    return lambda*lambda/norm;
}

double Marker2D::angle(Marker2D *p1, Marker2D *p2, Marker2D *p3, Marker2D *p4)
{
    Marker2D v1, v2;
    double d,p;
    Marker2D::difference(p2, p1, &v1);
    Marker2D::difference(p4, p3, &v2);
    d = Marker2D::determinant(&v1, &v2);
    p = Marker2D::scalarProduct(&v1, &v2);
    return atan2(d,p); // radians
}


std::ostream& PoLAR::operator<<(std::ostream& out, const Marker2D& marker)
{
    out << std::fixed << std::setprecision(6) << marker.label() << ": (" << marker.x() << ", " << marker.y() << ")" << std::endl;
    return out;
}
