#include <DrawableTaggedPolygon2D.h>
#include "Viewer2D.h"

using namespace PoLAR;


DrawableTaggedPolygon2D::DrawableTaggedPolygon2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                                 QColor unselCol, QColor selCol,
                                                 QColor selmarkCol, MarkerShape unselShape,
                                                 MarkerShape selShape, MarkerShape selmarkShape,
                                                 int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth),
      _colors(NULL),
      _nbColors(0),
      _ptr_cpy(1)
{
    _object = new TaggedPolygon2D(list);
    setName("DrawableTaggedPolygon2D");
    setType(DrawableObject2D::TaggedPolygon);
}


DrawableTaggedPolygon2D::DrawableTaggedPolygon2D(Viewer2D *Parent,
                                                 QColor unselCol, QColor selCol,
                                                 QColor selmarkCol, MarkerShape unselShape,
                                                 MarkerShape selShape, MarkerShape selmarkShape,
                                                 int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth),
      _colors(NULL),
      _nbColors(0),
      _ptr_cpy(1)
{
    _object = new TaggedPolygon2D();
    setName("DrawableTaggedPolygon2D");
    setType(DrawableObject2D::TaggedPolygon);
}


DrawableTaggedPolygon2D::DrawableTaggedPolygon2D(std::list<Marker2D*> list, Viewer2D *Parent, QPen& unselectPen,
                                                 QPen& selectPen, QPen& selectMarkerPen,
                                                 MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                                 int Size,  int Precision):
    DrawableObject2D(Parent,
                     selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _colors(NULL),
    _nbColors(0),
    _ptr_cpy(1)
{
    _object = new TaggedPolygon2D(list);
    setName("DrawableTaggedPolygon2D");
    setType(DrawableObject2D::TaggedPolygon);
}

DrawableTaggedPolygon2D::DrawableTaggedPolygon2D(Viewer2D *Parent, QPen& unselectPen,
                                                 QPen& selectPen, QPen& selectMarkerPen,
                                                 MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                                 int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _colors(NULL),
    _nbColors(0),
    _ptr_cpy(1)
{
    _object = new TaggedPolygon2D();
    setName("DrawableTaggedPolygon2D");
    setType(DrawableObject2D::TaggedPolygon);
}

DrawableTaggedPolygon2D::DrawableTaggedPolygon2D(Viewer2D *Parent, TaggedPolygon2D *object, QPen& unselectPen,
                                                 QPen& selectPen, QPen& selectMarkerPen,
                                                 MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                                 int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _colors(NULL),
    _nbColors(0),
    _ptr_cpy(1)
{
    _object = object;
    setName("DrawableTaggedPolygon2D");
    setType(DrawableObject2D::TaggedPolygon);
}

DrawableTaggedPolygon2D::DrawableTaggedPolygon2D(DrawableTaggedPolygon2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy), _colors(NULL), _ptr_cpy(1), _nbColors(0)
{
    setColors(obj._colors, obj._nbColors, obj._ptr_cpy);
}


DrawableTaggedPolygon2D::~DrawableTaggedPolygon2D()
{
    if (!_ptr_cpy && _colors)
    {
        delete[] _colors;
    }
}


void DrawableTaggedPolygon2D::setColors(QColor *colors, int nbColors, char PTR_CPY)
{
    // clean up if necessary
    if (!_ptr_cpy && _colors)
    {
        delete[] _colors;
    }
    _colors = NULL, _nbColors = 0;
    // modify _ptr_cpy (useful here?) that does not make a big difference...
    _ptr_cpy = PTR_CPY;

    // check the params. If invalid, let things clean as they are and return
    if (!colors || nbColors <= 0) return;

    // everything's OK
    // update the number of colors
    _nbColors = nbColors;
    // update the colors array, depending on PTR_CPY
    switch(PTR_CPY)
    {
    case 1: _colors = colors; break;
    case 0:
        _colors = new QColor[_nbColors];
        for (int i=0; i<_nbColors; i++)
        {
            _colors[i] = colors[i];
        }
        break;
    }
}


void DrawableTaggedPolygon2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!isDisplayed()) return;

    if (_object->nbMarkers() >= 2) // we must have at least 2 points
    {
        if(_currentPath) delete _currentPath;
        _currentPath = new QPainterPath;

        QPen pen = (isSelected() && isEditable() ? _penList[DrawableObject2D::Selected] : _penList[DrawableObject2D::Unselected]);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);

        std::list<Marker2D*> list = _object->getMarkers();
        std::list<Marker2D*>::iterator tmp = list.begin();
        if (!_colors) // use default color
        {
            Marker2D *origin = list.front();
            for(unsigned int index=0; index<list.size()-1; ++index)
            {
                float x1, y1, x2, y2;
                transformCoord((*tmp)->x, (*tmp)->y, &x1, &y1);
                ++tmp;
                transformCoord((*tmp)->x, (*tmp)->y, &x2, &y2);

                QPainterPath path;
                path.moveTo(x1, y1);
                path.lineTo(x2, y2);
                _currentPath->addPath(path);
                painter->drawPath(path);
            }

            // close the polygon
            if (_object->nbMarkers() > 2)
            {
                float x1, y1, x2, y2;
                transformCoord((*tmp)->x, (*tmp)->y, &x1, &y1);
                transformCoord(origin->x, origin->y, &x2, &y2);

                QPainterPath path;
                path.moveTo(x1, y1);
                path.lineTo(x2, y2);
                _currentPath->addPath(path);
                painter->drawPath(path);
            }
        }
        else // use color array
        {
            Marker2D *origin = *tmp;
            for(unsigned int index=0; index<list.size()-1; ++index)
            {
                QPen pen(_colors[(int)floor((*tmp)->label)]);
                //std::cout << (int)floor((*tmp)->label) << " ";
                pen.setWidth(_linewidth);
                painter->setPen(pen);
                float x1, y1, x2, y2;
                transformCoord((*tmp)->x, (*tmp)->y, &x1, &y1);
                ++tmp;
                transformCoord((*tmp)->x, (*tmp)->y, &x2, &y2);

                QPainterPath path;
                path.moveTo(x1, y1);
                path.lineTo(x2, y2);
                _currentPath->addPath(path);
                painter->drawPath(path);
            }

            // close the polygon
            if (_object->nbMarkers() > 2)
            {
                QPen pen(_colors[(int)floor((*tmp)->label)]);
               //std::cout << (int)floor((*tmp)->label) << " ";
                pen.setWidth(_linewidth);
                painter->setPen(pen);
                float x1, y1, x2, y2;
                transformCoord((*tmp)->x, (*tmp)->y, &x1, &y1);
                transformCoord(origin->x, origin->y, &x2, &y2);

                QPainterPath path;
                path.moveTo(x1, y1);
                path.lineTo(x2, y2);
                _currentPath->addPath(path);
                painter->drawPath(path);
            }
        }
       // std::cout << std::endl;
    }
    DrawableObject2D::paint(painter, option, widget);
}

void DrawableTaggedPolygon2D::swap(DrawableTaggedPolygon2D &first, DrawableTaggedPolygon2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
    using std::swap;
    swap(first._colors, second._colors);
    swap(first._nbColors, second._nbColors);
    swap(first._ptr_cpy, second._ptr_cpy);
}

DrawableTaggedPolygon2D& DrawableTaggedPolygon2D::operator=(DrawableTaggedPolygon2D obj)
{
    swap(*this, obj);
    return *this;
}
