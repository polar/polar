/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DrawableSpline2D.h"
#include "Viewer2D.h"

using namespace PoLAR;

REGISTER_TYPE(DrawableSpline2D)


DrawableSpline2D::DrawableSpline2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                   QColor unselCol, QColor selCol,
                                   QColor selmarkCol, MarkerShape unselShape,
                                   MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision,LineWidth)
{
    _object = new Spline2D(list);
    setName("DrawableSpline2D");
    setType(DrawableObject2D::Spline);
}


DrawableSpline2D::DrawableSpline2D(Viewer2D *Parent,
                                   QColor unselCol, QColor selCol,
                                   QColor selmarkCol, MarkerShape unselShape,
                                   MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision,LineWidth)
{
    _object = new Spline2D();
    setName("DrawableSpline2D");
    setType(DrawableObject2D::Spline);
}


DrawableSpline2D::DrawableSpline2D(Viewer2D *Parent,  Spline2D *object,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = object;
    setName("DrawableSpline2D");
    setType(DrawableObject2D::Spline);
}

DrawableSpline2D::DrawableSpline2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                   QPen& selectPen, QPen& unselectPen, QPen& selectMarkerPen,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision):
    DrawableObject2D(Parent,
                     selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Spline2D(list);
    setName("DrawableSpline2D");
    setType(DrawableObject2D::Spline);
}

DrawableSpline2D::DrawableSpline2D(Viewer2D *Parent,
                                   QPen& selectPen, QPen& unselectPen, QPen& selectMarkerPen,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Spline2D();
    setName("DrawableSpline2D");
    setType(DrawableObject2D::Spline);
}

DrawableSpline2D::DrawableSpline2D(Viewer2D *Parent, Spline2D *object,
                                   QPen& selectPen, QPen& unselectPen, QPen& selectMarkerPen,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = object;
    setName("DrawableSpline2D");
    setType(DrawableObject2D::Spline);
}

DrawableSpline2D::DrawableSpline2D(DrawableSpline2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy)
{
}


void DrawableSpline2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!isDisplayed()) return;

    if (_object->nbMarkers() >= 2) // we must have at least 2 points
    {
        if(_currentShapePath) delete _currentShapePath;
        _currentShapePath = new QPainterPath;

        QPen pen = (isSelected() && isEditable() ? _penList[DrawableObject2D::Selected] : _penList[DrawableObject2D::Unselected]);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);
        double *tcp = static_cast<Spline2D*>(_object.get())->getControlPoints();
        QPainterPath path;
        for (unsigned int i=0; i<_object->nbMarkers()-1; i++, tcp+=9)
        {
            /*
             * Cubic Bezier curve:
             *  tcp[0] = x0, tcp[1] = y0 -> starting point
             *  tcp[3] = x1, tcp[4] = y1 -> control point 1
             *  tcp[6] = x2, tcp[7] = y2 -> control point 2
             *  tcp[9] = x3, tcp[10] = y3 -> ending point
             * */
            double x0, y0, x1, y1, x2, y2, x3, y3;
            transformCoord(tcp[0], tcp[1], x0, y0);
            transformCoord(tcp[3], tcp[4], x1, y1);
            transformCoord(tcp[6], tcp[7], x2, y2);
            transformCoord(tcp[9], tcp[10], x3, y3);

            path.moveTo(x0, y0);
            path.cubicTo(x1, y1, x2, y2, x3, y3);
        }
        painter->drawPath(path);
        updateShapePath(path);
    }
    DrawableObject2D::paint(painter, option, widget);
}


void DrawableSpline2D::swap(DrawableSpline2D &first, DrawableSpline2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
}

DrawableSpline2D& DrawableSpline2D::operator=(DrawableSpline2D obj)
{
    swap(*this, obj);
    return *this;
}
