/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DrawableArrows2D.h"
#include "Viewer2D.h"
#include <cmath>

using namespace PoLAR;

REGISTER_TYPE(DrawableArrows2D)


DrawableArrows2D::DrawableArrows2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                   QColor unselCol, QColor selCol,
                                   QColor selmarkCol, MarkerShape unselShape,
                                   MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape, selShape, selmarkShape,
                       Size,Precision,LineWidth),
      _arrowShape(false),
      _arrowSize(10.0f),
      _invertArrowDirection(false)
{
    _object = new Arrows2D(list);
    setName("DrawableArrows2D");
    setType(DrawableObject2D::Arrows);
}


DrawableArrows2D::DrawableArrows2D(Viewer2D *Parent,
                                   QColor unselCol, QColor selCol,
                                   QColor selmarkCol, MarkerShape unselShape,
                                   MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape, selShape, selmarkShape,
                       Size,Precision,LineWidth),
      _arrowShape(false),
      _arrowSize(10.0f),
      _invertArrowDirection(false)
{
    _object = new Arrows2D();
    setName("DrawableArrows2D");
    setType(DrawableObject2D::Arrows);
}


DrawableArrows2D::DrawableArrows2D(Viewer2D *Parent,  Arrows2D *object,
                                   QColor unselCol, QColor selCol,
                                   QColor selmarkCol, MarkerShape unselShape,
                                   MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth),
      _arrowShape(false),
      _arrowSize(10.0f),
      _invertArrowDirection(false)
{
    _object = object;
    setName("DrawableArrows2D");
    setType(DrawableObject2D::Arrows);
}

DrawableArrows2D::DrawableArrows2D(std::list<Marker2D*> list, Viewer2D *Parent, QPen& unselectPen,
                                   QPen& selectPen, QPen& selectMarkerPen,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _arrowShape(false),
    _arrowSize(10.0f),
    _invertArrowDirection(false)
{
    _object = new Arrows2D(list);
    setName("DrawableArrows2D");
    setType(DrawableObject2D::Arrows);
}

DrawableArrows2D::DrawableArrows2D(Viewer2D *Parent, QPen& unselectPen,
                                   QPen& selectPen, QPen& selectMarkerPen,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _arrowShape(false),
    _arrowSize(10.0f),
    _invertArrowDirection(false)
{
    _object = new Arrows2D();
    setName("DrawableArrows2D");
    setType(DrawableObject2D::Arrows);
}


DrawableArrows2D::DrawableArrows2D(Viewer2D *Parent, Arrows2D *object, QPen& unselectPen,
                                   QPen& selectPen, QPen& selectMarkerPen,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _arrowShape(false),
    _arrowSize(10.0f),
    _invertArrowDirection(false)
{
    _object = object;
    setName("DrawableArrows2D");
    setType(DrawableObject2D::Arrows);
}



DrawableArrows2D::DrawableArrows2D(DrawableArrows2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy),
    _arrowShape(obj._arrowShape),
    _arrowSize(obj._arrowSize),
    _invertArrowDirection(obj._invertArrowDirection)
{
}


void DrawableArrows2D::drawArrow(QPointF &m1, QPointF &m2, QPainter *painter)
{
    double X = (m1.x() - m2.x());
    double Y = (m1.y() - m2.y());
    double angle = std::atan(Y/X) * 180 / M_PI;
    if(std::signbit(X))
        angle += 180.0;
    QPolygon P;
    float Z = _arrowSize * _viewer->getZoom();
    P << QPoint(-Z, Z) << QPoint(0,0) << QPoint(-Z, -Z);
    painter->translate(m1);
    painter->rotate(angle);
    painter->drawPolygon(P);
    painter->resetTransform();
}

void DrawableArrows2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!isDisplayed()) return;

    std::list<Marker2D *> list = _object->getMarkers();
    if (_object->nbMarkers() >= 2) // we must have at least 2 points
    {
        QPen pen = (isSelected() && isEditable() ? _penList[DrawableObject2D::Selected] : _penList[DrawableObject2D::Unselected]);
        pen.setWidth(_linewidth);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);

        QPointF m1, m2;

        std::list<Marker2D *>::iterator tmp = list.begin();
        if(list.size() % 2 == 1) // odd number of markers : we jump the first marker of the list (i.e. the last added)
        {
            ++tmp;
        }

        QPainterPath path;
        while (tmp!=list.end())
        {
            double x1, y1, x2, y2;
            transformCoord((*tmp)->x(), (*tmp)->y(), x1, y1);
            m1.rx() = x1;
            m1.ry() = y1;
            ++tmp;
            if(tmp == list.end()) break;
            transformCoord((*tmp)->x(), (*tmp)->y(), x2, y2);
            m2.rx() = x2;
            m2.ry() = y2;

            path.moveTo(x1, y1);
            path.lineTo(x2, y2);
            ++tmp;

            // Adding the arrow marker shape
            if(_arrowShape)
            {
                if(_invertArrowDirection)
                    drawArrow(m2, m1, painter);
                else
                    drawArrow(m1, m2, painter);
            }
        }
        updateShapePath(path);
        painter->drawPath(path);
    }
    DrawableObject2D::paint(painter, option, widget);
}


void DrawableArrows2D::swap(DrawableArrows2D &first, DrawableArrows2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
    using std::swap;
    swap(first._arrowShape, second._arrowShape);
}

DrawableArrows2D& DrawableArrows2D::operator=(DrawableArrows2D obj)
{
    swap(*this, obj);
    return *this;
}
