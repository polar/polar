/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DrawablePolyLine2D.h"
#include "Viewer2D.h"

using namespace PoLAR;

REGISTER_TYPE(DrawablePolyLine2D)


DrawablePolyLine2D::DrawablePolyLine2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                       QColor unselCol, QColor selCol,
                                       QColor selmarkCol, MarkerShape unselShape,
                                       MarkerShape selShape, MarkerShape selmarkShape,
                                       int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape, selShape, selmarkShape,
                       Size,Precision,LineWidth)
{
    _object = new PolyLine2D(list);
    setName("DrawablePolyLine2D");
    setType(DrawableObject2D::PolyLine);
}


DrawablePolyLine2D::DrawablePolyLine2D(Viewer2D *Parent,
                                       QColor unselCol, QColor selCol,
                                       QColor selmarkCol, MarkerShape unselShape,
                                       MarkerShape selShape, MarkerShape selmarkShape,
                                       int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape, selShape, selmarkShape,
                       Size,Precision,LineWidth)
{
    _object = new PolyLine2D();
    setName("DrawablePolyLine2D");
    setType(DrawableObject2D::PolyLine);
}

DrawablePolyLine2D::DrawablePolyLine2D(Viewer2D *Parent,  PolyLine2D *object,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = object;
    setName("DrawablePolyLine2D");
    setType(DrawableObject2D::PolyLine);
}

DrawablePolyLine2D::DrawablePolyLine2D(std::list<Marker2D*> list, Viewer2D *Parent, QPen& unselectPen,
                                       QPen& selectPen, QPen& selectMarkerPen,
                                       MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                       int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new PolyLine2D(list);
    setName("DrawablePolyLine2D");
    setType(DrawableObject2D::PolyLine);
}

DrawablePolyLine2D::DrawablePolyLine2D(Viewer2D *Parent, QPen& unselectPen,
                                       QPen& selectPen, QPen& selectMarkerPen,
                                       MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                       int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new PolyLine2D();
    setName("DrawablePolyLine2D");
    setType(DrawableObject2D::PolyLine);
}

DrawablePolyLine2D::DrawablePolyLine2D(Viewer2D *Parent, PolyLine2D* object, QPen& unselectPen,
                                       QPen& selectPen, QPen& selectMarkerPen,
                                       MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                       int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = object;
    setName("DrawablePolyLine2D");
    setType(DrawableObject2D::PolyLine);
}

DrawablePolyLine2D::DrawablePolyLine2D(DrawablePolyLine2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy)
{
}


void DrawablePolyLine2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!isDisplayed()) return;

    if (_object->nbMarkers() >= 2) // we must have at least 2 points
    {
       QPen pen = (isSelected() && isEditable() ? _penList[DrawableObject2D::Selected] : _penList[DrawableObject2D::Unselected]);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);

        std::list<Marker2D*> list = _object->getMarkers();
        std::list<Marker2D*>::iterator tmp = list.begin();
        QPainterPath path;
        while (tmp != list.end())
        {
            double x1, y1, x2, y2;
            transformCoord((*tmp)->x(), (*tmp)->y(), x1, y1);
            ++tmp;
            if(tmp == list.end()) break;
            transformCoord((*tmp)->x(), (*tmp)->y(), x2, y2);

            path.moveTo(x1, y1);
            path.lineTo(x2, y2);
        }
        updateShapePath(path);
        painter->drawPath(path);
    }
    DrawableObject2D::paint(painter, option, widget);
}


void DrawablePolyLine2D::swap(DrawablePolyLine2D &first, DrawablePolyLine2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
}

DrawablePolyLine2D& DrawablePolyLine2D::operator=(DrawablePolyLine2D obj)
{
    swap(*this, obj);
    return *this;
}
