/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DrawableText2D.h"
#include "Viewer2D.h"
#include <QPainter>

using namespace PoLAR;

REGISTER_TYPE(DrawableText2D)

DrawableText2D::DrawableText2D(std::list<Marker2D*> list, Viewer2D *Parent, QString text,
                               QColor unselCol, QColor selCol,
                               QColor selmarkCol, MarkerShape unselShape,
                               MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth),
      _fontSize(0), _resizeWithZoom(true)
{
    _object = new Text2D(list);
    setText(text);
    setName("DrawableText2D");
    setType(DrawableObject2D::Text);
}


DrawableText2D::DrawableText2D(Viewer2D *Parent, QString text,
                               QColor unselCol, QColor selCol,
                               QColor selmarkCol, MarkerShape unselShape,
                               MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision, int LineWidth):
    DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                     unselShape,selShape,selmarkShape,
                     Size,Precision, LineWidth),
    _fontSize(0), _resizeWithZoom(true)
{
    _object = new Text2D();
    setText(text);
    setName("DrawableText2D");
    setType(DrawableObject2D::Text);
}


DrawableText2D::DrawableText2D(std::list<Marker2D*> list, Viewer2D *Parent, QString text, QPen& unselectPen,
                               QPen& selectPen, QPen& selectMarkerPen,
                               MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _fontSize(0), _resizeWithZoom(true)
{
    _object = new Text2D(list);
    setText(text);
    setName("DrawableText2D");
    setType(DrawableObject2D::Text);
}

DrawableText2D::DrawableText2D(Viewer2D *Parent,  Text2D *object,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = object;
    setName("DrawableText2D");
    setType(DrawableObject2D::Text);
}

DrawableText2D::DrawableText2D(Viewer2D *Parent, QString text, QPen& unselectPen,
                               QPen& selectPen, QPen& selectMarkerPen,
                               MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _fontSize(0), _resizeWithZoom(true)
{
    _object = new Text2D();
    setText(text);
    setName("DrawableText2D");
    setType(DrawableObject2D::Text);
}

DrawableText2D::DrawableText2D(Viewer2D *Parent, Text2D *object, QPen& unselectPen,
                               QPen& selectPen, QPen& selectMarkerPen,
                               MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                               int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision),
    _fontSize(0), _resizeWithZoom(true)
{
    _object = object;
    setText(object->getText());
    setName("DrawableText2D");
    setType(DrawableObject2D::Text);
}

DrawableText2D::DrawableText2D(DrawableText2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy)
{
    setTextFont(obj._textFont);
    setResizeWithZoom(obj._resizeWithZoom);
}



DrawableText2D::~DrawableText2D()
{
}


void DrawableText2D::setText(QString& text)
{
    Text2D *obj = static_cast<Text2D*>(_object.get());
    obj->setText(text);
}


QString& DrawableText2D::getText()
{
    Text2D *obj = static_cast<Text2D*>(_object.get());
    return obj->getText();
}



void DrawableText2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!isDisplayed() || !_object->nbMarkers()) return;

    if(_currentShapePath) delete _currentShapePath;
    _currentShapePath = new QPainterPath;
    QPen pen = (isSelected() && isEditable() ? _penList[DrawableObject2D::Selected] : _penList[DrawableObject2D::Unselected]);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);
    if(_resizeWithZoom) _textFont.setPointSizeF(_fontSize * _viewer->getZoom());
    painter->setFont(_textFont);

    Marker2D *m;
    std::list<Marker2D *> list = _object->getMarkers();
    std::list<Marker2D *>::iterator tmp = list.begin();
    QPainterPath path;
    while(tmp != list.end())
    {
        m = *tmp;
        double x, y;
        transformCoord(m->x(), m->y(), x, y);
        path.addText(x, y, painter->font(), getText());
        painter->drawText(x, y, getText());
        ++tmp;
    }
    updateShapePath(path);

    DrawableObject2D::paint(painter, option, widget);
}


void DrawableText2D::swap(DrawableText2D &first, DrawableText2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
    using std::swap;
    swap(first._textFont, second._textFont);
    swap(first._fontSize, second._fontSize);
    swap(first._resizeWithZoom, second._resizeWithZoom);
}

DrawableText2D& DrawableText2D::operator=(DrawableText2D obj)
{
    swap(*this, obj);
    return *this;
}
