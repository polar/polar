/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "DrawableMarkers2D.h"

using namespace PoLAR;

REGISTER_TYPE(DrawableMarkers2D)

DrawableMarkers2D::DrawableMarkers2D(std::list<Marker2D*> list, Viewer2D *Parent,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = new Markers2D(list);
    setName("DrawableMarkers2D");
    setType(DrawableObject2D::Markers);
}


DrawableMarkers2D::DrawableMarkers2D(Viewer2D *Parent,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = new Markers2D();
    setName("DrawableMarkers2D");
    setType(DrawableObject2D::Markers);
}


DrawableMarkers2D::DrawableMarkers2D(std::list<Marker2D*> list, Viewer2D *Parent, QPen& unselectPen,
                                     QPen& selectPen, QPen& selectMarkerPen,
                                     MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Markers2D(list);
    setName("DrawableMarkers2D");
    setType(DrawableObject2D::Markers);
}

DrawableMarkers2D::DrawableMarkers2D(Viewer2D *Parent,  Markers2D *object,
                                     QColor unselCol, QColor selCol,
                                     QColor selmarkCol, MarkerShape unselShape,
                                     MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision, int LineWidth)
    : DrawableObject2D(Parent,
                       unselCol,selCol,selmarkCol,
                       unselShape,selShape,selmarkShape,
                       Size,Precision, LineWidth)
{
    _object = object;
    setName("DrawableMarkers2D");
    setType(DrawableObject2D::Markers);
}

DrawableMarkers2D::DrawableMarkers2D(Viewer2D *Parent, QPen& unselectPen,
                                     QPen& selectPen, QPen& selectMarkerPen,
                                     MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = new Markers2D();
    setName("DrawableMarkers2D");
    setType(DrawableObject2D::Markers);
}

DrawableMarkers2D::DrawableMarkers2D(Viewer2D *Parent, Markers2D *object, QPen& unselectPen,
                                     QPen& selectPen, QPen& selectMarkerPen,
                                     MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                     int Size,  int Precision):
    DrawableObject2D(Parent,selectPen,unselectPen,selectMarkerPen,
                     unselShape, selShape, selmarkShape,
                     Size, Precision)
{
    _object = object;
    setName("DrawableMarkers2D");
    setType(DrawableObject2D::Markers);
}

DrawableMarkers2D::DrawableMarkers2D(DrawableMarkers2D &obj, bool deepCopy):
    DrawableObject2D(obj, deepCopy)
{
}


void DrawableMarkers2D::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
    if (!isDisplayed()) return;
    updateShapePath();
    DrawableObject2D::paint(painter, option, widget);
}

void DrawableMarkers2D::swap(DrawableMarkers2D &first, DrawableMarkers2D &second)
{
    DrawableObject2D::swap(static_cast<DrawableObject2D&>(first), static_cast<DrawableObject2D&>(second));
}

DrawableMarkers2D& DrawableMarkers2D::operator=(DrawableMarkers2D obj)
{
    swap(*this, obj);
    return *this;
}
