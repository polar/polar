/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "PolyLine2D.h"

using namespace PoLAR;


PolyLine2D::PolyLine2D(std::list<Marker2D*> &list)
    : Object2D(list)
{
}

PolyLine2D::PolyLine2D()
    : Object2D()
{
}


PolyLine2D::PolyLine2D(PolyLine2D &obj):
    Object2D(obj)
{
}

Marker2D *PolyLine2D::getClosestSegment(Marker2D *marker)
{
    std::list<Marker2D*>::iterator tmp = _listMarkers.begin();
    Marker2D *ret;
    if (_nbMarkers <= 2) return *tmp;

    std::list<Marker2D*>::iterator tmpNext = _listMarkers.begin();
    ++tmpNext;
    float d;
    float dmin = Marker2D::distance2ToSegment(marker, *tmp, *tmpNext);
    ret = *tmp;
    ++tmp;
    ++tmpNext;
    while (tmpNext != _listMarkers.end())
    {
        d = Marker2D::distance2ToSegment(marker, *tmp, *tmpNext);
        if (d < dmin) {dmin = d; ret = *tmp;}
        ++tmp;
        ++tmpNext;
    }
    return ret;
}

Marker2D *PolyLine2D::getClosestSegment(double x, double y)
{
    Marker2D *m = new Marker2D(x,y,0);
    Marker2D *ret = getClosestSegment(m);
    delete m;
    return ret;
}


float PolyLine2D::selectSegment(double x, double y, double precision)
{
    Marker2D *m = new Marker2D(x, y, 0);
    Marker2D *ret = getClosestSegment(m);
    _currentMarker = std::find(_listMarkers.begin(), _listMarkers.end(), ret);
    _currentMarkerIndex = std::distance(_listMarkers.begin(), _currentMarker);
    if (_currentMarker == _listMarkers.end()) return -1;

    std::list<Marker2D*>::iterator tmp = _currentMarker;
    tmp++;
    float dist = (tmp == _listMarkers.end()) ?
                Marker2D::distance2ToSegment(m, *_currentMarker, *tmp)
              : Marker2D::distance2(m, *_currentMarker);
    if (precision >= 0 && dist > precision*precision)
    {
        _currentMarker = _listMarkers.end();
        _currentMarkerIndex = -1;
    }
    return dist;
}

void PolyLine2D::unselectSegment()
{
    unselectMarker();
}

// returns the total length of the polygon
double PolyLine2D::length() const
{
    if (_nbMarkers <= 1) return 0;

    double l=0;
    std::list<Marker2D*>::const_iterator tl = _listMarkers.begin();
    std::list<Marker2D*>::const_iterator tlNext = _listMarkers.begin();
    ++tlNext;
    while (tlNext != _listMarkers.end())
    {
        l += Marker2D::distance(*tl, *tlNext);
        ++tl;
        ++tlNext;
    }

    return l;
}
