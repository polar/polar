/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "ListDrawableObject2D.h"
#include "Viewer2D.h"
#include "Viewer.h"

#include "DrawableMarkers2D.h"
#include "DrawablePolygon2D.h"
#include "DrawablePolyLine2D.h"
#include "DrawableSpline2D.h"
#include "DrawableBlob2D.h"
#include "DrawableArrows2D.h"
#include "DrawableText2D.h"

#include <QDebug>
#include <QtSvg>
#include <iostream>
#include <fstream>

using Qt::ShiftModifier;
using namespace PoLAR;


ListDrawableObject2D::ListDrawableObject2D(Viewer2D *Parent)
    : QObject(Parent),
      _currentObject((DrawableObject2D *)NULL),
      _precision(DrawableObject2D::defaultPrecision),
      _editMarkers(false),
      _viewer(Parent),
      _isCurrent(false)
{
    _scene = dynamic_cast<GraphicsScene*>(_viewer->scene());
    QObject::connect((QObject*)this, SIGNAL(updateSignal()), _viewer->scene(), SLOT(update()));
    QObject::connect((QObject*)this, SIGNAL(selectObjectSignal(DrawableObject2D*)), _viewer, SIGNAL(selectObjectSignal(DrawableObject2D*)));
    Viewer *imgViewer = dynamic_cast<Viewer*>(_viewer);
    if(imgViewer) QObject::connect(imgViewer, SIGNAL(imageIndexSignal(int)), this, SLOT(imageIndexChanged(int)));
}

ListDrawableObject2D::~ListDrawableObject2D()
{
    clear();
}


void ListDrawableObject2D::deactivateCurrentObject()
{
    if (_currentObject && _currentObject->isEmpty()) removeCurrentObject();
    if (_currentObject)
    {
        _currentObject->deactivate();
        unselectObject();
    }
}

void ListDrawableObject2D::activateCurrentObject()
{
    if (_currentObject)
    {
        _currentObject->activate();
        if (_currentObject->isDisplayed()) emit selectObjectSignal(_currentObject);
    }
}


void ListDrawableObject2D::selectObject(DrawableObject2D *object)
{
    // just in case a null object is created and another one is selected by mistake
    // we must remove the empty object since there is no way to select it with
    // the mouse (no marker on it!)
    if (_currentObject && _currentObject->isEmpty()) removeCurrentObject();

    if(!_editMarkers) return;

    if(object && object->isEditable() && object->isDisplayed())
    {

        if (object != _currentObject)
        {
            deactivateCurrentObject();
            _currentObject = object;
            activateCurrentObject();
        }
    }
    emit updateSignal();
}


void ListDrawableObject2D::selectObject(float x, float y)
{
    // just in case a null object is created and another one is selected by mistake
    // we must remove the empty object since there is no way to select it with
    // the mouse (no marker on it!)
    if (_currentObject && _currentObject->isEmpty()) removeCurrentObject();
    std::list<DrawableObject2D*>::iterator itrL;
    itrL=_list.begin();

    while ((itrL != _list.end()) && (*itrL) && !(*itrL)->isEditable() && !(*itrL)->isDisplayed()) itrL++;
    if (*itrL)
    {
        DrawableObject2D *obj = *itrL;
        float dmin = obj->getObject()->select(x,y,-1), d;
        if (obj != _currentObject) obj->getObject()->unselectMarker();
        itrL++;
        while ((itrL != _list.end()) && (*itrL))
        {
            if ((*itrL)->isEditable() && (*itrL)->isDisplayed())
            {
                d = (*itrL)->getObject()->select(x,y,-1);
                if ((*itrL) != _currentObject)
                    (*itrL)->getObject()->unselectMarker();
                if (d < dmin) { dmin = d; obj = (*itrL);}
            }
            itrL++;
        }
        if (dmin > _precision*_precision) obj = (DrawableObject2D *)NULL;
        if (obj != _currentObject)
        {
            deactivateCurrentObject();
            _currentObject = obj;
            activateCurrentObject();
        }
    }
    emit updateSignal();
}


void ListDrawableObject2D::unselectObject()
{
    _currentObject = (DrawableObject2D *)NULL;
    emit unselectObjectSignal();
}

void ListDrawableObject2D::addObject(DrawableObject2D *obj)
{
    if (obj)
    {
        deactivateCurrentObject();

        QObject::connect((QObject*)obj, SIGNAL(destroyed(QObject*)), _viewer, SIGNAL(removeObjectSignal(QObject*)));
        QObject::connect((QObject*)obj, SIGNAL(destroyed(QObject*)), (QObject*)this, SLOT(removeDeadObject(QObject*)));
        QObject::connect((QObject*)obj, SIGNAL(modifyObjectSignal(DrawableObject2D*)), _viewer, SIGNAL(modifyObjectSignal(DrawableObject2D*)));

        _list.push_front(obj);
        updateZValues();
        _currentObject = obj;
        if(_isCurrent && _scene) _scene->addItem(_currentObject);
        if(_editMarkers) activateCurrentObject();
	emit _viewer->addObjectSignal(obj);
    }
}

DrawableObject2D* ListDrawableObject2D::addMarkers(std::list<Marker2D *> listmarker)
{
    DrawableObject2D* obj = new DrawableMarkers2D(listmarker, (Viewer2D*)parent());
    addObject(obj);
    return obj;
}

DrawableObject2D* ListDrawableObject2D::addMarkers()
{
    DrawableObject2D* obj = new DrawableMarkers2D((Viewer2D*)parent());
    addObject(obj);
    return obj;
}

DrawableObject2D* ListDrawableObject2D::addPolygon(std::list<Marker2D *> listmarker)
{
    DrawableObject2D* obj = new DrawablePolygon2D(listmarker, (Viewer2D*)parent());
    addObject(obj);
    return obj;
}

DrawableObject2D *ListDrawableObject2D::addPolygon()
{
    DrawableObject2D* obj = new DrawablePolygon2D((Viewer2D*)parent());
    addObject(obj);
    return obj;
}


DrawableObject2D* ListDrawableObject2D::addPolyLine(std::list<Marker2D *> listmarker)
{
    DrawableObject2D* obj = new DrawablePolyLine2D(listmarker, (Viewer2D*)parent());
    addObject(obj);
    return obj;
}

DrawableObject2D* ListDrawableObject2D::addPolyLine()
{
    DrawableObject2D* obj = new DrawablePolyLine2D((Viewer2D*)parent());
    addObject(obj);
    return obj;
}

DrawableObject2D* ListDrawableObject2D::addSpline(std::list<Marker2D *> listmarker)
{
    DrawableObject2D* obj = new DrawableSpline2D(listmarker, (Viewer2D*)parent());
    addObject(obj);
    return obj;
}


DrawableObject2D* ListDrawableObject2D::addSpline()
{
    DrawableObject2D* obj = new DrawableSpline2D((Viewer2D*)parent());
    addObject(obj);
    return obj;
}


DrawableObject2D* ListDrawableObject2D::addBlob(std::list<Marker2D *> listmarker)
{
    DrawableObject2D* obj = new DrawableBlob2D(listmarker, (Viewer2D*)parent());
    addObject(obj);
    return obj;
}

DrawableObject2D* ListDrawableObject2D::addBlob()
{
    DrawableObject2D* obj = new DrawableBlob2D((Viewer2D*)parent());
    addObject(obj);
    return obj;
}


DrawableObject2D* ListDrawableObject2D::addArrows(std::list<Marker2D *> listmarker)
{
    DrawableObject2D* obj = new DrawableArrows2D(listmarker, (Viewer2D*)parent());
    addObject(obj);
    return obj;
}

DrawableObject2D* ListDrawableObject2D::addArrows()
{
    DrawableObject2D* obj = new DrawableArrows2D((Viewer2D*)parent());
    addObject(obj);
    return obj;
}


DrawableObject2D* ListDrawableObject2D::addText(std::list<Marker2D *> listmarker, QString& text)
{
    DrawableObject2D* obj = new DrawableText2D(listmarker, (Viewer2D*)parent(), text);
    addObject(obj);
    return obj;
}

DrawableObject2D* ListDrawableObject2D::addText(QString& text)
{
    DrawableObject2D* obj = new DrawableText2D((Viewer2D*)parent(), text);
    addObject(obj);
    return obj;
}



void ListDrawableObject2D::removeCurrentObject()
{
    if (_currentObject)
    {
        _currentObject->deactivate();
        if(_isCurrent && _scene) _scene->removeItem(_currentObject);
        delete _currentObject;
        emit updateSignal();
        updateZValues();
    }
    _currentObject = NULL;
}

void ListDrawableObject2D::removeDeadObject(QObject *obj)
{
    if (obj)
    {
        _list.remove((DrawableObject2D*)obj);
        if (obj == _currentObject) unselectObject();
    }
}

void ListDrawableObject2D::clear()
{
    std::list<DrawableObject2D*>::iterator it = _list.begin();
    while (!_list.empty())
    {
        _currentObject = *it;
        removeCurrentObject();
        it = _list.begin();
    }
    _list.clear();
    _currentObject = NULL;
}


void ListDrawableObject2D::save(const char *fname) const
{
    if (_list.size())
    {
        std::ofstream out(fname, std::ios::out);
        if (out.good())
        {
            std::list<DrawableObject2D*>::const_reverse_iterator itrL;
            for (itrL=_list.rbegin(); itrL != _list.rend(); ++itrL)
                if (!(*itrL)->isEmpty()) out << (**itrL);
        }
        out.close();
    }
}


void ListDrawableObject2D::saveAsSvg(QString& fname)
{
    if(!fname.isEmpty())
    {
        QSvgGenerator gen;
        gen.setFileName(fname);
        gen.setDescription(QString("Generated from PoLAR"));
        QString title("ListDrawableObject2D" + QString::number(_viewer->getBgImage()->imageIndex()));
        gen.setTitle(title);
        gen.setSize(QSize(_viewer->width(), _viewer->height()));
        gen.setViewBox(QRect(0,0,_viewer->width(),_viewer->height()));
        QPainter painter;
        painter.begin(&gen);
        std::list<DrawableObject2D*>::reverse_iterator it;
        for(it=_list.rbegin(); it!=_list.rend(); ++it)
        {
            (*it)->paint(&painter, 0, 0);
        }
        painter.end();
    }
}


bool ListDrawableObject2D::load(const char *fname)
{
    std::ifstream in(fname);
    bool ret = load(in);
    in.close();
    return ret;
}

bool ListDrawableObject2D::load(std::istream &in)
{
    DrawableObject2D *obj;
    try
    {
        do
        {
            obj = DrawableObject2D::load(in, (Viewer2D*)parent());
            addObject(obj);
        }
        while (obj);
    }
    catch(DrawableObject2D::IOError &e)
    {
        qWarning() << e;
    }
    return in.eof();
}

void ListDrawableObject2D::changeMarkerLabel(float l)
{
    if (_currentObject && _currentObject->getObject()->getCurrentMarkerIterator() != _currentObject->getObject()->getMarkers().end())
        _currentObject->changeMarkerLabel(l);
}

// display/undisplay current object (true or false)
void ListDrawableObject2D::setCurrentObjectDisplay(bool b)
{
    if (_currentObject)
    {
        if (b==true && !_currentObject->isDisplayed())
        {
            _currentObject->displayOn();
            emit updateSignal();
        }
        else if (b == false && _currentObject->isDisplayed())
        {
            _currentObject->displayOff();
            emit updateSignal();
        }
    }
}

// sets current object editable/uneditable (true or false)
void ListDrawableObject2D::setCurrentObjectEdition(bool b)
{
    if (_currentObject)
    {
        if (b==true && !_currentObject->isEditable())
            _currentObject->editOn();
        else if (b == false && _currentObject->isEditable())
            _currentObject->editOff();
    }
}

void ListDrawableObject2D::startEditMarkers()
{
    int n = _countEditable();
    std::list<DrawableObject2D*>::iterator itrL = _list.begin();
    switch (n)
    {
    case 0:
        break;
    case 1:
        while ((itrL != _list.end()) && (*itrL))
        {
            if ((*itrL)->isEditable() ) {_currentObject = (*itrL); break;}
            itrL++;
        }
        activateCurrentObject();
        break;
    default:
        n = _countDisplayed();
        switch(n)
        {
        case 0: break;
        case 1:
            while ((itrL != _list.end()) && (*itrL))
            {
                if ((*itrL)->isDisplayed()) {_currentObject = (*itrL); break;}
                itrL++;
            }
            activateCurrentObject();
            break;
        default:
            activateCurrentObject();
            break;
        }
        break;
    }
    _editMarkers = true;
    emit updateSignal();
}

void ListDrawableObject2D::stopEditMarkers()
{
    deactivateCurrentObject();
    if (!parent()) qDebug("ListDrawableObject2D::stopEditMarkers() disconnect");
    _editMarkers = false;
    emit updateSignal();
}

std::list<DrawableObject2D*> ListDrawableObject2D::getObjectsByName(const char* name) const
{
    std::list<DrawableObject2D*> computedList;
    if (_list.size())
    {
      for (auto o: _list)
	if (!strcmp((char*)(o->getName()), name)) computedList.push_back(o); 
    }
    return computedList;
}

std::list<DrawableObject2D*> ListDrawableObject2D::getObjectsByType(char type) const
{
    std::list<DrawableObject2D*> computedList;
    if (_list.size())
    {
      for (auto o:_list)
	if (o->getType() == type) computedList.push_back(o);
    }
    return computedList;
}


std::list<DrawableObject2D*> ListDrawableObject2D::getObjectsByType(const char* type) const
{
    std::list<DrawableObject2D*> computedList;
    if (_list.size())
    {
        std::list<DrawableObject2D*>::const_iterator itrL;
        for (itrL=_list.begin(); itrL != _list.end(); ++itrL)
            if(!strcmp((*itrL)->className(), type))
                computedList.push_back(*itrL);
    }
    return computedList;
}



DrawableObject2D* ListDrawableObject2D::getObjectByIndex(unsigned int index) const
{
    if (_list.size() > index)
    {
        index++;
        std::list<DrawableObject2D*>::const_iterator itrL = _list.end();
        while (index != 0) index--, itrL--;
        return (*itrL);
    }
    else return (DrawableObject2D*)NULL;
}


void ListDrawableObject2D::updateZValues()
{
    std::list<DrawableObject2D*>::iterator it = _list.begin();
    int zVal = countObjects();
    while(it != _list.end())
    {
        (*it)->setZValue(zVal);
        zVal--;
        ++it;
    }
}


void ListDrawableObject2D::imageIndexChanged(int index)
{
    if(_isCurrent) // if the list is the current one to be drawn
    {
        if(_scene && _scene->getImageIndex() != index) // the scene is outdated
        {
            if(_scene->countItems() != 1) _scene->removeAllItems(); //if there is only the GLWidget left, no need to delete the items

            if(_list.size())
            {
                std::list<DrawableObject2D*>::iterator it = _list.begin();
                while(it != _list.end())
                {
                    _scene->addItem(*it);
                    ++it;
                }
            }
            _scene->setImageIndex(index); // update the scene current index
        }
    }
}
