/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Polygon2D.h"
#include <iostream>

using namespace PoLAR;


Polygon2D::Polygon2D(std::list<Marker2D*> list)
    : Object2D(list)
{
}


Polygon2D::Polygon2D()
    : Object2D()
{
}


Polygon2D::Polygon2D(Polygon2D &obj):
    Object2D(obj)
{
}


Marker2D *Polygon2D::getClosestSegment(Marker2D *marker)
{
    std::list<Marker2D*>::iterator tmp = _listMarkers.begin();
    Marker2D *ret;
    if (_nbMarkers <= 2)
    {
        return *tmp;
    }
    std::list<Marker2D*>::iterator tmpNext = _listMarkers.begin();
    ++tmpNext;
    double dmin = Marker2D::distance2ToSegment(marker, *tmp, *tmpNext);
    double d;
    ret = *tmp;
    ++tmp;
    ++tmpNext;
    while (tmpNext != _listMarkers.end())
    {
        d = Marker2D::distance2ToSegment(marker, *tmp, *tmpNext);
        if (d < dmin)
        {
            dmin = d;
            ret = *tmp;
        }
        ++tmp;
        ++tmpNext;
    }
    d = Marker2D::distance2ToSegment(marker, *tmp, _listMarkers.front());

    if (d < dmin)
    {
        return *tmp;
    }
    else
    {
        return ret;
    }
}


Marker2D *Polygon2D::getClosestSegment(double x, double y)
{
    Marker2D m(x,y,0);
    Marker2D *ret = getClosestSegment(&m);
    return ret;
}


double Polygon2D::selectSegment(double x, double y, double precision)
{
    Marker2D m(x, y, 0);
    _currentMarker = std::find(_listMarkers.begin(), _listMarkers.end(), getClosestSegment(&m));
    if (_currentMarker == _listMarkers.end())
    {
        _currentMarkerIndex = -1;
        return -1;
    }
    _currentMarkerIndex = std::distance(_listMarkers.begin(), _currentMarker);
    std::list<Marker2D*>::iterator next = _currentMarker;
    next++;
    double dist = (next != _listMarkers.end())
            ? Marker2D::distance2ToSegment(&m, *_currentMarker, *next)
            : Marker2D::distance2ToSegment(&m, *_currentMarker, _listMarkers.front());
    if (precision >= 0 && dist > precision*precision)
    {
        _currentMarker = _listMarkers.end();
        _currentMarkerIndex = -1;
    }
    return dist;
}

void Polygon2D::unselectSegment()
{
    unselectMarker();
}


void Polygon2D::addMarker(Marker2D *marker)
{
    if(_nbMarkers == 0) // first marker
    {
        _listMarkers.push_front(marker);
        _currentMarker = _listMarkers.begin();
        _currentMarkerIndex = 0;
    }
    else
    {
        Marker2D *closest = getClosestSegment(marker);
        std::list<Marker2D*>::iterator closestIt = std::find(_listMarkers.begin(), _listMarkers.end(), closest);
        ++closestIt;
        _currentMarker = _listMarkers.insert(closestIt, marker);
        _currentMarkerIndex = std::distance(_listMarkers.begin(), _currentMarker);
    }
    updateNbMarkers();
}


// returns the total length of the polygon
double Polygon2D::length() const
{
    if (_nbMarkers <= 1) return 0;

    double l = 0;
    std::list<Marker2D*>::const_iterator tl = _listMarkers.begin();
    std::list<Marker2D*>::const_iterator tlNext = _listMarkers.begin();
    ++tlNext;
    while (tlNext != _listMarkers.end() && tl != _listMarkers.end())
    {
        l += Marker2D::distance(*tl, *tlNext);
        ++tl;
        ++tlNext;

    }
    // last segment to close the polygon
    l += Marker2D::distance(*tl, _listMarkers.front());
    return l;
}



void Polygon2D::revertOrientation()
{
    _listMarkers.reverse();
    int rank = _nbMarkers - 1 - _currentMarkerIndex;
    _currentMarker = _listMarkers.begin();
    std::advance(_currentMarker, rank);
    _currentMarkerIndex = rank;
}


