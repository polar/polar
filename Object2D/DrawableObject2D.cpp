/******************************************************************
*        PoLAR (Portable Library for Augmented Reality)           *
*     Copyright (C) 2015 Inria, Université de Lorraine, CNRS      *
*                                                                 *
* This library is free software; you can redistribute it and/or   *
* modify it under the terms of the GNU General Public License     *
* as published by the Free Software Foundation; either version 3  *
* of the License, or (at your option) any later version.          *
*                                                                 *
* This library is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of  *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the    *
* GNU General Public License for more details.                    *
*                                                                 *
*                                                                 *
* You should have received a copy of the GNU General Public       *
* License along with this library.                                *
* If not, see <http://www.gnu.org/licenses/>.                     *
******************************************************************/

#include "Viewer2D.h"
#include "DrawableObject2D.h"
#include "DrawableMarkers2D.h"
#include "DrawablePolygon2D.h"
#include "DrawablePolyLine2D.h"
#include "DrawableSpline2D.h"
#include "DrawableBlob2D.h"
#include "DrawableArrows2D.h"
#include "DrawableText2D.h"
#include "Marker2D.h"

#include <QtSvg>

using Qt::ShiftModifier;

using namespace PoLAR;

const QColor DrawableObject2D::defaultSelCol = QColor("yellow");
const QColor DrawableObject2D::defaultUnselCol = QColor("blue");
const QColor DrawableObject2D::defaultSelmarkCol = QColor("red");
const int DrawableObject2D::NbMarkerShape = 5;
const int DrawableObject2D::defaultPrecision = 3;
const int DrawableObject2D::defaultSize = 3;
const int DrawableObject2D::defaultLineWidth = 1;
const DrawableObject2D::MarkerShape DrawableObject2D::defaultSelShape = SQUARE;
const DrawableObject2D::MarkerShape DrawableObject2D::defaultSelmarkShape = SQUARE;
const DrawableObject2D::MarkerShape DrawableObject2D::defaultUnselShape = CROSS;

std::map<std::string, DrawableObject2DFactory*> DrawableObject2D::_factories;

void DrawableObject2D::readHeader(std::istream &s, std::string &type, std::string &name, QColor &c, unsigned int &np)
{
    s >> type;
    if(type.empty()) return; // we reached EOF or the file is corrupted: it is unnecessary to continue the processing

    if (!DrawableObject2D::isTypeValid(type))
        throw DrawableObject2D::IOError("DrawableObject2D::load failed: object type not handled ("+type+")");

    s >> name;
    if (name.size() == 0)
        throw DrawableObject2D::IOError("DrawableObject2D::load failed: object name is empty");

    std::string m;
    s >> m;
    c = QColor(m.c_str());
    if (!c.isValid())
        throw DrawableObject2D::IOError("DrawableObject2D::load failed: color is invalid ("+m+")");

    s >> np;
    if (!s.good())
        throw DrawableObject2D::IOError("DrawableObject2D::load failed: number of points not present?");
}


bool DrawableObject2D::isTypeValid(const std::string &s)
{
    // for retrocompatibility purpose
    if(s.size() == 1)
    {
        char c = s.c_str()[0];
        return c==char(Markers) || c==char(Arrows) || c==char(Blob) || c==char(Spline)
                || c==char(Polygon) || c==char(PolyLine) || c==char(Text);
    }
    else
    {
        // the type is known in the list of registered types
        if(_factories.find(s) != _factories.end())
            return true;
    }
    return false;
}


DrawableObject2D::DrawableObject2D(Viewer2D *Parent,
                                   QColor unselCol, QColor selCol, QColor selmarkCol,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size, int Precision, int LineWidth):
    QObject(Parent), _viewer(Parent), _unselcol(unselCol), _selcol(selCol), _selmarkcol(selmarkCol),
    _currentShapePath(new QPainterPath)
{

    if(Parent)
    {
        QObject::connect((QObject*)this, SIGNAL(updateSignal()), Parent, SLOT(requestRedraw()));
        QObject::connect((QObject*)this, SIGNAL(unselectMarkerSignal()), Parent, SLOT(unselectMarkerSlot()));
        QObject::connect((QObject*)this, SIGNAL(selectMarkerSignal(Marker2D*)), Parent, SLOT(selectMarkerSlot(Marker2D*)));
    }
    setMarkerSize(Size);
    setPointingPrecision(Precision);
    setLineWidth(LineWidth);
    setSelectColor(selCol);
    setUnselectColor(unselCol);
    setSelectMarkerColor(selmarkCol);
    setSelectShape(selShape);
    setUnselectShape(unselShape);
    setSelectMarkerShape(selmarkShape);
    displayOn();
    editOn();
    selectOn();
    initPens();
}


DrawableObject2D::DrawableObject2D(Viewer2D *Parent,
                                   QPen& unselectPen, QPen& selectPen, QPen& selectMarkerPen,
                                   MarkerShape unselShape, MarkerShape selShape, MarkerShape selmarkShape,
                                   int Size,  int Precision):
    QObject(Parent), _viewer(Parent), _currentShapePath(new QPainterPath)
{
    _unselcol = unselectPen.color();
    _selcol = selectPen.color();
    _selmarkcol = selectMarkerPen.color();
    if(Parent)
    {
        QObject::connect((QObject*)this, SIGNAL(updateSignal()), Parent, SLOT(requestRedraw()));
        QObject::connect((QObject*)this, SIGNAL(unselectMarkerSignal()), Parent, SLOT(unselectMarkerSlot()));
        QObject::connect((QObject*)this, SIGNAL(selectMarkerSignal(Marker2D*)), Parent, SLOT(selectMarkerSlot(Marker2D*)));
    }
    setPens(selectPen, unselectPen, selectMarkerPen);
    setSelectColor(_selcol);
    setUnselectColor(_unselcol);
    setSelectMarkerColor(_selmarkcol);
    setMarkerSize(Size);
    setPointingPrecision(Precision);
    setLineWidth(selectPen.width());
    setSelectShape(selShape);
    setUnselectShape(unselShape);
    setSelectMarkerShape(selmarkShape);
    displayOn();
    editOn();
    selectOn();
}




DrawableObject2D::DrawableObject2D(DrawableObject2D &obj, bool deepCopy):
    QObject(obj.parent()),
    QGraphicsItem(),
    _viewer(obj._viewer),
    _unselcol(obj._unselcol),
    _selcol(obj._selcol),
    _selmarkcol(obj._selmarkcol),
    _currentShapePath(new QPainterPath),
    _display(obj._display),
    _edit(obj._edit),
    _select(obj._select)
{
    if(deepCopy)
        _object = obj._object->clone();
    else
        _object = obj._object;

    if(_viewer)
    {
        QObject::connect((QObject*)this, SIGNAL(updateSignal()), _viewer, SLOT(requestRedraw()));
        QObject::connect((QObject*)this, SIGNAL(unselectMarkerSignal()), _viewer, SLOT(unselectMarkerSlot()));
        QObject::connect((QObject*)this, SIGNAL(selectMarkerSignal(Marker2D*)), _viewer, SLOT(selectMarkerSlot(Marker2D*)));
    }

    setSelectColor(_selcol);
    setUnselectColor(_unselcol);
    setSelectMarkerColor(_selmarkcol);
    setMarkerSize(obj.getMarkerSize());
    setPointingPrecision(obj.getPointingPrecision());
    setLineWidth(obj.getLineWidth());
    setSelectShape(obj.getSelectShape());
    setUnselectShape(obj.getUnselectShape());
    setSelectMarkerShape(obj.getSelectMarkerShape());
    setPens(obj._penList.at(0), obj._penList.at(1), obj._penList.at(2));
    setObjectName(obj.objectName());
    setType(obj.getType());
}


DrawableObject2D::~DrawableObject2D()
{
    emit modifyObjectSignal(this);

    if(_currentShapePath) delete _currentShapePath;

    _penList.clear();
}



void DrawableObject2D::swap(DrawableObject2D &first, DrawableObject2D &second)
{
    using std::swap;

    swap(first._viewer, second._viewer);
    swap(first._unselcol, second._unselcol);
    swap(first._selcol, second._selcol);
    swap(first._selmarkcol, second._selmarkcol);
    swap(first._object, second._object);
    swap(first._size, second._size);
    swap(first._linewidth, second._linewidth);
    swap(first._penList, second._penList);
    swap(first.precision, second.precision);
    swap(first.selshape, second.selshape);
    swap(first.unselshape, second.unselshape);
    swap(first.selmarkshape, second.selmarkshape);
    swap(first._display, second._display);
    swap(first._edit, second._edit);
    swap(first._select, second._select);
    swap(first._type, second._type);
    first.setObjectName(second.objectName());
}



void DrawableObject2D::deactivate()
{
    selectOff();
    _object->unselectMarker();
    emit unselectMarkerSignal();
    if (!parent()) qDebug("DrawableObject2D::deactivate() disconnect");
    QObject::disconnect(_viewer,0,this,0);
}

void DrawableObject2D::activate()
{
    if (isEditable())
    {
        selectOn();
        QObject::connect(_viewer, SIGNAL(markerManipulationSignal(double, double, int)), this, SLOT(markerManipulationSlot(double, double, int)), Qt::UniqueConnection);
        QObject::connect(_viewer, SIGNAL(markerReleaseSignal()), this, SLOT(markerReleaseSlot()));
    }
}


void DrawableObject2D::addMarker(double x, double y)
{
    _object->selectMarker(x, y, _size);
    Marker2D *m = _object->getCurrentMarker();
    if(!m) // the marker does not already exist
    {
        _object->addMarker(new Marker2D(x,y,0));
        emit modifyObjectSignal(this);
        emit selectMarkerSignal(_object->getCurrentMarker());
    }
    else
    {
        std::cout << (*m);
        emit selectMarkerSignal(_object->getCurrentMarker());
    }
    emit updateSignal();
}


void DrawableObject2D::removeMarker(double x, double y)
{
    _object->selectMarker(x, y, _size);
    Marker2D *m = _object->getCurrentMarker();
    if(m)
    {
        _object->removeMarker();
        delete m;
        emit modifyObjectSignal(this);
        emit unselectMarkerSignal();
    }
    else unselectMarker();
    emit updateSignal();
}


void DrawableObject2D::removeCurrentMarker()
{
    Marker2D *m = _object->getCurrentMarker();
    if(m)
    {
        _object->removeMarker();
        delete m;
        emit modifyObjectSignal(this);
        emit unselectMarkerSignal();
    }
    else unselectMarker();
    emit updateSignal();
}


void DrawableObject2D::printMarker(double x, double y)
{
    _object->selectMarker(x, y, _size);
    Marker2D *m = _object->getCurrentMarker();
    if(m)
    {
        std::cout << (*m);
        emit selectMarkerSignal(_object->getCurrentMarker());
    }
    emit updateSignal();
}


void DrawableObject2D::selectMarker(double x, double y)
{
    _object->selectMarker(x, y, _size);
    Marker2D *m = _object->getCurrentMarker();
    if(m)
    {
        ex = m->x() - x, ey = m->y() - y;
        QObject::connect(_viewer, SIGNAL(markerMoveSignal(double, double)), this, SLOT(markerMoveSlot(double, double)));
        emit selectMarkerSignal(_object->getCurrentMarker());
    }
    else unselectMarker();
    emit updateSignal();
}


void DrawableObject2D::unselectMarker()
{
    emit unselectMarkerSignal();
    emit updateSignal();
}

void DrawableObject2D::markerManipulationSlot(double x, double y, int type)
{
    if (type == SELECT) // select marker + move marker
    {
        selectMarker(x, y);
    }
    else if (type == REMOVE) // remove marker
    {
        removeMarker(x, y);
    }
    else if (type == ADD) // add marker
    {
        addMarker(x, y);
    }
}


void DrawableObject2D::markerMoveSlot(double x, double y)
{
    _object->moveCurrentMarker(x+ex,y+ey);
    emit modifyObjectSignal(this);
    emit updateSignal();
}

void DrawableObject2D::markerReleaseSlot()
{
    if (!parent()) qDebug("DrawableObject2D::mouseReleaseSlot() disconnect");
    QObject::disconnect(_viewer,SIGNAL(markerMoveSignal(double, double)),this,0);
}

void DrawableObject2D::changeMarkerLabel(float l)
{
    if (_object->getCurrentMarker())
    {
        (_object->getCurrentMarker())->setLabel(l);
        emit modifyObjectSignal(this);
    }
}

// properties management

void DrawableObject2D::setPens(QPen& select, QPen& unselect, QPen& selectMarker)
{
    if(_penList.size() == 0)
    {
        _penList.push_back(select);
        _penList.push_back(unselect);
        _penList.push_back(selectMarker);
    }
    else
    {
        _penList[DrawableObject2D::Selected] = select;
        _penList[DrawableObject2D::Unselected] = unselect;
        _penList[DrawableObject2D::MarkerSelected] = selectMarker;
    }
    updateSettingsFromPens();
}

void DrawableObject2D::setPen(QPen &pen, PenType type)
{
    if(_penList.size() == 0) initPens();
    if(type >= _penList.size()) return;
    _penList[type] = pen;
    updateSettingsFromPens();
}


QPen& DrawableObject2D::getPen(PenType type)
{
    return _penList[type];
}


void DrawableObject2D::initPens()
{
    if(_penList.size() == 0)
    {
        QPen select(_selcol);
        select.setWidth(_linewidth);
        _penList.push_back(select);

        QPen unselect(_unselcol);
        unselect.setWidth(_linewidth);
        _penList.push_back(unselect);

        QPen selectMarker(_selmarkcol);
        selectMarker.setWidth(_linewidth);
        _penList.push_back(selectMarker);
    }
}


void DrawableObject2D::reinitPenColor(PenType ind)
{
    if(ind >= _penList.size()) return;
    QColor color = (ind == DrawableObject2D::Selected ? _selcol : (ind == DrawableObject2D::Unselected ? _unselcol : _selmarkcol));
    _penList[ind].setColor(color);
}


void DrawableObject2D::updateSettingsFromPens()
{
    _selcol = _penList[0].color();
    _linewidth = _penList[0].width();
    _unselcol = _penList[1].color();
    _selmarkcol = _penList[2].color();
}

void DrawableObject2D::reinitPensWidth(int width)
{
    for(unsigned int i=0; i<_penList.size(); i++)
    {
        _penList[i].setWidth(width);
    }
}

/** set selection color
 */
void DrawableObject2D::setSelectColor(QColor color)
{
    _selcol = color;
    reinitPenColor(DrawableObject2D::Selected);
    emit updateSignal();
}

/** set selected marker color
 */
void DrawableObject2D::setSelectMarkerColor(QColor color)
{
    _selmarkcol = color;
    reinitPenColor(DrawableObject2D::MarkerSelected);
    emit updateSignal();
}

/** set unselected color
 */
void DrawableObject2D::setUnselectColor(QColor color)
{
    _unselcol = color;
    reinitPenColor(DrawableObject2D::Unselected);
    emit updateSignal();
}

// set marker shape when object is selected
void DrawableObject2D::setSelectShape(MarkerShape shape)
{
    selshape = shape >= 0 && shape < NbMarkerShape ? shape : defaultSelShape;
    emit updateSignal();
}

// set selected marker shape
void DrawableObject2D::setSelectMarkerShape(MarkerShape shape)
{
    selmarkshape = shape >= 0 && shape < NbMarkerShape ? shape : defaultSelmarkShape;
    emit updateSignal();
}

// set unselected marker shape
void DrawableObject2D::setUnselectShape(MarkerShape shape)
{
    unselshape = shape >= 0 && shape < NbMarkerShape ? shape : defaultUnselShape;
    emit updateSignal();
}

// set marker size
void DrawableObject2D::setMarkerSize(int Size)
{
    _size = Size > 0 ? Size : defaultSize;
    emit updateSignal();
}

// set pointing precision
void DrawableObject2D::setPointingPrecision(int Precision)
{
    precision = Precision > 0 ? Precision : defaultPrecision;
}

// set line width
void DrawableObject2D::setLineWidth(int LineWidth)
{
    _linewidth = LineWidth > 0 ? LineWidth : defaultLineWidth;
    reinitPensWidth(_linewidth);
    emit updateSignal();
}

void DrawableObject2D::setProperties(QColor unselectColor,
                                     QColor selectColor,
                                     QColor selectMarkerColor,
                                     MarkerShape unselectShape,
                                     MarkerShape selectShape,
                                     MarkerShape selectMarkerShape,
                                     int Size,
                                     int Precision,
                                     int LineWidth)
{
    setMarkerSize(Size);
    setPointingPrecision(Precision);
    setLineWidth(LineWidth);
    setUnselectColor(unselectColor);
    setSelectColor(selectColor);
    setSelectMarkerColor(selectMarkerColor);
    setUnselectShape(unselectShape);
    setSelectShape(selectShape);
    setSelectMarkerShape(selectMarkerShape);
}

void DrawableObject2D::save(std::ostream &out) const
{
    //out << char(getType()) << " " << getName() <<" #";
    out << className() << " " << getName() <<" #";
    QColor uc = getUnselectColor();
    out.flags(std::ios::hex | std::ios::uppercase);
    out << std::setw(2) << std::setfill('0') << uc.red()
        << std::setw(2) << std::setfill('0') << uc.green()
        << std::setw(2) << std::setfill('0') << uc.blue();
    out.unsetf(std::ios::hex | std::ios::uppercase);

    out << " " << _object->nbMarkers() << std::endl;

    out.setf(std::ios::fixed,std::ios::floatfield);
    out.precision(6);

    std::list<Marker2D*> list = _object->getMarkers();
    list.reverse(); // To print the markers in the original ordering. Markers are stored in a LIFO list

    for(std::list<Marker2D*>::iterator it = list.begin(); it != list.end(); ++it)
    {
        (*it)->save(out);
    }
}


void DrawableObject2D::saveAsSvg(QString& fname)
{
    if(!fname.isEmpty())
    {
        QSvgGenerator gen;
        gen.setFileName(fname);
        gen.setDescription(QString("Generated from PoLAR"));
        gen.setSize(QSize(_viewer->width(), _viewer->height()));
        gen.setViewBox(QRect(0,0,_viewer->width(),_viewer->height()));
        gen.setTitle(getName());
        QPainter painter;
        painter.begin(&gen);
        paint(&painter, 0, 0);
        painter.end();
    }
}


DrawableObject2D *DrawableObject2D::load(std::istream &in, Viewer2D *Parent)
{
    std::string type;
    std::string name;
    QColor c;
    unsigned int np;
    std::list<Marker2D*> l;
    DrawableObject2D *ret = NULL;
    if (in.eof()) return ret;
    try
    {
        DrawableObject2D::readHeader(in, type, name, c, np);
        if(type.empty()) return NULL;
        Marker2D m;
        unsigned int i;
        for (i=0; i<np; i++)
        {
            DrawableObject2D::readMarker2D(in,m);
            l.push_front(new Marker2D(&m));
        }
    }
    catch (DrawableObject2D::IOError())
    {
        return NULL;
    }

    // For retrocompatibility purpose
    if(type.size() == 1)
    {
        char c = type.c_str()[0];
        switch (c)
        {
        case DrawableObject2D::Markers: ret = new DrawableMarkers2D(l,Parent); break;
        case DrawableObject2D::Arrows: ret = new DrawableArrows2D(l,Parent); break;
        case DrawableObject2D::Blob: ret = new DrawableBlob2D(l,Parent); break;
        case DrawableObject2D::Spline: ret = new DrawableSpline2D(l,Parent); break;
        case DrawableObject2D::Polygon: ret = new DrawablePolygon2D(l,Parent); break;
        case DrawableObject2D::PolyLine: ret = new DrawablePolyLine2D(l,Parent); break;
        case DrawableObject2D::Text: ret = new DrawableText2D(l,Parent); break;
        }
    }
    else
    {
        ret = create(type, l, Parent);
    }
    if (ret)
    {
        ret->setUnselectColor(c);
        ret->setName(name.c_str());
        ret->setSelect(false);
        ret->load(in); // load the custom parameters for subclasses
    }
    return ret;
}



void DrawableObject2D::drawMarkerShape(QPainter *painter, MarkerShape selectedShape, int x, int y)
{
    float zoom = _viewer->getZoom();
    int s = _size * zoom;

    // we start by creating a cross at the position (x,y)
    QPainterPath cross;
    cross.moveTo(x, y-s);
    cross.lineTo(x, y+s);
    cross.moveTo(x-s, y);
    cross.lineTo(x+s, y);

    switch(selectedShape)
    {
    case DrawableObject2D::CROSS:
        // we deactivate antialiasing to avoid blurry straight lines
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform, false);
        painter->drawPath(cross);
        break;
    case DrawableObject2D::SQUARE:
        // we deactivate antialiasing to avoid blurry straight lines
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform, false);
        painter->drawRect(x-s, y-s, 2*s, 2*s);
        _currentShapePath->addRect(x-s, y-s, 2*s, 2*s);
        break;
    case DrawableObject2D::POINT:
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);
        painter->drawPoint(x, y);
        break;
    case DrawableObject2D::CIRCLE:
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing | QPainter::SmoothPixmapTransform);
        painter->drawEllipse(QPoint(x, y), s, s);
        _currentShapePath->addEllipse(QPoint(x, y), s, s);
        break;
    default: // unrecognized type
        break;
    }
    _currentShapePath->addPath(cross); // we add a cross to the bounding region too for easier selection

}


void DrawableObject2D::drawMarkers(QPainter *painter)
{
    Marker2D *m = NULL;
    if(isEditable()) m = _object->getCurrentMarker();

    MarkerShape shape = isSelected() && isEditable() ? selshape : unselshape;

    std::list<Marker2D*> list = _object->getMarkers();
    std::list<Marker2D*>::iterator tmp = list.begin();

    QPen pen = isSelected() && isEditable() ? _penList[Selected] : _penList[Unselected];
    painter->setPen(pen);
    while (tmp != list.end())
    {
        if (*tmp != m) // we jump the selected marker (will be drawn after all the others)
        {
            double x = (*tmp)->x();
            double y = (*tmp)->y();
            double ix, iy;
            transformCoord(x, y, ix, iy);
            drawMarkerShape(painter, shape, ix, iy);
        }
        ++tmp;
    }

    if (_object->nbMarkers() && m)
    { // the selected marker
        QPen pen(_penList[MarkerSelected]);
        painter->setPen(pen);
        double x = m->x();
        double y = m->y();
        double ix, iy;
        transformCoord(x, y, ix, iy);
        drawMarkerShape(painter, selmarkshape, ix, iy);
    }
}


void DrawableObject2D::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    drawMarkers(painter);
    (void)option;
    (void)widget;
}


QRectF DrawableObject2D::boundingRect() const
{
#if 0
    float xll, yll, xur, yur;
    _object->boundingBox(&xll, &yll, &xur, &yur);
    return QRectF(xll, yur, xur, yll);
#else
    //if(_currentShapePath) return _currentShapePath->controlPointRect();
    return QRectF(0,0,0,0);
#endif
}


QPainterPath DrawableObject2D::shape() const
{
    // we create a stroke of the current path in order to get enough width to click it
    QPainterPathStroker stroker;
    stroker.setWidth(precision /* _viewer->getZoom()*/);
    return stroker.createStroke(*_currentShapePath);
}


void DrawableObject2D::updateShapePath(QPainterPath &path)
{
    if(_currentShapePath) delete _currentShapePath;
    _currentShapePath = new QPainterPath;
    _currentShapePath->addPath(path);
}


void DrawableObject2D::updateShapePath()
{
    if(_currentShapePath) delete _currentShapePath;
    _currentShapePath = new QPainterPath;
}


void DrawableObject2D::transformCoord(double xima, double yima, double &xqt, double &yqt) const
{
    _viewer->ImageToQt(xima, yima, xqt, yqt);
}


std::ostream& PoLAR::operator<<(std::ostream& out, const PoLAR::DrawableObject2D& object2D)
{
    object2D.save(out);
    return out;
}
